package com.appy.android.code.domain.model.status.dimmer

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
internal class GetStatusDimmerForm(
        internal val homeId: String,
        internal val dimmerId: String
) : AppyForm()
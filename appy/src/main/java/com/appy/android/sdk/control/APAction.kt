package com.appy.android.sdk.control

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
open class APAction<T : Serializable> : Parcelable {

    var name: String? = null

    var description: String? = null

    var action: T? = null

    constructor()

    constructor(source: Parcel) : this() {
        name = source.readString()
        description = source.readString()

        source.readSerializable()?.let {
            action = it as T
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        dest.writeString(description)
        dest.writeSerializable(action)
    }

    companion object CREATOR : Parcelable.Creator<APAction<Serializable>> {
        override fun createFromParcel(parcel: Parcel): APAction<Serializable> {
            return APAction(parcel)
        }

        override fun newArray(size: Int): Array<APAction<Serializable>?> {
            return arrayOfNulls(size)
        }
    }
}
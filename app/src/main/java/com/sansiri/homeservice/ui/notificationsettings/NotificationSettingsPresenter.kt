package com.sansiri.homeservice.ui.notificationsettings

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 1/9/18.
 */
class NotificationSettingsPresenter : BasePresenterImpl<NotificationSettingsContract.View>(), NotificationSettingsContract.Presenter {
    override fun start() {

    }

    override fun destroy() {
    }
}
package com.sansiri.homeservice.ui.electric.mea.login

import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MeaElectricLoginContract {
    interface Presenter : BasePresenter<View> {
        fun login(ca: String)
        fun init(home: Hut, meaInfo: MEAInfo?)
        fun saveCA(ca: String)
        fun process(code: String?)
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun launchHome(ca: String)
        fun fillCA(ca: String?)
    }
}
package com.sansiri.homeservice.model.api.announcement

import android.os.Parcelable
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

interface Announcement<T: AnnouncementDetail>: Parcelable {
    var objectId: String?
    var isAutoActivate: Boolean?
    var isSupportNonUser: Boolean?
    var isActive: Boolean?
    var updatedAt: String?
    var createdAt: String?
    var publishedAt: String?
    var createdBy: String?
    var details: List<T>?

    val displayDate: Date?
        get() {
            try {
                return DateTime(this.publishedAt).toDate()
            } catch (e: ParseException) {
                return null
            }
        }
}

package com.sansiri.homeservice.ui.homecare.history

import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.model.api.ListResponse
import com.sansiri.homeservice.model.api.homecare.ListHomeCareResponse
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

/**
 * Created by sansiri on 10/11/17.
 */
interface HomeCareHistoryContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun fetchData(currentPage: Int)
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showNoData()
        fun addData(histories: ListHomeCareResponse<HomeCareHistory>)
        fun showExpiredDate(expiredDate: Date?)
    }
}
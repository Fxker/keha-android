package com.sansiri.homeservice.ui.meter.water

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.meter.SmartMeterRepository
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.toISO8601
import java.util.*

class SmartWaterMeterPresenter(val repository: SmartMeterRepository) : BasePresenterImpl<SmartWaterMeterContract.View>(), SmartWaterMeterContract.Presenter {
    override var smartWaterMeter: MutableLiveData<Resource<SmartWaterMeterOverAll>>? = null
    var mUnitId: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
    }

    fun fetch() {
        if (mUnitId != null) {
            smartWaterMeter = repository.getSmartWaterMeterSummary(mUnitId!!, "default", Calendar.getInstance().time.toISO8601())
        }
    }
}
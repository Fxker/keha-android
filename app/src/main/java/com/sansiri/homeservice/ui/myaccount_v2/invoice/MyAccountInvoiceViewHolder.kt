package com.sansiri.homeservice.ui.myaccount_v2.invoice

import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_payable_card.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class MyAccountInvoiceViewHolder(val view: View, val itemClick: (Invoice) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(invoice: Invoice, color: Int) {
        with(view) {
            imageIcon.setImageResource(R.drawable.ic_home_wallet)
            textTitle.setTextColor(context.resources.getColor(color))
            textTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_headline2))
            textSubtitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_body2))
            textDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_body2))

            textSubtitle.text = "${context.getString(R.string.due)}: ${invoice._dueAt?.reportDMYFull()}"
            textTitle.text = invoice.grandTotal.toCurrencyFormat()
            textDescription.text = invoice._createdAt?.report()

            if (invoice.paymentBarcode != null) {
                layoutPayable.show()
                layoutUnPayable.hide()
            } else {
                layoutPayable.hide()
                layoutUnPayable.show()
            }

            root.setOnClickListener {
                itemClick.invoke(invoice)
            }
        }
    }
}
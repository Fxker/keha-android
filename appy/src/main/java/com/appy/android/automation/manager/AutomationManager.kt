package com.appy.android.automation.manager

import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import com.appy.android.automation.helper.Validator
import com.appy.android.automation.network.BTCStatusListener
import com.appy.android.automation.network.SocketHandler
import com.appy.android.automation.network.TCPSocketHandler
import com.appy.android.automation.network.THMStatusListener
import com.appy.android.sdk.AppySDK
import com.bticino.openwebnet.OpenWebNetUtils
import java.util.*

class AutomationManager private constructor() {

    private val BTCStatusSockets = ArrayList<TCPSocketHandler>()
    private val statusListener: THMStatusListener? = null

    private fun calculatePassword(seed: String, OPENPassword: String): String? {
        if (!Validator.isValid(seed) || !Validator.isValid(OPENPassword)) {
            return null
        }

        val seedString = seed.replace("[^0-9]".toRegex(), "")
        val password = OpenWebNetUtils.passwordFromSeed(java.lang.Long.valueOf(seedString)!!, Integer.valueOf(OPENPassword)!!)
        return "*#" + password.toString() + "##"
    }

    fun newBticinoTCPSocket(address: String): TCPSocketHandler {
        return TCPSocketHandler(address, 20000)
    }

    fun startMonitoringBTCStatus(addresses: List<String>, listener: BTCStatusListener) {
        stopMonitoringBTCStatus()

        for (address in addresses) {
            val BTCStatusSocket = newBticinoTCPSocket(address)
            BTCStatusSocket.onConnectedListener = object : TCPSocketHandler.OnConnectedListener {
                override fun onConnected(socket: TCPSocketHandler) {
                    socket.read("##")
                }
            }
            BTCStatusSocket.onReceivedResponseListener = object : TCPSocketHandler.OnReceivedResponseListener {
                override fun onReceivedResponse(socket: TCPSocketHandler, response: String) {
                    if (socket.totalResponses == 1) {
                        socket.print("*99*1##")
                    } else if (socket.totalResponses > 1) {
                        if (isSeed(response)) {
                            val password = calculatePassword(response, AppySDK.instance().pin!!)
                            if (password != null) {
                                socket.print(password)
                            } else {
                                socket.terminate()
                                return
                            }
                        } else {
                            extractBTCDataFromStatus(response, object : ExtractBTCStatusListener {
                                override fun onExtracted(status: String, fetchMessage: String, value: String) {
                                    Log.i("Dev", "Incoming Status : $status, FetchMessage : $fetchMessage, Value : $value")
                                    listener.onReceiveMessage(status, fetchMessage, value)
                                }
                            })
                        }
                    }

                    socket.read("##")
                }
            }
            BTCStatusSocket.connectionTimeOut = 99999999
            BTCStatusSocket.start()

            BTCStatusSockets.add(BTCStatusSocket)
        }
    }

    fun stopMonitoringBTCStatus() {
        for (BTCStatusSocket in BTCStatusSockets) {
            BTCStatusSocket.terminate()
        }

        BTCStatusSockets.clear()
    }

    fun getBTCStatus(address: String, message: String, listener: BTCStatusListener) {
        val BTCStatusSocket = newBticinoTCPSocket(address)
        var isFetchMessageSent = false
        BTCStatusSocket.onConnectedListener = object : TCPSocketHandler.OnConnectedListener {
            override fun onConnected(socket: TCPSocketHandler) {
                socket.read("##")
            }
        }
        BTCStatusSocket.onReceivedResponseListener = object : TCPSocketHandler.OnReceivedResponseListener {
            override fun onReceivedResponse(socket: TCPSocketHandler, response: String) {
                if (socket.totalResponses == 1) {
                    socket.print("*99*1##")
                } else if (socket.totalResponses > 1) {
                    if (isSeed(response)) {
                        val password = calculatePassword(response, AppySDK.instance().pin!!)
                        if (password != null) {
                            socket.print(password)
                        } else {
                            socket.terminate(SocketHandler.AUTHENTICATION_FAILED)
                            return
                        }
                    } else if (!isFetchMessageSent) {
                        isFetchMessageSent = true
                        socket.print(message)
                    } else {
                        Log.i("Dev", "Response $response")
                        extractBTCDataFromStatus(response, object : ExtractBTCStatusListener {
                            override fun onExtracted(status: String, fetchMessage: String, value: String) {
                                if (fetchMessage.equals(message)) {
                                    Log.i("Dev", "Incoming Status : $status, FetchMessage : $fetchMessage, Value : $value")
                                    listener.onReceiveMessage(status, fetchMessage, value)
                                    socket.terminate()
                                }
                            }
                        })
                    }
                }

                socket.read("##")
            }
        }
        BTCStatusSocket.start()
    }

    fun sendBTCCommand(address: String, command: String, receiveBTCCommandResponseListener: ReceiveBTCCommandResponseListener?, sendBTCCommandListener: SendBTCCommandListener?) {
        if (!Validator.isValid(command) || !Validator.isValid(address)) {
            return
        }

        Log.i("SSA", "BTC >> Command : $command, Address : $address")

        val sh = newBticinoTCPSocket(address)
        sh.onConnectedListener = object : TCPSocketHandler.OnConnectedListener {
            override fun onConnected(socket: TCPSocketHandler) {
                Log.i("Dev", "Connected")
                socket.read("##")
            }
        }
        sh.onReceivedResponseListener = object : TCPSocketHandler.OnReceivedResponseListener {
            internal var commendSent = false
            internal var expectedACK = getTotalMatch(command, "##") + 2

            override fun onReceivedResponse(socket: TCPSocketHandler, response: String) {
                Log.i("Dev", "Receive Response : " + response)
                if (receiveBTCCommandResponseListener != null) {
                    Handler(Looper.getMainLooper()).post { receiveBTCCommandResponseListener.onReceived(socket, command, response) }
                }

                if (hasNACK(response)) {
                    socket.terminate(SocketHandler.CONNECTION_ERROR)
                } else if (socket.totalResponses >= expectedACK) {
                    socket.terminate()
                } else {
                    if (socket.totalResponses == 1) {
                        socket.print("*99*0##")
                    } else {
                        if (isSeed(response)) {
                            expectedACK += 1

                            val password = calculatePassword(response, AppySDK.instance().pin!!)

                            if (password != null) {
                                Log.i("Dev", "Submit Password : $password, Seed : $response")
                                socket.print(password)
                            } else {
                                socket.terminate(SocketHandler.AUTHENTICATION_FAILED)
                                return
                            }
                        } else if (!commendSent) {
                            Log.i("Dev", "Sending : " + command)
                            socket.print(command)
                            commendSent = true
                        }
                    }

                    socket.read("##")
                }
            }
        }
        sh.onTerminatedListener = object : TCPSocketHandler.OnTerminatedListener {
            override fun onTerminated(socket: TCPSocketHandler) {
                Log.i("Dev", "Terminated")
                if (sendBTCCommandListener != null) {
                    Handler(Looper.getMainLooper()).post {
                        if (!socket.hasError()) {
                            sendBTCCommandListener.onSubmitted(socket, !socket.hasError())
                        } else {
                            sendBTCCommandListener.onFailed(socket, socket.errorCode)
                        }
                    }
                }
            }
        }
        sh.start()
    }

    interface SendBTCCommandListener {
        fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean)

        fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int)
    }

    interface ReceiveBTCCommandResponseListener {
        fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String)
    }

    interface ExtractBTCStatusListener {
        fun onExtracted(status: String, fetchMessage: String, value: String)
    }

    companion object {

        val ACK = "*#*1##"
        val NACK = "*#*0##"
        private val THM_STATUS_PREFIX = "STATUS OUTPUTS:"
        var instance: AutomationManager? = null
            get() {
                if (field == null) {
                    field = AutomationManager()
                }
                return field
            }

        private fun isSeed(message: String): Boolean {
            return if (!Validator.isValid(message)) {
                false
            } else message.matches("\\*#[0-9]{5,10}##".toRegex())

        }

        private fun hasNACK(message: String): Boolean {
            return if (!Validator.isValid(message)) {
                false
            } else message.contains(NACK)

        }

        private fun isMessage(message: String): Boolean {
            return if (!Validator.isValid(message)) {
                false
            } else !message.matches("\\*#[0-9]{5,10}##".toRegex()) && !message.equals(ACK, ignoreCase = true) && !message.contains(NACK)

        }

        private fun extractFetchMessage(status: String): String {
            val components = TextUtils.split(status, "\\*")
            if (components.size <= 2) {
                return ""
            }

            val array = ArrayList(Arrays.asList(*components))
            array.removeAt(2)

            var tmp = array.removeAt(1)
            tmp = "#" + tmp
            array.add(1, tmp)

            return TextUtils.join("*", array)
        }

        private fun extractValue(status: String): String {
            val components = TextUtils.split(status, "\\*")

            return if (components.size > 2) {
                components[2]
            } else {
                ""
            }
        }

        private fun getTotalMatch(string: String, pattern: String): Int {
            var totalCommand = 0
            var lastIndex = 0

            while (lastIndex != -1) {
                lastIndex = string.indexOf(pattern, lastIndex)
                if (lastIndex != -1) {
                    totalCommand++
                    lastIndex += pattern.length
                }
            }

            return totalCommand
        }


        fun extractBTCDataFromStatus(line: String?, listener: ExtractBTCStatusListener?) {
            var line = line
            if (line == null || listener == null) {
                return
            }

            if (!isMessage(line)) {
                return
            }

            line = line.replace(ACK, "")
            line = line.replace(NACK, "")

            val array = TextUtils.split(line, "##")

            for (i in array.indices) {
                if (array[i] === "") {
                    continue
                }

                val status = array[i] + "##"
                val fetchMessage = extractFetchMessage(status)
                val value = extractValue(status)
                listener.onExtracted(status, fetchMessage, value)


            }
        }
    }
}

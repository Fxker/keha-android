package com.appy.android.sdk.status.dimmer

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.status.APStatus

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
class APStatusDimmer : APStatus<String> {
    constructor() : super()
    constructor(control: APControl, value: String) : super(control, value) {
        maxValue = if (control.isFibaroModel()) {
            "100"
        } else {
            "10"
        }
    }
}
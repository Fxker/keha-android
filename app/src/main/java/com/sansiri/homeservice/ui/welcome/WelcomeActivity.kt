package com.sansiri.homeservice.ui.welcome

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.Toast
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.OnVerticalScrollListener
import com.sansiri.homeservice.model.api.announcement.FamilyAnnouncement
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.util.*
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator

import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity<WelcomeContract.View, WelcomeContract.Presenter>(), WelcomeContract.View {

    override var mPresenter: WelcomeContract.Presenter = WelcomePresenter()

    val mAnnouncementAdapter = WelcomeAdapter() { view, announcement ->
        ContentActivity.start(this, view, getString(R.string.news_update), announcement)
    }

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, WelcomeActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        with(listNews) {
            setHasFixedSize(true)
            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            adapter = mAnnouncementAdapter
            itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
            itemAnimator?.addDuration = 700
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int) {
                    mPresenter.fetchData(page)
                }
            })

            addOnScrollListener(object : OnVerticalScrollListener() {
                override fun onScrolledUp() {
                    showLoginButton()
                }

                override fun onScrolledDown() {
                    hideLoginButton()
                }
            })
        }

        buttonLogin.setOnClickListener {
            RegisterActivity.start(this)
            finish()
        }

        mPresenter.fetchData(0)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("WELCOME")
    }


    override fun bindData(announcement: List<FamilyAnnouncement>) {
        mAnnouncementAdapter.addDataDelay(announcement)
        listNews.scrollToPosition(0)
    }

    override fun showLoading() {
        progressBar.show()

    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showLoginButton() {
        if (buttonPanel.visibility == View.GONE) {
            val slide = Slide()
            slide.slideEdge = Gravity.BOTTOM
            TransitionManager.beginDelayedTransition(container, slide)
            buttonPanel.visibility = View.VISIBLE
        }
    }

    fun hideLoginButton() {
        if (buttonPanel.visibility == View.VISIBLE) {
//            val slide = Slide()
//            slide.slideEdge = Gravity.BOTTOM
//            TransitionManager.beginDelayedTransition(container, slide)
            buttonPanel.visibility = View.GONE
        }
    }

}

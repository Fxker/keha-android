package com.sansiri.homeservice.ui.sell

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Sell
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.reportDate
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class SellHistoryAdapter() : androidx.recyclerview.widget.RecyclerView.Adapter<SellHistoryAdapter.ViewHolder>() {
    private val mData = mutableListOf<Sell>()


    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_summary_card))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }


    fun setData(data: List<Sell>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(data: Sell) {
            with(view) {
                imageIcon.setImageResource(R.drawable.ic_sale_rentout)
                textDate.text = data.address
                textTitle.text = data.title
                textSubtitle.text = data.status
                textDate.text = data.date.reportDate()
            }
        }
    }

}
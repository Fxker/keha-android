package com.sansiri.homeservice.ui.phonedirectory

import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/30/17.
 */
interface PhoneDirectoryContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
        fun start(projectId: String)
    }

    interface View : BaseView {
        fun bindData(phoneDirectory: List<PhoneDirectoryGroup>)
        fun showNoData()
        fun showLoading()
        fun hideLoading()
    }
}
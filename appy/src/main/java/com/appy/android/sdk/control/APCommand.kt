package com.appy.android.sdk.control

interface APCommand {

    fun action(): String
}
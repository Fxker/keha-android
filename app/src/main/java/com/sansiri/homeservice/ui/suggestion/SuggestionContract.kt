package com.sansiri.homeservice.ui.suggestion

import android.graphics.Bitmap
import android.net.Uri
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SuggestionContract {
    interface Presenter: BasePresenter<View> {
        fun init(unitId: String)
        fun submit()
        fun uploadImage(images: List<Pair<String, ByteArray>>)
        fun submitSuggestion(imagePath: List<String>)
    }

    interface View: BaseView {
        fun showUploadProgress(progress: Int, max: Int)
        fun getDetail(): String
        fun showSuccess()
        fun tryAgain()
        fun showAlertToEnterDetail()
        fun showSubmitProgress()
        fun prepareBitmapToUploadIfExist()
    }
}
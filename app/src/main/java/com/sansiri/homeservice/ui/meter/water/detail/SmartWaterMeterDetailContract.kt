package com.sansiri.homeservice.ui.meter.water.detail

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterHistoryList
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

interface SmartWaterMeterDetailContract {
    interface Presenter : BasePresenter<View> {
        fun init(type: String?, unitId: String?, initDate: Calendar?)
        fun fetch()
        fun requestDatePicker()
        fun fetchFromSelectedDate(calendar: Calendar)
    }

    interface View : BaseView {
        fun attachHistoryObserver(smartWaterMeterHistory: MutableLiveData<Resource<SmartWaterMeterHistoryList>>)
        fun bindData(history: SmartWaterMeterHistoryList)
        fun showLoading()
        fun hideLoading()
        fun showDatePicker(calendar: Calendar)
        fun showMonthPicker(calendar: Calendar)
        fun showSelectedDate(calendar: Calendar)
    }
}
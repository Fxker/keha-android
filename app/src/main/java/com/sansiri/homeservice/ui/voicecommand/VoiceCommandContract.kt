package com.sansiri.homeservice.ui.voicecommand

import android.graphics.Bitmap
import com.onionshack.onionspeech.*
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.Doc
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

/**
 * Created by sansiri on 11/3/17.
 */
interface VoiceCommandContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Home, isMock: Boolean)
        fun fetchData(userId: String)
        fun actionHomeAutomation(device: OnionDevice, param: OnionHomeControlParameter)
        fun actionCheckInvoice(callback: (Map<OnionHomeService.UtilityType, Double>, Date) -> Unit)
        fun uploadImage(image: Bitmap, onSuccess: (String) -> Unit, onError: (Throwable) -> Unit)
        fun requestToOpenFacility(id: String)
        fun bookFacility(id: String, startTime: Date, endTime: Date, onSuccess: () -> Unit, onError: (OnionHomeService.BookFacilityCallback.BookingResult, Map<OnionHomeService.BookFacilityCallback.BookingResultAdditionalInfo, Object>) -> Unit)
    }

    interface View : BaseView {
        fun launchVoiceCommandScreen(userId: String, rooms: List<OnionRoom>, facilities: List<OnionFacility>)
        fun launchDemoVoiceCommandScreen(userId: String)
    }
}
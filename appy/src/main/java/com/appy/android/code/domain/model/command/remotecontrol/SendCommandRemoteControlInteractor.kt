package com.appy.android.code.domain.model.command.remotecontrol

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

internal class SendCommandRemoteControlInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendCommandRemoteControlForm> (threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendCommandRemoteControlForm): Observable<CommandApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .sendCommandRemoteControl(params.headers, params.homeId, params.remoteControlID, params.mSendCommandRemoteControlJsonBody)
    }
}
package com.appy.android.code.base.models

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */

internal abstract class BaseRoomJsonModel {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
}
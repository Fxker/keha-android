package com.sansiri.homeservice.ui.homeupgrade.summary

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_home_upgrade_summary_item.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class HomeUpgradeSummaryViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: HomeUpgradeHardware) {
        with(view) {
            textTitle.text = data.name
            textAmount.text = "x${data.amount}"
            Glide.with(this).loadCache(data.imageUrl ?: "", imageLogo as ImageView)
        }
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGateWayQR
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface CreditCardContract {
    interface Presenter : BasePresenter<View> {
        fun setCardName(name: String)
        fun setCardNumber(number: String)
        fun setCCV(ccv: String)
        fun setExpiryMonth(month: Int)
        fun setExpiryYear(year: Int)
        fun validate()
        fun purchase(): MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>?
        fun init(unitId: String?, userId: String?, amount: Int)
        fun fetch()
        var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>?
    }

    interface View : BaseView {
        fun disableContinueButton()
        fun enableContinueButton()
        fun showCreditCardError()
        fun showSubmitting()
        fun showSubmitted()
        fun hideSubmitting()
    }
}
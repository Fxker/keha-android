package com.sansiri.homeservice.ui.facilitybooking.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.GeneralDialogFragment
import kotlinx.android.synthetic.main.dialog_cancel_booking.*
import kotlinx.android.synthetic.main.dialog_general.*

/**
 * Created by sansiri on 10/16/17.
 */
class CancelBookingConfirmDialogFragment(): GeneralDialogFragment() {
    @SuppressLint("ValidFragment")
    constructor(onSubmit: (() -> Unit)): this() {
        this.onSubmit = onSubmit
    }

    private var onSubmit: (() -> Unit)? = null
    override val onClickListener: () -> Unit = {
        onSubmit?.invoke()
        dismiss()
    }

    companion object {
        val TIME = "TIME"
        fun newInstance(onSubmit: () -> Unit): CancelBookingConfirmDialogFragment {
            val fragment = CancelBookingConfirmDialogFragment(onSubmit)
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_cancel_booking, null)
        panelContent.addView(view)

        textTitle.text = "CANCEL BOOKING"
        buttonSubmit.text = "CANCEL BOOKING"
        buttonSubmit.setTextColor(resources.getColor(R.color.colorCancel))

        buttonSubmit.isEnabled = false
        buttonSubmit.alpha = 0.8f

        checkbox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                buttonSubmit.isEnabled = true
                buttonSubmit.alpha = 1.0f
            } else {
                buttonSubmit.isEnabled = false
                buttonSubmit.alpha = 0.8f
            }
        }

    }
}
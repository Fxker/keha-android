package com.appy.android.code.base.domain.subscriber

import com.appy.android.code.base.data.rest.error.APError
import com.appy.android.code.base.data.rest.result.AppyAPIErrorResult
import com.appy.android.code.base.domain.callback.BaseApiCallback
import com.appy.android.code.base.domain.callback.BasicApiCallback
import com.appy.android.code.base.domain.callback.BasicApiCallbackWithResult
import com.appy.android.code.data.rest.error.APErrorFactory
import com.appy.android.code.data.rest.error.APUnknownErrorException
import com.minor.bemynt.helpers.GsonHelper
import retrofit2.HttpException


/**
 * Created by ssa-dev-4 on 27/9/2560.
 */

internal class BaseApiSubscriber<T>(internal var c: BaseApiCallback?) : DefaultSubscriber<T>() {

    override fun onComplete() {
        super.onComplete()
        if (c != null) {
            if (c is BasicApiCallback) {
                (c as BasicApiCallback).onSuccess()
            } else if (c is BasicApiCallbackWithResult<*>) {
                (c as BasicApiCallbackWithResult<T>).onSuccess(resultObj!!)
            }
        }
    }

    override fun onError(e: Throwable) {
        super.onError(e)
        if (c != null) {
            if (e is HttpException && !e.response().errorBody()?.string().isNullOrEmpty()) {
                try {
                    val appyAPIError = GsonHelper.build().fromJson(e.response().errorBody()?.string(), AppyAPIErrorResult::class.java)
                    if (appyAPIError == null) {
                        createUnknownException(e)
                        return
                    }
                    (c as BaseApiCallback).onFailed(APErrorFactory.create(appyAPIError))
                } catch (e: Exception) {
                    val apError = APUnknownErrorException()
                    apError.error = e.message
                    (c as BaseApiCallback).onFailed(apError)
                }
            } else if (e is APError) {
                (c as BaseApiCallback).onFailed(e)
            } else {
                val apError = APUnknownErrorException()
                apError.error = e.message
                (c as BaseApiCallback).onFailed(apError)
            }
        }
    }

    fun createUnknownException(e: Exception) {
        val apError = APUnknownErrorException()
        apError.error = e.message
        (c as BaseApiCallback).onFailed(apError)
    }
}
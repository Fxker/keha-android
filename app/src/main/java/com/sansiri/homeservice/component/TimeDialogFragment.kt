package com.sansiri.homeservice.component

import com.sansiri.homeservice.util.format
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.toDate
import kotlinx.android.synthetic.main.dialog_sandee_feedback.*
import java.util.*

/**
 * Created by sansiri on 10/16/17.
 */
abstract class TimeDialogFragment: GeneralDialogFragment() {
    protected fun getTextTime(time: Calendar?): String {
        return "${time?.get(Calendar.HOUR_OF_DAY)?.format(2)}:${time?.get(Calendar.MINUTE)?.format(2)}"
    }

    protected fun getTextDate(date: Calendar?): String {
        return "${date?.get(Calendar.YEAR)}-${date?.get(Calendar.MONTH)?.plus(1)}-${date?.get(Calendar.DAY_OF_MONTH)}".toDate().report()
    }
}
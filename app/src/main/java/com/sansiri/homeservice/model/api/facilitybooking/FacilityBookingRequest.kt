package com.sansiri.homeservice.model.api.facilitybooking

import com.google.gson.annotations.SerializedName

data class FacilityBookingRequest(@SerializedName("facility_id")
                                  val facilityId: String = "",
                                  @SerializedName("start_date")
                                  val startDate: String = "",
                                  @SerializedName("end_date")
                                  val endDate: String = "",
                                  @SerializedName("note")
                                  val note: String = "",
                                  @SerializedName("first_name")
                                  val firstName: String?,
                                  @SerializedName("last_name")
                                  val lastName: String?,
                                  @SerializedName("hsa_unit_name")
                                  val address: String?)
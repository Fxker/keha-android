package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.form.AppyForm

internal class GetDaikinsForm(
        internal val homeId: String
) : AppyForm()
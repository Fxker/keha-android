package com.sansiri.homeservice.ui.svvh.registration

import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.bottomsheet.DatePickerBottomSheet
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.svvh.SVVHActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_svvh_registration.*
import org.jetbrains.anko.collections.forEachWithIndex
import java.util.*

class SVVHRegistrationFragment : BaseFragment<SVVHRegistrationContract.View, SVVHRegistrationContract.Presenter>(), SVVHRegistrationContract.View {
    override var mPresenter: SVVHRegistrationContract.Presenter = SVVHRegistrationPresenter()
    private lateinit var mDatePicker: DatePickerBottomSheet
    private lateinit var genderItems: List<Pair<String, String>>
    private var selectedDate: Calendar = Calendar.getInstance()
        set(value) {
            textDate?.text = value.time.reportDMYFull()
            mPresenter.setBirthDate(value)
            field = value
        }

    companion object {
        const val HOME = "HOME"
        const val CUSTOMER = "CUSTOMER"
        fun newInstance(home: Hut, customer: SVVHRegisterCustomer?): SVVHRegistrationFragment {
            val fragment = SVVHRegistrationFragment()
            val bundle = Bundle().apply {
                putParcelable(HOME, home)
                putParcelable(CUSTOMER, customer)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_svvh_registration, container, false)
    }

    private var mHome: Hut? = null
    private var mCustomer: SVVHRegisterCustomer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHome = arguments?.getParcelable(HOME)
        mCustomer = arguments?.getParcelable<SVVHRegisterCustomer>(CUSTOMER)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SVVH_REGISTER")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textDate?.text = getString(R.string.please_select_your_birthdate)

        genderItems = listOf(
                Pair(SVVHRegisterCustomer.MALE, getString(R.string.male)),
                Pair(SVVHRegisterCustomer.FEMALE, getString(R.string.female))
        )

        textDate.setOnClickListener {
            mDatePicker.show(fragmentManager!!, "Date Picker")
        }

        editFirstname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setFirstName(it)
        })

        editLastname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setLastName(it)
        })

        editTel.addTextChangedListener(TextWatcherExtend {
            mPresenter.setTel(it)
        })

        buttonRegister.setOnClickListener {
            mPresenter.submit()
        }

        // gender radio

        val typeface = ResourcesCompat.getFont(context!!, R.font.graphik_th_regular)

        genderItems.forEachWithIndex { index, option ->
            val itemRadio = RadioButton(context!!)
            itemRadio.text = option.second
            itemRadio.id = index
            itemRadio.typeface = typeface
            itemRadio.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(com.sansiri.homeservice.R.dimen.text_body2))
            if (index == 0) {
                itemRadio.isChecked = true
            }
            val params = RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 0, resources.getDimension(R.dimen.activity_horizontal_margin).toInt(), 0)
            itemRadio.layoutParams = params
            spinnerOption.addView(itemRadio)
        }

        spinnerOption.setOnCheckedChangeListener { group, checkedId ->
            mPresenter.setGender(genderItems[checkedId].first)
        }

        mPresenter.setGender(genderItems[0].first)

        if (mHome != null) {
            mPresenter.init(mHome!!, mCustomer)
        }

        mPresenter.start()
    }

    override fun bindData(customer: SVVHRegisterCustomer) {
        textIDCard.text = customer.citizenOrPassportId

        editFirstname.setText(customer.firstname)
        editLastname.setText(customer.lastname)
        editTel.setText(customer.contactNumber)

        setUpDatePicker(
                if (customer._birthDate != null) {
                    selectedDate = Calendar.getInstance().apply {
                        time = customer._birthDate
                    }

                    selectedDate
                } else {
                    null
                }
        )
    }

    private fun setUpDatePicker(calendar: Calendar?) {
        val calenderInit = calendar ?: Calendar.getInstance().apply {
            set(Calendar.YEAR, 1995)
        }

        mDatePicker = DatePickerBottomSheet(calenderInit, false, null, Calendar.getInstance()) { calendar ->
            this.selectedDate = calendar
        }

    }

    override fun showFirstNameError() {
        editFirstname.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showLastNameError() {
        editLastname.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showTelError() {
        editTel.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showBirthDateError() {
        showError(getString(R.string.please_select_your_birthdate))
    }

    override fun showGenderError() {
        showError(getString(R.string.please_select_your_gender))
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    override fun showLoading() {
        buttonRegister.hide()
        progressBar.show()
    }

    override fun hideLoading() {
        buttonRegister.show()
        progressBar.hide()
    }

    override fun redirectUrl(url: String) {
        (activity as SVVHActivity).launchWebView(url)
    }
}
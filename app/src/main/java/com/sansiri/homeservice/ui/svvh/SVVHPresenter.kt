package com.sansiri.homeservice.ui.svvh

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import retrofit2.HttpException

class SVVHPresenter() : BasePresenterImpl<SVVHContract.View>(), SVVHContract.Presenter {

    private var mHome: Hut? = null

    override fun init(home: Hut) {
        mHome = home
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        if (mHome != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mView?.showLoading()
                    api.getSVVHCustomer(mHome?.unitId ?: "").observe().subscribe({ customer ->
                        mView?.hideLoading()
                        if (customer.redirectUrl != null) {
                            mView?.showPreview(mHome!!, customer)
                        } else {
                            mView?.showRegistration(mHome!!, customer)
                        }
                    }) { error ->
                        mView?.hideLoading()
                        mView?.showError(error.showHttpError())
                    }
                }
            }
        }
    }
    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
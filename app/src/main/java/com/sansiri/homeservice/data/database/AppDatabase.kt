package com.sansiri.homeservice.data.database

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.sansiri.homeservice.data.database.creditcard.SavingCreditCardDao
import com.sansiri.homeservice.data.database.notification.NotificationDao
import com.sansiri.homeservice.data.database.orvibo.OrviboDao
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard
import com.sansiri.homeservice.model.database.orvibo.OrviboHome

@Database(entities = [OrviboHome::class, SavingCreditCard::class, PushNotification::class, PushNotificationData::class], version = 4, exportSchema = true)
public abstract class AppDatabase : RoomDatabase() {
    abstract fun orviboDao(): OrviboDao
    abstract fun savingCreditCardDao(): SavingCreditCardDao
    abstract fun notificationDao(): NotificationDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        @VisibleForTesting
        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS 'saving_credit_card' ('cardToken' TEXT NOT NULL, 'securedCardNumber' TEXT, 'provider' TEXT, 'billingEmail' TEXT,PRIMARY KEY('cardToken'))")
            }
        }

        @VisibleForTesting
        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `notification` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `message` TEXT, `type` TEXT, `data_objectId` TEXT, `data_unitId` TEXT, `data_projectId` TEXT, `data_accessoryId` TEXT, `data_planId` TEXT)")
                database.execSQL("CREATE TABLE IF NOT EXISTS `notification_data` (`objectId` TEXT NOT NULL, `unitId` TEXT, `projectId` TEXT, `accessoryId` TEXT, `planId` TEXT, PRIMARY KEY(`objectId`))")
            }
        }

        @VisibleForTesting
        val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE `notification`")
                database.execSQL("DROP TABLE `notification_data`")
                database.execSQL("CREATE TABLE IF NOT EXISTS `notification` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `message` TEXT, `type` TEXT, `createdAt` TEXT, `isRead` INTEGER NOT NULL, `data_objectId` TEXT, `data_unitId` TEXT, `data_projectId` TEXT, `data_accessoryId` TEXT, `data_profileImage` TEXT, `data_planId` TEXT)")
                database.execSQL("CREATE TABLE IF NOT EXISTS `notification_data` (`objectId` TEXT NOT NULL, `unitId` TEXT, `projectId` TEXT, `accessoryId` TEXT, `profileImage` TEXT, `planId` TEXT, PRIMARY KEY(`objectId`))")

//                database.execSQL("ALTER TABLE `notification` ADD COLUMN `profileImage` TEXT")
//                database.execSQL("ALTER TABLE `notification_data` ADD COLUMN `isRead` INTEGER DEFAULT 0 NOT NULL")
//                database.execSQL("ALTER TABLE `notification_data` ADD COLUMN `data_accessoryId` TEXT")
//                database.execSQL("ALTER TABLE `notification_data` ADD COLUMN `data_profileImage` TEXT")
            }
        }

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "hsa.db"
                ).addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
package com.appy.android.code.domain.model.command.outlet

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandOutletJsonBody : BaseCommandJsonBody<String>() {

}
package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.scenario.ScenarioCommand
import java.text.MessageFormat

class SendFibaroScenarioCommandForm(light: APScenario, command: ScenarioCommand) : SendFibaroCommandForm(light) {

    init {
        action = "start"
    }

    override fun getURL(): String {
        return MessageFormat.format("http://{0}/api/scenes/{1}/action/{2}", this.getHost(), this.getControlId(), this.action)
    }
}
package com.sansiri.homeservice.ui.adapter

import android.content.res.Resources
import android.graphics.Bitmap
import android.net.Uri
import androidx.transition.TransitionManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.*

import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.view_card_photo.view.*
import kotlinx.android.synthetic.main.view_icon_and_title.view.*
import kotlinx.android.synthetic.main.view_menu_card.view.*
import org.jetbrains.anko.dimen

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class MultiSelectPhotoAdapter(val addButtonClick: () -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val TYPE_ADD_BUTTON = 0
    private val TYPE_REGULAR = 1
    private var mData = mutableListOf<Bitmap>()
    private var isShowSelectButton = true
    var maximumSelection: Int = 10

    override fun getItemCount(): Int {
        isShowSelectButton = mData.size < maximumSelection

        if (isShowSelectButton)
            return mData.size + 1
        return mData.size
    }

    override fun getItemViewType(position: Int): Int {
        if (isShowSelectButton && position == itemCount - 1) {
            return TYPE_ADD_BUTTON
        }
        return TYPE_REGULAR
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        if (viewType == TYPE_ADD_BUTTON) {
            return AddViewHolder(parent.inflate(R.layout.view_menu_card), addButtonClick)
        } else {
            return PhotoViewHolder(parent.inflate(R.layout.view_card_photo), { position ->
                mData.removeAt(position)
                notifyItemRemoved(position)

                // in case of item less than 1
                if (mData.isEmpty()) {
                    showSelectButton()
                }
            })
        }
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        //holder.bind(mData[position])

        if (holder is PhotoViewHolder) {
            resizeView(holder.view)
            holder.bind(mData[position])
        } else if (holder is AddViewHolder) {
            resizeView(holder.view)
        }
    }

    fun resizeView(view: View, widthRatio: Float = 1f, heightRatio: Float = 1f) {
        with(view) {
            val gridColumn = context.resources.getInteger(R.integer.grid_column)
            // set card to relating with screen programmatically
            val params = layoutParams
            val displayWidth = Resources.getSystem().displayMetrics.widthPixels - (context.resources.getDimension(R.dimen.activity_horizontal_margin) * 2).toInt()

            if (widthRatio > 0) {
                params.width = (displayWidth * widthRatio / gridColumn).toInt()
            } else {
                params.width = widthRatio.toInt() //fix shadow take out some space}
            }

            if (heightRatio > 0) {
                params.height = ((displayWidth * heightRatio / gridColumn) + 3.px).toInt()
            } else {
                params.height = heightRatio.toInt() //fix shadow take out some space}
            }

            layoutParams = params
        }
    }


    fun setData(data: MutableList<Bitmap>) {
        mData = data
        notifyDataSetChanged()
    }

    class PhotoViewHolder(val view: View, val onDelete:(Int) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var isShowDelete = false
        fun bind(bitmap: Bitmap) {
            with(view) {
                isShowDelete = false
                // Glide.with(context).load(path).into(image)
                image.setImageBitmap(bitmap)
                setOnClickListener {
                    isShowDelete = !isShowDelete
                    refresh()
                }
                buttonDelete.setOnClickListener {
                    onDelete(layoutPosition)
                }
                refresh()
            }
        }

        private fun refresh() {
            TransitionManager.beginDelayedTransition(view.root)
            if (isShowDelete) {
                view.panelDelete.show()
            } else {
                view.panelDelete.hide()
            }
        }
    }

    class AddViewHolder(val view: View, val itemClick: () -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        init {
            with(view) {
                imageLogo.setImageResource(R.drawable.ic_camera)
                textTitle.text = context.getString(R.string.add_photo)
                layoutContent.setOnClickListener {
                    itemClick()
                }

                imageLogo.tintImage(ContextCompat.getColor(context, R.color.colorSecondary))
            }
        }
    }

//    fun setData(images: List<Uri>) {
//        mData.clear()
//        mData.addAll(images)
//        notifyDataSetChanged()
//    }

    fun hideSelectButton() {
        isShowSelectButton = false
        notifyDataSetChanged()
    }

    fun showSelectButton() {
        isShowSelectButton = true
        notifyDataSetChanged()
    }
}
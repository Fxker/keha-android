package com.sansiri.homeservice.ui.myaccount_v2.invoice

import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MyAccountInvoiceDetailContract {
    interface Presenter : BasePresenter<View> {
        fun init(invoice: Invoice)
    }

    interface View : BaseView {
        fun bindData(invoice: Invoice)
    }
}
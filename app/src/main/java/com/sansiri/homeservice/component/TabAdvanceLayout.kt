package com.sansiri.homeservice.component

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import com.sansiri.homeservice.R
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.collections.forEachWithIndex

/**
 * Created by sansiri on 10/10/17.
 */
class TabAdvanceLayout : LinearLayout {
    val mTabs = mutableListOf<View>()
    val mRelatedViews = mutableListOf<Triple<Int, Int, Int>>()
    private var mOnSelectedListener: ((index: Int) -> Unit)? = null

    constructor(context: Context) : super(context) {
    }


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)


    private fun initialize(context: Context, attrs: AttributeSet) {
//        val a = context.obtainStyledAttributes(attrs, R.styleable.ToggleLayout, 0, 0)
//        mColorOn = a.getColor(R.styleable.ToggleLayout_onColor, Color.BLUE)
//        mColorOff = a.getColor(R.styleable.ToggleLayout_offColor, Color.GRAY)
//
//        a.recycle()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        mTabs.forEachWithIndex { position, _ ->
            getChildAt(position).setOnClickListener {
                selectItem(position)
            }
        }
    }

    fun addTab(vararg views: View) {
        mTabs.addAll(views)
    }

    fun addViewIdAndColorStatus(viewId: Int, colorActive: Int, colorInactive: Int) {
        mRelatedViews.add(Triple(viewId, colorActive, colorInactive))
    }

    fun selectItem(position: Int) {
        mTabs.forEachWithIndex { index, _ ->
            if (index == position) {
                renderOn(getChildAt(index))
                mOnSelectedListener?.invoke(index)
            } else {
                renderOff(getChildAt(index))
            }
        }
    }

    private fun renderOn(tab: View) {
        mRelatedViews.forEach { relatedView ->
            val colorOn = relatedView.second
            val view = tab.findViewById<View>(relatedView.first)
            when (view) {
                is ImageView -> view.setColorFilter(colorOn)
                is TextView -> view.setTextColor(colorOn)
                is CardView -> view.backgroundColor = colorOn
                is ViewGroup -> view.backgroundColor = colorOn
            }
        }
    }

    private fun renderOff(tab: View) {
        mRelatedViews.forEach { relatedView ->
            val colorOff = relatedView.third
            val view = tab.findViewById<View>(relatedView.first)
            when (view) {
                is ImageView -> view.setColorFilter(colorOff)
                is TextView -> view.setTextColor(colorOff)
                is CardView -> view.backgroundColor = colorOff
                is ViewGroup -> view.backgroundColor = colorOff
            }
        }
    }


    fun setOnItemSelectedListener(listener: (index: Int) -> Unit) {
        mOnSelectedListener = listener
    }

}
package com.sansiri.homeservice.model.api

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class MailBox(
        var objectId: String? = null,
        var staffNote: String? = null,
        var images: List<String?>? = null,
        var size: String? = null,
        var sizeDisplayText: String? = null,
        var type: String? = null,
        var typeDisplayText: String? = null,
        var color: String? = null,
        var colorDisplayText: String? = null,
        var recipient: String? = null,
        var unitId: String? = null,
        var details: String? = null,
        var to: String? = null,
        var categoryDisplayText: String? = null,
        var category: String? = null,
        var projectId: String? = null,
        var isSupportSandee: Boolean,
        var status: String? = null,
        var createdAt: String? = null,
        var updatedAt: String? = null,
        var deliveredAt: String? = null
) : Parcelable {
    val _createdAt: Date?
        get() {
            try {
                return DateTime(this.createdAt).toDate()
            } catch (e: ParseException) {
                return null
            }
        }

    val _deliveredAt: Date?
        get() {
            try {
                return DateTime(this.deliveredAt).toDate()
            } catch (e: ParseException) {
                return null
            }
        }


}

package com.sansiri.homeservice.model.api.partner.trendywash

data class TrendyWashRegisterCustomer(
        val id: String? = null,
        val name: String? = null,
        val email: String? = null,
        val mobileno: String? = null,
        val password: String? = null
)
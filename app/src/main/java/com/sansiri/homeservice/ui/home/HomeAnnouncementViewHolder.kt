package com.sansiri.homeservice.ui.home

import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.model.api.announcement.Announcement

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_announcement_home.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class HomeAnnouncementViewHolder(val overlayColor: Int, val backgroundColor: Int, val view: View, val itemClick: (Announcement<*>) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(announcement: Announcement<*>) {
        with(view) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                card.preventCornerOverlap = false
            }
            val detail = announcement.details?.getOrNull(0)
            textTitle.text = detail?.title
            textSubtitle.text = detail?.subTitle
            textDate.text = announcement.displayDate?.report()
            imageThumbnail.hide()
//            Glide.with(context).loadCache(detail?.coverImageUrl ?: "", imageThumbnail)
            layoutRoot.setBackgroundColor(backgroundColor)
            textTitle.setTextColor(overlayColor)
            textSubtitle.setTextColor(overlayColor)
            textDate.setTextColor(overlayColor)
            line.setBackgroundColor(overlayColor)

            panelClickable.setOnClickListener {
                itemClick(announcement)
            }
        }
    }
}
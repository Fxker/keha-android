package com.sansiri.homeservice.ui.projectprogress

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.ProjectProgress
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.getHttpCode
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 11/6/17.
 */
class ProjectProgressPresenter : BasePresenterImpl<ProjectProgressContract.View>(), ProjectProgressContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData(projectId: String) {
        mView?.showLoading()

        val api = ApiRepositoryProvider.provideApiUnAuthRepository()
        mCompositeDisposable.add(api.getProjectProgress(projectId).observe().subscribe({
            mView?.bindData(it)
            mView?.hideLoading()
        }){
            if (it.getHttpCode() == 404) {
                mView?.showComingSoon()
            } else {
                mView?.showError(it.showHttpError())
            }
            mView?.hideLoading()
            mView?.hideData()
        })
    }
}
package com.sansiri.homeservice.ui.profile

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity<ProfileContract.View, ProfileContract.Presenter>(), ProfileContract.View {
    override var mPresenter: ProfileContract.Presenter = ProfilePresenter()

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, ProfileActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setBackToolbar(getString(R.string.my_profile))

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_PROFILE")
    }

    override fun bindData(profile: Profile) {
        "${profile.firstname} ${profile.lastname}".showTextOrHideIfNull(textName, titleName)
        profile.gender.showTextOrHideIfNull(textGender, titleGender)
        profile.country.showTextOrHideIfNull(textNationality, titleNationality)
        profile.phoneNumber.showTextOrHideIfNull(textPhoneNumber, titlePhoneNumber)
        profile.email.showTextOrHideIfNull(textEmail, titleEmail)
        profile.contactEmail.showTextOrHideIfNull(textEmailContact, titleEmailContact)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showError(message: String) {
        alertError(message)
    }


}

package com.sansiri.homeservice.data


object FeatureConfig: IFeatureConfig {
    override val ANNOUNCEMENT_ALL = true
    override val ANNOUNCEMENT_PROJECT = true
    override val SANSIRI_FAMILY = false
    override val VOICE_COMMAND = false
    override val GOOGLE_ASSISTANT = false
    override val LOUNGE: Boolean = false
    override val IS_SUPPORT_CALLCENTER = false
}
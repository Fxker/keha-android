package com.sansiri.homeservice.ui.base

/**
 * Created by oakraw on 6/12/2017 AD.
 */
interface BasePresenter<in V : BaseView> {
    fun start()
    fun destroy()
    fun attachView(view: V)
    fun detachView()
}
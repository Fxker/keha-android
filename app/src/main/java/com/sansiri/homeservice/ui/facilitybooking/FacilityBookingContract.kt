package com.sansiri.homeservice.ui.facilitybooking

import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/16/17.
 */
interface FacilityBookingContract {
    interface Presenter : BasePresenter<View> {
        fun init(projectId: String, unitId: String, address: String)
        fun fetchData()
        fun requestToLaunchCalendar(title: String, facilityId: String)
    }

    interface View : BaseView {
        fun bindFacility(menu: List<Menu>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
        fun launchCalendar(projectId: String, unitId: String, address: String, facility: FacilityBooking)
    }
}
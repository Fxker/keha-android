package com.sansiri.homeservice.ui.web

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.activity_web2.*
import android.os.Build.VERSION_CODES.LOLLIPOP
import android.os.Build.VERSION.SDK_INT
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.view.*
import android.webkit.*
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_web2.webview
import kotlinx.android.synthetic.main.view_web_fullscreen.view.*


class WebPopUpCompatActivity : AppCompatActivity() {
    private var mUrlPrefixForClosingDialog: String? = null
    private var mContext: Context? = null
    private var mWebviewPop: WebView? = null
    private var mPopUpBuilder: AlertDialog? = null
    private val mLastBackPressTime: Long = 0
    private val mToast: Toast? = null

    companion object {
        val URL = "URL"
        val URL_PREFIX_FOR_CLOSING_DIALOG = "URL_PREFIX_FOR_CLOSING_DIALOG"

        fun start(activity: Activity, url: String, urlPrefixForClosingDialog: String? = null) {
            val intent = Intent(activity, WebPopUpCompatActivity::class.java)
            intent.putExtra(URL, url)
            intent.putExtra(URL_PREFIX_FOR_CLOSING_DIALOG, urlPrefixForClosingDialog)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_web2)

        val url = intent.getStringExtra(URL)
        mUrlPrefixForClosingDialog = intent.getStringExtra(URL_PREFIX_FOR_CLOSING_DIALOG)

        setBackToolbar("")

        webview.settings.apply {
            javaScriptEnabled = true
            setAppCacheEnabled(true)
            javaScriptCanOpenWindowsAutomatically = true
            setSupportMultipleWindows(true)
            savePassword = true
            saveFormData = true
        }
        webview.webViewClient = UriWebViewClient()
        webview.webChromeClient = UriChromeClient(this)

        webview.loadUrl(url)

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptCookie(true)
        if (SDK_INT >= LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(webview, true)
        }


    }

    private inner class UriWebViewClient : WebViewClient() {
        override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
            return super.shouldInterceptRequest(view, request)
        }
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            val host = Uri.parse(url).host
            //Log.d("shouldOverrideUrlLoading", url);
//            if (host == "m.facebook.com") {
//                return false
//            }
            mUrlPrefixForClosingDialog?.let {
                if (url.contains(mUrlPrefixForClosingDialog ?: "")) {
                    try {
                        mWebviewPop?.destroy()
                        mPopUpBuilder?.dismiss()
                    } catch (e: Exception) {

                    }
                }
            }

            if (url.startsWith("http:") || url.startsWith("https:")) {
                return super.shouldOverrideUrlLoading(view, url)
            } else {
                try {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                    finish()
                } catch (e: Exception) {
                }
                return true
            }

        }
    }

    inner class UriChromeClient(val context: Context) : WebChromeClient() {

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            progressBar?.progress = newProgress;
            if (newProgress == 100) {
                progressBar?.hide();
            } else {
                progressBar?.show();
            }
        }

        @SuppressLint("SetJavaScriptEnabled")
        override fun onCreateWindow(view: WebView?, isDialog: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {
            val root = findViewById<ViewGroup>(android.R.id.content)

            val dialogView = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_web_fullscreen, root, false)
            mWebviewPop = dialogView.webView
            mWebviewPop?.isVerticalScrollBarEnabled = false
            mWebviewPop?.isHorizontalScrollBarEnabled = false
            mWebviewPop?.webViewClient = UriWebViewClient()
            mWebviewPop?.webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)

                    if (newProgress == 100) {
                        dialogView.progressBar?.hide()
                    } else {
                        dialogView.progressBar?.show()
                    }
                }
            }
            mWebviewPop?.settings?.apply {
                javaScriptEnabled = true
                savePassword = true
                saveFormData = true
            }

            val builder = AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
            builder.setView(dialogView)
            mPopUpBuilder = builder.create()
            mPopUpBuilder?.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.close)) { dialog, which ->
                mWebviewPop?.destroy()
                dialog.dismiss()
            }

            mPopUpBuilder?.show()
            mPopUpBuilder?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)

            val cookieManager = CookieManager.getInstance()
            cookieManager.setAcceptCookie(true)
            if (SDK_INT >= LOLLIPOP) {
                cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true)
            }

            val transport = resultMsg?.obj as WebView.WebViewTransport
            transport.webView = mWebviewPop
            resultMsg.sendToTarget()


            return true
        }

        override fun onCloseWindow(window: WebView?) {
            try {
                mWebviewPop?.destroy()
                mPopUpBuilder?.dismiss()
            } catch (e: Exception) {

            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_close) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_close, menu)
        return true
    }

    override fun onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack()
            return
        }
        super.onBackPressed()
    }
}
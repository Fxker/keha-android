package com.sansiri.homeservice.model.api.partner.trendywash

data class TrendyWashPaymentGateWayQR(
	val paymentQrCode: String? = null
)

package com.sansiri.homeservice.ui.parking.smartparking

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_parking_status.view.*
import org.joda.time.IllegalInstantException

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ElevatorAdapter() : RecyclerView.Adapter<ElevatorAdapter.ViewHolder>() {
    private val mData = mutableListOf<SmartParkingStatus.Elevator>()
    override fun getItemCount(): Int = mData.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_parking_status))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    fun setData(data: List<SmartParkingStatus.Elevator>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(elevator: SmartParkingStatus.Elevator) = with(view) {
            textElevatorNo.text = elevator.unit
            textCardNo.text = elevator.cardId
            textStatus.text = elevator.statusDisplay
            textDescription.text = elevator.messageDisplay

            try {
                elevator.bgColor?.let { card.setCardBackgroundColor(Color.parseColor(it)) }
                elevator.textColor?.let {
                    val color = Color.parseColor(it)
                    textElevatorNo.setTextColor(color)
                    titleCardNo.setTextColor(color)
                    textCardNo.setTextColor(color)
                    textStatus.setTextColor(color)
                    textDescription.setTextColor(color)
                }
            } catch (e: IllegalArgumentException) {
            }
        }
    }
}
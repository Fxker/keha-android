package com.sansiri.homeservice.ui.lounge

import android.graphics.Bitmap
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/30/17.
 */
interface QRContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
    }

    interface View : BaseView {
        fun bindQR(bitmap: Bitmap)
        fun showLoading()
        fun hideLoading()
    }
}
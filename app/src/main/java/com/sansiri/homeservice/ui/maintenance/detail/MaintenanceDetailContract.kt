package com.sansiri.homeservice.ui.maintenance.detail

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MaintenanceDetailContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut, accessoryId: String, planId: String?, plan: MaintenancePlan?)
        fun fetch()
        fun submit()
    }

    interface View : BaseView {
        fun showWeb(html: String)
        fun bindData(plan: MaintenancePlan?)
        fun showSubmitSuccess(accessoryId: String)
        fun showLoading()
    }
}
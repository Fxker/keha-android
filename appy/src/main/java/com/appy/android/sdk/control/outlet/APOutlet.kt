package com.appy.android.sdk.control.outlet

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APOutlet : APControl, Parcelable {
    var mActions: List<APActionOutlet>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionOutlet)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionOutlet()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                OutletCommand.OFF.action() -> {
                    action.action = OutletCommand.OFF
                }
                OutletCommand.ON.action() -> {
                    action.action = OutletCommand.ON
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APOutlet> {
        const val TYPE = "APOutlet"

        override fun createFromParcel(parcel: Parcel): APOutlet {
            val type = parcel.readString()
            return APOutlet(parcel)
        }

        override fun newArray(size: Int): Array<APOutlet?> {
            return arrayOfNulls(size)
        }
    }
}
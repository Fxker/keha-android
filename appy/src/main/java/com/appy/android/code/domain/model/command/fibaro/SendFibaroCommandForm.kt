package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.APControl
import com.google.gson.annotations.SerializedName
import java.text.MessageFormat
import java.util.*

open class SendFibaroCommandForm(control: APControl) : FibaroBaseForm(control) {

    var action = ""
        protected set
    var body: Any? = FormBody()
        protected set

    class FormBody : Any() {

        @SerializedName("args")
        var args = ArrayList<Any>()
    }

    open fun getURL(): String {
       return MessageFormat.format("http://{0}/api/devices/{1}/action/{2}", this.getHost(), this.getControlId(), this.action)
    }
}
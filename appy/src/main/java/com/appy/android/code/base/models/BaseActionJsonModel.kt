package com.appy.android.code.base.models

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
internal abstract class BaseActionJsonModel<T> {
    @SerializedName("name")
    var name: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("action")
    var action: T? = null
}
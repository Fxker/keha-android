package com.sansiri.homeservice.data.network.unauth

import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.model.api.CitizenIDVerifyResult
import com.sansiri.homeservice.model.api.ObjectId
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.model.api.RegisterNotificationRequest
import com.sansiri.homeservice.model.api.announcement.FamilyAnnouncement
import com.sansiri.homeservice.model.menu.HomeCareMenu
import com.sansiri.homeservice.util.observe
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody

/**
 * Created by sansiri on 10/24/17.
 */
class ApiUnAuthRepository(val apiService: ApiUnAuthService, val language: String) {
    fun verifyCitizenId(citizenId: String) = apiService.verifyCitizenID(citizenId)

    fun getPhoneDirectory(projectId: String) = apiService.getPhoneDirectory(language, projectId).map { return@map it.items }

    fun getFamilyAnnouncement(offSet: Int, limit: Int) = apiService.getFamilyAnnouncement(language, offSet, limit).map { return@map it.items }

    fun getProjectProgress(projectId: String) = apiService.getProjectProgress(language, projectId)

    fun getHomeCareType(onSuccess: (List<HomeCareMenu>) -> Unit, onFail: (Throwable) -> Unit): Disposable? {
        if (RuntimeCache.homeCareType.isNotEmpty()) {
            onSuccess(RuntimeCache.homeCareType)
            return null
        }

        return apiService.getHomeCareType(language).observe().subscribe({
            RuntimeCache.homeCareType.addAll(it)
            onSuccess(it)
        }) {
            onFail(it)
        }
    }

    fun registerNotification(deviceId: String, registerNotificationRequest: RegisterNotificationRequest) = apiService.registerNotification(deviceId, registerNotificationRequest)

    fun downloadFileWithDynamicUrl(fileUrl: String) = apiService.downloadFileWithDynamicUrl(fileUrl)

    fun getAnnouncementById(objectId: String) = apiService.getAnnouncementById(language, objectId)

    fun getAQI(projectId: String) = apiService.getAQI(language, projectId)

    fun uploadFile(url: String, data: ByteArray): Observable<ResponseBody> {
        val mediaType = MediaType.parse("application/octet-stream")
        val requestBody = RequestBody.create(mediaType, data)
        return apiService.uploadFile(url, requestBody)
    }
}
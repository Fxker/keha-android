package com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.displayTime
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_trendy_wash_machine.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textColor
import org.koin.androidx.scope.currentScope
import java.util.concurrent.TimeUnit

class TrendyWashMachineFragment : BaseV2Fragment<TrendyWashMachineContract.View, TrendyWashMachineContract.Presenter>(), TrendyWashMachineContract.View {
    override val mPresenter: TrendyWashMachineContract.Presenter  by currentScope.inject()

    companion object {
        fun newInstance(): TrendyWashMachineFragment {
            return TrendyWashMachineFragment()
        }

        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"
        const val MACHINE = "MACHINE"

        fun newInstance(unitId: String, userId: String, machine: TrendyWashMachine): TrendyWashMachineFragment {
            return TrendyWashMachineFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(USER_ID, userId)
                    putParcelable(MACHINE, machine)
                }
            }
        }
    }

    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mMachine: TrendyWashMachine? = null

    private var MAX_SELECTION_SECOND: Int = TimeUnit.MINUTES.toSeconds(120).toInt()
    private var MIN_SELECTION_SECOND: Int = TimeUnit.MINUTES.toSeconds(10).toInt()
    private var secondSelection: Int = 0
        set(value) {
            field = if (value > MAX_SELECTION_SECOND)
                MAX_SELECTION_SECOND
            else if (value < MIN_SELECTION_SECOND)
                MIN_SELECTION_SECOND
            else
                value

            mPresenter.calculatePriceFromTime(field)
            showSelectionTime(field)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mUserId = arguments?.getString(USER_ID)
        mMachine = arguments?.getParcelable(MACHINE)
    }

    override fun onResume() {
        super.onResume()
        when (mMachine?.typeId) {
            TrendyWashMachine.TYPE_DRYER -> sendScreenView("TRENDY_WASH_DRYER")
            TrendyWashMachine.TYPE_MACHINE -> sendScreenView("TRENDY_WASH_WASHING_MACHINE")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trendy_wash_machine, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textToolbar?.text = "Trendy Wash"
        buttonBack?.setOnClickListener {
            activity?.onBackPressed()
        }

        buttonConfirm?.setOnClickListener {
            mPresenter.startMachine()?.observe(this, Observer { resource ->
                when (resource?.status) {
                    Resource.LOADING -> {
                        showMachineStarting()
                    }
                    Resource.SUCCESS -> {
                        showMachineBusy()
                        mPresenter.startCountDown(resource.data?.actionTime ?: 0)
                    }
                    Resource.ERROR -> {
                        showMachineAvailable()
                        showError(resource.message ?: "Error")
                    }
                }
            })
        }

        if (mUnitId != null && mUserId != null && mMachine != null) {
            mPresenter.init(mUnitId!!, mUserId!!, mMachine!!)
            mPresenter.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(machine: TrendyWashMachine) {
        imageIcon.imageResource = machine.localIcon
        textName?.text = machine.machineName

        textPrice?.text = String.format(getString(R.string.price_x_baht), machine.price)
        textTime?.text = "${getString(R.string.wash_duration)} ${TimeUnit.SECONDS.toMinutes(machine.defaultTime.toLong()).toInt()} ${getString(R.string.min)}"

        if (machine.typeId == TrendyWashMachine.TYPE_DRYER) {
            layoutFixSettings.hide()
            layoutDynamicSettings.show()
            secondSelection = machine.defaultTime
            buttonIncrease.setOnClickListener {
                secondSelection += TimeUnit.MINUTES.toSeconds(10).toInt()
            }
            buttonDecrease.setOnClickListener {
                secondSelection -= TimeUnit.MINUTES.toSeconds(10).toInt()
            }
        } else {
            layoutFixSettings.show()
            layoutDynamicSettings.hide()
        }

        if (machine.status == TrendyWashMachine.STATUS_AVAILABLE) {
            showMachineAvailable()
        } else {
            mPresenter.startCountDown(machine.remainTime)
            layoutDynamicSettings.hide()
            showMachineBusy()
        }
    }


    override fun showMachineStarting() {
        progressBarStarting?.show()
        textNotice?.hide()
        buttonConfirm?.hide()
    }

    override fun showMachineBusy() {
        textStatus?.text = getString(R.string.busy)
        textStatus?.textColor = resources.getColor(R.color.textHint)
        textTimeCountDown?.show()
        textNotice?.hide()
        buttonConfirm?.hide()
        layoutDynamicSettings.hide()
        progressBarStarting?.hide()
    }

    override fun showMachineAvailable() {
        textStatus?.text = getString(R.string.available)
        textStatus?.textColor = resources.getColor(R.color.textAccent)
        textTimeCountDown?.hide()
        textNotice?.show()
        buttonConfirm?.show()
        progressBarStarting?.hide()
    }

    override fun showCalculatedPrice(price: Int) {
        textCalculatedPrice.text = String.format(getString(R.string.price_x_baht), price)
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    override fun updateCountDownTime(totalSecs: Int) {
        if (textTimeCountDown != null)
            textTimeCountDown.text = displayTime(totalSecs)
    }

    override fun setNotificationAlarm(seconds: Int, machine: TrendyWashMachine?) {
        val title = "Trendy Wash"
        val content = String.format(getString(R.string.x_has_finished), machine?.machineName
                ?: "เครื่องที่คุณเลือกไว้")
        val color = context!!.resources.getColor(R.color.colorAccent)
        val type = Feature.PARTNER_TRENDY_WASH

        NotificationHelper(context!!, 23).create(title, content, color, type).schedule(TimeUnit.SECONDS.toMillis(seconds.toLong()))
    }

    private fun showSelectionTime(second: Int) {
        textTimeSelection.text = "${TimeUnit.SECONDS.toMinutes(second.toLong()).toInt()} ${getString(R.string.min)}"
    }
}

package com.appy.android.sdk.control

import com.appy.android.code.base.data.rest.result.AppyApiApiStatusResult
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class ControlsApiResult {
    var status: AppyApiStatusResult? = null
    var apiStatus: AppyApiApiStatusResult? = null
    var datas: ArrayList<APControl> = ArrayList()
}
package com.sansiri.homeservice.ui.meter.water.detail

import android.app.Activity
//import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.github.mikephil.charting.data.Entry
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.bottomsheet.DatePickerBottomSheet
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterDetail
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterHistoryList
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.meter.LineChartHelper
import com.sansiri.homeservice.util.*
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_smart_water_meter_detail.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.koin.androidx.scope.currentScope
import java.util.*

class SmartWaterMeterDetailActivity : BaseV2Activity<SmartWaterMeterDetailContract.View, SmartWaterMeterDetailContract.Presenter>(), SmartWaterMeterDetailContract.View {
    private var mType: SmartWaterMeterOverAll.Summarize? = null
    private var mScreenView: String? = null
    private var progressDailog: ProgressDialog? = null
    override val mPresenter: SmartWaterMeterDetailContract.Presenter by currentScope.inject()
    private var mDateType: String? = null

    companion object {
        const val TYPE = "TYPE"
        const val UNIT_ID = "UNIT_ID"

        fun start(activity: Activity, type: SmartWaterMeterOverAll.Summarize?, unitId: String) {
            val intent = Intent(activity, SmartWaterMeterDetailActivity::class.java)
            intent.putExtra(TYPE, type)
            intent.putExtra(UNIT_ID, unitId)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smart_water_meter_detail)

        mType = intent.getParcelableExtra<SmartWaterMeterOverAll.Summarize>(TYPE)
        val unitId = intent.getStringExtra(UNIT_ID)

        mDateType = mType?.dateType

        chart.apply {
            setNoDataTextColor(resources.getColor(R.color.colorHighlight))
            setNoDataText(resources.getString(R.string.loading))
            setNoDataTextTypeface(Typeface.create(ResourcesCompat.getFont(this@SmartWaterMeterDetailActivity, R.font.graphik_th_regular), Typeface.NORMAL))
        }

        mPresenter.init(mDateType, unitId, mType?._timestamp?.toCalendar())
        mPresenter.start()

        buttonBack.setOnClickListener {
            finish()
        }

        when (mDateType) {
            SmartWaterMeterOverAll.Summarize.WEEK -> {
                buttonCalendar.hide()
            }
            SmartWaterMeterOverAll.Summarize.MONTH -> {
                buttonCalendar.setOnClickListener {
                    mPresenter.requestDatePicker()
                }
            }
            else -> {
                buttonCalendar.setOnClickListener {
                    mPresenter.requestDatePicker()
                }
            }
        }

        textUnit.text = "(${getString(R.string.litre)})"
        textTitle.text = "${mType?.title ?: ""}\n${getString(R.string.total)}"
    }

    override fun onResume() {
        super.onResume()
        mScreenView = when (mDateType) {
            SmartWaterMeterOverAll.Summarize.WEEK -> {
                "SMART_WATER_METER_DETAIL_WEEK"
            }
            SmartWaterMeterOverAll.Summarize.MONTH -> {
                "SMART_WATER_METER_DETAIL_MONTH"
            }
            else -> {
                "SMART_WATER_METER_DETAIL_DATE"
            }
        }
        mScreenView?.let { sendScreenView(it) }
    }


    override fun attachHistoryObserver(smartWaterMeterHistory: MutableLiveData<Resource<SmartWaterMeterHistoryList>>) {
        smartWaterMeterHistory.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    showLoading()
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let {
                        bindData(it)
                    }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })
    }

    override fun bindData(history: SmartWaterMeterHistoryList) {
        textAverageAmount.text = history.displayLiter
        textCost.text = history.costEstimateDisplay
        renderChart(history.items)
    }

    private fun renderChart(graphData: List<SmartWaterMeterDetail>?) {
        if (graphData.isNullOrEmpty()) {
            chart.hide()
            return
        } else {
            chart.show()
        }

        val entries = graphData.map { data ->
            val x = when (mDateType) {
                SmartWaterMeterOverAll.Summarize.DATE -> data._timestamp?.hours?.toFloat()
                SmartWaterMeterOverAll.Summarize.WEEK -> data._timestamp?.date?.toFloat()
                SmartWaterMeterOverAll.Summarize.MONTH -> data._timestamp?.date?.toFloat()
                else -> 0f
            }
            Entry(x ?: 0f, data.volumeUsage.toFloat())
        }

        LineChartHelper.LineChartBuilder(this, chart, R.layout.view_smart_water_graph_value_preview)
                .createDataSet(
                        entries,
                        "",
                        resources.getColor(R.color.colorBlueLineChart),
                        resources.getDrawable(R.drawable.graph_blue_fade),
                        resources.getColor(R.color.colorHighlight)
                )
                .build()
    }

    override fun showDatePicker(calendar: Calendar) {
        val dialog = DatePickerDialog.newInstance({ _, year, monthOfYear, dayOfMonth ->
            mPresenter.fetchFromSelectedDate(
                    Calendar.getInstance().apply {
                        set(Calendar.YEAR, year)
                        set(Calendar.MONTH, monthOfYear)
                        set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    }
            )
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dialog.maxDate = Calendar.getInstance().apply { add(Calendar.DATE, -1) }
        dialog.show(supportFragmentManager, "DatePickerDialog")
    }

    override fun showMonthPicker(calendar: Calendar) {
        mScreenView?.let { sendEvent(it, "CLICK", "DATE_PICKER") }
        val datePickerBottomSheet = DatePickerBottomSheet(
                calendar,
                true,
                null,
                Calendar.getInstance()) { calendar ->
            mPresenter.fetchFromSelectedDate(calendar)
        }

        datePickerBottomSheet.show(supportFragmentManager, "MonthPickerDialog")
    }

    override fun showSelectedDate(calendar: Calendar) {
        val now = Calendar.getInstance()

        when(mDateType) {
            SmartWaterMeterOverAll.Summarize.WEEK -> {
//                buttonCalendar.hide()
            }
            SmartWaterMeterOverAll.Summarize.MONTH -> {
                textTitle.text = if (now.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && now.get(Calendar.YEAR) == calendar.get(Calendar.YEAR))
                    "${mType?.title ?: ""}\n${getString(R.string.total)}"
                else
                    "${calendar.time.reportMMY()}\n${getString(R.string.total)}"
            }
            else -> {
                now.apply { add(Calendar.DATE, -1) }
                textTitle.text = if (now.get(Calendar.DATE) == calendar.get(Calendar.DATE) && now.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && now.get(Calendar.YEAR) == calendar.get(Calendar.YEAR))
                    "${mType?.title ?: ""}\n${getString(R.string.total)}"
                else
                    "${calendar.time.reportDMYFull()}\n${getString(R.string.total)}"
            }
        }
    }

    override fun showLoading() {
        progressDailog = indeterminateProgressDialog(getString(R.string.loading))
    }

    override fun hideLoading() {
        progressDailog?.dismiss()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

package com.sansiri.homeservice.ui.automation.bathome.widget

import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.model.menu.LocalIconMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import vc.siriventures.bathome.BAtHomeSDK
import vc.siriventures.bathome.model.BAtHomeComponent

class BAtHomeWidgetPresenter() : BasePresenterImpl<BAtHomeWidgetContract.View>(), BAtHomeWidgetContract.Presenter {

    private var mUsername: String? = null
    private var mPassword: String? = null
    private var mProjectId: String? = null


    override fun init(username: String?, password: String?, projectId: String?) {
        mUsername = username
        mPassword = password
        mProjectId = projectId
    }

    override fun start() {
        val menus = mutableListOf<Menu>().apply {
            add(LocalIconMenu(BAtHomeAutomation.TYPE_LIGHT, "Light", R.drawable.ic_bulb))
            add(LocalIconMenu(BAtHomeAutomation.TYPE_AC, "AC", R.drawable.ic_air_condition))
            add(LocalIconMenu(BAtHomeAutomation.TYPE_SCENE, "Scene", R.drawable.ic_book))
        }

        mView?.bindMenu(menus)
    }

    override fun destroy() {
    }

}
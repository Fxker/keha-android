package com.sansiri.homeservice.ui.homeupgrade.selector

import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgrade
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import com.sansiri.homeservice.util.inflate

/**
 * Created by sansiri on 4/9/18.
 */
class HomeUpgradeAdapter(val itemSelected:(HomeUpgradeHardware) -> Unit): androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    protected val mData = mutableListOf<HomeUpgrade>()

    companion object {
        val TITLE = 0
        val TOGGLE = 1
        val SELECTOR = 2
        val FUNDAMENTAL = 3
    }

    override fun getItemViewType(position: Int): Int {
        val data = mData[position]
        return if (data is HomeUpgradeRoom) {
            TITLE
        } else {
            if (data is HomeUpgradeHardware && data.attributes != null) {
                if (data.attributes.multiple) {
                    SELECTOR
                } else if (data.attributes.fundamental) {
                    FUNDAMENTAL
                } else {
                    TOGGLE
                }
            } else {
                TOGGLE
            }
        }
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is TitleViewHolder -> holder.bind(mData[position].name)
            is HomeUpgradeCheckViewHolder -> holder.bind(mData[position] as HomeUpgradeHardware)
            is HomeUpgradeFundamentalViewHolder -> holder.bind(mData[position] as HomeUpgradeHardware)
            is HomeUpgradeNumberPickerViewHolder -> holder.bind(mData[position] as HomeUpgradeHardware)
        }
    }

    override fun getItemCount() = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when(viewType) {
            TITLE -> TitleViewHolder(parent.inflate(R.layout.view_title))
            SELECTOR -> HomeUpgradeNumberPickerViewHolder(parent.inflate(R.layout.view_home_upgrade_number_picker), itemSelected)
            FUNDAMENTAL -> HomeUpgradeFundamentalViewHolder(parent.inflate(R.layout.view_home_upgrade_check), itemSelected)
            else -> HomeUpgradeCheckViewHolder(parent.inflate(R.layout.view_home_upgrade_check), itemSelected)
        }
    }

    fun setData(rooms: List<HomeUpgradeRoom>) {
        mData.clear()
        rooms.forEach { room ->
            mData.add(room)
            room.hardwareList?.forEach { hardware ->
                hardware.roomId = room.id
                // add fundamental item to list automatically
                if (hardware.attributes != null && hardware.attributes.fundamental) {
                    hardware.amount = 1
                    itemSelected(hardware)
                }
                mData.add(hardware)
            }
        }
        notifyDataSetChanged()
    }
}
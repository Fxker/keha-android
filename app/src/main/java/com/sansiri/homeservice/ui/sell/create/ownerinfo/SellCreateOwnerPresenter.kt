package com.sansiri.homeservice.ui.sell.create.ownerinfo

import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 11/7/17.
 */
class SellCreateOwnerPresenter : BasePresenterImpl<SellCreateOwnerContract.View>(), SellCreateOwnerContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }
}
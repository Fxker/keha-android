package com.sansiri.homeservice.ui.mailbox

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.util.hide
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class MailBoxViewHolder(val view: View, val itemClick: (MailBox) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: MailBox) {
        with(view) {
            textType.text = data.typeDisplayText
            textTitle.text = data.to
            if (data.details != null)
                textSubtitle.text = "${data.details}"
            else
                textSubtitle.hide()
            imageIcon.setImageResource(R.drawable.ic_mail_box)
            textDate.text = data.objectId
            root.setOnClickListener {
                itemClick(data)
            }
        }
    }
}
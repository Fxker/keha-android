package com.sansiri.homeservice.ui.automation.bathome.widget

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.adapter.GridMenuHorizontalAdapter
import com.sansiri.homeservice.ui.automation.bathome.control.BAtHomeControlActivity
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_widget_home_menu.*
import org.koin.androidx.scope.currentScope
import vc.siriventures.bathome.model.BAtHomeComponent

class BAtHomeWidgetFragment: BaseV2Fragment<BAtHomeWidgetContract.View, BAtHomeWidgetContract.Presenter>(), BAtHomeWidgetContract.View {
    override val mPresenter: BAtHomeWidgetContract.Presenter by currentScope.inject()

    companion object {
        val USERNAME = "USERNAME"
        val PASSWORD = "PASSWORD"
        val PROJECT_ID = "PROJECT_ID"

        fun newInstance(username: String, password: String, projectId: String): BAtHomeWidgetFragment {
            return BAtHomeWidgetFragment().apply {
                val bundle = Bundle()
                bundle.putString(USERNAME, username)
                bundle.putString(PASSWORD, password)
                bundle.putString(PROJECT_ID, projectId)
                arguments = bundle
            }
        }
    }

    private var mUsername: String? = null
    private var mPassword: String? = null
    private var mProjectId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUsername = arguments?.getString(USERNAME)
        mPassword = arguments?.getString(PASSWORD)
        mProjectId = arguments?.getString(PROJECT_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_widget_home_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        title.text = getString(R.string.home_automation)

        mPresenter.init(mUsername, mPassword, mProjectId)
        mPresenter.start()
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun bindMenu(menus: List<Menu>) {
        with(list) {
            if (menus.size <= 6) {
                layoutManager = GridLayoutManager(this.context, resources.getInteger(R.integer.grid_column))
                adapter = GridMenuAdapter(resources.getColor(R.color.colorAccent), null) {
                    launchComponentType(it.title, it.id)
                }.apply {
                    val animator = ScaleInAnimator(OvershootInterpolator(1f))
                    itemAnimator = animator
                    setDataDelay(menus)
                }
            } else {
                layoutManager = GridLayoutManager(this.context, 2, GridLayoutManager.HORIZONTAL, false)
                adapter = GridMenuHorizontalAdapter(resources.getColor(R.color.colorAccent), null, resources.getInteger(R.integer.grid_column) + 0.2) {
                    launchComponentType(it.title, it.id)
                }.apply {
                    val animator = ScaleInAnimator(OvershootInterpolator(1f))
                    itemAnimator = animator
                    setDataDelay(menus)
                }

            }
        }
    }

    override fun launchComponentType(title: String?, type: String?) {
        sendEvent("B@HOME_HOME_AUTOMATION", "CLICK", type)
        BAtHomeControlActivity.start(activity!!, title, type, mUsername, mPassword, mProjectId)
    }

    override fun showError(message: String) {
    }
}
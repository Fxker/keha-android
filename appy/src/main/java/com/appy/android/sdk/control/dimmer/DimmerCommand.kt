package com.appy.android.sdk.control.dimmer

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class DimmerCommand : APCommand {
    OFF {
        override fun action(): String {
            return "DIMMER_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "DIMMER_ON"
        }
    },
    DIMMER10 {
        override fun action(): String {
            return "DIMMER_10"
        }
    },
    DIMMER20 {
        override fun action(): String {
            return "DIMMER_20"
        }
    },
    DIMMER30 {
        override fun action(): String {
            return "DIMMER_30"
        }
    },
    DIMMER40 {
        override fun action(): String {
            return "DIMMER_40"
        }
    },
    DIMMER50 {
        override fun action(): String {
            return "DIMMER_50"
        }
    },
    DIMMER60 {
        override fun action(): String {
            return "DIMMER_60"
        }
    },
    DIMMER70 {
        override fun action(): String {
            return "DIMMER_70"
        }
    },
    DIMMER80 {
        override fun action(): String {
            return "DIMMER_80"
        }
    },
    DIMMER90 {
        override fun action(): String {
            return "DIMMER_90"
        }
    },
    DIMMER100 {
        override fun action(): String {
            return "DIMMER_100"
        }
    };

    companion object {
        @JvmStatic
        fun getLastCommand(value: String): String {
            return DimmerCommand.values()[value.toInt()].action()
        }

        @JvmStatic
        fun getAvailableCommand(value: String): List<String> {
            val dimmerCommandArray = arrayListOf<String>()
            DimmerCommand.values().forEach {
                if (it.action() != value) {
                    dimmerCommandArray.add(it.action())
                }
            }
            return dimmerCommandArray
        }
    }
}
package com.sansiri.homeservice.ui.phonedirectory

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_phone_directory_card.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class PhoneDirectoryViewHolder(val view: View, val itemClick: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: PhoneDirectoryGroup) {
        view.textTitle.text = data.name
        val panelContent = view.findViewById<LinearLayout>(R.id.panelContent)
        panelContent.removeAllViews()
        data.directories.forEach { contact ->
            val viewContact = panelContent.inflate(R.layout.view_phone_directory_contact)
            viewContact.findViewById<TextView>(R.id.textTitle).text = contact.name

            val panelNumber = viewContact.findViewById<LinearLayout>(R.id.panelNumber)
            for ((index, number) in contact.phones.withIndex()) {
                val viewNumber = panelNumber.inflate(R.layout.view_phone_directory_number)

                val textNumber = viewNumber.findViewById<TextView>(R.id.textNumber)
                textNumber.text = number
                textNumber.setOnClickListener {
                    itemClick(number)
                }

                panelNumber.addView(viewNumber)

                if (index == 0) {
                    viewContact.findViewById<View>(R.id.buttonCall).setOnClickListener {
                        itemClick(contact.phones[index])
                    }
                }
            }

            panelContent.addView(viewContact)
        }
    }
}
package com.sansiri.homeservice.ui.myhome

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.MyHome
import com.sansiri.homeservice.model.api.MyHomeDocument
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MyHomeContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut)
        fun fetch()
        fun requestToGoogleMap()
    }

    interface View : BaseView {
        fun bindBasicInfo(home: Hut)
        fun launchGoogleMap(lat: Double, lng: Double)
        fun bindAdvanceInfo(myHome: MyHome)
        fun bindDocument(documents: List<MyHomeDocument>)
        fun showLoading()
        fun hideLoading()
    }
}
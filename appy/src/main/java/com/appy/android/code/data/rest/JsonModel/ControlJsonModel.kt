package com.appy.android.code.data.rest.JsonModel

import com.appy.android.code.data.rest.JsonModel.fibaro.LocalId
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
class ControlJsonModel {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("alternate_title")
    var titleAlternate: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("class")
    var klass: String? = null
    @SerializedName("icon")
    var icon: String? = null
    @SerializedName("username")
    var username: String? = null
    @SerializedName("password")
    var password: String? = null
    @SerializedName("actions")
    var actions: List<ActionJsonModel>? = null

    // Bticino
    @SerializedName("circuit_address")
    var circuitAddress: String? = null
    @SerializedName("host_address")
    var hostAddress: String? = null
    @SerializedName("scene_command")
    var sceneAddress: String? = null

    // Daikin
    @SerializedName("status")
    var status: StatusJsonModel? = null

    // Fibaro
    @SerializedName("local_host")
    var localhost: String? = null
    @SerializedName("local_port")
    var localport: String? = null
    @SerializedName("hc_name")
    var hcName: String? = null
    @SerializedName("local_id")
    var localId: LocalId = LocalId()
}
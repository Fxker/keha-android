package com.sansiri.homeservice.service

import android.app.Notification
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.Feature

class NotificationPublisher : BroadcastReceiver() {

    companion object {

        var ID = "ID"
        var TITLE = "TITLE"
        var CONTENT = "CONTENT"
        var COLOR = "COLOR"
        var TYPE = "TYPE"
    }

    override fun onReceive(context: Context, intent: Intent) {

        val id = intent.getIntExtra(ID, 0)
        val title = intent.getStringExtra(TITLE)
        val content = intent.getStringExtra(CONTENT)
        val color = intent.getIntExtra(COLOR, context.resources.getColor(R.color.colorSecondary))
        val type = intent.getStringExtra(TYPE)

        NotificationHelper(context, id).create(title, content, color, type).submit()
    }

}
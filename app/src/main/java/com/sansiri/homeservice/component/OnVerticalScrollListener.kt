package com.sansiri.homeservice.component

/**
 * Created by oakraw on 10/2/2017 AD.
 */
import androidx.recyclerview.widget.RecyclerView
import android.util.Log

abstract class OnVerticalScrollListener : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
    var isScrollDown = false

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        if (!recyclerView!!.canScrollVertically(-1)) {
            onScrolledToTop()
        } else if (!recyclerView.canScrollVertically(1)) {
            onScrolledToBottom()
        }
        if (Math.abs(dy) > 1) {
            if (dy < 0 && isScrollDown) {
                isScrollDown = false
                Log.d("onScroll", "Up x:${dx} y:${dy}")
                onScrolledUp(dy)
            } else if (dy > 0 && !isScrollDown) {
                Log.d("onScroll", "Down x:${dx} y:${dy}")
                isScrollDown = true
                onScrolledDown(dy)
            }
        }
    }

    open fun onScrolledUp(dy: Int) {
        onScrolledUp()
    }

    open fun onScrolledDown(dy: Int) {
        onScrolledDown()
    }

    open fun onScrolledUp() {}

    open fun onScrolledDown() {}

    open fun onScrolledToTop() {}

    open fun onScrolledToBottom() {}
}
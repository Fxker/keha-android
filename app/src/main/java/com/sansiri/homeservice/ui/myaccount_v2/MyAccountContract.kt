package com.sansiri.homeservice.ui.myaccount_v2

import com.sansiri.homeservice.model.api.myaccount.BalanceV2
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/31/17.
 */
interface MyAccountContract {
    interface Presenter : BasePresenter<View> {
    }

    interface View : BaseView {
    }
}
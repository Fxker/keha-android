package com.sansiri.homeservice.service

import com.sansiri.homeservice.model.api.OnionShackDeviceStatus
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody

/**
 * Created by sansiri on 10/24/17.
 */
class OnionShackRepository(val idToken: String, val apiService: OnionShackService) {
    fun dimmerOn(apiKey: String, deviceId: String, command: String): Observable<ResponseBody> {
        return apiService.dimmerOn(idToken, apiKey, deviceId, command)
    }

    fun dimmerOn(command: String) {
        dimmerOn(
                "629ed199-3bcb-4dea-b861-4a298ca17c17",
                "bf109b55-1930-4840-ac57-50626c57280c",
                command
        ).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({

                }, {
                })
    }

    fun getDeviceStatusList(apiKey: String): Observable<List<OnionShackDeviceStatus>> {
       return apiService.getAllDeviceStatus(idToken, apiKey)
    }
}
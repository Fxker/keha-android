package com.sansiri.homeservice.ui.announcement.summary

import com.sansiri.homeservice.model.api.announcement.Announcement

/**
 * Created by sansiri on 12/14/17.
 */
data class SummaryItem(val title: String, val id: String, val announcement: List<Announcement<*>>) {
    companion object {
        val SANSIRI_FAMILY = "SANSIRI_FAMILY"
        val ANNOUNCEMENT = "ANNOUNCEMENT"
    }
}
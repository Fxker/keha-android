package com.sansiri.homeservice.ui.electric.mea.consent


import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.text.HtmlCompat
import com.bumptech.glide.Glide
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.util.TextWatcherExtend
import kotlinx.android.synthetic.main.fragment_electric_mea_consent.*
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainActivity
import com.sansiri.homeservice.ui.scanner.QRScannerActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import im.delight.android.webview.AdvancedWebView
import org.koin.androidx.scope.currentScope

class MeaElectricConsentFragment : BaseV2Fragment<MeaElectricConsentContract.View, MeaElectricConsentContract.Presenter>(), MeaElectricConsentContract.View, AdvancedWebView.Listener {
    override val mPresenter: MeaElectricConsentContract.Presenter by currentScope.inject()
    private var mIconUrl: String? = null
    private var mConsent: Consent? = null

    companion object {
        const val ICON_URL = "ICON_URL"
        const val CONSENT = "CONSENT"

        fun newInstance(iconUrl: String?, consent: Consent): MeaElectricConsentFragment {
            return MeaElectricConsentFragment().apply {
                arguments = Bundle().apply {
                    putString(ICON_URL, iconUrl)
                    putParcelable(CONSENT, consent)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIconUrl = arguments?.getString(ICON_URL)
        mConsent = arguments?.getParcelable(CONSENT)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MEA_TERM_AND_CONDITION")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_electric_mea_consent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mIconUrl?.let { Glide.with(this).load(it).into(imageIcon) }

        buttonBack.setOnClickListener {
            activity?.finish()
        }

        buttonDecline.setOnClickListener {
            sendEvent("MEA_TERM_AND_CONDITION", "MEA_REGISTER", "DECLINE")
            activity?.finish()
        }

        buttonAccept.setOnClickListener {
            sendEvent("MEA_TERM_AND_CONDITION", "MEA_REGISTER", "ACCEPT")
            mPresenter.accept()
        }

        webView.setBackgroundColor(Color.TRANSPARENT)

        mPresenter.init(mConsent)
        mPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(consent: Consent) {
        var detail = consent.description
        if (detail != null) {
            webView.setListener(activity, this)
            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    val removeNotNeedPathUrl = url.removePrefix("file:///android_asset/css/")
                    try {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(removeNotNeedPathUrl))
                        startActivity(intent)
                        return true
                    } catch (e: Exception) {
                        val intent = Intent(Intent.ACTION_WEB_SEARCH)
                        intent.putExtra(SearchManager.QUERY, removeNotNeedPathUrl);
                        startActivity(intent)
                        return true
                    }
                }
            }


            if (!detail.contains("body")) {
                detail = "<body>$detail</body>"
            }

            val html = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"classic.css\"/></head>$detail</html>"
            webView.loadHtml(html, "file:///android_asset/css/")
        }

    }

    override fun onPageFinished(url: String?) {
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
    }

    override fun onExternalPageRequest(url: String?) {
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun launchNext() {
        (activity as MEAElectricMainActivity).onConsentAccepted()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

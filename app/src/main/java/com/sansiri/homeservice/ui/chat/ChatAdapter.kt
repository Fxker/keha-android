package com.sansiri.homeservice.ui.chat

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.chat.Sender
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ChatAdapter(val onImageClicked: (String) -> Unit, val onLinkClicked: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mConversation = mutableListOf<ChatMessage>()
    val MESSAGE_ME = 0
    val PHOTO_ME = 1
    val MESSAGE_OTHER = 2
    val PHOTO_OTHER = 3
    private var mSenderProfileImage: String = ""

    override fun getItemCount(): Int = mConversation.size

    override fun getItemViewType(position: Int): Int {
        val message = mConversation[position]
        if (message.sender.type == Sender.TYPE_USER) {
            if (message.type == ChatMessage.TYPE_TEXT) {
                return MESSAGE_ME
            } else {
                return PHOTO_ME
            }
        } else {
            if (message.type == ChatMessage.TYPE_TEXT) {
                return MESSAGE_OTHER
            } else {
                return PHOTO_OTHER
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            MESSAGE_ME -> MeMessageViewHolder(parent.inflate(R.layout.view_chat_box_me), onLinkClicked)
            PHOTO_ME -> MePhotoViewHolder(parent.inflate(R.layout.view_chat_photo_me), onImageClicked)
            PHOTO_OTHER -> OtherPhotoViewHolder(parent.inflate(R.layout.view_chat_photo_other), mSenderProfileImage, onImageClicked)
            else -> OtherMessageViewHolder(parent.inflate(R.layout.view_chat_box_other), mSenderProfileImage, onLinkClicked)
        }
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is MeMessageViewHolder) {
            holder.bind(mConversation[position])
        } else if (holder is MePhotoViewHolder) {
            holder.bind(mConversation[position])
        } else if (holder is OtherMessageViewHolder) {
            holder.bind(mConversation[position])
        } else if (holder is OtherPhotoViewHolder) {
            holder.bind(mConversation[position])
        }
    }


    fun setMessage(conversation: List<ChatMessage>) {
        mConversation.clear()
        mConversation.addAll(conversation)
        notifyDataSetChanged()
    }

    fun addMessage(message: List<ChatMessage>) {
        mConversation.addAll(message)
        notifyDataSetChanged()
    }

    fun addMessageAt(message: List<ChatMessage>, position: Int) {
        mConversation.addAll(position, message)
        notifyDataSetChanged()
    }

    fun setSenderProfile(image: String) {
        mSenderProfileImage = image
    }

}
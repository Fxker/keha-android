package com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachineStart
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import java.util.concurrent.TimeUnit

class TrendyWashMachinePresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TrendyWashMachineContract.View>(), TrendyWashMachineContract.Presenter {
    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mMachine: TrendyWashMachine? = null


    override fun init(unitId: String, userId: String, machine: TrendyWashMachine) {
        mUnitId = unitId
        mUserId = userId
        mMachine = machine
    }

    override fun start() {
        if (mMachine != null) {
            mView?.bindData(mMachine!!)
        }
    }

    override fun destroy() {
        updateMachines()
        mCompositeDisposable.clear()
    }

    override fun startMachine(): MutableLiveData<Resource<TrendyWashMachineStart>>? =
            if (mUnitId != null && mUnitId != null && mMachine?.machineNo != null && mMachine?.defaultTime != null)
                repository.startTrendyWashMachine(mUnitId!!, mUserId!!, mMachine?.machineNo!!, TrendyWashMachineStart(mMachine?.defaultTime!!))
            else null


    override fun startCountDown(seconds: Int) {
        object : CountDownTimer(TimeUnit.SECONDS.toMillis(seconds.toLong()), 1000) {
            override fun onFinish() {
                mView?.showMachineAvailable()
            }

            override fun onTick(millisUntilFinished: Long) {
                val remainTime = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished).toInt()
                mView?.updateCountDownTime(remainTime)
            }

        }.start()

        mView?.setNotificationAlarm(seconds, mMachine)
    }

    override fun calculatePriceFromTime(second: Int) {
        mMachine?.defaultTime = second
        mView?.showCalculatedPrice(TimeUnit.SECONDS.toMinutes(second.toLong()).toInt() / 10 * (mMachine?.price ?: 0))
    }

    private fun updateMachines() {
        if (mUnitId != null && mUserId != null) {
            repository.getTrendyWashMachines(mUnitId!!, mUserId!!)
            repository.getTrendyWashCredit(mUnitId!!, mUserId!!)
        }
    }
}
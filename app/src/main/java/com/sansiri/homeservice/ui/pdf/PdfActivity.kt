package com.sansiri.homeservice.ui.pdf

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.widget.Toast
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.PermissionActivity
import com.sansiri.homeservice.ui.base.requestPermissionReadWriteWithPermissionCheck
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_pdf.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileOutputStream

class PdfActivity : PermissionActivity<PdfContract.View, PdfContract.Presenter>(), PdfContract.View {
    override var mPresenter: PdfContract.Presenter = PdfPresenter()

    companion object {
        val URL = "URL"
        val TITLE = "TITLE"
        fun start(activity: Activity, url: String, title: String) {
            val intent = Intent(activity, PdfActivity::class.java)
            intent.putExtra(URL, url)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf)

        val url = intent.getStringExtra(WebActivity.URL)
        val title = intent.getStringExtra(WebActivity.TITLE)

        setBackToolbar(title)
        mPresenter.downloadPdfFromUrl(url)
    }

    override fun renderPdf(byte: ByteArray) {
        pdfView.fromBytes(byte).spacing(2).load()
    }

    override fun createFile(byte: ByteArray, title: String) {
        callback = object : Callback {
            override fun onPermissionAllowed(permission: String) {
                if (permission == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    doAsync {
                        val folderHomeService = File(Environment.getExternalStorageDirectory().toString(), "HomeService")
                        if (!folderHomeService.exists()) {
                            folderHomeService.mkdir()
                        }

                        val folderPDF = File(folderHomeService, "PDF")
                        if (!folderPDF.exists()) {
                            folderPDF.mkdir()
                        }

                        val file = File(folderPDF, title)
                        if (!file.exists()) {
                            file.createNewFile()
                        }

                        val outputStream = FileOutputStream(file)
                        outputStream.write(byte)
                        outputStream.close()
                        uiThread {
                            Toast.makeText(this@PdfActivity, "PDF Saved", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
        requestPermissionReadWriteWithPermissionCheck()
    }

    override fun shareFile(url: String) {
        val intent = Intent(Intent.ACTION_SEND).apply {
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        startActivity(intent)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }



    override fun showError(message: String) {
        alertError(message)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_download) {
            mPresenter.download()
        } else if (item.itemId == R.id.action_share) {
            mPresenter.share()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_download_share, menu)
        return true
    }
}

package com.sansiri.homeservice.ui.homecare.history.detail

import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 10/11/17.
 */
class HomeCareHistoryDetailPresenter : BasePresenterImpl<HomeCareHistoryDetailContract.View>(), HomeCareHistoryDetailContract.Presenter {

    fun init() {
    }

    override fun start() {
    }

    override fun destroy() {
    }

}

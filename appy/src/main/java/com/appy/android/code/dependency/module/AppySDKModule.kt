package com.appy.android.code.dependency.module

import android.app.Application
import android.content.Context
import com.appy.android.code.base.domain.executor.*
import com.appy.android.code.data.rest.RestClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
@Module
internal class AppySDKModule(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return this.application
    }

    @Provides
    @Singleton
    internal fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(appyUIThread: UIThread): PostExecutionThread {
        return appyUIThread
    }

    @Provides
    @Singleton
    internal fun provideUiThreadHandler(context: Context): TheadHandlerImpl {
        return TheadHandlerImpl(context)
    }

    @Provides
    @Singleton
    internal fun provideRestClient(context: Context, threadExecutor: ThreadExecutor): RestClient {
        return RestClient(context, threadExecutor)
    }
}

package com.appy.android.sdk.control.fibaro

import com.appy.android.sdk.control.APCommand

/**
 * Created by Francisco Barrios 15/11/2018.
 */

enum class FibaroFanSpeedCommand : APCommand {
    AUTO_LOW {
        override fun action(): String {
            return "{\"args\":[0]}"
        }
    },
    LOW_SPEED {
        override fun action(): String {
            return "{\"args\":[1]}"
        }
    },
    AUTO_HIGH_SPEED {
        override fun action(): String {
            return "{\"args\":[2]}"
        }
    },
    HIGH_SPEED {
        override fun action(): String {
            return "{\"args\":[3]}"
        }
    },
    AUTO_MEDIUM_SPEED {
        override fun action(): String {
            return "{\"args\":[4]}"
        }
    },
    MEDIUM_SPEED {
        override fun action(): String {
            return "{\"args\":[5]}"
        }
    },
    OFF {
        override fun action(): String {
            return "{\"args\":[128]}"
        }
    };
}
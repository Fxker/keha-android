package com.sansiri.homeservice.ui.language

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import java.util.*

/**
 * Created by sansiri on 10/20/17.
 */
class LanguagePresenter : BasePresenterImpl<LanguageContract.View>(), LanguageContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun selectThai() {
        mView?.setAppLanguage(Locale("th"))
    }

    override fun selectEnglish() {
        mView?.setAppLanguage(Locale.ENGLISH)
    }

    override fun selectJapanese() {
        mView?.setAppLanguage(Locale.JAPANESE)
    }

    override fun selectChineseSimplified() {
        mView?.setAppLanguage(Locale.SIMPLIFIED_CHINESE)
    }

    override fun selectChineseTraditional() {
        mView?.setAppLanguage(Locale.TRADITIONAL_CHINESE)
    }
}
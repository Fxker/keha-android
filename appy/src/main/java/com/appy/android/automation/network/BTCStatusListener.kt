package com.appy.android.automation.network

/**
 * Created by Tar on 9/15/16 AD.
 */
interface BTCStatusListener {

    fun onReceiveMessage(status: String, fetchMessage: String, value: String)

}
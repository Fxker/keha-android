package com.sansiri.homeservice.model.event

data class CloseEvent(val forceClose: Boolean = false)
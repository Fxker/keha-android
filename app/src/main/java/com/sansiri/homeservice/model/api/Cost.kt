package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

data class Cost(
        override var title: String?,
        @SerializedName("costUrl")
        override var url: String?
) : Doc
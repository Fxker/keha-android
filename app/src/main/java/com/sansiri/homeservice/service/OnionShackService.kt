package com.sansiri.homeservice.service

import com.sansiri.homeservice.data.network.RetrofitFactory
import com.sansiri.homeservice.model.api.*
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.PUT
import retrofit2.http.Path

/**
 * Created by sansiri on 10/24/17.
 */
interface OnionShackService {
    @PUT("{api_key}/levels/{device_id}/on/{command}")
    fun dimmerOn(@Header("Authorization") idToken: String,
                 @Path("api_key") apiKey: String,
                 @Path("device_id") deviceId: String,
                 @Path("command") command: String
    ): Observable<ResponseBody>

    @GET("{api_key}/levels")
    fun getAllDeviceStatus(@Header("Authorization") idToken: String,
                 @Path("api_key") apiKey: String
    ): Observable<List<OnionShackDeviceStatus>>



    companion object Factory {
        fun create(): OnionShackService {
            return RetrofitFactory.instanceOnionShack.create(OnionShackService::class.java)
        }
    }
}
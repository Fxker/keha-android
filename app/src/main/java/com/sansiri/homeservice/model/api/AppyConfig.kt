package com.sansiri.homeservice.model.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AppyConfig (
        val appyId: String? = null,
        val pin: String? = null
): Parcelable
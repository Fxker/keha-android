package com.sansiri.homeservice.ui.chat

import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.model.api.chat.ChatTemplate

import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_chat_template_item.view.*
import java.util.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ChatTemplateAdapter(val onSelected: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<ChatTemplateAdapter.ViewHolder>() {
    private val mData = mutableListOf<ChatTemplate>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatTemplateAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_chat_template_item), onSelected)
    }

    override fun onBindViewHolder(holder: ChatTemplateAdapter.ViewHolder, position: Int) {
        holder.bindData(mData[position])
    }

    fun setData(data: List<ChatTemplate>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View, val onSelected: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bindData(chatTemplate: ChatTemplate) {
            if (Store.getLocale() == Locale("th")) {
                initButton(chatTemplate.titleTh, chatTemplate.messageTh)
            } else {
                initButton(chatTemplate.titleEn, chatTemplate.messageEn)
            }
        }

        private fun initButton(title: String?, message: String?) {
            with(view) {
                button.text = title
                button.setOnClickListener {
                    if (message != null) {
                        onSelected(message)
                    }
                }
            }
        }
    }
}
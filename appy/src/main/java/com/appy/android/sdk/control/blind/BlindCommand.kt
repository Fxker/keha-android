package com.appy.android.sdk.control.blind

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class BlindCommand : APCommand {
    STOP {
        override fun action(): String {
            return "BLIND_STOP"
        }
    },
    ON {
        override fun action(): String {
            return "BLIND_ON"
        }
    },
    OFF {
        override fun action(): String {
            return "BLIND_OFF"
        }
    };

    companion object {
        @JvmStatic
        fun getLastCommand(value: String): BlindCommand {
            return BlindCommand.values()[value.toInt()]
        }

        @JvmStatic
        fun getAvailableCommand(value: String): List<BlindCommand> {
            var blindCommandArray = BlindCommand.values().filter { it != getLastCommand(value) }
            return blindCommandArray
        }
    }
}
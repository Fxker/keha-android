package com.sansiri.homeservice.ui.scanner

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class QRScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {


    companion object {
        val CODE = 23
        val DATA = "DATA"

        fun start(activity: Activity) {
            val intent = Intent(activity, QRScannerActivity::class.java)
            activity.startActivityForResult(intent, CODE)
        }

        fun start(fragment: Fragment) {
            val intent = Intent(fragment.activity, QRScannerActivity::class.java)
            fragment.startActivityForResult(intent, CODE)
        }
    }

    private var mScannerView: ZXingScannerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerView = ZXingScannerView(this)
        mScannerView?.setLaserEnabled(false)
        mScannerView?.setBorderColor(Color.WHITE)

        setContentView(mScannerView)
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    fun showCamera() {
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
    }

    override fun onResume() {
        super.onResume()
        showCameraWithPermissionCheck()
    }

    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }

    override fun handleResult(rawResult: Result) {
        val intent = Intent()
        intent.putExtra(DATA, rawResult.text)
        setResult(Activity.RESULT_OK, intent)
        finish()
        //mScannerView?.resumeCameraPreview(this)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    fun onPermissionDenied() {
        finish()
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    fun onNeverAskAgain() {
        finish()
    }
}

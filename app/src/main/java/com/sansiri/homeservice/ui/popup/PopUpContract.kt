package com.sansiri.homeservice.ui.popup

import com.sansiri.homeservice.model.api.PopUp
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface PopUpContract {
    interface Presenter : BasePresenter<View> {
    }

    interface View : BaseView {
    }
}
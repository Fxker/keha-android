package com.sansiri.homeservice.ui.myaccount_v2.balance

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class MyAccountBalanceAdapter(val color: Int, val itemClick: (PaymentHistory) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<PaymentHistory>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return MyAccountBalanceViewHolder(parent.inflate(R.layout.view_summary_card), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is MyAccountBalanceViewHolder)
            holder.bind(mData[position], color)
    }

    fun setData(data: List<PaymentHistory>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: List<PaymentHistory>) {
        mData.addAll(data)
        notifyDataSetChanged()
    }
}
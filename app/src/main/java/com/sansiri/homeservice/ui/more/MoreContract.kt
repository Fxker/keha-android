package com.sansiri.homeservice.ui.more

import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/24/17.
 */
interface MoreContract {
    interface Presenter : BasePresenter<View> {
        fun selectMenu(menu: Menu)
        fun unregisterNotification(deviceId: String?, callback: () -> Unit)
    }

    interface View : BaseView {
        fun bindMenu(menu: List<GenericMenu>)
        fun launchProfile()
        fun launchSansiriLounge()
        fun launchLanguage()
        fun signOut()
        fun launchNotificationSettings()
        fun launchGoogleAssistant()
        fun showSignOut()
        fun hideSignOut()
    }
}
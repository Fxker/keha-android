package com.sansiri.homeservice.ui.automation.orvibo.home

//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import androidx.fragment.app.Fragment
//import com.bumptech.glide.Glide
//import com.google.android.material.bottomsheet.BottomSheetDialog
//import com.homemate.sdk.model.HMFamily
//import com.sansiri.homeservice.R
//import com.sansiri.homeservice.model.Home
//import com.sansiri.homeservice.model.Hut
//import com.sansiri.homeservice.ui.automation.orvibo.login.OrviboLoginActivity
//import com.sansiri.homeservice.ui.base.BaseV2Activity
//import com.sansiri.homeservice.util.glide.loadCache
//import com.sansiri.homeservice.util.hide
//import com.sansiri.homeservice.util.show
//import kotlinx.android.synthetic.main.activity_orvibo_home.*
//import kotlinx.android.synthetic.main.dialog_bottom_sheet_family_selector.view.*
//import org.koin.android.ext.android.inject
//import org.koin.androidx.scope.currentScope
//
//
//class OrviboHomeActivity : BaseV2Activity<OrviboHomeContract.View, OrviboHomeContract.Presenter>(), OrviboHomeContract.View {
//    override val mPresenter: OrviboHomeContract.Presenter by inject()
//    private var mHome: Hut? = null
//
//    companion object {
//        const val HOME = "HOME"
//
//        fun start(activity: Activity, home: Hut?) {
//            val intent = Intent(activity, OrviboHomeActivity::class.java)
//            intent.putExtra(HOME, home)
//            activity.startActivity(intent)
//        }
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_orvibo_home)
//
//        mHome = intent.getParcelableExtra(HOME)
//
//        mPresenter.init(mHome?.unitId ?: "")
//        mPresenter.start()
//
//        buttonLogout.setOnClickListener {
//            mPresenter.logout()
//            finish()
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//        sendScreenView("ORVIBO_HOME_AUTOMATION_HOME")
//    }
//
//    override fun bindFamilies(families: List<HMFamily>) {
//        imageFamily.show()
//
//        val bottomSheetView = layoutInflater.inflate(R.layout.dialog_bottom_sheet_family_selector, null)
//        val bottomSheetFamilyDialog = BottomSheetDialog(this).apply {
//            setContentView(bottomSheetView)
//        }
//
//        bottomSheetView.listFamily.adapter = OrviboFamilyAdapter {family ->
//            mPresenter.switchFamily(family.id)
//            bottomSheetFamilyDialog.dismiss()
//        }.apply {
//            addData(families)
//        }
//
//        imageFamily.setOnClickListener {
//            bottomSheetFamilyDialog.show()
//        }
//    }
//
//    override fun bindCurrentFamily(currentFamily: HMFamily) {
//        if (currentFamily.pic != null) {
//            Glide.with(this).loadCache(currentFamily.pic!!, imageFamily)
//        } else {
//            imageFamily.setImageResource(R.drawable.placeholder_contact)
//        }
//    }
//
//    override fun hideFamilySelector() {
//        imageFamily.hide()
//    }
//
//    override fun launchLoginPage() {
//        OrviboLoginActivity.start(this, mHome)
//        finish()
//    }
//
//    override fun logoutSuccess() {
//        finish()
//    }
//
//    override fun showError(message: String) {
//    }
//}

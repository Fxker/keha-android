package com.appy.android.automation.network

import com.appy.android.automation.helper.Validator
import java.io.*
import java.net.InetAddress
import java.net.Socket

class TCPSocketHandler(address: String, port: Int) : SocketHandler() {

    internal var socket: Socket? = null

    var onConnectedListener: OnConnectedListener? = null
    var onTerminatedListener: OnTerminatedListener? = null
    var onReceivedResponseListener: OnReceivedResponseListener? = null

    val isDisconnect: Boolean
        get() = socket!!.isClosed

    init {
        this.address = address
        this.port = port
    }

    override fun run() {
        try {
            socket = Socket(InetAddress.getByName(address!!.replace("http://", "").replace("https://", "")), port)

            if (onConnectedListener != null) {
                onConnectedListener!!.onConnected(this)
            }
        } catch (e: Exception) {
            e.printStackTrace()

            terminate(SocketHandler.CONNECTION_ERROR)

            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }
        }

    }

    fun print(message: String) {
        if (!Validator.isValid(message)) {
            return
        }

        try {
            val bw = BufferedWriter(OutputStreamWriter(socket!!.getOutputStream()))
            val writer = PrintWriter(bw, true)
            writer.print(message)
            writer.flush()

            if (writer.checkError()) {
                setErrorCode(null, SocketHandler.Companion.FAILED_TO_WRITE)
            }

            scheduleTimeOut()
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_WRITE)
        }

    }

    fun println(message: String) {
        if (!Validator.isValid(message)) {
            return
        }

        try {
            val bw = BufferedWriter(OutputStreamWriter(socket!!.getOutputStream()))
            val writer = PrintWriter(bw, true)
            writer.println(message)
            writer.flush()

            if (writer.checkError()) {
                setErrorCode(null, SocketHandler.Companion.FAILED_TO_WRITE)
            }

            scheduleTimeOut()
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_WRITE)
        }

    }

    fun read() {
        scheduleTimeOut()

        try {
            val br = BufferedReader(InputStreamReader(socket!!.getInputStream()))
            val line: String

            var buffer: CharArray? = CharArray(1024 * 1024)
            val size = br.read(buffer!!)

            if (size != -1) {
                line = String(buffer, 0, size)
                responses.add(line)
                buffer = null

                if (onReceivedResponseListener != null) {
                    onReceivedResponseListener!!.onReceivedResponse(this, line)
                }
            }
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_READ)
        }

    }

    fun read(marker: String) {
        scheduleTimeOut()

        try {
            val br = BufferedReader(InputStreamReader(socket!!.getInputStream()))
            var line = ""

            while (true) {
                val i = br.read()
                if (i != -1) {
                    line += i.toChar()

                    if (line.endsWith(marker)) {
                        responses.add(line)

                        if (onReceivedResponseListener != null) {
                            onReceivedResponseListener!!.onReceivedResponse(this, line)
                        }
                        break
                    }
                } else {
                    break
                }
            }
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_READ)
        }

    }

    fun readLine() {
        scheduleTimeOut()

        try {
            val br = BufferedReader(InputStreamReader(socket!!.getInputStream()))
            val line = br.readLine()

            if (line != null) {
                responses.add(line)

                if (onReceivedResponseListener != null) {
                    onReceivedResponseListener!!.onReceivedResponse(this, line)
                }
            }
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_READ)
        }

    }

    override fun terminate() {
        super.terminate()

        try {
            if (socket != null) {
                socket!!.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }

            onConnectedListener = null
            onTerminatedListener = null
            onReceivedResponseListener = null
        }
    }

    interface OnConnectedListener {
        fun onConnected(socketHandler: TCPSocketHandler)
    }

    interface OnTerminatedListener {
        fun onTerminated(socketHandler: TCPSocketHandler)
    }

    interface OnReceivedResponseListener {
        fun onReceivedResponse(socketHandler: TCPSocketHandler, response: String)
    }
}

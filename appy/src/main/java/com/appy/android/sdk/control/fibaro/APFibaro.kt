package com.appy.android.sdk.control.fibaro

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

class APFibaro : APControl, Parcelable {
    var mActions: List<APActionFibaro>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionFibaro)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionFibaro()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                FibaroCommand.ON_OFF.action() -> {
                    action.action = FibaroCommand.ON_OFF
                }
                FibaroCommand.FAN_SPEED.action() -> {
                    action.action = FibaroCommand.FAN_SPEED
                }
                FibaroCommand.TEMP.action() -> {
                    action.action = FibaroCommand.TEMP
                }
            }
            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APFibaro> {
        const val TYPE = "APFibaro"

        override fun createFromParcel(parcel: Parcel): APFibaro {
            val type = parcel.readString()
            return APFibaro(parcel)
        }

        override fun newArray(size: Int): Array<APFibaro?> {
            return arrayOfNulls(size)
        }
    }
}
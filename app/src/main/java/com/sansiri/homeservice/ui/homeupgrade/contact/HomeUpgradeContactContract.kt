package com.sansiri.homeservice.ui.homeupgrade.contact

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeUpgradeContactContract {
    interface Presenter : BasePresenter<View> {
        fun init(orderRequest: List<HomeUpgradeOrderRequest>, unitId: String)
        fun submit(name: String, email: String, phone: String)
    }

    interface View : BaseView {
        fun nameError(message: String)
        fun emailError(message: String)
        fun phoneError(message: String)
        fun getStringFromRes(res: Int): String
        fun bindData(displayname: String, email: String, mobilePhone: String)
        fun showSubmitting()
        fun showSubmitted()
    }
}
package com.sansiri.homeservice.data.repository.meter

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterDetail
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterHistoryList
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterOverAll
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterHistoryList
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll

interface SmartMeterRepository {
    fun getSmartWaterMeterSummary(unitId: String, meterId: String, date: String): MutableLiveData<Resource<SmartWaterMeterOverAll>>
    fun getSmartWaterMeterHistory(unitId: String, meterId: String, type: String, date: String): MutableLiveData<Resource<SmartWaterMeterHistoryList>>
    fun getSmartElectricMeterSummary(unitId: String, meterId: String, date: String): MutableLiveData<Resource<SmartElectricMeterOverAll>>
    fun getSmartElectricMeterHistory(unitId: String, meterId: String, type: String, date: String): MutableLiveData<Resource<SmartElectricMeterHistoryList>>
    fun destroy()
    fun getSmartElectricMeterLast(unitId: String, meterId: String, date: String): MutableLiveData<Resource<SmartElectricMeterDetail>>
}

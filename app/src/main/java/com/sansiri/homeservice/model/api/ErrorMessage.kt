package com.sansiri.homeservice.model.api

data class ErrorMessage(val message: String? = null)
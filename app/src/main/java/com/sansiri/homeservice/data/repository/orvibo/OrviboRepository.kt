package com.sansiri.homeservice.data.repository.orvibo

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
//import com.homemate.sdk.model.*
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import java.util.*

interface OrviboRepository {
    fun getOrviboHome(unitId: String): LiveData<OrviboHome>

    @WorkerThread suspend fun insertOrviboHome(home: OrviboHome)

    fun isLoggedIn(): Boolean

    fun login(username: String, password: String, onSuccess: () -> Unit, onError: (String) -> Unit)

    fun getPasswordMD5(): String

//    fun getFamilies(onSuccess: (List<HMFamily>) -> Unit)

    fun getCurrentFamilyId(): String?

    fun logout()

    fun switchFamily(familyId: String, onSuccess: () -> Unit)

//    fun getScenes(): List<HMScene>?

//    fun getRooms(): List<HMRoom>?

    @WorkerThread suspend fun updateFamilyId(unitId: String, familyId: String)

    @WorkerThread suspend fun deleteOrviboHome(unitId: String)

//    fun getDevices(hubId: String?, roomId: String): List<HMDevice>?

//    fun getHubInfo(onSuccess: (List<HMHub>?) -> Unit, onError: () -> Unit)

    fun turnOnSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit)

    fun turnOffSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit)

    fun toggleSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit)

    fun dimmingLight(hubId: String, deviceId: String, progress: Int, onSuccess: () -> Unit)

    fun activateScene(hubId: String, sceneId: String, onSuccess: () -> Unit)
}
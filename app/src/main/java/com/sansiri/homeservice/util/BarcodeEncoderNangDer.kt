package com.sansiri.homeservice.util

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import androidx.core.content.res.ResourcesCompat
import com.google.zxing.qrcode.QRCodeWriter


class BarcodeEncoderNangDer: BarcodeEncoder() {
    private val WHITE = Color.WHITE
    private val BLACK = Color.BLACK

//    @Throws(WriterException::class)
//    fun encodeBitmap(contents: String, width: Int, height: Int, hints: Map<EncodeHintType, *>): Bitmap {
//        val matrix = QRCodeWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hints)
//
//        MatrixToImageWriter
//
//        image = MatrixToImageWriter.toBufferedImage(matrix)
//
//        return createBitmap(encode(contents, format, width, height, hints))
//    }

    fun createBitmapWithLogo(matrix: BitMatrix, logoBitmap: Bitmap): Bitmap {
        val width = matrix.width
        val height = matrix.height
        val pixels = IntArray(width * height)
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (matrix.get(x, y)) BLACK else WHITE
            }
        }

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)



        return bitmap
    }
}
package com.sansiri.homeservice.ui.automation.appy.remotecontrol.ac

import android.util.Log
import com.appy.android.code.base.data.rest.error.APError
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.home.APHome
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.showHttpError

class HomeAutomationACRemoteControlPresenter : BasePresenterImpl<HomeAutomationACRemoteControlContract.View>(), HomeAutomationACRemoteControlContract.Presenter {
    lateinit var mAppyManager: AppyRepositoryImpl
    lateinit var control: APRemoteControl
    lateinit var homeId: String
    lateinit var home: APHome
    lateinit var projectId: String

    private val DEFAULT_TEMPERATURE = 25

    var temperature = DEFAULT_TEMPERATURE
        set(value) {
            field = value
            mView?.setTemperature(value)
        }
    var isTurnOn = false
        set(value) {
            field = value
            mView?.setTurnOn(value)
        }
    var commandFanIndex: Int? = null
    var commandModeIndex: Int? = null

    private var minTemp = 18
    private var maxTemp = 30

    override fun init(control: APRemoteControl, homeId: String, projectId: String) {
        this.control = control
        this.homeId = homeId
        this.projectId = projectId
        mAppyManager = AppyRepositoryImpl(projectId, "12345")

        home = APHome()
        home.id = mAppyManager.getDedicatedUnitId(homeId)
    }

    override fun start() {
        mView?.bindAirConditioner(control)
        getMinMaxTemp()
        control.id?.let { mView?.restoreSave(homeId, it) }
    }

    override fun destroy() {
    }

    override fun decreaseTemp() {
        if (temperature > minTemp) {
            temperature -= 1
            commandTemp(temperature)
        }
    }

    override fun increaseTemp() {
        if (temperature < maxTemp) {
            temperature += 1
            commandTemp(temperature)
        }
    }

    private fun getMinMaxTemp() {
        control.mActions?.forEach { action ->
            if (action.action != null && action.action!!.toUpperCase().contains("SET_TEMP")) {
                val tempSet = action.getParameters()?.map { param ->
                    try {
                        param.split(" celcius")[0].toInt()
                    } catch (e: NumberFormatException) {
                        0
                    }
                }

                minTemp = tempSet?.min() ?: 0
                maxTemp = tempSet?.max() ?: 40
            }
        }
    }

    override fun commandTemp(temp: Int) {
//        mAppyManager.commandRemoteControl(home, )
        takeAction("SET_TEMP", "$temp celcius")
    }

    override fun commandFan(index: Int) {
        if (commandFanIndex != index) {
            commandFanIndex = index
            takeAction("FAN_SPEED", when (index) {
                0 -> "low_speed"
                1 -> "medium_speed"
                2 -> "high_speed"
                else -> "auto_medium_speed"
            })
        }
    }

    override fun commandMode(index: Int) {
        if (commandModeIndex != index) {
            commandModeIndex = index
            takeAction("MODE", when (index) {
                0 -> "cool"
                1 -> "dry_air"
                2 -> "fan"
                else -> "auto_change_over"
            })
        }
    }

    override fun commandTurnOn(isTurnOn: Boolean) {
        if (this.isTurnOn != isTurnOn) {
            this.isTurnOn = isTurnOn
            if (isTurnOn) {
                takeAction("ON", "resume")
            } else {
                takeAction("OFF", "off")
            }
        }
    }

    private fun takeAction(targetAction: String, targetParam: String) {
        save()

        control.mActions?.forEach { action ->
            if (action.action != null && action.action == targetAction) {
                action.getParameters()?.forEach { param ->
                    if (param == targetParam) {
                        action(action, param)
                    }
                }
            }
        }
    }

    override fun action(actionRemoteControl: APActionRemoteControl, param: String) {
        Log.d(actionRemoteControl.name, param)

        val home = APHome()
        home.id = mAppyManager.getDedicatedUnitId(homeId ?: "")

        mAppyManager.commandRemoteControl(home, control, actionRemoteControl, param, {
            mView?.showMessage("Success")
        }) {
            processErrorMessage(it)
        }



    }

    private fun processErrorMessage(error: Throwable) {
        if (error is APError) {
            mView?.showError(error.description ?: error.error ?: error.code?.toString() ?: "Error")
        } else {
            mView?.showError(error.showHttpError())
        }
    }

    private fun save() {
        val map = hashMapOf<String, String>(
                "TEMPERATURE" to "$temperature",
                "IS_TURN_ON" to "$isTurnOn",
                "COMMAND_FAN_INDEX" to "$commandFanIndex",
                "COMMAND_MODE_INDEX" to "$commandModeIndex"
        )
        control.id?.let { mView?.save(homeId, it, map) }
    }

    override fun mapSave(save: Map<String, String>) {
        temperature = try {
            save["TEMPERATURE"]?.toInt() ?: DEFAULT_TEMPERATURE
        } catch (e: NumberFormatException) {
            DEFAULT_TEMPERATURE
        }
        isTurnOn = save["IS_TURN_ON"]?.toBoolean() ?: false
        commandFanIndex = try {
            save["COMMAND_FAN_INDEX"]?.toInt()
        } catch (e: NumberFormatException) {
            null
        }
        commandModeIndex = try {
            save["COMMAND_MODE_INDEX"]?.toInt()
        } catch (e: NumberFormatException) {
            null
        }

        commandFanIndex?.let { mView?.setCommandFanIndex(it) }
        commandModeIndex?.let { mView?.setCommandModeIndex(it) }
    }
}
package com.sansiri.homeservice.model.automation

import com.appy.android.sdk.control.APControl

/**
 * Created by sansiri on 10/10/17.
 */
data class AppyHomeAutomation(
        override var title: String = "",
        override var icon: Int,
        override var type: HomeAutomation.Type,
        val apControl: APControl? = null,
        override var action: Int = 0) : HomeAutomation() {
}
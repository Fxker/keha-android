package com.sansiri.homeservice.ui.facilitybooking.calendar

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.api.facilitybooking.DatesItem
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.Reservation
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.toCalendarHM
import com.sansiri.homeservice.util.toDate
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by sansiri on 10/16/17.
 */
class FacilityBookingCalendarPresenter : BasePresenterImpl<FacilityBookingCalendarContract.View>(), FacilityBookingCalendarContract.Presenter {
    private var mProjectId: String = ""
    private var mFacility: FacilityBooking? = null
    private var mReservation: Reservation? = null
        set(value) {
            mView?.setReservation(value)
            field = value
        }
    private var mMe: Profile? = null
    private var nextAvailableTimeSlot: Date? = null
    override var currentTimeline: List<TimesItem>? = null

    override fun init(projectId: String, facility: FacilityBooking) {
        this.mProjectId = projectId
        this.mFacility = facility

        mView?.showDescription(mFacility?.getContent()?.terms ?: "")
    }

    override fun start() {

    }

    override fun destroy() {
    }


    override fun fetchData() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null && mFacility != null) {
                mCompositeDisposable.add(
                        api.getMe().observe().subscribe({ me ->
                            this.mMe = me
                            mCompositeDisposable.add(
                                    api.getFacilityBookingById(mProjectId, mFacility?.id!!).observe().subscribe({
                                        mReservation = it.reservation
                                        val date = getNearestAvailableDate(it.reservation?.dates)
                                        mView?.setCalendarDate(date)
                                        selectBookingDate(date)
                                        mView?.hideLoading()
                                    }) {
                                        mView?.hideLoading()
                                    }
                            )
                        }) {}
                )
            } else {
                mView?.hideLoading()
                mView?.showUnavailable()
            }
        }
    }

    private fun getNearestAvailableDate(dateItem: List<DatesItem>?): String = run loop@{
        val today = mView!!.getCalendarDate()
        if (today == null) {
            val todayDate = Calendar.getInstance().time

            if (dateItem != null) {
                for (item in dateItem) {
                    if (item.times != null) {
                        for (time in item.times) {
                            time.bookBefore?.let {
                                val bookBefore = parseAppyDate(time.bookBefore)
                                if (bookBefore != null && !time.booked && todayDate.before(bookBefore)) {
                                    time.startTime?.let { nextAvailableTimeSlot = parseAppyDate(time.startTime) }
                                    return@loop item.date ?: todayDate.report("yyyy-MM-dd")
                                }
                            }
                        }
                    }
                }
            }
        }
        return@loop today!!
    }

    override fun selectBookingDate(date: String) {
        doAsync {
            val dateToBook = mReservation?.dates?.find { it.date == date }
            uiThread {
                if (dateToBook != null) {
                    renderBookingDate(dateToBook)
                } else {
                    mView?.showUnavailable()
                }
            }
        }

    }


    private fun renderBookingDate(dateToBook: DatesItem) {
        val now = Calendar.getInstance().time

        try {
            val todayItem = dateToBook.times
            currentTimeline = todayItem
            val lastItem = parseAppyDate(todayItem?.last()?.endTime!!)

            if (lastItem != null) {
                mView?.hideUnavailable()
                mView?.clearTimeLine()
                todayItem.forEach { timeSlot ->
                    timeSlot.startTime?.let {
                        val startBooking = parseAppyDate(timeSlot.startTime)?.toCalendarHM()

                        startBooking?.let {


                            timeSlot.bookBefore?.let {
                                val bookBefore = parseAppyDate(timeSlot.bookBefore)
                                val disable = !(bookBefore != null && bookBefore.after(now))
                                mView?.addSlotToTimeLine(startBooking, disable, nextAvailableTimeSlot)
                            }

                        }
                    }
                }
                mView?.addSlotToTimeLine(lastItem.toCalendarHM())

            } else {
                mView?.showUnavailable()
                return
            }

        } catch (e: ParseException) {
            mView?.showUnavailable()
            return
        }

        dateToBook.times.forEach { timeSlot ->
            if (timeSlot.booked) {
                try {
                    val startBooking = timeSlot.startTime?.let { parseAppyDate(timeSlot.startTime)?.toCalendarHM() }
                    val endBooking = timeSlot.endTime?.let { parseAppyDate(timeSlot.endTime)?.toCalendarHM() }

                    if (startBooking != null && endBooking != null) {
                        val bookBefore = timeSlot.bookBefore?.let { parseAppyDate(timeSlot.bookBefore) }
                        if (bookBefore != null && bookBefore.after(now)) {
                            if (timeSlot.bookedBy != null && mMe != null) {
                                if (timeSlot.bookedBy.email == mMe?.email) {
                                    mView?.bookMine(timeSlot.bookedBy.bookingId, startBooking, endBooking, timeSlot.bookedBy.note)
                                } else if (mMe?.phoneNumber != null && timeSlot.bookedBy.phone == mMe?.phoneNumber) {
                                    mView?.bookMine(timeSlot.bookedBy.bookingId, startBooking, endBooking, timeSlot.bookedBy.note)
                                } else {
                                    mView?.book(startBooking, endBooking)
                                }
                            } else {
                                mView?.book(startBooking, endBooking)
                            }
                        }
                    }
                } catch (e: ParseException) {
                }
            }
        }
    }

    private fun buildDate(date: String?, time: String?): Date? {
        if (date != null && time != null) {
            return "$date $time".toDate("yyyy-MM-dd HH:mm")
        }
        return null
    }

    private fun parseAppyDate(date: String): Date? =
            try {
                date.toDate("yyyy-MM-dd HH:mm:ss Z")
            } catch (e: Exception) {
                null
            }
}
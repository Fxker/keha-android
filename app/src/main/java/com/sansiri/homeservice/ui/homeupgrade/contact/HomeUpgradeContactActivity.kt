package com.sansiri.homeservice.ui.homeupgrade.contact

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.ProgressBar
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.homeupgrade.myorder.HomeUpgradeMyOrderActivity
import com.sansiri.homeservice.ui.main.MainActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_home_upgrade_contact.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class HomeUpgradeContactActivity : BaseActivity<HomeUpgradeContactContract.View, HomeUpgradeContactContract.Presenter>(), HomeUpgradeContactContract.View {
    override var mPresenter: HomeUpgradeContactContract.Presenter = HomeUpgradeContactPresenter()
    private var mSubmitDialog: AlertDialog? = null
    private var mSuccessAnimation: LottieAnimationView? = null
    private var mProgressBar: ProgressBar? = null
    private var mTextUpload: TextView? = null

    companion object {
        val ORDER = "ORDER"
        val UNIT_ID = "UNIT_ID"
        fun start(activity: Activity, orderRequest: ArrayList<HomeUpgradeOrderRequest>, unitId: String) {
            val intent = Intent(activity, HomeUpgradeContactActivity::class.java)
            intent.putParcelableArrayListExtra(ORDER, orderRequest)
            intent.putExtra(UNIT_ID, unitId)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_upgrade_contact)

        setBackToolbar(getString(R.string.contact_info))
        val orderRequest = intent.getParcelableArrayListExtra<HomeUpgradeOrderRequest>(ORDER)
        val unitId = intent.getStringExtra(UNIT_ID)

        buttonSubmit.setOnClickListener {
            mPresenter.submit(editName.text.toString(), editEmail.text.toString(), editTel.text.toString())
        }

        val builder = AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Dialog)
        val view = layoutInflater.inflate(R.layout.dialog_upload, null)
        mProgressBar = view.findViewById(R.id.progressBar)
        mSuccessAnimation = view.findViewById(R.id.success)
        mTextUpload = view.findViewById(R.id.textName)
        builder.setView(view)
        mSubmitDialog = builder.create()

        mPresenter.init(orderRequest, unitId)
        mPresenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(displayname: String, email: String, mobilePhone: String) {
        editName.setText(displayname)
        editEmail.setText(email)
        editTel.setText(mobilePhone)
    }

    override fun showSubmitting() {
        editName.isEnabled = false
        editEmail.isEnabled = false
        editTel.isEnabled = false
        buttonSubmit.hide()

        mTextUpload?.text = "${getString(R.string.processing_order)}..."
        mSubmitDialog?.show()
        mProgressBar?.show()
    }

    override fun showSubmitted() {
        if (mSubmitDialog != null && !mSubmitDialog!!.isShowing) {
            mSubmitDialog!!.show()
        }
        mProgressBar?.hide()
        mTextUpload?.text = getString(R.string.your_order_sent_back_soon)
        mSuccessAnimation?.show()
        mSuccessAnimation?.playAnimation()
        ({ launchHomeScreen() }).withDelay(2500)
    }

    override fun showError(message: String) {
        mSubmitDialog?.hide()
        editName.isEnabled = true
        editEmail.isEnabled = true
        editTel.isEnabled = true
        buttonSubmit.show()

        alert(message, getString(R.string.error_something_went_wrong)) {
            yesButton {}
        }.show()
    }

    override fun getStringFromRes(res: Int): String {
        return getString(res)
    }

    override fun nameError(message: String) {
        editName.error = message
    }

    override fun emailError(message: String) {
        editEmail.error = message
    }

    override fun phoneError(message: String) {
        editTel.error = message
    }

    private fun launchHomeScreen() {
        MainActivity.reload(this)
    }
}

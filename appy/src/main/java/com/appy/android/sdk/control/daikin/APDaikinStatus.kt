package com.appy.android.sdk.control.daikin

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class APDaikinStatus : Parcelable {
    @SerializedName("temperature")
    var temperature: String? = null

    @SerializedName("on_off")
    var onOff: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("fan_speed")
    var fanSpeed: String? = null

    @SerializedName("fan_direction")
    var fanDirection: String? = null

    @SerializedName("operation_mode")
    var operationMode: String? = null

    constructor()

    constructor(source: Parcel) : this() {
        temperature = source.readString()
        onOff = source.readString()
        address = source.readString()
        fanSpeed = source.readString()
        fanDirection = source.readString()
        operationMode = source.readString()
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(temperature)
        dest.writeString(onOff)
        dest.writeString(address)
        dest.writeString(fanSpeed)
        dest.writeString(fanDirection)
        dest.writeString(operationMode)
    }

    companion object CREATOR : Parcelable.Creator<APDaikinStatus> {

        override fun createFromParcel(parcel: Parcel): APDaikinStatus {
            return APDaikinStatus(parcel)
        }

        override fun newArray(size: Int): Array<APDaikinStatus?> {
            return arrayOfNulls(size)
        }
    }
}
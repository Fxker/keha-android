package com.sansiri.homeservice.data.network

import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.auth.ApiAuthRepository
import com.sansiri.homeservice.data.network.auth.ApiAuthService
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.data.network.unauth.ApiUnAuthRepository
import com.sansiri.homeservice.data.network.unauth.ApiUnAuthService
import com.sansiri.homeservice.service.*
import com.sansiri.homeservice.util.Connectivity
import java.util.*

/**
 * Created by sansiri on 10/24/17.
 */
object ApiRepositoryProvider {

    fun provideApiUnAuthRepository(): ApiUnAuthRepository = ApiUnAuthRepository(ApiUnAuthService.create(), getLanguage())

    fun provideOnionShackRepository(): OnionShackRepository = OnionShackRepository(
            "Bearer 6ea2d505-9dea-4f9b-8386-3a82ee55ebeb",
            OnionShackService.create()
    )

    fun provideApiAuthRepository(callback: (ApiAuthRepository?) -> Unit) {
        if (Connectivity.isConnected()) {
            FirebaseAuthManager.getToken({
                callback(ApiAuthRepository(it, ApiAuthService.create(), getLanguage()))
            }, {
                callback(null)
            })
        } else {
            callback(ApiAuthRepository("", ApiAuthService.create(), getLanguage()))
        }
    }

    fun getToken(callback: (String?) -> Unit) {
        FirebaseAuthManager.getToken({ token ->
            callback(token)
        }, {
            callback(null)
        })
    }

    fun getLanguage(): String {
        val locale = Store.getLocale()
        return when(locale) {
            Locale("th") -> "th, en;q=0.9, *;q=0.3"
            Locale.JAPANESE -> "ja, en;q=0.9, *;q=0.3"
            Locale.TRADITIONAL_CHINESE -> "zh-Hant, en;q=0.9, *;q=0.3"
            Locale.SIMPLIFIED_CHINESE -> "zh-Hans, en;q=0.9, *;q=0.3"
            else -> "en, th;q=0.9, *;q=0.3"
        }
    }

}
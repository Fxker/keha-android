package com.sansiri.homeservice.data.network

import com.sansiri.homeservice.BuildConfig
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody

class AppInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = try {
            val newRequest = chain.request().newBuilder()
                    .addHeader("app-version", BuildConfig.VERSION_NAME)
                    .addHeader("app-platform", "ANDROID")
                    .build()

            chain.proceed(newRequest)
        } catch (e: Exception) {
            offlineRequest(chain)
        }

        if (response.code() !in 200..399) {
            val url = chain.request().url()?.url()?.toString() ?: ""
            val code = response.code()
            val body = try {
                response.body()?.string()
            } catch (e: Exception) {
                null
            }

            if (url.contains("/verify/citizen") && code == 409) {

            } else {
                AnalyticProvider.sendError(url, code, body)
            }

            return response.newBuilder().body(ResponseBody.create(response.body()?.contentType(), body)).build()
        }
//        else if (response.code() == 204) {
//            response.newBuilder().body(ResponseBody.create(MediaType.parse("application/json"), "{}")) .build()
//        }

        return response

    }

    private fun offlineRequest(chain: Interceptor.Chain): Response {
        val offlineRequest = chain.request().newBuilder()
                .header("Cache-Control", "public, only-if-cached," +
                        "max-stale=" + 60 * 60 * 24)
                .build()

        return chain.proceed(offlineRequest)
    }
}
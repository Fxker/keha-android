package com.sansiri.homeservice.model.api.meter.water

import android.os.Parcelable
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class SmartWaterMeterDetail(
	val volume: Double,
	val volumeUsage: Double,
	val batt: Double,
	val battUsage: Double,
	val rssi: Double,
	val cwuUsage: Double,
	val twuUsage: Double,
	val meterId: String? = null,
	val flowRate: Double?,
	val timestamp: String? = null
): Parcelable {
	val _timestamp: Date?
		get() {
			return try {
				if (this.timestamp == null) return null

				DateTime(this.timestamp).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	val displayVolume: String
		get() {
			return volume.toCurrencyFormat()
		}

	val displayVolumeUsage: String
		get() {
			return volumeUsage.toCurrencyFormat()
		}
}

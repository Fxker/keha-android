package com.sansiri.homeservice.data

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.auth.ApiAuthRepository

/**
 * Created by oakraw on 9/12/2017 AD.
 */
class DataManager {
    private fun getAuthApi(callback:(ApiAuthRepository?) -> Unit) = ApiRepositoryProvider.provideApiAuthRepository(callback)

    fun getHome() {
        getAuthApi {

        }
    }
}
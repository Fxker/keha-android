package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.data.network.RetrofitFactory
import com.sansiri.homeservice.model.SuggestionRequest
import com.sansiri.homeservice.model.api.*
import com.sansiri.homeservice.model.api.announcement.FamilyAnnouncement
import com.sansiri.homeservice.model.api.announcement.ProjectAnnouncement
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.downpayment.Downpayment
import com.sansiri.homeservice.model.api.facilitybooking.CategoryData
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBookingRequest
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.model.api.homecare.HomeCareRequest
import com.sansiri.homeservice.model.api.homecare.ListHomeCareResponse
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDetail
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDetailSubmit
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.model.api.myaccount.BalanceV2
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.model.api.partner.svvh.SVVHPromotion
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterResponse
import com.sansiri.homeservice.model.api.partner.trendywash.*
import com.sansiri.homeservice.model.api.tutorial.TutorialPage
import com.sansiri.homeservice.model.menu.HomeCareMenu
import com.sansiri.homeservice.model.menu.Section
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*
import vc.siriventures.hsa.admin.model.api.FileRequest
import vc.siriventures.hsa.admin.model.api.FileResponse

/**
 * Created by sansiri on 10/24/17.
 */
interface ApiAuthService {
    @GET("customers/bind/{customerId}")
    fun bindUser(@Header("Authorization") idToken: String, @Path("customerId") customerId: String): Observable<ResponseBody>

    @GET("me")
    fun me(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<Profile>

    @GET("me/units")
    fun getAllUnits(@Header("Authorization") idToken: String): Observable<ListResponse<UnitInfo>>

    @GET("me/projects")
    fun getAllProjects(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<ListResponse<ProjectInfo>>

    @GET("me/dynamics/buttons/homes")
    fun getDynamicButton(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<Map<String, List<Section>>>

    @GET("me/today/summary")
    fun getTodaySummary(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<Map<String, ListResponse<TodaySummary>>>

    @GET("/v1/me/unit/{unit_objectid}/balance")
    fun getBalance(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unit_objectid") unitObjectId: String): Observable<Balance>

    @GET("/v1/me/unit/{unit_objectid}/balance/invoice/payable")
    fun getInvoicePayable(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unit_objectid") unitObjectId: String): Observable<List<com.sansiri.homeservice.model.api.Invoice>>

    @GET("me/units/{unitId}/partners/plus/balances")
    fun getBalanceV2(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<BalanceV2>

    @GET("me/units/{unitId}/partners/plus/balances/histories")
    fun getBalanceHistory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<PaymentHistory>>

    @GET("me/units/{unitId}/partners/plus/invoices")
    fun getInvoicePayableV2(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int, @Query("isPaid") isPaid: Boolean = false): Observable<ListResponse<Invoice>>

    @GET("me/units/{unitId}/information")
    fun getMyHome(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<MyHome>

    @GET("me/units/{unitId}/documents")
    fun getMyHomeDocument(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<ListResponse<MyHomeDocument>>

    @GET("me/units/{unitId}/partners/sansiri/documents/transfer")
    fun getTransferDoc(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<List<Transfer>>

    @GET("me/units/{unitId}/partners/sansiri/documents/cost/pdf")
    fun getCostDoc(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<Cost>

    @GET("me/units/{unitId}/partners/sansiri/downpayments")
    fun getDownpayment(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<Downpayment>

    @GET("me/units/{unitId}/partners/sansiri/cr")
    fun getContact(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<Cr>

    @GET("me/partners/sansiri/lounge/qr")
    fun getQR(@Header("Authorization") idToken: String): Observable<Data>

    @GET("me/announcements/family")
    fun getFamilyAnnouncement(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<FamilyAnnouncement>>

    @GET("me/announcements/project")
    fun getAllProjectAnnouncement(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<Map<String, List<ProjectAnnouncement>>>

    @GET("me/announcements/central")
    fun getCentralProjectAnnouncement(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<ProjectAnnouncement>>

    @GET("projects/{projectId}/announcements/project")
    fun getProjectAnnouncement(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("projectId") projectId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<ProjectAnnouncement>>

    @GET("me/units/{unitId}/mailboxes")
    fun getMailBox(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<ListResponse<MailBox>>

    @GET("me/units/{unitId}/mailboxes/histories")
    fun getMailBoxHistory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<MailBox>>

    @PUT("me/units/{unitId}/partners/ibox/information")
    fun registerIBox(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Body request: IBoxInformation): Observable<ResponseBody>

    @GET("me/units/{unitId}/partners/ibox/mailboxes")
    fun getIBox(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<IBoxItem>>

    @GET("me/units/{unitId}/partners/sansiri/defects")
    fun getInspection(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String): Observable<List<Inspection>>

    @Deprecated("use createFileToUpload() instead")
    @POST("/v1/file")
    fun uploadFile(@Header("Authorization") idToken: String, @Body uploadFile: UploadFile): Observable<List<String>>

    @POST("files")
    fun createFileToUpload(@Header("Authorization") idToken: String, @Body fileRequest: FileRequest): Observable<FileResponse>

    @POST("me/units/{unitId}/suggestions")
    fun submitSuggestion(@Header("Authorization") idToken: String, @Path("unitId") unitObjectId: String, @Body request: SuggestionRequest): Observable<ResponseBody>

    @GET("me/units/{unitId}/partners/sansiri/homecare")
    fun getHomeCareHistory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Query("limit") limit: String, @Query("offset") offset: String, @Query("orderBy") orderBy: String?, @Query("statusId") statusId: String?): Observable<ListHomeCareResponse<HomeCareHistory>>

    @POST("me/units/{unitId}/partners/sansiri/homecare")
    fun submitHomeCare(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitObjectId: String, @Body request: HomeCareRequest): Observable<ResponseBody>

    @GET("projects/{projectId}/partners/appy/units/bookings")
    fun getFacilityBooking(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("projectId") projectId: String, @Query("includeCategoryData") includeCategoryData: String): Observable<ListDataResponse<FacilityBooking>>

    @GET("projects/{projectId}/partners/appy/units/bookings/{facilityId}")
    fun getFacilityBookingById(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("projectId") projectId: String, @Path("facilityId") facilityId: String): Observable<DataGeneric<CategoryData>>

    @POST("projects/{projectId}/partners/appy/units/bookings/unit/{unitId}")
    fun submitFacility(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("projectId") projectId: String, @Path("unitId") unitId: String, @Body request: FacilityBookingRequest): Observable<ResponseBody>

    @POST("me/units/{unitId}/partners/appy/facilities/bookings/{bookingId}/cancel")
    fun cancelFacility(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("bookingId") bookingId: String): Observable<ResponseBody>

    @PUT("devices/{deviceId}/notification/register")
    fun registerNotification(@Header("Authorization") idToken: String, @Path("deviceId") deviceId: String, @Body registerNotificationRequest: RegisterNotificationRequest): Observable<ResponseBody>

    @DELETE("devices/{deviceId}/notification/register")
    fun unregisterNotification(@Header("Authorization") idToken: String, @Path("deviceId") deviceId: String): Observable<ResponseBody>

    @POST("notifications/chat/{projectId}/{unitId}")
    fun notifyJuristic(@Header("Authorization") idToken: String, @Path("projectId") projectId: String, @Path("unitId") unitObjectId: String, @Body chat: ChatMessage): Observable<ResponseBody>

    @GET("me/units/{unitId}/upgrades/home/automation/rooms")
    fun getHomeUpgradeList(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<ListResponse<HomeUpgradeRoom>>

    @POST("me/units/{unitId}/upgrades/home/automation/orders")
    fun submitHomeUpgrade(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body request: HomeUpgradeRequest): Observable<ResponseBody>

    @GET("me/units/{unitId}/upgrades/home/automation/orders/latest")
    fun getHomeUpgradeHistory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<HomeUpgradeHistory>

    @GET("me/popups")
    fun getPopUp(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<ListResponse<PopUp>>

    @POST("me/units/{unitId}/partners/teohong/visitors")
    fun submitVisitorAccess(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body request: VisitorAccessRequest): Observable<Code>

    @GET("me/units/{unitId}/partners/teohong/visitors/settings")
    fun getVisitorAccessOptions(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<VisitorAccessOptions>

    @GET("me/units/{unitId}/partners/teohong/visitors/feed/histories")
    fun getVisitorAccessHistory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<VisitorAccessHistory>>

    @GET("me/units/{unitId}/partners/sansiri/maintenances/preventive/plans")
    fun getMaintenanceDevice(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<ListResponse<MaintenanceDevice>>

    @GET("me/units/{unitId}/partners/sansiri/maintenances/preventive/plans/near")
    fun getNearestMaintenancePlan(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<ListResponse<MaintenancePlan>>

    @GET("me/units/{unitId}/partners/sansiri/maintenances/preventive/plans/{planId}")
    fun getMaintenancePlan(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("planId") planId: String): Observable<ListResponse<MaintenancePlan>>

    @GET("me/units/{unitId}/partners/sansiri/maintenances/preventive/accessories/{accessoryId}")
    fun getMaintenanceAccessory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("accessoryId") accessoryid: String): Observable<MaintenanceDetail>

    @PATCH("me/units/{unitId}/partners/sansiri/maintenances/preventive/accessories/{accessoryId}/status")
    fun submitMaintenanceAccessory(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("accessoryId") accessoryid: String, @Body body: MaintenanceDetailSubmit): Observable<ResponseBody>

    @GET("me/google/assistants")
    fun getGoogleAssistant(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String): Observable<ListResponse<GoogleAssistant>>

    @PUT("me/google/assistants/units/{unitId}")
    fun bindGoogleAssistant(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body body: GoogleJwt): Observable<ResponseBody>

    @PATCH("me/google/assistants/{googleId}/units/{unitId}")
    fun updateGoogleAssistant(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("googleId") googleId: String, @Body body: GoogleAssistant): Observable<ResponseBody>

    @DELETE("me/google/assistants/{googleId}/signout")
    fun deleteGoogleAssistant(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("googleId") googleId: String): Observable<Response<Void>>

    @GET("me/units/{unitId}/partners/samitivej/hospital/informations/customer")
    fun getSVVHCustomer(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<SVVHRegisterCustomer>

    @GET("me/units/{unitId}/partners/samitivej/hospital/promotion")
    fun getSVVHPromotion(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<SVVHPromotion>

    @POST("me/units/{unitId}/partners/samitivej/hospital/generates/request")
    fun submitSVVH(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body request: SVVHRegisterCustomer): Observable<SVVHRegisterResponse>

    @POST("me/units/{unitId}/partners/trendyWash/users/register")
    fun registerTrendyWashAccount(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body request: TrendyWashRegisterCustomer): Observable<ResponseBody>

    @POST("me/units/{unitId}/partners/trendyWash/users/login")
    fun loginTrendyWash(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Body request: TrendyWashLogin): Observable<TrendyWashRegisterCustomer>

    @GET("me/units/{unitId}/partners/trendyWash/machines")
    fun getTrendyWashWashingMachines(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String): Observable<List<TrendyWashMachine>>

    @GET("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/credit")
    fun getTrendyWashCredit(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String): Observable<TrendyWashCredit>

    @POST("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/topup/qrpromptpay")
    fun checkoutTrendyWashPaymentQR(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Body request: TrendyWashPaymentGatewayRequest): Observable<TrendyWashPaymentGateWayQR>

    @POST("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/topup/creditcard")
    fun checkoutTrendyWashCreditCard(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Body request: TrendyWashPaymentGatewayRequest): Observable<TrendyWashPaymentGateWayQR>

    @PATCH("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/{machineNo}/start")
    fun startTrendyWashMachine(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Path("machineNo") machineNo: String, @Body request: TrendyWashMachineStart): Observable<TrendyWashMachineStart>

    @GET("me/buttons/{menu}/tutorial")
    fun getTutorial(@Header("Authorization") idToken: String, @Header("Accept-Language") language: String, @Path("menu") menu: String): Observable<List<TutorialPage>>

    companion object Factory {
        fun create(): ApiAuthService {
            return RetrofitFactory.instance.create(ApiAuthService::class.java)
        }
    }
}
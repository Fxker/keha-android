package com.sansiri.homeservice.model.api.meter.electric

import android.os.Parcelable
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class SmartElectricMeterOverAll(
		val average: Summarize? = null,
		val today: Summarize? = null,
		val histories: List<Summarize>? = null,
		val graph: List<SmartElectricMeterDetail>? = null,
		val updatedAt: String? = null
): Parcelable {

	@Parcelize
	data class Summarize(
			val dateType: String? = null,
			val kiloWatt: Double,
			val killWattDisplay: String,
			val costEstimate: Double,
			val costEstimateDisplay: String,
			val title: String? = null,
			val timestamp: String? = null
	): Parcelable {

		companion object DateType{
			val DATE = "DATE"
			val WEEK = "WEEK"
			val MONTH = "MONTH"
		}

		val displayKiloWattHour: String
			get() {
				return "${kiloWatt.toCurrencyFormat()} kWh"
			}

		val displayWatt: String
			get() {
				return "${(kiloWatt * 1000).toCurrencyFormat()} W"
			}

		val displayCostEstimate: String
			get() {
				return "${costEstimate.toCurrencyFormat()}฿"
			}

		val _timestamp: Date?
			get() {
				return try {
					if (this.timestamp == null) return null

					DateTime(this.timestamp).toDate()
				} catch (e: ParseException) {
					null
				}
			}
	}

	val _updatedAt: Date?
		get() {
			return try {
				if (this.updatedAt == null) return null

				DateTime(this.updatedAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}

}

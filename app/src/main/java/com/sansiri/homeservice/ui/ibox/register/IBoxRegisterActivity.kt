package com.sansiri.homeservice.ui.ibox.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.ibox.list.IBoxListActivity
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_ibox_register.*

class IBoxRegisterActivity : BaseActivity<IBoxRegisterContract.View, IBoxRegisterContract.Presenter>(), IBoxRegisterContract.View {

    override var mPresenter: IBoxRegisterContract.Presenter = IBoxRegisterPresenter()
    private lateinit var mHome: Hut
    private var mTitle: String? = null
    private var mPhoneNo: String? = null

    companion object {
        private const val HOME = "HOME"
        private const val TITLE = "TITLE"
        private const val PHONE_NO = "PHONE_NO"

        fun start(activity: Activity, home: Hut, title: String?, phoneNumber: String? = null) {
            val intent = Intent(activity, IBoxRegisterActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            intent.putExtra(PHONE_NO, phoneNumber)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ibox_register)

        mHome = intent.getParcelableExtra(HOME)
        mTitle = intent.getStringExtra(TITLE)
        mPhoneNo = intent.getStringExtra(PHONE_NO)

        setBackToolbar(mTitle ?: "iBOX")
        buttonContinue.setOnClickListener {
            mPresenter.submit(editTel.text.toString())
        }
        mPresenter.init(mHome, mPhoneNo)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("IBOX_REGISTER")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(mobilePhone: String) {
        editTel.setText(mobilePhone)
    }

    override fun showError(message: String) {
    }

    override fun showLoading() {
        progressBar.show()
        buttonContinue.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonContinue.show()
    }

    override fun launchIBoxList() {
        sendEvent("IBOX_REGISTER", "CLICK", "REGISTER")
        IBoxListActivity.start(this, mHome, mTitle)
        finish()
    }
}

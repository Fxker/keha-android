package com.sansiri.homeservice.model.api

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.util.toDate
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class TodaySummary(

        @SerializedName("createdAt")
        val createdAt: String? = null,

        @SerializedName("subTitle")
        val subTitle: String? = null,

        @SerializedName("iconUrl")
        val iconUrl: String? = null,

        @SerializedName("title")
        val title: String? = null,

        @SerializedName("typeName")
        val typeName: String? = null,

        @SerializedName("type")
        val type: String? = null,

        @SerializedName("priority")
        val priority: Int? = null,

        @SerializedName("objectId")
        val objectId: String? = null,

        @SerializedName("isCanDelete")
        val isCanDelete: Boolean? = null,

        @SerializedName("updatedAt")
        val updatedAt: String? = null,

        @SerializedName("html")
        val data: ObjectId? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readString(),
            source.readParcelable<ObjectId>(ObjectId::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(createdAt)
        writeString(subTitle)
        writeString(iconUrl)
        writeString(title)
        writeString(typeName)
        writeString(type)
        writeValue(priority)
        writeString(objectId)
        writeValue(isCanDelete)
        writeString(updatedAt)
        writeParcelable(data, 0)
    }

    val _createdAt: Date?
        get() {
            return try {
                if (this.createdAt == null) return null

                DateTime(this.createdAt).toDate()
            } catch (e: ParseException) {
                null
            }
        }


    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TodaySummary> = object : Parcelable.Creator<TodaySummary> {
            override fun createFromParcel(source: Parcel): TodaySummary = TodaySummary(source)
            override fun newArray(size: Int): Array<TodaySummary?> = arrayOfNulls(size)
        }
    }
}
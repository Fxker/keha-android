package com.appy.android.code.data.rest.adapter

import com.appy.android.code.domain.model.shared.common.DateWithTimeZone
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.appy.android.code.base.data.rest.adapter.GsonDateAdapter
import java.lang.reflect.Type
import java.util.*

/**
 * Created by Nott on 18/9/2560.
 * AppySDK
 */
internal class DateWithTimeZoneAdapter : GsonDateAdapter("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") {

    override fun deserialize(jsonElement: JsonElement, type: Type, jsonDeserializationContext: JsonDeserializationContext): Date {
        val date = super.deserialize(jsonElement, type, jsonDeserializationContext)
        val dateWithTimeZone = DateWithTimeZone()
        dateWithTimeZone.time = date.time

        return dateWithTimeZone
    }
}
package com.sansiri.homeservice.ui.notification

import androidx.lifecycle.LiveData
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface NotificationContract {
    interface Presenter : BasePresenter<View> {
        var notifications: LiveData<List<PushNotification>>
        fun handleNotification(notifications: List<PushNotification>?)
        fun onNotificationClicked(notification: PushNotification)
        fun deleteAll()
    }

    interface View : BaseView {
        fun attachObserver()
        fun bindData(notifications: List<PushNotification>)
        fun showNoData()
        fun launchNotificationAction(notification: PushNotification)
    }
}
package com.appy.android.code.data.rest.adapter

import com.appy.android.code.base.data.rest.adapter.BaseGsonAdapter
import com.appy.android.code.base.data.rest.result.AppyApiApiStatusResult
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.code.domain.model.ControlFactory
import com.appy.android.sdk.control.ControlsApiResult
import com.google.gson.*
import java.lang.reflect.Type

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */

internal class ControlAdapter : BaseGsonAdapter(), JsonDeserializer<ControlsApiResult> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ControlsApiResult? {

        val getControls = ControlsApiResult()

        val jsonObject = json as JsonObject
        if (jsonObject.has("status")) {
            getControls.status = context.deserialize<AppyApiStatusResult>(jsonObject.get("status"), AppyApiStatusResult::class.java)
        }
        if (jsonObject.has("api_status")) {
            getControls.apiStatus = context.deserialize<AppyApiApiStatusResult>(jsonObject.get("api_status"), AppyApiApiStatusResult::class.java)
        }
        if (jsonObject.has("data")) {
            val datas = jsonObject.getAsJsonArray("data")
            for (i in 0..(datas.size() - 1)) {
                val controlJsonModel = context.deserialize<ControlJsonModel>(datas[i], ControlJsonModel::class.java)
                ControlFactory.create(controlJsonModel)?.let {
                    getControls.datas.add(it)
                }
            }
        }
        return getControls
    }
}
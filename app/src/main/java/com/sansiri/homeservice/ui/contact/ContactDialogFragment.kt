package com.sansiri.homeservice.ui.contact

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.GeneralDialogFragment
import com.sansiri.homeservice.model.api.Cr
import com.sansiri.homeservice.util.alertError

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.dialog_contact_us.*
import kotlinx.android.synthetic.main.dialog_general.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

/**
 * Created by sansiri on 10/16/17.
 */
class ContactDialogFragment() : GeneralDialogFragment(), ContactDialogContract.View {
    val mPresenter = ContactDialogPresenter()

    override val onClickListener: () -> Unit = {
    }

    companion object {
        val UNIT_ID = "UNIT_ID"
        fun newInstance(unitObjectId: String): ContactDialogFragment {
            val fragment = ContactDialogFragment()
            val bundle = Bundle()
            bundle.putString(UNIT_ID, unitObjectId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_contact_us, null)
        mPresenter.attachView(this)
        panelContent.addView(view)
        buttonSubmit.hide()

        buttonTel.setOnClickListener {
            launchPhoneCall(textTel.text.toString())
            sendEvent("CONTACT_US", "CLICK", "TEL")
        }
        buttonMail.setOnClickListener {
            launchEmail(textEmail.text.toString())
            sendEvent("CONTACT_US", "CLICK", "EMAIL")
        }

        val unitId = arguments?.getString(UNIT_ID)
        mPresenter.start(unitId ?: "")

    }

    override fun onResume() {
        super.onResume()
        sendScreenView("CONTACT_US")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    override fun bindData(crTransfer: Cr.Transfer?) {
        if (!crTransfer?.imageUri.isNullOrEmpty()) {
            Glide.with(this).loadCache(crTransfer?.imageUri!!, imageProfile)
        }
        textName.text = crTransfer?.name
        textTel.text = crTransfer?.telephone
        textEmail.text = crTransfer?.email
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
        dismiss()
    }

    fun launchPhoneCall(tel: String) {
        try {
            val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
            startActivity(phoneIntent)
        } catch (e: ActivityNotFoundException) {
            showError("Your device doesn't support this feature.")
        }
    }

    fun launchEmail(email: String) {
        try {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$email"))
            startActivity(emailIntent)
        } catch (e: ActivityNotFoundException) {
            showError("Your device doesn't support this feature.")
        }
    }
}
package com.appy.android.code.data.rest.adapter

import com.appy.android.code.base.data.rest.adapter.BaseGsonAdapter
import com.appy.android.code.base.data.rest.result.AppyApiApiStatusResult
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult
import com.appy.android.code.data.rest.JsonModel.RoomJsonModel
import com.appy.android.code.domain.model.ControlFactory
import com.appy.android.sdk.room.RoomApiResult
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

/**
 * Created by ssa-dev-4 on 7/11/2560.
 */
internal class RoomAdapter : BaseGsonAdapter(), JsonDeserializer<RoomApiResult> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): RoomApiResult? {

        val getRoom = RoomApiResult()

        val jsonObject = json as JsonObject
        if (jsonObject.has("status")) {
            getRoom.status = context.deserialize<AppyApiStatusResult>(jsonObject.get("status"), AppyApiStatusResult::class.java)
        }
        if (jsonObject.has("api_status")) {
            getRoom.apiStatus = context.deserialize<AppyApiApiStatusResult>(jsonObject.get("api_status"), AppyApiApiStatusResult::class.java)
        }
        if (jsonObject.has("data")) {
            val data = jsonObject.getAsJsonObject("data")
            val roomJsonModel = context.deserialize<RoomJsonModel>(data, RoomJsonModel::class.java)
            getRoom.datas?.id = roomJsonModel.id
            getRoom.datas?.title = roomJsonModel.title
            getRoom.datas?.titleAlternate = roomJsonModel.titleAlternate
            getRoom.datas?.icon?.original = roomJsonModel.icon?.original
            getRoom.datas?.icon?.xxLarge = roomJsonModel.icon?.xxLarge
            getRoom.datas?.icon?.xLarge = roomJsonModel.icon?.xLarge
            getRoom.datas?.icon?.large = roomJsonModel.icon?.large
            getRoom.datas?.icon?.medium = roomJsonModel.icon?.medium
            getRoom.datas?.icon?.small = roomJsonModel.icon?.small
            getRoom.datas?.icon?.thumb = roomJsonModel.icon?.thumb
            getRoom.datas?.background?.original = roomJsonModel.background?.original
            getRoom.datas?.background?.xxLarge = roomJsonModel.background?.xxLarge
            getRoom.datas?.background?.xLarge = roomJsonModel.background?.xLarge
            getRoom.datas?.background?.large = roomJsonModel.background?.large
            getRoom.datas?.background?.medium = roomJsonModel.background?.medium
            getRoom.datas?.background?.small = roomJsonModel.background?.small
            getRoom.datas?.background?.thumb = roomJsonModel.background?.thumb

            roomJsonModel.controls?.let {
                it.forEach {
                    ControlFactory.create(it)?.let {
                        getRoom.datas?.controls?.add(it)
                    }
                }
            }
        }

        return getRoom
    }
}
package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
internal class SendCommandCoolAutomationTemperatureJsonBody : BaseCommandJsonBody<String>() {
    @SerializedName("command_params")
    var commandParam: String? = null
}
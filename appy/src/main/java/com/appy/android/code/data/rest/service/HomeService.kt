package com.appy.android.code.data.rest.service

import com.appy.android.code.data.rest.params.ParamsMap
import com.appy.android.code.domain.model.command.ac.AirConditionerCommandJsonBody
import com.appy.android.code.domain.model.command.blind.SendCommandBlindJsonBody
import com.appy.android.code.domain.model.command.coolautomation.*
import com.appy.android.code.domain.model.command.daikin.SendCommandDaikinCommandValueJsonBody
import com.appy.android.code.domain.model.command.dimmer.SendCommandDimmerJsonBody
import com.appy.android.code.domain.model.command.light.SendCommandLightJsonBody
import com.appy.android.code.domain.model.command.outlet.SendCommandOutletJsonBody
import com.appy.android.code.domain.model.command.remotecontrol.SendCommandRemoteControlJsonBody
import com.appy.android.code.domain.model.command.scenario.SendCommandScenarioJsonBody
import com.appy.android.code.domain.model.command.toggleswitch.SendCommandToggleSwitchJsonBody
import com.appy.android.sdk.control.CommandApiResult
import com.appy.android.sdk.control.ControlsApiResult
import com.appy.android.sdk.home.HomeApiResult
import com.appy.android.sdk.home.HomesApiResult
import com.appy.android.sdk.room.RoomApiResult
import com.appy.android.sdk.room.RoomsApiResult
import com.appy.android.sdk.status.blind.StatusBlindApiResult
import com.appy.android.sdk.status.daikin.StatusDaikinApiResult
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import com.appy.android.sdk.status.light.StatusLightApiResult
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal interface HomeService {
    @GET("homes")
    fun getHomes(
            @HeaderMap headerMap: Map<String, String>?,
            @QueryMap query: ParamsMap?
    ): Observable<HomesApiResult>

    @GET("homes/{home_id}")
    fun getHome(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<HomeApiResult>

    @GET("homes/{home_id}/controls")
    fun getControls(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/rooms/{id}")
    fun getRoom(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") roomId: String,
            @QueryMap query: ParamsMap?
    ): Observable<RoomApiResult>

    @GET("homes/{home_id}/rooms")
    fun getRooms(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<RoomsApiResult>

    @GET("homes/{home_id}/air_conditioners")
    fun getAirConditioners(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/blinds")
    fun getBlinds(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/cool_automations")
    fun getCoolAutoMations(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/dimmers")
    fun getDimmers(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/lights")
    fun getLights(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/outlets")
    fun getOutlets(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/scenarios")
    fun getScenarios(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/switches")
    fun getSwitchs(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/remote_controls")
    fun getRemoteControls(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @GET("homes/{home_id}/daikins")
    fun getDaikins(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @QueryMap query: ParamsMap?
    ): Observable<ControlsApiResult>

    @POST("homes/{home_id}/air_conditioners/{id}/run")
    fun sendCommandAC(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") acId: String,
            @Body airConditionerCommandJsonBody: AirConditionerCommandJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/blinds/{id}/run")
    fun sendCommandBlind(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") blindId: String,
            @Body sendCommandBlindJsonBody: SendCommandBlindJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/cool_automations/{id}/run")
    fun sendCommandCoolAutomationControl(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") coolId: String,
            @Body sendCommandCoolAutomationJsonBody: SendCommandCoolAutomationJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/cool_automations/{id}/run")
    fun sendCommandCoolAutomationParamControl(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") coolId: String,
            @Body sendCommandCoolAutomationParamJsonBody: SendCommandCoolAutomationParamJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/cool_automations/{id}/run")
    fun sendCommandCoolAutomationTemperature(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") coolId: String,
            @Body sendCommandCoolAutomationTemperatureJsonBody: SendCommandCoolAutomationTemperatureJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/cool_automations/{id}/run")
    fun sendCommandCoolAutomationFanSpeed(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") coolId: String,
            @Body sendCommandCoolAutomationFanSpeedJsonBody: SendCommandCoolAutomationFanSpeedJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/cool_automations/{id}/run")
    fun sendCommandCoolAutomationFanSwing(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") coolId: String,
            @Body sendCommandCoolAutomationFanSwingJsonBody: SendCommandCoolAutomationFanSwingJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/daikins/{id}/run")
    fun sendCommandDaikin(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") daikinId: String,
            @Body sendCommandDaikinCommandValueJsonBody: SendCommandDaikinCommandValueJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/dimmers/{id}/run")
    fun sendCommandDimmer(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") dimmerId: String,
            @Body sendCommandDimmerJsonBody: SendCommandDimmerJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/lights/{id}/run")
    fun sendCommandLight(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") lightId: String,
            @Body sendCommandLightJsonBody: SendCommandLightJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/outlets/{id}/run")
    fun sendCommandOutlet(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") outletId: String,
            @Body sendCommandOutletJsonBody: SendCommandOutletJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/remote_controls/{id}/run")
    fun sendCommandRemoteControl(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") remoteControlId: String,
            @Body sendCommandJsonBody: SendCommandRemoteControlJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/scenarios/{id}/run")
    fun sendCommandScenario(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") scenarioId: String,
            @Body sendCommandScenarioJsonBody: SendCommandScenarioJsonBody
    ): Observable<CommandApiResult>

    @POST("homes/{home_id}/switches/{id}/run")
    fun sendCommandToggleSwitch(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") switchId: String,
            @Body sendCommandToggleSwitchJsonBody: SendCommandToggleSwitchJsonBody
    ): Observable<CommandApiResult>

    @GET("homes/{home_id}/blinds/{id}/status")
    fun getStatusBlind(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") blindId: String,
            @QueryMap query: ParamsMap?
    ): Observable<StatusBlindApiResult>

    @GET("homes/{home_id}/daikins/{id}/status")
    fun getStatusDaikin(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") daikinId: String,
            @QueryMap query: ParamsMap?
    ): Observable<StatusDaikinApiResult>

    @GET("homes/{home_id}/dimmers/{id}/status")
    fun getStatusDimmer(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") dimmerId: String,
            @QueryMap query: ParamsMap?
    ): Observable<StatusDimmerApiResult>

    @GET("homes/{home_id}/lights/{id}/status")
    fun getStatusLight(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") lightId: String,
            @QueryMap query: ParamsMap?
    ): Observable<StatusLightApiResult>

    @GET("homes/{home_id}/switches/{id}/status")
    fun getStatusToggleSwitch(
            @HeaderMap headerMap: Map<String, String>?,
            @Path("home_id") homeId: String,
            @Path("id") switchId: String,
            @QueryMap query: ParamsMap?
    ): Observable<StatusToggleSwitchApiResult>
}
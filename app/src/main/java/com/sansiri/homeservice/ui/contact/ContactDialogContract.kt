package com.sansiri.homeservice.ui.contact

import com.sansiri.homeservice.model.api.Cr
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/30/17.
 */
interface ContactDialogContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
    }

    interface View : BaseView {
        fun bindData(crTransfer: Cr.Transfer?)
        fun showLoading()
        fun hideLoading()
    }
}
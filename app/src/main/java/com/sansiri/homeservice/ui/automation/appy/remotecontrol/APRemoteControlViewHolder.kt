package com.sansiri.homeservice.ui.automation.appy.remotecontrol

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_home_automation_remote_control_action.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class APRemoteControlViewHolder(val view: View, val itemClick: (APActionRemoteControl) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: APActionRemoteControl) {
        with(view) {
            textTitle.text = data.name
            panelClickable.setOnClickListener {
                itemClick(data)
            }
        }
    }
}
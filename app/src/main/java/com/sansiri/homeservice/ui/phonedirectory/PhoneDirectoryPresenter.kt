package com.sansiri.homeservice.ui.phonedirectory

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/30/17.
 */
class PhoneDirectoryPresenter : BasePresenterImpl<PhoneDirectoryContract.View>(), PhoneDirectoryContract.Presenter {
    private lateinit var mProjectId: String

    override fun start(projectId: String) {
        this.mProjectId = projectId
        fetchData()
    }

    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData() {
        mView?.showLoading()

        mCompositeDisposable.add(ApiRepositoryProvider.provideApiUnAuthRepository()
                .getPhoneDirectory(mProjectId)
                .observe()
                .subscribe({
                    mView?.hideLoading()
                    if (it.isEmpty()) {
                        mView?.showNoData()
                    } else {
                        mView?.bindData(it)
                    }
                }) {
                    mView?.hideLoading()
                    mView?.showError(it.showHttpError())
                }
        )
    }
}
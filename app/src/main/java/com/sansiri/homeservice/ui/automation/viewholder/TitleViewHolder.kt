package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import com.sansiri.homeservice.util.hide
import kotlinx.android.synthetic.main.view_title_extend.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class TitleViewHolder(val view: View) : LifecycleViewHolder(view) {
    fun bind(title: String) {
        with(view) {
            if (title.isNotEmpty()) {
                textTitle.text = title
            } else {
                textTitle.setPadding(0,0,0,0)
                textTitle.hide()
            }
        }
    }
}
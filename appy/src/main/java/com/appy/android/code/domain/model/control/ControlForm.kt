package com.appy.android.code.domain.model.control

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
internal open class ControlForm(
        internal val homeId: String
) : AppyForm()

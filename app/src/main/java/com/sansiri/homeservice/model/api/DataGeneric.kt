package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 11/30/17.
 */
data class DataGeneric<T>(val data: T)
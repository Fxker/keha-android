package com.sansiri.homeservice.ui.epayment.barcode

import com.sansiri.homeservice.ui.base.BasePresenterImpl

class QRPresenter : BasePresenterImpl<QRContract.View>(), QRContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }
}
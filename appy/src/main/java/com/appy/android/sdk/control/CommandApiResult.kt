package com.appy.android.sdk.control

import com.appy.android.code.base.data.rest.result.AppyApiResult
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult
import com.google.gson.JsonObject

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
class CommandApiResult : AppyApiResult<Any>() {

    init {
        this.status = AppyApiStatusResult()
        this.data = JsonObject()
    }

}
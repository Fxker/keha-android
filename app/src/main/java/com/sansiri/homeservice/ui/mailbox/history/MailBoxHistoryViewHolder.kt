package com.sansiri.homeservice.ui.mailbox.history

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class MailBoxHistoryViewHolder(val view: View, val itemClick: (MailBox) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: MailBox) {
        with(view) {
            textType.text = data.typeDisplayText
            textTitle.text = data.to
            if (data.details != null) {
                textSubtitle.text = "${data.details}\n${context.getString(R.string.picked_up_by)} ${data.recipient}\n" +
                        "${context.getString(R.string.picked_up_on)} ${data._deliveredAt?.report()}"
            }
            else
                textSubtitle.hide()
            imageIcon.setImageResource(R.drawable.ic_mail_box)
            textDate.text = data.objectId
            root.setOnClickListener {
                itemClick(data)
            }
        }
    }
}
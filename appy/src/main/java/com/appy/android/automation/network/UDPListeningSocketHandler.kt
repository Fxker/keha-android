package com.appy.android.automation.network

import android.content.Context
import android.net.wifi.WifiManager
import android.net.wifi.WifiManager.MulticastLock
import android.util.Log
import com.appy.android.automation.manager.AutomationManager

import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket

class UDPListeningSocketHandler(internal var context: Context, address: String, port: Int) : SocketHandler() {

    override var message: String? = null
        set(value: String?) {
            super.message = value
        }

    internal var socket: MulticastSocket? = null
    internal var group: InetAddress? = null
    internal var multicastLock: MulticastLock? = null

    protected var onConnectedListener: OnConnectedListener? = null
    protected var onTerminatedListener: OnTerminatedListener? = null
    protected var onReceivedResponseListener: OnReceivedResponseListener? = null

    init {
        this.address = address
        this.port = port
    }

    override fun run() {
        try {
            val wifi = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
            multicastLock = wifi.createMulticastLock("multicastLock")
            multicastLock!!.setReferenceCounted(true)
            multicastLock!!.acquire()

            socket = MulticastSocket(port)
            socket!!.joinGroup(InetAddress.getByName(address))

            socket!!.broadcast = true
            socket!!.reuseAddress = true
            socket!!.soTimeout = 0

            if (onConnectedListener != null) {
                onConnectedListener!!.onConnected(this)
            }
        } catch (e: Exception) {
            e.printStackTrace()

            terminate(SocketHandler.Companion.CONNECTION_ERROR)

            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }
        }

    }

    override fun terminate() {
        super.terminate()

        try {
            if (socket != null) {
                socket!!.leaveGroup(group)
                socket!!.close()
                socket = null
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (multicastLock != null) {
                multicastLock!!.release()
                multicastLock = null
            }

            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }

            onTerminatedListener = null
            onReceivedResponseListener = null
        }

    }

    fun listening(message: String?) {
        scheduleTimeOut()

        Log.i("Dev", "Listening")

        try {
            if (message != null) {
                val buffer = message.toByteArray()

                val messagePacket = DatagramPacket(
                        buffer,
                        buffer.size,
                        InetAddress.getByName("255.255.255.255"),
                        8760)
                //                socket.joinGroup(InetAddress.getByName(address));
                socket!!.send(messagePacket)
                socket!!.reuseAddress = true
            }

            val buf = ByteArray(1024 * 1024)
            val recv = DatagramPacket(buf, buf.size)
            socket!!.receive(recv)
            socket!!.reuseAddress = true

            if (onReceivedResponseListener != null) {
                val response = String(buf).trim { it <= ' ' }
                onReceivedResponseListener!!.onReceivedResponse(this, response)
            }
        } catch (e: Exception) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_READ)
        }

    }

    interface OnConnectedListener {
        fun onConnected(socketHandler: UDPListeningSocketHandler)
    }

    interface OnTerminatedListener {
        fun onTerminated(socketHandler: UDPListeningSocketHandler)
    }

    interface OnReceivedResponseListener {
        fun onReceivedResponse(socketHandler: UDPListeningSocketHandler, response: String)
    }
}
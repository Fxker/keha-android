package com.sansiri.homeservice.ui.ibox.list

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.IBoxInformation
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class IBoxListPresenter : BasePresenterImpl<IBoxListContract.View>(), IBoxListContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetchData(unitObjectId: String) {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                mCompositeDisposable.add(
                        it.getIBox(unitObjectId, 0, 10).observe().subscribe({
                            mView?.hideLoading()
                            if (it.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                mView?.bindData(it)
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }
}
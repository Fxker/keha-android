package com.sansiri.homeservice.ui.washingmachine.trendywash.home

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.ui.base.BasePresenterImpl

class TrendyWashHomePresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TrendyWashHomeContract.View>(), TrendyWashHomeContract.Presenter {
    private var mUnitId: String? = null
    private var mUserId: String? = null

    override var machines: MutableLiveData<Resource<List<TrendyWashMachine>>>? = null
    override var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>? = null

    override fun init(unitId: String, userId: String) {
        mUnitId = unitId
        mUserId = userId
    }

    override fun start() {
        fetchMachines()
        fetchCredit()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }


    override fun fetchMachines(isUpdate: Boolean) {
        if (mUnitId != null && mUserId != null) {
            machines = repository.getTrendyWashMachines(mUnitId!!, mUserId!!)
        }
    }

    override fun fetchCredit(isUpdate: Boolean) {
        if (mUnitId != null && mUserId != null) {
            creditInfo = repository.getTrendyWashCredit(mUnitId!!, mUserId!!)
        }
    }

    override fun findMachineById(machineNo: String?) {
        if (machineNo != null) {
            val machine = machines?.value?.data?.find { machine -> machine.machineNo == machineNo }
            if (machine != null) {
                mView?.launchMachine(machine)
                return
            }
        }
        mView?.showErrorNoTitle("Machine not found")
    }
}
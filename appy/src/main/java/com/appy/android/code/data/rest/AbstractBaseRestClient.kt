package com.appy.android.code.data.rest

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.google.gson.Gson
import com.minor.bemynt.helpers.ConstantHelper
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

/**
 * Created by gregoire barret on 11/12/15.
 * For Stroll Guam project.
 */
abstract class AbstractBaseRestClient(protected var context: Context) {
    protected var httpClientBuilder: OkHttpClient.Builder
    protected var retrofitBuilder: Retrofit.Builder

    init {

        // Log
//         val interceptor = HttpLoggingInterceptor()
//         interceptor.level = HttpLoggingInterceptor.Level.BODY
//         httpClientBuilder = OkHttpClient.Builder().addInterceptor(interceptor)

        // No log
        httpClientBuilder = OkHttpClient.Builder()

        retrofitBuilder = Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
    }

    abstract fun getGson(): Gson

    abstract fun getBaseUrl(): String

    open fun <S> createService(serviceClass: Class<S>): S {
        return createService(serviceClass, null, null)
    }

    open fun <S> createService(serviceClass: Class<S>, username: String?, password: String?): S {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val authToken = Credentials.basic(username!!, password!!)
            return createService(serviceClass, AuthenticationInterceptor("Authorization", authToken))
        }

        return createService(serviceClass, null)
    }

    open fun <S> createService(serviceClass: Class<S>, authenticationInterceptor: AuthenticationInterceptor?): S {
        authenticationInterceptor?.let {
            val iter = httpClientBuilder.interceptors().iterator()
            while (iter.hasNext()) {
                val interceptor = iter.next()

                if (interceptor is AuthenticationInterceptor) {
                    iter.remove()
                }
            }

            httpClientBuilder.addInterceptor(it)
        }

        retrofitBuilder.baseUrl(ConstantHelper.getControlApiDomain())
        retrofitBuilder.client(httpClientBuilder.build())
        return retrofitBuilder.build().create(serviceClass)
    }

    class AuthenticationInterceptor(internal var key: String, internal var authToken: String) : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()
            val requestBuilder = original
                    .newBuilder()
                    .header(key, authToken)
            val request = requestBuilder.build()

            Log.i("SSA", "Request Header = " + request.headers().toString())
            return chain.proceed(request)
        }
    }

    companion object {

        val TAG = "RestClient"
    }
}

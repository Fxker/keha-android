package com.sansiri.homeservice.ui.announcement.content

import android.util.Log
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

/**
 * Created by sansiri on 3/13/18.
 */
class ContentPresenter : BasePresenterImpl<ContentContract.View>(), ContentContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData(announcementId: String) {
        ApiRepositoryProvider.provideApiUnAuthRepository().getAnnouncementById(announcementId).observe().subscribe({
            mView?.renderAnnouncement(it)
        }){
            Log.e("ContentPresenter", it.message, it)
        }
    }
}
package com.sansiri.homeservice.ui.web

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.webkit.*
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo






class SmartWebViewClient(val activity: Activity, val extraRedirect: ((String, String?) -> Unit)? = null) : WebViewClient() {
    val REDIRECT = "hsaevent"
    var fragment: androidx.fragment.app.Fragment? = null

    constructor(fragment: androidx.fragment.app.Fragment, extraRedirect: ((String, String?) -> Unit)? = null) : this(fragment.activity!!, extraRedirect) {
        this.fragment = fragment
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

        return super.shouldOverrideUrlLoading(view, request)
    }

//
    override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
        return super.shouldInterceptRequest(view, request)
    }

//    private fun interceptRequest(url: String): WebResourceResponse? {
//        try {
//            val httpClient = OkHttpClient()
//            val requestBuilder = Request.Builder()
//                    .url(url.trim())
//
//            if (token != null) {
//                requestBuilder.addHeader("Authorization", token)
//            }
//
//            val request = requestBuilder.build()
//
//            val response = httpClient.newCall(request).execute()
//
//            return WebResourceResponse(
//                    null,
//                    "UTF-8",
//                    response.body().byteStream()
//            )
//        } catch (e: Exception) {
//            return null
//        }
//    }


    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        val uri = Uri.parse(url)
        val redirectParam = uri.getQueryParameter(REDIRECT)
        if (redirectParam != null) {
            extraRedirect?.invoke(redirectParam, url)
            return true
        }

        if (url.startsWith("http:") || url.startsWith("https:")) {
            if (fragment != null) {
                WebActivity.start(activity, url, "")
                return true
            } else {
                return super.shouldOverrideUrlLoading(view, url)
            }
        } else if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mailto:") || url.startsWith("mms:") || url.startsWith("mmsto:")) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity.startActivity(intent)
            return true
        } else if (url.startsWith("intent:") ) {
            val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
            if (intent != null) {
                val packageManager = activity.getPackageManager()
                val info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
                if (info != null) {
                    activity.startActivity(intent)
                } else {
                    val fallbackUrl = intent.getStringExtra("browser_fallback_url")
                    view.loadUrl(fallbackUrl)
                }
            }
            return true
        } else {
            try {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                activity.startActivity(intent)

                if (fragment == null) {
                    activity.finish()
                }
            } catch (e: Exception) {
            }
            return true
        }
    }

}
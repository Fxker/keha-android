package com.sansiri.homeservice.model.api.chat

data class ChatTemplate (
        val titleEn: String? = null,
        val titleTh: String? = null,
        val messageEn: String? = null,
        val messageTh: String? = null
)
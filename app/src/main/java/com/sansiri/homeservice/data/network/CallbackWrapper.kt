package com.sansiri.homeservice.data.network

import com.sansiri.homeservice.ui.base.BaseView

import org.json.JSONObject

import java.io.IOException
import java.lang.ref.WeakReference
import java.net.SocketTimeoutException

import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import retrofit2.HttpException

abstract class CallbackWrapper<T>(view: BaseView) : DisposableObserver<T>() {
    //BaseView is just a reference of a View in MVP
    private val weakReference: WeakReference<BaseView> = WeakReference(view)

    protected abstract fun onSuccess(t: T)

    override fun onNext(t: T) {
        //You can return StatusCodes of different cases from your API and handle it here. I usually include these cases on BaseResponse and iherit it from every Response
        onSuccess(t)
    }

    override fun onError(e: Throwable) {
        //    BaseView view = weakReference.get();
        //    if (e instanceof HttpException) {
        //      ResponseBody responseBody = ((HttpException)e).response().errorBody();
        //      view.onUnknownError(getErrorMessage(responseBody));
        //    } else if (e instanceof SocketTimeoutException) {
        //      view.onTimeout();
        //    } else if (e instanceof IOException) {
        //      view.onNetworkError();
        //    } else {
        //      view.onUnknownError(e.getMessage());
        //    }
    }

    override fun onComplete() {

    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        try {
            val jsonObject = JSONObject(responseBody.string())
            return jsonObject.getString("message")
        } catch (e: Exception) {
            return e.message ?: ""
        }

    }
}
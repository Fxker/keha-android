package com.appy.android.automation.helper

class ErrorMessageHelper {
    companion object {
        const val AUTHENTICATION_FAILED = "Authentication failed. Please try again."
        const val CONNECTION_FAILED = "Connection failed. Please try again."
        const val CONNECTION_TIMEOUT = "Connection time out. Please try again."
        const val DISCONNECT_BY_PEER = "Socket disconnected by peer. Please try again."
        const val UNKNOWN_CONNECTION = "Something went wrong. Please try again."
    }
}
package com.sansiri.homeservice.ui.chat

import android.util.Log
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.data.network.firebase.FireStoreManager
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.chat.ChatMessage.Companion.TYPE_IMAGE
import com.sansiri.homeservice.model.api.chat.ChatMessage.Companion.TYPE_TEXT
import com.sansiri.homeservice.model.api.chat.Sender
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.time.ZoneId
import java.util.*

/**
 * Created by sansiri on 10/18/17.
 */
class ChatPresenter : BasePresenterImpl<ChatContract.View>(), ChatContract.Presenter {
    private var home: Hut? = null
    private var unitId: String? = null
    var loadFirstConversation = true
    private var fireStore: FireStoreManager? = null

    override fun init(home: Hut) {
        this.home = home
        // fireStore = FireStoreManager("2145", "2145-099999")
        fireStore = FireStoreManager(home.projectId, home.unitId)
    }

    override fun start() {
        mView?.hideChatTemplateButton()

        fireStore?.attachChatRoom() {
            Log.d("", "")
            if (loadFirstConversation) {
                loadFirstConversation = false
                mView?.addMessage(it)
            } else {
                mView?.addMessageAt(it, 0)
            }
        }

        fireStore?.getChatTemplateDefault({ chatTemplates ->
            mView?.enableChatTemplateButton()
            mView?.bindChatTemplate(chatTemplates)
            mView?.showChatTemplateButton()
        }) {
            mView?.disableChatTemplateButton()
        }

        checkTimeToShowNotice()
    }

    override fun destroy() {
        fireStore?.detach()
    }

    private fun checkTimeToShowNotice() {
        val bkkTimeZone = DateTimeZone.forID("Asia/Bangkok")
        val openTime = DateTime.now(bkkTimeZone).withTime(9,0,0,0)
        val closeTime = DateTime.now(bkkTimeZone).withTime(17,30,0,0)
        val now = DateTime.now(bkkTimeZone)

        if (now.isBefore(openTime.millis) || now.isAfter(closeTime.millis)) {
            mView?.showNotice()
        } else {
            mView?.hideNotice()
        }
    }

    override fun sendMessage(message: String) {
        val myMessage = ChatMessage(TYPE_TEXT, DateTime.now(DateTimeZone.UTC).toString(), Sender(Sender.TYPE_USER), message, null)
        //val othersMessage = ChatMessage(MESSAGE_OTHER, MockData.getChatMessage())
        //mView?.addMessage(myMessage)
        fireStore?.sendMessage(myMessage, {
            home?.let { fireStore?.notifyJuristic(it, myMessage) }
        }) {
            mView?.showSendMessageError()
        }
    }

    override fun sendMessage(message: ChatMessage) {
        fireStore?.sendMessage(message, {
            if (message.sender.type == Sender.TYPE_USER) {
                home?.let { fireStore?.notifyJuristic(it, message) }
            }
        }) {
            mView?.showSendMessageError()
        }
    }

    override fun upload(fileName: String, imageFile: ByteArray) {
        mView?.showUploadProgress()
        fireStore?.uploadImage(fileName, imageFile, { progress ->
            mView?.updateUploadProgress(progress)
        }, {
            mView?.hideUploadProgress()
            sendPhoto(it.toString())
        }) {
            mView?.hideUploadProgress()
            mView?.showUploadError()
        }
    }

    override fun sendPhoto(imageUrl: String) {
        val myMessage = ChatMessage(TYPE_IMAGE, DateTime.now(DateTimeZone.UTC).toString(), Sender(Sender.TYPE_USER), null, imageUrl)
        fireStore?.sendMessage(myMessage, {

        }) {
            mView?.showSendMessageError()
        }
    }

    override fun loadMore() {
        fireStore?.loadMoreConversation({
            mView?.addMessage(it)
        }) {}
    }
}
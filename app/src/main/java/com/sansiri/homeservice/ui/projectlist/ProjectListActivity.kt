package com.sansiri.homeservice.ui.projectlist

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.util.withDelay
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_project_list.*

class ProjectListActivity : AppCompatActivity() {

    val mAdapter = ProjectListAdapter() { hut ->
        val intent = Intent()
        intent.putExtra(HOME, hut.unitId)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    companion object {
        val HOME = "HOME"
        val SELECTED_HOME = "SELECTED_HOME"
        val CODE = 10
        fun start(activity: Activity, homes: ArrayList<Hut>, selectedHome: Hut? = null) {
            val intent = Intent(activity, ProjectListActivity::class.java)
            val hut = arrayListOf<Hut>()
            hut.addAll(homes.map { Hut(it.unitId, it.title, it.icon, it.image, it.address, it.projectId) })
            intent.putParcelableArrayListExtra(HOME, hut)
            intent.putExtra(SELECTED_HOME, selectedHome)
            activity.startActivityForResult(intent, CODE)
        }

        fun start(fragment: Fragment, homes: ArrayList<Hut>, selectedHome: Hut? = null) {
            val intent = Intent(fragment.activity, ProjectListActivity::class.java)
            val hut = arrayListOf<Hut>()
            hut.addAll(homes.map { Hut(it.unitId, it.title, it.icon, it.image, it.address, it.projectId) })
            intent.putParcelableArrayListExtra(HOME, hut)
            intent.putExtra(SELECTED_HOME, selectedHome)
            fragment.startActivityForResult(intent, CODE)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_list)

        val homes = intent?.getParcelableArrayListExtra<Hut>(HOME)
        val selectedHome = intent?.getParcelableExtra<Hut>(SELECTED_HOME)

        with(list) {
            adapter = mAdapter
            layoutManager = CenterLayoutManager(this@ProjectListActivity)
            if (homes != null) {
                mAdapter.setData(homes)
            }
//            itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
//            itemAnimator?.addDuration = 400
//            if (homes != null) {
//                mAdapter.setDataDelay(homes)
//            }
        }



        if (homes != null && selectedHome != null) {
            val targetHomePosition = homes.indexOfFirst { home -> home.unitId == selectedHome.unitId }
            ({
                list.smoothScrollToPosition(targetHomePosition)
            }).withDelay(100)
        }

    }
}

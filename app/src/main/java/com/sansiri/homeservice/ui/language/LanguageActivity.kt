package com.sansiri.homeservice.ui.language

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper.IS_FIRST_TIME
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.welcome.WelcomeActivity
import kotlinx.android.synthetic.main.activity_language.*
import com.sansiri.homeservice.data.database.PreferenceHelper.defaultPrefs
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.service.AppFirebaseInstanceIdService
import com.sansiri.homeservice.util.alertError
import java.util.*

class LanguageActivity : BaseActivity<LanguageContract.View, LanguageContract.Presenter>(), LanguageContract.View {

    override var mPresenter: LanguageContract.Presenter = LanguagePresenter()

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, LanguageActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)

        buttonEnglish.setOnClickListener {
            // RegisterActivity.start(this)
            mPresenter.selectEnglish()
            sendEvent("LANGUAGE", "SELECT", "ENGLISH")
        }

        buttonThai.setOnClickListener {
            mPresenter.selectThai()
            sendEvent("LANGUAGE", "SELECT", "THAI")
        }

        buttonJapanese.setOnClickListener {
            mPresenter.selectJapanese()
            sendEvent("LANGUAGE", "SELECT", "JAPANESE")
        }

        buttonChineseSimplified.setOnClickListener {
            mPresenter.selectChineseSimplified()
            sendEvent("LANGUAGE", "SELECT", "CHINESE_SIMPLIFIED")
        }

        buttonChineseTraditional.setOnClickListener {
            mPresenter.selectChineseTraditional()
            sendEvent("LANGUAGE", "SELECT", "CHINESE_TRADITIONAL")
        }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("LANGUAGE")
    }

    override fun setAppLanguage(locale: Locale){
        RuntimeCache.clear()
        setLanguage(locale)
        val pref = defaultPrefs(this)
        if (pref[IS_FIRST_TIME, true]!!) {
            pref[IS_FIRST_TIME] = false
            WelcomeActivity.start(this)
        }
        finish()
        registerNotification(locale)
    }

    override fun showError(message: String) {
        alertError(message)
    }


    private fun registerNotification(locale: Locale) {
        val deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        AppFirebaseInstanceIdService.sendRegistrationToServer(deviceId, locale)
    }
}

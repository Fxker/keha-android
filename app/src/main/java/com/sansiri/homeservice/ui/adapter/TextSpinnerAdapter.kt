package com.sansiri.homeservice.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.reportTime
import com.sansiri.homeservice.util.toDate

class TextSpinnerAdapter<T>(context: Context, val res: Int, val objects: List<Pair<T, String>>) : ArrayAdapter<Pair<T, String>>(context, res, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(res, parent, false)
        view?.findViewById<TextView>(android.R.id.text1)?.text = objects[position].second
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)
        view?.findViewById<TextView>(android.R.id.text1)?.text = objects[position].second
        return view
    }
}
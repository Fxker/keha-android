package com.sansiri.homeservice.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by oakraw on 10/3/2017 AD.
 */
@Parcelize
data class Hut(
        var unitId: String = "",
        var title: String = "",
        var icon: String = "",
        var image: String = "",
        var address: String = "",
        var projectId: String = "",
        var lat: Double = 0.0,
        var lng: Double = 0.0
) : Parcelable
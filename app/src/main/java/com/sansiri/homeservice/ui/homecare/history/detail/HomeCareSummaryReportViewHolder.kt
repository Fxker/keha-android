package com.sansiri.homeservice.ui.homecare.history.detail

import android.graphics.Color
import android.view.View
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_homecare_summary_report_card.view.*

class HomeCareSummaryReportViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: HomeCareHistory) {
        with(view) {
            SummaryReportTitle.text = data.typeName
            SummaryReportName.text = data.detail
            SummaryReportStatus.text = data.statusName
            SummaryReportDate.text = data.createdDate?.report("yyyy.MM.dd") ?: "-"
            SummaryReportAcceptedDate.text = data.acceptedDate?.report("yyyy.MM.dd") ?: "-"
            SummaryReportUpdatedDate.text = data.updatedDate?.report("yyyy.MM.dd") ?: "-"
            Card.setCardBackgroundColor(Color.parseColor(data.statusBgColor))
        }
    }
}
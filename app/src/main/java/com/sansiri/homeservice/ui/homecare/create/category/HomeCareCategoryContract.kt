package com.sansiri.homeservice.ui.homecare.create.category

import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by oakraw on 10/2/2017 AD.
 */
interface HomeCareCategoryContract {
    interface Presenter: BasePresenter<View> {
        fun loadCategories()
        fun selectCategory(menu: Menu)
    }

    interface View: BaseView {
        fun bindCategories(categories: List<Menu>)
        fun nextScreen(menu: Menu)
        fun showLoading()
        fun hideLoading()
    }
}
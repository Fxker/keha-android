package com.sansiri.homeservice.ui.electric.mea

import com.sansiri.homeservice.data.network.RuntimeCache.isMeaConsentAccepted
import com.sansiri.homeservice.data.repository.mea.MeaRepository
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign

class MEAElectricMainPresenter(val repository: MeaRepository) : BasePresenterImpl<MEAElectricMainContract.View>(), MEAElectricMainContract.Presenter {
    private var mHome: Hut? = null

    override fun init(home: Hut?) {
        this.mHome = home
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
    }

    override fun fetch() {
        mView?.showLoading()
        if (!isMeaConsentAccepted) {
            mCompositeDisposable += repository.getConsent().observe().subscribe({ response ->
                mView?.hideLoading()
                if (response.code() == 204) {
                    checkCA()
                    isMeaConsentAccepted = true
                } else {
                    response.body()?.let { mView?.launchConsent(it) }
                }

            }) {
                mView?.hideLoading()
                mView?.showError(it.showHttpError())
            }
        } else {
            checkCA()
        }
    }

    override fun checkCA() {
        mHome?.let { home ->
            mView?.showLoading()
            repository.getMEAInfo(home.unitId).observe().subscribe({ meaInfo ->
                mView?.hideLoading()
                if (meaInfo.contractAccountId == null) {
                    mView?.launchLogin(home, meaInfo)
                } else {
                    mView?.launchHome(meaInfo.contractAccountId)
                }
            }) {
                mView?.hideLoading()
                mView?.showError(it.showHttpError())
            }
        }
    }
}
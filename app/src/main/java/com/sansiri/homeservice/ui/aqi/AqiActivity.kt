package com.sansiri.homeservice.ui.aqi

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.robinhood.ticker.TickerUtils
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.aqi.Air
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_aqi.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class AqiActivity : BaseActivity<AqiContract.View, AqiContract.Presenter>(), AqiContract.View {
    override var mPresenter: AqiContract.Presenter = AqiPresenter()

    var mHome: Hut? = null

    companion object {
        val HOME = "HOME"
        fun start(activity: Activity, home: Hut) {
            val intent = Intent(activity, AqiActivity::class.java)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    private val weatherAdapter = AirWeatherAdapter()
    private val pollutantAdapter = AirPollutantAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aqi)

        buttonBack.setOnClickListener {
            finish()
        }

        mHome = intent.getParcelableExtra(HOME)
        if (mHome != null) {
            mPresenter.init(mHome!!.projectId)
            mPresenter.start()
        } else {
            showError(getString(R.string.error_default_message))
        }

        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.padding_extra_extra_large)
        val gridItemDecoration = GridSpacingItemDecoration(2, spacingInPixels, false, 0)

        listWeather.addItemDecoration(gridItemDecoration)
        listWeather.adapter = weatherAdapter
        listPollutant.addItemDecoration(gridItemDecoration)
        listPollutant.adapter = pollutantAdapter

        textPm25Amount.setCharacterLists(TickerUtils.provideNumberList())
    }

    override fun bindData(air: Air) {
        textLocation.text = air.placeName ?: mHome?.title
        air._updatedAt?.let { textUpdateTime.text = "${it.report()} - ${it.reportTime()}" }

        if (air.aqi != null) {
            textPm25Amount.apply {
                textColor = Color.parseColor(air.aqi.color)
                setText(air.aqi.valueDisplay, true)
            }
            textPm25Description.text = air.aqi.description
            viewPm25Indicator.backgroundColor = Color.parseColor(air.aqi.color)
        } else {
            layoutPm25.hide()
        }

        if (!air.weathers.isNullOrEmpty()) {
            weatherAdapter.setData(air.weathers)
        } else {
            layoutWeather.hide()
        }

        if (!air.pollutants.isNullOrEmpty()) {
            pollutantAdapter.setData(air.pollutants)
        } else {
            layoutPollutant.hide()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("AIR_QUALITY_DETAIL")
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        layoutError.show()
        snackError(message)
    }

}

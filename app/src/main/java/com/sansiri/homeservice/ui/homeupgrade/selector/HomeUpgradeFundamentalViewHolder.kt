package com.sansiri.homeservice.ui.homeupgrade.selector

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.cardview.widget.CardView
import androidx.appcompat.widget.DrawableUtils
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_home_upgrade_check.view.*
import kotlinx.android.synthetic.main.view_menu_card_no_title.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class HomeUpgradeFundamentalViewHolder(val view: View, val itemSelected: (HomeUpgradeHardware) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: HomeUpgradeHardware) {
        with(view) {
            textTitle.text = data.name
            textDescription.text = data.description
            Glide.with(this).loadCache(data.imageUrl ?: "", imageLogo as ImageView)
            root.setOnClickListener {  }
            toggleLayout.addRelatedChild(textTitle, textDescription, imageLogo)
            toggleLayout.isChecked = true

            checkbox.hide()
            data.amount = 1
            itemSelected(data)
        }
    }
}
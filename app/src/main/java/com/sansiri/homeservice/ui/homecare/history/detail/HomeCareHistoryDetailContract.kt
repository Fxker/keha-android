package com.sansiri.homeservice.ui.homecare.history.detail;

import com.sansiri.homeservice.ui.base.BasePresenter;
import com.sansiri.homeservice.ui.base.BaseView

public interface HomeCareHistoryDetailContract {

        interface Presenter: BasePresenter<View>{
        }

        interface View: BaseView {
        }
}

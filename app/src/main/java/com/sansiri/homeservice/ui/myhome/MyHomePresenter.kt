package com.sansiri.homeservice.ui.myhome

import android.util.Log
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class MyHomePresenter : BasePresenterImpl<MyHomeContract.View>(), MyHomeContract.Presenter {
    private var mHome: Hut? = null

    override fun init(home: Hut) {
        this.mHome = home
    }

    override fun start() {
        if (mHome != null) {
            fetch()
            mView?.bindBasicInfo(mHome!!)
        }
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null && mHome != null) {
                mView?.showLoading()
                mCompositeDisposable.add(api.getMyHome(mHome!!.unitId).observe().subscribe({
                    mView?.hideLoading()
                    mView?.bindAdvanceInfo(it)
                }) {
                    mView?.hideLoading()
                    val message = if (it.localizedMessage.isNotEmpty()) {
                        it.localizedMessage
                    } else {
                        it.showHttpError()
                    }
                    mView?.showError(message)
                })
                mCompositeDisposable.add(
                        api.getMyHomeDocument(mHome!!.unitId).observe().subscribe({
                            mView?.bindDocument(it)
                        }) {
                        })
            }
        }
    }

    override fun requestToGoogleMap() {
        mHome?.let { mView?.launchGoogleMap(it.lat, it.lng) }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
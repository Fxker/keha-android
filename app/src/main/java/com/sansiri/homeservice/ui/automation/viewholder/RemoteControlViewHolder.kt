package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import kotlinx.android.synthetic.main.view_home_automation_remote_control_action.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class RemoteControlViewHolder(val view: View, val itemClick: (HomeAutomation, Any) -> Unit) : LifecycleViewHolder(view) {
    fun bind(data: HomeAutomation) {
        with(view) {
            textTitle.text = data.title
            panelClickable.setOnClickListener {
                itemClick(data, 0)
            }
        }
    }
}
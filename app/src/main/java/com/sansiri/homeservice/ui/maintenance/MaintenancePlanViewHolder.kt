package com.sansiri.homeservice.ui.maintenance

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.TimeUtils
import android.util.TypedValue
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_summary_menu.view.*
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by sansiri on 10/12/17.
 */
class MaintenancePlanViewHolder(val view: View, val itemClick: (MaintenancePlan) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: MaintenancePlan) {
        with(view) {
            textType.text = data.productDescription
            textDate.text = data._planDatetime?.report()
            textTitle.text = data.accessoryName
            textSubtitle.setTextColor(context.resources.getColor(R.color.colorSecondary))
            textSubtitle.text = data._planDatetime?.timeDiffMessage(context)

            textType.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_body1))
            textDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_body1))
            textSubtitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_caption))

            Glide.with(this).loadCache(data.imagePath ?: "", imageIcon)
            root.setOnClickListener {
                itemClick(data)
            }
        }
    }
}
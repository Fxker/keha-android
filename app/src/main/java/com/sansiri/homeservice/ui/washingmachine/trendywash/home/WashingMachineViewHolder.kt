package com.sansiri.homeservice.ui.washingmachine.trendywash.home

import android.os.CountDownTimer
import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.ui.washingmachine.trendywash.displayTime
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_card.view.*
import org.jetbrains.anko.textColor
import java.util.concurrent.TimeUnit

/**
 * Created by sansiri on 10/11/17.
 */
class WashingMachineViewHolder(val view: View, val itemClick: (TrendyWashMachine) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    var countDownTimer: CountDownTimer? = null

    fun bind(data: TrendyWashMachine) {
        with(view) {
            imageIcon?.setImageResource(data.localIcon)
//            textTitle.setTextColor(context.resources.getColor(color))
//            textTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_display3))

            textDate?.hide()

//            data.status = 1
//            data.remainTime = Random.nextInt(5, 10)

            textSubtitle?.text = String.format(context.getString(R.string.price_x_baht), data.price)


            textTitle?.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_display1))
            textSubtitle?.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_body2))
            textType?.text = data.machineName

            root?.setOnClickListener {
                itemClick(data)
            }

            when (data.status) {
                TrendyWashMachine.STATUS_AVAILABLE -> renderAvailableStatus()
                else -> {
                    renderBusyStatusWithTime(data.remainTime)
                    startCountDown(data)
                }
            }
        }
    }

    fun renderBusyStatusWithTime(second: Int) {
        view.apply {
            textTitle?.text = "${resources.getString(R.string.busy)} (${displayTime(second)})"
            textTitle?.textColor = resources.getColor(R.color.textSubTitle2)
        }
    }

    fun renderAvailableStatus() {
        view.apply {
            textTitle?.text = resources.getString(R.string.available)
            textTitle?.textColor = resources.getColor(R.color.textAccent)
        }
    }


    private fun startCountDown(machine: TrendyWashMachine) {
        countDownTimer?.cancel()

        countDownTimer = object : CountDownTimer(TimeUnit.SECONDS.toMillis(machine.remainTime.toLong()), 1000) {
            override fun onFinish() {
                machine.status = TrendyWashMachine.STATUS_AVAILABLE
                renderAvailableStatus()
            }

            override fun onTick(millisUntilFinished: Long) {
                val remainTime = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished).toInt()
                machine.remainTime = remainTime
                renderBusyStatusWithTime(remainTime)
            }

        }

        countDownTimer?.start()
    }
}
package com.sansiri.homeservice.ui.more.googleassistant

import android.util.Log
import com.google.android.gms.common.api.Api
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.HomeGoogleAssistant
import com.sansiri.homeservice.model.api.GoogleAssistant
import com.sansiri.homeservice.model.api.GoogleJwt
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

//import kotlinx.coroutines.rx2.await

class GoogleAssistantPresenter : BasePresenterImpl<GoogleAssistantContract.View>(), GoogleAssistantContract.Presenter {
    private var homes: List<Home>? = null
    private var mGoogleAssistant: GoogleAssistant? = null
    private var mGoogleToken: String? = null
    private var mUnitId: String? = null

    override fun start() {
        fetch()
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                mCompositeDisposable.add(
                        api.getHomeless().observe().subscribe({ homes ->
                            this.homes = homes
                            api.getGoogleAssistant().observe().subscribe({ googleAssistants ->
                                mView?.hideLoading()
                                if (googleAssistants.isEmpty()) {
                                    mView?.showUnregister()
                                    if (homes.isNotEmpty()) {
                                        selectUnit(homes[0].unitId)
                                    }
                                } else {
                                    googleAssistants[0].let {
                                        mGoogleAssistant = it
                                        mUnitId = it.unitId

                                        if (mUnitId != null) {
                                            selectUnit(mUnitId!!)
                                        }

                                        if (it.email != null) {
                                            mView?.showGoogleLoggedIn(it.email)
                                        }
                                    }
                                }
//                        googleAssistant = googleAssistants[0]
                            }) {
                                mView?.hideLoading()
                                mView?.showError(it.showHttpError())
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }

    override fun requestForSelectUnit() {
        if (homes != null) {
            mView?.launchProjectSelector(homes!!)
        }
    }

    override fun selectUnit(unitId: String) {
        mUnitId = unitId

        val home = homes?.find { item -> item.unitId == unitId }
        home?.let { mView?.bindData(home) }
    }

    override fun onGoogleLoggedIn(idToken: String?, email: String?) {
        mGoogleToken = idToken

        mView?.showGoogleLoggedIn(email)
    }

    override fun validateAndSubmit() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                val observable = if (mUnitId != null && mGoogleToken != null) {
                    mView?.showSyncing()
                    api.bindGoogleAssistant(mUnitId!!, GoogleJwt(mGoogleToken!!))
                } else if (mUnitId != null && mGoogleAssistant != null) {
                    mView?.showSyncing()
                    api.updateGoogleAssistant(mUnitId!!, mGoogleAssistant?.googleId!!, mGoogleAssistant!!)
                } else {
                    null
                }

                val disposable = observable?.observe()?.subscribe({
                    mView?.hideSyncing()
                    mView?.showSuccess()
                }) {
                    mView?.hideSyncing()
                    mView?.showError(it.showHttpError())
                }

                disposable?.let { mCompositeDisposable.add(disposable) }
            }
        }
    }

    override fun logout() {
        if (mGoogleAssistant != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mView?.showSyncing()
                    api.deleteGoogleAssistant(mGoogleAssistant!!.googleId!!).observe().subscribe({
                        mView?.hideSyncing()
                        mView?.showUnregister()
                        if (!homes.isNullOrEmpty()) {
                            selectUnit(homes!![0].unitId)
                        }
                    }) {
                        mView?.hideSyncing()
                        mView?.showError(it.showHttpError())
                    }
                }
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
package com.sansiri.homeservice.ui.myaccount

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Balance
import com.sansiri.homeservice.model.api.Invoice
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.synthetic.main.activity_my_account.*

class MyAccountActivity : BaseActivity<MyAccountContract.View, MyAccountContract.Presenter>(), MyAccountContract.View {
    override var mPresenter: MyAccountContract.Presenter = MyAccountPresenter()

    companion object {
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"
        val TITLE = "TITLE"
        fun start(activity: Activity, unitObjectId: String, title: String?) {
            val intent = Intent(activity, MyAccountActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val mAdapter = MyAccountAdapter(R.color.colorCancel) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.my_account))

        val unitObjectId = intent.getStringExtra(UNIT_OBJECT_ID)
        unitObjectId?.let { mPresenter.fetchData(unitObjectId) }

        with(listHistory) {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mAdapter
        }

//        mPlanAdapter.setData(listOf(
//                DownPayment("20,000", "Laundry payment", Calendar.getInstance(), getString(R.string.regular)),
//                DownPayment("80,000", "Groceries Shopping: Dean & Deluca", Calendar.getInstance(), getString(R.string.Bullet))
//        ))
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(balance: Balance) {
        textBalance.text = balance.balance.toCurrencyFormat()
    }

    override fun bindList(list: List<Invoice>) {
        titleHistory.show()
        listHistory.show()
        mAdapter.setData(list)
    }

    override fun showError(message: String) {
        textBalance.text = "Error"
        alertError(message)
    }
}
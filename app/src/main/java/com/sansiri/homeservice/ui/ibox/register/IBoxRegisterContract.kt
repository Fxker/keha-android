package com.sansiri.homeservice.ui.ibox.register

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface IBoxRegisterContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun fetchProfileSuccess(phoneNumber: String)
        fun submit(phoneNumber: String)
        fun init(home: Hut, phoneNumber: String? = null)
    }

    interface View : BaseView {
        fun bindData(mobilePhone: String)
        fun showLoading()
        fun hideLoading()
        fun launchIBoxList()
    }
}
package com.sansiri.homeservice.data.database.creditcard

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard

@Dao
interface SavingCreditCardDao {
    @Query("SELECT * FROM saving_credit_card")
    fun getSavingCreditCards(): LiveData<List<SavingCreditCard>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCreditCard(creditCard: SavingCreditCard)

    @Query("DELETE FROM saving_credit_card WHERE cardToken = :cardToken")
    suspend fun deleteCreditCard(cardToken: String)

    @Query("DELETE FROM saving_credit_card")
    fun deleteAll()

}

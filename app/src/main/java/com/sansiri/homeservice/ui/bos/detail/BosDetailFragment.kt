package com.sansiri.homeservice.ui.bos.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.ui.base.PhotoV2Fragment
import com.sansiri.homeservice.ui.bos.BosActivity
import com.sansiri.homeservice.ui.bos.BosActivity.Companion.TYPE_UNIT
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_bos_detail.*
import kotlinx.android.synthetic.main.view_progress_status.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import org.koin.androidx.scope.currentScope

class BosDetailFragment : PhotoV2Fragment<BosDetailContract.View, BosDetailContract.Presenter>(), BosDetailContract.View {
    override val mPresenter: BosDetailContract.Presenter by currentScope.inject()

    private var mUnitId: String? = null
    private var mGroupType: Int = 0
    private var mJobType: BosMenu? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val GROUP_TYPE = "GROUP_TYPE"
        const val JOB_TYPE = "JOB_TYPE"

        fun newInstance(unitId: String, groupType: Int, jobType: BosMenu): BosDetailFragment {
            return BosDetailFragment().apply {
                val bundle = Bundle()
                bundle.putString(UNIT_ID, unitId)
                bundle.putInt(GROUP_TYPE, groupType)
                bundle.putParcelable(JOB_TYPE, jobType)
                arguments = bundle
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mGroupType = arguments?.getInt(GROUP_TYPE, 0) ?: 0
        mJobType = arguments?.getParcelable<BosMenu>(JOB_TYPE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bos_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter.maximumSelection = 1

        listPhoto.apply {
            mAdapter.setData(bitmaps)
            adapter = mAdapter
        }

        buttonCreate.setOnClickListener {
            sendEvent("BOS", "CLICK", "SUBMIT")
            mPresenter.submit()
        }

        if (mGroupType == TYPE_UNIT) {
            textArea.hide()
            editTextArea.hide()
            textFloor.hide()
            editTextFloor.hide()
            textBuilding.hide()
            editTextBuilding.hide()
        }

        textSubtitle.text = "${textSubtitle.text} *"
        textName.text = "${textName.text} *"
        textContact.text = "${textContact.text} *"
        textFloor.text = "${textFloor.text} *"
        textArea.text = "${textArea.text} *"

        mPresenter.init(mUnitId, mGroupType, mJobType)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        (activity as BosActivity).setToolbarTitle(mJobType?.title ?: "")
        sendScreenView("BOS_DETAIL")
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as BosActivity).setToolbarTitle()
    }

    override fun bindUser(profile: Profile?) {
        profile?.let {
            editTextName.setText(it.displayname ?: "${profile.firstname} ${profile.lastname}")
            editTextContact.setText(it.phoneNumbers?.get(0)?.tel)
        }
    }

    override fun prepareBitmapToUploadIfExist() {
        if (bitmaps.isNotEmpty()) {
            sendEvent("BOS", "UPLOAD", "PHOTO")

            compressBitmapObservable.observe().subscribe({ imageToUpload ->
                mPresenter.uploadImage(imageToUpload)
            }) {
                showError(getString(R.string.error_fail_to_upload))
            }
        } else {
            mPresenter.createRequest(listOf())
        }
    }

    override fun showUploadProgress(progress: Int, max: Int) {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = "${getString(R.string.uploading)} ($progress/$max)"
    }

    override fun getDetail() = editTextDescription.text.toString()

    override fun getName() = editTextName.text.toString()

    override fun getContact() = editTextContact.text.toString()

    override fun getArea() = if (editTextArea.text.isEmpty()) null else editTextArea.text.toString()

    override fun getFloor() = if (editTextFloor.text.isEmpty()) null else  editTextFloor.text.toString()

    override fun getBuilding() = if (editTextBuilding.text.isEmpty()) null else  editTextBuilding.text.toString()

    override fun hideUploadProgress() {
        panelProgress.hide()
        buttonCreate.show()
        editTextDescription.isEnabled = true
    }

    override fun showSubmitProgress() {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = getString(R.string.submitting)
    }

    override fun tryAgain() {
        editTextDescription.isEnabled = true
        buttonCreate.text = getString(R.string.try_again)
        panelProgress.hide()
        buttonCreate.show()
    }

    override fun showSuccess() {
        progressBar.hide()
        activity?.alert(getString(R.string.bos_confirmation_message), getString(R.string.done)) {
            yesButton {
                val returnIntent = Intent()
                activity?.setResult(Activity.RESULT_OK, returnIntent)
                activity?.finish()
            }
        }?.apply {
            isCancelable = false
            show()
        }
        textProgress.text = getString(R.string.done)
    }


    override fun showAlertToEnterDetail() {
        activity?.snack(getString(R.string.please_fill_in_details))
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}
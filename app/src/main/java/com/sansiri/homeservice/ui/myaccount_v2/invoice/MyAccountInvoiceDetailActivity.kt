package com.sansiri.homeservice.ui.myaccount_v2.invoice

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.downpayment.InvoiceItemAdapter
import com.sansiri.homeservice.ui.myaccount_v2.invoice.payment.MyAccountInvoiceEPaymentBarcodeActivity
import com.sansiri.homeservice.ui.myaccount_v2.invoice.payment.MyAccountInvoiceEPaymentQRActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_my_account_invoice.*
import android.view.LayoutInflater
import android.view.ViewTreeObserver
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.ui.epayment.options.EPaymentOptionsFragment
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.activity_my_account_invoice.textTitle
import kotlinx.android.synthetic.main.view_payment_button.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.koin.androidx.scope.currentScope


class MyAccountInvoiceDetailActivity : BaseV2Activity<MyAccountInvoiceDetailContract.View, MyAccountInvoiceDetailContract.Presenter>(), MyAccountInvoiceDetailContract.View, EPaymentOptionsFragment.OnFragmentInteractionListener {
    override val mPresenter: MyAccountInvoiceDetailContract.Presenter by currentScope.inject()
    private var mHome: Hut? = null
    private var mInvoice: Invoice? = null
    private var mDialogLoading: ProgressDialog? = null

    companion object {
        val HOME = "HOME"
        val INVOICE = "INVOICE"

        fun start(activity: Activity, home: Hut, invoice: Invoice) {
            val intent = Intent(activity, MyAccountInvoiceDetailActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(INVOICE, invoice)
            activity.startActivity(intent)
        }
    }

    val mAdapter = InvoiceItemAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account_invoice)

        listInvoiceItem.apply {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mAdapter
        }

        mInvoice = intent.getParcelableExtra<Invoice>(INVOICE)
        mHome = intent.getParcelableExtra(HOME)

        setBackToolbar(mInvoice?._createdAt?.reportDMYFull() ?: "")

        if (mInvoice != null) {
            mPresenter.init(mInvoice!!)
        }

        // need for view complete to render before view capture
        root.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                root.viewTreeObserver.removeOnGlobalLayoutListener(this)
                mPresenter.start()
            }
        })
    }

    override fun bindData(invoice: Invoice) {

        textTitle.text = "${getString(R.string.invoice_number)} ${invoice.objectId}"
        textDueDate.text = invoice._dueAt?.reportDMYFull()
        textAmount.text = "${invoice.grandTotal.toCurrencyFormat()} ${getString(R.string.baht)}"

        if (invoice.items != null && invoice.items.isNotEmpty()) {
            mAdapter.setData(invoice.items)
        }

        invoice.paymentChannels?.let {
            replaceFragment(EPaymentOptionsFragment.newInstance(
                    invoice.paymentChannels,
                    PaymentGatewayData(
                            paymentTitle = "${getString(R.string.invoice_number)} ${invoice.objectId}",
                            refCode1 = invoice.reference1,
                            refCode2 = invoice.reference2,
                            amount = invoice.amount
                    ),
                    layoutDetail.capture()?.toByteArray(),
                    invoice.paymentBarcode != null,
                    "MY_ACCOUNT_INVOICE"
            ), false
            )
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT_INVOICE_EPAYMENT")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

//    override fun showOtherPaymentOptions(paymentChannels: List<PaymentChannel>) {
//        paymentChannels.forEach { paymentChannel ->
//            val mainColor = try {
//                Color.parseColor(paymentChannel.bgColorRGB)
//            } catch (e: Exception) {
//                null
//            }
//
//            val accentColor = try {
//                Color.parseColor(paymentChannel.textColorRGB)
//            } catch (e: Exception) {
//                null
//            }
//
//            val view = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_payment_button, layoutPaymentOptions, false).apply {
//                if (paymentChannel.title != null) textTitle.text = paymentChannel.title else textTitle.hide()
//                if (paymentChannel.subTitle != null) textSubTitle.text = paymentChannel.subTitle else textSubTitle.hide()
//                if (paymentChannel.iconUrl != null) Glide.with(context).loadCache(paymentChannel.iconUrl, imageIcon) else imageIcon.hide()
//                mainColor?.let { card.setCardBackgroundColor(mainColor) }
//                accentColor?.let {
//                    textTitle.setTextColor(accentColor)
//                    textSubTitle.setTextColor(accentColor)
//                }
//                layoutClickable.setOnClickListener {
//                    sendEvent("DOWNPAYMENT_EPAYMENT", "CLICK", paymentChannel.channel)
//                    mPresenter.takeAction(paymentChannel)
//                }
//            }
//
//            layoutPaymentOptions.addView(view)
//        }
//
//    }
//
//    override fun launchWeb(url: String) {
//        WebActivity.start(this, url, "")
//    }
//
//
//    override fun launchPhoneVerify(invoice: Invoice, paymentChannel: PaymentChannel) {
//        val mainColor = try {
//            Color.parseColor(paymentChannel.bgColorRGB)
//        } catch (e: java.lang.Exception) {
//            null
//        }
//
//        val accentColor = try {
//            Color.parseColor(paymentChannel.textColorRGB)
//        } catch (e: java.lang.Exception) {
//            null
//        }
//
//        if (paymentChannel.command != null) {
//            PaymentGatewayActivity.startPhoneVerify(this, PaymentGatewayData(
//                    command = paymentChannel.command ?: "",
//                    merchantName = paymentChannel.merchantName,
//                    gatewayName = paymentChannel.gatewayName,
//                    paymentTitle = "${getString(R.string.invoice_number)} ${invoice.objectId}",
//                    refCode1 = invoice.reference1,
//                    refCode2 = invoice.reference2,
//                    amount = invoice.amount,
//                    mainThemeColor = mainColor,
//                    accentThemeColor = accentColor,
//                    icon = paymentChannel.iconUrl
//            ))
//        } else {
//            showError(getString(R.string.error_default_message))
//        }
//    }
//
//    override fun showLoading() {
//        mDialogLoading = indeterminateProgressDialog(getString(R.string.loading), null)
//    }
//
//    override fun hideLoading() {
//        mDialogLoading?.dismiss()
//    }
//
//    override fun showPopup(title: String, message: String, onSuccess: () -> Unit) {
//        alert(message, title) {
//            okButton {
//                onSuccess.invoke()
//            }
//        }.show()
//    }

    override fun showError(message: String) {
        alertError(message)
    }

    override fun onBarcodeClicked() {
        sendEvent("MY_ACCOUNT_INVOICE_EPAYMENT", "CLICK", "BARCODE")
        if (mHome != null && mInvoice != null)
            MyAccountInvoiceEPaymentBarcodeActivity.start(this@MyAccountInvoiceDetailActivity, mHome!!, mInvoice!!)
    }

    override fun onQRClicked() {
        sendEvent("MY_ACCOUNT_INVOICE_EPAYMENT", "CLICK", "QR")
        if (mHome != null && mInvoice != null)
            MyAccountInvoiceEPaymentQRActivity.start(this@MyAccountInvoiceDetailActivity, mHome!!, mInvoice!!)
    }
}

package com.appy.android.code.data.rest.error

import com.appy.android.automation.helper.ErrorMessageHelper
import com.appy.android.automation.network.SocketHandler
import com.appy.android.code.base.data.rest.error.APError
import com.appy.android.code.base.data.rest.result.AppyAPIErrorResult
import retrofit2.Response

class APErrorFactory {

    companion object {

        @JvmStatic
        fun create(socketErrorCode: Int): APError {
            var apError: APError = APUnknownErrorException()

            when (socketErrorCode) {
                SocketHandler.AUTHENTICATION_FAILED -> {
                    apError = APAuthenticationFailedException()
                    apError.error = ErrorMessageHelper.AUTHENTICATION_FAILED
                    apError.description = ErrorMessageHelper.AUTHENTICATION_FAILED
                }
                SocketHandler.CONNECTION_ERROR -> {
                    apError = APConnectionFailedException()
                    apError.error = ErrorMessageHelper.CONNECTION_FAILED
                    apError.description = ErrorMessageHelper.CONNECTION_FAILED
                }
                SocketHandler.DISCONNECTED_BY_PEER -> {
                    apError = APGatewayRejectedException()
                    apError.error = ErrorMessageHelper.DISCONNECT_BY_PEER
                    apError.description = ErrorMessageHelper.DISCONNECT_BY_PEER
                }
                SocketHandler.CONNECTION_TIMEOUT -> {
                    apError = APConnectionTimeoutException()
                    apError.error = ErrorMessageHelper.CONNECTION_TIMEOUT
                    apError.description = ErrorMessageHelper.CONNECTION_TIMEOUT
                }
                else -> {
                    apError = APUnknownConnectionErrorException()
                    apError.error = ErrorMessageHelper.UNKNOWN_CONNECTION
                    apError.description = ErrorMessageHelper.UNKNOWN_CONNECTION

                }
            }

            apError.code = socketErrorCode
            return apError
        }

        @JvmStatic
        fun create(errorResult: AppyAPIErrorResult): APError {
            var apError: APError = if (errorResult.status?.code == 400
                && errorResult.data?.error.equals("connection_failed")) {
                APConnectionFailedException()
            } else if (errorResult.status?.code == 400
                && errorResult.data?.error.equals("connection_timeout")) {
                APConnectionTimeoutException()
            } else if (errorResult.status?.code == 400
                && (errorResult.data?.error.equals("unknown_error")
                    || (errorResult.status?.description.isNullOrBlank()))) {
                APConnectionTimeoutException()
            } else if (errorResult.status?.code == 400
                && errorResult.data?.error.equals("gateway_rejected")) {
                APGatewayRejectedException()
            } else if (errorResult.status?.code == 400
                && errorResult.data?.error.equals("authentication_failed")) {
                APAuthenticationFailedException()
            } else if (errorResult.status?.code == 404) {
                APResourceNotFoundException()
            } else if (errorResult.status?.code == 500) {
                APInternalServerErrorException()
            } else {
                APUnknownErrorException()
            }

            apError.code = errorResult.status?.code
            apError.description = errorResult.status?.description
            apError.error = errorResult.data?.error
            return apError
        }

        @JvmStatic
        fun create(response: Response<*>): APError {
            var apError: APError = if (response.code() == 401) {
                APAuthenticationFailedException()
            } else if (response.code() == 404) {
                APResourceNotFoundException()
            } else if (response.code() == 500) {
                APInternalServerErrorException()
            } else {
                APUnknownErrorException()
            }

            apError.code = response.code()
            apError.description = response.message()
            apError.error = response.message()

            return apError
        }
    }
}
package com.sansiri.homeservice.model.api

data class Profile(
        val objectId: String? = null,
        val username: String? = null,
        val displayname: String? = null,
        val email: String? = null,
        val contactEmail: String? = null,
        val firstname: String? = null,
        val lastname: String? = null,
        val phoneNumbers: List<PhoneNumber>? = null,
        val country: String? = null,
        val sansiriCustomerId: String? = null,
        val prospectId: String? = null,
        val createdAt: String? = null,
        val updatedAt: String? = null,
        val prefixName: String? = null,
        val gender: String? = null
) {
    data class PhoneNumber(val tel: String? = null)

    val phoneNumber: String?
        get() = phoneNumbers?.getOrNull(0)?.tel
}

package com.sansiri.homeservice.ui.register.idcard


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.BuildConfig

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.ui.register.federatiallogin.LoginFragment
import com.sansiri.homeservice.util.TextWatcherExtend
import com.sansiri.homeservice.util.replaceFragment
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_citizen_id_verify.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton


/**
 * A simple [Fragment] subclass.
 */
class CitizenIDVerifyFragment : BaseFragment<CitizenIDVerifyContract.View, CitizenIDVerifyContract.Presenter>(), CitizenIDVerifyContract.View {
    override var mPresenter: CitizenIDVerifyContract.Presenter = CitizenIDVerifyPresenter()

    companion object {
        fun newInstance(): CitizenIDVerifyFragment {
            return CitizenIDVerifyFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_citizen_id_verify, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonContinue.setOnClickListener {
            mPresenter.verifyCitizenID(editIDCard.text.toString())
        }
        buttonByPass.setOnClickListener {
            (activity as RegisterActivity).replaceFragment(LoginFragment.newInstance(false), true)
        }

        buttonByPass.hide()

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("ID_VERIFICATION")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showLoading() {
        editIDCard.isEnabled = false
        progressBar.show()
        buttonContinue.hide()
    }

    override fun hideLoading() {
        editIDCard.isEnabled = true
        progressBar.hide()
        buttonContinue.show()
    }

    override fun showPleaseEnterCitizenId() {
        showError(getString(R.string.please_fill_your_id))
    }

    override fun showHelpDialog() {
        activity?.alert(getString(R.string.id_card_not_found)) {
            yesButton {  }
        }?.show()
    }

    override fun showError(message: String) {
        editIDCard.error = message
    }

    override fun launchRegisterScreen(customerId: String) {
        val registerActivity = activity as RegisterActivity
        registerActivity.storeCustomerId(customerId)
        registerActivity.replaceFragment(LoginFragment.newInstance(true), true)
    }

    override fun launchLoginScreen(provider: String?, registeredWithEmailOrName: String?) {
        val registerActivity = activity as RegisterActivity
        registerActivity.replaceFragment(LoginFragment.newInstance(false, provider, registeredWithEmailOrName), true)
    }
}

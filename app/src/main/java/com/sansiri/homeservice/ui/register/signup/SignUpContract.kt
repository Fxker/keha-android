package com.sansiri.homeservice.ui.register.signup

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/17/17.
 */
interface SignUpContract {
    interface Presenter : BasePresenter<View> {
        fun validate(email: String, emailConfirm: String, password: String, passwordConfirm: String)
        fun signUp(email: String, password: String)
    }

    interface View : BaseView {
        fun usernameError(message: String)
        fun emailError(message: String)
        fun passwordError(message: String)
        fun passwordConfirmError(message: String)
        fun showLoading()
        fun hideLoading()
        fun nextScreen()
        fun getStringFromRes(res: Int): String
        fun emailConfirmError(message: String)
    }
}
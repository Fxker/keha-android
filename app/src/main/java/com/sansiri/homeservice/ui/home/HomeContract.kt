package com.sansiri.homeservice.ui.home

import android.location.Location
import androidx.lifecycle.LiveData
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.api.TodaySummary
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.announcement.ProjectAnnouncementDetail
import com.sansiri.homeservice.model.api.partner.aqi.Air
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.model.menu.Section
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/27/17.
 */
interface HomeContract {
    interface Presenter : BasePresenter<View> {
        var currentPosition: Int
        var isAnnouncementShow: Boolean
        fun fetchHome()
        fun selectHome(position: Int)
        fun sortHome(location: Location)
        fun requestHomeAutomationScene(sceneMenu: Menu)
        fun requestHomeAutomation(roomMenu: Menu)
        fun selectMenu(menu: Menu)
        fun prepareForVoiceCommand()
        fun requestMenuNavigation(feature: String, data: PushNotificationData?)
        fun invokeProjectList()
        fun findHome(homeId: String)
        fun reopenPreviousMenuWith(query: String)
        fun requestHomeUpgrade()
        fun requestAqi()
        fun fetchAqi(home: Home)
        fun fetchOrviboRoom(orviboHome: OrviboHome?)
        fun handleNotification(notifications: List<PushNotification>?)
        fun clearNotificationHistory(unitId: String, featureId: String)

        var notifications: LiveData<List<PushNotification>>
    }

    interface View : BaseView {
        fun bindHome(home: List<Home>)
        fun refreshHome(forceUpdate: Boolean = false)
        fun showHomeMissing()
        fun bindHomeAutomationScene(homeAutomation: List<Menu>)
        fun bindHomeAutomation(homeAutomation: List<Menu>)
        fun bindSummary(summary: List<TodaySummary>)
        fun bindSection(sections: List<Section>, update: Boolean = false)
        fun hideSummary()
        fun hideHomeAutomation()
        fun hideSection()
        fun launchOrviboHomeAutomation(title: String, roomId: String)
        fun launchAppyHomeAutomation(title: String, projectId: String, homeId: String, roomId: String)
        fun launchMailbox(home: Home, title: String?)
        fun launchiBOX(home: Home, menu: Menu?)
        fun launchHomeCare(unitObjectId: String, title: String?)
        fun launchHomeCareCreate(unitObjectId: String)
        fun launchDownPayment(home: Home, title: String?)
        fun launchFacilityBooking(home: Home, title: String?)
        fun launchFacilityBookingDetail(title: String, projectId: String, facilityId: String)
        fun launchWeb(url: String, title: String)
        fun launchWebWithToken(url: String, title: String)
        fun launchChat(home: Home, menu: Menu?)
        fun launchPhoneDirectory(projectId: String, title: String?)
        fun launchMyAccount(home: Home, title: String?)
        fun launchInspection(unitObjectId: String, title: String?)
        fun launchAppointment()
        fun launchTransfer(unitObjectId: String, title: String?)
        fun launchContactUs(unitObjectId: String, title: String?)
        fun launchProjectProgress(projectId: String, projectTitle: String, title: String?)
        fun launchSellAndRentOut()
        fun launchAnotherApp(id: String, title: String, packageName: String)
        fun launchMenuService(id: String, title: String, section: List<Section>)
        fun launchSell()
        fun launchSuggestion(unitObjectId: String, title: String?)
        fun launchVisitorAccess(home: Home, title: String?)
        fun bindAnnouncement(announcement: List<Announcement<ProjectAnnouncementDetail>>)
        fun hideAnnouncement()
        fun launchDeepLink(id: String, title: String, uri: String, androidPackageName: String?)
        fun hideHome()
        fun showLoading()
        fun hideLoading()
        fun launchVoiceCommand(home: Home?)
        fun launchPlayStore(packageName: String)
        fun sortHomeByLocation(location: Location?)
        fun scrollToHome(position: Int)
        fun launceProjectPreview(homeList: ArrayList<Hut>, selectedHome: Hut?)
        fun launchMechanicalParking(projectId: String, title: String?)
        fun launchAnnouncement(announcementId: String)
        fun launchHomeUpgrade(unitId: String)
        fun showSessionTimeoutDialog()
        fun showSignOut()
        fun hideSignOut()
        fun signOut()
        fun showLoadingHomeUpgrade()
        fun showHomeUpgrade()
        fun launchMyHome(home: Home, title: String?)
        fun hideHomeUpgrade()
        fun showHomeUpgradeWaiting(history: HomeUpgradeHistory)
        fun launchHomeUpgradeMyOrder(history: HomeUpgradeHistory)
        fun showHomeUpgradeAppointment(history: HomeUpgradeHistory)
        fun saveProfileId(uid: String?)
        fun launchMaintenanceGuide(home: Home, title: String?, planId: String?, accessoryId: String?)
        fun launchAqi(home: Home)
        fun loadingAirData()
        fun bindAirData(air: Air)
        fun errorAirData()
        fun launchDialogToUpdate(function: () -> Unit)
        fun launchSVVH(home: Home, menu: Menu)
        fun launchOrvibo(home: Home)
        fun subscribeOrviboHome(orviboHomeLiveData: LiveData<OrviboHome>)
        fun launchTrendyWash(home: Home, menu: Menu)
        fun launchSmartParking(home: Home, menu: Menu)
        fun showBAtHome(username: String, password: String, projectId: String)
        fun hideBAtHome()
        fun launchSmartWaterMeter(home: Home, menu: Menu)
        fun launchMea(home: Home, menu: Menu): Unit?
        fun launchSmartElectricMeter(home: Home, menu: Menu)
        fun attachObserver()
        fun launchBos(home: Home, menu: Menu)
    }
}
package com.sansiri.homeservice.ui.myaccount_v2.invoice

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class MyAccountInvoicePresenter : BasePresenterImpl<MyAccountInvoiceContract.View>(), MyAccountInvoiceContract.Presenter {
    var mUnitId: String? = null
    var allItemCount = Int.MAX_VALUE

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        if (mUnitId != null) {
            fetch(0)
        }
    }

    override fun fetch(page: Int) {
        val offset = page * 10
        if (offset > allItemCount) return

        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                mCompositeDisposable.add(
                        api.getInvoicePayableV2(mUnitId ?: "", page * 10, 10)
                                .observe()
                                .subscribe({
                                    mView?.hideLoading()

                                    allItemCount = it.count ?: Int.MAX_VALUE

                                    val invoice = it.items

                                    if (allItemCount > 0) {
                                        mView?.bindData(invoice)
                                    } else {
                                        mView?.showNoData()
                                    }
                                }) {
                                    mView?.hideLoading()
                                    mView?.showError(it.localizedMessage)
                                }
                )
            } else {
                mView?.showError("Network Error")
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
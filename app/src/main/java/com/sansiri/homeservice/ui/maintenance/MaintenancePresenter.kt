package com.sansiri.homeservice.ui.maintenance

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.getHttpCode
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.subjects.AsyncSubject
import retrofit2.HttpException

class MaintenancePresenter : BasePresenterImpl<MaintenanceContract.View>(), MaintenanceContract.Presenter {
    var mDevices: List<MaintenanceDevice>? = null
    var mHome: Hut? = null
    val mDeviceObservable: AsyncSubject<List<MaintenanceDevice>> = AsyncSubject.create()

    override fun init(home: Hut) {
        this.mHome = home
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null && mHome != null) {
                mView?.showLoadingDevice()
                mCompositeDisposable.add(
                        api.getMaintenanceDevice(mHome!!.unitId).observe().subscribe({ devices ->
                            mView?.hideLoadingDevice()
                            if (devices.isNotEmpty()) {
                                mDevices = devices
                                mView?.bindDevice(devices)

                            } else {
                                mView?.showEmptyDevice()
                            }

                            mDeviceObservable.onNext(devices)
                            mDeviceObservable.onComplete()
                        }) {
                            mView?.hideLoadingDevice()
                            mView?.showEmptyDevice()
                            if (it.getHttpCode() != 404) {
                                mView?.showError(it.showHttpError())
                            }
                        })

                mView?.showLoadingPlan()
                mCompositeDisposable.add(
                        api.getNearestMaintenancePlan(mHome!!.unitId).observe().subscribe({ plans ->
                            mView?.hideLoadingPlan()
                            if (plans.isNotEmpty()) {
                                mView?.bindPlan(plans)
                            } else {
                                mView?.showEmptyPlan()
                            }
                        }) {
                            mView?.hideLoadingPlan()
                            mView?.showEmptyPlan()
                            if (it.getHttpCode() != 404) {
                                mView?.showError(it.showHttpError())
                            }
                        }
                )
            }
        }


//        mView?.bindPlan(listOf(
//                MaintenancePlan("2",
//                        "ฟิลเตอร์ (แผ่นกรองอากาศ) (Wall Type)",
//                        "ลูกค้าสามารถดำเนินการเองได้",
//                        "ทำความสะอาดทุก 6 เดือน/ครั้ง",
//                        "2019-07-31T00:00:00"
//                )
//        ))
//        mView?.bindPlan(plans)

    }

    override fun requestToLaunchDevice(planId: String) {
        if (mDevices != null) {
            val device = mDevices?.find { device -> device.planId == planId }

            if (mHome != null && device != null) {
                mView?.launchDevice(mHome!!, device)
            }
        } else {

            mView?.let {
                mDeviceObservable.observe().subscribe({ devices ->
                    val device = devices.find { device -> device.planId == planId }
                    if (mHome != null && device != null) {
                        mView?.launchDevice(mHome!!, device)
                    }
                }) {}
            }
        }

    }

    override fun requestToLaunchDetail(plan: MaintenancePlan) {
        if (mHome != null && plan.accessoryId != null) {
            mView?.launchDetail(mHome!!, plan.accessoryId, null, plan)
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
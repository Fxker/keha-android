package com.sansiri.homeservice.model.menu

import com.appy.android.sdk.room.APRoom
import com.sansiri.homeservice.R

/**
 * Created by sansiri on 11/28/17.
 */
class AppyHomeAutomationRoomMenu(
        override val id: String,
        override val icon: String,
        override val title: String
) : Menu {
    override fun getLocalIcon(): Int {
        val target = title.toLowerCase()
        return if (target.contains("bedroom")) {
            R.drawable.ic_bedroom
        } else if (target.contains("kitchen")){
            R.drawable.ic_kitchen
        } else if (target.contains("living")){
            R.drawable.ic_living_room
        } else if (target.contains("balcony")){
            R.drawable.ic_balcony_2
        } else {
            R.drawable.ic_home
        }
    }
}
package com.sansiri.homeservice.model.api.chat

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 12/21/17.
 */
data class ChatMessage(
        val type: String,
        val createdAt: String,
        val sender: Sender,
        val text: String?,
        val imageUrl: String?
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readParcelable<Sender>(Sender::class.java.classLoader),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(type)
        writeString(createdAt)
        writeParcelable(sender, 0)
        writeString(text)
        writeString(imageUrl)
    }

    companion object {
        val TYPE_TEXT = "text"

        val TYPE_IMAGE = "image"

        @JvmField
        val CREATOR: Parcelable.Creator<ChatMessage> = object : Parcelable.Creator<ChatMessage> {
            override fun createFromParcel(source: Parcel): ChatMessage = ChatMessage(source)
            override fun newArray(size: Int): Array<ChatMessage?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.visitor

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.RadioButton
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.bottomsheet.DatePickerBottomSheet
import com.sansiri.homeservice.model.api.TextValue
import com.sansiri.homeservice.ui.adapter.TextSpinnerAdapter
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.visitor.qr.VisitorAccessQRActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_visitor_access.*
import org.jetbrains.anko.collections.forEachWithIndex
import java.util.*
import androidx.core.content.res.ResourcesCompat
import android.graphics.Typeface
import android.util.TypedValue
import android.view.Menu
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.visitor.history.VisitorAccessHistoryActivity


class VisitorAccessActivity : BaseActivity<VisitorAccessContract.View, VisitorAccessContract.Presenter>(), VisitorAccessContract.View {
    override var mPresenter: VisitorAccessContract.Presenter = VisitorAccessPresenter()

    var mHome: Hut? = null

    companion object {
        const val HOME = "HOME"
        const val TITLE = "TITLE"

        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, VisitorAccessActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    private lateinit var mDatePicker: DatePickerBottomSheet
    private var selectedDate: Calendar = Calendar.getInstance()
        set(value) {
            textDate?.text = value.time.reportDMYFull()
            mPresenter.setDate(value)
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visitor_access)

        setBackToolbar(intent.getStringExtra(TITLE) ?: "Visitor Access")

        mHome = intent.getParcelableExtra(HOME)
        mPresenter.init(mHome?.unitId ?: "")

        selectedDate = Calendar.getInstance()


        mDatePicker = DatePickerBottomSheet(Calendar.getInstance(), false, Calendar.getInstance(), null) { calendar ->
            selectedDate = calendar
        }

        textDate.setOnClickListener {
            mDatePicker.show(supportFragmentManager, "Date Picker")
        }

        editFirstname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setFirstName(it)
        })

        editLastname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setLastName(it)
        })

        editIDCard.addTextChangedListener(TextWatcherExtend {
            mPresenter.setIDCard(it)
        })

        editTel.addTextChangedListener(TextWatcherExtend {
            mPresenter.setTel(it)
        })

        buttonContinue.setOnClickListener {
            val optionIndex = spinnerOption.checkedRadioButtonId
            sendEvent("VISITOR_ACCESS", "CLICK", "REQUEST_QR")
            mPresenter.validate(optionIndex)
        }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("VISITOR_ACCESS")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_history, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_history -> VisitorAccessHistoryActivity.start(this, mHome?.unitId ?: "", getString(R.string.history))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showAccessOptions(options: List<TextValue>) {
        val typeface = ResourcesCompat.getFont(this, R.font.graphik_th_regular)

        options.forEachWithIndex { index, option ->
            val itemRadio = RadioButton(this)
            itemRadio.text = option.text
            itemRadio.id = index
            itemRadio.typeface = typeface
            itemRadio.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_body2))
            if(index == 0) {
                itemRadio.isChecked = true
            }

            spinnerOption.addView(itemRadio)
        }
    }

    override fun showFirstNameError() {
        editFirstname.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showLastNameError() {
        editLastname.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showIDCardError() {
        editIDCard.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showTelError() {
        editTel.error = getString(R.string.please_fill_out_this_field)
    }

    override fun showError(message: String) {
        alertError(message, getString(R.string.error_something_went_wrong)) {
            finish()
        }
    }

    override fun showLoading() {
        progressBar.show()
        buttonContinue.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonContinue.show()
    }

    override fun launchQR(code: String) {
        VisitorAccessQRActivity.start(this, mHome, code)
    }
}

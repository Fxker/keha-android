package com.sansiri.homeservice.model

import android.os.Parcel
import android.os.Parcelable
import com.sansiri.homeservice.model.api.TodaySummary

/**
 * Created by sansiri on 10/31/17.
 */
data class Summary(val icon: Int, val type: String, val typeTitle: String, val title: String, val subTitle: String?, val date: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(icon)
        writeString(type)
        writeString(typeTitle)
        writeString(title)
        writeString(subTitle)
        writeString(date)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Summary> = object : Parcelable.Creator<Summary> {
            override fun createFromParcel(source: Parcel): Summary = Summary(source)
            override fun newArray(size: Int): Array<Summary?> = arrayOfNulls(size)
        }
    }
}
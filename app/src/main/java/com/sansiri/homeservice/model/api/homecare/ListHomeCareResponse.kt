package com.sansiri.homeservice.model.api.homecare

import com.sansiri.homeservice.model.api.ListResponse
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

/**
 * Created by sansiri on 11/27/17.
 */
data class ListHomeCareResponse<T>(
        val count: Int = 0,
        val items: List<T>,
        val expiredAt: String? = null
) {
    val _expiredAt: Date?
        get() {
            return if (expiredAt != null)
                try {
                    DateTime(this.expiredAt).toDate()
                } catch (e: ParseException) {
                    null
                } else null
        }

}
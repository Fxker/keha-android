package com.appy.android.code.data.rest.JsonBody

import com.appy.android.sdk.AppySDK
import com.google.gson.annotations.SerializedName

/**
 * Created by tar on 7/12/2017 AD.
 */
abstract class BaseCommandJsonBody<T> {

    @SerializedName("pin_code")
    open var pinCode: String? = AppySDK.instance().pin

    @SerializedName("unit")
    open var unit: String? = AppySDK.instance().unit

    @SerializedName("command")
    open var command: T? = null
}
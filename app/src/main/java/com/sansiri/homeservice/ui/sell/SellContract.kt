package com.sansiri.homeservice.ui.sell

import com.sansiri.homeservice.model.Sell
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/7/17.
 */
interface SellContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
    }

    interface View : BaseView {
        fun bindData(list: List<Sell>)
    }
}
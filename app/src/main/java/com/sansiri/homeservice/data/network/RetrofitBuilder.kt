package com.sansiri.homeservice.data.network

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class RetrofitBuilder(val okHttpClient: OkHttpClient,
                      val converterFactory: Converter.Factory,
                      val adapterFactory: CallAdapter.Factory) {

    inline fun <reified T> build(baseUrl: String): T {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
//                .addConverterFactory(NullOnEmptyConverterFactory())
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(adapterFactory)
                .build()
                .create(T::class.java)
    }

}
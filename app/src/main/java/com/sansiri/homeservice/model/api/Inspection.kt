package com.sansiri.homeservice.model.api

import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class Inspection(
	val title: String? = null,
	val typeName: String? = null,
	val status: Int? = null,
	val statusText: String? = null,
	val createDate: String? = null
) {
    val _createdAt: Date?
        get() {
            try {
                return DateTime(this.createDate).toDate()
            } catch (e: ParseException) {
                return null
            }
        }
}

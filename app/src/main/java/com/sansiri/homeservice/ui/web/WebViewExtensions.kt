package com.sansiri.homeservice.ui.web

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Message
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.fragment.app.Fragment
import android.widget.ProgressBar
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_web.*
import android.webkit.WebSettings.PluginState
import android.widget.FrameLayout
import androidx.core.content.ContextCompat.startActivity


fun WebView.makeItSmart(activity: Activity, progressBar: ProgressBar? = null, extraRedirect: ((String, String?) -> Unit)? = null) {
    setUpWebView(this, progressBar)
    webViewClient = SmartWebViewClient(activity, extraRedirect)
}

fun WebView.makeItSmart(fragment: Fragment, progressBar: ProgressBar? = null, extraRedirect: ((String, String?) -> Unit)? = null) {
    setUpWebView(this, progressBar)
    webViewClient = SmartWebViewClient(fragment, extraRedirect)
}

private fun setUpWebView(webView: WebView, progressBar: ProgressBar? = null) {
    webView.settings.apply {
        setSupportZoom(true)
        builtInZoomControls = true
        displayZoomControls = false
        loadWithOverviewMode = true
        useWideViewPort = true
        cacheMode = WebSettings.LOAD_NO_CACHE
    }

    webView.webChromeClient = object : WebChromeClient() {
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            progressBar?.progress = newProgress;
            if (newProgress == 100) {
                progressBar?.hide();
            } else {
                progressBar?.show();
            }
        }

        override fun onGeolocationPermissionsShowPrompt(origin: String?, callback: GeolocationPermissions.Callback?) {
            callback?.invoke(origin, true, false)
        }


//        override fun onCreateWindow(view: WebView, isDialog: Boolean,
//                           isUserGesture: Boolean, resultMsg: Message): Boolean {
//            webView.visibility = View.GONE
//            val cookieManager = CookieManager.getInstance()
//            cookieManager.setAcceptCookie(true)
//            val wvNew = WebView(view.context)
//            wvNew.isVerticalScrollBarEnabled = false
//            wvNew.isHorizontalScrollBarEnabled = false
//            wvNew.webViewClient = UriWebViewClient()
//            val webSettings = wvNew.settings
//            webSettings.javaScriptEnabled = true
//            webSettings.setAppCacheEnabled(true)
//            webSettings.javaScriptCanOpenWindowsAutomatically = true
//            webSettings.setSupportMultipleWindows(true)
//            wvNew.layoutParams = FrameLayout.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.MATCH_PARENT)
//            mContainer.addView(wvNew)
//            val transport = resultMsg.obj as WebView.WebViewTransport
//            transport.webView = wvNew
//            resultMsg.sendToTarget()
//
//            return true
//        }


    }
}
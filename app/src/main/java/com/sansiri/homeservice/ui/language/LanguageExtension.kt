package com.sansiri.homeservice.ui.language

import java.util.*

fun Locale.getLanguageExtension(): String? {
    return when(this) {
        Locale.SIMPLIFIED_CHINESE -> "zh-Hans"
        Locale.TRADITIONAL_CHINESE -> "zh-Hant"
        else -> language
    }
}
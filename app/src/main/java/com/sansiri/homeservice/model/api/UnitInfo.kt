package com.sansiri.homeservice.model.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UnitInfo(
        var objectId: String = "",

        var createdAt: String = "",

        var usageArea: Double = 0.0,

        var address: String = "",

        var isTransfer: Boolean = false,

        var rolename: String = "",

        var floor: String = "",

        @Deprecated("use homeAutomationGateway instead")
        var appyConfig: AppyConfig? = null,

        var projectId: String = "",

        var updatedAt: String = "",

        var homeUpgradeRequestId: String = "",

        var isSupportHomeUpgrade: Boolean = false,

        var homeAutomationGateway: HomeAutomationGateway? = null
): Parcelable {
    @Parcelize
    data class HomeAutomationGateway(
            var objectId: String? = null,
            var provider: String? = null,
            var isSelected: Boolean = false,
            var createdAt: String? = null,
            var updatedAt: String? = null,
            var appyId: String? = null,
            var pin: String? = null,
            var username: String? = null,
            var password: String? = null,
            var code: String? = null
    ): Parcelable {
        companion object Provider {
            const val APPY = "APPY"
            const val ORVIBO = "ORVIBO"
            const val BATHOME = "B@HOME"
        }
    }
}
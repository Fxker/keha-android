package com.appy.android.sdk.control.daikin

enum class DaikinTemperatureCommand: APDaikinParameter {
    TEMP_18 {
        override fun action(): String {
            return "18"
        }
    },
    TEMP_19 {
        override fun action(): String {
            return "19"
        }
    },
    TEMP_20 {
        override fun action(): String {
            return "20"
        }
    },
    TEMP_21 {
        override fun action(): String {
            return "21"
        }
    },
    TEMP_22 {
        override fun action(): String {
            return "22"
        }
    },
    TEMP_23 {
        override fun action(): String {
            return "23"
        }
    },
    TEMP_24 {
        override fun action(): String {
            return "24"
        }
    },
    TEMP_25 {
        override fun action(): String {
            return "25"
        }
    },
    TEMP_26 {
        override fun action(): String {
            return "26"
        }
    },
    TEMP_27 {
        override fun action(): String {
            return "27"
        }
    },
    TEMP_28 {
        override fun action(): String {
            return "28"
        }
    },
    TEMP_29 {
        override fun action(): String {
            return "29"
        }
    },
    TEMP_30 {
        override fun action(): String {
            return "30"
        }
    };
}
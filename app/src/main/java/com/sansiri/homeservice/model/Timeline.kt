package com.sansiri.homeservice.model

import java.sql.Time
import java.util.*

/**
 * Created by sansiri on 10/16/17.
 */
data class Timeline(val time: Calendar, var title: String? = null) {
    var isNewEventClick = false
    var isRender = false
    var hideLine = false
    var startPosition: Int = 0
    var endPosition: Int = 0
    var isMe: Boolean = false
    var idBooking: String? = null
    var disable: Boolean = false
}
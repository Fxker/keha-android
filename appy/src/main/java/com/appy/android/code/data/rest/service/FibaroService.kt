package com.appy.android.code.data.rest.service

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

interface FibaroService {

    @POST
    fun startScene(@Url url: String, @Body body: Any?): Observable<Response<Void?>>

}
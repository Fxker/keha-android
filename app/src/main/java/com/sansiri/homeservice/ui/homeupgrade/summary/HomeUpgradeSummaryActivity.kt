package com.sansiri.homeservice.ui.homeupgrade.summary

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.homeupgrade.contact.HomeUpgradeContactActivity
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.toCurrencyFormat
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_home_upgrade_selector.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class HomeUpgradeSummaryActivity : BaseActivity<HomeUpgradeSummaryContract.View, HomeUpgradeSummaryContract.Presenter>(), HomeUpgradeSummaryContract.View {

    override var mPresenter: HomeUpgradeSummaryContract.Presenter = HomeUpgradeSummaryPresenter()

    companion object {
        val ROOMS = "ROOMS"
        val UNIT_ID = "UNIT_ID"
        fun start(activity: Activity, homeUpgradeRooms: ArrayList<HomeUpgradeRoom>, unitId: String) {
            val intent = Intent(activity, HomeUpgradeSummaryActivity::class.java)
            intent.putParcelableArrayListExtra(ROOMS, homeUpgradeRooms)
            intent.putExtra(UNIT_ID, unitId)
            activity.startActivity(intent)
        }
    }

    val mAdapter =  SectionedRecyclerViewAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_upgrade_selector)

        setBackToolbar("สรุปรายการสั่งซื้อ")

        // val unitId = intent.getStringExtra(UNIT_ID)
        val rooms = intent.getParcelableArrayListExtra<HomeUpgradeRoom>(ROOMS)
        val unitId = intent.getStringExtra(UNIT_ID)

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@HomeUpgradeSummaryActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }

        buttonNext.setOnClickListener {
            mPresenter.requestNextStep()
        }

        showTotalPrice(0.0)

        mPresenter.init(rooms, unitId)
        mPresenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun bindData(summary: HashMap<String, List<HomeUpgradeHardware>>) {
        for((key, value) in summary) {
            val adapter = HomeUpgradeSummarySectionAdapter(key, value)
            mAdapter.addSection(adapter)
        }

        mAdapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
        alert(message, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showTotalPrice(totalPrice: Double) {
        textTotalPrice.text = "${totalPrice.toCurrencyFormat()} ${getString(R.string.baht)}"
    }

    override fun launchNextStep(orderRequest: ArrayList<HomeUpgradeOrderRequest>, unitId: String) {
        HomeUpgradeContactActivity.start(this, orderRequest, unitId)
    }
}

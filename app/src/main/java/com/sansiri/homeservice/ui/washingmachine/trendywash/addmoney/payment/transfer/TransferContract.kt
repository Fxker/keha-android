package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGateWayQR
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TransferContract {
    interface Presenter : BasePresenter<View> {
        var paymentGateWayQR: MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>?
        var creditInfoLiveData: MutableLiveData<Resource<TrendyWashCredit>>?

        fun init(unitId: String?, userId: String?, amount: Int)
        fun fetch()
    }

    interface View : BaseView {
        fun bindData(qrCode: String)
        fun showLoading()
        fun hideLoading()
    }
}
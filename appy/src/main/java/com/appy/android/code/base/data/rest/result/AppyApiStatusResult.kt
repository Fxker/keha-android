package com.appy.android.code.base.data.rest.result

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
class AppyApiStatusResult {
    @SerializedName("code")
    open var code: Int? = 200

    @SerializedName("description")
    open var description: String? = ""
}
package com.sansiri.homeservice.ui.visitor

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.auth.ApiAuthRepository
import com.sansiri.homeservice.model.api.TextValue
import com.sansiri.homeservice.model.api.VisitorAccessRequest
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import org.joda.time.DateTime
import java.util.*

class VisitorAccessPresenter() : BasePresenterImpl<VisitorAccessContract.View>(), VisitorAccessContract.Presenter {
    private var mUnitId: String? = null
    private var mFirstName: String? = null
    private var mLastName: String? = null
    private var mIDCard: String? = null
    private var mTel: String? = null
    private var mDate: String? = null

    override fun init(unitId: String) {
        this.mUnitId = unitId
    }

    override fun start() {
        this.fetch()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    private var mAccessOptions: List<TextValue>? = null

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null && mUnitId != null) {
                mCompositeDisposable.add(api.getVisitorAccessOptions(mUnitId!!).observe().subscribe({
                    if (it.accessOption != null) {
                        mAccessOptions = it.accessOption
                        mView?.showAccessOptions(it.accessOption)
                    } else {
                        mView?.showError("")
                    }
                }) {
                    mView?.showError(it.showHttpError())
                })
            }
        }
    }

    override fun setFirstName(firstName: String) {
        this.mFirstName = firstName
    }

    override fun setLastName(lastName: String) {
        this.mLastName = lastName
    }

    override fun setIDCard(idCard: String) {
        this.mIDCard = idCard
    }

    override fun setTel(tel: String) {
        this.mTel = tel
    }

    override fun setDate(calendar: Calendar) {
        this.mDate = DateTime(calendar.time).toString()
    }

    override fun validate(selectedOption: Int) {
        var isValidate = false

        if (this.mFirstName.isNullOrEmpty()) {
            mView?.showFirstNameError()
            isValidate = true
        }

        if (this.mLastName.isNullOrEmpty()) {
            mView?.showLastNameError()
            isValidate = true
        }

        if (this.mIDCard.isNullOrEmpty()) {
            mView?.showIDCardError()
            isValidate = true
        }

        if (this.mTel.isNullOrEmpty()) {
            mView?.showTelError()
            isValidate = true
        }

        val optionValue = mAccessOptions?.get(selectedOption)?.value

        if (!isValidate && optionValue != null) {
            val request = VisitorAccessRequest(
                    mFirstName,
                    mLastName,
                    mIDCard,
                    mTel,
                    mDate,
                    optionValue,
                    VisitorAccessRequest.ONE_TIME
            )

            mView?.sendEvent("VISITOR_ACCESS", "SELECT", "ACCESS_OPTION", "1", Pair("Option", optionValue))
            submit(request)
        }
    }

    override fun submit(request: VisitorAccessRequest) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                mCompositeDisposable.add(api.submitVisitorAccess(mUnitId
                        ?: "", request).observe().subscribe({
                    mView?.hideLoading()
                    if (it.code != null) {
                        mView?.launchQR(it.code)
                    }
                }) {
                    mView?.hideLoading()
                    mView?.showError(it.showHttpError())
                })
            }
        }
    }
}
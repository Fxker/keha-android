package com.sansiri.homeservice.ui.svvh

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SVVHContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun init(home: Hut)
    }

    interface View : BaseView {
        fun showRegistration(home: Hut, customer: SVVHRegisterCustomer?)
        fun showPreview(home: Hut, customer: SVVHRegisterCustomer)
        fun launchWebView(redirectUrl: String?)
        fun showLoading()
        fun hideLoading()
    }
}
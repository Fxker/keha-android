package com.sansiri.homeservice.ui.svvh.registration

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import android.view.LayoutInflater
import android.widget.TextView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.reportTime
import com.sansiri.homeservice.util.toDate


class GenderSpinnerAdapter(context: Context, val res: Int, val objects: List<Pair<String, String>>) : ArrayAdapter<Pair<String, String>>(context, res, objects) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(res, parent, false)

        view?.findViewById<TextView>(android.R.id.text1)?.text = objects[position].second

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_text_item_spinner, parent, false)
        val textView = view?.findViewById<TextView>(android.R.id.text1)
        if (position == 0) {
            textView?.setTextColor(context.resources.getColor(R.color.textHint))
        }
        textView?.text = objects[position].second
        return view
    }

    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }
}
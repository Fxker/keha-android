package com.sansiri.homeservice.ui.inspection

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Inspection
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.mailbox.MailBoxActivity
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class InspectionActivity : BaseActivity<InspectionContract.View, InspectionContract.Presenter>(), InspectionContract.View {
    override var mPresenter: InspectionContract.Presenter = InspectionPresenter()


    val mAdapter = SectionedRecyclerViewAdapter()

    companion object {
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"
        val TITLE = "TITLE"

        fun start(activity: Activity, unitObjectId: String, title: String?) {
            val intent = Intent(activity, InspectionActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.inspection))

        textNoData.text = getString(R.string.no_pending_inspection_item)

        val unitObjectId = intent?.getStringExtra(UNIT_OBJECT_ID)
        if (unitObjectId != null) {
            mPresenter.init(unitObjectId)
            mPresenter.start()
        } else {
            showNoData()
        }

        list.adapter = mAdapter

//        bindData(arrayListOf(
//                Inspection(getString(R.string.mock_inspection_title_1), getString(R.string.chip), getString(R.string.edited)),
//                Inspection(getString(R.string.mock_inspection_title_2), getString(R.string.stretch), getString(R.string.edited))
//        ))

    }

    override fun onResume() {
        super.onResume()
        sendScreenView("INSPECTION")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(inspections: List<Inspection>) {
        var mapSection = hashMapOf<String, MutableList<Inspection>>()

        inspections.forEach {
            val reportDate = it._createdAt?.report()
            if(reportDate != null) {
                if (mapSection[reportDate] == null) {
                    mapSection.put(reportDate, mutableListOf(it))
                } else {
                    mapSection[reportDate]?.add(it)
                }
            }
        }

        mapSection.forEach {
            val adapter = InspectionSectionAdapter(it.key, it.value) {}
            mAdapter.addSection(adapter)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alertError(message)
    }


    override fun showNoData() {
        textNoData.show()
    }
}

package com.appy.android.sdk.status

import com.appy.android.sdk.control.APControl
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
abstract class APStatus<T> {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("alternate_title")
    var titleAlternate: String? = null
    @SerializedName("min_value")
    var minValue: String? = null
    @SerializedName("max_value")
    var maxValue: String? = null
    @SerializedName("current_value")
    var currentValue: String? = null
    @SerializedName("last_action")
    var lastAction: T? = null
    @SerializedName("available_actions")
    var availableAction: List<T>? = ArrayList()

    constructor()

    constructor(control: APControl, value: String) {
        id = control.id
        title = control.title
        titleAlternate = control.titleAlternate
        minValue = "0"
        maxValue = "1"
        currentValue = value
    }
}
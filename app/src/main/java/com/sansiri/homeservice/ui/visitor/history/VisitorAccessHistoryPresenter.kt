package com.sansiri.homeservice.ui.visitor.history

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class VisitorAccessHistoryPresenter : BasePresenterImpl<VisitorAccessHistoryContract.View>(), VisitorAccessHistoryContract.Presenter {
    val ITEM = 10
    val allHistories = mutableListOf<VisitorAccessHistory>()
    override fun start() {
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetchData(unitObjectId: String, page: Int) {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(
                        api.getVisitorAccessHistory(unitObjectId, page * ITEM, ITEM).observe().subscribe({ histories ->
                            mView?.hideLoading()
                            if (histories.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                allHistories.addAll(histories)
                                mView?.bindData(allHistories)
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }
}
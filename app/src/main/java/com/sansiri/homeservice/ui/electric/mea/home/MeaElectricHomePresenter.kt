package com.sansiri.homeservice.ui.electric.mea.home

import com.mea.energysdk.widget.MEAEnergyWidget
import com.sansiri.homeservice.data.repository.mea.MeaRepository
import com.sansiri.homeservice.ui.base.BasePresenterImpl

class MeaElectricHomePresenter(val repository: MeaRepository) : BasePresenterImpl<MeaElectricHomeContract.View>(), MeaElectricHomeContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun loadWidget(meaWidget: MEAEnergyWidget?, ca: String) {
        if (meaWidget != null) {
            repository.showReport(meaWidget, ca)
        }
    }
}
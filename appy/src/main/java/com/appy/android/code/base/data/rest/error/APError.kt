package com.appy.android.code.base.data.rest.error

import com.google.gson.annotations.SerializedName

abstract class APError : Exception() {
    @SerializedName("code")
    var code: Int? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("error")
    var error: String? = null
}
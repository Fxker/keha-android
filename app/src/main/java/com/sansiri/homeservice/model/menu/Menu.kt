package com.sansiri.homeservice.model.menu

import com.sansiri.homeservice.R
import kotlinx.android.parcel.Parcelize

/**
 * Created by sansiri on 11/21/17.
 */
interface Menu {
    val id: String?
    val title: String?
    val icon: String?

    fun getLocalIcon(): Int = when (id) {
        Feature.SALE_RENTOUT, Feature.SALE -> R.drawable.ic_sale_rentout

        Feature.DOWNPAYMENT -> R.drawable.ic_bill

        Feature.HOME_CARE -> R.drawable.ic_home_care

        Feature.MAIL_BOX -> R.drawable.ic_mail_box

        Feature.HOME_WALLET -> R.drawable.ic_my_account

        Feature.CONTACT_US -> R.drawable.ic_contact

        Feature.FACILITY_BOOKING -> R.drawable.ic_facility_booking

        Feature.PHONE_DIRECTORY -> R.drawable.ic_phone

        Feature.PROJECT_PROGRESS -> R.drawable.ic_project_progress

        Feature.CHAT -> R.drawable.ic_contact_us

        Feature.TRANSFER -> R.drawable.ic_home_wallet

        Feature.INSPECTION -> R.drawable.ic_inspection

        Feature.APPOINTMENT -> R.drawable.ic_facility_booking

        Feature.SUGGESTION -> R.drawable.ic_mail

        Feature.PROFILE -> R.drawable.ic_profile

        Feature.LOUNGE -> R.drawable.ic_social_lounge

        Feature.LANGUAGE -> R.drawable.ic_language

        Feature.SIGN_OUT -> R.drawable.ic_logout

        Feature.NOTIFICATION_SETTINGS, Feature.NOTIFICATION_SETTINGS_ALL -> R.drawable.ic_bell

        Feature.CONNECT_GOOGLE_ASSISTANT -> R.drawable.ic_google_assist

        Feature.PARTNER_ORBIVO -> R.drawable.ic_orvibo

        else -> R.drawable.placeholder_menu
    }

}
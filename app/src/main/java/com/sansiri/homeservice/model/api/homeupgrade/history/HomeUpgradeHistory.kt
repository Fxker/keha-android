package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable

data class HomeUpgradeHistory(
        val no: String? = null,
        val createdAt: String? = null,
        val totalPriceThb: Int? = null,
        val installation: Installation? = null,
        val payment: Payment? = null,
        val id: String? = null,
        val items: List<Item?>? = null,
        val status: String? = null,
        val customer: Contact? = null,
        val vendor: Vendor? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readParcelable<Installation>(Installation::class.java.classLoader),
            source.readParcelable<Payment>(Payment::class.java.classLoader),
            source.readString(),
            source.createTypedArrayList(Item.CREATOR),
            source.readString(),
            source.readParcelable<Contact>(Contact::class.java.classLoader),
            source.readParcelable<Vendor>(Vendor::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(no)
        writeString(createdAt)
        writeValue(totalPriceThb)
        writeParcelable(installation, 0)
        writeParcelable(payment, 0)
        writeString(id)
        writeTypedList(items)
        writeString(status)
        writeParcelable(customer, 0)
        writeParcelable(vendor, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeUpgradeHistory> = object : Parcelable.Creator<HomeUpgradeHistory> {
            override fun createFromParcel(source: Parcel): HomeUpgradeHistory = HomeUpgradeHistory(source)
            override fun newArray(size: Int): Array<HomeUpgradeHistory?> = arrayOfNulls(size)
        }
    }
}

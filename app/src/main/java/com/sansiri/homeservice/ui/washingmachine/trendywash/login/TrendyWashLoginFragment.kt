package com.sansiri.homeservice.ui.washingmachine.trendywash.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.ui.washingmachine.trendywash.TrendyWashMainActivity
import com.sansiri.homeservice.util.TextWatcherExtend
import kotlinx.android.synthetic.main.fragment_trendy_wash_login.*
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.web.WebPopUpCompatActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import org.koin.androidx.scope.currentScope

class TrendyWashLoginFragment : BaseV2Fragment<TrendyWashLoginContract.View, TrendyWashLoginContract.Presenter>(), TrendyWashLoginContract.View {
    override val mPresenter: TrendyWashLoginContract.Presenter by currentScope.inject()
    private var mUnitId: String? = null
    private var mIconUrl: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val ICON_URL = "ICON_URL"

        fun newInstance(unitId: String, iconUrl: String?): TrendyWashLoginFragment {
            return TrendyWashLoginFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(ICON_URL, iconUrl)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mIconUrl = arguments?.getString(ICON_URL)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRENDY_WASH_LOGIN")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trendy_wash_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonRegister.setOnClickListener {
            if (activity is TrendyWashMainActivity) {
                (activity as TrendyWashMainActivity).launchRegister()
            }
        }

        buttonFacebook.setOnClickListener {
            launchWeb(mPresenter.getFacebookLoginLink())
        }

        buttonBack.setOnClickListener {
            activity?.onBackPressed()
        }

        editPhone.addTextChangedListener(TextWatcherExtend {
            checkLoginButtonAvailability()
        })

        editPassword.addTextChangedListener(TextWatcherExtend {
            checkLoginButtonAvailability()
        })

        checkLoginButtonAvailability()

        mIconUrl?.let { Glide.with(this).load(it).into(imageIcon) }


        if (mUnitId != null) {
            buttonLogin.setOnClickListener {
                mPresenter.login(editPhone.text.toString(), editPassword.text.toString()).observe(this, Observer { resource ->
                    when (resource?.status) {
                        Resource.LOADING -> {
                            showLoading()
                        }
                        Resource.SUCCESS -> {
                            hideLoading()
                            val customerId = resource.data?.id
                            if (customerId != null) {
                                launchHome(customerId)
                            } else {
                                showError("Login Fail")
                            }
                        }
                        Resource.ERROR -> {
                            hideLoading()
                            showError(resource.message ?: "Error")
                        }
                    }
                })
            }

            mPresenter.init(mUnitId!!)
            mPresenter.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun launchWeb(url: String) {
        WebPopUpCompatActivity.start(activity!!, url, "login/success")
    }

    private fun checkLoginButtonAvailability() {
        val isEnable = editPhone.text.toString().isNotEmpty() && editPassword.text.toString().isNotEmpty()
        buttonLogin.isEnabled = isEnable
        buttonLogin.alpha = if (isEnable) 1f else 0.5f
    }

    override fun launchHome(userId: String) {
        PreferenceHelper.defaultPrefs(context!!)[PreferenceHelper.TREND_WASH_USER_ID] = userId
        if (activity is TrendyWashMainActivity) {
            (activity as TrendyWashMainActivity).launchHome(userId)
        }
    }

    override fun showLoading() {
        progressBar.show()
        buttonLogin.hide()
        buttonRegister.hide()
        editPhone.isEnabled = false
        editPassword.isEnabled = false
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonLogin.show()
        buttonRegister.show()
        editPhone.isEnabled = true
        editPassword.isEnabled = true
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

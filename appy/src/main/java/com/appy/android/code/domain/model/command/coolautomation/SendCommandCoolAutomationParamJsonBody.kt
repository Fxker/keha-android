package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
internal class SendCommandCoolAutomationParamJsonBody : BaseCommandJsonBody<String>() {
    @SerializedName("command_params")
    var commandParam: String? = null
}
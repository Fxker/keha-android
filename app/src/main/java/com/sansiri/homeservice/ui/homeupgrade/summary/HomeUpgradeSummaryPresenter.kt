package com.sansiri.homeservice.ui.homeupgrade.summary

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 12/19/17.
 */
class HomeUpgradeSummaryPresenter : BasePresenterImpl<HomeUpgradeSummaryContract.View>(), HomeUpgradeSummaryContract.Presenter {

    lateinit var mRooms: List<HomeUpgradeRoom>
    lateinit var mUnitId: String

    override fun init(homeUpgradeRooms: List<HomeUpgradeRoom>, unitId: String) {
        this.mRooms = homeUpgradeRooms
        this.mUnitId = unitId
    }

    override fun start() {
        val section = hashMapOf<String, List<HomeUpgradeHardware>>()
        mRooms.forEach { room ->
            val name = room.name
            val list = room.hardwareList?.filter { hardware ->
                hardware.amount > 0
            }
            if (list != null && list.isNotEmpty()) {
                section[name] = list
            }
        }
        mView?.bindData(section)
        calculateTotalPrice()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    private fun calculateTotalPrice() {
        var totalPrice = 0.0
        mRooms.forEach { room ->
            room.hardwareList?.filter { it.amount != 0 }?.forEach { hardware ->
                totalPrice += hardware.price!! * hardware.amount
            }
        }

        mView?.showTotalPrice(totalPrice)
    }

    override fun requestNextStep() {
        val orderRequest = arrayListOf<HomeUpgradeOrderRequest>()
        mRooms.forEach { room ->
            room.hardwareList?.filter { it.amount > 0 }?.forEach { hardware ->
                orderRequest.add(HomeUpgradeOrderRequest(
                        room.id,
                        hardware.id,
                        hardware.amount
                ))
            }
        }

        mView?.launchNextStep(orderRequest, mUnitId)
    }
}
package com.appy.android.code.domain.model.status.daikin

import com.appy.android.code.data.rest.form.AppyForm

internal class GetStatusDaikinForm(
        internal val homeId: String,
        internal val daikinId: String
) : AppyForm()
package com.appy.android.code.domain.model.command.remotecontrol

import com.appy.android.code.data.rest.form.AppyForm

internal class GetRemoteControlsForm(
        internal val homeId: String
) : AppyForm()
package com.sansiri.homeservice.ui.announcement.summary

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/18/17.
 */
class SummaryAnnouncementPresenter : BasePresenterImpl<SummaryAnnouncementContract.View>(), SummaryAnnouncementContract.Presenter {
    override fun start() {
        fetchData()
    }

    override fun destroy() {
    }

    override fun fetchData() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                mCompositeDisposable.add(

                        api.getFamilyAnnouncement(0, 3).observe().subscribe({
                            mView?.hideLoading()
                            // mView?.addSansiriFamily(it)
                            mView?.addDataAt(0, SummaryItem(mView?.getString(R.string.news_update) ?: "Announcement", SummaryItem.SANSIRI_FAMILY, it))
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )

                mCompositeDisposable.add(

                        api.getCentralProjectAnnouncement(0,3).observe().subscribe({
                            mView?.hideLoading()
                            val title = mView?.getString(R.string.announcement_central) ?: "Announcement"
                            mView?.addDataAt(1, SummaryItem(title, SummaryItem.ANNOUNCEMENT, it))
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )

                mCompositeDisposable.add(

                        api.getAllProjectAnnouncement().observe().subscribe({ announcements ->
                            mView?.hideLoading()
                            mCompositeDisposable.add(

                                    api.getAllProjects().observe().subscribe({ projects ->
                                        for (announcement in announcements) {
                                            val project = projects.find { it.objectId == announcement.key }
                                            if (project != null && announcement.value.isNotEmpty()) {
                                                mView?.addData(SummaryItem(project.name, project.objectId, announcement.value))
                                            }
                                        }
                                    }) {
                                        mView?.hideLoading()
                                        mView?.showError(it.showHttpError())
                                    }
                            )
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            } else {
                mView?.showError("Network Error")
            }
        }
        // mView?.setData(AnnouncementMock.map)
    }
}
package com.appy.android.code.base.data.rest.params

import java.util.*

/**
 * Created by Tar on 8/4/16 AD.
 */
open class BaseParamsMap : HashMap<String, String>() {

    open fun put(key: String, value: Boolean): String? {
        return put(key, if (value) "true" else "false")
    }

    open fun put(key: String, value: Int): String? {
        return put(key, value.toString())
    }

    open fun put(key: String, value: Double): String? {
        return put(key, value.toString())
    }

    open fun put(key: String, value: Long): String? {
        return put(key, value.toString())
    }
}

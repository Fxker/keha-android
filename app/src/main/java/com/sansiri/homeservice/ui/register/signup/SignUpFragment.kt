package com.sansiri.homeservice.ui.register.signup


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_sign_up.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class SignUpFragment : BaseFragment<SignUpContract.View, SignUpContract.Presenter>(), SignUpContract.View {
    override var mPresenter: SignUpContract.Presenter = SignUpPresenter()

    companion object {
        fun newInstance(): SignUpFragment {
            return SignUpFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        editUsername.setText("oakraw")
//        editEmail.setText("oakaw@gmail.com")
//        editPassword.setText("12345678")
//        editPasswordConfirm.setText("12345678")

        buttonSignUp.setOnClickListener {
            mPresenter.validate(
                    editEmail.text.toString().removeSpace(),
                    editEmailConfirm.text.toString().removeSpace(),
                    editPassword.text.toString(),
                    editPasswordConfirm.text.toString()
            )
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("EMAIL_SIGN_UP")
    }

    override fun nextScreen() {
        val registerActivity = activity as RegisterActivity
        showLoading()
        registerActivity.bindUser({
            hideLoading()
            (activity as RegisterActivity).launchMainPage()
        }){
            hideLoading()
            showError(it.showHttpError())
        }
    }

    override fun getStringFromRes(res: Int): String {
        return getString(res)
    }



    override fun usernameError(message: String) {
        // editUsername.showError = message
    }

    override fun emailError(message: String) {
        editEmail.error = message
    }

    override fun emailConfirmError(message: String) {
        editEmailConfirm.error = message
    }

    override fun passwordError(message: String) {
        editPassword.error = message
    }

    override fun passwordConfirmError(message: String) {
        editPasswordConfirm.error = message
    }

    override fun showLoading() {
        // editUsername.isEnabled = false
        editEmail.isEnabled = false
        editPassword.isEnabled = false
        editPasswordConfirm.isEnabled = false
        progressBar.show()
        buttonSignUp.hide()
    }

    override fun hideLoading() {
        // editUsername.isEnabled = true
        editEmail.isEnabled = true
        editPassword.isEnabled = true
        editPasswordConfirm.isEnabled = true
        progressBar.hide()
        buttonSignUp.show()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }


}

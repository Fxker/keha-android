package com.sansiri.homeservice.ui.inspection

import com.sansiri.homeservice.model.api.Inspection
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 12/12/17.
 */
interface InspectionContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitObjectId: String?)
    }

    interface View : BaseView {
        fun bindData(inspections: List<Inspection>)
        fun showNoData()
        fun showLoading()
        fun hideLoading()
    }
}
package com.sansiri.homeservice.util

import androidx.recyclerview.widget.RecyclerView
import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log


abstract class EndlessScrollListener(private val mLinearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager, val visibleThreshold: Int = 5) : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of html to load.
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0

    private var current_page = 0

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        Log.d("EndlessScrollListener", "firstVisibleItem: $firstVisibleItem, visibleItemCount: $visibleItemCount, totalItemCount: $totalItemCount")
        visibleItemCount = recyclerView.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            Log.d("EndlessScrollListener", "current_page: $current_page")

            // End has been reached

            // Do something
            current_page++

            onLoadMore(current_page)

            loading = true
        }
    }

    abstract fun onLoadMore(currentPage: Int)

    companion object {
        var TAG = EndlessScrollListener::class.java.simpleName
    }
}
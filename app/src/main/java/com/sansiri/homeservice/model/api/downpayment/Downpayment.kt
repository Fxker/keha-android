package com.sansiri.homeservice.model.api.downpayment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Downpayment(
        val remainAmount: Double? = null,
        val netPrice: Double? = null,
        val items: MutableList<DownpaymentItem>? = null,
        val paidAmount: Double? = null
) : Parcelable {
}
package com.sansiri.homeservice.ui.epayment.gateway.phone

import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface PaymentGatewayPhoneVerifyContract {
    interface Presenter : BasePresenter<View> {
        fun init(paymentGatewayData: PaymentGatewayData)
        fun verifyPhone(phoneNumber: String)
    }

    interface View : BaseView {
        fun bindData(paymentGatewayData: PaymentGatewayData)
        fun launchWeb(url: String)
        fun showLoading()
        fun hideLoading()
        fun showPopup(message: String)
        fun processResponse(response: PaymentResponse)
    }
}
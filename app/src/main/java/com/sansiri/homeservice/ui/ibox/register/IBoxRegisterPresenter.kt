package com.sansiri.homeservice.ui.ibox.register

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.IBoxInformation
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class IBoxRegisterPresenter : BasePresenterImpl<IBoxRegisterContract.View>(), IBoxRegisterContract.Presenter {
    private var mHome: Hut? = null
    private var mPhoneNumber: String? = null

    override fun init(home: Hut, phoneNumber: String?) {
        this.mHome = home
        this.mPhoneNumber = phoneNumber
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetch() {
        if (mPhoneNumber != null) {
            fetchProfileSuccess(mPhoneNumber!!)
        } else {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                api?.getMe()?.observe()?.subscribe({ profile ->
                    fetchProfileSuccess(profile.phoneNumber ?: "")
                }) {}
            }
        }
    }

    override fun fetchProfileSuccess(phoneNumber: String) {
        mView?.bindData(phoneNumber)
    }

    override fun submit(phoneNumber: String) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            mView?.showLoading()
            if (api != null) {
                mCompositeDisposable.add(api.registerIBox(mHome?.unitId
                        ?: "", IBoxInformation(phoneNumber)).observe().subscribe({
                    markButtonAsRegistered(phoneNumber)
                    mView?.launchIBoxList()
                    mView?.hideLoading()
                }) {
                    mView?.hideLoading()
                    mView?.showError(it.showHttpError())
                })
            }
        }
    }

    private fun markButtonAsRegistered(phoneNumber: String) {
        RuntimeCache.dynamicButton?.get(mHome?.unitId)?.forEach { section ->
            val iBoxButton = section.items?.find { item ->
                item.id == Feature.IBOX
            }

            if (iBoxButton?.data != null) {
                iBoxButton.data?.iboxMobileNo = phoneNumber
            } else {
                iBoxButton?.data = DynamicMenu.Data(phoneNumber)
            }
        }
    }
}
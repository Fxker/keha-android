package com.sansiri.homeservice.ui.electric.mea.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import kotlinx.android.synthetic.main.activity_electric_main.*
import kotlinx.android.synthetic.main.fragment_electric_mea.*
import org.koin.androidx.scope.currentScope

class MeaElectricHomeFragment: BaseV2Fragment<MeaElectricHomeContract.View, MeaElectricHomeContract.Presenter>(), MeaElectricHomeContract.View {
    override val mPresenter: MeaElectricHomeContract.Presenter by currentScope.inject()
    private var mCA: String? = null

    companion object {
        private const val CA = "CA"

        fun newInstance(ca: String): MeaElectricHomeFragment {
            return MeaElectricHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(CA, ca)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCA = arguments?.getString(CA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_electric_mea, container, false)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MEA_MAIN")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mCA != null) {
            mPresenter.loadWidget(meaWidget, mCA!!)
        }

    }

    override fun showError(message: String) {
    }
}
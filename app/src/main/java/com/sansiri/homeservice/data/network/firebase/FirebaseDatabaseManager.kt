package com.sansiri.homeservice.data.network.firebase

import com.google.firebase.database.*
import android.content.ClipData.Item
import android.util.Log
import com.google.firebase.database.GenericTypeIndicator
import com.sansiri.homeservice.model.api.ParkingQueue


/**
 * Created by sansiri on 2/27/18.
 */
class FirebaseDatabaseManager {
    private val mDb = FirebaseDatabase.getInstance()
    private var mRefParking: DatabaseReference?
    private var mParkingQueueEvent: ValueEventListener? = null

    init {
        mRefParking = mDb.getReference("parking/queue")
    }


    fun attachParkingQueue(projectId: String, onUpdate: (List<ParkingQueue>) -> Unit) {
        mParkingQueueEvent = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val queue = mutableListOf<ParkingQueue>()
                dataSnapshot.children.forEach {
                    try {
                        val slot = it.getValue(ParkingQueue::class.java)
                        slot?.let { queue.add(slot) }
                    } catch (e: Exception) {
                        Log.e("attachParkingQueue", e.message, e)
                    }
                }
                queue.let(onUpdate)
            }
        }

        mRefParking?.child(projectId)?.addValueEventListener(mParkingQueueEvent!!)
    }

    fun detachParkingQueue() {
        mRefParking?.removeEventListener(mParkingQueueEvent!!)
    }
}
package com.sansiri.homeservice.ui.popup

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.R

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.dialog_image.*
import kotlinx.android.synthetic.main.view_chat_box_other.view.*

class PopupImageFragment: androidx.fragment.app.DialogFragment() {

    companion object {
        fun newInstance(): PopupImageFragment {
            return PopupImageFragment().apply {
                setStyle(androidx.fragment.app.DialogFragment.STYLE_NORMAL, R.style.PopupDialog)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_image, container)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(0)) // set window background to transparent
        // mTracker = (activity?.application as AppController).getDefaultTracker()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Glide.with(this).load("https://s3.envato.com/files/179834411/jpg/300x600_Half_Page_Banner.jpg").into(image)
    }
}
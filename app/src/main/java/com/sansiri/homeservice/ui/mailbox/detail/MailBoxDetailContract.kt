package com.sansiri.homeservice.ui.mailbox.detail

import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/12/17.
 */
interface MailBoxDetailContract {
    interface Presenter : BasePresenter<View> {
        fun prepareBarcode()
    }

    interface View : BaseView {
        fun generateBarcode(data: String)
        fun showSandeeDialog()
        fun bindData(mail: MailBox)
    }
}
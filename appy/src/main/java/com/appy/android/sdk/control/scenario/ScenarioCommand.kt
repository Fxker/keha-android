package com.appy.android.sdk.control.scenario

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class ScenarioCommand : APCommand {
    ACTIVATE {
        override fun action(): String {
            return "ACTIVATE_SCENE"
        }
    };
}
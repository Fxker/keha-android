package com.sansiri.homeservice.model.api

/**
 * Created by oakraw on 17/12/2017 AD.
 */
data class UploadFile(
        val filename: String = "",
        val base64: String = ""
) {
}
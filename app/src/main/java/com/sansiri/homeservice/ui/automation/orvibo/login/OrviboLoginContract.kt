package com.sansiri.homeservice.ui.automation.orvibo.login

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface OrviboLoginContract {
    interface Presenter : BasePresenter<View> {
        fun login(unitId: String?, username: String, password: String)
    }

    interface View : BaseView {
        fun launchOrviboHomeControl()
        fun showLoading()
        fun hideLoading()
    }
}
package com.sansiri.homeservice.ui.welcome

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by oakraw on 10/2/2017 AD.
 */
class WelcomePresenter: BasePresenterImpl<WelcomeContract.View>(), WelcomeContract.Presenter {
    override fun start() {

    }

    override fun destroy() {
    }

    override fun fetchData(page: Int) {
        mView?.showLoading()
        val api = ApiRepositoryProvider.provideApiUnAuthRepository()
        mCompositeDisposable.add(api.getFamilyAnnouncement(page * 5, 5).observe().subscribe({
            mView?.bindData(it)
            mView?.hideLoading()
        }){
            mView?.showError(it.showHttpError())
            mView?.hideLoading()
        })
    }
}
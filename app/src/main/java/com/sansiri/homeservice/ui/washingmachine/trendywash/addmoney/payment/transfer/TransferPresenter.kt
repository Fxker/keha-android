package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGateWayQR
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGatewayRequest
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import java.lang.NumberFormatException

class TransferPresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TransferContract.View>(), TransferContract.Presenter {
    var mUnitId: String? = null
    var mUserId: String? = null
    var mAmount: Int = 0

    override var paymentGateWayQR: MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>? = null
    override var creditInfoLiveData: MutableLiveData<Resource<TrendyWashCredit>>? = null

    override fun init(unitId: String?, userId: String?, amount: Int) {
        mUnitId = unitId
        mUserId = userId
        mAmount = amount
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetch() {
        try {
            if (mUnitId != null && mUserId != null) {
                paymentGateWayQR = repository.checkoutTrendyWashPaymentQR(mUnitId!!, mUserId!!, TrendyWashPaymentGatewayRequest(mAmount))
                creditInfoLiveData = repository.getTrendyWashCredit(mUnitId!!, mUserId!!)
            }
        } catch (e: NumberFormatException) {
        }

    }
}
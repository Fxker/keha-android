package com.appy.android.sdk.status.daikin

import com.google.gson.annotations.SerializedName

class APStatusDaikin {

    @SerializedName("title")
    var title: String? = null
    @SerializedName("address")
    var address: String? = null
    @SerializedName("on_off")
    var onOff: String? = null
    @SerializedName("operation_mode")
    var operationMode: String? = null
    @SerializedName("temp")
    var temp: String? = null
    @SerializedName("fan_speed")
    var fanSpeed: String? = null
    @SerializedName("fan_direction")
    var fanDirection: String? = null

}
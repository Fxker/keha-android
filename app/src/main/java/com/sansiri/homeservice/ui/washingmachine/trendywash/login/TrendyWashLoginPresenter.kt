package com.sansiri.homeservice.ui.washingmachine.trendywash.login

import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashLogin
import com.sansiri.homeservice.ui.base.BasePresenterImpl

class TrendyWashLoginPresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TrendyWashLoginContract.View>(), TrendyWashLoginContract.Presenter {
    private var mUnitId: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun getFacebookLoginLink() = repository.getFacebookLoginTrendyWashLink(mUnitId ?: "")

    override fun login(mobileNo: String, password: String)
            = repository.loginTrendyWash(mUnitId ?: "", TrendyWashLogin(mobileNo, password))
}
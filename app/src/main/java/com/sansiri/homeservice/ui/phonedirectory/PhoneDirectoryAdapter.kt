package com.sansiri.homeservice.ui.phonedirectory

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class PhoneDirectoryAdapter(val itemClick: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<PhoneDirectoryViewHolder>() {
    private val mData = mutableListOf<PhoneDirectoryGroup>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneDirectoryViewHolder {
        return PhoneDirectoryViewHolder(parent.inflate(R.layout.view_phone_directory_card), itemClick)
    }

    override fun onBindViewHolder(holder: PhoneDirectoryViewHolder, position: Int) {
       holder.bind(mData[position])
    }

    fun setData(data: List<PhoneDirectoryGroup>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

}
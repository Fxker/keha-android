package com.sansiri.homeservice.model.preference

data class HomeAutomationSelectionSave(
        val unitId: String? = null,
        val deviceId: String? = null,
        val value: Map<String, String>? = null
)
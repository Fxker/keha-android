package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result


import android.app.ProgressDialog
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail.Companion.GATEWAY_2C2P
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_payment_gateway_2c2p_transaction_result.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.koin.androidx.scope.currentScope


class PaymentGateway2C2PTransactionResultFragment : BaseV2Fragment<PaymentGateway2C2PTransactionResultContract.View, PaymentGateway2C2PTransactionResultContract.Presenter>(), PaymentGateway2C2PTransactionResultContract.View {
    override val mPresenter: PaymentGateway2C2PTransactionResultContract.Presenter by currentScope.inject()

    companion object {
        const val TRANSACTION_ID = "TRANSACTION_ID"
        const val BILLING_EMAIL = "BILLING_EMAIL"
        const val ANALYTIC_TITLE = "ANALYTIC_TITLE"

        fun newInstance(transactionId: String?, billingEmail: String?, analyticTitle: String?): PaymentGateway2C2PTransactionResultFragment {
            return PaymentGateway2C2PTransactionResultFragment().apply {
                arguments = Bundle().apply {
                    putString(TRANSACTION_ID, transactionId)
                    putString(BILLING_EMAIL, billingEmail)
                    putString(ANALYTIC_TITLE, analyticTitle)
                }
            }
        }
    }

    private var mDialogLoading: ProgressDialog? = null
    private var mTransactionId: String? = null
    private var mBillingEmail: String? = null
    private var analyticTitle: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mTransactionId = arguments?.getString(TRANSACTION_ID)
        mBillingEmail = arguments?.getString(BILLING_EMAIL)
        analyticTitle = arguments?.getString(ANALYTIC_TITLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment_gateway_2c2p_transaction_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonConfirm.setOnClickListener {
            (activity as PaymentGatewayActivity).finish2c2p()
        }

        if (mTransactionId != null) {
            mPresenter.init(mTransactionId, mBillingEmail)
            mPresenter.start()
        } else {
            layoutDetail.hide()
        }

        mPresenter.transactionDetail?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data != null) {
                        resource.data?.let { bindData(it) }
                        showLoading()
                    } else {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let { detail ->
                        bindData(detail)
                        if (detail.channel == GATEWAY_2C2P && detail.externalData?.cardToken != null) {
                            mPresenter.saveCard(detail.externalData.pan, detail.externalData.cardToken, detail.method)
                        }
                    }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })


    }

    override fun onResume() {
        super.onResume()
        analyticTitle?.let { sendScreenView("${it}_EPAYMENT_2C2P_COMPLETE") }
    }


    override fun showLoading() {
        mDialogLoading = activity?.indeterminateProgressDialog(getString(R.string.loading), null)

    }

    override fun hideLoading() {
        mDialogLoading?.dismiss()
    }

    override fun bindData(transactionDetail: Transaction2C2PDetail) {
        layoutDetail.show()

        textAmount.text = transactionDetail.total?.toCurrencyFormat()
        textTransactionId.text = "${getString(R.string.transaction_id)}: ${transactionDetail.transactionId}"
        textPaymentMethod.text = "${getString(R.string.payment_method)}: ${transactionDetail.method}"

        transactionDetail._paymentAt?.let {
            textTransactionDate.text = "${getString(R.string.date)}: ${it.reportDMYNumber()} - ${it.reportTime()}"
        }

    }

    override fun showError(messageRes: Int) {
        showError(getString(messageRes))
    }

    override fun showError(message: String) {
    }
}

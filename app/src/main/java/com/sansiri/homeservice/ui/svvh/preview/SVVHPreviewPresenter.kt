package com.sansiri.homeservice.ui.svvh.preview

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class SVVHPreviewPresenter : BasePresenterImpl<SVVHPreviewContract.View>(), SVVHPreviewContract.Presenter {
    var mHome: Hut? = null
    var mCustomer: SVVHRegisterCustomer? = null

    override fun init(home: Hut, customer: SVVHRegisterCustomer) {
        mHome = home
        mCustomer = customer
    }

    override fun start() {
        if (mCustomer != null) {
            mView?.bindData(mCustomer!!)
            fetchPromotion()
        }
    }

    override fun fetchPromotion() {
        if (mHome != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    api.getSVVHPromotion(mHome!!.unitId).observe().subscribe({ promotion ->
                        mView?.showPromotion(promotion)
                    }){}
                }
            }
        }
    }

    override fun destroy() {
    }
}
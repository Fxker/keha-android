package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.appy.android.sdk.control.blind.BlindCommand
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_home_automation_curtain.view.*
import kotlinx.android.synthetic.main.view_icon_and_title.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class CurtainViewHolder(val view: View, val overlayColor: Int, val itemClick: (HomeAutomation, Int) -> Unit) : LifecycleViewHolder(view) {

    var action = BlindCommand.OFF.ordinal

    fun bind(data: HomeAutomation) {
        action = data.action

        with(view) {
            if (data is AppyHomeAutomation) {
                data.apControl?.icon?.let {
                    Glide.with(this).loadCache(it, imageLogo)
                }
            } else {
                imageLogo.setImageResource(data.icon)
            }
            imageLogo.setColorFilter(context.resources.getColor(overlayColor))
            textTitle.text = data.title

            setButtonAction()

            buttonNext.setOnClickListener {
                action = if (action == BlindCommand.OFF.ordinal) BlindCommand.ON.ordinal else BlindCommand.OFF.ordinal
                setButtonAction()
                itemClick(data, action)
            }

            buttonPause.setOnClickListener {
                itemClick(data, BlindCommand.STOP.ordinal)
            }
        }
    }

    private fun setButtonAction() {
        if (action == BlindCommand.OFF.ordinal) {
            view.buttonNext.setImageResource(R.drawable.ic_curtain_open)
        } else {
            view.buttonNext.setImageResource(R.drawable.ic_curtain_close)
        }
    }

    fun setStatus(action: Int) {
        this.action = action
        setButtonAction()
    }
}
package com.sansiri.homeservice.data.database

import android.annotation.SuppressLint
import android.content.Context
import org.jetbrains.anko.configuration
import java.util.*

@SuppressLint("StaticFieldLeak")
/**
 * Created by sansiri on 1/15/18.
 */
object Store {
    var context: Context? = null

    fun initialize(context: Context) {
        this.context = context
    }

    fun getLocale(): Locale? {
        return context?.configuration?.locale
    }
}
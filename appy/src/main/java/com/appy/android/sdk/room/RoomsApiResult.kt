package com.appy.android.sdk.room

import com.appy.android.code.base.data.rest.result.AppyApiResult

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
class RoomsApiResult : AppyApiResult<List<APRoom>>()
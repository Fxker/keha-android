package com.appy.android.sdk.control.scenario

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APScenario : APControl, Parcelable {
    var mActions: List<APActionScenario>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionScenario)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionScenario()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                ScenarioCommand.ACTIVATE.action() -> {
                    action.action = ScenarioCommand.ACTIVATE
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APScenario> {
        const val TYPE = "APScenario"

        override fun createFromParcel(parcel: Parcel): APScenario {
            val type = parcel.readString()
            return APScenario(parcel)
        }

        override fun newArray(size: Int): Array<APScenario?> {
            return arrayOfNulls(size)
        }
    }
}
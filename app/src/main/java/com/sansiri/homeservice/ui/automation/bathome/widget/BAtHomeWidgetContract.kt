package com.sansiri.homeservice.ui.automation.bathome.widget

import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import vc.siriventures.bathome.model.BAtHomeComponent

interface BAtHomeWidgetContract {
    interface Presenter : BasePresenter<View> {
        fun init(username: String?, password: String?, projectId: String?)
    }

    interface View : BaseView {
        fun bindMenu(menus: List<Menu>)
        fun launchComponentType(title: String?, type: String?)
    }
}
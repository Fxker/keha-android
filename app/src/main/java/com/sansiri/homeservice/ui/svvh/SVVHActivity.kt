package com.sansiri.homeservice.ui.svvh

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.svvh.preview.SVVHPreviewFragment
import com.sansiri.homeservice.ui.svvh.registration.SVVHRegistrationFragment
import com.sansiri.homeservice.ui.tutorial.dynamic.TutorialDynamicActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_fragment_container.*


class SVVHActivity : BaseActivity<SVVHContract.View, SVVHContract.Presenter>(), SVVHContract.View {
    private var mMenu: DynamicMenu? = null
    override var mPresenter: SVVHContract.Presenter = SVVHPresenter()

    companion object {
        const val HOME = "HOME"
        const val MENU = "MENU"

        fun start(activity: Activity, home: Hut, menu: Menu) {
            val intent = Intent(activity, SVVHActivity::class.java)
            intent.putExtra(HOME, home)
            if (menu is DynamicMenu) {
                intent.putExtra(MENU, menu)
            }
            activity.startActivity(intent)
        }
    }

    private var mTitle: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        setBackToolbar("")

        val mHome = intent.getParcelableExtra<Hut>(HOME)
        mMenu = intent.getParcelableExtra<DynamicMenu>(MENU)
        mTitle = mMenu?.title ?: ""

        mPresenter.init(mHome)
        mPresenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showPreview(home: Hut, customer: SVVHRegisterCustomer) {
        supportActionBar?.title = mTitle
        replaceFragment(SVVHPreviewFragment.newInstance(home, customer), false)

        mMenu?.id?.let {
            val prefs = PreferenceHelper.defaultPrefs(this)
            prefs[PreferenceHelper.IS_FIRST_TIME_SVVH] = true

            if (prefs[PreferenceHelper.IS_FIRST_TIME_SVVH, true] != false) {
                TutorialDynamicActivity.start(this, mMenu?.title ?: "", mMenu?.id!!)
                prefs[PreferenceHelper.IS_FIRST_TIME_SVVH] = false
            }
        }
    }

    override fun showRegistration(home: Hut, customer: SVVHRegisterCustomer?) {
        supportActionBar?.title = getString(R.string.register)
        replaceFragment(SVVHRegistrationFragment.newInstance(home, customer), false)
    }

    override fun launchWebView(redirectUrl: String?) {
        redirectUrl?.let {
            launchExternalBrowser(redirectUrl)
            finish()
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        snackError(message)
    }
}

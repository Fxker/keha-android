package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney


import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.request.target.Target
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.*
import com.sansiri.homeservice.util.*

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_tile_title.view.*

/**
 * Created by sansiri on 12/12/17.
 */
class TitleTileViewHolder(val overlayColor: Int, val view: View, val itemClick: (Int) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(value: Int) =
            with(view) {
                textTitle.text = value.toString()
                layoutContent.setOnClickListener {
                    itemClick(value)
                }
            }
}
package com.sansiri.homeservice.data.repository.bos

import com.sansiri.homeservice.data.network.auth.BosService
import com.sansiri.homeservice.model.api.bos.BosDetail

class BosRepositoryImpl(val service: BosService): BosRepository {
    override fun getBosType(unitId: String) = service.getBosType(unitId)
    override fun submitCommonAreaIssue(unitId: String, bosDetail: BosDetail) = service.submitCommonAreaIssue(unitId, bosDetail)
    override fun submitMyHomeIssue(unitId: String, bosDetail: BosDetail) = service.submitMyHomeIssue(unitId, bosDetail)
}
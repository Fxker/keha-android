package com.sansiri.homeservice.data.database

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.room.testing.MigrationTestHelper
import org.junit.runner.RunWith
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import androidx.test.core.app.ApplicationProvider
import com.sansiri.homeservice.data.database.AppDatabase.Companion.MIGRATION_1_2
import com.sansiri.homeservice.data.database.AppDatabase.Companion.MIGRATION_2_3
import com.sansiri.homeservice.data.database.AppDatabase.Companion.MIGRATION_3_4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import androidx.room.Room.databaseBuilder
import androidx.sqlite.db.SupportSQLiteDatabase
import junit.framework.Assert.assertEquals


@RunWith(AndroidJUnit4::class)
class MigrationTest {
    private val TEST_DB_NAME = "test-db"

    @Rule @JvmField
    var helper = MigrationTestHelper(InstrumentationRegistry.getInstrumentation(),
            AppDatabase::class.java.canonicalName,
            FrameworkSQLiteOpenHelperFactory())

    @Rule @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Helper for creating SQLite database in version 1
    private var mSqliteTestDbHelper: SqliteTestDbOpenHelper? = null
    lateinit var db: SupportSQLiteDatabase

    @Before
    fun setUp() {
//        mSqliteTestDbHelper = SqliteTestDbOpenHelper(ApplicationProvider.getApplicationContext(),
//                TEST_DB_NAME)
//
//        SqliteDatabaseTestHelper.createTable(mSqliteTestDbHelper!!)
    }

//    @After
//    fun tearDown() {
//        mSqliteTestDbHelper?.let { SqliteDatabaseTestHelper.clearDatabase(it) }
//    }

    @Test
    fun migrate2To3() {
        db = helper.createDatabase(TEST_DB_NAME, 2)

        val values = ContentValues()
        values.put("unitId", "test")
        values.put("username", "test")
        values.put("passwordMD5", "test")
        values.put("familyId", "test")

        val id = db.insert("orvibo_home", SQLiteDatabase.CONFLICT_REPLACE, values)

        db.close()

        db = helper.runMigrationsAndValidate(TEST_DB_NAME, 3, true, MIGRATION_1_2, MIGRATION_2_3)
        // Prepare for the
        val latestDb = getMigratedRoomDatabase()

//        latestDb.orviboDao().getOrviboHome("test").observeForever {
//            assertEquals(it.unitId, "test")
//        }

        latestDb.notificationDao()
        assertEquals(true, true)

//        assertEquals(true, true)
//        if (mSqliteTestDbHelper != null) {
//            SqliteDatabaseTestHelper.insertOrviboHome(mSqliteTestDbHelper!!)
////
//            mMigrationTestHelper.runMigrationsAndValidate(TEST_DB_NAME, 2, true,
//                    MIGRATION_1_2)
//            // Get the latest, migrated, version of the database
//            val latestDb = getMigratedRoomDatabase()
////
////            // Check that the correct data is in the database
//            latestDb.orviboDao().getOrviboHome("test").observeForever {
//                assertEquals(it.unitId, "test")
//            }
////
//        }
    }

    @Test
    fun migrate3To4() {
        db = helper.createDatabase(TEST_DB_NAME, 3)
        db = helper.runMigrationsAndValidate(TEST_DB_NAME, 4, true, MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)

        val values = ContentValues()
        values.put("title", "test")
        values.put("isRead", 0)
        val id = db.insert("notification", SQLiteDatabase.CONFLICT_REPLACE, values)

        db.close()

        // Prepare for the
        val latestDb = getMigratedRoomDatabase()

        latestDb.notificationDao().getNotifications().observeForever {
            assertEquals("test", it.getOrNull(0)?.title)
        }
    }

    private fun getMigratedRoomDatabase(): AppDatabase {
        val database = Room.databaseBuilder(ApplicationProvider.getApplicationContext(),
                AppDatabase::class.java, TEST_DB_NAME)
                .addMigrations(MIGRATION_1_2 , MIGRATION_2_3, MIGRATION_3_4)
                .build()
        // close the database and release any stream resources when the test finishes
        helper.closeWhenFinished(database)
        return database
    }

}
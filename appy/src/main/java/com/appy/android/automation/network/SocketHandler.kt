package com.appy.android.automation.network

import android.util.Log
import java.util.*

/**
 * Created by Tar on 3/17/16.
 */
open class SocketHandler : Thread() {

    open var message: String? = null
        protected set
    var address: String? = null
        protected set
    var port: Int = 0
        protected set
    var errorCode = NO_ERROR
        protected set

    var connectionTimeOut: Long = 3000
        set(value) {
            if (value < 1000) {
                field = 1000
            } else {
                field = value
            }

            scheduleTimeOut()
        }

    protected var terminated = false
    var responses = ArrayList<String>()
        protected set
    protected var scheduleTimer: Timer? = null

    val totalResponses: Int
        get() = responses.size

    protected fun setErrorCode(e: Exception?, errorCode: Int) {
        if (e != null) {
            Log.w("APH", "Exception : " + e.message)
            for (s in e.stackTrace) {
                Log.w("APH", "  " + s.toString())
            }
        }

        if (terminated) {
            return
        }

        this.errorCode = errorCode
    }

    protected fun scheduleTimeOut() {
        if (scheduleTimer != null) {
            scheduleTimer!!.cancel()
            scheduleTimer = null
        }

        scheduleTimer = Timer()
        scheduleTimer!!.schedule(object : TimerTask() {
            override fun run() {
                terminate(CONNECTION_TIMEOUT)
            }
        }, connectionTimeOut)
    }

    fun resetConnectionTimeOut() {
        connectionTimeOut = connectionTimeOut
        // setConnectionTimeOut(connectionTimeOut)
    }

    open fun terminate() {
        if (scheduleTimer != null) {
            scheduleTimer!!.cancel()
            scheduleTimer = null
        }

        terminated = true
    }

    fun terminate(errorCode: Int) {
        this.errorCode = errorCode
        terminate()
    }

    fun hasError(): Boolean {
        return errorCode != NO_ERROR
    }

    companion object {
        @JvmField val NO_ERROR = 1000
        @JvmField val CONNECTION_ERROR = 1001
        @JvmField val CONNECTION_TIMEOUT = 1002
        @JvmField val FAILED_TO_READ = 1003
        @JvmField val FAILED_TO_WRITE = 1004
        @JvmField val AUTHENTICATION_FAILED = 1005
        @JvmField val DISCONNECTED_BY_PEER = 1006
    }
}
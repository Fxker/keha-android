package com.sansiri.homeservice.ui.myaccount_v2.invoice.payment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.epayment.barcode.QRContract
import com.sansiri.homeservice.ui.epayment.barcode.QRPresenter
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_barcode.*
import com.sansiri.homeservice.ui.epayment.qr.QRActivity



class MyAccountInvoiceEPaymentQRActivity : QRActivity("MY_ACCOUNT_INVOICE_EPAYMENT_QR") {

    override var mPresenter: QRContract.Presenter = QRPresenter()
    var mInvoice: Invoice? = null
    var mHome: Hut? = null

    companion object {
        const val INVOICE = "INVOICE"
        const val HOME = "HOME"

        fun start(activity: Activity, home: Hut, invoice: Invoice) {
            val intent = Intent(activity, MyAccountInvoiceEPaymentQRActivity::class.java)
            intent.putExtra(INVOICE, invoice)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buttonDownload.hide()

        mInvoice = intent.getParcelableExtra(INVOICE)
        mHome = intent.getParcelableExtra(HOME)

        renderQRCode(mInvoice?.paymentBarcode)
    }

    override fun getDetailBitmap(): Bitmap = createDetailBitmap(this, mHome, mInvoice)

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT_INVOICE_EPAYMENT_QR")
    }

    override fun showError(message: String) {
    }
}

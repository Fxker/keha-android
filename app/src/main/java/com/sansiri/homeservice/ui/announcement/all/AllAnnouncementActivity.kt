package com.sansiri.homeservice.ui.announcement.all

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class AllAnnouncementActivity : BaseActivity<AllAnnouncementContract.View, AllAnnouncementContract.Presenter>(), AllAnnouncementContract.View {
    override var mPresenter: AllAnnouncementContract.Presenter = AllAnnouncementPresenter()
    private var mTitle: String? = null

    val mAdapter = AllAnnouncementAdapter() { view, announcement ->
        ContentActivity.start(this, view, mTitle ?: "", announcement)
    }

    companion object {
        val TITLE = "TITLE"
        val ID = "ID"

        fun start(activity: Activity, title: String, id: String) {
            val intent = Intent(activity, AllAnnouncementActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(ID, id)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        mTitle = intent?.getStringExtra(TITLE)
        setBackToolbar(mTitle ?: resources.getString(R.string.announcement))

        val id = intent?.getStringExtra(ID)
        mPresenter.init(id)
        mPresenter.fetchData(0)

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@AllAnnouncementActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
            itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
            itemAnimator?.addDuration = 700
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(currentPage: Int) {
                    mPresenter.fetchData(currentPage)
                }
            })
        }



        // mPlanAdapter.setData(AnnouncementMock.sansiriFamily)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("ALL_ANNOUNCEMENT")
    }

    override fun addData(list: List<Announcement<*>>) {
        // val sortedList = list.sortedByDescending { it.displayDate }
        mAdapter.addDataDelay(list)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(text: String) {
        alert(text, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

}

package com.sansiri.homeservice.ui.facilitybooking

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking

import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_facility_booking_info.*

class FacilityBookingInfoActivity : LocalizationActivity() {

    companion object {
        val FACILITY = "FACILITY"

        fun start(activity: Activity, facility: FacilityBooking) {
            val intent = Intent(activity, FacilityBookingInfoActivity::class.java)
            intent.putExtra(FACILITY, facility)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facility_booking_info)

        val facilityInfo = intent?.getParcelableExtra<FacilityBooking>(FACILITY)

        setBackToolbar(toolbar, "")

        if (facilityInfo != null) {
            Glide.with(this).load(facilityInfo.media?.get(0)?.image?.image).into(imageCover)
            textTitle.text = facilityInfo.getContent()?.title
            textDescription.text = facilityInfo.getContent()?.description
            if (facilityInfo.getContent()?.terms != null) {
                textTermAndCondition.text = facilityInfo.getContent()?.terms
            } else {
                titleTermAndCondition.hide()
                textTermAndCondition.hide()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}

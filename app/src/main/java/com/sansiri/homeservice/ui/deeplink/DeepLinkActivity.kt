package com.sansiri.homeservice.ui.deeplink

import android.os.Bundle
import com.sansiri.homeservice.R
import android.app.ActivityManager
import android.widget.Toast
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.main.MainActivity
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.model.event.CloseEvent
import com.sansiri.homeservice.ui.washingmachine.trendywash.TrendyWashMainActivity
import org.greenrobot.eventbus.EventBus


class DeepLinkActivity : BaseActivity<DeepLinkContract.View, DeepLinkContract.Presenter>(), DeepLinkContract.View {
    override var mPresenter: DeepLinkContract.Presenter = DeepLinkPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val uri = intent.data
        if (uri != null && uri.scheme == getString(R.string.app_scheme)) {
            if (uri.host == "app") {
                val feature = uri.pathSegments?.getOrNull(0)
                val action = uri.pathSegments?.getOrNull(1)
                if (feature != null) {
                    when (feature) {
                        "webview" -> {
                            if (action != null && action == "close") {
                                closeWeb()
                            } else {
                                mPresenter.requestLaunchWeb(uri)
                            }
                        }
                        "webview_token" -> mPresenter.requestLaunchWeb(uri, true)
                        "trendy_wash" -> {
                            if (action == "login") {
                                val customerId = uri.getQueryParameter("customer_id")
                                launchTrendyWash(customerId)
                            }
                        }
                    }

                    return
                }
            }
        }

        launchLatestActivityIfExist()
    }

    override fun launchWeb(url: String) {
        WebActivity.start(this, url, "")
        finish()
    }

    override fun launchWebWithToken(url: String) {
        WebActivity.start(this, url, "", true)
        finish()
    }

    override fun closeWeb() {
        EventBus.getDefault().post(CloseEvent(true))
        finish()
    }

    override fun launchTrendyWash(customerId: String?) {
        TrendyWashMainActivity.loggedIn(this, customerId)
        finish()
    }

    override fun launchLatestActivityIfExist() {
        run breaker@{
            val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
            val tasks = activityManager.getRunningTasks(10)
            for (task in tasks) {
                if (task.topActivity.className == localClassName) {
                    if (task.numActivities > 1) {
                        finish()
                        return@breaker
                    }
                }
            }
            MainActivity.start(this)
            finish()
        }
    }

    override fun showError(message: String) {
        launchLatestActivityIfExist()
        Toast.makeText(this, getString(R.string.error_default_message), Toast.LENGTH_LONG).show()
        finish()
    }
}

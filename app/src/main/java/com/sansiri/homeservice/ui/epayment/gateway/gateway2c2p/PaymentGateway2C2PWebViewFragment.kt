package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment

import com.ccpp.pgw.sdk.android.callback.TransactionResultCallback
import com.ccpp.pgw.sdk.android.core.authenticate.PGWJavaScriptInterface
import com.ccpp.pgw.sdk.android.core.authenticate.PGWWebViewClient
import com.ccpp.pgw.sdk.android.enums.APIResponseCode
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity

class PaymentGateway2C2PWebViewFragment : Fragment() {

    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-3ds-or-non-3ds#step-5
     */
    companion object {
        private val ARG_REDIRECT_URL = "ARG_REDIRECT_URL"

        fun newInstance(redirectUrl: String): PaymentGateway2C2PWebViewFragment {

            val fragment = PaymentGateway2C2PWebViewFragment()
            val args = Bundle()
            args.putString(ARG_REDIRECT_URL, redirectUrl)
            fragment.setArguments(args)

            return fragment
        }
    }

    val TAG = ""
    private var mRedirectUrl: String? = null

//    private var mMerchantServerSimulator: MerchantServerSimulator? = null

//    internal var mProgressDialog: ProgressDialog

    private val mTransactionResultCallback = object : TransactionResultCallback {

        override fun onResponse(response: TransactionResultResponse) {

            val paymentActivity = activity as PaymentGatewayActivity
            paymentActivity.update2c2p(response)
//            if (response.responseCode == APIResponseCode.TRANSACTION_COMPLETED) {
//
//            } else {
//                Log.e(TAG, response.responseCode + " :: " + response.responseDescription)
//            }
            fragmentManager?.popBackStack()
        }

        override fun onFailure(error: Throwable) {
            Log.e(TAG, error.message)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        mMerchantServerSimulator = MerchantServerSimulator()
//        mProgressDialog = UIHelper.getProgressDialog(getActivity())

        if (getArguments() != null) {
            mRedirectUrl = arguments?.getString(ARG_REDIRECT_URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val webview = WebView(activity)
        webview.settings.javaScriptEnabled = true
        webview.webViewClient = PGWWebViewClient()
        webview.addJavascriptInterface(PGWJavaScriptInterface(mTransactionResultCallback), PGWJavaScriptInterface.JAVASCRIPT_TRANSACTION_RESULT_KEY)
        webview.loadUrl(mRedirectUrl)

        return webview
    }

//    private fun buildPaymentInquiry(transactionID: String): PaymentInquiryRequest {
//
//        //Construct payment inquiry request
//        val request = PaymentInquiryRequest()
//        request.setTransactionID(transactionID)
//
//        return request
//    }

    private fun inquiry(transactionID: String) {

//        mProgressDialog.setMessage(getString(R.string.common_message_payment_result_message))
//        UIHelper.showProgressDialog(this@WebViewFragment, mProgressDialog)
//
//        //Step 6: Get payment result.
//        mMerchantServerSimulator!!.inquiryPaymentResult(buildPaymentInquiry(transactionID), object : HTTPResponseCallback() {
//
//            fun onResponse(response: String) {
//
//                UIHelper.dismissProgressDialog(this@WebViewFragment, mProgressDialog)
//
//                replaceFragment(TransactionResultFragment.newInstance(response))
//            }
//
//            fun onFailure(response: String) {
//
//                UIHelper.dismissProgressDialog(this@WebViewFragment, mProgressDialog)
//
//                Log.e(TAG, response)
//            }
//        })
    }


}

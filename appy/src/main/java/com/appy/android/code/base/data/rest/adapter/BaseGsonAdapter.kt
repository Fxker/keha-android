package com.appy.android.code.base.data.rest.adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonObject
import java.util.*

/**
 * Created by Tar on 9/20/16 AD.
 */
open class BaseGsonAdapter {

  open fun getInt(json: JsonObject, key: String): Int {
    if (hasValueForKey(json, key)) {
      return json.get(key).asInt
    }

    return 0
  }

  open fun getString(json: JsonObject, key: String): String? {
    if (hasValueForKey(json, key)) {
      return json.get(key).asString
    }

    return null
  }

  open fun getBoolean(json: JsonObject, key: String): Boolean {
    if (hasValueForKey(json, key)) {
      return json.get(key).asBoolean
    }

    return false
  }

  open fun getDouble(json: JsonObject, key: String): Double {
    if (hasValueForKey(json, key)) {
      return json.get(key).asDouble
    }

    return 0.0
  }

  open fun getLong(json: JsonObject, key: String): Long {
    if (hasValueForKey(json, key)) {
      return json.get(key).asLong
    }

    return 0
  }

  open fun getFloat(json: JsonObject, key: String): Float {
    if (hasValueForKey(json, key)) {
      return json.get(key).asFloat
    }

    return 0f
  }

  open fun getDate(json: JsonObject, key: String, context: JsonDeserializationContext): Date? {
    if (hasValueForKey(json, key)) {
      return context.deserialize<Date>(json.get(key), Date::class.java)
    }

    return null
  }

  private fun hasValueForKey(jsonObject: JsonObject, key: String): Boolean {
    return jsonObject.has(key) && jsonObject.get(key) != null
  }
}

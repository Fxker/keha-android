package com.appy.android.sdk.control.daikin

enum class DaikinFanDirectionCommand: APDaikinParameter {
    DIRECTION_0 {
        override fun action(): String {
            return "0"
        }
    },
    DIRECTION_1 {
        override fun action(): String {
            return "1"
        }
    },
    DIRECTION_2 {
        override fun action(): String {
            return "2"
        }
    },
    DIRECTION_3 {
        override fun action(): String {
            return "3"
        }
    },
    DIRECTION_4 {
        override fun action(): String {
            return "4"
        }
    },
    SWING {
        override fun action(): String {
            return "7"
        }
    };
}
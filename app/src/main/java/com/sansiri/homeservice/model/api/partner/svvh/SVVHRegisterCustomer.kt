package com.sansiri.homeservice.model.api.partner.svvh

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class SVVHRegisterCustomer(
        val firstname: String? = null,
        val lastname: String? = null,
        val citizenOrPassportId: String? = null,
        val birthDate: String? = null,
        val contactNumber: String? = null,
        val gender: String? = null,
        val redirectUrl: String? = null
) : Parcelable {

    val _birthDate: Date?
        get() {
            return try {
                if (this.birthDate == null) return null

                DateTime(this.birthDate).toDate()
            } catch (e: ParseException) {
                null
            }
        }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(firstname)
        writeString(lastname)
        writeString(citizenOrPassportId)
        writeString(birthDate)
        writeString(contactNumber)
        writeString(gender)
        writeString(redirectUrl)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SVVHRegisterCustomer> = object : Parcelable.Creator<SVVHRegisterCustomer> {
            override fun createFromParcel(source: Parcel): SVVHRegisterCustomer = SVVHRegisterCustomer(source)
            override fun newArray(size: Int): Array<SVVHRegisterCustomer?> = arrayOfNulls(size)
        }

        const val MALE = "MALE"
        const val FEMALE = "FEMALE"
    }
}
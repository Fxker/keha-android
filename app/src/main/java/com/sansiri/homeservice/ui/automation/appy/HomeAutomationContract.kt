package com.sansiri.homeservice.ui.automation.appy

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.daikin.APDaikinParameter
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/7/17.
 */
interface HomeAutomationContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(projectId: String?, homeId: String?, roomId: String?)
        fun action(control: APControl, commandNumber: Int)
        fun action(control: APControl, param: APDaikinParameter)
    }

    interface View : BaseView {
        fun bindData(controls: List<HomeAutomation>)
        fun showMessage(text: String)
        fun updateDevices()
        fun launchRemoteControl(control: APRemoteControl, projectId: String, homeId: String)
        fun showLoading()
        fun hideLoading()
        fun showLocalModeButton()
    }
}
package com.sansiri.homeservice.ui.adapter

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.withDelay

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class GridMenuHorizontalAdapter(val overlayColor: Int?, val backgroundColor: Int?, val spanCount: Double, val itemClick: (Menu) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<Menu>()
    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.view_menu_card)
        val width = parent.measuredWidth.toDouble() / spanCount
        view.layoutParams.width = width.toInt()
        return IconMenuViewHolder(
                overlayColor ?: ContextCompat.getColor(parent.context, R.color.colorSecondary),
                backgroundColor ?: Color.WHITE,
                ContextCompat.getColor(parent.context, R.color.colorAccent),
                view,
                itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is IconMenuViewHolder -> holder.bind(mData[position])
        }
    }

    fun setData(data: List<Menu>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    fun setDataDelay(data: List<Menu>) {
        mData.clear()
        data.forEach {
            {
                mData.add(it)
                notifyItemInserted(mData.size - 1)
            }.withDelay(1)
        }
    }
}
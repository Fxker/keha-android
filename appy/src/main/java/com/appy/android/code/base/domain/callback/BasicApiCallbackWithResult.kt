package com.appy.android.code.base.domain.callback

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
interface BasicApiCallbackWithResult<T> : BaseApiCallback {
    fun onSuccess(t: T)
}
package com.sansiri.homeservice.ui.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class CoroutineScopePresenter<V : BaseView>(): BasePresenterImpl<V>(), CoroutineScope {
    val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override fun start() {
    }

    override fun destroy() {
        job.cancel()
    }

}
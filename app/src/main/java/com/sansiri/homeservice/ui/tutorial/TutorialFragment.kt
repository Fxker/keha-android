package com.sansiri.homeservice.ui.tutorial

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.fragment_tutorial.*

/**
 * Created by sansiri on 4/5/18.
 */
class TutorialFragment : androidx.fragment.app.Fragment() {
    companion object {
        val CONTENT = "CONTENT"
        fun newInstance(content: TutorialContent): TutorialFragment {
            val fragment = TutorialFragment()
            val bundle = Bundle()
            bundle.putParcelable(CONTENT, content)
            fragment.arguments = bundle

            return fragment
        }
    }

    private var mContent: TutorialContent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContent = arguments?.getParcelable<TutorialContent>(CONTENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (mContent != null) {
            image.setImageResource(mContent!!.icon)
            textDescription.text = mContent!!.description
        }
    }
}
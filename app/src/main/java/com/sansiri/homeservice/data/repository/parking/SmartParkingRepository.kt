package com.sansiri.homeservice.data.repository.parking

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus

interface SmartParkingRepository {

    fun getSmartParkingStatus(unitId: String): MutableLiveData<Resource<SmartParkingStatus>>
    fun destroy()
}

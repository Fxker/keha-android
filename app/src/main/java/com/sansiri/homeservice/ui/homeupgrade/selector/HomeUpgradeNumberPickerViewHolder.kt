package com.sansiri.homeservice.ui.homeupgrade.selector

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_home_upgrade_number_picker.view.*
import kotlinx.android.synthetic.main.view_menu_card_no_title.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class HomeUpgradeNumberPickerViewHolder(val view: View, val itemSelected: (HomeUpgradeHardware) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    var number = 0
        set(value) {
            view.textNumber.text = value.toString()
            field = value
        }

    fun bind(data: HomeUpgradeHardware) {
        with(view) {
            textTitle.text = data.name
            textDescription.text = data.description
            Glide.with(this).loadCache(data.imageUrl ?: "", imageLogo as ImageView)

            toggleLayout.addRelatedChild(textTitle, textDescription, imageLogo, textNumber)

            logo.setOnClickListener {
                increaseAmount(data)
            }

            buttonIncrease.setOnClickListener {
                increaseAmount(data)
            }

            buttonDecrease.setOnClickListener {
                if (number > 0) {
                    number -= 1
                }

                if (number == 0) {
                    toggleLayout.isChecked = false
                }

                data.amount = number
                itemSelected(data)
            }

            toggleLayout.isChecked = data.amount > 0
            textNumber.text = data.amount.toString()
        }
    }

    private fun increaseAmount(data: HomeUpgradeHardware) {
        with(view) {
            number += 1
            toggleLayout.isChecked = true
            data.amount = number
            itemSelected(data)
        }
    }

}
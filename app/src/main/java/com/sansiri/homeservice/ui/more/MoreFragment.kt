package com.sansiri.homeservice.ui.more

import android.app.ProgressDialog
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.FeatureConfig
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.language.LanguageActivity
import com.sansiri.homeservice.ui.lounge.QRFragment
import com.sansiri.homeservice.ui.more.googleassistant.GoogleAssistantActivity
import com.sansiri.homeservice.ui.notificationsettings.NotificationSettingsActivity
import com.sansiri.homeservice.ui.profile.ProfileActivity
import com.sansiri.homeservice.ui.welcome.WelcomeActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.fragment_more.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import android.webkit.CookieSyncManager
import android.webkit.CookieManager


/**
 * Created by sansiri on 10/24/17.
 */
class MoreFragment : BaseFragment<MoreContract.View, MoreContract.Presenter>(), MoreContract.View {
    override var mPresenter: MoreContract.Presenter = MorePresenter()

    companion object {
        fun newInstance(): MoreFragment {
            return MoreFragment()
        }
    }

    val mMenuAdapter = GridMenuAdapter(null, null) {
        mPresenter.selectMenu(it)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_more)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listMenu.apply {
            layoutManager = androidx.recyclerview.widget.GridLayoutManager(this.context, resources.getInteger(R.integer.grid_column))
            adapter = mMenuAdapter
        }
        mPresenter.start()

        val moreMenu = arrayListOf(
                GenericMenu(Feature.PROFILE, resources.getString(R.string.my_profile), null)
        )

        moreMenu.add(GenericMenu(Feature.LANGUAGE, resources.getString(R.string.language), null))
        moreMenu.add(GenericMenu(Feature.NOTIFICATION_SETTINGS, resources.getString(R.string.notification_settings), null))

        if (FeatureConfig.GOOGLE_ASSISTANT) {
            moreMenu.add(GenericMenu(Feature.CONNECT_GOOGLE_ASSISTANT, resources.getString(R.string.connect_to_google_assistant), null))
        }

        moreMenu.add(GenericMenu(Feature.SIGN_OUT, resources.getString(R.string.sign_out), null))

        bindMenu(moreMenu)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MORE")
    }

    override fun bindMenu(menu: List<GenericMenu>) {
        mMenuAdapter.setData(menu)
    }

    override fun launchProfile() {
        ProfileActivity.start(activity!!)
    }

    override fun launchSansiriLounge() {
        QRFragment.newInstance().show(fragmentManager!!, "QRDialogFragment")
    }

    override fun launchLanguage() {
        LanguageActivity.start(activity!!)
    }

    override fun launchNotificationSettings() {
        NotificationSettingsActivity.start(activity!!)
    }

    override fun launchGoogleAssistant() {
        GoogleAssistantActivity.start(activity!!)
    }

    private var mProgressDialog: ProgressDialog? = null

    override fun showSignOut() {
        mProgressDialog = activity?.indeterminateProgressDialog(getString(R.string.signing_out), null)
    }

    override fun hideSignOut() {
        mProgressDialog?.dismiss()
    }

    override fun signOut() {
        sendEvent("SIGN_OUT", "CLICK", null)
        showSignOut()
        FirebaseAuthManager.signOut(activity!!) {
            hideSignOut()
            WelcomeActivity.start(activity!!)
            activity?.finish()
        }
        clearCookie()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    private fun clearCookie() {
        CookieSyncManager.createInstance(context)
        CookieManager.getInstance().removeAllCookie()
    }
}
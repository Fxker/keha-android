package com.sansiri.homeservice.ui.automation.orvibo.home
//
//import android.view.View
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import com.bumptech.glide.Glide
//import com.homemate.sdk.model.HMFamily
//import com.sansiri.homeservice.R
//import com.sansiri.homeservice.util.glide.loadCache
//import com.sansiri.homeservice.util.inflate
//import kotlinx.android.synthetic.main.view_orvibo_familiy.view.*
//
//class OrviboFamilyAdapter(val itemClick: (HMFamily) -> Unit) : RecyclerView.Adapter<OrviboFamilyAdapter.ViewHolder>() {
//    private val mData = mutableListOf<HMFamily>()
//
//    override fun getItemCount(): Int = mData.size
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(parent.inflate(R.layout.view_orvibo_familiy), itemClick)
//    }
//
//    override fun onBindViewHolder(holder: OrviboFamilyAdapter.ViewHolder, position: Int) {
//        holder.bind(mData[position])
//    }
//
//    fun addData(data: List<HMFamily>) {
//        mData.clear()
//        mData.addAll(data)
//    }
//
//    class ViewHolder(val view: View, val itemClick: (HMFamily) -> Unit) : RecyclerView.ViewHolder(view) {
//        fun bind(family: HMFamily) {
//            with(view) {
//                family.pic?.let {
//                    Glide.with(view).loadCache(it, view.imageProfile)
//                }
//                textName.text = family.name
//                setOnClickListener {
//                    itemClick(family)
//                }
//            }
//        }
//    }
//
//}
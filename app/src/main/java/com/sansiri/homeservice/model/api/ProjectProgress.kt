package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

data class ProjectProgress(
        @SerializedName("progressOverAll")
        val progressoverall: Float,

        @SerializedName("shortTitle")
        val shorttitle: String? = null,

        @SerializedName("eia")
        val eia: String? = null,

        @SerializedName("consts")
        val consts: String? = null,

        @SerializedName("rtm")
        val rtm: String? = null,

        @SerializedName("progressStructure")
        val prostructure: Float,

        @SerializedName("progressStructureText")
        val prostructuretxt: String? = null,

        @SerializedName("progressSystem")
        val prosystem: Float,

        @SerializedName("progressSystemText")
        val prosystemtxt: String? = null,

        @SerializedName("progressArchitecture")
        val proarchitecture: Float,

        @SerializedName("progressArchitectureText")
        val proarchitecturetxt: String? = null,

        @SerializedName("imageUrl")
        val imageurl: String? = null,

        @SerializedName("imagesUrl")
        val imagesUrl: List<ImageUrl>? = null,

        @SerializedName("url")
        val url: String? = null
) {
        data class ImageUrl(val imageUrl: String)
}
package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.daikin.DaikinCommand
import com.appy.android.sdk.control.daikin.DaikinOperationModeCommand

internal class SendCommandDaikinOperationModeForm(
        val homeId: String,
        val daikinId: String,
        val commandValue: DaikinOperationModeCommand
) : AppyForm() {
    val mSendCommandDaikinCommandValueJsonBody: SendCommandDaikinCommandValueJsonBody = SendCommandDaikinCommandValueJsonBody()

    init {
        this.mSendCommandDaikinCommandValueJsonBody.command = DaikinCommand.OPERATION_MODE.action()
        this.mSendCommandDaikinCommandValueJsonBody.commandValue = commandValue.action()
    }
}
package com.sansiri.homeservice.ui.automation.bathome.control

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Section
import com.sansiri.homeservice.ui.automation.HomeAutomationAdapter
import com.sansiri.homeservice.ui.automation.appy.HomeAutomationActivity
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.base.onRequestPermissionsResult
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.androidx.scope.currentScope
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import vc.siriventures.bathome.model.BAtHomeComponent

class BAtHomeControlActivity : BaseV2Activity<BAtHomeControlContract.View, BAtHomeControlContract.Presenter>(), BAtHomeControlContract.View {
    override val mPresenter: BAtHomeControlContract.Presenter by currentScope.inject()


    companion object {
        const val TITLE = "TITLE"
        const val TYPE = "TYPE"
        val USERNAME = "USERNAME"
        val PASSWORD = "PASSWORD"
        val PROJECT_ID = "PROJECT_ID"

        fun start(activity: Activity, title: String?, type: String?, username: String?, password: String?, projectId: String?) {
            val intent = Intent(activity, BAtHomeControlActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(TYPE, type)
            intent.putExtra(USERNAME, username)
            intent.putExtra(PASSWORD, password)
            intent.putExtra(PROJECT_ID, projectId)
            activity.startActivity(intent)
        }
    }

    private val mMenuAdapter = HomeAutomationAdapter(R.color.colorAccent) { homeAutomation, command ->
        if (homeAutomation is BAtHomeAutomation) {
            sendEvent("B@HOME_HOME_AUTOMATION", "COMMAND", homeAutomation.component?.type)
            mPresenter.action(homeAutomation, command)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setBackToolbar(intent.getStringExtra(TITLE) ?: "")
//
//        listHardware.apply {
//            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(this@BAtHomeControlActivity, resources.getInteger(R.integer.grid_column))
//            gridLayoutManager.spanSizeLookup = object : androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup() {
//                override fun getSpanSize(position: Int): Int {
//                    return when (mAdapter.getSectionItemViewType(position)) {
//                        SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> resources.getInteger(R.integer.grid_column)
//                        else -> 1
//                    }
//                }
//            }
//
//            layoutManager = gridLayoutManager
//            adapter = mAdapter
//        }

        val flexboxLayoutManager = FlexboxLayoutManager(this).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            alignItems = AlignItems.STRETCH
        }

        list.apply {
            layoutManager = flexboxLayoutManager
            adapter = mMenuAdapter
        }

        mPresenter.init(
                this,
                intent.getStringExtra(USERNAME),
                intent.getStringExtra(PASSWORD),
                intent.getStringExtra(PROJECT_ID),
                intent.getStringExtra(TYPE)
        )

        mPresenter.start()
    }


    override fun onResume() {
        super.onResume()
        sendScreenView("B@HOME_HOME_AUTOMATION_CONTROL")
    }


    override fun bindData(controls: List<BAtHomeAutomation>) {
        mMenuAdapter.setData(controls)
    }

    //    override fun bindData(controls: List<BAtHomeControlSection>) {
//        mMenuAdapter.setData(controls)
////        list.forEach {
////            mAdapter.addSection(MenuSectionAdapter(R.color.colorSecondary, it.displayName ?: "", it.items!!) { menu ->
////                if (menu is DynamicMenu) {
//////                        actionExtraMenu(menu)
////                }
////            })
////        }
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(text: String) {
        if (!BuildConfig.BUILD_TYPE.contains("release")) {
            snack(text)
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoDevice() {
        textNoData.text = "No Device"
    }

    override fun notifyList() {
        mMenuAdapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
        snackError(message)
    }
}

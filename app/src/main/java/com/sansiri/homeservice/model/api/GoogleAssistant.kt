package com.sansiri.homeservice.model.api

data class GoogleAssistant(
	var googleId: String? = null,
	var unitId: String? = null,
	var createdAt: String? = null,
	var projectName: String? = null,
	var removedAt: String? = null,
	var email: String? = null,
	var unitAddress: String? = null,
	var updatedAt: String? = null
)

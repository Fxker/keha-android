package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by sansiri on 12/12/17.
 */
@Parcelize
data class Section(
        val sectionName: String? = null,
        val displayName: String? = null,
        var items: MutableList<DynamicMenu>? = null
) : Parcelable
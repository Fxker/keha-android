package com.sansiri.homeservice.ui.mailbox.sandee

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.TimeDialogFragment
import com.sansiri.homeservice.component.bottomsheet.DatePickerBottomSheet
import com.sansiri.homeservice.component.bottomsheet.TimePickerBottomSheet
import com.sansiri.homeservice.model.api.fcm.FCMBody
import com.sansiri.homeservice.model.api.fcm.Data
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import kotlinx.android.synthetic.main.dialog_sandee_feedback.*
import kotlinx.android.synthetic.main.dialog_general.*
import java.util.*


/**
 * Created by sansiri on 10/12/17.
 */
class SandeeFragmentDialog() : TimeDialogFragment() {
    var callback: Listener? = null

    interface Listener {
        fun onAppointmentSelected(date: String, time: String)
    }

    override val onClickListener: () -> Unit = {
        //        ApiRepositoryProvider.provideFCMRepository() {
//            it?.sendMessage(FCMBody(
//                    "/topics/CALL_SANDEE",
//                    Data(
//                            "รบกวนส่งแสนดีมาด้วยครับ",
//                            "${textDate.text} ${textTime.text}"
//                    )
//            ))?.subscribe { }
//        }
        callback?.onAppointmentSelected(textDate.text.toString(), textTime.text.toString())
        dismiss()
    }

    private val TAG: String = "SandeeFragmentDialog"

    private lateinit var mTimePicker: TimePickerBottomSheet
    private lateinit var mDatePicker: DatePickerBottomSheet


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_sandee_feedback, null)
        panelContent.addView(view)

        buttonSubmit.text = getString(R.string.request_delivery)

        val now = Calendar.getInstance()

        textTime.text = getTextTime(now)
        textDate.text = getTextDate(now)

        mTimePicker = TimePickerBottomSheet(now) { time ->
            textTime.text = getTextTime(time)
        }

        val today = Calendar.getInstance()
        val nextDay = Calendar.getInstance().apply { add(Calendar.DATE, 7) }

        mDatePicker = DatePickerBottomSheet(now, false, today, nextDay) { date ->
            textDate.text = getTextDate(date)
        }

        textDate.setOnClickListener {
            mDatePicker.show(fragmentManager!!, TAG)
        }
        textTime.setOnClickListener {
            mTimePicker.show(fragmentManager!!, TAG)
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SANDEE_REQUEST")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callback = context as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement Listener")
        }
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

}
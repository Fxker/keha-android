package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable

data class Reservation(
		val dates: List<DatesItem>? = null,
		val rules: Rules? = null,
		val id: Int? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.createTypedArrayList(DatesItem.CREATOR),
			source.readParcelable<Rules>(Rules::class.java.classLoader),
			source.readValue(Int::class.java.classLoader) as Int?
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeTypedList(dates)
		writeParcelable(rules, 0)
		writeValue(id)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Reservation> = object : Parcelable.Creator<Reservation> {
			override fun createFromParcel(source: Parcel): Reservation = Reservation(source)
			override fun newArray(size: Int): Array<Reservation?> = arrayOfNulls(size)
		}
	}
}

package com.sansiri.homeservice.ui.homecare.history

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.model.api.ListResponse
import com.sansiri.homeservice.model.api.homecare.ListHomeCareResponse
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.homecare.create.HomeCareCreateActivity
import com.sansiri.homeservice.ui.homecare.history.detail.HomeCareHistoryDetailActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_homecare_history.*
import java.util.*


class HomeCareHistoryActivity : BaseActivity<HomeCareHistoryContract.View, HomeCareHistoryContract.Presenter>(), HomeCareHistoryContract.View {
    override var mPresenter: HomeCareHistoryContract.Presenter = HomeCareHistoryPresenter()

    val mAdapter = HomeCareHistoryAdapter {
        HomeCareHistoryDetailActivity.start(this, it, getString(R.string.details))
    }

    companion object {
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"
        val TITLE = "TITLE"
        fun start(activity: Activity, unitObjectId: String, title: String?) {
            val intent = Intent(activity, HomeCareHistoryActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    private var mUnitObjectId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homecare_history)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.home_care))

        textNoData.text = getString(R.string.no_homecare)

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@HomeCareHistoryActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(currentPage: Int) {
                    mPresenter.fetchData(currentPage)
                }
            })
        }

        mUnitObjectId = intent?.getStringExtra(UNIT_OBJECT_ID)
        if (mUnitObjectId != null) {
            mPresenter.init(mUnitObjectId!!)
            mPresenter.fetchData(0)
        } else {
            showNoData()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("HOMECARE_HISTORY")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                HomeCareCreateActivity.CODE -> {
                    mAdapter.clear()
                    mPresenter.fetchData(0)
                }
            }
        }
    }

    override fun showError(message: String) {
        alertError(message)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        list.hide()
        textNoData.show()
    }

    override fun addData(histories: ListHomeCareResponse<HomeCareHistory>) {
        list.show()
        textNoData.hide()
        mAdapter.addDataDelay(histories.items)
    }

    override fun showExpiredDate(expiredDate: Date?) {
        if (expiredDate != null) {
            textExpire.text = "${getString(R.string.warranty_expires)}: ${expiredDate.reportDMYNumber()}"
        } else {
            textExpire.hide()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_create_new, menu)
        menu.findItem(R.id.action_create).tint(ContextCompat.getColor(this, R.color.textAccent))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_create) {
            sendEvent("HOMECARE", "CLICK", "HOMECARE_CREATE")
            HomeCareCreateActivity.start(this, mUnitObjectId ?: "")
        }

        return super.onOptionsItemSelected(item)
    }
}

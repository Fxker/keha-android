package com.sansiri.homeservice.ui.appointment

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Inspection
import com.sansiri.homeservice.util.hide
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 11/1/17.
 */

class AppointmentViewHolder(val view: View, val itemClick: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(inspection: Inspection) {
        with(view) {
            imageIcon.setImageResource(R.drawable.ic_home)
            textDate.text = inspection.status
            textTitle.text = inspection.title
            textSubtitle.text = inspection.subTitle
            textDate.hide()
        }

    }

}
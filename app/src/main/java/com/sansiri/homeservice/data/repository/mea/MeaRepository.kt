package com.sansiri.homeservice.data.repository.mea

import com.mea.energysdk.service.MEAEnergyError
import com.mea.energysdk.widget.MEAEnergyWidget
import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.squareup.okhttp.Response
import com.squareup.okhttp.ResponseBody
import io.reactivex.Completable
import io.reactivex.Observable
import org.json.JSONArray

interface MeaRepository {
    fun showReport(widget: MEAEnergyWidget, ca: String)
    fun getMEAInfo(unitId: String): Observable<MEAInfo>
    fun registerCA(ca: String, projectId: String, meaInfo: MEAInfo?, onSuccess: () -> Unit, onError: (JSONArray?) -> Unit)
    fun saveCA(unitId: String, ca: String): Observable<MEAInfo>
    fun getConsent(): Observable<retrofit2.Response<Consent>>
    fun acceptConsent(): Completable
    fun getCAFromQRCode(code: String): String?
}
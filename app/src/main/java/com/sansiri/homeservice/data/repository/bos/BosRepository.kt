package com.sansiri.homeservice.data.repository.bos

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.sansiri.homeservice.model.api.ListResponse
import com.sansiri.homeservice.model.api.bos.BosDetail
import com.sansiri.homeservice.model.menu.BosMenu
import com.squareup.okhttp.Response
import com.squareup.okhttp.ResponseBody
import io.reactivex.Completable
import io.reactivex.Observable
import org.json.JSONObject

interface BosRepository {
    fun getBosType(unitId: String): Observable<ListResponse<BosMenu>>
    fun submitCommonAreaIssue(unitId: String, bosDetail: BosDetail): Completable
    fun submitMyHomeIssue(unitId: String, bosDetail: BosDetail): Completable
}
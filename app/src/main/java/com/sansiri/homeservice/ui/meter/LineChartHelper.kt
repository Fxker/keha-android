package com.sansiri.homeservice.ui.meter

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat

import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.MPPointF
import com.sansiri.homeservice.R

import java.util.ArrayList
import kotlin.math.roundToInt


/**
 * Created by oakraw on 10/8/2016 AD.
 */

class LineChartHelper(builder: LineChartBuilder) {
    private val mContext: Context
    private val mChart: LineChart
    private val mDataSets: ArrayList<ILineDataSet>

    init {
        mContext = builder.mContext
        mChart = builder.mChart
        mDataSets = builder.mDataSets

        drawChart()
    }

    fun drawChart() {
        val lineData = LineData(mDataSets)
        mChart.viewPortHandler.setDragOffsetX(80f)
        mChart.data = lineData
        mChart.invalidate()
    }

    fun drawChart(vararg chartOrder: Int) {
        val tmpDataSets = ArrayList<ILineDataSet>()
        for (order in chartOrder) {
            tmpDataSets.add(mDataSets[order])
        }
        val lineData = LineData(tmpDataSets)
        mChart.viewPortHandler.setDragOffsetX(80f)
        mChart.setData(lineData)
        mChart.invalidate()
    }

    class LineChartBuilder(val mContext: Context, val mChart: LineChart, val markerRes: Int? = null) {
        private val mTypeface: Typeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.graphik_th_regular), Typeface.NORMAL)
        val mDataSets = ArrayList<ILineDataSet>()


        init {
            mChart.apply {
                setHardwareAccelerationEnabled(false)
                axisRight.isEnabled = false
                axisRight.setDrawAxisLine(false)
                axisRight.setDrawGridLines(false)
                axisRight.axisMinimum = 0f
                axisLeft.isEnabled = false
                axisLeft.setDrawAxisLine(false)
                axisLeft.setDrawGridLines(false)
                axisLeft.axisMinimum = 0f
                xAxis.setDrawAxisLine(false)
                xAxis.setDrawGridLines(false)
                xAxis.isGranularityEnabled = true
                xAxis.position = XAxis.XAxisPosition.BOTTOM
                xAxis.typeface = mTypeface
                description = Description().apply { text = "" }
                legend.isEnabled = false
                setPinchZoom(false)
                isDoubleTapToZoomEnabled = false
                if (markerRes != null) {
                    mChart.marker = CustomMarkerView(mContext, markerRes)
                }
                setNoDataText(mContext.getString(R.string.no_data))
            }
        }

        fun createDataSet(entries: List<Entry>, label: String, lineColor: Int, fillLineDrawable: Drawable, highlightColor: Int): LineChartBuilder {

            val dataSet = LineDataSet(entries, label).apply {
                setColor(lineColor)
                lineWidth = 2.5f
                setDrawFilled(true)
                fillDrawable = fillLineDrawable //ContextCompat.getDrawable(mContext, R.drawable.graph_accent_fade)
                mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                highLightColor = highlightColor
                setDrawValues(false)
                setDrawCircles(false)
            }

            mChart.xAxis.valueFormatter = object : IndexAxisValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return value.roundToInt().toString()
                }
            }

            entries.getOrNull(0)?.let { mChart.xAxis.axisMinimum = it.x }

            mDataSets.add(dataSet)

            return this
        }

        fun build(): LineChartHelper {
            return LineChartHelper(this)
        }
    }

    class CustomMarkerView(context: Context, markerRes: Int) : MarkerView(context, markerRes) {
        private var mTextValue: TextView? = null
        private var mOffset: MPPointF? = null

        init {
            mTextValue = findViewById(R.id.textValue)
        }

        override fun refreshContent(e: Entry?, highlight: Highlight?) {
            mTextValue?.text = e?.y.toString()
            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF {
            if (mOffset == null) {
                // center the marker horizontally and vertically
                mOffset = MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
            }
            return mOffset!!
        }
    }

//    class IndexAxisValueFormatter(val labels: List<String>) : ValueFormatter() {
//        override fun getFormattedValue(value: Float): String {
//            Log.d("oakraw", value.toString())
//            val index = value.roundToInt()
//            return if (index < 0 || index >= labels.size) "" else labels[index]
//        }
//    }


}

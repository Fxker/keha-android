package com.sansiri.homeservice.data.repository.gateway2c2p

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard

interface Gateway2c2pRepository {

    interface Listener {
        fun onRequestWeb(url: String)
        fun onTransactionSuccess(transacnId: String)
        fun onFailure(error: Throwable)
    }

    fun init(merchantId: String,  listener: Listener)
    fun execute(token: String)
    fun createCreditCard(cardName: String, cardNumber: String, ccv: String, expiryMonth: Int, expiryYear: Int, isRemember: Boolean, email: String? = null)
    fun createCreditCard(cardToken: String, ccv: String, email: String? = null)
    fun inquiryTransaction(transactionId: String): MutableLiveData<Resource<Transaction2C2PDetail>>
    @WorkerThread
    suspend fun saveCard(creditCard: SavingCreditCard)

    fun getSavingCreditCards(): LiveData<List<SavingCreditCard>>
    @WorkerThread
    suspend fun deleteCard(cardToken: String)
}

package com.sansiri.homeservice.ui.aqi

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import com.robinhood.ticker.TickerUtils
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.aqi.AirPollutant

import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.synthetic.main.view_air_pollutant.view.*
import org.jetbrains.anko.backgroundColor

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class AirPollutantAdapter() : androidx.recyclerview.widget.RecyclerView.Adapter<AirPollutantAdapter.ViewHolder>() {
    private val mData = mutableListOf<AirPollutant>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirPollutantAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_air_pollutant))
    }

    override fun onBindViewHolder(holder: AirPollutantAdapter.ViewHolder, position: Int) {
        holder.bindData(mData[position])
    }

    fun setData(data: List<AirPollutant>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bindData(polllutant: AirPollutant) {
            with(view) {
                textType.text = polllutant.title
                textUnit.text = polllutant.unit
                textAmount.setCharacterLists(TickerUtils.provideNumberList())
                textAmount.setText(polllutant.value?.toCurrencyFormat(), true)
                viewIndicator.backgroundColor = Color.parseColor(polllutant.color)
            }
        }
    }
}
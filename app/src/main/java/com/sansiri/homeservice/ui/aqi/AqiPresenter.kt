package com.sansiri.homeservice.ui.aqi

import android.util.Log
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.data.network.firebase.FireStoreManager
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.chat.ChatMessage.Companion.TYPE_IMAGE
import com.sansiri.homeservice.model.api.chat.ChatMessage.Companion.TYPE_TEXT
import com.sansiri.homeservice.model.api.chat.Sender
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

/**
 * Created by sansiri on 10/18/17.
 */
class AqiPresenter : BasePresenterImpl<AqiContract.View>(), AqiContract.Presenter {
    var mProjectId: String? = null

    override fun init(projectId: String) {
        mProjectId = projectId
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        mView?.showLoading()

        val api = ApiRepositoryProvider.provideApiUnAuthRepository()
        mCompositeDisposable.add(
                api.getAQI(mProjectId ?: "").observe().subscribe({ air ->
                    mView?.hideLoading()
                    mView?.bindData(air)

                    // update aqi in cache
                    RuntimeCache.projects.find { it.objectId == mProjectId }?.air = air
                }) {
                    mView?.showError(it.showHttpError())
                }
        )
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

}
package com.sansiri.homeservice.ui.parking.smartparking

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.parking.SmartParkingRepository
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.ui.base.CoroutineScopePresenter

class SmartParkingPresenter(val repository: SmartParkingRepository) : CoroutineScopePresenter<SmartParkingContract.View>(), SmartParkingContract.Presenter {
    override var smartParkingStatus: MutableLiveData<Resource<SmartParkingStatus>>? = null
    private var mUnitId: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        repository.destroy()
        super.destroy()

    }

    override fun fetch() {
        if (mUnitId != null) {
            smartParkingStatus = repository.getSmartParkingStatus(mUnitId!!)
        }
    }

}
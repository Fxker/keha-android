package com.appy.android.code.domain.model.command.ac

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
internal class GetAirConditionersForm(
        internal val homeId: String
) : AppyForm()
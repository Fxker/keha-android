package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable

data class HMHub(
        val id: String? = null,
        val status: Int? = 0
) : Parcelable {
    class OnlineStatus {
        companion object {
            val ONLINE = 1
            val OFFLINE = 0
            val NOT_EXIST = 2
        }
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeValue(status)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMHub> = object : Parcelable.Creator<HMHub> {
            override fun createFromParcel(source: Parcel): HMHub = HMHub(source)
            override fun newArray(size: Int): Array<HMHub?> = arrayOfNulls(size)
        }
    }
}
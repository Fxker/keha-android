package com.appy.android.code.domain.model.status.blind

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-04 on 19/1/2561.
 */
internal class GetStatusBlindForm(
        internal val homeId: String,
        internal val blindId: String
) : AppyForm()
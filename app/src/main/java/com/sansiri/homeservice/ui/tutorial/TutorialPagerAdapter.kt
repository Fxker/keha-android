package com.sansiri.homeservice.ui.tutorial

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Created by sansiri on 4/5/18.
 */
class TutorialPagerAdapter(fm: androidx.fragment.app.FragmentManager): androidx.fragment.app.FragmentPagerAdapter(fm) {
    val items = mutableListOf<TutorialContent>()

    override fun getItem(position: Int): androidx.fragment.app.Fragment = TutorialFragment.newInstance(items[position])

    override fun getCount(): Int = items.size

    fun setItem(content: List<TutorialContent>) {
        items.clear()
        items.addAll(content)
        notifyDataSetChanged()
    }

}
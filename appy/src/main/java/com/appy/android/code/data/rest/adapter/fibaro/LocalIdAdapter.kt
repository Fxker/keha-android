package com.appy.android.code.data.rest.adapter.fibaro

import com.appy.android.code.base.data.rest.adapter.BaseGsonAdapter
import com.appy.android.code.data.rest.JsonModel.fibaro.LocalId
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */

internal class LocalIdAdapter : BaseGsonAdapter(), JsonDeserializer<LocalId> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): LocalId? {

        val deviceLocalId = LocalId()

        if (json.isJsonObject) {
            val jsonObject = json.asJsonObject
            jsonObject.keySet().forEach {
                deviceLocalId.ids.put(jsonObject.get(it).asString, it)
            }
        } else if (json.isJsonPrimitive) {
            deviceLocalId.ids.put(json.asString, json.asString)
        }

        return deviceLocalId
    }
}
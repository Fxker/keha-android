package com.sansiri.homeservice.ui.myaccount

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/31/17.
 */
class MyAccountPresenter : BasePresenterImpl<MyAccountContract.View>(), MyAccountContract.Presenter {

    override fun start() {
    }

    override fun fetchData(unitObjectId: String) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                val disposable = api.getBalance(unitObjectId)
                        .observe()
                        .subscribe({
                            mView?.bindData(it)
                        }, {
                            mView?.showError(it.showHttpError())
                        })

                mCompositeDisposable.add(disposable)

                mCompositeDisposable.add(
                        api.getInvoicePayable(unitObjectId)
                                .observe()
                                .subscribe({
                                    mView?.bindList(it)
                                }){}
                )
            } else {
                mView?.showError("Network Error")
            }
        }
    }

    override fun destroy() {

    }
}
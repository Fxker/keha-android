package com.appy.android.code.base.domain.interactor

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactorInterface.NoInternetConnectionInterface
import com.appy.android.code.data.rest.RestClient
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.functions.Function
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal abstract class AppyRestInteractor<T, Params>(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               protected var restClient: RestClient) : AppyInteractor<T, Params>(threadExecutor, postExecutionThread) {
    protected var noInternetConnectionInterface: NoInternetConnectionInterface? = null
    var retryWhenFunction: io.reactivex.functions.Function<Observable<Throwable>, ObservableSource<*>> = Function { throwableObservable ->
        throwableObservable.flatMap { throwable ->
            if (noInternetConnectionInterface != null && (throwable is HttpException || throwable is IOException)) {
                noInternetConnectionInterface!!.noInternetConnection(throwable)
            } else {
                Observable.error<Int>(throwable)
            }
        }
    }

    abstract fun buildApiUseCaseObservable(params: Params): Observable<T>

    override fun buildUseCaseObservable(params: Params): Observable<T> {
        return buildApiUseCaseObservable(params).retryWhen(retryWhenFunction)
    }

    fun setNoInternetConnectionInterfaceListener(
            noInternetConnectionInterface: NoInternetConnectionInterface) {
        this.noInternetConnectionInterface = noInternetConnectionInterface
    }
}
package com.sansiri.homeservice.data.repository.payment

import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import io.reactivex.Observable

interface PaymentRepository {
    fun requestPayment(request: PaymentRequest): Observable<PaymentResponse>
}
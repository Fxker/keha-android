package com.sansiri.homeservice.ui.automation.bathome.control

import android.app.Activity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import vc.siriventures.bathome.BAtHomeSDK
import vc.siriventures.bathome.model.BAtHomeComponent

class BAtHomeControlPresenter(val bAtHomeSDK: BAtHomeSDK) : BasePresenterImpl<BAtHomeControlContract.View>(), BAtHomeControlContract.Presenter {
    private var mActivity: Activity? = null
    private var mUsername: String? = null
    private var mPassword: String? = null
    private var mProjectId: String? = null
    private var mType: String? = null

    private var mControls: MutableList<BAtHomeAutomation>? = null

    override fun init(activity: Activity, username: String?, password: String?, projectId: String?, type: String?) {
        mActivity = activity
        mUsername = username
        mPassword = password
        mProjectId = projectId
        mType = type
    }

    override fun start() {
        if (mActivity != null && mProjectId != null && mUsername != null && mPassword != null && mType != null) {
            mView?.showLoading()

            bAtHomeSDK.init(mActivity!!, mProjectId!!, mUsername!!, mPassword!!, {
                bAtHomeSDK.login({
                    mView?.hideLoading()
                    fetch(mType)
                }) {
                    mView?.hideLoading()
                    mView?.showError(it.localizedMessage)
                }
            }) {
                mView?.hideLoading()
                mView?.showError(it.localizedMessage)
            }
        }
    }

    private val onFetchSuccess: ((List<BAtHomeComponent>) -> Unit) = {
        mView?.hideLoading()
        if (it.isEmpty()) {
            mView?.showNoDevice()
        } else {
            extractToGroup(it)
        }
    }

    private val onFetchError: ((Throwable) -> Unit) = {
        mView?.hideLoading()
        mView?.showError(it.localizedMessage)
    }

    override fun fetch(type: String?) {
        mView?.showLoading()
        when (type) {
            BAtHomeAutomation.TYPE_LIGHT -> {
                bAtHomeSDK.getLights(onFetchSuccess, onFetchError)
            }
            BAtHomeAutomation.TYPE_AC -> {
                bAtHomeSDK.getACs(onFetchSuccess, onFetchError)
            }
            BAtHomeAutomation.TYPE_SCENE -> {
                bAtHomeSDK.getScenes(onFetchSuccess, onFetchError)
            }
        }
    }

    private fun extractToGroup(components: List<BAtHomeComponent>) {
        val map = hashMapOf<String, MutableList<BAtHomeComponent>>()
        components.forEach { component ->
            val key = component.group ?: ""
            if (map[key] == null) {
                map[key] = mutableListOf(component)
            } else {
                map[key]!!.add(component)
            }
        }

        mControls = mutableListOf<BAtHomeAutomation>()

        map.toList().forEach { pair ->
            if (pair.first.isNotEmpty()) {
                mControls?.add(BAtHomeAutomation(pair.first, 0, HomeAutomation.Type.GROUP_TOGGLE, null))
            }
            mControls?.addAll(pair.second.map { component ->
                BAtHomeAutomation(component.description
                        ?: "", getIcon(component), getType(component), component)
            })
        }

        mControls?.let { mView?.bindData(it) }
    }

    private fun getIcon(component: BAtHomeComponent) = when (component.type) {
        BAtHomeComponent.TYPE_LIGHT -> R.drawable.ic_bulb
        BAtHomeComponent.TYPE_SCENE -> if (component.ac) R.drawable.ic_air_condition else R.drawable.ic_book
        else -> R.drawable.placeholder_menu
    }

    private fun getType(component: BAtHomeComponent) = when (component.type) {
        BAtHomeComponent.TYPE_LIGHT -> HomeAutomation.Type.TOGGLE
        BAtHomeComponent.TYPE_SCENE -> if (component.ac) HomeAutomation.Type.REMOTE_CONTROL else HomeAutomation.Type.SWITCH
        else -> HomeAutomation.Type.SWITCH
    }

    override fun action(homeAutomation: BAtHomeAutomation, command: Any) {
        if (homeAutomation.type == HomeAutomation.Type.GROUP_TOGGLE && command is Int) {
            actionToggleGroup(homeAutomation, command)
        } else if (homeAutomation.component != null && homeAutomation.component.type == BAtHomeComponent.TYPE_LIGHT) {
            actionLight(homeAutomation)
        } else if (homeAutomation.component?.type != null && homeAutomation.component.type!!.contains(BAtHomeComponent.TYPE_SCENE)) {
            actionScene(homeAutomation)
        }
    }

    private fun actionLight(homeAutomation: BAtHomeAutomation) {
        if (homeAutomation.component?.id != null) {
            if (homeAutomation.component.status == BAtHomeAutomation.ACTION_ON) {
                bAtHomeSDK.turnOnLight(homeAutomation.component.id!!, {
                    mView?.showMessage("Success")
                }) {
                    mView?.showError(it.localizedMessage)
                }
            } else {
                bAtHomeSDK.turnOffLight(homeAutomation.component.id!!, {
                    mView?.showMessage("Success")
                }) {
                    mView?.showError(it.localizedMessage)
                }
            }
        }
    }

    private fun actionScene(homeAutomation: BAtHomeAutomation) {
        if (homeAutomation.component?.id != null) {
            bAtHomeSDK.turnOnScene(homeAutomation.component.id!!, {
                mView?.showMessage("Success")
            }) {
                mView?.showError(it.localizedMessage)
            }
        }
    }

    private fun actionToggleGroup(homeAutomation: BAtHomeAutomation, command: Int) {
        if (command == 1) {
            bAtHomeSDK.turnOnGroup(homeAutomation.title, {
                mView?.showMessage("Success")
                updateGroupStatus(BAtHomeAutomation.ACTION_ON)
            }) {
                mView?.showError(it.localizedMessage)
            }
        } else {
            bAtHomeSDK.turnOffGroup(homeAutomation.title, {
                mView?.showMessage("Success")
                updateGroupStatus(BAtHomeAutomation.ACTION_OFF)
            }) {
                mView?.showError(it.localizedMessage)
            }
        }
    }

    private fun updateGroupStatus(status: String) {
        mControls?.forEach { control ->
            control.component?.status = status
        }
        mView?.notifyList()
    }

    override fun destroy() {
        bAtHomeSDK.destroy()
    }
}
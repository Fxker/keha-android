package com.appy.android.sdk.control.fibaro

import com.appy.android.sdk.control.APCommand

/**
 * Created by Francisco Barrios 15/11/2018.
 */

enum class FibaroOperationModeCommand : APCommand {
    HEAT {
        override fun action(): String {
            return "{\"args\":[1]}"
        }
    },
    COOL {
        override fun action(): String {
            return "{\"args\":[2]}"
        }
    },
    RESUME {
        override fun action(): String {
            return "{\"args\":[5]}"
        }
    },
    FAN_ONLY {
        override fun action(): String {
            return "{\"args\":[6]}"
        }
    },
    DRY_AIR {
        override fun action(): String {
            return "{\"args\":[8]}"
        }
    },
    AUTO_CHANGE_OVER {
        override fun action(): String {
            return "{\"args\":[10]}"
        }
    };
}
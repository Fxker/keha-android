package com.sansiri.homeservice.model.api.partner.trendywash

import android.os.Parcelable
import com.sansiri.homeservice.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TrendyWashMachine(
        var machineId: String? = null,
        var machineNo: String? = null,
        var machineName: String? = null,
        var status: Int = 0,
        var lastUpdate: String? = null,
        var price: Int = 0,
        var defaultTime: Int = 0,
        var coinValue: Int = 0,
        var typeId: Int = 0,
        var indexOrder: Int = 0,
        var icon: String? = null,
        var remainTime: Int = 0
) : Parcelable {
    companion object {
        const val STATUS_AVAILABLE = 0
        const val STATUS_BUSY = 1
        const val TYPE_MACHINE = 1
        const val TYPE_DRYER = 2
    }

    val localIcon: Int
        get() = if (typeId == TYPE_DRYER) {
            R.drawable.ic_dryer
        } else {
            R.drawable.ic_washing_machine
        }
}

package com.sansiri.homeservice.ui.parking.smartparking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_smart_parking.*
import kotlinx.android.synthetic.main.appbar_center_title.*
import org.koin.androidx.scope.currentScope

class SmartParkingActivity : BaseV2Activity<SmartParkingContract.View, SmartParkingContract.Presenter>(), SmartParkingContract.View {
    override val mPresenter: SmartParkingContract.Presenter by currentScope.inject()

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val TITLE = "TITLE"

        fun start(activity: Activity, unitId: String, title: String?) {
            val intent = Intent(activity, SmartParkingActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val mElevatorAdapter = ElevatorAdapter()
    val mReservationAdapter = ReservationAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smart_parking)

        val unitId = intent.getStringExtra(UNIT_ID)
        val title = intent.getStringExtra(TITLE)


        setBackToolbar(findViewById(R.id.toolbar), "")
        textToolbar.text = title ?: "Smart Parking"

        listLift.apply {
            adapter = mElevatorAdapter
        }

        listReservation.apply {
            adapter = mReservationAdapter
        }

        mPresenter.init(unitId)
        mPresenter.start()

        mPresenter.smartParkingStatus?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data == null) {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let { bindData(it) }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SMART_PARKING")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun setFetchingObserver(data: MutableLiveData<Resource<SmartParkingStatus>>) {
        data.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data != null) {
                        resource.data?.let { bindData(it) }
//                        showLoading()
                    } else {
//                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let { bindData(it) }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })
    }

    override fun bindData(smartParkingStatus: SmartParkingStatus) {
        if (!smartParkingStatus.elevator.isNullOrEmpty()) {
            mElevatorAdapter.setData(smartParkingStatus.elevator)
        } else {
            showNoElevator()
        }

        if (!smartParkingStatus.reservation.isNullOrEmpty()) {
            textNoData.hide()
            mReservationAdapter.setData(smartParkingStatus.reservation)
        } else {
            showNoReservation()
        }
    }

    private fun showNoElevator() {

    }

    private fun showNoReservation() {
        textNoData.show()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

package com.appy.android.code.domain.model.command.blind

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.blind.BlindCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandBlindForm(
        val homeId: String,
        val blindId: String,
        val command: BlindCommand
) : AppyForm() {
    val mSendCommandBlindJsonBody: SendCommandBlindJsonBody = SendCommandBlindJsonBody()

    init {
        this.mSendCommandBlindJsonBody.command = command.action()
    }

}
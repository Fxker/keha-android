package com.sansiri.homeservice.ui.suggestion


import android.graphics.Bitmap
import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.adapter.MultiSelectPhotoAdapter
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.base.PhotoFragment
import com.sansiri.homeservice.ui.homecare.create.HomeCareCreateActivity
import com.sansiri.homeservice.util.*
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_home_care_detail.*
import kotlinx.android.synthetic.main.view_progress_status.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class SuggestFragment : PhotoFragment<SuggestionContract.View, SuggestionContract.Presenter>(), SuggestionContract.View, IPickResult {
    override var mPresenter: SuggestionContract.Presenter = SuggestionPresenter()
    private var unitId: String? = null


    companion object {
        val UNIT_ID = "UNIT_ID"
        fun newInstance(unitId: String): androidx.fragment.app.Fragment {
            val fragment = SuggestFragment()
            val bundle = Bundle()
            bundle.putString(UNIT_ID, unitId)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        unitId = arguments?.getString(UNIT_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_home_care_detail)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter.maximumSelection = 9


        listPhoto.apply {
            mAdapter.setData(bitmaps)
            adapter = mAdapter
        }

        buttonCreate.setOnClickListener {
            sendEvent("SUGGESTION", "CLICK", "SUGGESTION_REQUEST")
            mPresenter.submit()
        }

        buttonCreate.text = getString(R.string.send_suggestion)

        textName.hide()
        editTextName.hide()
        textContact.hide()
        editTextContact.hide()
        textEmail.hide()
        editTextEmail.hide()

        mPresenter.init(unitId!!)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SUGGESTION")
    }

    override fun prepareBitmapToUploadIfExist() {
        if (bitmaps.isNotEmpty()) {
            sendEvent("SUGGESTION", "UPLOAD", "PHOTO")

            compressBitmapObservable.observe().subscribe({ imageToUpload ->
                mPresenter.uploadImage(imageToUpload)
            }) {
                showError(getString(R.string.error_fail_to_upload))
            }
        } else {
            mPresenter.submitSuggestion(listOf())
        }
    }

    override fun getDetail(): String = editTextDescription.text.toString()

    override fun showUploadProgress(progress: Int, max: Int) {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = "${getString(R.string.uploading)} ($progress/$max)"
    }


    override fun showSubmitProgress() {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = getString(R.string.submitting)
    }

    override fun tryAgain() {
        editTextDescription.isEnabled = true
        buttonCreate.text = getString(R.string.try_again)
        panelProgress.hide()
        buttonCreate.show()
    }

    override fun showSuccess() {
        ({
            activity!!.finish()
        }).withDelay(1000)

        progressBar.hide()
        textProgress.text = getString(R.string.done)
    }

    override fun showAlertToEnterDetail() {
        activity?.snack("Please fill in details")
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}
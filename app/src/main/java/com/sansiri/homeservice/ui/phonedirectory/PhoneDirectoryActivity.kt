package com.sansiri.homeservice.ui.phonedirectory

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class PhoneDirectoryActivity : BaseActivity<PhoneDirectoryContract.View, PhoneDirectoryContract.Presenter>(), PhoneDirectoryContract.View {
    override var mPresenter: PhoneDirectoryContract.Presenter = PhoneDirectoryPresenter()

    val mAdapter = PhoneDirectoryAdapter() {
        val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", it, null))
        sendEvent("PHONE_DIRECTORY", "CLICK", "TELEPHONE")
        startActivity(phoneIntent)
    }

    companion object {
        val PROJECT_ID = "PROJECT_ID"
        val TITLE = "TITLE"

        fun start(activity: Activity, projectId: String, title: String?) {
            val intent = Intent(activity, PhoneDirectoryActivity::class.java)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.phone_directory))
        list.adapter = mAdapter

        val projectId = intent?.getStringExtra(PROJECT_ID)
        mPresenter.start(projectId ?: "")
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("PHONE_DIRECTORY")
    }

    override fun bindData(phoneDirectory: List<PhoneDirectoryGroup>) {
        mAdapter.setData(phoneDirectory)
    }

    override fun showError(message: String) {
        alert(message, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        list.hide()
        textNoData.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}

package com.sansiri.homeservice.ui.welcome

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager

/**
 * Created by sansiri on 1/19/18.
 */
class LogoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun bind() {
        with(view) {
            FirebaseRemoteConfigManager().fetch { remote ->
                remote.let { remoteConfig ->
                    remoteConfig.getString(FirebaseRemoteConfigManager.LOGO_IMAGE)?.let { logo ->
                        val imageLogo = view.findViewById<ImageView>(R.id.imageLogo)
                        if (imageLogo != null && logo.isNotEmpty()) Glide.with(this).load(logo).into(imageLogo)
                    }
                }
            }
        }
    }
}
package com.sansiri.homeservice.model.api.partner.parking

import com.sansiri.homeservice.util.random

data class SmartParkingStatus(
        val elevator: List<Elevator>? = null,
        val reservation: List<Reservation>? = null
) {
    companion object {
        val STATUS_READY = "READY"
        val STATUS_BUSY = "BUSY"
    }

    data class Elevator(
            val unit: String? = null,
            val timeRemain: String? = null,
            val timeRemainDisplay: String? = null,
            val message: String? = null,
            val messageDisplay: String? = null,
            val status: String? = null,
            val statusDisplay: String? = null,
            val cardId: String? = null,
            val bgColor: String? = null,
            val textColor: String? = null
    )

    data class Reservation(
            val no: String? = null,
            val cardId: String? = null,
            val timeRemain: String? = null,
            val timeRemainDisplay: String? = null
    )
}
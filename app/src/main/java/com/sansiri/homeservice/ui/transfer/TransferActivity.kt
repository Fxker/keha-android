package com.sansiri.homeservice.ui.transfer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Doc
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.pdf.PdfActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_list.*

class TransferActivity : BaseActivity<TransferContract.View, TransferContract.Presenter>(), TransferContract.View {
    override var mPresenter: TransferContract.Presenter = TransferPresenter()

    val mAdapter = TransferAdapter() {
        PdfActivity.start(this, it.url ?: "", it.title ?: "")
        // WebActivity.start(this, "http://docs.google.com/gview?embedded=true&url=${it.url}", it.title!!)
    }

    companion object {
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"
        val TITLE = "TITLE"

        fun start(activity: Activity, unitObjectId: String, title: String?) {
            val intent = Intent(activity, TransferActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.transfer_docs))

        val unitObjectId = intent.getStringExtra(UNIT_OBJECT_ID)
        unitObjectId?.let { mPresenter.fetchData(unitObjectId) }

        with(list) {
            layoutManager = androidx.recyclerview.widget.GridLayoutManager(this.context, 2)
            adapter = mAdapter
        }

    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRANSFER_DOCS")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(docs: List<Doc>) {
        mAdapter.setData(docs)
    }

    override fun addData(doc: Doc) {
        mAdapter.addData(doc)
    }


    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

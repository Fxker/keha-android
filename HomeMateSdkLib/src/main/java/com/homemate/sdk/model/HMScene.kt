package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable

data class HMScene(
        override var id: String? = null,
        override var name: String? = null
) : HMControl, Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMScene> = object : Parcelable.Creator<HMScene> {
            override fun createFromParcel(source: Parcel): HMScene = HMScene(source)
            override fun newArray(size: Int): Array<HMScene?> = arrayOfNulls(size)
        }
    }
}
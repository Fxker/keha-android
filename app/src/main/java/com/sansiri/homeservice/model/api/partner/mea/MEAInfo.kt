package com.sansiri.homeservice.model.api.partner.mea

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MEAInfo(
	val createdAt: String? = null,
	val projectNameTh: String? = null,
	val contractAccountId: String? = null,
	val projectNameEn: String? = null,
	val projectAddressEn: String? = null,
	val projectAddressTh: String? = null,
	val objectId: String? = null,
	val updatedAt: String? = null
): Parcelable

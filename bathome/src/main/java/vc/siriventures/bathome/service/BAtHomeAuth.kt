package vc.siriventures.bathome.service

import android.app.Activity
import android.net.Uri
import android.util.Log
import com.digithai.bathome.BatHomeLib
import com.digithai.bathome.dto.DigithaiLoginResponseDTO
import com.digithai.bathome.util.BatHomeCallBack
import java.lang.Exception
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class BAtHomeAuth(val activity: Activity) {
    val lib = BatHomeLib()

    fun auth(username: String, password: String, projectId: String, onSuccess: (String) -> Unit, onError: (Throwable) -> Unit) {
        lib.authenticate(username, password, projectId, object : BatHomeCallBack {
            override fun onResult(response: Any?) {
//                continuation.resume(p0)
                if (response is DigithaiLoginResponseDTO) {
                    selectHost(response.privateUrl, response.publicUrl)?.let {
                        onSuccess(it)
                        return
                    }
                }
                onError(Exception("Fail to authenticate"))
            }

            override fun onError(p0: String?) {
                onError(Exception(p0))
            }
        }, activity)
    }


    private fun selectHost(privateUrl: String?, publicUrl: String?): String? {
        if (privateUrl != null) {
            val uri = Uri.parse(privateUrl)
            if (uri != null && uri.host != null) {
                val process = Runtime.getRuntime().exec("/system/bin/ping -c 1 ${uri.host}")
                val returnVal = process.waitFor()
                Log.d("oakraw ping", "${uri.host} $returnVal")
                if (returnVal == 0) {
                    return privateUrl
                }
            }
        }

        if (publicUrl != null) {
            return publicUrl
        }

        return null
    }
}
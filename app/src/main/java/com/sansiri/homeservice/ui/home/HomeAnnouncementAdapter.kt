package com.sansiri.homeservice.ui.home

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.announcement.all.AllAnnouncementAdapter
import com.sansiri.homeservice.ui.announcement.all.AnnouncementViewHolder
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.withDelay
import kotlinx.android.synthetic.main.view_announcement_home.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class HomeAnnouncementAdapter(var overlayColor: Int?, var backgroundColor: Int?, val itemClick: (Announcement<*>) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<HomeAnnouncementViewHolder>() {
    private val mData = mutableListOf<Announcement<*>>()
    var maxItemHeight = 0

    fun setData(list: List<Announcement<*>>) {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: HomeAnnouncementViewHolder, position: Int) {
        holder.bind(mData[position])
        holder.view.post {
            if (holder.view.space.height > holder.view.layoutTop.height) {
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int = mData.size

    fun setColor(overlayColor: Int?, backgroundColor: Int?) {
        this.overlayColor = overlayColor
        this.backgroundColor = backgroundColor
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAnnouncementViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_announcement_home, parent, false)
        return HomeAnnouncementViewHolder(
                overlayColor ?: ContextCompat.getColor(parent.context, R.color.colorSecondary),
                backgroundColor ?: Color.WHITE, view, itemClick)
    }
}
package com.sansiri.homeservice.model.automation

import com.appy.android.sdk.control.APControl

/**
 * Created by sansiri on 10/10/17.
 */
data class OnionShackHomeAutomation(override var title: String = "", override var icon: Int, override var type: Type, val deviceId: String, var status: Int = 0, override var action: Int): HomeAutomation()
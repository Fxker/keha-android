package com.sansiri.homeservice.ui.deeplink

import android.net.Uri
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.observe
import io.reactivex.Single
import java.net.URLDecoder

class DeepLinkPresenter : BasePresenterImpl<DeepLinkContract.View>(), DeepLinkContract.Presenter {
    override fun start() {
    }

    override fun fetchMe(): Single<Profile> {
        return Single.create<Profile> { emitter ->
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                api?.getMe()?.observe()?.subscribe({ profile ->
                    emitter.onSuccess(profile)
                }) { e ->
                    emitter.onError(e)
                }
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun requestLaunchWeb(uri: Uri, useToken: Boolean) {
        val url = uri.getQueryParameter("url")
        val decodedUrl = URLDecoder.decode(url, "UTF-8")

        var observer = if (decodedUrl.contains("{uuid}")) {
            fetchMe().map { profile ->
                decodedUrl?.replace("{uuid}", profile.objectId ?: "")
            }
        } else {
            Single.just(decodedUrl)
        }

        if (decodedUrl.contains("{langcode}")) {
            observer = observer.map { it.replace("{langcode}", getLanguageCode()) }
        }

        mCompositeDisposable.add(observer.observe().subscribe({
            if (url != null) {
                if (useToken) {
                    mView?.launchWebWithToken(url)
                } else {
                    mView?.launchWeb(url)
                }
            } else {
                mView?.showError("")
            }
        }) {
            mView?.showError("")
        })
    }


    private fun getLanguageCode() = if (Store.getLocale()?.language == "th") {
        "th"
    } else {
        "en"
    }

}
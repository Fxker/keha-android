package com.sansiri.homeservice.ui.myaccount_v2.balance

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class MyAccountBalancePresenter : BasePresenterImpl<MyAccountBalanceContract.View>(), MyAccountBalanceContract.Presenter {
    var mUnitId: String? = null
    var allItemCount = Int.MAX_VALUE

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        if (mUnitId != null) {
            fetch(0)
        }
    }

    override fun fetch(page: Int) {
        val offset = page * 10
        if (offset > allItemCount) return

        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(
                        api.getBalanceV2(mUnitId ?: "")
                                .observe()
                                .subscribe({ invoice ->
                                    invoice.let { mView?.bindBalance(it) }
                                }) {
                                    mView?.showBalanceError()
                                }
                )

                mCompositeDisposable.add(
                        api.getBalanceHistory(mUnitId ?: "", offset, 10)
                                .observe()
                                .subscribe({
                                    allItemCount = it.count ?: Int.MAX_VALUE
                                    val paymentHistories = it.items
                                    mView?.bindHistory(paymentHistories)
                                }) {
                                    mView?.showError(it.showHttpError())
                                })
            } else {
                mView?.showError("Network Error")
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.dimmer.DimmerCommand

class SendFibaroDimmerCommandForm(light: APDimmer, command: DimmerCommand) : SendFibaroCommandForm(light) {

    init {
        when(command) {
            DimmerCommand.ON -> {
                action = "turnOn"
            }
            DimmerCommand.OFF -> {
                action = "turnOff"
            }
            else -> {
                action = "setValue"
                val b = FormBody()
                b.args.add(command.action().removePrefix("DIMMER_"))

                this.body = b
            }
        }
    }
}
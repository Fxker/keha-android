package vc.siriventures.bathome

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.Uri
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import vc.siriventures.bathome.constants.BAtHomeException
import vc.siriventures.bathome.constants.BAtHomeException.Companion.HOST_NOT_FOUND
import vc.siriventures.bathome.model.GatewayInfoRequest
import vc.siriventures.bathome.service.ApiService
import vc.siriventures.bathome.service.AuthorizeService
import vc.siriventures.bathome.service.UnsafeOkHttpClient
import java.lang.Exception
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.*
import okhttp3.CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256
import retrofit2.HttpException
import vc.siriventures.bathome.model.BAtHomeComponent
import vc.siriventures.bathome.service.BAtHomeAuth
import java.net.UnknownHostException
import java.util.*


class BAtHomeSDK(val context: Context) {
    var apiService: ApiService? = null
    val compositeDisposable = CompositeDisposable()

    var username: String? = null
    var password: String? = null

    val httpClient = OkHttpClient.Builder().apply {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        addInterceptor(logging)
    }

    fun init(activity: Activity, projectCode: String, username: String, password: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        val okhttpClient = UnsafeOkHttpClient(projectCode, username, password)

        this.username = username
        this.password = password

        BAtHomeAuth(activity).auth(username, password, projectCode, { host ->
            apiService = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okhttpClient.getUnsafeOkHttpClient(context))
                    .baseUrl(host)
                    .build()
                    .create(ApiService::class.java)
            onSuccess()
        }, onError)

    }

    fun login(onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        if (username == null || password == null) {
            return
        }

        compositeDisposable +=
                apiService?.login(username!!.toRequestBody(), password!!.toRequestBody())
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            if (it is HttpException && it.code() in 0..399) {
                                onSuccess()
                            } else {
                                onError(it)
                            }
                        }
    }

    fun getAllComponents(onSuccess: (List<BAtHomeComponent>) -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.getAllComponents()
                        ?.observe()
                        ?.subscribe({
                            onSuccess(it)
                        }) {
                            onError(it)
                        }
    }

    fun getLights(onSuccess: (List<BAtHomeComponent>) -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.getLights()
                        ?.observe()
                        ?.subscribe({
                            onSuccess(it)
                        }) {
                            onError(it)
                        }
    }

    fun getACs(onSuccess: (List<BAtHomeComponent>) -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.getACs()
                        ?.observe()
                        ?.subscribe({
                            onSuccess(it)
                        }) {
                            onError(it)
                        }
    }


    fun getScenes(onSuccess: (List<BAtHomeComponent>) -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.getScenes()
                        ?.observe()
                        ?.subscribe({
                            onSuccess(it)
                        }) {
                            onError(it)
                        }
    }


    fun turnOnLight(lightId: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.turnOnLight(lightId)
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            onError(it)
                        }
    }

    fun turnOffLight(lightId: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.turnOffLight(lightId)
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            onError(it)
                        }
    }

    fun turnOnGroup(groupId: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.turnOnGroup(groupId)
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            onError(it)
                        }
    }

    fun turnOffGroup(groupId: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.turnOffGroup(groupId)
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            onError(it)
                        }
    }

    fun turnOnScene(sceneId: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        compositeDisposable +=
                apiService?.turnOnScene(sceneId)
                        ?.observe()
                        ?.subscribe({
                            onSuccess()
                        }) {
                            onError(it)
                        }
    }

    fun destroy() {
        compositeDisposable.clear()
    }

    fun String.toRequestBody() = RequestBody.create(MediaType.parse("text/plain"), this)


    private fun selectHost(privateUrl: String?, publicUrl: String?) = Observable.create<String> { emitter ->
        if (privateUrl != null) {
            val uri = Uri.parse(privateUrl)
            if (uri != null && uri.host != null) {
                val process = Runtime.getRuntime().exec("/system/bin/ping -c 1 ${uri.host}")
                val returnVal = process.waitFor()
                Log.d("oakraw ping", "${uri.host} $returnVal")
                if (returnVal == 0) {
                    emitter.onNext(privateUrl)
                    emitter.onComplete()
                }
            }
        }

        if (publicUrl != null) {
            emitter.onNext(publicUrl)
            emitter.onComplete()
        }

        emitter.onError(UnknownHostException("Host is unavailable"))
    }.map {
//        apiService = Retrofit.Builder()
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient(context))
//                .baseUrl(it)
//                .build()
//                .create(ApiService::class.java)
        return@map it
    }


    operator fun CompositeDisposable.plusAssign(disposable: Disposable?) {
        disposable?.let { add(disposable) }
    }

    fun <T> Observable<T>.observe(): Observable<T> {
        return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }


}
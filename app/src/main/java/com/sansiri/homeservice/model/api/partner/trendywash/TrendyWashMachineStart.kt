package com.sansiri.homeservice.model.api.partner.trendywash

data class TrendyWashMachineStart(
	val actionTime: Int = 0,
	val message: String? = null
)

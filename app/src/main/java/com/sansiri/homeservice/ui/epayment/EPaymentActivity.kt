package com.sansiri.homeservice.ui.epayment

import android.content.Intent
import android.graphics.*
import android.os.Bundle
import androidx.core.content.FileProvider
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import com.sansiri.homeservice.ui.base.PermissionActivity
import com.sansiri.homeservice.ui.base.requestPermissionReadWriteWithPermissionCheck
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_barcode.*
import java.io.File
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


abstract class EPaymentActivity<V : BaseView, T : BasePresenter<V>> : PermissionActivity<V, T>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode)
        buttonDownload.hide()
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            containerImagePreview.hide()
        }
    }

    fun save(qrBitmap: Bitmap) {
        doAsync {
            val detailBitmap = getDetailBitmap()
            val finalBitmap = CodeEngine(this@EPaymentActivity).extendBitmap(detailBitmap, qrBitmap)

            uiThread {
                callback = object : Callback {
                    override fun onPermissionAllowed(permission: String) {
                        if (permission == android.Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                            val file = SaveUtil().saveImage(this@EPaymentActivity, finalBitmap)
                            showPreview(finalBitmap, file)
                        }
                    }
                }
                requestPermissionReadWriteWithPermissionCheck()
            }
        }
    }

    fun showPreview(preview: Bitmap, file: File) {
        imagePreview.setImageBitmap(preview)
        containerImagePreview.setOnClickListener {
            launchGallery(file)
        }

        containerImagePreview.translationX = containerImagePreview.width.toFloat() * -1
        containerImagePreview
                .animate()
                .setDuration(700)
                .setInterpolator(OvershootInterpolator())
                .translationX(0f)


        ({ hidePreview() }).withDelay(CodeEngine.timeToHidePreviewImage)
    }

    fun hidePreview() {
        containerImagePreview
                .animate()
                .setDuration(700)
                .setInterpolator(OvershootInterpolator())
                .translationX(containerImagePreview.width.toFloat() * -1)
    }

    abstract fun getDetailBitmap(): Bitmap

    private fun launchGallery(file: File) {
        try {
            val uri = FileProvider.getUriForFile(this, "${BuildConfig.APPLICATION_ID}.com.vansuita.pickimage.provider", file)
            val intent = Intent()
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            intent.action = Intent.ACTION_VIEW
            intent.setDataAndType(uri, "image/*")
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        } catch (e: Exception) {
        }
    }

    override fun showError(message: String) {
    }
}

package com.sansiri.homeservice.data.repository.meter

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.network.auth.SmartMeterService
import com.sansiri.homeservice.data.network.networkCall
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterDetail
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterHistoryList
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterOverAll
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterHistoryList
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import kotlinx.coroutines.*


/**
 * Created by sansiri on 11/7/17.
 */
class SmartMeterRepositoryImpl(val service: SmartMeterService): SmartMeterRepository {
    private val FETCH_TIME_INTERVAL = 10000L
    private val electricMeterLast = MutableLiveData<Resource<SmartElectricMeterDetail>>()
    private var fetchElectricMeterOverAllJob: Job? = null

    override fun getSmartWaterMeterSummary(unitId: String, meterId: String, date: String) = networkCall<SmartWaterMeterOverAll>() {
        client = service.getSmartWaterMeterSummary(unitId = unitId, meterId = meterId, dateTime = date)
    }

    override fun getSmartWaterMeterHistory(unitId: String, meterId: String, type: String, date: String) = networkCall<SmartWaterMeterHistoryList>() {
        client = service.getSmartWaterMeterHistory(unitId = unitId, meterId = meterId, dateType = type, dateTime = date)
    }

    override fun getSmartElectricMeterLast(unitId: String, meterId: String, date: String): MutableLiveData<Resource<SmartElectricMeterDetail>> {
        fetchElectricMeterOverAllJob = fetchMeterLastDetailJob(unitId, meterId, date)
        return electricMeterLast
    }

    override fun getSmartElectricMeterSummary(unitId: String, meterId: String, date: String) = networkCall<SmartElectricMeterOverAll> {
        client = service.getSmartElectricMeterSummary(unitId = unitId, meterId = meterId, dateTime = date)
    }

    override fun getSmartElectricMeterHistory(unitId: String, meterId: String, type: String, date: String) = networkCall<SmartElectricMeterHistoryList>() {
        client = service.getSmartElectricMeterHistory(unitId = unitId, meterId = meterId, dateType = type, dateTime = date)
    }

    private fun fetchMeterLastDetailJob(unitId: String, meterId: String, date: String): Job = GlobalScope.launch {
        while(true) {
            withContext(Dispatchers.Main) {
                networkCall(electricMeterLast) {
                    client = service.getSmartElectricMeterLast(unitId = unitId, meterId = meterId, dateTime = date)
                }
            }
            delay(FETCH_TIME_INTERVAL)
        }
    }

    override fun destroy() {
        fetchElectricMeterOverAllJob?.cancel()
        fetchElectricMeterOverAllJob = null
    }
}
package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcel
import android.os.Parcelable

data class HomeUpgradeOrderRequest(
        val roomId: String? = null,
        val hardwareId: String? = null,
        val amount: Int = 0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(roomId)
        writeString(hardwareId)
        writeInt(amount)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeUpgradeOrderRequest> = object : Parcelable.Creator<HomeUpgradeOrderRequest> {
            override fun createFromParcel(source: Parcel): HomeUpgradeOrderRequest = HomeUpgradeOrderRequest(source)
            override fun newArray(size: Int): Array<HomeUpgradeOrderRequest?> = arrayOfNulls(size)
        }
    }
}
package com.appy.android.code.base.data.rest.result

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
abstract class AppyApiResult<T> {
    @SerializedName("status")
    open var status: AppyApiStatusResult? = null

    @SerializedName("api_status")
    open var apiStatus: AppyApiApiStatusResult? = null

    @SerializedName("data")
    open var data: T? = null
}
package com.sansiri.homeservice.ui.maintenance

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDetail
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.LocalIconMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.maintenance.detail.MaintenanceDetailActivity
import com.sansiri.homeservice.ui.maintenance.device.MaintenanceDeviceActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.activity_maintenance.*

class MaintenanceActivity : BaseActivity<MaintenanceContract.View, MaintenanceContract.Presenter>(), MaintenanceContract.View {

    override var mPresenter: MaintenanceContract.Presenter = MaintenancePresenter()

    val mPlanAdapter = MaintenancePlanAdapter() { plan ->
        sendEvent(Feature.MAINTENANCE_GUIDE, "CLICK", "NEAR_PLAN", "2", Pair("AccessoryId", plan.accessoryId ?: ""))
        mPresenter.requestToLaunchDetail(plan)
    }
    val mDeviceAdapter = GridMenuAdapter(null, null) {menu ->
        sendEvent(Feature.MAINTENANCE_GUIDE, "CLICK", "DEVICE", "2", Pair("DeviceId", menu.id ?: ""))
        mPresenter.requestToLaunchDevice(menu.id ?: "")
    }

    companion object {
        const val TITLE = "TITLE"
        const val HOME = "HOME"
        const val PLAN_ID = "PLAN_ID"
        const val ACCESSORY_ID = "ACCESSORY_ID"

        fun start(activity: Activity, home: Hut, title: String?, planId: String?, accessoryId: String?) {
            val intent = Intent(activity, MaintenanceActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(HOME, home)
            intent.putExtra(PLAN_ID, planId)
            intent.putExtra(ACCESSORY_ID, accessoryId)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maintenance)

        setBackToolbar(intent.getStringExtra(TITLE) ?: "")

        val home = intent.getParcelableExtra<Hut>(HOME)
        val planId = intent.getStringExtra(PLAN_ID)
        val accessoryId = intent.getStringExtra(ACCESSORY_ID)
        if (planId != null && accessoryId != null) {
            launchDetail(home, accessoryId, planId, null)
        } else if (planId != null && accessoryId == null) {
            mPresenter.requestToLaunchDevice(planId)
        }

        listPlan.apply {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mPlanAdapter
        }

        listDevice.apply {
            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
            adapter = mDeviceAdapter
        }


        mPresenter.init(home)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView(Feature.MAINTENANCE_GUIDE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MaintenanceDetailActivity.CODE) {
            if (resultCode == Activity.RESULT_OK) {
                mPresenter.start()
            }
        }
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun showLoadingPlan() {
        listPlan.hide()
        textNoPlan.hide()
        progressBarPlan.show()
    }

    override fun hideLoadingPlan() {
        progressBarPlan.hide()
    }

    override fun showLoadingDevice() {
        listDevice.hide()
        textNoDevice.hide()
        progressBarDevice.show()
    }

    override fun hideLoadingDevice() {
        progressBarDevice.hide()
    }

    override fun bindPlan(plans: List<MaintenancePlan>) {
        listPlan.show()
        textNoPlan.hide()

        if (plans.isNotEmpty()) {
            listPlan.show()
            mPlanAdapter.setData(plans)
        }
    }

    override fun showEmptyPlan() {
        listPlan.hide()
        textNoPlan.show()
    }

    override fun showEmptyDevice() {
        listDevice.hide()
        textNoDevice.show()
    }

    override fun bindDevice(devices: List<MaintenanceDevice>) {
        if (devices.isNotEmpty()) {
            listDevice.show()
            mDeviceAdapter.setData(devices.map { device ->
                GenericMenu(device.planId ?: "", device.product, device.imagePath, "")
            })
        }
    }

    override fun launchDevice(home: Hut, device: MaintenanceDevice) {
        MaintenanceDeviceActivity.start(this, home, device)
    }

    override fun launchDetail(mHome: Hut, accessoryId: String, planId: String?, plan: MaintenancePlan?) {
        MaintenanceDetailActivity.start(this, mHome, accessoryId, planId, plan)

    }

    override fun showError(message: String) {
        alertError(message)
    }
}

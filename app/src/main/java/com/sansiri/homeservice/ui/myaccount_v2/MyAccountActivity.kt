package com.sansiri.homeservice.ui.myaccount_v2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.myaccount_v2.balance.MyAccountBalanceFragment
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceAdapter
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceDetailActivity
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceFragment
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_my_account_v2.*

class MyAccountActivity : BaseActivity<MyAccountContract.View, MyAccountContract.Presenter>(), MyAccountContract.View {
    override var mPresenter: MyAccountContract.Presenter = MyAccountPresenter()


    var mHome: Hut? = null
    companion object {
        val HOME = "HOME"
        val TITLE = "TITLE"
        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, MyAccountActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val mAdapter = MyAccountInvoiceAdapter(R.color.colorCancel) { invoce ->
        if (mHome != null) {
            MyAccountInvoiceDetailActivity.start(this, mHome!!, invoce)
        } else {
            showError(getString(R.string.error_something_went_wrong))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account_v2)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.my_account))

        mHome = intent.getParcelableExtra(HOME)

        val adapter = MyAccountTabAdapter(supportFragmentManager)
        adapter.addFragment(MyAccountInvoiceFragment.newInstance(mHome!!), getString(R.string.invoice))
        adapter.addFragment(MyAccountBalanceFragment.newInstance(mHome!!), getString(R.string.my_account_balance))
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showError(message: String) {
    }
}

package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.R

/**
 * Created by oakraw on 10/3/2017 AD.
 */
class GenericMenu(
        @SerializedName("type")
        override val id: String?,
        @SerializedName("name")
        override val title: String? = null,
        @SerializedName("iconUrl")
        override val icon: String? = null,
        @SerializedName("actionType")
        val type: String? = null,
        @SerializedName("url")
        val url: String? = null
) : Menu, Parcelable {

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(title)
        writeString(icon)
        writeString(type)
        writeString(url)
    }

    companion object {
        val HOME_AUTOMATION = "HOME_AUTOMATION"

        val SALE_RENTOUT = "SALE_RENT_OUT"

        val SALE = "SALE"

        val DOWNPAYMENT = "DOWNPAYMENT"

        val HOME_CARE = "HOME_CARE"

        val MAIL_BOX = "MAILBOX"

        val HOME_WALLET = "MY_ACCOUNT"

        val CONTACT_US = "CONTACT_US"

        val FACILITY_BOOKING = "FACILITY_BOOKING"

        val PHONE_DIRECTORY = "PHONE_DIRECTORY"

        val PROJECT_PROGRESS = "PROJECT_PROGRESS"

        val CHAT = "CHAT"

        val TRANSFER = "TRANSFER"

        val INSPECTION = "INSPECTION"

        val APPOINTMENT = "APPOINTMENT"

        val SUGGESTION = "SUGGESTION"

        @JvmField
        val CREATOR: Parcelable.Creator<GenericMenu> = object : Parcelable.Creator<GenericMenu> {
            override fun createFromParcel(source: Parcel): GenericMenu = GenericMenu(source)
            override fun newArray(size: Int): Array<GenericMenu?> = arrayOfNulls(size)
        }
    }
}

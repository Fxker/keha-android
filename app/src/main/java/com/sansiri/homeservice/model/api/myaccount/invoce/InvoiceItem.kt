package com.sansiri.homeservice.model.api.myaccount.invoce

import android.os.Parcel
import android.os.Parcelable

data class InvoiceItem(
		val sequenceId: Int,
		val loanTypeId: Int,
		val loanTypeDescription: String? = null,
		val amount: Double
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readInt(),
			source.readInt(),
			source.readString(),
			source.readDouble()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeInt(sequenceId)
		writeInt(loanTypeId)
		writeString(loanTypeDescription)
		writeDouble(amount)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<InvoiceItem> = object : Parcelable.Creator<InvoiceItem> {
			override fun createFromParcel(source: Parcel): InvoiceItem = InvoiceItem(source)
			override fun newArray(size: Int): Array<InvoiceItem?> = arrayOfNulls(size)
		}
	}
}

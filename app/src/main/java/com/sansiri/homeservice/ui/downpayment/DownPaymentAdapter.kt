package com.sansiri.homeservice.ui.downpayment

import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class DownPaymentAdapter(val color: Int, val itemClick: ((DownpaymentItem) -> Unit)? = null) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<DownpaymentItem>()


    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return DownPaymentViewHolder(parent.inflate(R.layout.view_summary_payable_card), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is DownPaymentViewHolder)
            holder.bind(mData[position], color)
    }

    fun addData(data: DownpaymentItem) {
        mData.add(data)
        notifyItemChanged(itemCount - 1)
    }


}
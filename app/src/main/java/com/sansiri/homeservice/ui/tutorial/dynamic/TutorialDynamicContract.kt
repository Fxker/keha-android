package com.sansiri.homeservice.ui.tutorial.dynamic

import com.sansiri.homeservice.model.api.tutorial.TutorialPage
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TutorialDynamicContract {
    interface Presenter : BasePresenter<View> {
        fun fetch(menu: String)
        fun init(menu: String)
    }

    interface View : BaseView {
        fun bindData(pages: List<TutorialPage>)

    }
}
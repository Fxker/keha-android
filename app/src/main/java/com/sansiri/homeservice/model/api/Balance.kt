package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

/**
 * Created by sansiri on 10/31/17.
 */
data class Balance (val balance: Double)
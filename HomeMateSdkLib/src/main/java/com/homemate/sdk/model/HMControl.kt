package com.homemate.sdk.model

interface HMControl {
    var id: String?
    var name: String?
}
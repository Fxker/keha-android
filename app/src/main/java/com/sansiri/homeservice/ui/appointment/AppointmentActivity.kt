package com.sansiri.homeservice.ui.appointment

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Inspection
import com.sansiri.homeservice.ui.inspection.InspectionSectionAdapter
import com.sansiri.homeservice.util.reportDate
import com.sansiri.homeservice.util.setBackToolbar
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*

class AppointmentActivity : LocalizationActivity() {

    val mAdapter = SectionedRecyclerViewAdapter()

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, AppointmentActivity::class.java)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setBackToolbar(getString(R.string.appointment))

        list.adapter = mAdapter

        bindData(arrayListOf(
                //Inspection(getString(R.string.inspection_2), getString(R.string.view_inspection_list), getString(R.string.inspection_appointment)),
                Inspection(getString(R.string.inspection_approval), getString(R.string.view_previous_inspection_list), getString(R.string.inspection_appointment))
        ), arrayListOf(
                Inspection(getString(R.string.transfer), getString(R.string.prepare_transfer_documents), getString(R.string.transfer_appointment))
        ))

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    fun bindData(vararg listInspectionList: MutableList<Inspection>) {
        for ((index, list) in listInspectionList.withIndex()) {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, 1 * index)
            mAdapter.addSection(AppointmentSectionAdapter(calendar.reportDate(), list) {})
        }
    }
}

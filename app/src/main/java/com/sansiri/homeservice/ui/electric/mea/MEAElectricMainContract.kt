package com.sansiri.homeservice.ui.electric.mea

import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MEAElectricMainContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun init(home: Hut?)
        fun checkCA()
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun launchLogin(home: Hut, meaInfo: MEAInfo)
        fun launchHome(ca: String)
        fun launchConsent(consent: Consent)
        fun onConsentAccepted()
    }
}
package com.sansiri.homeservice.model.api.maintenance

import android.os.Parcel
import android.os.Parcelable

data class MaintenanceDevice(
		val planId: String? = null,
		val brand: String? = null,
		val contractPhones: List<String>? = null,
		val product: String? = null,
		val productId: String? = null,
		val imagePath: String? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.createStringArrayList(),
			source.readString(),
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(planId)
		writeString(brand)
		writeStringList(contractPhones)
		writeString(product)
		writeString(productId)
		writeString(imagePath)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<MaintenanceDevice> = object : Parcelable.Creator<MaintenanceDevice> {
			override fun createFromParcel(source: Parcel): MaintenanceDevice = MaintenanceDevice(source)
			override fun newArray(size: Int): Array<MaintenanceDevice?> = arrayOfNulls(size)
		}
	}
}

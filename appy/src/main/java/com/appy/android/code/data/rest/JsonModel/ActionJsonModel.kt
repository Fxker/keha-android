package com.appy.android.code.data.rest.JsonModel

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class ActionJsonModel {
    @SerializedName("name")
    var name: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("action")
    var action: String? = null
    @SerializedName("actionValues")
    var actionValues: List<String>? = null
    @SerializedName("local_actions")
    var localActions: LocalActionJsonModel? = null

    public class LocalActionJsonModel {
        @SerializedName("command")
        var command: String? = null
        @SerializedName("device_type")
        var deviceType: String? = null
        @SerializedName("device_id")
        var deviceId: String? = null
        @SerializedName("parameters")
        var parameters: HashMap<String, String>? = null
    }
}
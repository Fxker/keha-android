package com.sansiri.homeservice.ui.washingmachine.trendywash

fun displayTime(totalSecs: Int): String {
    val minutes = totalSecs / 60
    val seconds = totalSecs % 60

    return String.format("%02d:%02d", minutes, seconds);
}
package com.appy.android.sdk.control.daikin

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl
import com.google.gson.annotations.SerializedName

class APDaikin : APControl, Parcelable {

    var mAction: List<String>? = null

    override val actions: List<APAction<*>>
        get() = listOf()

    @SerializedName("status")
    var status: APDaikinStatus? = null

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mAction = source.createStringArrayList()
        status = source.readParcelable(APDaikinStatus::class.java.classLoader)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mAction = ArrayList()  // Todo - need sample of action

        this.status = APDaikinStatus()
        this.status?.let {
            it.temperature = controlJsonModel.status?.temperature
            it.onOff = controlJsonModel.status?.onOff
            it.address = controlJsonModel.status?.address
            it.fanSpeed = controlJsonModel.status?.fanSpeed
            it.fanDirection = controlJsonModel.status?.fanDirection
            it.operationMode = controlJsonModel.status?.operationMode
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeStringList(mAction)
        dest.writeParcelable(status, flags)
    }

    companion object CREATOR : Parcelable.Creator<APDaikin> {
        const val TYPE = "APDimmer"

        override fun createFromParcel(parcel: Parcel): APDaikin {
            val type = parcel.readString()
            return APDaikin(parcel)
        }

        override fun newArray(size: Int): Array<APDaikin?> {
            return arrayOfNulls(size)
        }
    }
}
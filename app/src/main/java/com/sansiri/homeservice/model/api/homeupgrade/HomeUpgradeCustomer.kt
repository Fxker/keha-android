package com.sansiri.homeservice.model.api.homeupgrade

data class HomeUpgradeCustomer(
        val name: String,
        val email: String,
        val phone: String
)
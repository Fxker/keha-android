package com.sansiri.homeservice.ui.washingmachine.trendywash.registration

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TrendyWashRegisterContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun validate(): Boolean
        fun setFirstName(firstName: String)
        fun setLastName(lastName: String)
        fun setEmail(email: String)
        fun setTel(tel: String)
        fun setPassword(password: String)
        fun setPasswordConfirm(passwordConfirm: String)
        fun submit(): MutableLiveData<Resource<TrendyWashRegisterCustomer>>
        fun init(unitId: String)
    }

    interface View : BaseView {
        fun bindData(profile: Profile)
        fun showEmailError(messageRes: Int)
        fun showLoading()
        fun hideLoading()
        fun launchHome(userId: String)
    }
}
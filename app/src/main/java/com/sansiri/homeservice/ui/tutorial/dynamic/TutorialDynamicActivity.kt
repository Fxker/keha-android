package com.sansiri.homeservice.ui.tutorial.dynamic

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.tutorial.TutorialPage
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.tutorial.dynamic.TutorialPagerAdapter
import kotlinx.android.synthetic.main.activity_tutorial.*
import org.koin.core.logger.MESSAGE

class TutorialDynamicActivity : BaseActivity<TutorialDynamicContract.View, TutorialDynamicContract.Presenter>(), TutorialDynamicContract.View {
    override var mPresenter: TutorialDynamicContract.Presenter = TutorialDynamicPresenter()


    var pagerAdapter: TutorialPagerAdapter? = null

    companion object {
        const val MENU = "MENU"
        const val TITLE = "TITLE"
        fun start(activity: Activity, title: String, menuName: String) {
            val intent = Intent(activity, TutorialDynamicActivity::class.java)
            intent.putExtra(MENU, menuName)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        val menuName = intent.getStringExtra(MENU)
        val title = intent.getStringExtra(TITLE)

        pagerAdapter = TutorialPagerAdapter(supportFragmentManager);
        viewPager.adapter = pagerAdapter

        buttonBack.setOnClickListener {
            onBackPressed()
        }

        textTitle.text = title

        mPresenter.init(menuName)
        mPresenter.start()
    }

    override fun bindData(pages: List<TutorialPage>) {
        pagerAdapter?.setItem(pages)
        viewPager.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                bindButton(pages[position])
                textTitle.text = pages[position].title
            }
        })

        bindButton(pages[0])
        textTitle.text = pages[0].title
    }

    private fun bindButton(page: TutorialPage) {
        buttonNext.text = page.button?.text
        buttonNext.setOnClickListener {
            val action = page.button?.actionType
            if (action != null) {
                when (action) {
                    TutorialPage.Button.NEXT_PAGE -> {
                        viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                    }
                    TutorialPage.Button.CLOSE_PAGE -> finish()
                }
            }
        }
    }

    override fun showError(message: String) {
    }
}

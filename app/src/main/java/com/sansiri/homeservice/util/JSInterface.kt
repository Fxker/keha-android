package com.sansiri.homeservice.util

import android.app.Activity
import android.webkit.JavascriptInterface
import android.widget.Toast

/**
 * Created by sansiri on 2/14/18.
 */
class JSInterface(val onImageClick: (String) -> Unit) {
    @JavascriptInterface
    fun openFullScreenImage(img: String) {
        onImageClick(img)
    }
}
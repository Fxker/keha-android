package com.sansiri.homeservice.component.creditcard

import java.util.Locale

import co.omise.android.ui.NumberRangeSpinnerAdapter

class ExpiryMonthSpinnerAdapter : NumberRangeSpinnerAdapter(1, 12) {

    override fun getItemDropDownLabel(number: Int): String {
        return String.format(Locale.getDefault(), "%02d", number)
    }

    override fun getItemLabel(number: Int): String {
        return String.format(Locale.getDefault(), "%02d", number)
    }
}

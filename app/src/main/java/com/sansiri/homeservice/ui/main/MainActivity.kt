package com.sansiri.homeservice.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Looper
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.content.ContextCompat
import android.util.DisplayMetrics
import android.util.Log
import android.view.Menu
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.location.*
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.gson.Gson
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.FeatureConfig
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.PopUp
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.announcement.summary.SummaryAnnouncementFragment
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.chat.ChatActivity
import com.sansiri.homeservice.ui.home.HomeFragment
import com.sansiri.homeservice.ui.meter.electric.SmartElectricMeterActivity
import com.sansiri.homeservice.ui.meter.water.SmartWaterMeterActivity
import com.sansiri.homeservice.ui.more.MoreFragment
import com.sansiri.homeservice.ui.notification.NotificationFragment
import com.sansiri.homeservice.ui.popup.PopUpActivity
import com.sansiri.homeservice.ui.privilege.PrivilegeFragment
import com.sansiri.homeservice.ui.web.WebFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import permissions.dispatcher.*
import q.rorbin.badgeview.Badge
import q.rorbin.badgeview.QBadgeView
import java.util.concurrent.TimeUnit

@RuntimePermissions
class MainActivity : BaseActivity<MainContract.View, MainContract.Presenter>(), MainContract.View {
    override var mPresenter: MainContract.Presenter = MainPresenter()

    private val NAVIGATION_HOME = 0
    private val NAVIGATION_ANNOUNCEMENT = 1
    private val NAVIGATION_SANSIRI_FAMILY = 2
    private val NAVIGATION_NOTIFICATION = 3
    private val NAVIGATION_MORE = 4

    private var currentPageId = -1
    private var fragmentHome: HomeFragment? = null
    private var fragmentSummaryAnnouncement: SummaryAnnouncementFragment? = null
    private var fragmentSansiriFamily: WebFragment? = null
    private var fragmentNotification: NotificationFragment? = null
    private var fragmentMore: MoreFragment? = null
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            val location = if (BuildConfig.DEBUG) {
                result.lastLocation.apply {
                    latitude = 13.7645782
                    longitude = 100.5432383
                }
            } else {
                result.lastLocation
            }
            fragmentHome?.sortHomeByLocation(location)
        }
    }

    private lateinit var prefs: SharedPreferences
    private var badgeAnnouncement: Badge? = null

    companion object {
        const val APP_UPDATE = 1

        fun start(activity: Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
        }

        fun reload(activity: Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            intent.putExtra(NotificationHelper.TYPE, Feature.REFRESH)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivity(intent)
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        if (item.itemId != currentPageId) {
            currentPageId = item.itemId // prevent select duplicate page
            when (item.itemId) {
                NAVIGATION_HOME -> {
                    fragmentHome = HomeFragment.newInstance()
                    replaceNonExistFragment(fragmentHome!!, "home", false)
                    return@OnNavigationItemSelectedListener true
                }
                NAVIGATION_ANNOUNCEMENT -> {
                    prefs[PreferenceHelper.NOTIFICATION_ANNOUNCEMENT] = false
                    badgeAnnouncement?.hide(false)
                    fragmentSummaryAnnouncement = SummaryAnnouncementFragment.newInstance()
                    replaceNonExistFragment(fragmentSummaryAnnouncement!!, "news", false)
                    return@OnNavigationItemSelectedListener true
                }
                NAVIGATION_SANSIRI_FAMILY -> {
                    fragmentSansiriFamily = PrivilegeFragment.newInstance()
                    replaceNonExistFragment(fragmentSansiriFamily!!, "sansiri", false)
                    return@OnNavigationItemSelectedListener true
                }
                NAVIGATION_NOTIFICATION -> {
                    fragmentNotification = NotificationFragment.newInstance()
                    replaceNonExistFragment(fragmentNotification!!, "notification", false)
                    return@OnNavigationItemSelectedListener true
                }
                NAVIGATION_MORE -> {
                    fragmentMore = MoreFragment.newInstance()
                    replaceNonExistFragment(fragmentMore!!, "more", false)
                    return@OnNavigationItemSelectedListener true
                }
            }
        }
        false
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIncomingIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        (application as AppController).removeBadgeCount()
    }

    override fun onStop() {
        super.onStop()
        mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
    }

    private fun handleIncomingIntent(intent: Intent) {
        val type = intent.getStringExtra(NotificationHelper.TYPE)

        if (type == Feature.REFRESH) {
            mOnNavigationItemSelectedListener.onNavigationItemSelected(navigation.menu.findItem(NAVIGATION_HOME))
            fragmentHome?.refreshHome(true)
        }

        intent.getStringExtra(NotificationHelper.DATA)?.let {
            if (type != null) {
                try {
                    val data = Gson().fromJson(it, PushNotificationData::class.java)
                    if (data != null) {
                        when (type) {
                            Feature.MAIL_BOX, Feature.IBOX, Feature.VISITOR_QR_TEOHONG, Feature.MAINTENANCE_GUIDE -> {
                                navigateToMenu(type, data)
                            }
                            Feature.ANNOUNCEMENT -> {
                                data.objectId.let { announcementId ->
                                    ContentActivity.start(this, announcementId)
                                }
                            }
                            Feature.CHAT -> {
                                if (data.unitId != null && data.projectId != null) {
                                    ChatActivity.start(this,  Hut(
                                            unitId = data.unitId,
                                            projectId = data.projectId,
                                            icon = data.profileImage ?: ""
                                    ), getString(R.string.chat))
                                } else {
                                    navigateToMenu(type, data)
                                }
                            }
                            Feature.PARTNER_SANSIRI_SMART_ELECTRIC_METER -> {
                                data.unitId?.let { unitId ->
                                    SmartElectricMeterActivity.start(this, unitId, null)
                                }
                            }
                            Feature.PARTNER_SANSIRI_SMART_WATER_METER -> {
                                data.unitId?.let { unitId ->
                                    SmartWaterMeterActivity.start(this, unitId, null)
                                }
                            }
                            else -> {

                            }
                        }
                    } else {

                    }
                } catch (e: Exception) {
                    Log.e("MainActivity", e.message, e)
                }
            }

        }
    }

    private fun navigateToMenu(type: String, notificationData: PushNotificationData?) {
        mOnNavigationItemSelectedListener.onNavigationItemSelected(navigation.menu.findItem(NAVIGATION_HOME))
        waitAndNavigateToMenu(type, notificationData)
    }

    private fun waitAndNavigateToMenu(feature: String, data: PushNotificationData?) {
        fragmentHome?.let {
            ({
                it.navigationToMenu(feature, data)
            }).withDelay(500)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("NAV_PAGE", navigation.selectedItemId)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        navigation.selectedItemId = savedInstanceState?.getInt("NAV_PAGE") ?: NAVIGATION_HOME
        super.onRestoreInstanceState(savedInstanceState)
    }


    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        handleIncomingIntent(intent)

        (application as AppController).guardRoute(this)

        navigation.apply {
            with(menu) {
                add(Menu.NONE, NAVIGATION_HOME, 0, "").setIcon(R.drawable.ic_home)
                add(Menu.NONE, NAVIGATION_ANNOUNCEMENT, 1, "").setIcon(R.drawable.ic_news_paper)
                if (FeatureConfig.SANSIRI_FAMILY) {
                    add(Menu.NONE, NAVIGATION_SANSIRI_FAMILY, 2, "").setIcon(R.drawable.ic_privileges)
                }
                add(Menu.NONE, NAVIGATION_NOTIFICATION, 3, "").setIcon(R.drawable.ic_notification2)
                add(Menu.NONE, NAVIGATION_MORE, 4, "").setIcon(R.drawable.ic_menu)
            }

            setTextVisibility(false)
            onNavigationItemSelectedListener = mOnNavigationItemSelectedListener
            itemIconTintList = ContextCompat.getColorStateList(this@MainActivity, R.drawable.primary_color_state_list)
            enableAnimation(false)
            enableShiftingMode(false)
            enableItemShiftingMode(false)
            mOnNavigationItemSelectedListener.onNavigationItemSelected(menu.findItem(NAVIGATION_HOME))

            selectedItemId = NAVIGATION_HOME
        }

        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        val height = Math.round(dm.heightPixels / dm.density)
        val width = Math.round(dm.widthPixels / dm.density)
        textDebug.text = "${width}x${height} (${dm.densityDpi})"
        textDebug.hide()

        when (BuildConfig.BUILD_TYPE) {
            "debug" -> Toast.makeText(this, "This is dev version", Toast.LENGTH_SHORT).show()
            "staging" -> Toast.makeText(this, "This is staging version", Toast.LENGTH_SHORT).show()
        }

        prefs = PreferenceHelper.defaultPrefs(this)
        val isShowAnnouncementBadge = prefs[PreferenceHelper.NOTIFICATION_ANNOUNCEMENT, false]
                ?: false
        if (isShowAnnouncementBadge) {
            badgeAnnouncement = addBadgeAt(1)
        }

        checkAppUpdate()

        requestLocationWithPermissionCheck()
        mPresenter.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                snackError("Update failed")
            }
        }
    }


    override fun showPopUps(popups: List<PopUp>) {
        var everyPopUpRead = true

        popups.forEach { popup ->
            if (popup.objectId != null && everyPopUpRead) {
                everyPopUpRead = PreferenceHelper.isPopupIsRead(this, popup.objectId)
            }
        }

        if (!everyPopUpRead) {
            PopUpActivity.start(this, arrayListOf<PopUp>().apply { addAll(popups) })
        }
    }

    override fun setPrivilegeButton(iconUrl: String) {
        if (FeatureConfig.SANSIRI_FAMILY) {
            Glide.with(this)
                    .load(iconUrl)
                    .into(object : SimpleTarget<Drawable>() {
                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                            navigation.menu.getItem(2).icon = resource
                        }
                    })
        }

    }

    private fun checkAppUpdate() {
        layoutAppUpdate.hide()

        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
            val appUpdateManager = AppUpdateManagerFactory.create(this)
            val appUpdateInfoTask = appUpdateManager.appUpdateInfo

            appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                ) {
                    layoutAppUpdate.show()
                    buttonUpdate.setOnClickListener {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo,
                                AppUpdateType.IMMEDIATE,
                                this,
                                APP_UPDATE)
                        layoutAppUpdate.hide()
                    }
                }
            }
        }
    }

    private var mFusedLocationClient: FusedLocationProviderClient? = null

    @SuppressLint("MissingPermission")
    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun requestLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            fragmentHome?.sortHomeByLocation(location)
        }

        mFusedLocationClient?.requestLocationUpdates(LocationRequest().apply {
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            interval = TimeUnit.MINUTES.toMillis(5)
        }, mLocationCallback, Looper.myLooper())

    }

    @OnShowRationale(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun showRationaleForLocation(request: PermissionRequest) {
        alert(getString(R.string.permission_rationale_location), getString(R.string.permission_rationale_location_title)) {
            yesButton { request.proceed() }
            noButton { request.cancel() }
        }
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onPermissionDenied() {
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onNeverAskAgain() {
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun showError(message: String) {
    }

    private fun addBadgeAt(position: Int): Badge {
        return QBadgeView(this)
                .setBadgeText("")
                .setShowShadow(false)
                .setBadgeBackgroundColor(resources.getColor(R.color.colorCancel))
                .setGravityOffset(26f, 2f, true)
                .bindTarget(navigation.getBottomNavigationItemView(position))
    }
}

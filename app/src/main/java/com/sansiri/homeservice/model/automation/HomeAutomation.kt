package com.sansiri.homeservice.model.automation

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 11/15/17.
 */
abstract class HomeAutomation {
    abstract var title: String
    abstract var icon: Int
    abstract var type: Type
    abstract var action: Int
    enum class Type {
        TITLE,
        AC,
        TOGGLE,
        SWITCH,
        CURTAIN,
        SELECTOR,
        SEEK_BAR,
        MUSIC_PLAYER,
        REMOTE_CONTROL,
        GROUP_TOGGLE,
    }
}
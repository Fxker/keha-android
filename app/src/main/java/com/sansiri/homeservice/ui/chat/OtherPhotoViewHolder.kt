package com.sansiri.homeservice.ui.chat

import android.graphics.Bitmap
import android.net.Uri
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.facebook.drawee.drawable.ProgressBarDrawable
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.util.TimeShow

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_chat_photo_other.view.*

/**
 * Created by oakraw on 28/10/2017 AD.
 */
class OtherPhotoViewHolder(val view: View, val senderProfileImage: String, val onImageClick: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(message: ChatMessage) {
        with(view) {
//            Glide.with(context).asBitmap().diskCacheStrategy(DiskCacheStrategy.RESOURCE).load(message.imageUrl).into(object: SimpleTarget<Bitmap>() {
//                override fun onResourceReady(resource: Bitmap?, transition: Transition<in Bitmap>?) {
//                    image.setImageBitmap(resource)
//                    card.setOnClickListener {
//                        message.imageUrl?.let(onImageClick)
//                    }
//                }
//            })
            image.hierarchy.setProgressBarImage(ProgressBarDrawable())
            image.setImageURI(message.imageUrl)
            card.setOnClickListener {
                message.imageUrl?.let(onImageClick)
            }
            textTime.text = TimeShow.show(message.createdAt)

            if (senderProfileImage.isNotEmpty()) {
                Glide.with(this).loadCache(senderProfileImage, imageProfile)
            } else {
                imageProfile.setImageResource(R.drawable.placeholder_contact)
            }
        }
    }
}
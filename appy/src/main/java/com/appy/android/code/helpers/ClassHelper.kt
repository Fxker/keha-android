package com.appy.android.code.helpers

internal class ClassHelper {
    companion object {
        const val CLASS_DAIKIN_INDOOR = "class_daikin_indoor"
        const val CLASS_OTHER = "class_other"

        @JvmStatic
        fun getClass(klass: String): String {
            return when (klass) {
                "Daikin::IndoorUnit" -> CLASS_DAIKIN_INDOOR
                else -> CLASS_OTHER
            }
        }
    }
}
package com.sansiri.homeservice.ui.tutorial.dynamic

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.tutorial.TutorialPage
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class TutorialDynamicPresenter : BasePresenterImpl<TutorialDynamicContract.View>(), TutorialDynamicContract.Presenter {
    private var mMenu: String? = null

    override fun init(menu: String) {
        mMenu = menu
    }

    override fun start() {
        if (mMenu != null) {
            fetch(mMenu!!)
        }
    }

    override fun fetch(menu: String) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                api.getTutorial(menu).observe().subscribe({ pages ->
                    mView?.bindData(pages)
                }){}
            }
        }
    }

    override fun destroy() {
    }
}
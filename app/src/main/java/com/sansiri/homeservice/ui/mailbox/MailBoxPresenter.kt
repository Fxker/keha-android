package com.sansiri.homeservice.ui.mailbox

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/12/17.
 */
class MailBoxPresenter : BasePresenterImpl<MailBoxContract.View>(), MailBoxContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData(unitObjectId: String) {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                mCompositeDisposable.add(
                        it.getMailBox(unitObjectId).observe().subscribe({
                            mView?.hideLoading()
                            if (it.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                mView?.bindData(it)
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }

//        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
//
//        mView?.setData(mutableListOf<MailBox>(
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-03")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-03")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-10")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-10"))
//        ))
    }
}
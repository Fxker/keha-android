package com.sansiri.homeservice.util.glide

import android.graphics.drawable.PictureDrawable
import android.widget.ImageView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
//import com.sansiri.homeservice.util.glide.GlideSvgExtension.Companion.asSvg
import kotlinx.android.synthetic.main.view_announcement_large.view.*

/**
 * Created by sansiri on 12/1/17.
 */
//fun RequestManager.renderSvg(uri: String, imageView: ImageView) {
////    this.`as`(PictureDrawable::class.java)
////            .transition(withCrossFade())
////            .listener(SvgSoftwareLayerSetter())
//    asSvg()
//            .load(uri)
//            .into(imageView)
//}

fun RequestManager.loadCache(uri: String, imageView: ImageView) {
    this.load(uri)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView)
}
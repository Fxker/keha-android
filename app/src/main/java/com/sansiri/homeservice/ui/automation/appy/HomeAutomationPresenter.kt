package com.sansiri.homeservice.ui.automation.appy

import android.util.Log
import com.appy.android.code.base.data.rest.error.APError
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.ac.APAirConditioner
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.coolautomation.APCoolAutomation
import com.appy.android.sdk.control.daikin.APDaikin
import com.appy.android.sdk.control.daikin.APDaikinParameter
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.light.LightCommand
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand
import com.appy.android.sdk.home.APHome
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.repository.appy.AppyRepository
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.localizeTitle
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 11/7/17.
 */
class HomeAutomationPresenter(var appyRepository: AppyRepository) : BasePresenterImpl<HomeAutomationContract.View>(), HomeAutomationContract.Presenter {
    private var mProjectId: String? = null
    private var mHomeId: String? = null
    private var mRoomId: String? = null

    override fun start() {}

    override fun destroy() {}

    override fun fetchData(projectId: String?, homeId: String?, roomId: String?) {
        mProjectId = projectId
        mHomeId = homeId
        mRoomId = roomId

        if (!mHomeId.isNullOrEmpty() && !mRoomId.isNullOrEmpty()) {
            appyRepository = AppyRepositoryImpl(projectId
                    ?: "", "12345")
            mView?.showLoading()
            appyRepository.getControl(homeId!!, roomId!!, {
                if (it.datas?.controls != null) {
                    mView?.hideLoading()
                    it.datas?.controls?.convert()?.let { convertedList ->
                        mView?.bindData(convertedList)
                    }
                }
            }) {
                Log.e("AppyHomeAutomation", it.message, it)
            }
        }
    }

    private fun checkDeviceStatus(controls: List<AppyHomeAutomation>) {
        val home = APHome()
        home.id = appyRepository.getDedicatedUnitId(mHomeId ?: "")

        controls.forEach { control ->
            if (control.apControl is APLight) {
                appyRepository.checkStatusLight(mHomeId ?: "", control.apControl, {
                    val value = it.data?.first()?.currentValue ?: "0"
                    control.action = if (value == "1") LightCommand.ON.ordinal else LightCommand.OFF.ordinal
                    mView?.updateDevices()
                }) {
                    Log.d("", "")
                }
            } else if (control.apControl is APToggleSwitch) {
                appyRepository.checkStatusToggleSwitch(mHomeId ?: "", control.apControl, {
                    val value = it.data?.currentValue ?: "0"
                    control.action = if (value == "1") ToggleSwitchCommand.ON.ordinal else ToggleSwitchCommand.OFF.ordinal
                    mView?.updateDevices()
                }) {
                    Log.d("", "")
                }
            } else if (control.apControl is APDimmer) {
                appyRepository.checkStatusDimmer(mHomeId ?: "", control.apControl, {
                    try {
                        val value = it.data?.currentValue?.toInt() ?: 0
                        control.action = value / 10
                        mView?.updateDevices()
                    } catch (e: Exception) {

                    }
                    //                    val value = it.html?.first()?.currentValue ?: "0"
//                    control.action = value
                }) {}
            } else if (control.apControl is APDaikin) {
                appyRepository.checkStatusDaikin(home, control.apControl, {}) {}
            }
//            else if (control.apControl is APBlind) {
//                mAppyManager?.checkStatusBlind(mmHomeId ?: "", control.apControl, {
//                    val value = it.html?.first()?.currentValue
//                    control.action = if (value == "on") BlindCommand.ON.ordinal else BlindCommand.OFF.ordinal
//                    mView?.updateDevices()
//                }) {
//                    Log.d("", "")
//                }
//            }
        }

    }

    private fun List<APControl>.convert(): List<AppyHomeAutomation> {
        val hardware = mutableListOf<AppyHomeAutomation>()
        val scene = mutableListOf<AppyHomeAutomation>()
        forEach { control ->

            isDeviceSupportLocalMode(control.klass ?: "")

            val homeAutomation = when (control) {
                is APLight -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_bulb, HomeAutomation.Type.TOGGLE, control)
                is APDimmer -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_bulb, HomeAutomation.Type.SEEK_BAR, control)
                is APAirConditioner -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_bulb, HomeAutomation.Type.TOGGLE, control)
                is APToggleSwitch -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_wall, HomeAutomation.Type.TOGGLE, control)
                is APBlind -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_curtain, HomeAutomation.Type.CURTAIN, control)
                is APScenario -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_book, HomeAutomation.Type.SWITCH, control)
                is APCoolAutomation -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_book, HomeAutomation.Type.AC, control)
                is APRemoteControl -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_air_condition, HomeAutomation.Type.REMOTE_CONTROL, control)
                is APDaikin -> AppyHomeAutomation(control.localizeTitle, R.drawable.ic_air_condition, HomeAutomation.Type.AC, control)
                else -> null
            }

            if (homeAutomation != null) {
                if (homeAutomation.apControl is APScenario) {
                    scene.add(homeAutomation)
                } else {
                    hardware.add(homeAutomation)
                }
            }

        }

        if (scene.isNotEmpty()) {
            hardware.add(AppyHomeAutomation("SCENE", 0, HomeAutomation.Type.TITLE, null))
            hardware.addAll(scene)
        }

        checkDeviceStatus(hardware)

        return hardware
    }

    private fun isDeviceSupportLocalMode(deviceClass: String) {
        if (!deviceClass.toLowerCase().contains("overkiz")) {
            mView?.showLocalModeButton()
        }
    }

    override fun action(control: APControl, commandNumber: Int) {
        val home = APHome()
        home.id = appyRepository.getDedicatedUnitId(mHomeId ?: "")

        try {
            when (control) {
                is APLight ->
                    appyRepository.commandLight(home, control, commandNumber, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
                is APToggleSwitch ->
                    appyRepository.commandToggleSwitch(home, control, commandNumber, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
                is APBlind ->
                    appyRepository.commandBlind(home, control, commandNumber, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
                is APDimmer ->
                    appyRepository.commandDimmer(home, control, commandNumber, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
                is APScenario ->
                    appyRepository.commandScene(home, control, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
                is APRemoteControl ->
                    mView?.launchRemoteControl(control, mProjectId
                            ?: "", mHomeId ?: "")
            }
        } catch (e: RuntimeException) {
            mView?.showError(e.localizedMessage)
        }
    }

    override fun action(control: APControl, param: APDaikinParameter) {
        val home = APHome()
        home.id = appyRepository.getDedicatedUnitId(mHomeId ?: "")

        try {
            when (control) {
                is APDaikin ->
                    appyRepository.commandDaikin(home, control, param, {
                        mView?.showMessage("Success")
                    }) { error ->
                        processErrorMessage(error)
                    }
            }
        } catch (e: RuntimeException) {
            mView?.showError(e.localizedMessage)
        }
    }

    private fun processErrorMessage(error: Throwable) {
        if (error is APError) {
            mView?.showError(error.description ?: error.error ?: error.code?.toString() ?: "Error")
        } else {
            mView?.showError(error.showHttpError())
        }
    }

}


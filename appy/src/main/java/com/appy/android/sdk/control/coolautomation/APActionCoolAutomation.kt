package com.appy.android.sdk.control.coolautomation

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionCoolAutomation : APAction<CoolAutomationCommand>, Parcelable {
    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionCoolAutomation> {
        override fun createFromParcel(parcel: Parcel): APActionCoolAutomation {
            return APActionCoolAutomation(parcel)
        }

        override fun newArray(size: Int): Array<APActionCoolAutomation?> {
            return arrayOfNulls(size)
        }
    }
}
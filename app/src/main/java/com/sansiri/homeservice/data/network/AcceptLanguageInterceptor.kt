package com.sansiri.homeservice.data.network

import android.content.Context
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GetTokenResult

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.configuration
import java.util.*

class AcceptLanguageInterceptor(val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val modifiedRequest = request.newBuilder()
                .addHeader("Accept-Language", getLanguage())
                .build()
        return chain.proceed(modifiedRequest)
    }

    fun getLanguage(): String {
        val locale = context.configuration.locale
        return when(locale) {
            Locale("th") -> "th, en;q=0.9, *;q=0.3"
            Locale.JAPANESE -> "ja, en;q=0.9, *;q=0.3"
            Locale.TRADITIONAL_CHINESE -> "zh-Hant, en;q=0.9, *;q=0.3"
            Locale.SIMPLIFIED_CHINESE -> "zh-Hans, en;q=0.9, *;q=0.3"
            else -> "en, th;q=0.9, *;q=0.3"
        }
    }

}
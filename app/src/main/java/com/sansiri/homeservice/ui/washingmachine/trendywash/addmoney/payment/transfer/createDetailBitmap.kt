package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer

import android.content.Context
import android.graphics.*
import androidx.core.content.res.ResourcesCompat
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.epayment.CodeEngine

fun createDetailBitmap(context: Context, amount: Int, accountName: String?): Bitmap {
    
    val finalBitmap = Bitmap.createBitmap(
            CodeEngine.imageWidth,
            480,
            Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(finalBitmap)
    val width = canvas.width - CodeEngine.padding * 2

    canvas.drawRect(0f, 0f, finalBitmap.width.toFloat(), finalBitmap.height.toFloat(), Paint().apply {
        style = Paint.Style.FILL
        color = Color.WHITE
    })

    val textBodyPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 18f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textTitlePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 24f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textHeadlinePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 28f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textDisplayPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 36f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    canvas.translate(CodeEngine.padding.toFloat(), CodeEngine.padding.toFloat())

    textLayout("Trendy Wash", textDisplayPaint, width).apply {
        draw(canvas)
        canvas.translate(0f, height.toFloat() + CodeEngine.newlineMargin + 20)
    }

    textLayout("เติมเงิน", textBodyPaint, width).apply {
        draw(canvas)
        canvas.translate(0f, height.toFloat() + CodeEngine.newlineMargin)
    }

    textLayout("$amount ${context.getString(R.string.baht)}", textHeadlinePaint, width).apply {
        draw(canvas)
        canvas.translate(0f, height.toFloat() + CodeEngine.newlineMargin + 20)
    }

    if (accountName != null) {
        textLayout("สำหรับ Trendy Wash บัญชี", textBodyPaint, width).apply {
            draw(canvas)
            canvas.translate(0f, height.toFloat() + CodeEngine.newlineMargin)
        }

        textLayout(accountName, textBodyPaint, width).apply {
            draw(canvas)
            canvas.translate(0f, height.toFloat() + CodeEngine.newlineMargin)
        }
    }



    return finalBitmap
}

fun textLayout(text: String?, paint: TextPaint, width: Int): StaticLayout {
    return StaticLayout(text, paint, width, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false)
}
package com.sansiri.homeservice.ui.automation.orvibo.home

//import com.sansiri.homeservice.data.repository.orvibo.OrviboRepository
//import com.sansiri.homeservice.ui.base.CoroutineScopePresenter
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.withContext
//
//class OrviboHomePresenter(val orviboRepository: OrviboRepository) : CoroutineScopePresenter<OrviboHomeContract.View>(), OrviboHomeContract.Presenter {
//    var mUnitId: String? = null
//
//    override fun init(unitId: String) {
//        mUnitId = unitId
//    }
//
//    override fun start() {
//        if (orviboRepository.isLoggedIn()) {
//            fetch()
//        } else {
//            mView?.launchLoginPage()
//        }
//    }
//
//    override fun fetch() {
//        mView?.hideFamilySelector()
//
//        orviboRepository.getFamilies { families ->
//            val currentFamilyId = orviboRepository.getCurrentFamilyId()
//            if (mUnitId != null && currentFamilyId != null) {
//                launch {
//                    orviboRepository.updateFamilyId(mUnitId!!, currentFamilyId)
//                }
//            }
//            val currentFamily = families.find { family -> family.id == currentFamilyId }
//
//            if (currentFamily != null) {
//                mView?.bindCurrentFamily(currentFamily)
//            }
//
//            if (families.size > 1) {
//                mView?.bindFamilies(families)
//            }
//        }
//    }
//
//    override fun switchFamily(familyId: String?) {
//        if (familyId != null) {
//            orviboRepository.switchFamily(familyId) {
//                fetch()
//            }
//        } else {
//
//        }
//    }
//
//    override fun logout() {
//        mUnitId?.let {
//            launch {
//                orviboRepository.deleteOrviboHome(mUnitId!!)
//                withContext(Dispatchers.Main) {
//                    orviboRepository.logout()
//                    mView?.logoutSuccess()
//                }
//            }
//        }
//    }
//
//    override fun destroy() {
//    }
//}
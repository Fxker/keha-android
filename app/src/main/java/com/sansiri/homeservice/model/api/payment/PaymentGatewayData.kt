package com.sansiri.homeservice.model.api.payment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentGatewayData(
        var command: String? = null,
        var merchantName: String? = null,
        var gatewayName: String? = null,
        var paymentTitle: String? = null,
        var refCode1: String? = null,
        var refCode2: String? = null,
        var amount: Double = 0.0,
        var mainThemeColor: Int? = null,
        var accentThemeColor: Int? = null,
        var icon: String? = null
): Parcelable
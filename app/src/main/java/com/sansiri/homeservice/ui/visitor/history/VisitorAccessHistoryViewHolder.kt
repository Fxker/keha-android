package com.sansiri.homeservice.ui.visitor.history

import androidx.recyclerview.widget.RecyclerView
import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_card.view.*
import org.jetbrains.anko.textColor

/**
 * Created by sansiri on 10/12/17.
 */
class VisitorAccessHistoryViewHolder(val view: View, val itemClick: (VisitorAccessHistory) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: VisitorAccessHistory) {
        with(view) {
            textType.apply {
                textColor = context.resources.getColor(R.color.textHint)
                text = data._createdAt?.reportTime()
            }
            textTitle.apply {
                text = data.message
                setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_subhead));
            }
            textDate.hide()
            imageIcon.setImageResource(R.drawable.ic_qr_code)
            textSubtitle.hide()
            root.isClickable = false
            root.setOnClickListener(null)
        }
    }
}
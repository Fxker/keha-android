package com.appy.android.code.data.rest

import android.content.Context
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.data.rest.JsonModel.fibaro.LocalId
import com.appy.android.code.data.rest.adapter.ControlAdapter
import com.appy.android.code.data.rest.adapter.RoomAdapter
import com.appy.android.code.data.rest.adapter.fibaro.LocalIdAdapter
import com.appy.android.sdk.control.ControlsApiResult
import com.appy.android.sdk.room.RoomApiResult
import com.google.gson.Gson
import com.minor.bemynt.helpers.ConstantHelper
import com.minor.bemynt.helpers.GsonHelper
import io.reactivex.schedulers.Schedulers
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Inject

/**
 * Created by Nott on 18/9/2560.
 * AppySDK
 */
internal class RestClient @Inject
constructor(context: Context, threadExecutor: ThreadExecutor) : AbstractBaseRestClient(context) {

    init {
        retrofitBuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.from(threadExecutor)))
    }

    override fun getBaseUrl(): String = ConstantHelper.getControlApiDomain()

    override fun getGson(): Gson {
        return GsonHelper.builder()
                .registerTypeAdapter(RoomApiResult::class.java, RoomAdapter())
                .registerTypeAdapter(ControlsApiResult::class.java, ControlAdapter())
                .registerTypeAdapter(LocalId::class.java, LocalIdAdapter())
                .create()
    }

    fun <S> createAPIService(serviceClass: Class<S>): S {
        return createService(serviceClass)
    }

    fun <S> createAPIService(serviceClass: Class<S>, username: String, password: String): S {
        return createService(
            serviceClass,
            username,
            password
        )
    }

    fun <S> createAPIService(serviceClass: Class<S>, accessToken: String): S {
        return createService(
            serviceClass,
            AuthenticationInterceptor("Authorization", accessToken)
        )
    }
}

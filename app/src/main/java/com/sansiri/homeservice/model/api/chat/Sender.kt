package com.sansiri.homeservice.model.api.chat

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 12/21/17.
 */
data class Sender(val type: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(type)
    }

    companion object {
        val TYPE_USER = "user"

        val TYPE_STAFF = "staff"

        @JvmField
        val CREATOR: Parcelable.Creator<Sender> = object : Parcelable.Creator<Sender> {
            override fun createFromParcel(source: Parcel): Sender = Sender(source)
            override fun newArray(size: Int): Array<Sender?> = arrayOfNulls(size)
        }
    }
}

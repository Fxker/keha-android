package com.sansiri.homeservice.model.api.partner.svvh

data class SVVHPromotion(
	val title: String? = null,
	val details: String? = null,
	val titleRGB: String? = null
)

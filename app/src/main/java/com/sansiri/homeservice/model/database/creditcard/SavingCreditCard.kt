package com.sansiri.homeservice.model.database.creditcard

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "saving_credit_card")
data class SavingCreditCard(
        @PrimaryKey
        @ColumnInfo(name = "cardToken")
        val cardToken: String,
        @ColumnInfo(name = "securedCardNumber")
        val securedCardNumber: String? = null,
        @ColumnInfo(name = "provider")
        val provider: String? = null,
        @ColumnInfo(name = "billingEmail")
        val billingEmail: String? = null
)
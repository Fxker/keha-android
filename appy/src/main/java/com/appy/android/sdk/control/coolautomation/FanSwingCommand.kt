package com.appy.android.sdk.control.coolautomation

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
enum class FanSwingCommand : APCoolAutomationParameter {
    HORIZONTAL {
        override fun action(): String {
            return "H"
        }
    },
    VERTICAL {
        override fun action(): String {
            return "V"
        }
    },
    AUTO {
        override fun action(): String {
            return "A"
        }
    };
}
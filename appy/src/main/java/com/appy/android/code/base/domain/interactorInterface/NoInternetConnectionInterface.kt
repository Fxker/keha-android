package com.appy.android.code.base.domain.interactorInterface

import io.reactivex.subjects.PublishSubject

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal interface NoInternetConnectionInterface {
    fun noInternetConnection(retrofitError: Throwable): PublishSubject<Int>
}
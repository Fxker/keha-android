package com.sansiri.homeservice.ui.register

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/8/17.
 */
interface RegisterContract {
    interface Presenter : BasePresenter<View> {
        fun bindUser(onSuccess: () -> Unit, onFail: (Throwable) -> Unit)
        fun storeCustomerId(customerId: String)
    }

    interface View : BaseView {
    }
}
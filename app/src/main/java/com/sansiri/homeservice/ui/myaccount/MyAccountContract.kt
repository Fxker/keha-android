package com.sansiri.homeservice.ui.myaccount

import com.sansiri.homeservice.model.api.Balance
import com.sansiri.homeservice.model.api.Invoice
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/31/17.
 */
interface MyAccountContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String)
    }

    interface View : BaseView {
        fun bindData(balance: Balance)
        fun bindList(list: List<Invoice>)
    }
}
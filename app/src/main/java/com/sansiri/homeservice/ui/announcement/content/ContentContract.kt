package com.sansiri.homeservice.ui.announcement.content

import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 3/13/18.
 */
interface ContentContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(announcementId: String)
    }

    interface View : BaseView {
        fun renderAnnouncement(announcement: Announcement<*>)
    }
}
package com.sansiri.homeservice.ui.register.forgotpassword

import android.util.Patterns
import com.google.firebase.auth.FirebaseAuth
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 2/26/18.
 */
class ForgotPasswordPresenter : BasePresenterImpl<ForgotPasswordContract.View>(), ForgotPasswordContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun resetPassword(email: String) {
        mView?.showLoading( )
        FirebaseAuthManager.resetPassword(email, {
            mView?.hideLoading()
            mView?.showSuccess()
        }) {
            mView?.hideLoading()
            mView?.showError(it.localizedMessage)
        }
    }

    override fun validateAndSend(email: String) {
        var isPass = true

        if (email.isBlank()) {
            mView?.emailError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView?.emailError(mView?.getStringFromRes(R.string.please_enter_a_valid_email) ?: "")
            isPass = false
        }

        if (isPass) {
            resetPassword(email)
        }
    }
}
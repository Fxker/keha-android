package com.sansiri.homeservice.data.repository.trendywash

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.network.auth.TrendyWashService
import com.sansiri.homeservice.data.network.networkCall
import com.sansiri.homeservice.data.repository.omise.OmiseRepository
import com.sansiri.homeservice.model.api.partner.trendywash.*

class TrendyWashRepositoryImpl(val service: TrendyWashService, val omiseRepository: OmiseRepository, val serverUrl: String) : TrendyWashRepository {
    val creditInfo = MutableLiveData<Resource<TrendyWashCredit>>()
    val machines = MutableLiveData<Resource<List<TrendyWashMachine>>>()

    override fun loginTrendyWash(unitId: String, request: TrendyWashLogin) = networkCall<TrendyWashRegisterCustomer> {
        client = service.loginTrendyWash(unitId, request)
    }

    override fun getFacebookLoginTrendyWashLink(unitId: String) = "${serverUrl}me/units/$unitId/partners/trendyWash/users/login/facebook"

    override fun registerTrendyWashAccount(unitId: String, request: TrendyWashRegisterCustomer) = networkCall<TrendyWashRegisterCustomer> {
        client = service.registerTrendyWashAccount(unitId, request)
    }

    override fun getTrendyWashMachines(unitId: String, userId: String) = networkCall<List<TrendyWashMachine>>(machines) {
        client = service.getTrendyWashMachines(unitId, userId)
    }

    override fun getTrendyWashCredit(unitId: String, userId: String) = networkCall<TrendyWashCredit>(creditInfo) {
        client = service.getTrendyWashCredit(unitId, userId)
    }

    override fun checkoutTrendyWashPaymentQR(unitId: String, trendyWashUserId: String, request: TrendyWashPaymentGatewayRequest) = networkCall<TrendyWashPaymentGateWayQR> {
        client = service.checkoutTrendyWashPaymentQR(unitId, trendyWashUserId, request)
    }

    override fun checkoutTrendyWashCreditCard(amount: Int, unitId: String, trendyWashUserId: String, cardNumber: String, cardName: String, expirationMonth: Int, expirationYear: Int, securityCode: String): MutableLiveData<Resource<TrendyWashPaymentGateWayQR>> {
        val creditCardCheckout = MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>()
        creditCardCheckout.value = Resource.loading(null)

        omiseRepository.verifyCard(
                cardNumber = cardNumber,
                cardName = cardName,
                expirationMonth = expirationMonth,
                expirationYear = expirationYear,
                securityCode = securityCode,
                onSuccess = { tokenId ->
                    networkCall(creditCardCheckout) {
                        client = service.checkoutTrendyWashCreditCard(unitId, trendyWashUserId, TrendyWashPaymentGatewayRequest(amount, tokenId))
                    }
                }
        ) {
            creditCardCheckout.value = Resource.error(it.message, 0)
        }

        return creditCardCheckout
    }

    override fun startTrendyWashMachine(unitId: String, trendyWashUserId: String, machineId: String, request: TrendyWashMachineStart) = networkCall<TrendyWashMachineStart> {
        client = service.startTrendyWashMachine(unitId, trendyWashUserId, machineId, request)
    }
}
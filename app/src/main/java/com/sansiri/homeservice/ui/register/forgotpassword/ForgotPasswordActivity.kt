package com.sansiri.homeservice.ui.register.forgotpassword

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.removeSpace
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class ForgotPasswordActivity : BaseActivity<ForgotPasswordContract.View, ForgotPasswordContract.Presenter>(), ForgotPasswordContract.View {
    override var mPresenter: ForgotPasswordContract.Presenter = ForgotPasswordPresenter()

    companion object {
        val EMAIL = "EMAIL"

        fun start(activity: Activity, email: String) {
            val intent = Intent(activity, ForgotPasswordActivity::class.java)
            intent.putExtra(EMAIL, email)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        editEmail.setText(intent?.getStringExtra(EMAIL))
        buttonReset.setOnClickListener {
            mPresenter.validateAndSend(editEmail.text.toString().removeSpace())
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("FORGOT_PASSWORD")
    }


    override fun showLoading() {
        editEmail.isEnabled = false
        progressBar.show()
        buttonReset.hide()
    }

    override fun hideLoading() {
        editEmail.isEnabled = true
        progressBar.hide()
        buttonReset.show()
    }

    override fun showError(message: String) {
        alertError(message)
    }

    override fun emailError(message: String) {
        editEmail.error = message
    }

    override fun getStringFromRes(res: Int): String {
        return getString(res)
    }

    override fun showSuccess() {
        alert(getString(R.string.password_reset_email_message)) {
            yesButton {
                finish()
            }
        }.show()
    }

}


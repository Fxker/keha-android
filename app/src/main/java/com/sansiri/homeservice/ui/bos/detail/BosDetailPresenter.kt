package com.sansiri.homeservice.ui.bos.detail

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.repository.bos.BosRepository
import com.sansiri.homeservice.model.api.bos.BosDetail
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.bos.BosActivity
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign
import retrofit2.HttpException

class BosDetailPresenter(val repository: BosRepository) : BasePresenterImpl<BosDetailContract.View>(), BosDetailContract.Presenter {
    private var mUnitId: String? = null
    private var mGroupType: Int = 0
    private var mJobType: BosMenu? = null

    override fun init(unitId: String?, groupType: Int, jobType: BosMenu?) {
        this.mUnitId = unitId
        this.mGroupType = groupType
        this.mJobType = jobType
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                api.getMe().observe().subscribe({
                    mView?.bindUser(it)
                }) {}
            }
        }
    }

    override fun submit() {
        val detail = mView?.getDetail()
        val name = mView?.getName()
        val contact = mView?.getContact()
        val floor = mView?.getFloor()
        val area = mView?.getArea()

        if (mGroupType == BosActivity.TYPE_UNIT) {
            if (name.isNullOrEmpty() || detail.isNullOrEmpty() || contact.isNullOrEmpty()) {
                mView?.showAlertToEnterDetail()
                return
            }
        } else {
            if (name.isNullOrEmpty() || detail.isNullOrEmpty() || contact.isNullOrEmpty() || floor.isNullOrEmpty() || area.isNullOrEmpty()) {
                mView?.showAlertToEnterDetail()
                return
            }
        }

        mView?.prepareBitmapToUploadIfExist()

    }

    override fun uploadImage(images: List<Pair<String, ByteArray>>) {
        val imagePath = mutableListOf<String>()
        updateProgress(imagePath.size, images.size)
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                val paths = mutableListOf<String>()
                updateProgress(paths.size, images.size)

                images.forEach { image ->
                    val filename = image.first
                    val data = image.second

                    mCompositeDisposable += api.createFileToUpload(filename).observe().subscribe({
                        val downloadUrl = it.downloadUrl ?: ""
                        val uploadUrl = it.uploadUrl ?: ""
                        ApiRepositoryProvider.provideApiUnAuthRepository().uploadFile(uploadUrl, data).observe().subscribe {
                            paths.add(downloadUrl)
                            val isFinish = updateProgress(paths.size, images.size)
                            if (isFinish) {
                                createRequest(paths)
                            }
                        }?.let { mCompositeDisposable += it }
                    }) {
                        mView?.showError(it.showHttpError())
                    }
                }
            } else {
                mView?.tryAgain()
                mView?.showError("Network Error")
            }
        }
    }

    override fun createRequest(imagePath: List<String>) {
        val detail = mView?.getDetail()
        val name = mView?.getName()
        val contact = mView?.getContact()
        val area = mView?.getArea()
        val floor = mView?.getFloor()
        val building = mView?.getBuilding()

        val bosDetail = BosDetail(
                detail = detail,
                contactName = name,
                contactPhoneNo = contact,
                zone = area,
                floor = floor,
                building = building,
                typeId = mJobType?.id,
                imageUrl = imagePath.getOrNull(0)
        )

        if (mGroupType == BosActivity.TYPE_UNIT) {
            mView?.showSubmitProgress()
            mCompositeDisposable += repository.submitMyHomeIssue(mUnitId
                    ?: "", bosDetail).observe().subscribe({
                mView?.showSuccess()
            }) {
                mView?.showError(it.showHttpError())
                mView?.tryAgain()
            }
        } else {
            mView?.showSubmitProgress()
            mCompositeDisposable += repository.submitCommonAreaIssue(mUnitId
                    ?: "", bosDetail).observe().subscribe({
                mView?.showSuccess()
            }) {
                mView?.showError(it.showHttpError())
                mView?.tryAgain()
            }
        }
//
//        val requestItem = HomeCareRequest.HomeCareRequestItem(mTypeId, detail, imagePath)
//        val homeCareRequest = HomeCareRequest(name, contact, email, listOf(requestItem))
//
//        ApiRepositoryProvider.provideApiAuthRepository { api ->
//            if (api != null) {
//                mView?.showSubmitProgress()
//                if (!mUnitId.isEmpty()) {
//                    api.submitHomeCare(mUnitId, homeCareRequest).observe().subscribe({
//                        mView?.showSuccess()
//                    }) {
//                        mView?.showError(it.showHttpError())
//                        mView?.tryAgain()
//                    }
//                } else {
//                    mView?.tryAgain()
//                }
//            } else {
//                mView?.tryAgain()
//            }
//        }
    }

    private fun updateProgress(progress: Int, max: Int): Boolean {
        mView?.showUploadProgress(progress, max)
        if (progress == max) {
            return true
        }
        return false
    }

}
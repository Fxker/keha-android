package com.sansiri.homeservice.ui.homecare.history

import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.model.api.ListResponse
import com.sansiri.homeservice.ui.homecare.HomeCareOrderViewHolder
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.withDelay

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class HomeCareHistoryAdapter(val itemClick: (homeCareHistory: HomeCareHistory) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<HomeCareHistory>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return HomeCareOrderViewHolder(parent.inflate(R.layout.view_summary_card), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is HomeCareOrderViewHolder) {
            holder.bind(mData[position])
        }
    }


    fun setData(data: ListResponse<HomeCareHistory>) {
        mData.clear()
        mData.addAll(data.items)
        notifyDataSetChanged()
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addDataDelay(data: List<HomeCareHistory>) {
        data.forEach {
            {
                mData.add(it)
                notifyItemInserted(mData.size - 1)
            }.withDelay(1)
        }
    }


}
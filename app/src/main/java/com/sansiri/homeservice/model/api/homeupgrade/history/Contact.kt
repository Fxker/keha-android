package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable

data class Contact(
            val email: String? = null,
            val name: String? = null,
            val phone: String? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(email)
            writeString(name)
            writeString(phone)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Contact> = object : Parcelable.Creator<Contact> {
                override fun createFromParcel(source: Parcel): Contact = Contact(source)
                override fun newArray(size: Int): Array<Contact?> = arrayOfNulls(size)
            }
        }
    }
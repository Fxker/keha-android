package com.sansiri.homeservice.ui.epayment

import android.app.Activity
import android.content.Context
import android.graphics.*
import androidx.core.content.res.ResourcesCompat
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.sansiri.homeservice.R

class CodeEngine(val context: Context) {
    companion object {
        val imageWidth = 1080
        val padding = 50
        val timeToHidePreviewImage = 2000L
        val fontMultiplier = 2.5f
        val newlineMargin = 20f
    }


    fun createBarcode(code: String): Bitmap {
        return BarcodeEncoder().encodeBitmap(code, BarcodeFormat.CODE_128, 400, 50)
    }

    fun createQR(code: String): Bitmap {
        return BarcodeEncoder().encodeBitmap(code, BarcodeFormat.QR_CODE, 300, 300)
    }

    fun createBarcodeWithCode(barcodeBitmap: Bitmap, code: String?): Bitmap {
        val textBitmap = Bitmap.createBitmap(
                imageWidth - padding * 2,
                50,
                Bitmap.Config.ARGB_8888
        )

        val textCaptionPaint = TextPaint().apply {
            color = Color.BLACK
            typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
            textSize = 12f * fontMultiplier
            isAntiAlias = true
        }

        val canvas = Canvas(textBitmap)
        canvas.translate(0f, CodeEngine.newlineMargin / 2)

        canvas.drawRect(0f, 0f, textBitmap.width.toFloat(), textBitmap.height.toFloat(), Paint().apply {
            style = Paint.Style.FILL
            color = Color.WHITE
        })
        StaticLayout(code, textCaptionPaint, canvas.width, Layout.Alignment.ALIGN_CENTER, 1f, 0f, false).draw(canvas)

        val barcodeWithCodeBitmap = extendBitmap(barcodeBitmap, textBitmap)

        val barcodeWithCodeWithPaddingBitmap = Bitmap.createBitmap(
                barcodeWithCodeBitmap.width + padding * 2,
                barcodeWithCodeBitmap.height + padding * 2,
                Bitmap.Config.ARGB_8888
        )
        val canvasWithPadding = Canvas(barcodeWithCodeWithPaddingBitmap)
        canvasWithPadding.drawRect(0f, 0f, barcodeWithCodeWithPaddingBitmap.width.toFloat(), barcodeWithCodeWithPaddingBitmap.height.toFloat(), Paint().apply {
            style = Paint.Style.FILL
            color = Color.WHITE
        })
        canvasWithPadding.translate(padding.toFloat(), padding.toFloat())
        canvasWithPadding.drawBitmap(barcodeWithCodeBitmap, null, RectF(0f, 0f, barcodeWithCodeBitmap.width.toFloat(), barcodeWithCodeBitmap.height.toFloat()), null)

        return barcodeWithCodeWithPaddingBitmap
    }

    fun extendBitmap(topBitmap: Bitmap, bottomBitmap: Bitmap): Bitmap {
        val actualWidth = Math.max(topBitmap.width, bottomBitmap.width)

        val resizedTopBitmap = Bitmap.createScaledBitmap(
                topBitmap, actualWidth, topBitmap.height * actualWidth / topBitmap.width, false)

        val resizedBottomBitmap = Bitmap.createScaledBitmap(
                bottomBitmap, actualWidth, bottomBitmap.height * actualWidth / bottomBitmap.width, false)

        val finalBitmap = Bitmap.createBitmap(
                actualWidth,
                resizedTopBitmap.height + resizedBottomBitmap.height,
                Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(finalBitmap)
        canvas.drawBitmap(resizedTopBitmap, null, RectF(0f, 0f, resizedTopBitmap.width.toFloat(), resizedTopBitmap.height.toFloat()), null)
        canvas.translate(0f, resizedTopBitmap.height.toFloat())
        canvas.drawBitmap(resizedBottomBitmap, null, RectF(0f, 0f, resizedBottomBitmap.width.toFloat(), resizedBottomBitmap.height.toFloat()), null)

        return finalBitmap
    }
}
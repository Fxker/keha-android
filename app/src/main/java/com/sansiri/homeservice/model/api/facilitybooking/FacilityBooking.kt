package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.data.database.Store

data class FacilityBooking(
		@SerializedName("category_data")
		var categoryData: CategoryData? = null,
		val contents: List<ContentsItem>? = null,
		val preview: Preview? = null,
		val media: List<Media>? = null,
		val id: String? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readParcelable<CategoryData>(CategoryData::class.java.classLoader),
			source.createTypedArrayList(ContentsItem.CREATOR),
			source.readParcelable<Preview>(Preview::class.java.classLoader),
			source.createTypedArrayList(Media.CREATOR),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeParcelable(categoryData, 0)
		writeTypedList(contents)
		writeParcelable(preview, 0)
		writeTypedList(media)
		writeString(id)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<FacilityBooking> = object : Parcelable.Creator<FacilityBooking> {
			override fun createFromParcel(source: Parcel): FacilityBooking = FacilityBooking(source)
			override fun newArray(size: Int): Array<FacilityBooking?> = arrayOfNulls(size)
		}
	}

	fun getContent(): ContentsItem? {
		return if(Store.getLocale()?.language == "th") {
			findContentFromLocale("th")
		} else {
			findContentFromLocale("en")
		}
	}

	fun findContentFromLocale(localeCode: String): ContentsItem? {
		contents?.forEach { content ->
			if (content.language?.languageCode == localeCode) {
				return content
			}
		}
		return contents?.get(0)
	}
}

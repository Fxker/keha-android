package com.sansiri.homeservice.ui.myhome

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.MyHomeDocument
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.ui.announcement.all.AllAnnouncementAdapter
import com.sansiri.homeservice.ui.announcement.all.AnnouncementViewHolder
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class MyHomeDocumentAdapter(val itemClick: (MyHomeDocument) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<DocumentViewHolder>() {
    private val mData = mutableListOf<MyHomeDocument>()

    fun setData(list: List<MyHomeDocument>) {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DocumentViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentViewHolder {
        return DocumentViewHolder(parent.inflate(R.layout.view_document_my_home), itemClick)
    }
}
package com.sansiri.homeservice.ui.service

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.model.menu.*
import com.sansiri.homeservice.ui.adapter.GridMenuSectionAdapter
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.snackError
import com.sansiri.homeservice.util.startApp
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*

class MenuServiceSectionActivity : LocalizationActivity() {

    companion object {
        val SECTION = "SECTION"
        val TITLE = "TITLE"

        fun start(activity: Activity, title: String, sections: List<Section>) {
            val intent = Intent(activity, MenuServiceSectionActivity::class.java)
            intent.putExtra(SECTION, sections.toTypedArray())
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }
    private lateinit var analyticProvider: AnalyticProvider
    val mAdapter = SectionedRecyclerViewAdapter()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        analyticProvider = AnalyticProvider(application as AppController)

        val sections = intent?.getParcelableArrayExtra(SECTION)
        val title = intent?.getStringExtra(TITLE) ?: ""


        setBackToolbar(title)

        list.apply {
            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(this@MenuServiceSectionActivity, resources.getInteger(R.integer.grid_column))
            gridLayoutManager.spanSizeLookup = object : androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (mAdapter.getSectionItemViewType(position)) {
                        SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> resources.getInteger(R.integer.grid_column)
                        else -> 1
                    }
                }
            }

            layoutManager = gridLayoutManager
            adapter = mAdapter
        }

        sections?.let {
            bindData(it.toList() as List<Section>)
        }
    }

    private fun bindData(list: List<Section>) {
        list.forEach {
            mAdapter.addSection(GridMenuSectionAdapter(R.color.colorSecondary, it.displayName ?: "", it.items!!) { menu ->
                if (menu is DynamicMenu) {
                    actionExtraMenu(menu)
                }
            })
        }
    }

    private fun actionExtraMenu(menu: DynamicMenu) {
        when (menu.type) {
            ActionType.EXTERNAL_APP -> {
                if (menu.androidUri != null) {
                    launchDeepLink(menu.id ?: "", menu.title
                            ?: "", menu.androidUri, menu.androidPackageName)
                } else if (menu.androidPackageName != null) {
                    launchAnotherApp(menu.id ?: "", menu.title ?: "", menu.androidPackageName)
                } else {
                    showError("Cannot open the app.")
                }
            }
            ActionType.EXTERNAL_BROWSER -> menu.url?.let{ launchWeb(it, menu.title ?: "") }
            ActionType.MORE_MENU -> menu.items?.let { launchMenuService(menu.title ?: "", it) }
        }


    }

    private fun launchDeepLink(id: String, title: String, uri: String, androidPackageName: String?) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(uri)
            startActivity(intent)
            analyticProvider.sendEvent("Shopping & Service", "Open", id)
        } catch (e: ActivityNotFoundException) {
            androidPackageName?.let {
                launchAnotherApp(id, title, it)
            }
        }
    }

    fun launchAnotherApp(id: String, title: String, packageName: String) {
        analyticProvider.sendEvent("Shopping & Service", "Open", id)
        startApp(packageName)
    }

    private fun launchMenuService(title: String, section: List<Section>) {
        MenuServiceSectionActivity.start(this, title, section)
    }

    private fun launchWeb(url: String, title: String) {
        WebActivity.start(this, url, title)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

     fun showError(message: String?) {
        snackError(message ?: "")
    }
}
package com.sansiri.homeservice.model.automation

//import com.homemate.sdk.model.HMControl
//import com.homemate.sdk.model.HMDevice

/**
 * Created by sansiri on 10/10/17.
 */
//data class OrviboHomeAutomation(
//        override var title: String = "",
//        override var icon: Int,
//        override var type: HomeAutomation.Type,
//        val orviboDevice: HMControl,
//        override var action: Int = 0) : HomeAutomation() {
//    var isLoading = false
//
//    companion object {
//        val typesOnOff = listOf(HMDevice.DeviceType.LAMP,
//                HMDevice.DeviceType.CONTACT_RELAY,
//                HMDevice.DeviceType.SWITCH_RELAY,
//                HMDevice.DeviceType.SINGLE_FIRE_SWITCH
//        )
//
//        val typesToggle = listOf(
//                HMDevice.DeviceType.S20,
//                HMDevice.DeviceType.COCO
//        )
//
//        val typesDimmer = listOf(
//                HMDevice.DeviceType.DIMMER
//        )
//
//        val availableDeviceTypes = listOf(
//                HMDevice.DeviceType.LAMP,
//                HMDevice.DeviceType.CONTACT_RELAY,
//                HMDevice.DeviceType.SWITCH_RELAY,
//                HMDevice.DeviceType.SINGLE_FIRE_SWITCH,
//                HMDevice.DeviceType.DIMMER
//        )
//    }
//
//    fun isAvailable() = !(orviboDevice is HMDevice && !availableDeviceTypes.contains(orviboDevice.type))
//
//
//    fun isOnOffSwitch(): Boolean {
//        if (orviboDevice is HMDevice && typesOnOff.contains(orviboDevice.type)) {
//            return true
//        }
//
//        return false
//    }
//
//    fun isToggleSwitch(): Boolean {
//        if (orviboDevice is HMDevice && typesToggle.contains(orviboDevice.type)) {
//            return true
//        }
//
//        return false
//    }
//
//    fun isDimmer(): Boolean {
//        if (orviboDevice is HMDevice && typesDimmer.contains(orviboDevice.type)) {
//            return true
//        }
//
//        return false
//    }
//}
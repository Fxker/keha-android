package com.appy.android.code.domain.model.command.dimmer

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.dimmer.DimmerCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandDimmerForm(
        val homeId: String,
        val dimmerId: String,
        val command: DimmerCommand
) : AppyForm() {
    val mSendCommandDimmerJsonBody: SendCommandDimmerJsonBody = SendCommandDimmerJsonBody()

    init {
        this.mSendCommandDimmerJsonBody.command = command.action()
    }

}
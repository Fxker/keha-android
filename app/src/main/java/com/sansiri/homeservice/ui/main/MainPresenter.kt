package com.sansiri.homeservice.ui.main

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class MainPresenter : BasePresenterImpl<MainContract.View>(), MainContract.Presenter {
    override fun start() {
        fetchPopUp()

        FirebaseRemoteConfigManager().getPrivilegeDynamicButton { button ->
            if (button.icon != null) {
                mView?.setPrivilegeButton(button.icon)
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetchPopUp() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(api.getPopUp().observe().subscribe({ popups ->
                    mView?.showPopUps(popups)
                }) {})
            }
        }
    }
}
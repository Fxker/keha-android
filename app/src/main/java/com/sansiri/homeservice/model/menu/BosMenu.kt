package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.R
import kotlinx.android.parcel.Parcelize

/**
 * Created by oakraw on 10/3/2017 AD.
 */
@Parcelize
class BosMenu(
        @SerializedName("objectId")
        override val id: String?,
        @SerializedName("displayName")
        override val title: String? = null,
        @SerializedName("iconUrl")
        override val icon: String? = null,
        val orderNo: Int? = null
) : Menu, Parcelable

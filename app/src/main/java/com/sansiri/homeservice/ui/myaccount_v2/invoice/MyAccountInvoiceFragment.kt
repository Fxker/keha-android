package com.sansiri.homeservice.ui.myaccount_v2.invoice

import android.os.Bundle
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_my_account_invoice.*

class MyAccountInvoiceFragment : BaseFragment<MyAccountInvoiceContract.View, MyAccountInvoiceContract.Presenter>(), MyAccountInvoiceContract.View {
    override var mPresenter: MyAccountInvoiceContract.Presenter = MyAccountInvoicePresenter()
    private var mHome: Hut? = null

    companion object {
        const val HOME = "HOME"

        fun newInstance(home: Hut): MyAccountInvoiceFragment {
            val fragment = MyAccountInvoiceFragment()
            val bundle = Bundle().apply {
                putParcelable(HOME, home)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHome = arguments?.getParcelable(HOME)

    }

    val mAdapter = MyAccountInvoiceAdapter(R.color.colorCancel) { invoice ->
        if (mHome != null) {
            MyAccountInvoiceDetailActivity.start(activity!!, mHome!!, invoice)
        } else {
            showError(getString(R.string.error_something_went_wrong))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_account_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        with(listHistory) {
            isNestedScrollingEnabled = false
            adapter = mAdapter
            layoutManager = linearLayoutManager
        }

        scrollView.setOnScrollChangeListener(object : EndlessNestedScrollViewListener(linearLayoutManager) {
                override fun onLoadMore(currentPage: Int) {
                    mPresenter.fetch(currentPage)
                }
            })

        mPresenter.init(mHome?.unitId ?: "")
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT_INVOICE")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun bindData(invoices: List<Invoice>) {
        listHistory.show()
        mAdapter.addData(invoices)
    }

    override fun showNoData() {
        scrollView.hide()
        textNoData.show()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

package com.appy.android.code.domain.model.command.outlet

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.ControlsApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
internal class GetOutletsInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<ControlsApiResult, GetOutletsForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetOutletsForm): Observable<ControlsApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getOutlets( params.headers, params.homeId, params.params)
    }
}
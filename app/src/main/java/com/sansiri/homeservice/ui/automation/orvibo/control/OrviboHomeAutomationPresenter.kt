package com.sansiri.homeservice.ui.automation.orvibo.control
//
//import androidx.lifecycle.MutableLiveData
//import com.homemate.sdk.model.HMControl
//import com.homemate.sdk.model.HMDevice
//import com.homemate.sdk.model.HMHub
//import com.homemate.sdk.model.HMScene
//import com.sansiri.homeservice.R
//import com.sansiri.homeservice.data.repository.orvibo.OrviboRepository
//import com.sansiri.homeservice.model.automation.HomeAutomation
//import com.sansiri.homeservice.model.automation.OrviboHomeAutomation
//import com.sansiri.homeservice.model.menu.OrviboHomeAutomationRoomMenu
//import com.sansiri.homeservice.ui.base.BasePresenterImpl
//
///**
// * Created by sansiri on 11/7/17.
// */
//class OrviboHomeAutomationPresenter(var orviboRepository: OrviboRepository) : BasePresenterImpl<OrviboHomeAutomationContract.View>(), OrviboHomeAutomationContract.Presenter {
//    override var devicesLiveData = MutableLiveData<List<HMControl>>()
//
//    private var mRoomId: String? = null
//    private var mHub: HMHub? = null
//
//    override fun start() {}
//
//    override fun destroy() {}
//
//    override fun fetchData(roomId: String?) {
//        if (roomId != null) {
//            mRoomId = roomId
//            mView?.showLoading()
//            orviboRepository.getHubInfo({ hubs ->
//                mView?.hideLoading()
//
//                if (!hubs.isNullOrEmpty()) {
//                    mHub = hubs[0]
//                    if (roomId == OrviboHomeAutomationRoomMenu.SCENE_ID) {
//                        devicesLiveData.postValue(orviboRepository.getScenes())
//                    } else {
//                        devicesLiveData.postValue(orviboRepository.getDevices(mHub!!.id, roomId))
//                    }
//                }
//            }) {}
//        }
//    }
//
//    override fun controlDevice(homeAutomation: OrviboHomeAutomation, command: Any) {
//        if (mHub?.id != null && homeAutomation.orviboDevice is HMDevice) {
//            when {
//                homeAutomation.isOnOffSwitch() -> {
//                    if (command is Int && command == 0) {
//                        orviboRepository.turnOffSwitch(mHub?.id!!, homeAutomation.orviboDevice.id
//                                ?: "") {
//                            mView?.showMessage("Success")
//                        }
//                    } else {
//                        orviboRepository.turnOnSwitch(mHub?.id!!, homeAutomation.orviboDevice.id
//                                ?: "") {
//                            mView?.showMessage("Success")
//                        }
//                    }
//                }
//                homeAutomation.isToggleSwitch() -> {
//                    orviboRepository.toggleSwitch(mHub?.id!!, homeAutomation.orviboDevice.id
//                            ?: "") {
//                        mView?.showMessage("Success")
//                    }
//                }
//                homeAutomation.isDimmer() -> {
//                    if (command is Int) {
//                        if (command == 0) {
//                            orviboRepository.turnOffSwitch(mHub?.id!!, homeAutomation.orviboDevice.id
//                                    ?: "") {
//                                mView?.showMessage("Success")
//                            }
//                        } else {
//                            orviboRepository.dimmingLight(mHub?.id!!, homeAutomation.orviboDevice.id
//                                    ?: "", command) {
//                                mView?.showMessage("Success")
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    override fun controlScene(homeAutomation: OrviboHomeAutomation) {
//        homeAutomation.isLoading = true
//        mView?.notifyItemChange(homeAutomation)
//        if (mHub?.id != null && homeAutomation.orviboDevice is HMScene) {
//            orviboRepository.activateScene(mHub?.id!!, homeAutomation.orviboDevice.id ?: "") {
//                homeAutomation.isLoading = false
//                mView?.notifyItemChange(homeAutomation)
//                mView?.showMessage("Success")
//            }
//        }
//    }
//
//
//    override fun onDeviceUpdated(devices: List<HMControl>) {
//        val convertedDevice = devices.map { device ->
//            if (device is HMDevice) {
//                OrviboHomeAutomation(
//                        device.name ?: "",
//                        getLocalDeviceIcon(device.type),
//                        getDisplayDeviceType(device.type),
//                        device
//                )
//            } else if (device is HMScene) {
//                OrviboHomeAutomation(
//                        device.name ?: "",
//                        R.drawable.ic_book,
//                        HomeAutomation.Type.SWITCH,
//                        device
//                )
//            } else null
//        }.filterNotNull().filter { it.isAvailable() }
//
//        mView?.bindData(convertedDevice)
//    }
//
//    private fun getDisplayDeviceType(type: Int): HomeAutomation.Type = when (type) {
//        HMDevice.DeviceType.AC -> HomeAutomation.Type.AC
//        HMDevice.DeviceType.DIMMER -> HomeAutomation.Type.SEEK_BAR
//        else -> HomeAutomation.Type.TOGGLE
//    }
//
//    private fun getLocalDeviceIcon(type: Int) = when (type) {
//        HMDevice.DeviceType.AC -> R.drawable.ic_air_condition
//        HMDevice.DeviceType.CAMERA -> R.drawable.ic_camera
//        HMDevice.DeviceType.DIMMER -> R.drawable.ic_bulb
//        HMDevice.DeviceType.LAMP -> R.drawable.ic_lamp
//        HMDevice.DeviceType.CURTAIN, HMDevice.DeviceType.CURTAIN_PERCENT -> R.drawable.ic_curtain
//        HMDevice.DeviceType.REMOTE -> R.drawable.ic_remote_control
//        HMDevice.DeviceType.CONTACT_RELAY, HMDevice.DeviceType.SWITCH_RELAY, HMDevice.DeviceType.SINGLE_FIRE_SWITCH -> R.drawable.ic_bulb
//        else -> R.drawable.ic_bulb
//    }
//
//
//}


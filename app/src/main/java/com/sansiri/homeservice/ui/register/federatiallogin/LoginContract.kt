package com.sansiri.homeservice.ui.register.federatiallogin

import com.facebook.AccessToken
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/17/17.
 */
interface LoginContract {
    interface Presenter : BasePresenter<View> {
        fun facebookLogin(accessToken: AccessToken)
        fun googleLogin(id: String, email: String, token: String)
        fun validateAccount()
    }

    interface View : BaseView {
        fun launchNext()
        fun showLoading()
        fun hideLoading()
        fun launchMainPage()
        fun signOut()
        fun showMissingUser()
    }
}
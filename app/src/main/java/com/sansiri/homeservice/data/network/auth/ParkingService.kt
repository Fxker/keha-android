package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface ParkingService {
    @GET("me/units/{unitId}/partners/teohong/parking")
    fun getSmartParkingStatus(@Path("unitId") unitId: String): Deferred<Response<SmartParkingStatus>>
}
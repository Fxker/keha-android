package com.sansiri.homeservice.ui.myhome

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.MyHome
import com.sansiri.homeservice.model.api.MyHomeDocument
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.pdf.PdfActivity
import com.sansiri.homeservice.util.*

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.activity_my_home.*
import kotlinx.android.synthetic.main.view_project_list.*

class MyHomeActivity : BaseActivity<MyHomeContract.View, MyHomeContract.Presenter>(), MyHomeContract.View {


    override var mPresenter: MyHomeContract.Presenter = MyHomePresenter()

    companion object {
        const val HOME = "HOME"
        const val TITLE = "TITLE"
        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, MyHomeActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val mAdapter = MyHomeDocumentAdapter() {document ->
        sendEvent("MY_HOME", "VIEW", "DOCUMENT")
        PdfActivity.start(this, document.detail?.url ?: "", document.detail?.title ?: "")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_home)

        val home = intent.getParcelableExtra<Hut>(HOME)
        val title = intent.getStringExtra(TITLE)

        setBackToolbar(title ?: "My Home")

        listDocument.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@MyHomeActivity, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)
            setHasFixedSize(true)
            adapter = mAdapter
        }

        layout.foreground = null
        layout.isClickable = false

        if (home != null) {
            mPresenter.init(home)
            mPresenter.start()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_HOME")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindBasicInfo(home: Hut) {
        Glide.with(this).loadCache(home.image, imageProject)
        textAddress.text = home.address
        textProject.text = home.title
        if (home.lat != 0.0 && home.lng != 0.0) {
            buttonGoogleMap.show()
            buttonGoogleMap.setOnClickListener {
                mPresenter.requestToGoogleMap()
            }
        } else {
            buttonGoogleMap.hide()
        }
    }

    override fun bindAdvanceInfo(myHome: MyHome) {
        myHome.streetAddress?.showTextOrHideIfNull(textStreetAddress, titleStreetAddress)
        myHome.unitCode?.showTextOrHideIfNull(textUnitCode, titleUnitCode)
        myHome.unitTypeDescription?.showTextOrHideIfNull(textUnitType, titleUnitType)
        myHome.unitType?.showTextOrHideIfNull(textUnitTypeCode, titleUnitTypeCode)
        myHome.landArea?.toCurrencyFormat()?.showTextOrHideIfNull(textUsageArea, titleUsageArea)
    }

    override fun bindDocument(documents: List<MyHomeDocument>) {
        titleDocument.show()
        listDocument.show()
        mAdapter.setData(documents)
    }

    override fun launchGoogleMap(lat: Double, lng: Double) {
        sendEvent("MY_HOME", "CLICK", "GOOGLE_MAP")

        val url = "https://www.google.co.th/maps?q=loc:$lat,$lng"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alertError(message)
    }

}

package com.appy.android.sdk.room

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APImage
import com.appy.android.sdk.control.APControl
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */

class APRoom : Parcelable {

  @SerializedName("id")
  var id: String? = null

  @SerializedName("title")
  var title: String? = null

  @SerializedName("alternate_title")
  var titleAlternate: String? = null

  @SerializedName("icon")
  var icon: APImage? = APImage()

  @SerializedName("background")
  var background: APImage? = APImage()

  @SerializedName("controls")
  var controls: ArrayList<APControl>? = ArrayList()

  constructor()

  constructor(source: Parcel) : this() {
    id = source.readString()
    title = source.readString()
    titleAlternate = source.readString()
    icon = source.readParcelable(APImage::class.java.classLoader)
    background = source.readParcelable(APImage::class.java.classLoader)
    controls = source.createTypedArrayList(APControl)
  }

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) {
    dest.writeString(id)
    dest.writeString(title)
    dest.writeString(titleAlternate)
    dest.writeParcelable(icon, flags)
    dest.writeParcelable(background, flags)
    dest.writeTypedList(controls)
  }

  companion object CREATOR : Parcelable.Creator<APRoom> {
    override fun createFromParcel(parcel: Parcel): APRoom {
      return APRoom(parcel)
    }

    override fun newArray(size: Int): Array<APRoom?> {
      return arrayOfNulls(size)
    }
  }
}
package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable

data class Vendor(val contact: Contact? = null) : Parcelable {


    constructor(source: Parcel) : this(
            source.readParcelable<Contact>(Contact::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(contact, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Vendor> = object : Parcelable.Creator<Vendor> {
            override fun createFromParcel(source: Parcel): Vendor = Vendor(source)
            override fun newArray(size: Int): Array<Vendor?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.adapter


import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.request.target.Target
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.*
import com.sansiri.homeservice.util.*

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_icon_and_title.view.*
import kotlinx.android.synthetic.main.view_menu_card.view.*
import com.bumptech.glide.Glide
import android.graphics.Bitmap
import android.view.Gravity
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.transition.ChangeBounds
import androidx.transition.Slide
import androidx.transition.TransitionManager


/**
 * Created by sansiri on 12/12/17.
 */
class IconMenuViewHolder(val overlayColor: Int, val backgroundColor: Int, val accentColor: Int, val view: View, val itemClick: (Menu) -> Unit) : RecyclerView.ViewHolder(view) {
    fun bind(menu: Menu) =
            with(view) {
                layoutRoot.setBackgroundColor(backgroundColor)

                if (menu is DynamicMenu) {
                    val widthRatio = if (menu.columnWidth > 3) 3f else if (menu.columnWidth > 0) menu.columnWidth else 1f
                    view.gridSize(widthRatio, 1f)
                    imageCover.gridSize(widthRatio, 1f)
                    layoutContent.gridSizeWidth(0.9f)

                    when (menu.getButtonStyle()) {
                        DynamicMenu.Style.ICON_TEXT -> renderTitleButton(menu)
                        DynamicMenu.Style.FULL_IMAGE -> renderFullImageButton(menu)
                        else -> renderIconButton(menu)
                    }

                    if (menu.badge > 0) {
                        badge.tint(accentColor)
                        badge.show()
                    } else {
                        badge.hide()
                    }

                } else if (menu is ToggleMenu) {
                    if (menu.title.isNullOrEmpty()) {
                        renderIconButton(menu)
                    } else {
                        renderTitleButton(menu)
                    }

                    if (overlayColor != 0) {
                        imageLogo.tintImage(if (menu.isChecked) overlayColor else ContextCompat.getColor(context, R.color.overlay))
                        textTitle.setTextColor(if (menu.isChecked) overlayColor else ContextCompat.getColor(context, R.color.overlay))
                    }
                } else if (menu is LocalIconMenu) {
                    with(view) {
                        menuTitle.show()
                        imageFullLogo.hide()

                        if (menu.iconRes != null) {
                            imageLogo.setImageResource(menu.iconRes)
                        }

                        if (overlayColor != 0) {
                            imageLogo.tintImage(overlayColor)
                        }
                        textTitle.text = menu.title
                    }
                } else {
                    if (menu.title.isNullOrEmpty()) {
                        renderIconButton(menu)
                    } else {
                        renderTitleButton(menu)
                    }
                }

                view.findViewById<View>(R.id.layoutRoot)?.setOnClickListener {
                    if (menu is DynamicMenu) {
                        // always clear badge when button is clicked
                        menu.badge = 0
                        badge.hide()
                    }
                    itemClick(menu)
                }


            }

    private fun renderFullImageButton(menu: Menu) {
        with(view) {
            layoutContent.hide()
            imageCover.show()

            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    imageCover.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
                    imageCover.invalidate()
                }
            })

            menu.icon?.let {
                Glide.with(context).load(it).override(Target.SIZE_ORIGINAL).into(imageCover)
            }
        }
    }

    private fun renderIconButton(menu: Menu) {
        with(view) {
            imageFullLogo.show()
            menuTitle.hide()

            if (!menu.icon.isNullOrEmpty()) {
                Glide.with(context).loadCache(menu.icon!!, imageFullLogo)
            } else {
                imageFullLogo.setImageResource(menu.getLocalIcon())
            }
        }
    }

    private fun renderTitleButton(menu: Menu) {
        with(view) {
            menuTitle.show()
            imageFullLogo.hide()

            if (!menu.icon.isNullOrEmpty()) {
                Glide.with(context).loadCache(menu.icon!!, imageLogo)
            } else {
                imageLogo.setImageResource(menu.getLocalIcon())
            }

            if (overlayColor != 0) {
                imageLogo.tintImage(overlayColor)
            }
            textTitle.text = menu.title
            textTitle.setTextColor(overlayColor)
        }
    }
}
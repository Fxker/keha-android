package com.sansiri.homeservice.ui.profile

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by sansiri on 11/3/17.
 */
class ProfilePresenter : BasePresenterImpl<ProfileContract.View>(), ProfileContract.Presenter {
    private val compositeDisposable = CompositeDisposable()

    override fun start() {
        fetchData()
    }

    override fun destroy() {
        compositeDisposable.clear()
    }

    override fun fetchData() {
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                val disposable = it.getMe()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            mView?.bindData(it)
                        }, {

                        })

                compositeDisposable.add(disposable)
            } else {
            }
        }
    }
}
package com.sansiri.homeservice.model.api.maintenance

data class MaintenanceDetail(
        val html: String? = null
)

package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.google.gson.Gson
import com.google.gson.JsonObject

class SendFibaroRemoteControlCommandForm(
    val remoteControl: APRemoteControl,
    val remoteAction: APActionRemoteControl,
    val parameter: String?) : SendFibaroCommandForm(remoteControl) {

    init {
        action = remoteAction.localCommand.orEmpty()

        parameter?.let { parameter ->
            remoteAction.parameters?.let {
                if (it.containsKey(parameter)) {
                    body = Gson().fromJson(it.get(parameter), JsonObject::class.java)
                }
            }
        }
    }

    override fun getControlId(): String? {
        return if (remoteAction.deviceId != null) {
            remoteAction.deviceId
        } else {
            when (action) {
                "setThermostatSetpoint" -> {
                    remoteControl.localIds.get("com.fibaro.setPoint")
                }
                "setFanMode" -> {
                    remoteControl.localIds.get("com.fibaro.fanMode")
                }
                else -> {
                    remoteControl.localIds.get("com.fibaro.operatingMode")
                }
            }
        }
    }
}
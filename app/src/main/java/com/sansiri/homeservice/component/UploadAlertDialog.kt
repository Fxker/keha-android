package com.sansiri.homeservice.component

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AlertDialog
import android.widget.ProgressBar
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show

class UploadAlertDialog(activity: Activity) : AlertDialog.Builder(activity, R.style.Base_Theme_AppCompat_Dialog) {
    private var mSuccessAnimation: LottieAnimationView? = null
    private var mProgressBar: ProgressBar? = null
    private var mTextUpload: TextView? = null
    private var mDialog: AlertDialog? = null

    init {
        val view = activity.layoutInflater.inflate(R.layout.dialog_upload, null)
        mProgressBar = view.findViewById(R.id.progressBar)
        mSuccessAnimation = view.findViewById(R.id.success)
        mTextUpload = view.findViewById(R.id.textName)
        setView(view)
        mDialog = create()
    }

    fun showUploading(message: String = "กำลังโหลด...") {
        mTextUpload?.text = message
        mProgressBar?.show()
        mDialog?.show()
    }

    fun showDone(message: String = "สำเร็จ") {
        if (mDialog != null && !mDialog!!.isShowing) {
            mDialog?.show()
        }
        mProgressBar?.hide()
        mTextUpload?.text = message
        mSuccessAnimation?.show()
        mSuccessAnimation?.playAnimation()
    }

    fun hide() {
        mDialog?.hide()
    }
}
package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface PaymentGateway2C2PTransactionResultContract {
    interface Presenter : BasePresenter<View> {
        var transactionDetail: MutableLiveData<Resource<Transaction2C2PDetail>>?

        fun init(transactionId: String?, billingEmail: String?)
        fun saveCard(pan: String?, cardToken: String, method: String?)
    }

    interface View : BaseView {
        fun showError(messageRes: Int)
        fun showLoading()
        fun hideLoading()
        fun bindData(transactionDetail: Transaction2C2PDetail)
    }
}
package com.sansiri.homeservice.ui.facilitybooking.detail

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import android.view.LayoutInflater
import android.widget.TextView
import com.sansiri.homeservice.util.reportTime
import com.sansiri.homeservice.util.toDate


class TimeSpinnerAdapter(context: Context, val res: Int, val objects: List<TimesItem>) : ArrayAdapter<TimesItem>(context, res, objects) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(res, parent, false)

        view?.findViewById<TextView>(android.R.id.text1)?.text = getTimeAt(position)

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)
        view?.findViewById<TextView>(android.R.id.text1)?.text = getTimeAt(position)
        return view
    }

    private fun getTimeAt(position: Int): String = objects[position].endTime?.toDate("yyyy-MM-dd HH:mm:ss Z")?.reportTime() ?: ""
}
package com.sansiri.homeservice.model

/**
 * Created by sansiri on 12/18/17.
 */
data class SuggestionRequest(
        val message: String,
        val images: List<String>
)
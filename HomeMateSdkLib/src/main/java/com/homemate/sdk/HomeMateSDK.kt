package com.homemate.sdk

import android.app.Application
import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.homemate.sdk.model.*
import com.orvibo.homemate.api.*
import com.orvibo.homemate.bo.Device
import com.orvibo.homemate.bo.DeviceStatus
import com.orvibo.homemate.bo.Family
import com.orvibo.homemate.bo.Scene
import com.orvibo.homemate.data.*
import com.orvibo.homemate.event.BaseEvent
import com.orvibo.homemate.model.SceneControl
import com.orvibo.homemate.model.family.FamilyManager
import com.orvibo.homemate.model.gateway.QueryHubOnlineStatus
import com.orvibo.homemate.sharedPreferences.UserCache
import com.orvibo.homemate.util.AppTool
import com.orvibo.homemate.util.MyLogger
import com.orvibo.homemate.util.SocketUtil
import java.math.BigInteger
import java.security.MessageDigest

class HomeMateSDK(val context: Context) {

    companion object {

        fun init(appName: String, application: Application) {
            if (isMainProcess(application)) {
                OrviboApi.initHomeMateSDK(application)
                UserApi.initSource(appName, IDC.DEFAULT)
            }
        }

        private fun isMainProcess(application: Application): Boolean {
            val processName = AppTool.getProcessName()
            return !TextUtils.isEmpty(processName) && processName == application.packageName
        }

    }

    fun login(username: String, password: String, onSuccess: () -> Unit, onError: (String) -> Unit) {
        UserApi.setDebugMode(true, true)
        UserApi.login(username, password) { baseEvent ->
            MyLogger.jLog().d(baseEvent.uid)

            if (baseEvent.isSuccess) {
                onSuccess()
                UserCache.saveMd5Password(context, username, password.md5())
            } else {
                onError("An error occurred: ${when (baseEvent.result) {
                    12 -> "The username or password is incorrect"
                    else -> "${baseEvent.result}"
                }}")
            }
            // fetchData()
//            val userId = UserCache.getUserId(application, "rawipol@sansiri.com")
//
//            FamilyApi.queryFamilys(userId) { p0, p1 ->
//                Log.d("", "")
//            }
//            val familyIds = FamilyManager.getCurrentFamilyUids()
//            val id = FamilyManager.getCurrentFamilyId()
//            val scenes = LocalDataApi.getAllScenes(id)
//
//            val orderedList = mutableListOf<Scene>()
//
//            orderedList.addAll(scenes)

        }
    }

    fun getCurrentFamilyId() = FamilyManager.getCurrentFamilyId()

    fun getFamilies(onSuccess: (List<HMFamily>) -> Unit) {
        val userId = UserCache.getCurrentUserId(context)
        FamilyApi.queryFamilys(userId) { _, families ->
            if (families is List<*>) {
                onSuccess(families.map { obj ->
                    if (obj is Family) {
                        return@map HMFamily(
                                obj.familyId,
                                obj.pic,
                                obj.creator,
                                obj.familyName,
                                obj.nickname_in_family
                        )
                    }
                    return@map null
                }.filterNotNull())
            }
        }
    }

    fun getCurrentUsername() = UserCache.getCurrentUserName(context)

    fun getCurrentPasswordMD5() = UserCache.getMd5Password(context, getCurrentUsername())

    fun switchFamily(familyId: String, onSuccess: () -> Unit) {
        val username = getCurrentUsername()
        val password = getCurrentPasswordMD5()

        if (username != null && password != null) {
            FamilyApi.switchFamilyByMd5Password(username, password, familyId) {
                onSuccess()
            }
        }
    }

    fun isLoggedIn(): Boolean = UserCache.getCurrentUserId(context) != null

    fun logOut() {
        UserApi.logout(getCurrentUsername())
    }

    fun getRooms() = LocalDataApi.getAllRooms(getCurrentFamilyId()).map { room ->
        return@map HMRoom(
                room.roomId,
                room.roomName,
                room.floorId,
                room.roomType,
                room.imgUrl
        )
    }

    fun getScenes(): List<HMScene>? {
        val currentFamilyId = getCurrentFamilyId()

        return if (currentFamilyId != null) {
            LocalDataApi.getAllScenes(currentFamilyId).map { scene ->
                HMScene(
                        scene.sceneNo,
                        scene.sceneName
                )
            }

        } else null
    }

    fun getDevices(roomId: String): List<HMDevice>? {
        val currentFamilyId = getCurrentFamilyId()
        return if (currentFamilyId != null)
            LocalDataApi.getDevicesByRoom(currentFamilyId, roomId).map { device ->
                return@map HMDevice(
                        device.deviceId,
                        device.deviceName,
                        device.deviceType
                )
            }
        else null
    }

    fun getHubInfo(onSuccess: (List<HMHub>?) -> Unit, onError: () -> Unit) {
        val queryHub = object : QueryHubOnlineStatus() {
            override fun onQueryHubOnlineStatusResult(result: Int, hubOnlineStatuses: MutableMap<String, Int>?) {
                Log.d("", "")
                onSuccess(
                        hubOnlineStatuses?.toList()?.map {
                            HMHub(it.first, it.second)
                        }
                )
            }

            override fun onMainThreadFailResult(p0: BaseEvent?) {
                super.onMainThreadFailResult(p0)
                onError()
            }
        }
        queryHub.queryHubOnlineStatue(getCurrentFamilyId())
    }

    fun getDevicesStatus(hubId: String): List<HMStatus> {
        return LocalDataApi.getDeviceStatus(hubId).map { status ->
            HMStatus(
                    status.deviceId,
                    status.statusId,
                    status.isOnline,
                    status.value1,
                    status.value2,
                    status.value3,
                    status.value4
            )
        }
    }

    fun turnOnSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit) = DeviceControlApi.deviceOpen(hubId, deviceId, 0) { onSuccess() }

    fun turnOffSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit) = DeviceControlApi.deviceClose(hubId, deviceId, 0) { onSuccess() }

    fun toggleSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit) = DeviceControlApi.toggle(hubId, deviceId, 0) { onSuccess() }

    fun dimmingLight(hubId: String, deviceId: String, progress: Int, onSuccess: () -> Unit) = DeviceControlApi.dimmingLight(hubId, deviceId, progress, false) { onSuccess() }

    fun activateScene(hubId: String, sceneId: String, onSuccess: () -> Unit) {
        SmartSceneApi.controlScene(hubId, sceneId) { onSuccess() }
    }

    private fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }
}
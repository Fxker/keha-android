package com.sansiri.homeservice.ui.notification

import androidx.recyclerview.widget.DiffUtil
import com.sansiri.homeservice.model.api.notification.PushNotification

class NotificationDiffCallback(val oldList: List<PushNotification>, val newList: List<PushNotification>):  DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition]._id == newList.get(newItemPosition)._id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val isReadOld = oldList[oldPosition].isRead
        val isReadNew = newList[newPosition].isRead

        return isReadOld == isReadNew
    }

}
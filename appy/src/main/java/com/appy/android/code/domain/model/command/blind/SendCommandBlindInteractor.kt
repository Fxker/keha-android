package com.appy.android.code.domain.model.command.blind

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandBlindInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendCommandBlindForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendCommandBlindForm): Observable<CommandApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .sendCommandBlind(params.headers, params.homeId, params.blindId, params.mSendCommandBlindJsonBody)
    }
}
package com.sansiri.homeservice.model.api.homecare

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeCareRequest(
		val contactName: String? = null,
		val phoneNo: String? = null,
		val email: String? = null,
		val items: List<HomeCareRequestItem>? = null
) : Parcelable {

	@Parcelize
	data class HomeCareRequestItem(
			val typeId: Int,
			val detail: String? = null,
			val attachments: List<String>? = null
	) : Parcelable

}

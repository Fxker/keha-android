package com.sansiri.homeservice.component.creditcard

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.*
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.TextWatcherExtend

class CreditCardView : FrameLayout {
    interface Listener {
        fun onNameUpdated(name: String)
        fun onCardNumberUpdated(number: String)
        fun onExpiryMonthUpdated(expiryMonth: Int)
        fun onExpiryYearUpdated(expiryYear: Int)
        fun onCVVUpdated(cvv: String)
        fun onEmailUpdated(email: String)
        fun onRememberChecked(checked: Boolean)
    }

    private var mCheckBox: CheckBox? = null
    private var mEditCVV: EditText? = null
    private var mEditCardNumber: EditText? = null
    private var mEditName: EditText? = null
    private var mEditEmail: EditText? = null
    private var mSpinnerExpiryMonth: Spinner? = null
    private var mSpinnerExpiryYear: Spinner? = null

    private val mExpiryMonthAdapter = ExpiryMonthSpinnerAdapter()
    private val mExpiryYearAdapter = ExpiryYearSpinnerAdapter()

    var listener: Listener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    fun init() {
        val view = View.inflate(context, R.layout.view_credit_card, null)

        mEditName = view.findViewById<EditText>(R.id.editName)
        mEditCardNumber = view.findViewById<EditText>(R.id.editCreditCardNumber)
        mEditCVV = view.findViewById<EditText>(R.id.editCVV)
        mSpinnerExpiryMonth = view.findViewById<Spinner>(R.id.spinnerExpiryMonth)
        mSpinnerExpiryYear = view.findViewById<Spinner>(R.id.spinnerExpiryYear)
        mCheckBox = view.findViewById<CheckBox>(R.id.checkbox)
        mEditEmail = view.findViewById<EditText>(R.id.editEmail)

        mSpinnerExpiryMonth?.adapter = mExpiryMonthAdapter
        mSpinnerExpiryYear?.adapter = mExpiryYearAdapter

        mEditName?.addTextChangedListener(TextWatcherExtend {
            listener?.onNameUpdated(it)
        })
        mEditCardNumber?.addTextChangedListener(TextWatcherExtend {
            listener?.onCardNumberUpdated(it)
        })

        mEditCVV?.addTextChangedListener(TextWatcherExtend {
            listener?.onCVVUpdated(it)
        })

        mEditEmail?.addTextChangedListener(TextWatcherExtend {
            listener?.onEmailUpdated(it)
        })

        mSpinnerExpiryMonth?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val month = mSpinnerExpiryMonth?.selectedItem as Int
                listener?.onExpiryMonthUpdated(month)
            }
        }

        mSpinnerExpiryYear?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val year = mSpinnerExpiryYear?.selectedItem as Int
                listener?.onExpiryYearUpdated(year)
            }
        }

        mCheckBox?.setOnCheckedChangeListener { _, isChecked ->
            listener?.onRememberChecked(isChecked)
        }

        addView(view)
    }

    fun setDetail(name: String? = null, cardNumber: String? = null, cvv: String? = null, month: Int? = null, year: Int? = null) {
        mEditName?.setText(name)
        mEditCardNumber?.setText(cardNumber)
        mEditCVV?.setText(cvv)

        for (i in 0 until mExpiryMonthAdapter.count) {
            if (month != null && mExpiryMonthAdapter.getItemId(i) == month.toLong()) {
                mSpinnerExpiryMonth?.setSelection(i)
            }
        }

        for (i in 0 until mExpiryYearAdapter.count) {
            if (year != null && mExpiryYearAdapter.getItemId(i) == year.toLong()) {
                mSpinnerExpiryYear?.setSelection(i)
            }
        }
    }
}
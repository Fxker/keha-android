package com.appy.android.code.domain.model.command.ac

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.ac.AirConditionerCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandAirConditionerForm(
        val homeId: String,
        val acId: String,
        val command: AirConditionerCommand
) : AppyForm() {
    val mAirConditionerCommandJsonBody: AirConditionerCommandJsonBody = AirConditionerCommandJsonBody()

    init {
        this.mAirConditionerCommandJsonBody.command = command.action()
    }

}
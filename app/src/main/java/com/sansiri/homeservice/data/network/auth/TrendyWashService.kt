package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.partner.trendywash.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface TrendyWashService {
    @POST("me/units/{unitId}/partners/trendyWash/users/register")
    fun registerTrendyWashAccount(@Path("unitId") unitId: String, @Body request: TrendyWashRegisterCustomer): Deferred<Response<TrendyWashRegisterCustomer>>

    @POST("me/units/{unitId}/partners/trendyWash/users/login")
    fun loginTrendyWash(@Path("unitId") unitId: String, @Body request: TrendyWashLogin): Deferred<Response<TrendyWashRegisterCustomer>>

    @GET("me/units/{unitId}/partners/trendyWash/users/login/facebook")
    fun facebookLoginTrendyWash(@Path("unitId") unitId: String): Deferred<Response<String>>

    @GET("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/machines")
    fun getTrendyWashMachines(@Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String): Deferred<Response<List<TrendyWashMachine>>>

    @GET("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/credit")
    fun getTrendyWashCredit(@Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String): Deferred<Response<TrendyWashCredit>>

    @POST("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/topup/qrpromptpay")
    fun checkoutTrendyWashPaymentQR(@Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Body request: TrendyWashPaymentGatewayRequest): Deferred<Response<TrendyWashPaymentGateWayQR>>

    @POST("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/topup/creditcard")
    fun checkoutTrendyWashCreditCard(@Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Body request: TrendyWashPaymentGatewayRequest): Deferred<Response<TrendyWashPaymentGateWayQR>>

    @PATCH("me/units/{unitId}/partners/trendyWash/users/{trendyWashUserId}/machines/{machineNo}/start")
    fun startTrendyWashMachine(@Path("unitId") unitId: String, @Path("trendyWashUserId") trendyWashUserId: String, @Path("machineNo") machineNo: String, @Body request: TrendyWashMachineStart): Deferred<Response<TrendyWashMachineStart>>
}
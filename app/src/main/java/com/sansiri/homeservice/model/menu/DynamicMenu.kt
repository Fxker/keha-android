package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.Feature
import kotlinx.android.parcel.Parcelize

/**
 * Created by oakraw on 10/3/2017 AD.
 */
@Parcelize
data class DynamicMenu(
        @SerializedName("name")
        override val id: String?,

        @SerializedName("displayName")
        override val title: String? = null,

        @SerializedName("iconUrl")
        override val icon: String? = null,

        @SerializedName("badge")
        var badge: Int = 0,

        @SerializedName("url")
        var url: String? = null,

        @SerializedName("androidUri")
        val androidUri: String? = null,

        @SerializedName("androidPackageName")
        val androidPackageName: String? = null,

        @SerializedName("type")
        var type: String? = null,

        @SerializedName("items")
        val items: List<Section>? = null,

        @SerializedName("isDisableOverTitleOnButton")
        val isDisableOverTitleOnButton: Boolean = false,

        @SerializedName("columnWidth")
        val columnWidth: Float = 1f,

        @SerializedName("style")
        val style: Int = 0,

        @SerializedName("html")
        var data: Data? = null
) : Menu, Parcelable {
    enum class Style {
        FULL_IMAGE,
        ICON,
        ICON_TEXT
    }

    fun getButtonStyle(): Style {
        val isFullImage = style.and(2) > 0
        val hasText = style.and(1) > 0

        return when {
            hasText && !isFullImage -> Style.ICON_TEXT
            !hasText && isFullImage -> Style.FULL_IMAGE
            else -> Style.ICON
        }
    }

    @Parcelize
    data class Data(
            var iboxMobileNo: String? = null
    ) : Parcelable
}

package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.daikin.DaikinCommand
import com.appy.android.sdk.control.daikin.DaikinTemperatureCommand

internal class SendCommandDaikinTemperatureForm(
        val homeId: String,
        val daikinId: String,
        val commandValue: DaikinTemperatureCommand
) : AppyForm() {
    val mSendCommandDaikinCommandValueJsonBody: SendCommandDaikinCommandValueJsonBody = SendCommandDaikinCommandValueJsonBody()

    init {
        this.mSendCommandDaikinCommandValueJsonBody.command = DaikinCommand.TEMP.action()
        this.mSendCommandDaikinCommandValueJsonBody.commandValue = commandValue.action()
    }
}
package com.sansiri.homeservice.ui.electric.mea.login

import android.util.Log
import com.sansiri.homeservice.data.repository.mea.MeaRepository
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign

class MeaElectricLoginPresenter(val repository: MeaRepository) : BasePresenterImpl<MeaElectricLoginContract.View>(), MeaElectricLoginContract.Presenter {
    private var mHome: Hut? = null
    private var mMEAInfo: MEAInfo? = null

    override fun init(home: Hut, meaInfo: MEAInfo?) {
        this.mHome = home
        this.mMEAInfo = meaInfo
    }

    override fun start() {
    }

    override fun destroy() {
    }

    override fun login(ca: String) {
        mView?.showLoading()
        if (mHome != null) {
            repository.registerCA(ca, mHome!!.projectId, mMEAInfo, {
                saveCA(ca)
            }) {
                mView?.hideLoading()
                mView?.showError(it.toString())
            }
        }
    }

    override fun saveCA(ca: String) {
        if (mHome != null) {
            mCompositeDisposable += repository.saveCA(mHome!!.unitId, ca).observe().subscribe({
                mView?.launchHome(it.contractAccountId ?: ca)
                mView?.hideLoading()
            }) {
                mView?.hideLoading()
                mView?.showError(it.showHttpError())
            }
        }
    }

    override fun process(code: String?) {
        val ca = if (code != null && code.length > 9) repository.getCAFromQRCode(code) else code
        mView?.fillCA(ca)
    }
}
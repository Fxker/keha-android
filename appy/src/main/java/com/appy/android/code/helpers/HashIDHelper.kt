package com.appy.android.code.helpers

import org.hashids.Hashids


/**
 * Created by ssa-dev-04 on 15/1/2561.
 */
internal class HashIDHelper {
    companion object {

        @JvmField
        val SALTS = "@ppysphere_dev"

        @JvmStatic
        fun encode(any: Any) {

        }

        @JvmStatic
        fun decode(hash: String): LongArray {
            return Hashids(SALTS, 8).decode(hash)
        }
    }
}
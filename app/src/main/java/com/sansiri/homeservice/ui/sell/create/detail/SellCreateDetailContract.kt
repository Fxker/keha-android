package com.sansiri.homeservice.ui.sell.create.detail

import com.sansiri.homeservice.model.SellOption
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/7/17.
 */
interface SellCreateDetailContract {
    interface Presenter : BasePresenter<View> {
        fun validate(vararg sellOption: SellOption)
    }

    interface View : BaseView {
        fun launchNext()
    }
}
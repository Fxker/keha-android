package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 11/27/17.
 */
data class ListDataResponse<T> (
    val data: List<T>
)
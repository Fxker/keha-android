package com.sansiri.homeservice.ui.base

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.gms.analytics.Tracker
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.data.network.AnalyticProvider

/**
 * Created by oakraw on 6/12/2017 AD.
 */
abstract class BaseV2Fragment<in V : BaseView, T : BasePresenter<V>>: androidx.fragment.app.Fragment(), BaseView {
    protected abstract val mPresenter: T
    protected var mTracker: Tracker? = null
    private var analyticProvider: AnalyticProvider? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attachView(this as V)
        analyticProvider = AnalyticProvider(context!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    override fun sendScreenView(screenName: String) {
        analyticProvider?.sendScreenView(screenName)
    }

    override fun sendEvent(category: String, action: String, label: String?, logVersion: String, vararg optionalProp: Pair<String, String>) {
        analyticProvider?.sendEvent(category, action, label, logVersion, optionalProp.toList())
    }

}
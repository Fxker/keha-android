package com.sansiri.homeservice.ui.bos.detail

import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface BosDetailContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun submit()
        fun uploadImage(images: List<Pair<String, ByteArray>>)
        fun createRequest(imagePath: List<String>)
        fun init(unitId: String?, groupType: Int, jobType: BosMenu?)
    }

    interface View : BaseView {
        fun prepareBitmapToUploadIfExist()
        fun showUploadProgress(progress: Int, max: Int)
        fun getDetail(): String
        fun getName(): String
        fun getContact(): String
        fun hideUploadProgress()
        fun showSubmitProgress()
        fun tryAgain()
        fun showSuccess()
        fun showAlertToEnterDetail()
        fun bindUser(profile: Profile?)
        fun getArea(): String?
        fun getFloor(): String?
        fun getBuilding(): String?
    }
}
package com.sansiri.homeservice.ui.downpayment

import android.util.Log
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

/**
 * Created by sansiri on 11/14/17.
 */
class DownpaymentPresenter : BasePresenterImpl<DownpaymentContract.View>(), DownpaymentContract.Presenter {
    private val compositeDisposable = CompositeDisposable()
    private var mHome: Hut? = null

    override fun start() {
    }

    override fun fetchData(home: Hut) {
        mHome = home
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                // mView?.showLoading()
                val disposable = it.getDownpayment(home.unitId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .map { downpayments ->
                            downpayments.items?.sortBy { it.date }!!
                            return@map downpayments
                        }
                        .subscribe({
                            Log.d("DownpaymentPresenter", it.toString())
                            mView?.showNetPrice(it.netPrice)
                            mView?.showPaidAmount(it.paidAmount)
                            mView?.showRemainingAmount(it.remainAmount)
                            it.items?.let { classifyDownpaymentType(it) }
                            //mView?.bindList()
//                            mView?.hideLoading()
//                            mView?.setData(it)
                        }, {
                            mView?.showError(it.showHttpError())
                        })

                compositeDisposable.add(disposable)
            } else {
            }
        }
    }

    private fun classifyDownpaymentType(downpayments: List<DownpaymentItem>) {
        val today = Calendar.getInstance().time
        var upcomingIndex = 0

        doAsync {
            run breaker@ {
                downpayments.forEachIndexed { index, downpaymentItem ->
                    if (downpaymentItem.date != null && !downpaymentItem.isPaid && !downpaymentItem.isOverdue) {
                        if (downpaymentItem.date!!.after(today)) {
                            upcomingIndex = index
                            return@breaker
                        }
                    }
                }
            }
            uiThread {
                downpayments.forEachIndexed { index, downpaymentItem ->
                    when {
                        downpaymentItem.isPaid -> mView?.addPaidList(downpaymentItem)
                        downpaymentItem.isOverdue -> {
                            mView?.addOverdue(downpaymentItem)
                        }
                        index == upcomingIndex -> {
                            mView?.addUpComing(downpaymentItem)
                        }
                        else -> {
                            mView?.addFutureList(downpaymentItem)
                        }
                    }
                }
            }
        }
    }

    override fun requestEPayment(downPaymentItem: DownpaymentItem) {
        if (mHome != null) {
            mView?.launchDownpayment(mHome!!, downPaymentItem)
        }
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}
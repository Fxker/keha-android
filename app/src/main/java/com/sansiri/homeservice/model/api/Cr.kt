package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

data class Cr(
        @SerializedName("crTransfer")
        val crTransfer: Cr.Transfer? = null
) {
    data class Transfer(

            @SerializedName("imageUri")
            val imageUri: String? = null,

            @SerializedName("name")
            val name: String? = null,

            @SerializedName("telephone")
            val telephone: String? = null,

            @SerializedName("email")
            val email: String? = null
    )
}
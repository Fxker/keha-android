package vc.siriventures.bathome.service

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*
import vc.siriventures.bathome.model.GatewayInfoRequest
import vc.siriventures.bathome.model.GatewayInfoResponse

interface AuthorizeService {
    @POST("BatHomeWebService.svc/authenticate")
    fun authorization(@Body request: GatewayInfoRequest): Observable<GatewayInfoResponse>

}
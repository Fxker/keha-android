package com.sansiri.homeservice.data.network

import android.content.Context
import com.facebook.FacebookSdk
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

class OkHttpBuilder(val context: Context) {
    private val cache = Cache(FacebookSdk.getCacheDir(), 10 * 1024 * 1024)
    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    private val authInterceptor = FirebaseUserIdTokenInterceptor()
    private val acceptLanguageInterceptor = AcceptLanguageInterceptor(context)

    private val baseConfig = OkHttpClient.Builder()
            .cache(cache)
            .connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .readTimeout(5, TimeUnit.MINUTES)

    fun buildUnAuth(): OkHttpClient = baseConfig
            .addInterceptor(acceptLanguageInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(AppInterceptor())
            .build()

    fun buildAuth(): OkHttpClient = baseConfig
            .addInterceptor(authInterceptor)
            .addInterceptor(acceptLanguageInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(AppInterceptor())
            .build()


}
package com.sansiri.homeservice.ui.myaccount

import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Invoice
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class MyAccountViewHolder(val view: View, val itemClick: () -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: Invoice, color: Int) {
        with(view) {
            imageIcon.setImageResource(R.drawable.ic_home_wallet)
            textTitle.setTextColor(context.resources.getColor(color))
            textTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_display1))

            textSubtitle.text = data._invoiceDue?.reportDMYFull()
            textDate.hide()
            textTitle.text = data.invoiceAmount.toCurrencyFormat()
            textType.text = data.loanDescription

            root.background = null
        }
    }
}
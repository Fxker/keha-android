package com.sansiri.homeservice.ui.electric.mea.consent

import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.data.network.RuntimeCache.isMeaConsentAccepted
import com.sansiri.homeservice.data.repository.mea.MeaRepository
import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign
import retrofit2.HttpException

class MeaElectricConsentPresenter(val repository: MeaRepository) : BasePresenterImpl<MeaElectricConsentContract.View>(), MeaElectricConsentContract.Presenter {
    var mConsent: Consent? = null

    override fun init(consent: Consent?) {
        this.mConsent = consent
    }

    override fun start() {
        mConsent?.let { mView?.bindData(it) }
    }

    fun fetch() {

    }

    override fun destroy() {
    }

    override fun accept() {
        mView?.showLoading()
        mCompositeDisposable += repository.acceptConsent().observe().subscribe({
            mView?.hideLoading()
            mView?.launchNext()
            isMeaConsentAccepted = true
        }) {
            mView?.hideLoading()
            if (it is HttpException && it.code() in 200..399) {
                mView?.launchNext()
            } else {
                mView?.showError(it.showHttpError())
            }
        }
    }
}
package com.appy.android.code.base.domain.executor

import android.content.Context
import android.os.Handler

/**
 * Created by gregoire barret on 5/13/15.
 * For Perfumist project.
 */
open class BaseUiThreadHandler(var context: Context) : UiThreadHandler {

    override fun getMainHandler(): Handler {
        return Handler(context.mainLooper);
    }

    override fun post(r: Runnable) {
        getMainHandler().post(r)
    }
}

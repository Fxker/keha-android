package com.appy.android.code.base.domain.executor

import android.os.Handler

/**
 * Created by gregoire barret on 5/13/15.
 * For Perfumist project.
 */
interface UiThreadHandler {

    fun getMainHandler(): Handler

    fun post(r: Runnable)
}

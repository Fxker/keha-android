package com.sansiri.homeservice.ui.homecare.history

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/11/17.
 */
class HomeCareHistoryPresenter : BasePresenterImpl<HomeCareHistoryContract.View>(), HomeCareHistoryContract.Presenter {
    var unitId: String = ""

    override fun init(unitId: String) {
        this.unitId = unitId
    }

    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData(currentPage: Int) {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(
                        api.getHomeCareHistory(unitId, 10, currentPage * 10).observe().subscribe({ listResponse ->
                            mView?.hideLoading()
                            if (listResponse.count > 0) {
                                listResponse.items.forEach { it.sortSubItemAttachments() }
                                mView?.addData(listResponse)
                            } else {
                                mView?.showNoData()
                            }
                            mView?.showExpiredDate(listResponse._expiredAt)
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }
}
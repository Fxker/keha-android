package com.sansiri.homeservice.ui.register.emaillogin

import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by sansiri on 10/17/17.
 */
class EmailPresenter : BasePresenterImpl<EmailContract.View>(), EmailContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun validateAndSignIn(username: String, password: String) {
        var isPass = true
        if (username.isBlank()) {
            isPass = false
            mView?.usernameError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
        }

        if (password.isBlank()) {
            isPass = false
            mView?.passwordError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
        }

        if (isPass) {
            signIn(username, password)
        }
    }

    override fun signIn(email: String, password: String) {
        mView?.showLoading()
        FirebaseAuthManager.signIn(email, password, {
            mView?.hideLoading()
            validateAccount()
        }) {
            mView?.hideLoading()
            mView?.showError(email, it.localizedMessage)
        }
    }

    private fun validateAccount() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                it.getMe().observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            mView?.hideLoading()
                            if (it.objectId != null) {
                                mView?.nextScreen()
                            } else {
                                mView?.showError("This account doesn't exist")
                                mView?.signOut()
                            }
                        }, {
                            mView?.hideLoading()
                            mView?.showError(it.localizedMessage)
                            mView?.signOut()
                        })
            } else {
                mView?.hideLoading()
                mView?.showError("Something went wrong")
                mView?.signOut()
            }
        }
    }
}
package com.sansiri.homeservice.data.database.orvibo

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import io.reactivex.Flowable

@Dao
interface OrviboDao {
    @Query("SELECT * FROM orvibo_home WHERE unitId = :unitId")
    fun getOrviboHome(unitId: String): LiveData<OrviboHome>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrviboHome(orviboHome: OrviboHome)

    @Query("UPDATE orvibo_home SET familyId = :familyId WHERE unitId = :unitId")
    suspend fun updateFamilyId(unitId: String, familyId: String)

    @Query("DELETE FROM orvibo_home WHERE unitId = :unitId")
    suspend fun deleteOrviboHome(unitId: String)

    @Query("DELETE FROM orvibo_home")
    fun deleteAll()
}
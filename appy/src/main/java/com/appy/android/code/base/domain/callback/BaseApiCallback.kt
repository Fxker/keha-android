package com.appy.android.code.base.domain.callback

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
interface BaseApiCallback {
    fun onFailed(e: Throwable)
}
package com.sansiri.homeservice.ui.gallery

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.listeners.IPickResult
import permissions.dispatcher.*
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog



@RuntimePermissions
class GalleryActivity : AppCompatActivity(), IPickResult {
    override fun onPickResult(p0: PickResult?) {
    }

    companion object {
        val CODE = 99
        val URI_LIST = "URI_LIST"
        val MAX_SELECTABLE = "MAX_SELECTABLE"

        fun startForResult(activity: Activity, maxSelectable: Int = 1) {
            val intent = Intent(activity, GalleryActivity::class.java)
            intent.putExtra(MAX_SELECTABLE, maxSelectable)
            activity.startActivityForResult(intent, CODE)
        }

        fun startForResult(fragment: androidx.fragment.app.Fragment, maxSelectable: Int = 1) {
            val intent = Intent(fragment.activity, GalleryActivity::class.java)
            intent.putExtra(MAX_SELECTABLE, maxSelectable)
            fragment.startActivityForResult(intent, CODE)
        }
    }

    private var mMaxSelectable: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMaxSelectable = intent.getIntExtra(MAX_SELECTABLE, 1)
        showGalleryWithPermissionCheck()
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showGallery() {
        PickImageDialog.build(PickSetup()).show(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            val intent = Intent()
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else {
            setResult(resultCode, data)
            finish()
        }
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onPermissionDenied() {
        finish()
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgain() {
        finish()
    }
}

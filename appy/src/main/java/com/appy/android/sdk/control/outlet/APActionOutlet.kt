package com.appy.android.sdk.control.outlet

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionOutlet : APAction<OutletCommand>, Parcelable {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionOutlet> {
        override fun createFromParcel(parcel: Parcel): APActionOutlet {
            return APActionOutlet(parcel)
        }

        override fun newArray(size: Int): Array<APActionOutlet?> {
            return arrayOfNulls(size)
        }
    }
}
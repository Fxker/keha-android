package com.appy.android.sdk.control.blind

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionBlind : APAction<BlindCommand>, Parcelable {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionBlind> {
        override fun createFromParcel(parcel: Parcel): APActionBlind {
            return APActionBlind(parcel)
        }

        override fun newArray(size: Int): Array<APActionBlind?> {
            return arrayOfNulls(size)
        }
    }
}
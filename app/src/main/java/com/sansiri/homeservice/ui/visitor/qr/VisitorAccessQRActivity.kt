package com.sansiri.homeservice.ui.visitor.qr

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.ui.base.PermissionActivity
import com.sansiri.homeservice.ui.base.requestPermissionReadWriteWithPermissionCheck
import com.sansiri.homeservice.util.SaveUtil
import com.sansiri.homeservice.util.alertError

import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.shareBitmap
import kotlinx.android.synthetic.main.activity_visitor_access_qr.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.sansiri.homeservice.model.Hut
import java.util.*


class VisitorAccessQRActivity : PermissionActivity<VisitorAccessQRContract.View, VisitorAccessQRContract.Presenter>(), VisitorAccessQRContract.View {
    override var mPresenter: VisitorAccessQRContract.Presenter = VisitorAccessQRPresenter()
    private var mQRBitmap: Bitmap? = null
    private var mHome: Hut? = null

    companion object {
        private const val CODE = "CODE"
        private const val HOME = "HOME"
        fun start(activity: Activity, home: Hut?, code: String) {
            val intent = Intent(activity, VisitorAccessQRActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(CODE, code)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visitor_access_qr)

        setBackToolbar(getString(R.string.visitor_qr))

        val code = intent.getStringExtra(CODE)
        mHome = intent.getParcelableExtra(HOME)

        mPresenter.init(code)
        mPresenter.start()

        buttonShare.setOnClickListener {
            share()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("VISITOR_ACCESS_QR")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_download) {
            save()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_download, menu)
        return true
    }

    override fun generateQRBitmap(code: String) {
        getLogoBitmap { bitmapLogo ->

            doAsync {
                val hints = EnumMap<EncodeHintType, Any>(EncodeHintType::class.java).apply {
                    put(EncodeHintType.MARGIN, 2)
                    put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H)
                }
                val bitmapQR = BarcodeEncoder().encodeBitmap(code, BarcodeFormat.QR_CODE, 800, 800, hints)
                val finalBitmap = if (bitmapLogo != null) drawLogoOnQR(bitmapQR, bitmapLogo) else bitmapQR
                uiThread {
                    mQRBitmap = finalBitmap
                    imageQR.setImageBitmap(finalBitmap)
                }
            }
        }

    }

    private fun getLogoBitmap(onSucces: (Bitmap?) -> Unit) {
        Glide.with(this@VisitorAccessQRActivity).asBitmap().load(mHome?.icon).into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                val res = resource
                onSucces(resource)
            }

            override fun onLoadFailed(errorDrawable: Drawable?) {
                super.onLoadFailed(errorDrawable)
                onSucces(null)
            }
        })

    }

    private fun drawLogoOnQR(bitmapQR: Bitmap, bitmapLogo: Bitmap): Bitmap {
        val finalBitmap = Bitmap.createBitmap(
                bitmapQR.width,
                bitmapQR.height,
                Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(finalBitmap)

        val logoWidth = bitmapQR.width / 4
        val logoHeight = (bitmapLogo.height * bitmapQR.width / bitmapLogo.width) / 4

        val scaledBitmapLogo = Bitmap.createScaledBitmap(bitmapLogo, logoWidth, logoHeight, false)

        val logoX = (bitmapQR.width / 2f) - (scaledBitmapLogo.width / 2f)
        val logoY = (bitmapQR.height / 2f) - (scaledBitmapLogo.height / 2f)

        canvas.drawBitmap(bitmapQR, null, RectF(0f, 0f, bitmapQR.width.toFloat(), bitmapQR.height.toFloat()), null)
        canvas.translate(logoX, logoY)
        canvas.drawBitmap(scaledBitmapLogo, null, RectF(0f, 0f, scaledBitmapLogo.width.toFloat(), scaledBitmapLogo.height.toFloat()), null)

        return finalBitmap
    }

    private fun save() {
        sendEvent("VISITOR_ACCESS", "SAVE", "QR")
        if (mQRBitmap != null) {
            callback = object : PermissionActivity.Callback {
                override fun onPermissionAllowed(permission: String) {
                    if (permission == android.Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                        // sendEvent("EPAYMENT", "SAVE", "QR")
                        SaveUtil().saveImage(this@VisitorAccessQRActivity, mQRBitmap!!)
                    }
                }
            }
            requestPermissionReadWriteWithPermissionCheck()
        } else {
            showError("Please try to reopen app again.")
        }
    }

    private fun share() {
        sendEvent("VISITOR_ACCESS", "SHARE", "QR")
        if (mQRBitmap != null) {
            callback = object : PermissionActivity.Callback {
                override fun onPermissionAllowed(permission: String) {
                    if (permission == android.Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                        shareBitmap(mQRBitmap!!)
                    }
                }
            }
            requestPermissionReadWriteWithPermissionCheck()
        } else {
            showError("Please try to reopen app again.")
        }
    }

    override fun showError(message: String) {
        alertError(message)
    }

}

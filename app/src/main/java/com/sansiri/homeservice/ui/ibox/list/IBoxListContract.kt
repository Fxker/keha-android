package com.sansiri.homeservice.ui.ibox.list

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.IBoxItem
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface IBoxListContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String)
    }

    interface View : BaseView {
        fun bindData(mutableListOf: List<IBoxItem>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
    }
}
package com.sansiri.homeservice.data.repository.trendywash

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.*

interface TrendyWashRepository {
    fun loginTrendyWash(unitId: String, request: TrendyWashLogin): MutableLiveData<Resource<TrendyWashRegisterCustomer>>
    fun registerTrendyWashAccount(unitId: String, request: TrendyWashRegisterCustomer): MutableLiveData<Resource<TrendyWashRegisterCustomer>>
    fun getTrendyWashMachines(unitId: String, userId: String): MutableLiveData<Resource<List<TrendyWashMachine>>>
    fun getTrendyWashCredit(unitId: String, userId: String): MutableLiveData<Resource<TrendyWashCredit>>
    fun checkoutTrendyWashPaymentQR(unitId: String, trendyWashUserId: String, request: TrendyWashPaymentGatewayRequest): MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>
    fun checkoutTrendyWashCreditCard(amount: Int, unitId: String, trendyWashUserId: String, cardNumber: String, cardName: String, expirationMonth: Int, expirationYear: Int, securityCode: String): MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>
    fun startTrendyWashMachine(unitId: String, trendyWashUserId: String, machineId: String, request: TrendyWashMachineStart): MutableLiveData<Resource<TrendyWashMachineStart>>
    fun getFacebookLoginTrendyWashLink(unitId: String): String
}
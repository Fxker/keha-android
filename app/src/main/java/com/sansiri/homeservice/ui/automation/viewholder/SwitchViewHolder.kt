package com.sansiri.homeservice.ui.automation.viewholder

import android.graphics.PorterDuff
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.view_home_automation_switch.view.*
import kotlinx.android.synthetic.main.view_icon_and_title.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class SwitchViewHolder(val view: View, val itemClick: (HomeAutomation, Any) -> Unit) : LifecycleViewHolder(view) {

    fun bind(data: HomeAutomation) {
        with(view) {
            val title = data.title.toLowerCase()
            when {
                title.contains("ac") -> imageLogo.setImageResource(R.drawable.ic_air_condition)
                title.contains("cinema") -> imageLogo.setImageResource(R.drawable.ic_cinema)
                title.contains("sleep") -> imageLogo.setImageResource(R.drawable.ic_bedroom)
                title.contains("dining") -> imageLogo.setImageResource(R.drawable.ic_dining_room
                )
                else -> imageLogo.setImageResource(data.icon)

            }

            when (data) {
                is AppyHomeAutomation -> {
                    data.apControl?.icon?.let {
                        Glide.with(this).loadCache(it, imageLogo)
                    }
                }
            }

            imageLogo.setColorFilter(context.resources.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
            textTitle.text = data.title

            panelClickable.setOnClickListener {
                itemClick(data, 0)
            }
        }
    }
}
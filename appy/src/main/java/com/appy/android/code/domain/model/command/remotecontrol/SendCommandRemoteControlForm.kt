package com.appy.android.code.domain.model.command.remotecontrol

import com.appy.android.code.data.rest.form.AppyForm

internal class SendCommandRemoteControlForm(
        val homeId: String,
        val remoteControlID: String,
        val commandParams: String
) : AppyForm() {
    val mSendCommandRemoteControlJsonBody: SendCommandRemoteControlJsonBody = SendCommandRemoteControlJsonBody()

    init {
        this.mSendCommandRemoteControlJsonBody.command = "SEND_REMOTE_COMMAND"
        this.mSendCommandRemoteControlJsonBody.commandParams = commandParams
    }
}
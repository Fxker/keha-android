package com.appy.android.code.base.domain.executor

import android.content.Context

import javax.inject.Inject

/**
 * Created by gregoire barret on 5/13/15.
 * For Perfumist project.
 */
internal class TheadHandlerImpl @Inject
constructor(context: Context) : BaseUiThreadHandler(context)

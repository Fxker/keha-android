package com.appy.android.code.domain.model.status.blind

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.status.blind.StatusBlindApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-04 on 19/1/2561.
 */
internal class GetStatusBlindInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<StatusBlindApiResult, GetStatusBlindForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetStatusBlindForm): Observable<StatusBlindApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getStatusBlind(params.headers, params.homeId, params.blindId, params.params)
    }
}
package com.appy.android.code.domain.model.command.outlet

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.outlet.OutletCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandOutletForm(
        val homeId: String,
        val outletId: String,
        val command: OutletCommand
) : AppyForm() {
    val mSendCommandOutletJsonBody: SendCommandOutletJsonBody = SendCommandOutletJsonBody()

    init {
        this.mSendCommandOutletJsonBody.command = command.action()
    }

}
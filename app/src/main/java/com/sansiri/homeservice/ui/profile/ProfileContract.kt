package com.sansiri.homeservice.ui.profile

import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/3/17.
 */
interface ProfileContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
    }

    interface View : BaseView {
        fun bindData(profile: Profile)
    }
}
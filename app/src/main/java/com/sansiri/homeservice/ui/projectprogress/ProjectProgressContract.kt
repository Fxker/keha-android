package com.sansiri.homeservice.ui.projectprogress

import com.sansiri.homeservice.model.api.ProjectProgress
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/6/17.
 */
interface ProjectProgressContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(projectId: String)
    }

    interface View : BaseView {
        fun bindData(projectProgress: ProjectProgress)
        fun showLoading()
        fun hideLoading()
        fun hideData()
        fun showComingSoon()
    }
}
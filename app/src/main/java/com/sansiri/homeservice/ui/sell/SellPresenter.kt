package com.sansiri.homeservice.ui.sell

import com.sansiri.homeservice.model.Sell
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import java.util.*

/**
 * Created by sansiri on 11/7/17.
 */
class SellPresenter : BasePresenterImpl<SellContract.View>(), SellContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData() {
        mView?.bindData(arrayListOf<Sell>(
                Sell("98/1", "98,000,000 บาท", "ฝากขาย", Calendar.getInstance())
        ))
    }
}
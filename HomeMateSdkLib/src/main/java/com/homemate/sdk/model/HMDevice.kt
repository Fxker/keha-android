package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable

data class HMDevice(
        override var id: String? = null,
        override var name: String? = null,
        var type: Int
) : HMControl, Parcelable {
    var status: HMStatus? = null

    class DeviceType {
        companion object {
            val DIMMER = 0
            val LAMP = 1
            val OUTLET = 2
            val SCREEN = 3
            val WINDOW_SHADES = 4
            val AC = 5
            val TV = 6
            val SPEAKER_BOX = 7
            val CURTAIN = 8
            val CONTACT_RELAY = 9
            val SWITCH_RELAY = 10
            val IR_REPEATER = 11
            val WIRELESS = 12
            val SCENE_MODE = 13
            val CAMERA = 14
            val SCENE_KEYPAD = 15
            val REMOTE = 16
            val REPEATER = 17
            val LUMINANCE_SENSOR = 18
            val RGB = 19
            val VIDEO_INTERCOM = 20
            val LOCK = 21
            val TEMPERATURE_SENSOR = 22
            val HUMIDITY_SENSOR = 23
            val AIR_PURITY_SENSOR = 24
            val FLAMMABLE_GAS = 25
            val INFRARED_SENSOR = 26
            val SMOKE_SENSOR = 27
            val PANALARM = 28
            val S20 = 29
            val ALLONE = 30
            val KEPLER = 31
            val STB = 32
            val SELF_DEFINE_IR = 33
            val CURTAIN_PERCENT = 34
            val ROLLER_SHADES_PERCENT = 35
            val AC_PANEL = 36
            val PUSH_WINDOW = 37
            val COLOR_TEMPERATURE_LAMP = 38
            val ROLLING_GATE = 39
            val ROLLER_SHUTTERS = 42
            val COCO = 43
            val VICENTER = 44
            val MINIHUB = 45
            val MAGNETIC = 46
            val MAGNETIC_WINDOW = 47
            val MAGNETIC_DRAWER = 48
            val MAGNETIC_OTHER = 49
            val FIVE_KEY_SCENE_KEYPAD = 50
            val SEVEN_KEY_SCENE_KEYPAD = 51
            val CLOTHE_SHORSE = 52
            val WATER_SENSOR = 54
            val CO_SENSOR = 55
            val SOS_SENSOR = 56
            val BACK_MUSIC = 57
            val FAN = 58
            val TV_BOX = 59
            val PROJECTOR = 60
            val DISTRIBUTION_BOX = 64
            val FORMALIN_DETECTOR = 65
            val CO_DETECTOR = 66
            val RF_HUB = 67
            val RF_SWITCH_STATUS = 77
            val RF_SWITCH_NO_STATUS = 78
            val AC_VRV = 81
            val SENSOR_MODULE = 93
            val WATER_PURIFICATION = 101
            val SINGLE_FIRE_SWITCH = 102
            val DISTRIBUTION_BOX_V2 = 104
            val BLE_LOCK = 107
            val XINFENG_PANEL = 108
            val PRECENT_SCREEN = 109
            val PRECENT_PUSH_WINDOW = 110
            val PRECENT_ROLLING_GATE = 111
            val YILIN_FLOOR_HEAT = 112
            val MIXPAD = 114
            val VERTICAL_WINDOW_SHADES = 105
            val SLIDING_PANEL = 106
            val ALARM_HOST = 113
            val ALARM_DEVICE = 115
        }
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
        writeInt(type)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMDevice> = object : Parcelable.Creator<HMDevice> {
            override fun createFromParcel(source: Parcel): HMDevice = HMDevice(source)
            override fun newArray(size: Int): Array<HMDevice?> = arrayOfNulls(size)
        }
    }
}
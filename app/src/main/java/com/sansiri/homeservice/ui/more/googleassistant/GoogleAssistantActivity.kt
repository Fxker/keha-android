package com.sansiri.homeservice.ui.more.googleassistant

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.projectlist.ProjectListActivity
import com.sansiri.homeservice.ui.register.federatiallogin.LoginFragment
import com.sansiri.homeservice.util.*

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.activity_google_assistant.*
import kotlinx.android.synthetic.main.view_project_list.*

class GoogleAssistantActivity : BaseActivity<GoogleAssistantContract.View, GoogleAssistantContract.Presenter>(), GoogleAssistantContract.View {
    override var mPresenter: GoogleAssistantContract.Presenter = GoogleAssistantPresenter()

    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val GOOGLE_SIGN_IN = 123

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, GoogleAssistantActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_assistant)

        setBackToolbar(getString(R.string.connect_to_google_assistant))

//        with(list) {
//            adapter = mElevatorAdapter
//            itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
//            itemAnimator?.addDuration = 400
//        }

        buttonSelectUnit.setOnClickListener {
            mPresenter.requestForSelectUnit()
        }

        buttonGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient?.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
        }

        buttonLogout.setOnClickListener {
            mPresenter.logout()
        }

        layout.setOnClickListener {
            mPresenter.requestForSelectUnit()
        }

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_server_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        mPresenter.fetch()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ProjectListActivity.CODE -> {
                if (resultCode == RESULT_OK) {
                    val homeId = data?.getStringExtra(ProjectListActivity.HOME)
                    mPresenter.selectUnit(homeId ?: "")
                    mPresenter.validateAndSubmit()
//                    homeId?.let { mPresenter.findHome(homeId) }
                }
            }
            GOOGLE_SIGN_IN -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    mPresenter.onGoogleLoggedIn(account?.idToken, account?.email)
                    mPresenter.validateAndSubmit()
                } catch (e: ApiException) {
                    showError(e.message ?: "Something went wrong")
                }
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(home: Home) {
        projectSelected.show()
        buttonSelectUnit.hide()

        textAddress.text = home.address
        textProject.text = home.title
        Glide.with(this).loadCache(home.image, imageProject)
    }

    override fun launchProjectSelector(homes: List<Home>) {
        ProjectListActivity.start(this, ArrayList(homes.map { it.toHut() }))
    }

    override fun showGoogleLoginButton() {
        buttonGoogle.show()
        layoutLoggedIn.hide()
    }

    override fun showGoogleLoggedIn(email: String?) {
        buttonGoogle.hide()
        layoutLoggedIn.show()
        textEmail.text = email
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showUnregister() {
        showGoogleLoginButton()
        projectSelected.hide()
        buttonSelectUnit.show()
    }

    override fun showSyncing() {
        progressBarSyncing.show()
        textSyncing.text = getString(R.string.syncing)
        layoutSyncing.show()
    }

    override fun hideSyncing() {
        layoutSyncing.hide()
    }

    override fun showSuccess() {
        layoutSyncing.show()
        progressBarSyncing.hide()
        textSyncing.text = getString(R.string.success)

        ({
            hideSyncing()
        }).withDelay(3000)
    }

    override fun showError(message: String) {
        snackError(message)
    }
}

package com.sansiri.homeservice.di

import com.sansiri.homeservice.ui.automation.appy.HomeAutomationActivity
import com.sansiri.homeservice.ui.automation.appy.HomeAutomationContract
import com.sansiri.homeservice.ui.automation.appy.HomeAutomationPresenter
import com.sansiri.homeservice.ui.automation.bathome.control.BAtHomeControlActivity
import com.sansiri.homeservice.ui.automation.bathome.control.BAtHomeControlContract
import com.sansiri.homeservice.ui.automation.bathome.control.BAtHomeControlPresenter
import com.sansiri.homeservice.ui.automation.bathome.widget.BAtHomeWidgetContract
import com.sansiri.homeservice.ui.automation.bathome.widget.BAtHomeWidgetFragment
import com.sansiri.homeservice.ui.automation.bathome.widget.BAtHomeWidgetPresenter
//import com.sansiri.homeservice.ui.automation.orvibo.control.OrviboHomeAutomationContract
//import com.sansiri.homeservice.ui.automation.orvibo.control.OrviboHomeAutomationPresenter
//import com.sansiri.homeservice.ui.automation.orvibo.home.OrviboHomeContract
//import com.sansiri.homeservice.ui.automation.orvibo.home.OrviboHomePresenter
import com.sansiri.homeservice.ui.automation.orvibo.login.OrviboLoginContract
import com.sansiri.homeservice.ui.automation.orvibo.login.OrviboLoginPresenter
import com.sansiri.homeservice.ui.bos.detail.BosDetailContract
import com.sansiri.homeservice.ui.bos.detail.BosDetailFragment
import com.sansiri.homeservice.ui.bos.detail.BosDetailPresenter
import com.sansiri.homeservice.ui.bos.type.BosTypeContract
import com.sansiri.homeservice.ui.bos.type.BosTypeFragment
import com.sansiri.homeservice.ui.bos.type.BosTypePresenter
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentActivity
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentContract
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentPresenter
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainActivity
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainContract
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainPresenter
import com.sansiri.homeservice.ui.electric.mea.consent.MeaElectricConsentContract
import com.sansiri.homeservice.ui.electric.mea.consent.MeaElectricConsentFragment
import com.sansiri.homeservice.ui.electric.mea.consent.MeaElectricConsentPresenter
import com.sansiri.homeservice.ui.electric.mea.home.MeaElectricHomeContract
import com.sansiri.homeservice.ui.electric.mea.home.MeaElectricHomeFragment
import com.sansiri.homeservice.ui.electric.mea.home.MeaElectricHomePresenter
import com.sansiri.homeservice.ui.electric.mea.login.MeaElectricLoginContract
import com.sansiri.homeservice.ui.electric.mea.login.MeaElectricLoginPresenter
import com.sansiri.homeservice.ui.electric.mea.login.MeaElectricLoginFragment
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.PaymentGateway2C2PContract
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.PaymentGateway2C2PFragment
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.PaymentGateway2C2PPresenter
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result.PaymentGateway2C2PTransactionResultContract
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result.PaymentGateway2C2PTransactionResultFragment
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result.PaymentGateway2C2PTransactionResultPresenter
import com.sansiri.homeservice.ui.epayment.gateway.phone.PaymentGatewayPhoneVerifyContract
import com.sansiri.homeservice.ui.epayment.gateway.phone.PaymentGatewayPhoneVerifyFragment
import com.sansiri.homeservice.ui.epayment.gateway.phone.PaymentGatewayPhoneVerifyPresenter
import com.sansiri.homeservice.ui.epayment.options.EPaymentOptionsContract
import com.sansiri.homeservice.ui.epayment.options.EPaymentOptionsFragment
import com.sansiri.homeservice.ui.epayment.options.EPaymentOptionsPresenter
import com.sansiri.homeservice.ui.home.HomeContract
import com.sansiri.homeservice.ui.home.HomePresenter
import com.sansiri.homeservice.ui.meter.electric.SmartElectricMeterActivity
import com.sansiri.homeservice.ui.meter.electric.SmartElectricMeterContract
import com.sansiri.homeservice.ui.meter.electric.SmartElectricMeterPresenter
import com.sansiri.homeservice.ui.meter.electric.detail.SmartElectricMeterDetailActivity
import com.sansiri.homeservice.ui.meter.electric.detail.SmartElectricMeterDetailContract
import com.sansiri.homeservice.ui.meter.electric.detail.SmartElectricMeterDetailPresenter
import com.sansiri.homeservice.ui.meter.water.SmartWaterMeterActivity
import com.sansiri.homeservice.ui.meter.water.SmartWaterMeterContract
import com.sansiri.homeservice.ui.meter.water.SmartWaterMeterPresenter
import com.sansiri.homeservice.ui.meter.water.detail.SmartWaterMeterDetailActivity
import com.sansiri.homeservice.ui.meter.water.detail.SmartWaterMeterDetailContract
import com.sansiri.homeservice.ui.meter.water.detail.SmartWaterMeterDetailPresenter
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceDetailActivity
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceDetailContract
import com.sansiri.homeservice.ui.myaccount_v2.invoice.MyAccountInvoiceDetailPresenter
import com.sansiri.homeservice.ui.notification.NotificationContract
import com.sansiri.homeservice.ui.notification.NotificationFragment
import com.sansiri.homeservice.ui.notification.NotificationPresenter
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingActivity
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingContract
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingPresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.TrendyWashAddMoneyContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.TrendyWashAddMoneyFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.TrendyWashAddMoneyPresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard.CreditCardContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard.CreditCardFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard.CreditCardPresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer.TransferContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer.TransferFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer.TransferPresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.TrendyWashHomeContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.TrendyWashHomeFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.TrendyWashHomePresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine.TrendyWashMachineContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine.TrendyWashMachineFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine.TrendyWashMachinePresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.login.TrendyWashLoginContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.login.TrendyWashLoginFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.login.TrendyWashLoginPresenter
import com.sansiri.homeservice.ui.washingmachine.trendywash.registration.TrendyWashRegisterContract
import com.sansiri.homeservice.ui.washingmachine.trendywash.registration.TrendyWashRegisterFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.registration.TrendyWashRegisterPresenter
import org.koin.core.qualifier.named
import org.koin.dsl.module

val automationModule = module {
    scope(named<HomeAutomationActivity>()) {
        scoped<HomeAutomationContract.Presenter> { HomeAutomationPresenter(appyRepository = get()) }
    }

    factory<OrviboLoginContract.Presenter> { OrviboLoginPresenter(orviboRepository = get()) }
//    factory { OrviboHomePresenter(orviboRepository = get()) as OrviboHomeContract.Presenter }
//    factory { OrviboHomeAutomationPresenter(orviboRepository = get()) as OrviboHomeAutomationContract.Presenter }

    scope(named<BAtHomeWidgetFragment>()) {
        scoped<BAtHomeWidgetContract.Presenter> { BAtHomeWidgetPresenter() }
    }

    scope(named<BAtHomeControlActivity>()) {
        scoped<BAtHomeControlContract.Presenter> { BAtHomeControlPresenter(get()) }
    }
}

val homeModule = module {
    factory { HomePresenter(get(), get()/*, orviboRepository = get()*/) as HomeContract.Presenter }
}

val trendyWashModule = module {

    scope(named<TrendyWashLoginFragment>()) {
        scoped<TrendyWashLoginContract.Presenter> { TrendyWashLoginPresenter(get()) }
    }

    scope(named<TrendyWashHomeFragment>()) {
        scoped<TrendyWashHomeContract.Presenter> { TrendyWashHomePresenter(get()) }
    }

    scope(named<TrendyWashMachineFragment>()) {
        scoped<TrendyWashMachineContract.Presenter> { TrendyWashMachinePresenter(get()) }
    }

    scope(named<TrendyWashRegisterFragment>()) {
        scoped<TrendyWashRegisterContract.Presenter> { TrendyWashRegisterPresenter(get()) }
    }

    scope(named<TrendyWashAddMoneyFragment>()) {
        scoped<TrendyWashAddMoneyContract.Presenter> { TrendyWashAddMoneyPresenter(get()) }
    }

    scope(named<CreditCardFragment>()) {
        scoped<CreditCardContract.Presenter> { CreditCardPresenter(get()) }
    }

    scope(named<TransferFragment>()) {
        scoped<TransferContract.Presenter> { TransferPresenter(get()) }
    }
}


val paymentModule = module {
    scope(named<DownpaymentEPaymentActivity>()) {
        scoped<DownpaymentEPaymentContract.Presenter> { DownpaymentEPaymentPresenter() }
    }

    scope(named<MyAccountInvoiceDetailActivity>()) {
        scoped<MyAccountInvoiceDetailContract.Presenter> { MyAccountInvoiceDetailPresenter() }
    }

    scope(named<PaymentGatewayPhoneVerifyFragment>()) {
        scoped<PaymentGatewayPhoneVerifyContract.Presenter> { PaymentGatewayPhoneVerifyPresenter(get()) }
    }

    scope(named<PaymentGateway2C2PFragment>()) {
        scoped<PaymentGateway2C2PContract.Presenter> { PaymentGateway2C2PPresenter(get()) }
    }

    scope(named<PaymentGateway2C2PTransactionResultFragment>()) {
        scoped<PaymentGateway2C2PTransactionResultContract.Presenter> { PaymentGateway2C2PTransactionResultPresenter(get()) }
    }

    scope(named<PaymentGateway2C2PTransactionResultFragment>()) {
        scoped<PaymentGateway2C2PTransactionResultContract.Presenter> { PaymentGateway2C2PTransactionResultPresenter(get()) }
    }

    scope(named<EPaymentOptionsFragment>()) {
        scoped<EPaymentOptionsContract.Presenter> { EPaymentOptionsPresenter(get()) }
    }
}

val electricModule = module {
    scope(named<MeaElectricConsentFragment>()) {
        scoped<MeaElectricConsentContract.Presenter> { MeaElectricConsentPresenter(get()) }
    }
    scope(named<MeaElectricLoginFragment>()) {
        scoped<MeaElectricLoginContract.Presenter> { MeaElectricLoginPresenter(get()) }
    }
    scope(named<MeaElectricHomeFragment>()) {
        scoped<MeaElectricHomeContract.Presenter> { MeaElectricHomePresenter(get()) }
    }
    scope(named<MEAElectricMainActivity>()) {
        scoped<MEAElectricMainContract.Presenter> { MEAElectricMainPresenter(get()) }
    }
}

val parkingModule = module {
    scope(named<SmartParkingActivity>()) {
        scoped<SmartParkingContract.Presenter> { SmartParkingPresenter(get()) }
    }
}

val meterModule = module {
    scope(named<SmartWaterMeterActivity>()) {
        scoped<SmartWaterMeterContract.Presenter> { SmartWaterMeterPresenter(get()) }
    }
    scope(named<SmartWaterMeterDetailActivity>()) {
        scoped<SmartWaterMeterDetailContract.Presenter> { SmartWaterMeterDetailPresenter(get()) }
    }
    scope(named<SmartElectricMeterActivity>()) {
        scoped<SmartElectricMeterContract.Presenter> { SmartElectricMeterPresenter(get()) }
    }
    scope(named<SmartElectricMeterDetailActivity>()) {
        scoped<SmartElectricMeterDetailContract.Presenter> { SmartElectricMeterDetailPresenter(get()) }
    }
}

val notificationModule = module {
    scope(named<NotificationFragment>()) {
        scoped<NotificationContract.Presenter> { NotificationPresenter(get()) }
    }
}


val bosModule = module {
    scope(named<BosTypeFragment>()) {
        scoped<BosTypeContract.Presenter> { BosTypePresenter(get()) }
    }
    scope(named<BosDetailFragment>()) {
        scoped<BosDetailContract.Presenter> { BosDetailPresenter(get()) }
    }
}

val modules = listOf(
        automationModule,
        homeModule,
        trendyWashModule,
        paymentModule,
        parkingModule,
        meterModule,
        bosModule,
        baseModule,
        electricModule,
        repositoryModule,
        notificationModule,
        networkModule)

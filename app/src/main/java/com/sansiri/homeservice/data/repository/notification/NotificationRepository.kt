package com.sansiri.homeservice.data.repository.notification

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.sansiri.homeservice.model.api.notification.PushNotification

interface NotificationRepository {
    fun getNotifications(): LiveData<List<PushNotification>>

    @WorkerThread
    suspend fun insert(notification: PushNotification)

    @WorkerThread
    suspend fun deleteFeatureNotification(featureId: String, unitId: String)

    @WorkerThread
    suspend fun deleteAll()

    @WorkerThread
    suspend fun markAsRead(notificationId: Int)

    @WorkerThread
    suspend fun markAsRead(featureId: String, unitId: String)
}

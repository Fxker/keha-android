package com.sansiri.homeservice.ui.maintenance

import android.content.Context
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.Store
import net.danlew.android.joda.DateUtils
import org.joda.time.DateTime
import java.util.*
import java.util.concurrent.TimeUnit

fun Date.timeDiffMessage(context: Context): String {
    val today = Calendar.getInstance().time

    var timeDiff = time - today.time
    val isOverdue = timeDiff < 0
    timeDiff = Math.abs(timeDiff)

    val diffDays = TimeUnit.MILLISECONDS.toDays(timeDiff)
    val diffMonths = Math.floor(diffDays / 30.0).toInt()

    var dayUnit = ""
    var monthUnit = ""

    if (Store.getLocale()?.language == "en") {
        dayUnit = context.resources.getQuantityString(R.plurals.days, diffDays.toInt())
        monthUnit = context.resources.getQuantityString(R.plurals.months, diffMonths)
    } else {
        dayUnit = context.getString(R.string.day)
        monthUnit = context.getString(R.string.month)
    }

    val period = if (diffMonths > 0) "$diffMonths $monthUnit" else "$diffDays $dayUnit"

    return if (isOverdue) {
        "${context.getString(R.string.maintenance_overdue)} $period"
    } else {
        String.format(context.getString(R.string.recommended_maintenance_in), period)
    }
}
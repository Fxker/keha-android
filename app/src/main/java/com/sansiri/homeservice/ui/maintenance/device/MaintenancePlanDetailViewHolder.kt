package com.sansiri.homeservice.ui.maintenance.device

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.maintenance.timeDiffMessage
import kotlinx.android.synthetic.main.view_summary_menu_min.view.*


/**
 * Created by sansiri on 10/12/17.
 */
class MaintenancePlanDetailViewHolder(val view: View, val itemClick: (MaintenancePlan) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: MaintenancePlan) {
        with(view) {
            textTitle.text = data.accessoryName
            textSubtitle.setTextColor(context.resources.getColor(R.color.colorSecondary))
            textSubtitle.text = data._planDatetime?.timeDiffMessage(context)

            root.setOnClickListener {
                itemClick(data)
            }
        }
    }
}
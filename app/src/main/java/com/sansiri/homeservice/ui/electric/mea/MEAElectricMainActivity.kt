package com.sansiri.homeservice.ui.electric.mea

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.electric.mea.consent.MeaElectricConsentFragment
import com.sansiri.homeservice.ui.electric.mea.home.MeaElectricHomeFragment
import com.sansiri.homeservice.ui.electric.mea.login.MeaElectricLoginFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_electric_main.*
import kotlinx.android.synthetic.main.appbar_center_title.*
import org.koin.androidx.scope.currentScope

class MEAElectricMainActivity : BaseV2Activity<MEAElectricMainContract.View, MEAElectricMainContract.Presenter>(), MEAElectricMainContract.View {
    override val mPresenter: MEAElectricMainContract.Presenter by currentScope.inject()


    companion object {
        val HOME = "HOME"
        val MENU = "MENU"
        val CUSTOMER_ID = "CUSTOMER_ID"

        fun start(activity: Activity, home: Hut, menu: Menu) {
            val intent = Intent(activity, MEAElectricMainActivity::class.java)
            intent.putExtra(HOME, home)
            if (menu is DynamicMenu)
                intent.putExtra(MENU, menu)
            activity.startActivity(intent)
        }
    }

    private var mHome: Hut? = null
    private var mMenu: DynamicMenu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_electric_main)

        setBackToolbar(findViewById(R.id.toolbar), "")
        textToolbar.text = mMenu?.title ?: ""

        mHome = intent.getParcelableExtra(HOME)
        mMenu = intent.getParcelableExtra(MENU)

        mPresenter.init(mHome)
        mPresenter.start()
    }

    override fun launchConsent(consent: Consent) {
        fragmentContainerWrapper.hide()
        fragmentContainer2.show()
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer2, MeaElectricConsentFragment.newInstance(mMenu?.icon, consent)).commit()
    }

    override fun launchLogin(home: Hut, meaInfo: MEAInfo) {
        fragmentContainerWrapper.show()
        fragmentContainer2.hide()
        addFragment(MeaElectricLoginFragment.newInstance(home, meaInfo, mMenu?.icon), false)
    }

    override fun onConsentAccepted() {
        mPresenter.checkCA()
    }

    override fun launchHome(ca: String) {
        fragmentContainerWrapper.show()
        fragmentContainer2.hide()
        addFragment(MeaElectricHomeFragment.newInstance(ca), false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alertError(message)
    }

}

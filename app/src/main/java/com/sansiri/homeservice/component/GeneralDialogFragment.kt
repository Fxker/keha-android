package com.sansiri.homeservice.component

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.ui.base.BaseView
import kotlinx.android.synthetic.main.dialog_general.*
import java.util.*

/**
 * Created by sansiri on 10/12/17.
 */
abstract class GeneralDialogFragment : androidx.fragment.app.DialogFragment(), BaseView {
//    protected var mTracker: Tracker? = null
    abstract val onClickListener: () -> Unit
    private var analyticProvider: AnalyticProvider? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_general, container)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(0)) // set window background to transparent
        analyticProvider = AnalyticProvider(context!!)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonSubmit.setOnClickListener {
            onClickListener()
        }
    }

    override fun sendScreenView(screenName: String) {
        analyticProvider?.sendScreenView(screenName)
    }

    override fun sendEvent(category: String, action: String, label: String?, logVersion: String, vararg optionalProp: Pair<String, String>) {
        analyticProvider?.sendEvent(category, action, label, logVersion, optionalProp.toList())
    }


    override fun showError(message: String) {
    }
}
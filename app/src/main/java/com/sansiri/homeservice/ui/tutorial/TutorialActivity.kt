package com.sansiri.homeservice.ui.tutorial

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.activity_tutorial.*

open class TutorialActivity : LocalizationActivity() {

    var pagerAdapter: TutorialPagerAdapter? = null

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, TutorialActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        pagerAdapter = TutorialPagerAdapter(supportFragmentManager);
        viewPager.adapter = pagerAdapter

        buttonBack.setOnClickListener {
            onBackPressed()
        }
    }
}

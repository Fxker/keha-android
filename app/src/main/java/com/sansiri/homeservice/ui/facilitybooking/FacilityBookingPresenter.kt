package com.sansiri.homeservice.ui.facilitybooking

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/16/17.
 */
class FacilityBookingPresenter : BasePresenterImpl<FacilityBookingContract.View>(), FacilityBookingContract.Presenter {
    private var projectId: String? = null
    private var unitId: String? = null
    private var address: String? = null
    private var mFacilities: List<FacilityBooking>? = null

    override fun init(projectId: String, unitId: String, address: String) {
        this.projectId = projectId
        this.unitId = unitId
        this.address = address
        fetchData()
    }

    override fun start() {

    }

    override fun destroy() {
    }


    override fun fetchData() {
        // mView?.bindFacility(MockData.getFacilityBooking())
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(
                        api.getFacilityBooking(projectId ?: "").observe().subscribe({ facilities ->
//                            facilities.forEach { item ->
//                                Log.d("FacilityBookingList", "${item.contents?.get(0)?.title} : ${item.contents?.get(0)?.shortDescription}")
//                            }

                            this.mFacilities = facilities
                            mView?.hideLoading()
                            if (facilities.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                mView?.bindFacility(facilities.map {
                                    val title = it.getContent()?.title
                                    return@map GenericMenu(it.id, title, it.preview?.image)
                                })
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            } else {
                mView?.hideLoading()
                mView?.showError("Something went wrong")
            }
        }

    }


    private fun getLocalIcon(title: String): Int {
        val target = title.toLowerCase()
        return if (target.contains("spa")) {
            R.drawable.ic_spa
        } else if (target.contains("library")){
            R.drawable.ic_book
        } else if (target.contains("tea")){
            R.drawable.ic_teapot
        } else if (target.contains("social")){
            R.drawable.ic_lounge
        } else if (target.contains("screening") || target.contains("movie")){
            R.drawable.ic_cinema
        } else if (target.contains("multipurpose")){
            R.drawable.ic_multi_purpose
        } else if (target.contains("ev")){
            R.drawable.ic_ev_charging
        } else {
            R.drawable.ic_home
        }
    }

    override fun requestToLaunchCalendar(title: String, facilityId: String) {
        mFacilities?.let {
            val facility = it.find { it.id == facilityId }
            if (facility != null) {
                mView?.launchCalendar(projectId ?: "", unitId ?: "", address ?: "", facility)
            }
        }

    }
}
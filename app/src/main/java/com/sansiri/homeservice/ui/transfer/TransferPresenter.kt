package com.sansiri.homeservice.ui.transfer

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by sansiri on 11/3/17.
 */
class TransferPresenter : BasePresenterImpl<TransferContract.View>(), TransferContract.Presenter {
    private val compositeDisposable = CompositeDisposable()

    override fun start() {
    }

    override fun fetchData(unitObjectId: String) {
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                mView?.showLoading()

                // Observable.zip(it.getTransferDoc(unitObjectId), it.getTransferCost(unitObjectId), BiFunction { a, b -> })

                val disposable = it.getTransferDoc(unitObjectId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            mView?.hideLoading()
                            mView?.bindData(it)
                        }, {
                            mView?.hideLoading()
                        })

                val disposable2 = it.getTransferCost(unitObjectId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            mView?.addData(it)
                        }, {
                            mView?.hideLoading()
                        })

                compositeDisposable.addAll(disposable, disposable2)
            } else {
            }
        }
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}
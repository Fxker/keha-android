package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom

data class Item(
		val amount: Int? = null,
		val room: HomeUpgradeRoom? = null,
		val hardware: HomeUpgradeHardware? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readValue(Int::class.java.classLoader) as Int?,
			source.readParcelable<HomeUpgradeRoom>(HomeUpgradeRoom::class.java.classLoader),
			source.readParcelable<HomeUpgradeHardware>(HomeUpgradeHardware::class.java.classLoader)
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeValue(amount)
		writeParcelable(room, 0)
		writeParcelable(hardware, 0)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Item> = object : Parcelable.Creator<Item> {
			override fun createFromParcel(source: Parcel): Item = Item(source)
			override fun newArray(size: Int): Array<Item?> = arrayOfNulls(size)
		}
	}
}

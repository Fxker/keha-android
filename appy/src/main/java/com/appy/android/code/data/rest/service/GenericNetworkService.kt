package com.appy.android.code.data.rest.service

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import java.util.*

interface GenericNetworkService {

    @GET
    fun get(@Url url: String, @QueryMap params: HashMap<String, String>): Observable<Response<JsonObject>>

    @POST
    fun post(@Url url: String, @Body body: Any?): Observable<Response<JsonObject>>

    @DELETE
    fun delete(@Url url: String, @Body body: Any?): Observable<Response<JsonObject>>

    @PUT
    fun put(@Url url: String, @Body body: Any?): Observable<Response<JsonObject>>

}
package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p

import androidx.lifecycle.LiveData
import com.ccpp.pgw.sdk.android.enums.APIResponseCode
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.repository.gateway2c2p.Gateway2c2pRepository
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.base.CoroutineScopePresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PaymentGateway2C2PPresenter(val repository: Gateway2c2pRepository) : CoroutineScopePresenter<PaymentGateway2C2PContract.View>(), PaymentGateway2C2PContract.Presenter, Gateway2c2pRepository.Listener {
    override var liveDataSavingCreditCards: LiveData<List<SavingCreditCard>>? = null
    private var mPaymentResponse: PaymentResponse? = null
    private var mCardName: String? = null
    private var mCardNumber: String? = null
    private var mCCV: String? = null
    private var mEmail: String? = null
    private var mExpiryMonth: Int? = null
    private var mExpiryYear: Int? = null
    private var mSavingCard: SavingCreditCard? = null
        set(value) {
            isSavingCard = value != null
            field = value
        }
    private var isRemember: Boolean? = false
    private var isSavingCard: Boolean = false

    override fun init(paymentResponse: PaymentResponse?) {
        mPaymentResponse = paymentResponse
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
    }

    private fun fetch() {
        if (mPaymentResponse?.externalData?.merchantId != null) {
            repository.init(mPaymentResponse?.externalData?.merchantId!!, this)
            liveDataSavingCreditCards = repository.getSavingCreditCards()
        } else {
            mView?.showError(R.string.error_default_message)
        }
    }

    override fun setCardName(name: String) {
        mCardName = name
        validate()
    }

    override fun setCardNumber(number: String) {
        mCardNumber = number
        validate()
    }

    override fun setCCV(ccv: String) {
        mCCV = ccv
        validate()
    }

    override fun setEmail(email: String) {
        mEmail = email
    }

    override fun setExpiryMonth(month: Int) {
        mExpiryMonth = month
        validate()
    }

    override fun setExpiryYear(year: Int) {
        mExpiryYear = year
        validate()
    }

    override fun setRememberCreditCard(checked: Boolean) {
        isRemember = checked
    }

    override fun setNewCard() {
        mView?.showNewCard(mPaymentResponse)
        mSavingCard = null
    }

    override fun setSavingCard(creditCards: List<SavingCreditCard>) {
        if (creditCards.isNotEmpty()) {
            val card = creditCards.first()
            mSavingCard = card
            mView?.showSavingCard(card.securedCardNumber, card.provider, mPaymentResponse)
            if (card.billingEmail != null) {
                mView?.showSavingEmail(card.billingEmail)
            }
        }
    }

    override fun deleteCard() {
        launch {
            repository.deleteCard(mSavingCard?.cardToken ?: "")
            withContext(Dispatchers.Main) {
                mSavingCard = null
            }
        }
        mView?.showNewCard(mPaymentResponse)
    }

    override fun validate() {
        if (!isSavingCard) {
            if (
                    mCardName != null && mCardName!!.isNotEmpty() &&
                    mCardNumber != null && mCardNumber!!.isNotEmpty() &&
                    mCCV != null && mCCV!!.isNotEmpty() && mCCV!!.length == 3 &&
                    mExpiryMonth != null &&
                    mExpiryYear != null
            ) {
                mView?.enableContinueButton()
            } else {
                mView?.disableContinueButton()
            }
        } else {
            if (mCCV != null && mCCV!!.isNotEmpty() && mCCV!!.length == 3) {
                mView?.enableContinueButton()
            } else {
                mView?.disableContinueButton()
            }
        }
    }

    override fun execute() {
        if (isSavingCard) {
            repository.createCreditCard(
                    mSavingCard?.cardToken ?: "",
                    mCCV ?: "",
                    mEmail
            )
        } else {
            repository.createCreditCard(
                    mCardName ?: "",
                    mCardNumber ?: "",
                    mCCV ?: "",
                    mExpiryMonth ?: 0,
                    mExpiryYear ?: 0,
                    isRemember ?: false,
                    mEmail
            )
        }

        if (mPaymentResponse?.externalData?.paymentToken != null) {
            mView?.showLoading()
            repository.execute(mPaymentResponse?.externalData?.paymentToken!!)
        } else {
            mView?.showError(R.string.error_default_message)
        }
    }

    override fun onTransactionSuccess(transacnId: String) {
        mView?.launchTransactionResult(transacnId, mEmail)
    }

    override fun onRequestWeb(url: String) {
        mView?.hideLoading()
        mView?.launchWeb(url)
    }

    override fun onFailure(error: Throwable) {
        mView?.hideLoading()
        mView?.showError(error.localizedMessage)
    }

    override fun handleResponse(response: TransactionResultResponse) {
        if (response.responseCode == APIResponseCode.TRANSACTION_COMPLETED) {
            mView?.launchTransactionResult(response.transactionID, mEmail)
        } else {
            mView?.showError(response.responseDescription)
        }
    }
}
package com.sansiri.homeservice.ui.suggestion

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.auth.ApiAuthRepository
import com.sansiri.homeservice.model.SuggestionRequest
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/11/17.
 */
class SuggestionPresenter : BasePresenterImpl<SuggestionContract.View>(), SuggestionContract.Presenter {
    private var mUnitId: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
    }

    override fun destroy() {
    }

    private val images = mutableListOf<String>()

    override fun submit() {
        val detail = mView?.getDetail()

        if (detail?.isEmpty()!!) {
            mView?.showAlertToEnterDetail()
            return
        }

        mView?.prepareBitmapToUploadIfExist()
    }

    override fun uploadImage(images: List<Pair<String, ByteArray>>) {
        val imagePath = mutableListOf<String>()
        updateProgress(imagePath.size, images.size)
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                images.forEach { image ->
                    mCompositeDisposable.add(
                            api.uploadFile(image.first, image.second).observe().subscribe({ paths ->
                                val path = paths.getOrNull(0)
                                if (path != null) {
                                    imagePath.add(path)
                                    if (updateProgress(imagePath.size, images.size)) {
                                        submitSuggestion(imagePath)
                                    }
                                }
                            }) {
                                imagePath.clear()
                                mView?.tryAgain()
                                mView?.showError(it.showHttpError())
                            }
                    )
                }
            } else {
                mView?.tryAgain()
                mView?.showError("Network Error")
            }
        }
    }

    override fun submitSuggestion(imagePath: List<String>) {
        val detail = mView?.getDetail()

        mView?.showSubmitProgress()

        val request = SuggestionRequest(detail ?: "", imagePath)
        if (mUnitId != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mCompositeDisposable.addAll(
                            api.submitSuggestion(mUnitId!!, request).observe().subscribe({
                                mView?.showSuccess()
                            }) {
                                mView?.tryAgain()
                                mView?.showError(it.showHttpError())
                            }
                    )
                } else {
                    mView?.tryAgain()
                    mView?.showError("Network Error")
                }
            }
        }
    }

    private fun updateProgress(progress: Int, max: Int): Boolean {
        mView?.showUploadProgress(progress, max)
        if (progress == max) {
            return true
        }
        return false
    }
}

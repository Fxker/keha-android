package com.sansiri.homeservice.util

import android.content.Context
import com.sansiri.homeservice.R
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by sansiri on 12/21/17.
 */
object TimeShow {
    var mContext: Context? = null

    fun init(context: Context) {
        this.mContext = context
    }

    fun show(dateInPast: String): String {
        try {
            val past = DateTime(dateInPast).millis
            val now = DateTime.now(DateTimeZone.UTC).millis
            val seconds = TimeUnit.MILLISECONDS.toSeconds(now - past)
            val minutes = TimeUnit.MILLISECONDS.toMinutes(now - past)
            val hours = TimeUnit.MILLISECONDS.toHours(now - past)
            val days = TimeUnit.MILLISECONDS.toDays(now - past)

            if (seconds < 60) {
                return if (mContext != null) mContext!!.getString(R.string.just_now) else "Just now"
            } else if (minutes < 60) {
                return if (mContext != null) String.format(mContext!!.getString(R.string.minutes_ago), minutes) else "$minutes minutes ago"
            } else if (hours < 24) {
                return if (mContext != null) String.format(mContext!!.getString(R.string.hours_ago), hours) else "$hours hours ago"
            } else {
                return if (mContext != null) String.format(mContext!!.getString(R.string.days_ago), days) else "$days days ago"
            }
        } catch (e: Exception) {
            return ""
        }
    }

}
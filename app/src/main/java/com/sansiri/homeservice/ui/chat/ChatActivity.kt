package com.sansiri.homeservice.ui.chat

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.transition.Fade
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.OnVerticalScrollListener
import com.sansiri.homeservice.data.network.firebase.FireStoreManager
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.chat.ChatTemplate
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.*
import com.stfalcon.frescoimageviewer.ImageViewer
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.joda.time.DateTime
import java.io.File



class ChatActivity : BaseActivity<ChatContract.View, ChatContract.Presenter>(), ChatContract.View, IPickResult {

    override var mPresenter: ChatContract.Presenter = ChatPresenter()
    private var mPickUpDialog: PickImageDialog? = null
    private var mMessage: String = ""
        set(value) {
            field = value
            buttonSend?.isEnabled = value.isNotEmpty()
            buttonSend?.alpha = if (value.isNotEmpty()) 1f else 0.8f
        }
    private val mAdapter = ChatAdapter({ imageUrl ->
        ImageViewer.Builder(this, listOf(imageUrl))
                .setStartPosition(0)
                .show()
    }) { url ->
        WebActivity.start(this, url, url)
    }

    private val mChatTemplateAdapter = ChatTemplateAdapter() { message ->
        mPresenter.sendMessage(message)
        hidePanelChatTemplate()
        showChatTemplateButton()
    }

    var isEnableChatTemplate = false

    companion object {
        val HOME = "HOME"
        val TITLE = "TITLE"
        val MESSAGE_QUEUE = "MESSAGE_QUEUE"

        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, ChatActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }

        fun start(activity: Activity, home: Hut, title: String?, messageQueue: ArrayList<ChatMessage>) {
            val intent = Intent(activity, ChatActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            intent.putParcelableArrayListExtra(MESSAGE_QUEUE, messageQueue)
            activity.startActivity(intent)
        }
    }


    private var mMessageQueue: ArrayList<ChatMessage> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.chat))

        progressBarUpload.max = 100

        mPickUpDialog = PickImageDialog.build(PickSetup().setSystemDialog(true))
        mPickUpDialog?.setOnPickResult(this)

        editMessage.addTextChangedListener(TextWatcherExtend {
            mMessage = it
        })

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            linearLayoutManager.reverseLayout = true
            adapter = mAdapter
            layoutManager = linearLayoutManager
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager, FireStoreManager.CHAT_COUNT_IN_PAGE) {
                override fun onLoadMore(current_page: Int) {
                    Log.d("onLoadMore", "$current_page")
                    mPresenter.loadMore()
                }
            })

            addOnScrollListener(object : OnVerticalScrollListener() {
                override fun onScrolledUp() {
                    showChatTemplateButton()
                }

                override fun onScrolledDown() {
                    hideChatTemplateButton()
                }
            })
        }

        listTemplate.apply {
            adapter = mChatTemplateAdapter
        }

        buttonAddImage.setOnClickListener {
            mPickUpDialog?.show(this)
        }

        buttonSend.setOnClickListener {
            if (mMessage.isNotEmpty()) {
                mPresenter.sendMessage(mMessage)
                editMessage.setText("")
            }
        }

        buttonChatTemplate.setOnClickListener {
            showPanelChatTemplate()
            hideChatTemplateButton()
        }

        buttonCloseChatTemplate.setOnClickListener {
            hidePanelChatTemplate()
            showChatTemplateButton()
        }

        panelChatTemplate.setOnClickListener {
            hidePanelChatTemplate()
            showChatTemplateButton()
        }

        mMessage = ""

        val home = intent?.getParcelableExtra<Hut>(HOME)
        mMessageQueue = intent?.getParcelableArrayListExtra<ChatMessage>(MESSAGE_QUEUE)
                ?: arrayListOf()

        home?.let {
            mPresenter.init(it)
            mAdapter.setSenderProfile(home.icon)
        }

        mMessageQueue.forEach { message ->
            mPresenter.sendMessage(message)
            sendEvent("CHAT", "SEND", "TEXT")
        }

        checkJuristicAvailableTime()

        mPresenter.start()
    }

    private fun checkJuristicAvailableTime() {

    }

    override fun showError(message: String) {
        snackError(message)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("CHAT")
    }

    override fun onDestroy() {
        mPresenter.destroy()
        // jobHandler.removeCallbacks(jobRunnable)
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun addMessage(message: List<ChatMessage>) {
        mAdapter.addMessage(message)
        //list.scrollToPosition(mPlanAdapter.itemCount - 1)
    }

    override fun addMessageAt(message: List<ChatMessage>, position: Int) {
        mAdapter.addMessageAt(message, position)
        //list.scrollToPosition(mPlanAdapter.itemCount - 1)
    }

    override fun showUploadProgress() {
        progressBarUpload.show()
    }

    override fun hideUploadProgress() {
        progressBarUpload.hide()
    }

    override fun updateUploadProgress(progress: Double) {
        progressBarUpload.progress = progress.toInt()
    }

    override fun showUploadError() {
        snackError(getString(R.string.error_fail_to_upload))
    }

    override fun showSendMessageError() {
        snackError(getString(R.string.error_fail_to_send_message))
    }

    override fun showNotice() {
        panelNotice.show()
    }

    override fun hideNotice() {
        panelNotice.hide()
    }

    override fun bindChatTemplate(chatTemplates: List<ChatTemplate>) {
        mChatTemplateAdapter.setData(chatTemplates)
    }

    override fun showChatTemplateButton() {
        if (buttonChatTemplate.visibility == View.GONE && isEnableChatTemplate) {
            val slide = Slide()
            slide.slideEdge = Gravity.BOTTOM
            TransitionManager.beginDelayedTransition(container, slide)
            buttonChatTemplate.show()
        }
    }

    override fun hideChatTemplateButton() {
        if (buttonChatTemplate.visibility == View.VISIBLE) {
            val slide = Slide()
            slide.slideEdge = Gravity.BOTTOM
            TransitionManager.beginDelayedTransition(container, slide)
            buttonChatTemplate.hide()
        }
    }

    override fun showPanelChatTemplate() {
        val fade = Fade()
        TransitionManager.beginDelayedTransition(container, fade)
        panelChatTemplate.show()
    }

    override fun hidePanelChatTemplate() {
        val fade = Fade()
        TransitionManager.beginDelayedTransition(container, fade)
        panelChatTemplate.hide()
    }

    override fun enableChatTemplateButton() {
        isEnableChatTemplate = true
    }

    override fun disableChatTemplateButton() {
        isEnableChatTemplate = false
    }

    override fun onPickResult(result: PickResult) {
        doAsync {
            val file = File(result.path)
            val scaledBitmap = ImageCompress().resizeImage(result.path)
            val compressedByte = scaledBitmap.compress()
            uiThread {
                mPresenter.upload(file.name, compressedByte)
                sendEvent("CHAT", "SEND", "IMAGE")
            }
        }
    }
}

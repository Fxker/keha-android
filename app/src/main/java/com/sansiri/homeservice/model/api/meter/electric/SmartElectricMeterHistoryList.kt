package com.sansiri.homeservice.model.api.meter.electric

import com.sansiri.homeservice.util.toCurrencyFormat

data class SmartElectricMeterHistoryList (
        val count: Int = 0,
        val items: List<SmartElectricMeterDetail>,
        val title: String? = null,
        val dateType: String? = null,
        val timestamp: String? = null,
        val isFillEmptyData: Boolean = false,
        val kiloWatt: Double,
        val kiloWattDisplay: String,
        val costEstimate: Double,
        val costEstimateDisplay: String
) {
    val displayKiloWattHour: String
        get() {
            return "${kiloWatt.toCurrencyFormat()} kWh"
        }

    val displayCostEstimate: String
        get() {
            return "${costEstimate.toCurrencyFormat()}฿"
        }

}
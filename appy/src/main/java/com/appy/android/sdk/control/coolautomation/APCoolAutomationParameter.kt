package com.appy.android.sdk.control.coolautomation

interface APCoolAutomationParameter {

    fun action(): String

}
package com.sansiri.homeservice.ui.language

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

/**
 * Created by sansiri on 10/20/17.
 */
interface LanguageContract {
    interface Presenter : BasePresenter<View> {
        fun selectThai()
        fun selectEnglish()
        fun selectJapanese()
        fun selectChineseTraditional()
        fun selectChineseSimplified()
    }

    interface View : BaseView {
        fun setAppLanguage(locale: Locale)
    }
}
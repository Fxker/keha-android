package com.sansiri.homeservice.model.api.maintenance

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class MaintenancePlan(
		val productId: String? = null,
		val accessoryId: String? = null,
		val accessoryName: String? = null,
		val maintenanceType: String? = null,
		val frequency: String? = null,
		val productDescription: String? = null,
		val planDatetime: String? = null,
		val imagePath: String? = null
) : Parcelable {
	val _planDatetime: Date?
		get() {
			return try {
				if (this.planDatetime == null) return null

				DateTime(this.planDatetime).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(productId)
		writeString(accessoryId)
		writeString(accessoryName)
		writeString(maintenanceType)
		writeString(frequency)
		writeString(productDescription)
		writeString(planDatetime)
		writeString(imagePath)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<MaintenancePlan> = object : Parcelable.Creator<MaintenancePlan> {
			override fun createFromParcel(source: Parcel): MaintenancePlan = MaintenancePlan(source)
			override fun newArray(size: Int): Array<MaintenancePlan?> = arrayOfNulls(size)
		}
	}
}

package com.sansiri.homeservice.ui.homeupgrade.selector

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import java.util.function.BiConsumer

/**
 * Created by sansiri on 12/19/17.
 */
class HomeUpgradeSelectorPresenter : BasePresenterImpl<HomeUpgradeSelectorContract.View>(), HomeUpgradeSelectorContract.Presenter {


    lateinit var mUnitId: String
    var mHomeUpgradeRoom: ArrayList<HomeUpgradeRoom>? = null
    val mHardwareList = hashMapOf<Pair<String, String>, HomeUpgradeHardware>()
    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(
                        api.getHomeUpgradeList(mUnitId).observe().subscribe({
                            mHomeUpgradeRoom = ArrayList(it)
                            mView?.hideLoading()
                            mView?.bindData(it)
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun addOrderToList(hardware: HomeUpgradeHardware) {
        if (hardware.roomId != null) {
            mHardwareList[Pair(hardware.id, hardware.roomId!!)] = hardware
        }
        calculateTotalPrice()
    }

    override fun requestSummary() {
        if (mHomeUpgradeRoom != null) {
            mView?.launchSummary(mHomeUpgradeRoom!!, mUnitId)
        }
    }

    private fun calculateTotalPrice() {
        var totalPrice = 0.0

        for((_, value) in mHardwareList.filter { it.value.amount != 0 }) {
            totalPrice += value.price!! * value.amount
        }

        mView?.showTotalPrice(totalPrice)
    }
}
package com.appy.android.code.data.rest.JsonModel

import com.google.gson.annotations.SerializedName

class StatusJsonModel {
    @SerializedName("temperature")
    var temperature: String? = null
    @SerializedName("on_off")
    var onOff: String? = null
    @SerializedName("address")
    var address: String? = null
    @SerializedName("fan_speed")
    var fanSpeed: String? = null
    @SerializedName("fan_direction")
    var fanDirection: String? = null
    @SerializedName("operation_mode")
    var operationMode: String? = null
}
package com.sansiri.homeservice.data.network

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.util.showHttpError
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response

class CallHandler<DATA : Any> {
    lateinit var client: Deferred<Response<DATA>>

    @Suppress("UNCHECKED_CAST")
    fun makeCall(liveData: MutableLiveData<Resource<DATA>>?): MutableLiveData<Resource<DATA>> {
        val result = liveData ?: MutableLiveData()
        result.value = Resource.loading(result.value?.data)

        GlobalScope.launch {
            try {
                val response = client.awaitResult().getOrThrow()
                withContext(Dispatchers.Main) {
                    result.value = Resource.success(response)
                }
            } catch (e: Throwable) {
                withContext(Dispatchers.Main) {
                    if (e is HttpException)
                        result.value = Resource.error(e.showHttpError(), 0)
                    else
                        result.value = Resource.error("${e.message}", 0)
                }
                e.printStackTrace()
            }
        }

        return result
    }
}

fun <DATA : Any> networkCall(liveData: MutableLiveData<Resource<DATA>>? = null, block: CallHandler<DATA>.() -> Unit): MutableLiveData<Resource<DATA>> = CallHandler<DATA>().apply(block).makeCall(liveData)

interface DataResponse<T> {
    fun retrieveData(): T
}

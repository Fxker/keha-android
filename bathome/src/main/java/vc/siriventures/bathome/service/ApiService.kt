package vc.siriventures.bathome.service

import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*
import vc.siriventures.bathome.model.BAtHomeComponent
import vc.siriventures.bathome.model.GatewayInfoRequest
import vc.siriventures.bathome.model.GatewayInfoResponse

interface ApiService {
    @Multipart
    @POST("login")
    fun login(@Part("username") username: RequestBody, @Part("password") password: RequestBody): Observable<ResponseBody>

    @GET("bathome/api/list/components")
    fun getAllComponents(): Observable<List<BAtHomeComponent>>

    @GET("bathome/api/list/lights")
    fun getLights(): Observable<List<BAtHomeComponent>>

    @GET("bathome/api/list/ac")
    fun getACs(): Observable<List<BAtHomeComponent>>

    @GET("bathome/api/list/scenarios")
    fun getScenes(): Observable<List<BAtHomeComponent>>

    @POST("bathome/api/light-on/{lightId}")
    fun turnOnLight(@Path("lightId") lightId: String): Observable<ResponseBody>

    @POST("bathome/api/light-off/{lightId}")
    fun turnOffLight(@Path("lightId") lightId: String): Observable<ResponseBody>

    @POST("bathome/api/group-on/{groupId}")
    fun turnOnGroup(@Path("groupId") groupId: String): Observable<ResponseBody>

    @POST("bathome/api/group-off/{groupId}")
    fun turnOffGroup(@Path("groupId") groupId: String): Observable<ResponseBody>

    @POST("bathome/api/scenario/{sceneId}")
    fun turnOnScene(@Path("sceneId") sceneId: String): Observable<ResponseBody>
}
package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable
import com.orvibo.homemate.data.RoomType.DEFAULT_ROOM

data class HMRoom(
        var id: String? = null,
        var name: String? = null,
        var floorId: String? = null,
        var type: Int = DEFAULT_ROOM,
        var imgUrl: String? = null
) : Parcelable {
    class RoomType {
        companion object {
            val DEFAULT_ROOM = -1
            val DRAWING_ROOM = 0
            val BEDROOM_ONE = 1
            val BEDROOM_TWO = 2
            val DINING_ROOM = 3
            val KITCHEN = 4
            val TOLIET = 5
            val STUDY = 6
            val CHILD = 7
            val BALCONY = 8
            val CORRIDOR = 9
            val GARDEN = 10
            val CLOAKROOM = 11
            val LAUNDRY = 12
            val GARAGE = 13
            val OTHER = 14
            val ALL = 15
            val SCENE = 300
        }
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(name)
        writeString(floorId)
        writeInt(type)
        writeString(imgUrl)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMRoom> = object : Parcelable.Creator<HMRoom> {
            override fun createFromParcel(source: Parcel): HMRoom = HMRoom(source)
            override fun newArray(size: Int): Array<HMRoom?> = arrayOfNulls(size)
        }
    }
}
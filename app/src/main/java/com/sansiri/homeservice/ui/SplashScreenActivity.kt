package com.sansiri.homeservice.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper.IS_FIRST_TIME
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.ui.language.LanguageActivity
import com.sansiri.homeservice.ui.welcome.WelcomeActivity
import com.sansiri.homeservice.util.withDelay
import com.sansiri.homeservice.data.database.PreferenceHelper.defaultPrefs
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.ui.main.MainActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val prefs = defaultPrefs(this)

        if (FirebaseAuthManager.isSignIn()) {
            {
                MainActivity.start(this)
                finish()
            }.withDelay(500)
        } else if (prefs[IS_FIRST_TIME, true]!!) {
            {
                LanguageActivity.start(this)
                finish()
            }.withDelay(500)
        } else {
            {
                WelcomeActivity.start(this)
                finish()
            }.withDelay(500)
        }
    }
}

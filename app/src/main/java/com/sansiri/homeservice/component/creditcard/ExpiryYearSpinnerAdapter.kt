package com.sansiri.homeservice.component.creditcard

import org.joda.time.YearMonth

import co.omise.android.ui.NumberRangeSpinnerAdapter

class ExpiryYearSpinnerAdapter : NumberRangeSpinnerAdapter(YearMonth.now().year, YearMonth.now().year + 12) {

    override fun getItemDropDownLabel(number: Int): String {
        return Integer.toString(number)
    }

    override fun getItemLabel(number: Int): String {
        return Integer.toString(number).substring(2, 4)
    }
}

package com.appy.android.code.data.rest.error

import com.appy.android.code.base.data.rest.error.APError

class APUnknownConnectionErrorException : APError()
package com.sansiri.homeservice.ui.epayment.qr

import android.os.Bundle
import com.sansiri.homeservice.ui.epayment.barcode.QRContract
import com.sansiri.homeservice.ui.epayment.barcode.QRPresenter
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_barcode.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import com.sansiri.homeservice.ui.epayment.CodeEngine
import com.sansiri.homeservice.ui.epayment.EPaymentActivity


abstract class QRActivity(val analyticCode: String) : EPaymentActivity<QRContract.View, QRContract.Presenter>(), QRContract.View {
    override var mPresenter: QRContract.Presenter = QRPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buttonDownload.hide()
    }

    fun renderQRCode(code: String?) {
        textBarcode.text = code?.replace("\n", " ")

        doAsync {
            val bitmapQR = CodeEngine(this@QRActivity).createQR(code ?: "")
            uiThread {
                buttonDownload.show()
                buttonDownload.setOnClickListener {
                    sendEvent(analyticCode, "SAVE", "QR")
                    save(bitmapQR)
                }
                layoutBarcode.hide()
                imageQR.show()
                imageQR.setImageBitmap(bitmapQR)
            }
        }

        buttonBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun showError(message: String) {
    }
}

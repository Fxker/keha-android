package com.sansiri.homeservice.ui.facilitybooking.detail

import com.google.gson.Gson
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.api.facilitybooking.*
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.*
import retrofit2.HttpException

/**
 * Created by sansiri on 10/16/17.
 */
class FacilityBookingDetailPresenter : BasePresenterImpl<FacilityBookingDetailContract.View>(), FacilityBookingDetailContract.Presenter {
    private var mProjectId: String = ""
    private var mUnitId: String = ""
    private var mAddress: String = ""
    private var mFacility: FacilityBooking? = null
    private var mBooking: Booking? = null
    private var mProfile: Profile? = null

    override fun init(projectId: String, unitId: String, address: String, facility: FacilityBooking, booking: Booking) {
        this.mProjectId = projectId
        this.mUnitId = unitId
        this.mAddress = address
        this.mFacility = facility
        this.mBooking = booking
    }


    override fun start() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mCompositeDisposable.add(api.getMe().observe().subscribe({profile ->
                    this.mProfile = profile
                }){})
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun requestTermsAndConditions() {
        val terms = mFacility?.getContent()?.terms
        mView?.showTermsAndConditions(terms ?: "")
    }

    override fun submit(note: String, endTime: String) {
        mView?.showLoading()

        val startTime = mBooking?.startTime?.time?.reportAppyFormat()
        val endDate = endTime.toDate("yyyy-MM-dd HH:mm:ss Z").reportAppyFormat()

        if (mFacility != null && startTime != null) {
            val body = FacilityBookingRequest(mFacility!!.id!!, startTime, endDate, note, mProfile?.firstname, mProfile?.lastname, mAddress)

            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mCompositeDisposable.add(
                            api.submitFacilityBooking(mProjectId, mUnitId, body).observe().subscribe({
                                mView?.hideLoading()
                                mView?.success()
                            }){
                                mView?.hideLoading()
                                if (it is HttpException) {
                                    val error = it.response()?.errorBody()?.string()
                                    if (error != null) {
                                        try {
                                            val appyError = Gson().fromJson(error, AppyError::class.java)
                                            val errorCode = appyError?.errors?.getOrNull(0)
                                            if (errorCode != null) {
                                                when(errorCode) {
                                                    "reservation_book_limit_reach" -> mView?.showError(R.string.reservation_book_limit_reach)
                                                    "date_has_passed" -> mView?.showError(R.string.date_has_passed)
                                                    "slot_invalid" -> mView?.showError(R.string.slot_invalid)
                                                    "reservation_not_found", "slot_not_found" -> mView?.showError(R.string.reservation_not_found)
                                                    // "booking_deadline_passed" -> mView?.showError(R.string.booking_deadline_passed)
                                                    "reservation_has_been_booked", "slot_has_been_booked" -> mView?.showError(R.string.reservation_has_been_booked)
                                                    else -> {
                                                        mView?.showError(it.showHttpError())
                                                    }
                                                }
                                            } else {
                                                mView?.showError(it.showHttpError())
                                            }
                                        } catch (e: Exception) {
                                            mView?.showError(it.showHttpError())
                                        }
                                    }
                                } else {
                                    mView?.showError(it.localizedMessage)
                                }
                            }
                    )
                } else {
                    mView?.hideLoading()
                }
            }
        } else {
            // TODO: Error
        }

    }

    override fun cancelBooking() {
        if (mBooking != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mView?.showLoading()
                    mCompositeDisposable.add(
                            api.cancelFacilityBooking(mUnitId, mBooking?.idBooking ?: "").observe().subscribe({
                                mView?.hideLoading()
                                mView?.success()
                            }){
                                mView?.hideLoading()
                                mView?.showError(it.showHttpError())
                            }
                    )
                }
            }
        }
    }
}
package com.sansiri.homeservice.ui.homeupgrade.summary

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by sansiri on 10/12/17.
 */
class HomeUpgradeSummarySectionAdapter(val title: String, val mData: List<HomeUpgradeHardware>) : StatelessSection(SectionParameters.Builder(R.layout.view_home_upgrade_summary_item)
        .headerResourceId(R.layout.view_title)
        .build()) {

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return HomeUpgradeSummaryViewHolder(view)
    }

    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is HomeUpgradeSummaryViewHolder) {
            holder.bind(mData[position])
        }
    }


}
package com.sansiri.homeservice.model.api

data class MultiLanguageMessage (
    val th: String? = null,
    val en: String? = null
)
package com.sansiri.homeservice.ui.base

import android.content.Context
import android.os.Bundle
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.data.network.AnalyticProvider


/**
 * Created by oakraw on 6/12/2017 AD.
 */
abstract class AnalyticsActivity: LocalizationActivity() {
    private var analyticProvider: AnalyticProvider? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val app = application as AppController
        app.guardRoute(this)
        analyticProvider = AnalyticProvider(this)
    }

    fun sendScreenView(screenName: String) {
        analyticProvider?.sendScreenView(screenName)
    }

    fun sendEvent(category: String, action: String, label: String?, logVersion: String, vararg optionalProp: Pair<String, String>) {
        analyticProvider?.sendEvent(category, action, label, logVersion, optionalProp.toList())
    }
}
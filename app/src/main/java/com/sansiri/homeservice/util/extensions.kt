package com.sansiri.homeservice.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.*
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.MultiFormatWriter
import java.util.*
import android.os.Handler
import android.provider.MediaStore
import com.google.android.material.snackbar.Snackbar
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.ImageViewCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.google.zxing.BarcodeFormat
import android.widget.Toast
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.room.APRoom
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.onionshack.onionspeech.OnionDevice
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.CallbackWrapper
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.model.api.ErrorMessage
import com.sansiri.homeservice.model.menu.AppyHomeAutomationRoomMenu
import com.sansiri.homeservice.ui.base.BaseView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import retrofit2.HttpException
import java.io.ByteArrayOutputStream
import java.net.URLDecoder
import java.text.*
import kotlin.collections.ArrayList


/**
 * Created by oakraw on 9/29/2017 AD.
 */
fun String.removeSpace(): String = this.replace(" ", "")

fun String.removeDash(): String = this.replace("-", "")

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun AppCompatActivity.setBackToolbar(toolbar: Toolbar, title: String) {
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
}

fun AppCompatActivity.setBackToolbar(title: String, textAllCaps: Boolean = false) {
    setSupportActionBar(toolbar)
    supportActionBar?.title = ""
    textToolbar.text = title
    textToolbar.isAllCaps = textAllCaps
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
}

fun AppCompatActivity.replaceFragment(fragment: androidx.fragment.app.Fragment, isAddToBackStack: Boolean) {
    val transaction = supportFragmentManager.beginTransaction()
    if (isAddToBackStack) {
        transaction.replace(R.id.fragmentContainer, fragment).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null).commit()
    } else {
        transaction.replace(R.id.fragmentContainer, fragment).commit()
    }
}

fun AppCompatActivity.replaceNonExistFragment(fragment: androidx.fragment.app.Fragment, tag: String, isAddToBackStack: Boolean) {
    val transaction = supportFragmentManager.beginTransaction()
    if (supportFragmentManager.findFragmentByTag(tag) == null) {
        if (isAddToBackStack) {
            transaction.replace(R.id.fragmentContainer, fragment, tag).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null).commit()
        } else {
            transaction.replace(R.id.fragmentContainer, fragment, tag).commit()
        }
    }
}

fun AppCompatActivity.addFragment(fragment: androidx.fragment.app.Fragment, isAddToBackStack: Boolean) {
    val transaction = supportFragmentManager.beginTransaction()
    if (isAddToBackStack) {
        transaction.add(R.id.fragmentContainer, fragment).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null).commit()
    } else {
        transaction.add(R.id.fragmentContainer, fragment).commit()
    }
}

fun AppCompatActivity.addNonExistFragment(fragment: androidx.fragment.app.Fragment, tag: String, isAddToBackStack: Boolean) {
    val transaction = supportFragmentManager.beginTransaction()
    if (supportFragmentManager.findFragmentByTag(tag) == null) {
        if (isAddToBackStack) {
            transaction.add(R.id.fragmentContainer, fragment, tag).setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null).commit()
        } else {
            transaction.add(R.id.fragmentContainer, fragment, tag).commit()
        }
    }
}


fun Activity.startApp(packageName: String, forcePlayStore: Boolean = false) {
    val launchIntent = packageManager?.getLaunchIntentForPackage(packageName)
    if (launchIntent == null || forcePlayStore) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$packageName")))
    } else {
        this.startActivity(launchIntent)
    }
}

fun Activity.launchExternalBrowser(url: String) {
    val uri = Uri.parse(url)
    val browserIntent = Intent(Intent.ACTION_VIEW)
    browserIntent.setDataAndType(uri, "text/html")
    browserIntent.addCategory(Intent.CATEGORY_BROWSABLE)
    startActivity(browserIntent)
}

fun Activity.snack(text: String) {
    Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show()
}

fun Activity.snackError(text: String) {
    val snackBar = Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
    snackBar.view.setBackgroundColor(resources.getColor(R.color.colorCancel))
    snackBar.show()
}

fun Activity.alertError(message: String, title: String? = null, callback: (() -> Unit)? = null) {
    alert(message, title ?: getString(R.string.error_something_went_wrong)) {
        yesButton {
            callback?.invoke()
        }
    }.show()
}

fun Activity.alertMessageError(message: String, callback: (() -> Unit)? = null) {
    alert(message, null) {
        yesButton {
            callback?.invoke()
        }
    }.show()
}

fun Activity.shareBitmap(bitmap: Bitmap) {
    val bytes = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(contentResolver, bitmap, System.currentTimeMillis().toString(), null)
    val uri = Uri.parse(path)
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "image/jpg"
    intent.putExtra(Intent.EXTRA_STREAM, uri)
    try {
        startActivity(Intent.createChooser(intent, getString(R.string.share)));
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
        Toast.makeText(this, "Error: Could not find a compatible application", Toast.LENGTH_SHORT).show()
    }
}

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun View.tint(color: Int) {
    background.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun String?.showTextOrHideIfNull(textView: TextView, vararg views: View) {
    if (this.isNullOrEmpty()) {
        textView.hide()
        views.forEach { view -> view.hide() }
    } else {
        textView.show()
        views.forEach { view -> view.show() }
        textView.text = this
    }
}

fun Any.toJson(): String {
    return Gson().toJson(this)
}

fun <T> String.toObject(): T {
    val type = object : TypeToken<T>() {}.type
    return Gson().fromJson<T>(this, type)
}

fun <K, V> Map<out K, V>.toValueList(): List<V> {
    if (size == 0)
        return emptyList()

    val iterator = entries.iterator()
    if (!iterator.hasNext())
        return emptyList()
    val first = iterator.next()
    if (!iterator.hasNext())
        return listOf(first.value)

    val result = ArrayList<V>(size)
    result.add(first.value)
    do {
        result.add(iterator.next().value)
    } while (iterator.hasNext())
    return result
}

@SuppressLint("SimpleDateFormat")
fun String.toDate(): Date {
    val format = SimpleDateFormat("yyyy-MM-dd")
    return format.parse(this)
}

fun String.toDate(pattern: String): Date {
    val format = SimpleDateFormat(pattern, Locale.getDefault())
    return format.parse(this)
}


fun String.decode(): String = URLDecoder.decode(this, "UTF-8")


fun Date.report(): String {
    val format = when (Locale.getDefault()) {
        Locale.SIMPLIFIED_CHINESE, Locale.TRADITIONAL_CHINESE -> SimpleDateFormat("yyyy年MMMd日", Locale.getDefault())
        else -> SimpleDateFormat("d MMM yyyy", Locale.getDefault())
    }
    return format.format(this)
}

fun Date.report(pattern: String): String {
    val format = SimpleDateFormat(pattern, Locale.getDefault())
    return format.format(this)
}

fun Date.toISO8601(): String {
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault())
    return format.format(this)
}

fun Date.reportTime(): String {
    val format = SimpleDateFormat("HH:mm", Locale.getDefault())
    return format.format(this)
}

fun Date.reportDMYFull(): String {
    val format = SimpleDateFormat("d MMMM yyyy", Locale.getDefault())
    return format.format(this)
}

fun Date.reportMY(): String {
    val format = SimpleDateFormat("MMM yyyy", Locale.getDefault())
    return format.format(this)
}

fun Date.reportDMYNumber(): String {
    val format = SimpleDateFormat("dd/MM/yy", Locale.getDefault())
    return format.format(this)
}

fun Date.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = this

    return calendar
}


fun Date.toCalendarHM(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)

    return calendar
}

fun Date.reportMMY(): String {
    val format = SimpleDateFormat("MMMM yyyy", Locale.getDefault())
    return format.format(this)
}

fun Date.reportAppyFormat(): String {
    val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    return format.format(this)
}

fun Date.resetTime(): Date {
    return Calendar.getInstance().apply {
        time = this@resetTime
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.time
}

fun Calendar.reportTime(): String =
        "${get(Calendar.HOUR_OF_DAY)?.format(2)}:${get(Calendar.MINUTE)?.format(2)}"


fun Calendar.reportDate(): String =
        "${get(Calendar.YEAR)}-${get(Calendar.MONTH) + 1}-${get(Calendar.DAY_OF_MONTH)}".toDate().report()


fun Int.format(digits: Int) = String.format("%0${digits}d", this)


fun MultiFormatWriter.generateBarcode(data: String): Bitmap {
    val bm = encode(data, BarcodeFormat.CODE_128, 150, 150)
    val imageBitmap = Bitmap.createBitmap(bm.width, bm.height, Bitmap.Config.ARGB_8888)

    for (i in 0 until bm.width) {//width
        for (j in 0 until bm.height) {//height
            imageBitmap.setPixel(i, j, if (bm.get(i, j)) Color.BLACK else Color.WHITE)
        }
    }

    return imageBitmap
}

fun MultiFormatWriter.generateQR(data: String): Bitmap {
    val width = 500
    val bm = encode(data, BarcodeFormat.QR_CODE, width, width)
    return BarcodeEncoder().createBitmap(bm)
//    val imageBitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888)
//
//    for (i in 0 until width) {//width
//        for (j in 0 until width) {//height
//            imageBitmap.setPixel(i, j, if (bm.get(i, j)) Color.BLACK else Color.WHITE)
//        }
//    }
}

fun ImageView.tintImage(color: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(color))

}

fun Uri.getBitmap(context: Context): Bitmap? {
    val inputStream = context.contentResolver.openInputStream(this)
    val bitmap = BitmapFactory.decodeStream(inputStream)
    inputStream?.close()
    return bitmap
}

fun Int.toCurrencyFormat(): String {
    val formatter = DecimalFormat("#,###,###")
    return formatter.format(this)
}

fun Double.toCurrencyFormat(): String {
    if (this == 0.0) return "0"
    val formatter = DecimalFormat("#,###,###.00")
    var output = formatter.format(this)
    output = if (output[0] == '.') output.replace(".", "0.") else output
    return output.replace(".00", "")
}

fun (() -> Unit).withDelay(delay: Long): (() -> Unit) {
    val handler = Handler()
    val runnable = Runnable(this)
    handler.postDelayed(runnable, delay)
    return { handler.removeCallbacks(runnable) }
}

fun <T> Observable<T>.observe(): Observable<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}


fun <T> Single<T>.observe(): Single<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.observe(): Completable {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.subscribe(view: BaseView, onSuccess: (T) -> Unit, onError: ((Throwable) -> Unit)? = null): CallbackWrapper<T> {
    return this.subscribeWith(object : CallbackWrapper<T>(view) {
        override fun onSuccess(response: T) {
            onSuccess(response)
        }

        override fun onError(e: Throwable) {
            if (onError != null) {
                onError(e)
            } else {
                super.onError(e)
            }
        }
    })
}

fun Throwable.showHttpError(): String {
    if (this is HttpException) {
        var error: ErrorMessage? = null
        val errorMessage = response()?.errorBody()?.string()
        try {
            error = Gson().fromJson(errorMessage, ErrorMessage::class.java)
        } catch (e: Exception) {

        }
        if (error == null) {
            when (code()) {
                404 -> {
                    return "Data not found"
                }
                500 -> {
                    return "Internal server error"
                }
                400 -> {
                    return "Bad request"
                }
                else -> {
                    return "Oops, something went wrong. Please try again later"
                }
            }
        } else {
            return error.message ?: localizedMessage
        }
    } else {
        return ""
    }
}

fun Throwable.getHttpCode(): Int {
    return if (this is HttpException) {
        code()
    } else {
        0
    }
}


fun Bitmap.resize(maxSize: Int = 1600): Bitmap {
    var newWidth = 0
    var newHeight = 0

    if (width > height) {
        newWidth = this.width * maxSize / width
        newHeight = this.height * maxSize / width
    } else {
        newWidth = this.width * maxSize / height
        newHeight = this.height * maxSize / height
    }



    return Bitmap.createScaledBitmap(this, newWidth, newHeight, true)
}

fun Bitmap.compress(quality: Int = 70): ByteArray {
    val stream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.JPEG, quality, stream)
    return stream.toByteArray()
}


fun APRoom.convert(): AppyHomeAutomationRoomMenu {
    val displayTitle = if (Store.getLocale()?.language == "th") {
        if (titleAlternate != null && titleAlternate == "") {
            title ?: ""
        } else {
            titleAlternate ?: title ?: ""
        }
    } else {
        title ?: ""
    }

    return AppyHomeAutomationRoomMenu(this.id ?: "", this.icon?.large ?: "", displayTitle)
}

fun APControl.convert(): OnionDevice {
    val type = when (type) {
        AppyRepositoryImpl.Type.LIGHT.key ->
            OnionDevice.Type.Light
        AppyRepositoryImpl.Type.TOGGLE.key ->
            OnionDevice.Type.Light
        AppyRepositoryImpl.Type.AC.key ->
            OnionDevice.Type.AirConditioner
        AppyRepositoryImpl.Type.OUTLET.key ->
            OnionDevice.Type.UNDEFINED
        AppyRepositoryImpl.Type.BLIND.key ->
            OnionDevice.Type.Blind
        AppyRepositoryImpl.Type.DIMMER.key ->
            OnionDevice.Type.Dimmer
        AppyRepositoryImpl.Type.SCENE.key ->
            OnionDevice.Type.UNDEFINED
        else ->
            OnionDevice.Type.UNDEFINED
    }

    val splitTitle = title?.split(" - ")
    val deviceTitle = if (splitTitle?.getOrNull(1) != null) {
        splitTitle[1]
    } else {
        splitTitle?.first()
    }

    return OnionDevice(id, deviceTitle, type, titleAlternate)
}

val APControl.localizeTitle: String
    get() {
        if (Store.getLocale()?.language == "th") {
            if (titleAlternate != null && titleAlternate == "") {
                return title ?: ""
            }
            return titleAlternate ?: title ?: ""
        }
        return title ?: ""
    }

/*
* Random from 0 to this number
* */
fun Int.random() = Random().nextInt(this)

/*
* Random from 0 to this number
* */
fun Int.randomTo(maxNumber: Int) = Random().nextInt(maxNumber - this) + this

fun MenuItem.tint(color: Int) {
    val drawable = DrawableCompat.wrap(icon)
    DrawableCompat.setTint(drawable, color)
    icon = drawable
}

fun TextView.setTextHexColor(hexColor: String) {
    val color = try {
        Color.parseColor(hexColor)
    } catch (e: Exception) {
        null
    }

    color?.let { this.setTextColor(it) }
}

fun View.capture(): Bitmap? {
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)

    if (background != null) {
        background.draw(canvas)
    } else {
        canvas.drawColor(Color.WHITE)
    }

    draw(canvas)

    return bitmap
}

fun Bitmap.toByteArray(): ByteArray? {
    val stream = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.PNG, 100, stream)
    return stream.toByteArray()
}

fun ByteArray.toBitmap() = BitmapFactory.decodeByteArray(this, 0, size)

fun <T> List<T>.toArrayList(): ArrayList<T> {
    val arrayList = ArrayList<T>()
    arrayList.addAll(this)
    return arrayList
}
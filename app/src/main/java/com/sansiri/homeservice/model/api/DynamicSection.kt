package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.model.menu.GenericMenu

/**
 * Created by sansiri on 11/30/17.
 */
data class DynamicSection(
        @SerializedName("SECTION")
        val menu: List<GenericMenu>,
        @SerializedName("SHOPPING")
        val shopping: List<GenericMenu>
)
package com.sansiri.homeservice.model.database.orvibo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "orvibo_home")
data class OrviboHome(
        @PrimaryKey
        val unitId: String,
        @ColumnInfo(name = "username")
        val username: String,
        @ColumnInfo(name = "passwordMD5")
        val passwordMD5: String,
        @ColumnInfo(name = "familyId")
        val familyId: String? = null
)
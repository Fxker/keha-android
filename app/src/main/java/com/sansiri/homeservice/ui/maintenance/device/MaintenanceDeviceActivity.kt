package com.sansiri.homeservice.ui.maintenance.device

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.maintenance.MaintenancePlanDetailAdapter
import com.sansiri.homeservice.ui.maintenance.detail.MaintenanceDetailActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_maintenance_device.*
import kotlinx.android.synthetic.main.view_phone_directory_contact.*

class MaintenanceDeviceActivity : BaseActivity<MaintenanceDeviceContract.View, MaintenanceDeviceContract.Presenter>(), MaintenanceDeviceContract.View {
    override var mPresenter: MaintenanceDeviceContract.Presenter = MaintenanceDevicePresenter()

    companion object {
        const val DEVICE = "DEVICE"
        const val HOME = "HOME"

        fun start(activity: Activity, home: Hut, device: MaintenanceDevice) {
            val intent = Intent(activity, MaintenanceDeviceActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(DEVICE, device)
            activity.startActivity(intent)
        }
    }

    val mPlanAdapter = MaintenancePlanDetailAdapter { plan ->
        sendEvent("${Feature.MAINTENANCE_GUIDE}_DEVICE", "CLICK", "PLAN_ACCESSORY", "2", Pair("AccessoryId", plan.accessoryId ?: ""))
        mPresenter.requestForDetail(plan)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maintenance_device)

        val home = intent.getParcelableExtra<Hut>(HOME)
        val device = intent.getParcelableExtra<MaintenanceDevice>(DEVICE)

        setBackToolbar(device.product ?: "")

        listPlan.apply {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mPlanAdapter
        }

        mPresenter.init(home, device)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("${Feature.MAINTENANCE_GUIDE}_DEVICE")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MaintenanceDetailActivity.CODE) {
            if (resultCode == Activity.RESULT_OK) {
                mPresenter.start()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        listPlan.hide()
        textNoData.show()
    }

    override fun bindDeviceDetail(device: MaintenanceDevice?) {
        textBrand.text = device?.brand
        textTitle.text = "ติดต่อ Supplier"

        panelNumber.removeAllViews()
        if (device?.contractPhones != null || device?.contractPhones?.size == 0) {
            device.contractPhones.forEach {contact ->
                val viewNumber = panelNumber.inflate(R.layout.view_phone_directory_number)
                val textNumber = viewNumber.findViewById<TextView>(R.id.textNumber)
                textNumber.text = contact

                textNumber.setOnClickListener {
                    launchPhoneCall(contact ?: "")
                }

                panelNumber.addView(viewNumber)
            }

            buttonCall.setOnClickListener {
                launchPhoneCall(device.contractPhones[0] ?: "")
            }
        } else {
            panelPhone.hide()
        }
    }

    override fun bindPlanDetail(plans: List<MaintenancePlan>) {
        mPlanAdapter.setData(plans)
    }

    override fun launchDetail(home: Hut, plan: MaintenancePlan) {
        MaintenanceDetailActivity.start(this, home, plan.accessoryId ?: "", null, plan)
    }

    private fun launchPhoneCall(tel: String) {
        sendEvent("${Feature.MAINTENANCE_GUIDE}_DEVICE", "CLICK", "TELEPHONE")
        val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
       startActivity(phoneIntent)
   }

    override fun showError(message: String) {
        alertError(message)
    }
}

package com.sansiri.homeservice.model.api

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 11/30/17.
 */
data class ObjectId(val objectId: String? = null) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(objectId)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ObjectId> = object : Parcelable.Creator<ObjectId> {
            override fun createFromParcel(source: Parcel): ObjectId = ObjectId(source)
            override fun newArray(size: Int): Array<ObjectId?> = arrayOfNulls(size)
        }
    }
}
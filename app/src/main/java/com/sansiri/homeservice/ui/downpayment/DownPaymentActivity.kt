package com.sansiri.homeservice.ui.downpayment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentActivity
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.synthetic.main.activity_down_payment.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class DownPaymentActivity : BaseActivity<DownpaymentContract.View, DownpaymentContract.Presenter>(), DownpaymentContract.View {
    override var mPresenter: DownpaymentContract.Presenter = DownpaymentPresenter()

    companion object {
        val HOME ="HOME"
        val TITLE = "TITLE"
        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, DownPaymentActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val upComingAdapter = DownPaymentAdapter(R.color.colorCancel) { downPaymentItem ->
        mPresenter.requestEPayment(downPaymentItem)
    }
    val futureAdapter = DownPaymentAdapter(R.color.colorCancel)
    val paidPaymentAdapter = DownPaymentAdapter(R.color.colorDone)

    val overdueAdapter = DownPaymentAdapter(R.color.colorCancel) { downPaymentItem ->
        mPresenter.requestEPayment(downPaymentItem)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_down_payment)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.downpayment))

        listUpComing.apply {
            adapter = upComingAdapter
        }
        listFuture.apply {
            adapter = futureAdapter
        }
        listPaidPayment.apply {
            adapter = paidPaymentAdapter
        }
        listOverdue.apply {
            adapter = overdueAdapter
        }

//        upComingAdapter.setData(listOf(
//                DownPayment("80,000", "Downpayment 6", Calendar.getInstance(), "Bullet")
//        ))
//
//        futureAdapter.setData(listOf(
//                DownPayment("20,000", "Downpayment 7", Calendar.getInstance(), "Regular"),
//                DownPayment("80,000", "Downpayment 8", Calendar.getInstance(), "Bullet"),
//                DownPayment("80,000", "Downpayment 9", Calendar.getInstance(), "Bullet")
//        ))
//
//        paidPaymentAdapter.setData(listOf(
//                DownPayment("20,000", "Downpayment 1", Calendar.getInstance(), "Paid"),
//                DownPayment("20,000", "Downpayment 2", Calendar.getInstance(), "Paid")
//        ))

        val home = intent?.getParcelableExtra<Hut>(HOME)
        home?.let{ mPresenter.fetchData(home) }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("DOWNPAYMENT")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showNetPrice(netPrice: Double?) {
        textNetPrice.text = netPrice?.toCurrencyFormat()
    }

    override fun showPaidAmount(paidAmount: Double?) {
        textPaidAmount.text = paidAmount?.toCurrencyFormat()
    }

    override fun showRemainingAmount(remainAmount: Double?) {
        textRemaining.text = remainAmount?.toCurrencyFormat()
    }

    override fun addPaidList(downpaymentItem: DownpaymentItem) {
//        titlePaid.show()
//        listPaidPayment.show()
        paidPaymentAdapter.addData(downpaymentItem)
    }

    override fun addFutureList(downpaymentItem: DownpaymentItem) {
//        titleFuture.show()
//        listFuture.show()
        futureAdapter.addData(downpaymentItem)
    }

    override fun addUpComing(downpaymentItem: DownpaymentItem) {
//        titleUpComing.show()
//        listUpComing.show()
        upComingAdapter.addData(downpaymentItem)
    }

    override fun addOverdue(downpaymentItem: DownpaymentItem) {
        sendEvent("DOWNPAYMENT", "VIEW", "OVERDUE")
        titleOverdue.show()
        listOverdue.show()
        overdueAdapter.addData(downpaymentItem)
    }

    override fun launchDownpayment(home: Hut, downPaymentItem: DownpaymentItem) {
        DownpaymentEPaymentActivity.start(this, home, downPaymentItem)
    }

    override fun showError(message: String) {
        alert(message, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }
}

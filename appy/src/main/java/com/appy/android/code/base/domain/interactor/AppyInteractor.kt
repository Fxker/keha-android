package com.appy.android.code.base.domain.interactor

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal abstract class AppyInteractor<T, Params>(
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread
) : Interactor<T, Params>(threadExecutor, postExecutionThread)
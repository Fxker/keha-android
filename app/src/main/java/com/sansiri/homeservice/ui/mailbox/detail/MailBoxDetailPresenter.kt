package com.sansiri.homeservice.ui.mailbox.detail

import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.mailbox.MailBoxContract

/**
 * Created by sansiri on 10/12/17.
 */
class MailBoxDetailPresenter : BasePresenterImpl<MailBoxDetailContract.View>(), MailBoxDetailContract.Presenter {
    override fun start() {
        // prepareBarcode()
    }

    override fun destroy() {
    }

    override fun prepareBarcode() {
        // mView?.generateBarcode("213094329847923401249184")
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.util.gridSize
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.withDelay

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class GridMenuAdapter(val itemClick: (Int) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<Int>()
    override fun getItemCount(): Int = mData.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleTileViewHolder(R.color.colorAccent, parent.inflate(R.layout.view_tile_title), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TitleTileViewHolder -> {
                holder.bind(mData[position])
            }
        }
    }

    fun setData(data: List<Int>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    fun setDataDelay(data: List<Int>) {
        mData.clear()
        data.forEach {
            {
                mData.add(it)
                notifyItemInserted(mData.size - 1)
            }.withDelay(1)
        }
    }
}
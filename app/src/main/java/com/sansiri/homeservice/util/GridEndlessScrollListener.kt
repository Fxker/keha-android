package com.sansiri.homeservice.util

import androidx.recyclerview.widget.RecyclerView
import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.GridLayoutManager




abstract class GridEndlessScrollListener(private val gridLayoutManager: androidx.recyclerview.widget.GridLayoutManager) : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
    private var previousItemCount: Int = 0
    private var loading: Boolean = false
    private var current_page = 0

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            val itemCount = gridLayoutManager.itemCount

            if (itemCount != previousItemCount) {
                loading = false
            }

            if (!loading && gridLayoutManager.findLastVisibleItemPosition() >= itemCount - 1) {
                previousItemCount = itemCount
                current_page += 1
                onLoadMore(current_page)
            }
        }
    }

    abstract fun onLoadMore(current_page: Int)

}
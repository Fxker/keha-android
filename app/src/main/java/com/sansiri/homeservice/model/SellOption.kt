package com.sansiri.homeservice.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 11/7/17.
 */
data class SellOption(var cost: String?, var isEnabled: Boolean = false) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(cost)
        writeInt((if (isEnabled) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SellOption> = object : Parcelable.Creator<SellOption> {
            override fun createFromParcel(source: Parcel): SellOption = SellOption(source)
            override fun newArray(size: Int): Array<SellOption?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p


import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.lifecycle.Observer
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.creditcard.CreditCardView
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_payment_gateway_2c2p.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.koin.androidx.scope.currentScope


class PaymentGateway2C2PFragment : BaseV2Fragment<PaymentGateway2C2PContract.View, PaymentGateway2C2PContract.Presenter>(), PaymentGateway2C2PContract.View {
    override val mPresenter: PaymentGateway2C2PContract.Presenter by currentScope.inject()

    companion object {
        const val DATA = "DATA"
        const val DETAIL_IMAGE = "DETAIL_IMAGE"
        const val PAYMENT_RESPONSE = "PAYMENT_RESPONSE"
        const val ANALYTIC_TITLE = "ANALYTIC_TITLE"

        fun newInstance(paymentResponse: PaymentResponse?, detailImage: ByteArray?, analyticTitle: String?): PaymentGateway2C2PFragment {
            return PaymentGateway2C2PFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(PAYMENT_RESPONSE, paymentResponse)
                    putByteArray(DETAIL_IMAGE, detailImage)
                    putString(ANALYTIC_TITLE, analyticTitle)
                }
            }
        }
    }

    private var mPaymentResponse: PaymentResponse? = null
    private var mDetailImage: Bitmap? = null
    private var mDialogLoading: ProgressDialog? = null
    private var analyticTitle: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPaymentResponse = arguments?.getParcelable(PAYMENT_RESPONSE)
        mDetailImage = arguments?.getByteArray(DETAIL_IMAGE)?.toBitmap()
        analyticTitle = arguments?.getString(ANALYTIC_TITLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment_gateway_2c2p, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        if (mPaymentGatewayData != null) {
//            mPresenter.init(mPaymentGatewayData!!)
        if (mDetailImage == null) {
            cardImageDetail.hide()
        } else {
            cardImageDetail.show()
            imageDetail.setImageBitmap(mDetailImage)
        }

        mPresenter.init(mPaymentResponse)
        mPresenter.start()

        mPresenter.liveDataSavingCreditCards?.observe(this, Observer { creditCards ->
            if (!creditCards.isNullOrEmpty()) {
                // show saving card
                mPresenter.setSavingCard(creditCards)
            } else {
                // show new card
                mPresenter.setNewCard()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        analyticTitle?.let { sendScreenView("${it}_EPAYMENT_2C2P") }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showNewCard(paymentResponse: PaymentResponse?) {
        cardView.show()
        savingCardView.hide()
        textFee.text = paymentResponse?.feeText
        textNewCardCaption.text = paymentResponse?.feeDetails
        textPoweredBy.text = paymentResponse?.poweredByText

        buttonPayNewCard.text = if(paymentResponse?.totalAmountText != null)
            "${getString(R.string.pay)} ${paymentResponse.totalAmountText}"
        else
            getString(R.string.pay)


        creditCardView.listener = object : CreditCardView.Listener {
            override fun onNameUpdated(name: String) {
                mPresenter.setCardName(name)
            }

            override fun onCardNumberUpdated(number: String) {
                mPresenter.setCardNumber(number)
            }

            override fun onExpiryMonthUpdated(expiryMonth: Int) {
                mPresenter.setExpiryMonth(expiryMonth)
            }

            override fun onExpiryYearUpdated(expiryYear: Int) {
                mPresenter.setExpiryYear(expiryYear)
            }

            override fun onCVVUpdated(cvv: String) {
                mPresenter.setCCV(cvv)
            }

            override fun onEmailUpdated(email: String) {
                mPresenter.setEmail(email)
            }

            override fun onRememberChecked(checked: Boolean) {
                mPresenter.setRememberCreditCard(checked)
            }
        }

        if (BuildConfig.BUILD_TYPE == "debug") {
            creditCardView.setDetail(
                    name = "JOHN DOE",
                    cardNumber = "4111111111111111",
                    cvv = "123",
                    month = 12,
                    year = 2019
            )
            mPresenter.validate()
        }

        buttonPayNewCard.setOnClickListener {
            mPresenter.execute()
        }
    }


    override fun showSavingCard(securedCardNumber: String?, cardProvider: String?, paymentResponse: PaymentResponse?) {
        cardView.hide()
        savingCardView.show()
        textFee.text = paymentResponse?.feeText
        textSavingCardNumber.text = securedCardNumber
        textSavingCardProvider.text = cardProvider
        textSavingCardCaption.text = paymentResponse?.feeDetails
        textPoweredBy.text = paymentResponse?.poweredByText

        editSavingCVV.addTextChangedListener(TextWatcherExtend { cvv ->
            mPresenter.setCCV(cvv)
        })

        editSavingEmail.addTextChangedListener(TextWatcherExtend { email ->
            mPresenter.setEmail(email)
        })

        buttonPaySavingCard.text = if(paymentResponse?.totalAmountText != null)
            "${getString(R.string.pay)} ${paymentResponse.totalAmountText}"
        else
            getString(R.string.pay)

        buttonPaySavingCard.setOnClickListener {
            sendEvent("${analyticTitle}_EPAYMENT_2C2P", "CLICK", "PAY")
            mPresenter.execute()
        }

        buttonDeleteCard.setOnClickListener {
            mPresenter.deleteCard()
        }

        disableContinueButton()
    }

    override fun showSavingEmail(billingEmail: String) {
        editSavingEmail.setText(billingEmail)
    }

    override fun showLoading() {
        mDialogLoading = activity?.indeterminateProgressDialog(getString(R.string.processing), null)
    }

    override fun hideLoading() {
        mDialogLoading?.dismiss()
    }

    override fun launchWeb(url: String) {
        (activity as PaymentGatewayActivity).addFragment(PaymentGateway2C2PWebViewFragment.newInstance(url))
//        WebActivity.start(this, url, "")
    }

    override fun enableContinueButton() {
        buttonPaySavingCard.isEnabled = true
        buttonPaySavingCard.alpha = 1f

        buttonPayNewCard.isEnabled = true
        buttonPayNewCard.alpha = 1f
    }

    override fun disableContinueButton() {
        buttonPaySavingCard.isEnabled = false
        buttonPaySavingCard.alpha = 0.5f

        buttonPayNewCard.isEnabled = false
        buttonPayNewCard.alpha = 0.5f
    }

    override fun launchTransactionResult(transactionID: String, email: String?) {
        (activity as PaymentGatewayActivity).launch2C2PTransactionResult(email)
    }
    override fun deleteSavingCard() {
        cardView.show()
        savingCardView.hide()

        disableContinueButton()
    }

    override fun showError(messageRes: Int) {
        showError(getString(messageRes))
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    fun onResponse(response: TransactionResultResponse) {
        mPresenter.handleResponse(response)
    }
}

package com.sansiri.homeservice.model.api.maintenance

data class MaintenanceDetailSubmit(
        val repairedAt: String? = null
)

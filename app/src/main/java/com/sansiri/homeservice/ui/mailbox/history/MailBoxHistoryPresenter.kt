package com.sansiri.homeservice.ui.mailbox.history

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/12/17.
 */
class MailBoxHistoryPresenter : BasePresenterImpl<MailBoxHistoryContract.View>(), MailBoxHistoryContract.Presenter {
    val ITEM = 10
    val allHistories = mutableListOf<MailBox>()

    override fun start() {
    }

    override fun destroy() {
    }

    override fun fetchData(unitObjectId: String, page: Int) {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                mCompositeDisposable.add(
                        it.getMailBoxHistory(unitObjectId, page * ITEM, ITEM).observe().subscribe({
                            mView?.hideLoading()
                            if (it.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                allHistories.addAll(it)
                                mView?.bindData(allHistories)
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }

//        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
//
//        mView?.setData(mutableListOf<MailBox>(
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-03")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-03")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-10")),
//                MailBox("Alisa Sansiri", "From Sansiri Public Company Limited", R.drawable.ic_mail_box, dateFormat.parse("2017-10-10"))
//        ))
    }
}
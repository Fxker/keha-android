package com.sansiri.homeservice.model.api.partner.aqi

import android.os.Parcel
import android.os.Parcelable

data class AirPollutant(val valueDisplay: String? = null,
                        val unit: String? = null,
                        val color: String? = null,
                        val description: String? = null,
                        val iconUrl: String? = null,
                        val title: String? = null,
                        val value: Double? = null,
                        val objectId: String? = null) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(valueDisplay)
        writeString(unit)
        writeString(color)
        writeString(description)
        writeString(iconUrl)
        writeString(title)
        writeValue(value)
        writeString(objectId)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AirPollutant> = object : Parcelable.Creator<AirPollutant> {
            override fun createFromParcel(source: Parcel): AirPollutant = AirPollutant(source)
            override fun newArray(size: Int): Array<AirPollutant?> = arrayOfNulls(size)
        }
    }
}
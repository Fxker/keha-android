package com.sansiri.homeservice.ui.mailbox.history

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.mailbox.MailBoxActivity
import com.sansiri.homeservice.ui.mailbox.detail.MailBoxDetailActivity
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import java.util.*

class MailBoxHistoryActivity : BaseActivity<MailBoxHistoryContract.View, MailBoxHistoryContract.Presenter>(), MailBoxHistoryContract.View{
    override var mPresenter: MailBoxHistoryContract.Presenter = MailBoxHistoryPresenter()

    val mAdapter = SectionedRecyclerViewAdapter()
    var mHome: Hut? = null

    companion object {
        val HOME = "HOME"
        val TITLE = "TITLE"
        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, MailBoxHistoryActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.mailbox_history))

        mHome = intent?.getParcelableExtra<Hut>(HOME)

        with(list) {
            setHasFixedSize(true)
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int) {
                    mPresenter.fetchData(mHome?.unitId ?: "", page)
                }
            })

            adapter = mAdapter
        }

        mPresenter.fetchData(mHome?.unitId ?: "", 0)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MAIL_BOX_HISTORY")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun bindData(list: List<MailBox>) {
        val mapSection = hashMapOf<Date, MutableList<MailBox>>()
        mAdapter.removeAllSections()

        list.forEach {
            val reportDate = it._createdAt?.resetTime()
            if(reportDate != null) {
                if (mapSection[reportDate] == null) {
                    mapSection.put(reportDate, mutableListOf(it))
                } else {
                    mapSection[reportDate]?.add(it)
                }
            }
        }

        mapSection.toSortedMap(compareByDescending<Date> { it }).forEach {
            val adapter = MailBoxHistorySectionAdapter(it.key.report(), it.value.sortedByDescending { it._createdAt }) { mail ->
                MailBoxDetailActivity.start(this, mHome!!, mail)
            }
            mAdapter.addSection(adapter)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        if (mAdapter.itemCount == 0) {
            progressBar.show()
        }
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        if (mAdapter.itemCount == 0) {
            textNoData.show()
        }
    }


    override fun showError(message: String) {
        alertError(message)
    }
}

package com.sansiri.homeservice.ui.automation.orvibo.login

import com.sansiri.homeservice.data.repository.orvibo.OrviboRepository
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import com.sansiri.homeservice.ui.base.CoroutineScopePresenter
import kotlinx.coroutines.*

class OrviboLoginPresenter(val orviboRepository: OrviboRepository) : CoroutineScopePresenter<OrviboLoginContract.View>(), OrviboLoginContract.Presenter {
    override fun login(unitId: String?, username: String, password: String) {
        mView?.showLoading()
        orviboRepository.login(username, password, {
            if (unitId != null) {
                launch {
                    orviboRepository.insertOrviboHome(
                            OrviboHome(unitId, username, orviboRepository.getPasswordMD5())
                    )
                    withContext(Dispatchers.Main) {
                        mView?.hideLoading()
                        mView?.launchOrviboHomeControl()
                    }
                }
            }
        }) {
            mView?.hideLoading()
            mView?.showError(it)
        }
    }
}
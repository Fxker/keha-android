package com.sansiri.homeservice.ui.homecare.create.detail

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.model.api.homecare.HomeCareRequest
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign

/**
 * Created by sansiri on 10/11/17.
 */
class HomeCareDetailPresenter : BasePresenterImpl<HomeCareDetailContract.View>(), HomeCareDetailContract.Presenter {
    private lateinit var mUnitId: String
    private var mId = ""
    private var mTypeId: Int = 0

    override fun init(unitId: String, typeId: Int, id: String) {
        mId = id
        mUnitId = unitId
        mTypeId = typeId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                api.getMe().observe().subscribe({
                    mView?.bindUser(it)
                }){}
            }
        }

        FirebaseRemoteConfigManager().getHomeCareNotice { notice ->
            notice?.let { mView?.showNotice(it) }
        }
    }

    override fun submit() {
        val detail = mView?.getDetail()
        val name = mView?.getName()
        val email = mView?.getEmail()
        val contact = mView?.getContact()

        if (name.isNullOrEmpty() || detail.isNullOrEmpty() || email.isNullOrEmpty() || contact.isNullOrEmpty()) {
            mView?.showAlertToEnterDetail()
            return
        }

        mView?.prepareBitmapToUploadIfExist()

    }

    override fun uploadImage(images: List<Pair<String, ByteArray>>) {
        val imagePath = mutableListOf<String>()
        updateProgress(imagePath.size, images.size)
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                val paths = mutableListOf<String>()
                updateProgress(paths.size, images.size)

                images.forEach { image ->
                    val filename = image.first
                    val data = image.second

                    mCompositeDisposable += api.createFileToUpload(filename).observe().subscribe({
                        val downloadUrl = it.downloadUrl ?: ""
                        val uploadUrl = it.uploadUrl ?: ""
                        ApiRepositoryProvider.provideApiUnAuthRepository().uploadFile(uploadUrl, data).observe().subscribe {
                            paths.add(downloadUrl)
                            val isFinish = updateProgress(paths.size, images.size)
                            if (isFinish) {
                                createRequest(paths)
                            }
                        }?.let { mCompositeDisposable += it }
                    }) {
                        mView?.showError(it.showHttpError())
                    }
                }
            } else {
                mView?.tryAgain()
                mView?.showError("Network Error")
            }
        }
    }

    override fun createRequest(imagePath: List<String>) {
        val detail = mView?.getDetail()
        val name = mView?.getName()
        val contact = mView?.getContact()
        val email = mView?.getEmail()

        val requestItem = HomeCareRequest.HomeCareRequestItem(mTypeId, detail, imagePath)
        val homeCareRequest = HomeCareRequest(name, contact, email, listOf(requestItem))

        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showSubmitProgress()
                if (!mUnitId.isEmpty()) {
                    api.submitHomeCare(mUnitId, homeCareRequest).observe().subscribe({
                        mView?.showSuccess()
                    }) {
                        mView?.showError(it.showHttpError())
                        mView?.tryAgain()
                    }
                } else {
                    mView?.tryAgain()
                }
            } else {
                mView?.tryAgain()
            }
        }
    }

    private fun updateProgress(progress: Int, max: Int): Boolean {
        mView?.showUploadProgress(progress, max)
        if (progress == max) {
            return true
        }
        return false
    }

}

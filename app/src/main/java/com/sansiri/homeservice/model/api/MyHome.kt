package com.sansiri.homeservice.model.api

data class MyHome(
		val unitType: String? = null,
		val createdAt: String? = null,
		val landArea: Double? = null,
		val address: String? = null,
		val unitTypeDescription: String? = null,
		val lng: Double? = null,
		val streetAddress: String? = null,
		val unitCode: String? = null,
		val projectName: String? = null,
		val lat: Double? = null,
		val updatedAt: String? = null
)

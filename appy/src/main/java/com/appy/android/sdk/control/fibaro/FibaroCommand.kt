package com.appy.android.sdk.control.fibaro

import com.appy.android.sdk.control.APCommand

/**
 * Created by Francisco Barrios 15/11/2018.
 */
enum class FibaroCommand : APCommand {

    ON_OFF {
        override fun action(): String {
            return "ON_OFF"
        }
    },
    TEMP {
        override fun action(): String {
            return "TEMP"
        }
    },
    FAN_SPEED {
        override fun action(): String {
            return "FAN_SPEED"
        }
    };
}
package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.appy.android.sdk.control.daikin.*
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.ToggleLayout
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import kotlinx.android.synthetic.main.view_home_automation_air_condition_v2.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class AirConditionViewHolder(val view: View, val overlayColor: Int, val itemClick: (HomeAutomation, Any) -> Unit) : LifecycleViewHolder(view) {

    private val availableDaikinTemp: ArrayList<Pair<Int, Any>> = arrayListOf(
            Pair(18, DaikinTemperatureCommand.TEMP_18),
            Pair(19, DaikinTemperatureCommand.TEMP_19),
            Pair(20, DaikinTemperatureCommand.TEMP_20),
            Pair(21, DaikinTemperatureCommand.TEMP_21),
            Pair(22, DaikinTemperatureCommand.TEMP_22),
            Pair(23, DaikinTemperatureCommand.TEMP_23),
            Pair(24, DaikinTemperatureCommand.TEMP_24),
            Pair(25, DaikinTemperatureCommand.TEMP_25),
            Pair(26, DaikinTemperatureCommand.TEMP_26),
            Pair(27, DaikinTemperatureCommand.TEMP_27),
            Pair(28, DaikinTemperatureCommand.TEMP_28),
            Pair(29, DaikinTemperatureCommand.TEMP_29),
            Pair(30, DaikinTemperatureCommand.TEMP_30)
    )

    private var selectedTempIterator: MutableListIterator<Pair<Int, Any>>? = null

    fun bind(data: HomeAutomation) {
        if (data is AppyHomeAutomation) {
            val control = data.apControl
            if (control is APDaikin) {

                selectedTempIterator = availableDaikinTemp.listIterator(5)

                with(view) {
                    textDegree.text = "${selectedTempIterator?.next()?.first}°"

                    buttonOnOff.setOnCheckedChangeListener(object : ToggleLayout.OnCheckedChangeListener {
                        override fun onCheckedChanged(isChecked: Boolean) {
                            if (isChecked) {
                                textButtonOnOff.text = context.getString(R.string.on)
                                itemClick(data, DaikinOnOffCommand.ON)
                            } else {
                                textButtonOnOff.text = context.getString(R.string.off)
                                itemClick(data, DaikinOnOffCommand.Off)
                            }
                        }
                    })
                    buttonOnOff.addRelatedChild(imageButtonOnOff, textButtonOnOff)

                    buttonDecrease.setOnClickListener {
                        if (selectedTempIterator != null) {

                            if (selectedTempIterator!!.hasPrevious()) {
                                selectedTempIterator?.previous()
                            }

                            if (selectedTempIterator!!.hasPrevious()) {
                                selectedTempIterator?.previous()?.let { temp ->
                                    textDegree.text = "${temp.first}°"
                                    itemClick(data, temp.second)
                                }
                                selectedTempIterator?.next()
                            } else {
                                selectedTempIterator?.next()?.let { temp ->
                                    textDegree.text = "${temp.first}°"
                                    itemClick(data, temp.second)
                                }
                            }
                        }
                    }

                    buttonIncrease.setOnClickListener {
                        if (selectedTempIterator != null && selectedTempIterator!!.hasNext()) {
                            selectedTempIterator?.next()?.let { temp ->
                                textDegree.text = "${temp.first}°"
                                itemClick(data, temp.second)
                            }
                        }
                    }

                    toggleGroupFan.setOnItemSelectedListener { position ->
                        val command = when (position) {
                            0 -> DaikinFanSpeedCommand.LOW
                            1 -> DaikinFanSpeedCommand.MEDIUM
                            else -> DaikinFanSpeedCommand.HIGH
                        }

                        itemClick(data, command)
                    }

                    toggleGroupSwing.setOnItemSelectedListener {
                        val command = when (position) {
                            0 -> DaikinFanDirectionCommand.DIRECTION_0
                            1 -> DaikinFanDirectionCommand.DIRECTION_1
                            2 -> DaikinFanDirectionCommand.DIRECTION_2
                            3 -> DaikinFanDirectionCommand.DIRECTION_3
                            else -> DaikinFanDirectionCommand.SWING
                        }

                        itemClick(data, command)
                    }

                    toggleGroupMode.setOnItemSelectedListener {
                        val command = when (position) {
                            0 -> DaikinOperationModeCommand.COOL
                            1 -> DaikinOperationModeCommand.FAN
                            else -> DaikinOperationModeCommand.DRY
                        }

                        itemClick(data, command)
                    }
                }
            }
        }
    }


}
package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by sansiri on 2/26/18.
 */
data class Preview(
        @SerializedName("xxlarge")
        val image: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(image)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Preview> = object : Parcelable.Creator<Preview> {
            override fun createFromParcel(source: Parcel): Preview = Preview(source)
            override fun newArray(size: Int): Array<Preview?> = arrayOfNulls(size)
        }
    }
}
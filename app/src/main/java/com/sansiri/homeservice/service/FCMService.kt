package com.sansiri.homeservice.service

import com.sansiri.homeservice.data.network.RetrofitFactory
import com.sansiri.homeservice.model.api.fcm.FCMBody
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface FCMService {
    @POST("send")
    fun sendMessage(@Header("Authorization") idToken: String, @Body body: FCMBody): Observable<ResponseBody>

    companion object Factory {
        fun create(): FCMService {
            return RetrofitFactory.instanceFCM.create(FCMService::class.java)
        }
    }
}
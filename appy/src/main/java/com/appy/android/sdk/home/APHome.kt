package com.appy.android.sdk.home

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.room.APRoom
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
class APHome : Parcelable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("alternate_title")
    var alternateTitle: String? = null

    @SerializedName("rooms")
    var rooms: List<APRoom>? = listOf()

    constructor()

    constructor(source: Parcel) : this() {
        id = source.readString()
        title = source.readString()
        alternateTitle = source.readString()
        rooms = source.createTypedArrayList(APRoom)
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(title)
        dest.writeString(alternateTitle)
        dest.writeTypedList(rooms)

    }

    companion object CREATOR : Parcelable.Creator<APHome> {
        override fun createFromParcel(parcel: Parcel): APHome {
            return APHome(parcel)
        }

        override fun newArray(size: Int): Array<APHome?> {
            return arrayOfNulls(size)
        }
    }

}
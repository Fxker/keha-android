package com.sansiri.homeservice.model.api.meter.water

import android.os.Parcelable
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class SmartWaterMeterOverAll(
        val average: Summarize? = null,
        val today: Summarize? = null,
        val histories: List<Summarize>? = null,
        val graph: List<SmartWaterMeterDetail>? = null,
        val updatedAt: String? = null
): Parcelable {

	@Parcelize
	data class Summarize(
			val dateType: String? = null,
			val liter: Double,
			val costEstimate: Double,
			val title: String? = null,
			val timestamp: String? = null
	): Parcelable {

		companion object DateType{
			val DATE = "DATE"
			val WEEK = "WEEK"
			val MONTH = "MONTH"
		}

		val displayLiter: String
			get() {
				return liter.toCurrencyFormat()
			}

		val _timestamp: Date?
			get() {
				return try {
					if (this.timestamp == null) return null

					DateTime(this.timestamp).toDate()
				} catch (e: ParseException) {
					null
				}
			}

		val displayCostEstimate: String
			get() {
				return "${costEstimate.toCurrencyFormat()}฿"
			}
	}

	val _updatedAt: Date?
		get() {
			return try {
				if (this.updatedAt == null) return null

				DateTime(this.updatedAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}

}

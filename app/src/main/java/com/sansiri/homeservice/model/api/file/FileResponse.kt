package vc.siriventures.hsa.admin.model.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FileResponse(
        val downloadUrl: String? = null,
        val uploadUrl: String? = null
) : Parcelable
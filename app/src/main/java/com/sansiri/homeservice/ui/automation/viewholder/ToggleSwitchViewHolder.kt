package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.appy.android.sdk.control.light.LightCommand
import com.bumptech.glide.Glide
//import com.homemate.sdk.model.HMDevice
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
//import com.sansiri.homeservice.model.automation.OrviboHomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_icon_and_title.view.*
import kotlinx.android.synthetic.main.view_home_automation_toggle_switch.view.*
import vc.siriventures.bathome.model.BAtHomeComponent

/**
 * Created by sansiri on 10/9/17.
 */
class ToggleSwitchViewHolder(val view: View, val itemClick: (HomeAutomation, Any) -> Unit) : LifecycleViewHolder(view) {
    fun bind(data: HomeAutomation) {
        with(view) {
            toggleLayout.addRelatedChild(imageLogo, textTitle)
            toggleLayout.isChecked = false
            textTitle.text = data.title
            imageLogo.setImageResource(data.icon)

            if (data is AppyHomeAutomation) {
                toggleLayout.isChecked = data.action == LightCommand.ON.ordinal
                data.apControl?.icon?.let {
                    Glide.with(this).loadCache(it, imageLogo)
                }

                toggleLayout.setOnClickListener {
                    if (data.apControl != null) {
                        if (data.action == LightCommand.OFF.ordinal) {
                            data.action = LightCommand.ON.ordinal
                            itemClick(data, data.action)
                        } else {
                            data.action = LightCommand.OFF.ordinal
                            itemClick(data, data.action)
                        }
                    }
                }
            } else if (data is BAtHomeAutomation) {
                toggleLayout.isChecked = data.component?.status == BAtHomeAutomation.ACTION_ON

                toggleLayout.setOnClickListener {
                    if (data.component != null) {
                        if (data.component.status == BAtHomeAutomation.ACTION_ON) {
                            data.component.status = BAtHomeAutomation.ACTION_OFF
                            itemClick(data, data.action)
                        } else {
                            data.component.status = BAtHomeAutomation.ACTION_ON
                            itemClick(data, data.action)
                        }
                    }
                }
            }
//            else if (data is OrviboHomeAutomation && data.orviboDevice is HMDevice) {
//                if (data.isOnOffSwitch() || data.isToggleSwitch()) {
//                    toggleLayout.isChecked = data.orviboDevice.status?.value1 == 0
//
//                    toggleLayout.setOnClickListener {
//                        if (toggleLayout.isChecked) {
//                            itemClick(data, 1)
//                        } else {
//                            itemClick(data, 0)
//                        }
//                    }
//                }
//            }
        }
    }
}
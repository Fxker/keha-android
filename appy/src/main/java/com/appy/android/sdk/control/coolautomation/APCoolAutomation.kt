package com.appy.android.sdk.control.coolautomation

import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
class APCoolAutomation : APControl() {
    var mActions: List<APActionCoolAutomation>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionCoolAutomation()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                CoolAutomationCommand.OFF.action() -> {
                    action.action = CoolAutomationCommand.ON
                }
                CoolAutomationCommand.ON.action() -> {
                    action.action = CoolAutomationCommand.ON
                }
                CoolAutomationCommand.MODE_COOL.action() -> {
                    action.action = CoolAutomationCommand.MODE_COOL
                }
                CoolAutomationCommand.MODE_FAN.action() -> {
                    action.action = CoolAutomationCommand.MODE_FAN
                }
                CoolAutomationCommand.TEMPERATURE.action() -> {
                    action.action = CoolAutomationCommand.TEMPERATURE
                }
                CoolAutomationCommand.FAN_SPEED.action() -> {
                    action.action = CoolAutomationCommand.FAN_SPEED
                }
                CoolAutomationCommand.FAN_SWING.action() -> {
                    action.action = CoolAutomationCommand.FAN_SWING
                }
            }

            action
        }
    }
}
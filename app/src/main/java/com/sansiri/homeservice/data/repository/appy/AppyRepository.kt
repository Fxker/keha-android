package com.sansiri.homeservice.data.repository.appy

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.daikin.APDaikin
import com.appy.android.sdk.control.daikin.APDaikinParameter
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.appy.android.sdk.home.APHome
import com.appy.android.sdk.home.HomesApiResult
import com.appy.android.sdk.room.APRoom
import com.appy.android.sdk.room.RoomApiResult
import com.appy.android.sdk.status.blind.StatusBlindApiResult
import com.appy.android.sdk.status.daikin.StatusDaikinApiResult
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import com.appy.android.sdk.status.light.StatusLightApiResult
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import io.reactivex.Observable

interface AppyRepository {
    fun setProjectId(projectId: String, pin: String?)
    fun getDedicatedProjectId(projectId: String): String
    fun getDedicatedUnitId(unitId: String): String
    fun setLocalMode(isLocalMode: Boolean)
    fun getHomes(onSuccess: (HomesApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun getRooms(unitId: String): Observable<List<APRoom>>
    fun getRooms(home: APHome): Observable<List<APRoom>>
    fun getControl(unitId: String, roomId: String): Observable<List<APControl>>
    fun getControl(home: APHome, room: APRoom, onSuccess: (RoomApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun getControl(unitId: String, roomId: String, onSuccess: (RoomApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun checkStatusLight(homeId: String, light: APLight, onSuccess: (StatusLightApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun checkStatusToggleSwitch(homeId: String, light: APToggleSwitch, onSuccess: (StatusToggleSwitchApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun checkStatusDimmer(homeId: String, dimmer: APDimmer, onSuccess: (StatusDimmerApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun checkStatusBlind(homeId: String, blind: APBlind, onSuccess: (StatusBlindApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun commandLight(unitId: String, lightId: String, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandLight(home: APHome, light: APLight, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandToggleSwitch(home: APHome, toggle: APToggleSwitch, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandBlind(home: APHome, blind: APBlind, commandNumber: Int, delay: Int = 0, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandBlind(home: APHome, blind: APBlind, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandDimmer(unitId: String, dimmerId: String, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandDimmer(home: APHome, dimmer: APDimmer, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandScene(home: APHome, scene: APScenario, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandRemoteControl(home: APHome, remoteControl: APRemoteControl, actionRemoteControl: APActionRemoteControl, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun commandRemoteControl(home: APHome, remoteControl: APRemoteControl, actionRemoteControl: APActionRemoteControl, param: String, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun checkStatusDaikin(home: APHome, device: APDaikin, onSuccess: (StatusDaikinApiResult) -> Unit, onFailed: (Throwable) -> Unit)
    fun commandDaikin(home: APHome, device: APDaikin, param: APDaikinParameter, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit)
    fun setPin(pin: String)
}
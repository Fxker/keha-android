package com.sansiri.homeservice.ui.notification

import com.sansiri.homeservice.data.repository.notification.NotificationRepository
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NotificationPresenter(val notificationRepository: NotificationRepository) : BasePresenterImpl<NotificationContract.View>(), NotificationContract.Presenter {
    override var notifications = notificationRepository.getNotifications()

    override fun start() {
    }

    override fun destroy() {
    }

    override fun handleNotification(notifications: List<PushNotification>?) {
        if (!notifications.isNullOrEmpty()) {
            mView?.bindData(notifications)
        } else {
            mView?.showNoData()
        }
    }

    override fun onNotificationClicked(notification: PushNotification) {
        GlobalScope.launch {
            notificationRepository.markAsRead(notification._id)
        }
        mView?.launchNotificationAction(notification)
    }

    override fun deleteAll() {
        GlobalScope.launch {
            notificationRepository.deleteAll()
        }
    }
}
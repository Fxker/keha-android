package com.sansiri.homeservice.model.api

import org.joda.time.DateTime
import retrofit2.http.GET
import java.text.ParseException
import java.util.*

data class Invoice(
        val customerFullName: String? = null,
        val loanType: Int,
        val startMonth: Int? = null,
        val startYear: Int? = null,
        val customerCode: String? = null,
        val invoiceAmount: Double,
        val invoiceDate: String? = null,
        val endYear: Int? = null,
        val invoiceDue: String? = null,
        val loanDescription: String? = null,
        val invoiceNo: String? = null,
        val endMonth: Int? = null,
        val roundYear: Int? = null,
        val roundMonth: Int? = null
) {
    val _invoiceDue: Date?
        get() {
            return try {
                if (this.invoiceDue == null) return null

                DateTime(this.invoiceDue).toDate()
            } catch (e: ParseException) {
                null
            }
        }

    companion object {
        val COMMON_FACTOR = 3
        val COMMON = 4
        val WATER = 5
        val INSURANCE = 11
    }
}


package com.sansiri.homeservice.ui.downpayment.payment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.ui.epayment.options.EPaymentOptionsFragment
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.*
import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.activity_downpayment_epayment.*
import kotlinx.android.synthetic.main.activity_downpayment_epayment.textTitle
import kotlinx.android.synthetic.main.view_payment_button.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.textColor
import org.koin.androidx.scope.currentScope
import java.lang.Exception
import android.view.ViewTreeObserver
import com.sansiri.homeservice.model.Hut


class DownpaymentEPaymentActivity : BaseV2Activity<DownpaymentEPaymentContract.View, DownpaymentEPaymentContract.Presenter>(), DownpaymentEPaymentContract.View, EPaymentOptionsFragment.OnFragmentInteractionListener {
    override val mPresenter: DownpaymentEPaymentContract.Presenter by currentScope.inject()
    private var mHome: Hut? = null
    private var mDownPaymentItem: DownpaymentItem? = null
    private var mDialogLoading: ProgressDialog? = null

    companion object {
        const val PAYMENT = "PAYMENT"
        const val HOME = "HOME"

        fun start(activity: Activity, home: Hut, downPaymentItem: DownpaymentItem) {
            val intent = Intent(activity, DownpaymentEPaymentActivity::class.java)
            intent.putExtra(PAYMENT, downPaymentItem)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_downpayment_epayment)

        setBackToolbar(getString(R.string.payment))

        mDownPaymentItem = intent.getParcelableExtra<DownpaymentItem>(PAYMENT)
        mHome = intent.getParcelableExtra<Hut>(HOME)

        textTitle.text = mDownPaymentItem?.title
        textCost.text = mDownPaymentItem?.cost?.toCurrencyFormat()
        textDate.text = mDownPaymentItem?.date?.report()

        if (mDownPaymentItem != null) {
            mPresenter.init(mDownPaymentItem!!)
        }

        // need for view complete to render before view capture
        root.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                root.viewTreeObserver.removeOnGlobalLayoutListener(this)
                mPresenter.start()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("DOWNPAYMENT_EPAYMENT")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showPaymentOptions(downPaymentItem: DownpaymentItem) {
        downPaymentItem.paymentChannels?.let {
            replaceFragment(EPaymentOptionsFragment.newInstance(
                    downPaymentItem.paymentChannels ?: listOf(),
                    PaymentGatewayData(
                            paymentTitle = downPaymentItem.title,
                            refCode1 = downPaymentItem.reference1,
                            refCode2 = downPaymentItem.reference2,
                            amount = downPaymentItem.cost
                    ),
                    layoutDetail.capture()?.toByteArray(),
                    downPaymentItem.paymentBarcode != null,
                    "DOWNPAYMENT"
            ), false
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showError(message: String) {
        alertError(message)
    }

    override fun onBarcodeClicked() {
        sendEvent("DOWNPAYMENT_EPAYMENT", "CLICK", "BARCODE")
        if (mHome != null && mDownPaymentItem != null)
            DownpaymentEPaymentBarcodeActivity.start(this, mHome!!, mDownPaymentItem!!)
    }

    override fun onQRClicked() {
        sendEvent("DOWNPAYMENT_EPAYMENT", "CLICK", "QR")
        if (mHome != null && mDownPaymentItem != null)
            DownpaymentEPaymentQRActivity.start(this, mHome!!, mDownPaymentItem!!)
    }
}

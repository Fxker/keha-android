package com.sansiri.homeservice.ui.register.signup

import android.util.Patterns
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/17/17.
 */
class SignUpPresenter : BasePresenterImpl<SignUpContract.View>(), SignUpContract.Presenter {
    override fun start() {

    }

    override fun destroy() {
    }

    override fun validate(email: String, emailConfirm: String, password: String, passwordConfirm: String) {
        var isPass = true
        if (email.isBlank()) {
            mView?.emailError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        } else {
            if (email != emailConfirm) {
                mView?.emailConfirmError(mView?.getStringFromRes(R.string.please_match_your_confirmation_email)
                        ?: "")
                isPass = false
            }
        }

        if (emailConfirm.isBlank()) {
            mView?.emailConfirmError(mView?.getStringFromRes(R.string.please_fill_out_this_field)
                    ?: "")
            isPass = false
        }

        if (passwordConfirm.isBlank()) {
            mView?.passwordConfirmError(mView?.getStringFromRes(R.string.please_fill_out_this_field)
                    ?: "")
            isPass = false
        }

        if (!isEmailValid(email)) {
            mView?.emailError(mView?.getStringFromRes(R.string.please_enter_a_valid_email) ?: "")
            isPass = false
        }

        if (password.isBlank()) {
            mView?.passwordError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        } else {
            if (!isPasswordValid(password)) {
                mView?.passwordError(mView?.getStringFromRes(R.string.password_must_be_at_least)
                        ?: "")
                isPass = false
            } else {
                if (password != passwordConfirm) {
                    mView?.passwordConfirmError(mView?.getStringFromRes(R.string.please_match_your_confirmation_password)
                            ?: "")
                    isPass = false
                }
            }
        }

        if (isPass) {
            signUp(email, password)
        }
    }

    override fun signUp(email: String, password: String) {
        mView?.showLoading()
        FirebaseAuthManager.signUp(email, password, {
            mView?.hideLoading()
            mView?.nextScreen()
        }) {
            mView?.hideLoading()
            mView?.showError(it.localizedMessage)
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 6
    }
}
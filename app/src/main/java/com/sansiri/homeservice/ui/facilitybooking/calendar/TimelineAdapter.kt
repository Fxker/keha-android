package com.sansiri.homeservice.ui.facilitybooking.calendar

import android.opengl.Visibility
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Timeline
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.reportTime
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.view_summary_card.view.*
import kotlinx.android.synthetic.main.view_timeline.view.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class TimelineAdapter(val createBookingClick: (Calendar, Calendar) -> Unit, val myBookClick: (Timeline) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<TimelineAdapter.ViewHolder>() {
    val mData = mutableListOf<Timeline>()

    companion object {
        val TIME_MIN = 0
        val TIME_MAX = 60
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(mData[position]) {
            holder?.bind(this)
            if (isNewEventClick) {
                holder?.addNewEvent()
            } else {
                holder?.removeNewEvent()
            }

            if (isRender) {
                holder?.renderBlock(this)
            } else if (disable) {
                if (position == 0) {
                    holder?.disable(title)
                } else {
                    holder?.disable()
                }
            } else {
                holder?.hideRenderBlock()
            }
        }
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_timeline), { position ->
            val startTime = mData.getOrNull(position)?.time
            val endTime = mData.getOrNull(position + 1)?.time
            if (startTime != null && endTime != null) {
                createBookingClick(startTime, endTime)
            }
        },
                myBookClick,
                object : EventClick {
                    override fun onNewEventClicked(position: Int) {
                        if (position < mData.size - 1) {
                            selectNewEvent(position)
                        }
                    }
                })
    }

    fun setData(timeline: MutableList<Timeline>) {
        mData.clear()
        mData.addAll(timeline)
        notifyDataSetChanged()
    }

    fun clearData() {
        mData.clear()
    }

    fun addData(timeline: Timeline) {
        mData.add(timeline)
        notifyDataSetChanged()
    }

    fun myBook(idBooking: String, startTime: Calendar, endTime: Calendar, title: String) {
        book(idBooking, startTime, endTime, title, true)
    }

    fun book(idBooking: String, startTime: Calendar, endTime: Calendar, title: String = "Booked", isMe: Boolean = false) {
        if (mData.isEmpty()) return

        clearNewEventDialog()
        var slotStart = 0
        var slotEnd = 0
        for ((index, value) in mData.withIndex()) {
            if (startTime.before(value.time)) {
                slotStart = index - 1
                break
            }
        }
        for ((index, value) in mData.withIndex()) {
            if (endTime == value.time || endTime.before(value.time)) {
                slotEnd = index - 1
                break
            }
        }


        if (slotStart >= 0 && slotEnd >= 0) {
            if (slotStart == slotEnd) {
                //Calculate start time in slot
                val startTimeOfSlotInMillis = mData[slotStart].time.timeInMillis
                val minuteToStart = TimeUnit.MILLISECONDS.toMinutes(startTime.timeInMillis - startTimeOfSlotInMillis)
                //Calculate end time in slot
                val minuteToEnd = TimeUnit.MILLISECONDS.toMinutes(endTime.timeInMillis - startTimeOfSlotInMillis)

                addBookingBlockToSlot(idBooking, slotStart, minuteToStart.toInt(), minuteToEnd.toInt(), false, isMe, title)
            } else {
                for (i in slotStart..slotEnd) {
                    if (i == slotStart) {
                        val startTimeOfSlotInMillis = mData[slotStart].time.timeInMillis
                        val minuteToStart = TimeUnit.MILLISECONDS.toMinutes(startTime.timeInMillis - startTimeOfSlotInMillis)
                        addBookingBlockToSlot(idBooking, i, minuteToStart.toInt(), TimelineAdapter.TIME_MAX, false, isMe, title)
                    } else if (i == slotEnd) {
                        val startTimeOfSlotInMillis = mData[slotEnd].time.timeInMillis
                        val minuteToEnd = TimeUnit.MILLISECONDS.toMinutes(endTime.timeInMillis - startTimeOfSlotInMillis)
                        addBookingBlockToSlot(idBooking, i, TimelineAdapter.TIME_MIN, minuteToEnd.toInt(), true, isMe)
                    } else {
                        addBookingBlockToSlot(idBooking, i, TimelineAdapter.TIME_MIN, TimelineAdapter.TIME_MAX, true, isMe)
                    }
                }
            }
        }
    }

    private fun addBookingBlockToSlot(idBooking: String, position: Int, startPosition: Int, endPosition: Int, hideLine: Boolean = false, isMe: Boolean = false, title: String? = null) {
        with(mData[position]) {
            this.idBooking = idBooking
            this.startPosition = startPosition
            this.endPosition = endPosition
            this.isRender = true
            this.hideLine = hideLine
            this.title = title
            this.isMe = isMe
        }

        notifyItemChanged(position)
    }

    fun selectNewEvent(position: Int) {
        for ((index, value) in mData.withIndex()) {
            value.isNewEventClick = position == index
        }
        notifyDataSetChanged()
    }

    fun clearNewEventDialog() {
        mData.forEach {
            it.isNewEventClick = false
        }
        notifyDataSetChanged()
    }

    interface EventClick {
        fun onNewEventClicked(position: Int)
    }

    class ViewHolder(val view: View, val createNewClick: (Int) -> Unit, val myBookClick: (Timeline) -> Unit, private val onEventClicked: EventClick) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(time: Timeline) {
            with(view) {
                viewDisable.hide()
                textTime.text = time.time.reportTime()
                if (!time.isRender) {
                    panelClickable.setOnClickListener {
                        // itemClick(layoutPosition)
                        if (time.isNewEventClick) {
                            createNewClick(layoutPosition)
                        } else {
                            onEventClicked.onNewEventClicked(layoutPosition)
                        }
                    }
                } else {
                    if (time.isMe) {
                        firstQuartile.setOnClickListener {
                            myBookClick(time)
                        }
                    }
                }
            }
        }


        fun addNewEvent() {
            with(view) {
                firstQuartile.hide()
                panelClickable.setBackgroundColor(resources.getColor(R.color.colorAccent))
                panelClickable.textNote.text = context.getString(R.string.tap_again_to_book)
                textNote.setTextColor(resources.getColor(R.color.textLight))
            }
        }

        fun removeNewEvent() {
            with(view) {
                panelClickable.setBackgroundColor(resources.getColor(android.R.color.transparent))
                textNote.text = ""
            }
        }

        fun hideRenderBlock() {
            view.firstQuartile.hide()
        }

        fun renderBlock(timeline: Timeline) {

            when (timeline.startPosition) {
                in TimelineAdapter.TIME_MIN..TimelineAdapter.TIME_MAX -> {
                    view.firstQuartile.visibility = View.VISIBLE
                    if (timeline.title != null) {
                        view.textFirstQuartile.text = timeline.title
                        view.textFirstQuartile.visibility = View.VISIBLE
                    }
                    if (timeline.isMe) {
                        view.firstQuartile.setBackgroundColor(view.context.resources.getColor(R.color.colorSecondary))
                        view.textFirstQuartile.setTextColor(view.context.resources.getColor(R.color.textLight))
                    }
                }
            }

            when (timeline.endPosition) {
                in TimelineAdapter.TIME_MIN..TimelineAdapter.TIME_MAX -> {
                    view.firstQuartile.visibility = View.VISIBLE
                    if (timeline.isMe) {
                        view.firstQuartile.setBackgroundColor(view.context.resources.getColor(R.color.colorSecondary))
                        view.textFirstQuartile.setTextColor(view.context.resources.getColor(R.color.textLight))
                    }
                }
            }

            if (timeline.hideLine) {
                view.line.visibility = View.INVISIBLE
            }
        }

        fun disable(textHint: String? = null) {
            view.viewDisable.show()
            textHint?.let {
                view.textHint.show()
                view.textHint.text = textHint
            }
        }
    }
}
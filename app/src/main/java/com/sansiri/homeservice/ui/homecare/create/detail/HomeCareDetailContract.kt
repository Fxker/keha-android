package com.sansiri.homeservice.ui.homecare.create.detail

import android.graphics.Bitmap
import android.net.Uri
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by oakraw on 10/2/2017 AD.
 */
interface HomeCareDetailContract {
    interface Presenter: BasePresenter<View> {
        fun init(unitId: String, typeId: Int, id: String)
        fun submit()
        fun uploadImage(images: List<Pair<String, ByteArray>>)
        fun createRequest(imagePath: List<String>)
        fun fetch()
    }

    interface View: BaseView {
        fun getDetail(): String
        fun showUploadProgress(progress: Int, max: Int)
        fun showSubmitProgress()
        fun tryAgain()
        fun showSuccess()
        fun showAlertToEnterDetail()
        fun hideUploadProgress()
        fun prepareBitmapToUploadIfExist()
        fun getContact(): String
        fun getEmail(): String
        fun getName(): String
        fun bindUser(profile: Profile?)
        fun showNotice(notice: String)
    }
}
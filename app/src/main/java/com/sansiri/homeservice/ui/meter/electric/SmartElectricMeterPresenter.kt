package com.sansiri.homeservice.ui.meter.electric

import com.sansiri.homeservice.data.repository.meter.SmartMeterRepository
import com.sansiri.homeservice.ui.base.CoroutineScopePresenter
import com.sansiri.homeservice.util.toISO8601
import java.util.*

class SmartElectricMeterPresenter(val repository: SmartMeterRepository) : CoroutineScopePresenter<SmartElectricMeterContract.View>(), SmartElectricMeterContract.Presenter {
    var mUnitId: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        repository.destroy()
    }

    override fun fetch() {
        repository.destroy()
        if (mUnitId != null) {
            mView?.attachFetchingObserver(
                    repository.getSmartElectricMeterLast(mUnitId!!, "default", Calendar.getInstance().time.toISO8601()),
                    repository.getSmartElectricMeterSummary(mUnitId!!, "default", Calendar.getInstance().time.toISO8601())
            )
        }
    }
}
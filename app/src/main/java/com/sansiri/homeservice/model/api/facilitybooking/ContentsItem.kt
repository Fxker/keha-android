package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ContentsItem(
		@SerializedName("short_description")
		val shortDescription: String? = null,
		val terms: String? = null,
		val language: Language? = null,
		val description: String? = null,
		val title: String? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.readParcelable<Language>(Language::class.java.classLoader),
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(shortDescription)
		writeString(terms)
		writeParcelable(language, 0)
		writeString(description)
		writeString(title)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<ContentsItem> = object : Parcelable.Creator<ContentsItem> {
			override fun createFromParcel(source: Parcel): ContentsItem = ContentsItem(source)
			override fun newArray(size: Int): Array<ContentsItem?> = arrayOfNulls(size)
		}
	}
}




package com.sansiri.homeservice.ui.epayment.options

import com.sansiri.homeservice.data.repository.payment.PaymentRepository
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import retrofit2.HttpException
import java.util.ArrayList

class EPaymentOptionsPresenter(val paymentRepository: PaymentRepository) : BasePresenterImpl<EPaymentOptionsContract.View>(), EPaymentOptionsContract.Presenter {
    private var mPaymentGatewayData: PaymentGatewayData? = null
    private var mPaymentChannels: List<PaymentChannel>? = null

    override fun init(paymentChannels: List<PaymentChannel>?, paymentGatewayData: PaymentGatewayData?) {
        mPaymentGatewayData = paymentGatewayData
        mPaymentChannels = paymentChannels
    }

    override fun start() {
        mPaymentChannels?.let { mView?.showOtherPaymentOptions(it) }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun takeAction(paymentChannel: PaymentChannel) {
        if (paymentChannel.command != null) {
            if (paymentChannel.getAction() == PaymentChannel.Action.PHONE_NUMBER_REQUIRED) {
                if (mPaymentGatewayData != null) {
                    mView?.launchPhoneVerify(mPaymentGatewayData!!, paymentChannel)
                }
            } else {
                purchase(paymentChannel)
            }
        }
    }

    private fun purchase(paymentChannel: PaymentChannel) {
        mView?.showLoading()
        mCompositeDisposable.add(
                paymentRepository.requestPayment(PaymentRequest(paymentCommand = paymentChannel.command))
                        .observe()
                        .subscribe({ response ->
                            mView?.hideLoading()
                            handleResponse(response, paymentChannel)
                        }) { error ->
                            mView?.hideLoading()
                            if (error is HttpException) {
                                when (error.code()) {
                                    204 -> {

                                    }
                                    else -> {
                                        mView?.showError(error.showHttpError())
                                    }
                                }
                            }
                        }
        )
    }

    override fun handleResponse(response: PaymentResponse, paymentChannel: PaymentChannel?) {
        if (response.popupMessage != null) {
            mView?.showPopup(response.popupTitle ?: "", response.popupMessage) {
                purchaseAction(response, paymentChannel)
            }
        } else {
            purchaseAction(response, paymentChannel)
        }
    }

    private fun purchaseAction(response: PaymentResponse, paymentChannel: PaymentChannel? = null) {
        when (response.actionType) {
            PaymentResponse.EXTERNAL_BROWSER -> {
                mView?.launchWeb(response.externalUri ?: "")
            }
            PaymentResponse.SDK -> {
                mView?.launch2C2P(response, paymentChannel)
            }
        }
    }
}
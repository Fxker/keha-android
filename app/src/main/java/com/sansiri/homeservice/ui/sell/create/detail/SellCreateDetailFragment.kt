package com.sansiri.homeservice.ui.sell.create.detail


import android.content.Context
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView

import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.SellOption
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.sell.SellPresenter
import com.sansiri.homeservice.ui.sell.create.SellCreateActivity
import com.sansiri.homeservice.ui.sell.create.ownerinfo.SellCreateOwnerInfoFragment
import com.sansiri.homeservice.util.TextWatcherExtend
import com.sansiri.homeservice.util.hide
import kotlinx.android.synthetic.main.fragment_sell_create_detail.*

class SellCreateDetailFragment : BaseFragment<SellCreateDetailContract.View, SellCreateDetailContract.Presenter>(), SellCreateDetailContract.View {
    override var mPresenter: SellCreateDetailContract.Presenter = SellCreateDetailPresenter()
    private val sellOption = SellOption(null, false)
    private val monthlyRentOption = SellOption(null, false)
    private val yearlyRentOption = SellOption(null, false)

    companion object {
        val SELL_OPTION = "SELL_OPTION"
        val MONTHLY_OPTION = "MONTHLY_OPTION"
        val YEARLY_OPTION = "YEARLY_OPTION"
        val IS_RENT = "IS_RENT"

        fun newInstance(isRent: Boolean = true): SellCreateDetailFragment {
            val fragment = SellCreateDetailFragment()
            val bundle = Bundle()
            bundle.putBoolean(IS_RENT, isRent)
            fragment.arguments = bundle
            return fragment
        }
    }

    private var isRent: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isRent = arguments?.getBoolean(IS_RENT, true) ?: false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sell_create_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initOption(panelSell, getString(R.string.sell), getString(R.string.cost), sellOption)
        initOption(panelMonthlyRent, getString(R.string.monthly_rent), getString(R.string.monthly_rent_cost), monthlyRentOption)
        initOption(panelYearlyRent, getString(R.string.yearly_rent), getString(R.string.yearly_rent_cost), yearlyRentOption)

        if (!isRent) {
            panelMonthlyRent.hide()
            panelYearlyRent.hide()
        }

        buttonNext.setOnClickListener {
            mPresenter.validate(sellOption, monthlyRentOption, yearlyRentOption)
        }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        (activity as SellCreateActivity).setToolbarTitle("ฝากขายและปล่อยเช่า")
    }

    private fun initOption(view: View, title: String, hint: String, option: SellOption) {
        view.findViewById<TextView>(R.id.textTitle).text = title

        view.findViewById<EditText>(R.id.editCost).hint = hint
        view.findViewById<CheckBox>(R.id.checkbox).setOnCheckedChangeListener { _, isChecked ->
            option.isEnabled = isChecked
        }

        val editCost = view.findViewById<EditText>(R.id.editCost)
        editCost.setText(option.cost)
        editCost.hint = hint
        editCost.addTextChangedListener(TextWatcherExtend() {
            option.cost = it
        })
    }

    override fun showError(message: String) {
        Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun launchNext() {
        view?.clearFocus()
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
        (activity as SellCreateActivity).selectScreen(SellCreateOwnerInfoFragment.newInstance(), true)
    }
}

package com.appy.android.code.base.data.rest.result

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
class AppyApiApiStatusResult {
    @SerializedName("deprecated")
    val deprecated: String? = "false"

    @SerializedName("deprecation_date")
    val deprecationDate: Date? = null

    @SerializedName("message")
    val message: String? = ""
}
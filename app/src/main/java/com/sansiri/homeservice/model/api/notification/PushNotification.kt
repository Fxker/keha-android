package com.sansiri.homeservice.model.api.notification

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by oakraw on 27/1/2018 AD.
 */

@Entity(tableName = "notification")
data class PushNotification (
        @PrimaryKey(autoGenerate = true)
        val _id: Int = 0,
        val title: String? = null,
        val message: String? = null,
        val type: String? = null,
        val createdAt: String? = null,
        val isRead: Boolean = false,
        @Embedded(prefix = "data_")
        val data: PushNotificationData? = null
)
package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface PaymentService {
    @POST("me/payments")
    fun requestPayment(@Body request: PaymentRequest): Observable<PaymentResponse>
}
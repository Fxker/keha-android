package com.sansiri.homeservice.component

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.*
import com.sansiri.homeservice.R

/**
 * Created by sansiri on 10/10/17.
 */
class ToggleGroupLayout : LinearLayout {
    var mColorOn = Color.BLUE
    var mColorOff = Color.GRAY
    private var mOnSelectedListener: ((index: Int) -> Unit)? = null

    constructor(context: Context) : super(context) {
    }


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)


    private fun initialize(context: Context, attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.ToggleLayout, 0, 0)
        mColorOn = a.getColor(R.styleable.ToggleLayout_onColor, Color.BLUE)
        mColorOff = a.getColor(R.styleable.ToggleLayout_offColor, Color.GRAY)

        a.recycle()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        for (i in 0 until childCount) {
            getChildAt(i).setOnClickListener {
                selectItem(i)
            }
        }
    }

    fun selectItem(position: Int) {
        for (i in 0 until childCount) {
            if (i == position) {
                renderOn(getChildAt(i))
                mOnSelectedListener?.invoke(i)
            } else {
                renderOff(getChildAt(i))
            }
        }
    }

    private fun renderOn(view: View) {
        if (view is ImageView) {
            view.setColorFilter(mColorOn)
        } else if (view is TextView) {
            view.setTextColor(mColorOn)
        }
    }

    private fun renderOff(view: View) {
        if (view is ImageView) {
            view.setColorFilter(mColorOff)
        } else if (view is TextView) {
            view.setTextColor(mColorOff)
        }
    }


    fun setOnItemSelectedListener(listener: (index: Int) -> Unit) {
        mOnSelectedListener = listener
    }

}
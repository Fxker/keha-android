package com.appy.android.sdk.control.ac

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionAirConditioner : APAction<AirConditionerCommand>, Parcelable {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionAirConditioner> {
        override fun createFromParcel(parcel: Parcel): APActionAirConditioner {
            return APActionAirConditioner(parcel)
        }

        override fun newArray(size: Int): Array<APActionAirConditioner?> {
            return arrayOfNulls(size)
        }
    }
}
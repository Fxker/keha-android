package com.sansiri.homeservice.ui.svvh.registration

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

interface SVVHRegistrationContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut, customer: SVVHRegisterCustomer?)
        fun setFirstName(firstName: String)
        fun setLastName(lastName: String)
        fun setIDCard(idCard: String)
        fun setTel(tel: String)
        fun setBirthDate(calendar: Calendar)
        fun setGender(gender: String)
        fun submit()
    }

    interface View : BaseView {
        fun bindData(customer: SVVHRegisterCustomer)
        fun showFirstNameError()
        fun showLastNameError()
        fun showTelError()
        fun showGenderError()
        fun showBirthDateError()
        fun showLoading()
        fun hideLoading()
        fun redirectUrl(url: String)
    }
}
package com.sansiri.homeservice.ui.scheme

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sansiri.homeservice.R
import android.app.ActivityManager
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService
import android.util.Log
import com.sansiri.homeservice.ui.main.MainActivity


class SchemeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        run breaker@{
            val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
            val tasks = activityManager.getRunningTasks(10)
            for (task in tasks) {
                if (task.topActivity.className == localClassName) {
                    if (task.numActivities > 1) {
                        finish()
                        return@breaker
                    }
                }
            }
            MainActivity.start(this)
            finish()
        }
    }
}

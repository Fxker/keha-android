package com.sansiri.homeservice.ui.base

import android.Manifest
import android.annotation.SuppressLint
import android.telecom.Call
import com.sansiri.homeservice.ui.gallery.onRequestPermissionsResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
abstract class PermissionFragment<V : BaseView, T : BasePresenter<V>> : BaseV2Fragment<V, T>() {
    interface Callback {
        fun onPermissionAllowed(permission: String)
    }

    protected var callback: Callback? = null

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    fun requestPermissionReadWrite() {
        callback?.onPermissionAllowed(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    fun onPermissionReadWriteDenied() {
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    fun onPermissionReadWriteNeverAskAgain() {
    }
}
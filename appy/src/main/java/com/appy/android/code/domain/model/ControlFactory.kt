package com.appy.android.code.domain.model

import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.code.helpers.ClassHelper
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.ac.APAirConditioner
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.coolautomation.APCoolAutomation
import com.appy.android.sdk.control.daikin.APDaikin
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.outlet.APOutlet
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch

class ControlFactory {

    companion object {

        @JvmStatic
        fun create(controlJsonModel: ControlJsonModel) : APControl? {
            when (ClassHelper.getClass(controlJsonModel.klass!!)) {
                ClassHelper.CLASS_DAIKIN_INDOOR -> {
                    return convertDaikin(controlJsonModel)
                }
                else -> {
                    when (controlJsonModel.type) {
                        "AC" -> {
                            return convertAC(controlJsonModel)
                        }
                        "Blind" -> {
                            return convertBlind(controlJsonModel)
                        }
                        "Cool" -> {
                            return convertCoolMasterNet(controlJsonModel)
                        }
                        "Dimmer" -> {
                            return convertDimmer(controlJsonModel)
                        }
                        "Light" -> {
                            return convertLight(controlJsonModel)
                        }
                        "Outlet" -> {
                            return convertOutlet(controlJsonModel)
                        }
                        "Remote Control" -> {
                            return convertRemoteControl(controlJsonModel)
                        }
                        "Scene" -> {
                            return convertScenario(controlJsonModel)
                        }
                        "Switch" -> {
                            return convertSwitch(controlJsonModel)
                        }
                    }
                }
            }

            return null
        }

        private fun convertAC(controlJsonModel: ControlJsonModel): APAirConditioner {
            val apAction = APAirConditioner()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertBlind(controlJsonModel: ControlJsonModel): APBlind {
            val apAction = APBlind()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertCoolMasterNet(controlJsonModel: ControlJsonModel): APCoolAutomation {
            val apAction = APCoolAutomation()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertDimmer(controlJsonModel: ControlJsonModel): APDimmer {
            val apAction = APDimmer()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertLight(controlJsonModel: ControlJsonModel): APLight {
            val apAction = APLight()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertOutlet(controlJsonModel: ControlJsonModel): APOutlet {
            val apAction = APOutlet()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertRemoteControl(controlJsonModel: ControlJsonModel): APRemoteControl {
            val apAction = APRemoteControl()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertScenario(controlJsonModel: ControlJsonModel): APScenario {
            val apAction = APScenario()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertSwitch(controlJsonModel: ControlJsonModel): APToggleSwitch {
            val apAction = APToggleSwitch()
            apAction.init(controlJsonModel)

            return apAction
        }

        private fun convertDaikin(controlJsonModel: ControlJsonModel): APDaikin {
            val apAction = APDaikin()
            apAction.init(controlJsonModel)

            return apAction
        }
    }

}
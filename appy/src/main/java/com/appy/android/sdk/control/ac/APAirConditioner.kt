package com.appy.android.sdk.control.ac

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APAirConditioner : APControl, Parcelable {
    var mActions: List<APActionAirConditioner>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionAirConditioner)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionAirConditioner()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                AirConditionerCommand.ON.action() -> {
                    action.action = AirConditionerCommand.ON
                }
                AirConditionerCommand.OFF.action() -> {
                    action.action = AirConditionerCommand.OFF
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APAirConditioner> {
        const val TYPE = "APAirConditioner"

        override fun createFromParcel(parcel: Parcel): APAirConditioner {
            val type = parcel.readString()
            return APAirConditioner(parcel)
        }

        override fun newArray(size: Int): Array<APAirConditioner?> {
            return arrayOfNulls(size)
        }
    }
}
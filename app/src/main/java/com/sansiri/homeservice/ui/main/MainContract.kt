package com.sansiri.homeservice.ui.main

import com.sansiri.homeservice.model.api.PopUp
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MainContract {
    interface Presenter : BasePresenter<View> {
        fun fetchPopUp()
    }

    interface View : BaseView {
        fun showPopUps(popups: List<PopUp>)
        fun setPrivilegeButton(iconUrl: String)
    }
}
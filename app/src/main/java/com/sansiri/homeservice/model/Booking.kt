package com.sansiri.homeservice.model

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * Created by sansiri on 11/2/17.
 */
data class Booking(val idBooking: String, var startTime: Calendar, var endTime: Calendar, var note: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readSerializable() as Calendar,
            source.readSerializable() as Calendar,
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(idBooking)
        writeSerializable(startTime)
        writeSerializable(endTime)
        writeString(note)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Booking> = object : Parcelable.Creator<Booking> {
            override fun createFromParcel(source: Parcel): Booking = Booking(source)
            override fun newArray(size: Int): Array<Booking?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.automation.orvibo.login
//
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.view.MenuItem
//import com.homemate.sdk.HomeMateSDK
//import com.sansiri.homeservice.R
//import com.sansiri.homeservice.model.Home
//import com.sansiri.homeservice.model.Hut
//import com.sansiri.homeservice.ui.automation.orvibo.home.OrviboHomeActivity
//import com.sansiri.homeservice.ui.base.BaseV2Activity
//import com.sansiri.homeservice.util.alertError
//import com.sansiri.homeservice.util.hide
//import com.sansiri.homeservice.util.show
//import com.sansiri.homeservice.util.snackError
//import kotlinx.android.synthetic.main.activity_orvibo_login.*
//import org.koin.android.ext.android.inject
//import org.koin.androidx.scope.currentScope
//
//class OrviboLoginActivity : BaseV2Activity<OrviboLoginContract.View, OrviboLoginContract.Presenter>(), OrviboLoginContract.View {
//    override val mPresenter: OrviboLoginContract.Presenter by inject()
//    private var mHome: Hut? = null
//
//    companion object {
//        const val HOME = "HOME"
//        fun start(activity: Activity, home: Hut?) {
//            val intent = Intent(activity, OrviboLoginActivity::class.java)
//            intent.putExtra(HOME, home)
//            activity.startActivity(intent)
//        }
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_orvibo_login)
//
//        mHome = intent.getParcelableExtra(HOME)
//
//        buttonContinue.setOnClickListener {
//            mPresenter.login(mHome?.unitId, editUsername.text.toString(), editPassword.text.toString())
//        }
//
//        buttonBack.setOnClickListener {
//            finish()
//        }
//
//        buttonForgotPassword.hide()
//    }
//
//    override fun onResume() {
//        super.onResume()
//        sendScreenView("ORVIBO_HOME_AUTOMATION_LOGIN")
//    }
//
//    override fun launchOrviboHomeControl() {
//        OrviboHomeActivity.start(this, mHome)
//        finish()
//    }
//
//    override fun showLoading() {
//        buttonContinue.hide()
//        progressBar.show()
//    }
//
//    override fun hideLoading() {
//        buttonContinue.show()
//        progressBar.hide()
//    }
//
//    override fun showError(message: String) {
//        alertError(message)
//    }
//}

package com.appy.android.code.base.data.rest.result

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 12/7/2561.
 */
class AppyApiDataErrorResult {
    @SerializedName("error")
    val error: String? = ""
}
package com.sansiri.homeservice.ui.chat

import androidx.recyclerview.widget.RecyclerView
import android.text.util.Linkify
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.TimeShow

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_chat_box_other.view.*
import me.saket.bettermovementmethod.BetterLinkMovementMethod

/**
 * Created by oakraw on 28/10/2017 AD.
 */
class OtherMessageViewHolder(val view: View, val senderProfileImage: String, val onLinkClicked: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(message: ChatMessage) {
        with(view) {
            textMessage.text = message.text

            BetterLinkMovementMethod
                    .linkify(Linkify.ALL, textMessage).setOnLinkClickListener { textView, url ->
                        onLinkClicked(url)
                        true
                    }

            textTime.text = TimeShow.show(message.createdAt)
            if (senderProfileImage.isNotEmpty()) {
                Glide.with(this).loadCache(senderProfileImage, imageProfile)
            } else {
                imageProfile.setImageResource(R.drawable.placeholder_contact)
            }
        }
    }
}
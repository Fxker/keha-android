package com.sansiri.homeservice.util

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment

import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.widget.Toast
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import android.R.attr.path
import com.sansiri.homeservice.R


class SaveUtil {

    fun saveImage(context: Context, finalBitmap: Bitmap): File {
        val folderHomeService = File(Environment.getExternalStorageDirectory().toString(), "HomeService")
        if (!folderHomeService.exists()) {
            folderHomeService.mkdir()
        }

        val file = File(folderHomeService, "hsa_${System.currentTimeMillis()}.jpg")
        if (!file.exists()) {
            file.createNewFile()
        }

        val outputStream = FileOutputStream(file)
        finalBitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream)
        outputStream.close()
        Toast.makeText(context, context.getString(R.string.image_saved), Toast.LENGTH_SHORT).show()
        MediaScannerConnection.scanFile(context, arrayOf<String>(file.path), null
        ) { path, uri ->
        }

        return file
    }
}

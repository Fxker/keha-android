package com.sansiri.homeservice.ui.meter.water

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.Observer
import com.github.mikephil.charting.data.Entry
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterDetail
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.meter.LineChartHelper
import com.sansiri.homeservice.ui.meter.water.detail.SmartWaterMeterDetailActivity
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_smart_water.*
import kotlinx.android.synthetic.main.view_smart_electric_meter_history_item.view.*
import kotlinx.android.synthetic.main.view_smart_water_meter_history_item.view.*
import kotlinx.android.synthetic.main.view_smart_water_meter_history_item.view.textSubTitle
import kotlinx.android.synthetic.main.view_smart_water_meter_history_item.view.textTitle
import org.jetbrains.anko.indeterminateProgressDialog
import org.koin.androidx.scope.currentScope


class SmartWaterMeterActivity : BaseV2Activity<SmartWaterMeterContract.View, SmartWaterMeterContract.Presenter>(), SmartWaterMeterContract.View {
    private var progressDailog: ProgressDialog? = null
    override val mPresenter: SmartWaterMeterContract.Presenter by currentScope.inject()
    private var mUnitId: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val TITLE = "TITLE"

        fun start(activity: Activity, unitId: String, title: String?) {
            val intent = Intent(activity, SmartWaterMeterActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smart_water)

        mUnitId = intent.getStringExtra(SmartParkingActivity.UNIT_ID)
        val title = intent.getStringExtra(SmartParkingActivity.TITLE)

        mPresenter.init(mUnitId ?: "")
        mPresenter.start()

        mPresenter.smartWaterMeter?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    showLoading()
//                    if (resource.data != null && resource.data!!.isNotEmpty()) {
//                        resource.data?.let { bindData(it) }
//                        showLoading()
//                    } else {
//                        showLoading()
//                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let {
                        bindData(it)
                    }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })

        textGraphUnit.text = "(${getString(R.string.litre)})"
        textUnit.text = "(${getString(R.string.litre)})"

        buttonBack.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SMART_WATER_METER")
    }

    fun bindData(overall: SmartWaterMeterOverAll) {
        textTodayAmount.text = overall.today?.displayLiter

        val updatedAt = overall._updatedAt
        if (updatedAt != null) {
            textLatestUpdated.text = "${getString(R.string.latest_update)} ${updatedAt.reportTime()}\n${updatedAt.report()}"
        }

        titleAverageAmount.hide()
        textAverageAmount.hide()
        overall.average?.displayLiter?.let {
            titleAverageAmount.show()
            textAverageAmount.show()
            textAverageAmount.text = overall.average?.displayLiter
        }


        overall.histories?.forEach { history ->
            val view = LayoutInflater.from(this@SmartWaterMeterActivity).inflate(R.layout.view_smart_electric_meter_history_item, containerHistory, false)
            view.textTitle.text = history.title
            view.textSubTitle.text = history.displayLiter
            view.textCost.text = history.displayCostEstimate
            view.setOnClickListener {
                SmartWaterMeterDetailActivity.start(this, history, mUnitId ?: "")
            }
            containerHistory.addView(view)

        }

        renderChart(overall.graph)
    }

    private fun renderChart(graphData: List<SmartWaterMeterDetail>?) {
        if (graphData.isNullOrEmpty()) {
            chart.hide()
            return
        } else {
            chart.show()
        }

        val entries = graphData.map { data ->
            Entry(data._timestamp?.hours?.toFloat() ?: 0f, data.volumeUsage.toFloat())
        }

        LineChartHelper.LineChartBuilder(this, chart, R.layout.view_smart_water_graph_value_preview)
                .createDataSet(
                        entries,
                        "",
                        resources.getColor(R.color.colorBlueLineChart),
                        resources.getDrawable(R.drawable.graph_blue_fade),
                        resources.getColor(R.color.colorHighlight)
                )
                .build()
    }

    override fun showLoading() {
        progressDailog = indeterminateProgressDialog(getString(R.string.loading))
    }

    override fun hideLoading() {
        progressDailog?.dismiss()
    }

    override fun showError(message: String) {
        snackError(message)
    }
}

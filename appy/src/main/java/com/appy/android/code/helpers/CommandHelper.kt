package com.appy.android.code.helpers

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.ac.AirConditionerCommand
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.blind.BlindCommand
import com.appy.android.sdk.control.coolautomation.APCoolAutomation
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.dimmer.DimmerCommand
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.light.LightCommand
import com.appy.android.sdk.control.outlet.APOutlet
import com.appy.android.sdk.control.outlet.OutletCommand
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.scenario.ScenarioCommand
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch

/**
 * Created by ssa-dev-04 on 15/1/2561.
 */
internal class CommandHelper {
    companion object {

        @JvmStatic
        fun getCommandMessage(apControl: APControl, airConditionerCommand: AirConditionerCommand): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apControl)
            message += "*"
            message += getWHAT(airConditionerCommand)
            message += "*"
            message += getWHERE(apControl.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apControl: APControl, blindCommand: BlindCommand): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apControl)
            message += "*"
            message += getWHAT(blindCommand)
            message += "*"
            message += getWHERE(apControl.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apControl: APControl, dimmerCommand: DimmerCommand): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apControl)
            message += "*"
            message += getWHAT(dimmerCommand)
            message += "*"
            message += getWHERE(apControl.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apControl: APControl, lightCommand: LightCommand): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apControl)
            message += "*"
            message += getWHAT(lightCommand)
            message += "*"
            message += getWHERE(apControl.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apOutlet: APOutlet, outletCommand: OutletCommand): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apOutlet)
            message += "*"
            message += getWHAT(outletCommand)
            message += "*"
            message += getWHERE(apOutlet.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apScenario: APScenario): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apScenario)
            message += "*"
            message += getWHAT(apScenario.sceneAddress!!)
            message += "*"
            message += getWHERE(apScenario.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getCommandMessage(apToggleSwitch: APToggleSwitch): String {
            // *WHO*WHAT*WHERE##
            var message = "*"
            message += getWHO(apToggleSwitch)
            message += "*"
            message += getWHAT(apToggleSwitch.sceneAddress!!)
            message += "*"
            message += getWHERE(apToggleSwitch.circuitAddress!!)
            message += "##"
            return message
        }

        @JvmStatic
        fun getStatusMessage(apControl: APControl): String {
            // *#WHO*WHERE##
            var message = "*#"
            message += getWHO(apControl)
            message += "*"
            message += getWHERE(apControl.circuitAddress!!)
            message += "##"

            return message
        }

        @JvmStatic
        fun getRespondStatus(respond: String) {
            // *WHO*WHAT*WHERE##
            // *1*1*6##
        }

        private fun getWHO(apControl: APControl): String {
            return if (apControl is APLight
                    || apControl is APOutlet
                    || apControl is APCoolAutomation
                    || apControl is APDimmer) {
                "1"
            } else if (apControl is APBlind) {
                "2"
            } else if (apControl is APScenario) {
                "0"
            } else {
                ""
            }
        }

        private fun getWHAT(lightCommand: LightCommand): String {
            return when (lightCommand) {
                LightCommand.ON -> "1"
                LightCommand.OFF -> "0"
            }
        }

        private fun getWHAT(outletCommand: OutletCommand): String {
            return when (outletCommand) {
                OutletCommand.ON -> "1"
                OutletCommand.OFF -> "0"
            }
        }

        private fun getWHAT(airConditionerCommand: AirConditionerCommand): String {
            return when (airConditionerCommand) {
                AirConditionerCommand.ON -> "1"
                AirConditionerCommand.OFF -> "0"
            }
        }

        private fun getWHAT(dimmerCommand: DimmerCommand): String {
            return when (dimmerCommand) {
                DimmerCommand.OFF -> "0"
                DimmerCommand.ON -> "1"
                DimmerCommand.DIMMER10 -> ""
                DimmerCommand.DIMMER20 -> "2"
                DimmerCommand.DIMMER30 -> "3"
                DimmerCommand.DIMMER40 -> "4"
                DimmerCommand.DIMMER50 -> "5"
                DimmerCommand.DIMMER60 -> "6"
                DimmerCommand.DIMMER70 -> "7"
                DimmerCommand.DIMMER80 -> "8"
                DimmerCommand.DIMMER90 -> "9"
                DimmerCommand.DIMMER100 -> "1"
            }
        }

        private fun getWHAT(blindCommand: BlindCommand): String {
            return when (blindCommand) {
                BlindCommand.ON -> "1"
                BlindCommand.OFF -> "2"
                BlindCommand.STOP -> "0"
            }
        }

        private fun getWHAT(scenarioCommand: ScenarioCommand): String {
            return when (scenarioCommand) {
                ScenarioCommand.ACTIVATE -> "1"
            }
        }

        private fun getWHAT(address: String): String {
            val whatLongArray = HashIDHelper.decode(address)
            return whatLongArray[0].toString()
        }

        private fun getWHERE(address: String): String {
            val whereLongArray = HashIDHelper.decode(address)
            return whereLongArray[0].toString()
        }
    }
}
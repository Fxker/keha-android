package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.code.base.data.rest.result.AppyApiResult
import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.JsonModel.fibaro.DeviceJsonModel
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.GenericNetworkService
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.status.dimmer.APStatusDimmer
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import com.appy.android.sdk.status.toggleswitch.APStatusToggleSwitch
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import com.google.gson.Gson
import io.reactivex.Observable
import java.text.MessageFormat
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
internal class GetFibaroDeviceStatusInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<AppyApiResult<*>, GetFibaroDeviceStatusForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetFibaroDeviceStatusForm): Observable<AppyApiResult<*>> {
        val url = MessageFormat.format("http://{0}/api/devices/{1}", params.getHost(), params.getControlId())
        return restClient
            .createAPIService(GenericNetworkService::class.java, params.getUsername()!!, params.getPassword()!!)
            .get(url, HashMap())
            .map {
                val jsonModel = Gson().fromJson(it.body().toString(), DeviceJsonModel::class.java)
                val status = if (params.control is APDimmer) {
                    val apiResult = StatusDimmerApiResult()
                    val value = jsonModel.properties.value.toInt()
                    val valueRound = Math.round(value / 10.0f)
                    apiResult.data = APStatusDimmer(params.control, valueRound.toString())

                    apiResult
                } else {
                    val apiResult = StatusToggleSwitchApiResult()
                    apiResult.data = APStatusToggleSwitch(params.control, if (jsonModel.properties.value.equals("false", true)) {
                        "0"
                    } else {
                        "1"
                    })

                    apiResult
                }

                status
            }
    }
}
package com.sansiri.homeservice.ui.pdf

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.io.File

/**
 * Created by sansiri on 2/9/18.
 */
interface PdfContract {
    interface Presenter : BasePresenter<View> {
        fun downloadPdfFromUrl(url: String)
        fun download()
        fun share()
    }

    interface View : BaseView {
        fun renderPdf(byte: ByteArray)
        fun createFile(byte: ByteArray, title: String)
        fun shareFile(url: String)
        fun showLoading()
        fun hideLoading()
    }
}
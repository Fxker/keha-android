package com.sansiri.homeservice.ui.automation.orvibo.control

//import android.app.Activity
//import android.content.Intent
//import android.content.SharedPreferences
//import android.os.Bundle
//import android.view.Menu
//import android.view.MenuItem
//import androidx.lifecycle.LiveData
//import androidx.lifecycle.Observer
//import com.google.android.flexbox.AlignItems
//import com.google.android.flexbox.FlexDirection
//import com.google.android.flexbox.FlexWrap
//import com.google.android.flexbox.FlexboxLayoutManager
//import com.homemate.sdk.model.HMDevice
//import com.homemate.sdk.model.HMScene
//import com.sansiri.homeservice.BuildConfig
//import com.sansiri.homeservice.R
//import com.sansiri.homeservice.data.database.PreferenceHelper
//import com.sansiri.homeservice.model.automation.AppyHomeAutomation
//import com.sansiri.homeservice.model.automation.HomeAutomation
//import com.sansiri.homeservice.data.database.PreferenceHelper.get
//import com.sansiri.homeservice.data.database.PreferenceHelper.set
//import com.sansiri.homeservice.model.automation.OrviboHomeAutomation
//import com.sansiri.homeservice.ui.automation.HomeAutomationAdapter
//import com.sansiri.homeservice.ui.base.BaseV2Activity
//import com.sansiri.homeservice.util.*
//import kotlinx.android.synthetic.main.activity_list.*
//import org.koin.android.ext.android.inject
//import org.koin.androidx.scope.currentScope
//import org.koin.core.KoinComponent
//import org.koin.core.parameter.parametersOf
//
//
//class OrviboHomeAutomationActivity : BaseV2Activity<OrviboHomeAutomationContract.View, OrviboHomeAutomationContract.Presenter>(), OrviboHomeAutomationContract.View, KoinComponent {
//    override val mPresenter: OrviboHomeAutomationContract.Presenter by inject()
//
//    private val mMenuAdapter = HomeAutomationAdapter(R.color.colorAccent) { homeAutomation, command ->
//        if (homeAutomation is OrviboHomeAutomation) {
//            when (homeAutomation.orviboDevice) {
//                is HMDevice -> {
//                    mPresenter.controlDevice(homeAutomation, command)
//                    sendEvent("ORVIBO_HOME_AUTOMATION", "COMMAND", homeAutomation.orviboDevice.type.toString())
//                }
//                is HMScene -> {
//                    mPresenter.controlScene(homeAutomation)
//                    sendEvent("ORVIBO_HOME_AUTOMATION", "COMMAND", "SCENE")
//                }
//            }
//        }
//
//    }
//
//
//    companion object {
//        val TITLE = "TITLE"
//        val ROOM_ID = "ROOM_ID"
//        fun start(activity: Activity, title: String, roomId: String) {
//            val intent = Intent(activity, OrviboHomeAutomationActivity::class.java)
//            intent.putExtra(TITLE, title)
//            intent.putExtra(ROOM_ID, roomId)
//            activity.startActivity(intent)
//        }
//    }
//
//
//    private lateinit var prefs: SharedPreferences
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_list)
//        setBackToolbar(intent?.getStringExtra(TITLE) ?: "")
//        prefs = PreferenceHelper.defaultPrefs(this)
//
//        val flexboxLayoutManager = FlexboxLayoutManager(this).apply {
//            flexWrap = FlexWrap.WRAP
//            flexDirection = FlexDirection.ROW
//            alignItems = AlignItems.STRETCH
//        }
//
//        list.apply {
//            layoutManager = flexboxLayoutManager
//            adapter = mMenuAdapter
//        }
//
//
//        val roomId = intent?.getStringExtra(ROOM_ID)
//        mPresenter.fetchData(roomId)
//        mPresenter.devicesLiveData.observe(this, Observer { devices ->
//            if (devices != null) {
//                mPresenter.onDeviceUpdated(devices)
//            }
//        })
//    }
//
//    override fun onResume() {
//        super.onResume()
//        sendScreenView("ORVIBO_HOME_AUTOMATION_HOME_CONTROL")
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // handle arrow click here
//        when (item.itemId) {
//            android.R.id.home -> onBackPressed()
//        }
//
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun bindData(controls: List<HomeAutomation>) {
//        mMenuAdapter.setData(controls)
//    }
//
//    override fun updateDevices() {
//        mMenuAdapter.notifyDataSetChanged()
//    }
//
//    override fun notifyItemChange(homeAutomation: OrviboHomeAutomation) {
//        mMenuAdapter.notifyItemChange(homeAutomation)
//    }
//
//    override fun showNoData() {
//    }
//
//    override fun showMessage(text: String) {
//        if (!BuildConfig.BUILD_TYPE.contains("release")) {
//            snack(text)
//        }
//    }
//
//    override fun showLoading() {
//        progressBar.show()
//    }
//
//    override fun hideLoading() {
//        progressBar.hide()
//    }
//
//    override fun showError(text: String) {
//        snackError(text)
//    }
//}

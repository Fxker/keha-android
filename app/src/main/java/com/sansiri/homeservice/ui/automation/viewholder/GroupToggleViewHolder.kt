package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import com.sansiri.homeservice.util.hide
import kotlinx.android.synthetic.main.view_chat_template_item.view.*
import kotlinx.android.synthetic.main.view_menu_home_automation_group_toggle.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class GroupToggleViewHolder(val view: View, val itemClick: (HomeAutomation, Any) -> Unit) : LifecycleViewHolder(view) {
    fun bind(data: HomeAutomation) {
        with(view) {
            if (data.title.isNotEmpty()) {
                textTitle.text = data.title
            } else {
                textTitle.setPadding(0, 0, 0, 0)
                textTitle.hide()
            }

            if (data is BAtHomeAutomation) {
                buttonOff.setOnClickListener {
                    itemClick(data, 0)
                }
                buttonOn.setOnClickListener {
                    itemClick(data, 1)
                }
            }
        }
    }
}
package com.sansiri.homeservice.model.api

data class VisitorAccessOptions(
        val accessOption: List<TextValue>? = null,
        val effectiveTime: List<TextValue>? = null
)

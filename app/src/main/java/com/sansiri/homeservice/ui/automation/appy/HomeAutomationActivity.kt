package com.sansiri.homeservice.ui.automation.appy

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.appy.android.sdk.control.daikin.APDaikinParameter
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.ui.automation.HomeAutomationAdapter
import com.sansiri.homeservice.ui.automation.appy.remotecontrol.HomeAutomationRemoteControlActivity
import com.sansiri.homeservice.ui.automation.appy.remotecontrol.ac.HomeAutomationACRemoteControlActivity
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf


class HomeAutomationActivity : BaseV2Activity<HomeAutomationContract.View, HomeAutomationContract.Presenter>(), HomeAutomationContract.View {

    private var id: String? = null
    override val mPresenter: HomeAutomationContract.Presenter by currentScope.inject() { parametersOf(id)}

    private var isLocalMode = false
    private val appyManager = AppyRepositoryImpl()
    private var menuActionMode: MenuItem? = null

    private val mMenuAdapter = HomeAutomationAdapter(R.color.colorAccent) { homeAutomation, command ->
        if (homeAutomation is AppyHomeAutomation) {
            when (command) {
                is Int -> {
                    sendEvent("APPY_HOME_AUTOMATION", "COMMAND", homeAutomation.apControl?.type)
                    mPresenter.action(homeAutomation.apControl!!, command)
                }
                is APDaikinParameter -> {
                    mPresenter.action(homeAutomation.apControl!!, command)
                }
            }
        }

    }


    companion object {
        val TITLE = "TITLE"
        val PROJECT_ID = "PROJECT_ID"
        val HOME_ID = "HOME_ID"
        val ROOM_ID = "ROOM_ID"
        fun start(activity: Activity, title: String, projectId: String, homeId: String, roomId: String) {
            val intent = Intent(activity, HomeAutomationActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(HOME_ID, homeId)
            intent.putExtra(ROOM_ID, roomId)
            activity.startActivity(intent)
        }
    }


    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent?.getStringExtra(TITLE) ?: "")
        prefs = PreferenceHelper.defaultPrefs(this)

        val flexboxLayoutManager = FlexboxLayoutManager(this).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            alignItems = AlignItems.STRETCH
        }

        list.apply {
            layoutManager = flexboxLayoutManager
            adapter = mMenuAdapter
        }


        val projectId = intent?.getStringExtra(PROJECT_ID)
        val homeId = intent?.getStringExtra(HOME_ID)
        val roomId = intent?.getStringExtra(ROOM_ID)
        mPresenter.fetchData(projectId, homeId, roomId)
    }


    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val checkable = menu.findItem(R.id.action_mode)
        isLocalMode = prefs[PreferenceHelper.HOME_AUTOMATION_LOCAL_MODE, false] ?: false
        checkable.isChecked = isLocalMode
        menuActionMode = menu.findItem(R.id.action_mode)
        menuActionMode?.apply {
            if (!isLocalMode) {
                appyManager.setLocalMode(false)
                setIcon(R.drawable.ic_outside_house)
            } else {
                appyManager.setLocalMode(true)
                setIcon(R.drawable.ic_in_house)
            }
        }
        menuActionMode?.isVisible = false
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home_automation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_mode -> {
                isLocalMode = !item.isChecked
                item.isChecked = isLocalMode
                if (!isLocalMode) {
                    appyManager.setLocalMode(false)
                    item.setIcon(R.drawable.ic_outside_house)
                } else {
                    appyManager.setLocalMode(true)
                    item.setIcon(R.drawable.ic_in_house)
                }
                prefs[PreferenceHelper.HOME_AUTOMATION_LOCAL_MODE] = isLocalMode
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showLocalModeButton() {
        menuActionMode?.isVisible = true
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("APPY_HOME_AUTOMATION")
    }

    override fun bindData(controls: List<HomeAutomation>) {
        mMenuAdapter.setData(controls)
    }

    override fun updateDevices() {
        mMenuAdapter.notifyDataSetChanged()
    }

    override fun launchRemoteControl(control: APRemoteControl, projectId: String, homeId: String) {
        if (control.klass == "Fibaro::RemoteControl") {
            HomeAutomationACRemoteControlActivity.start(this, control, projectId, homeId)
        } else {
            HomeAutomationRemoteControlActivity.start(this, control, projectId, homeId)
        }
    }

    override fun showMessage(text: String) {
        if (!BuildConfig.BUILD_TYPE.contains("release")) {
            snack(text)
        }

    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(text: String) {
        snackError(text)
    }
}

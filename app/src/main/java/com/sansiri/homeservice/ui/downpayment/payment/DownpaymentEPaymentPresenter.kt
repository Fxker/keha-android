package com.sansiri.homeservice.ui.downpayment.payment

import com.sansiri.homeservice.data.repository.payment.PaymentRepository
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import retrofit2.HttpException

class DownpaymentEPaymentPresenter() : BasePresenterImpl<DownpaymentEPaymentContract.View>(), DownpaymentEPaymentContract.Presenter {
    var mDownPaymentItem: DownpaymentItem? = null

    override fun init(downPaymentItem: DownpaymentItem) {
        mDownPaymentItem = downPaymentItem
    }

    override fun start() {
        mDownPaymentItem?.let { mView?.showPaymentOptions(it) }
    }

    override fun destroy() {
    }
}
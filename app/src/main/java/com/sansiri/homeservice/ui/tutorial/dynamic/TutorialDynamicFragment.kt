package com.sansiri.homeservice.ui.tutorial.dynamic

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.tutorial.TutorialPage
import com.sansiri.homeservice.util.setTextHexColor
import kotlinx.android.synthetic.main.fragment_tutorial.*

/**
 * Created by sansiri on 4/5/18.
 */
class TutorialDynamicFragment : Fragment() {
    companion object {
        val CONTENT = "CONTENT"
        fun newInstance(content: TutorialPage): TutorialDynamicFragment {
            val fragment = TutorialDynamicFragment()
            val bundle = Bundle()
            bundle.putParcelable(CONTENT, content)
            fragment.arguments = bundle

            return fragment
        }
    }

    private var mContent: TutorialPage? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContent = arguments?.getParcelable(CONTENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (mContent != null) {
            Glide.with(this).load(mContent!!.imageUrl).into(image)
            textDescription.text = mContent!!.details
            textDescription.setTextHexColor(mContent!!.contentColorRGB ?: "")
        }
    }
}
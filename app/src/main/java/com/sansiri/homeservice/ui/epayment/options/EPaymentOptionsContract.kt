package com.sansiri.homeservice.ui.epayment.options

import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface EPaymentOptionsContract {
    interface Presenter : BasePresenter<View> {
        fun takeAction(paymentChannel: PaymentChannel)
        fun init(paymentChannels: List<PaymentChannel>?, paymentGatewayData: PaymentGatewayData?)
        fun handleResponse(response: PaymentResponse, paymentChannel: PaymentChannel? = null)
    }

    interface View : BaseView {
        fun showOtherPaymentOptions(paymentChannels: List<PaymentChannel>)
        fun launchWeb(url: String)
        fun launchPhoneVerify(paymentGateWayData: PaymentGatewayData, paymentChannel: PaymentChannel)
        fun showLoading()
        fun hideLoading()
        fun showPopup(title: String, message: String, onSuccess: () -> Unit)
        fun launch2C2P(response: PaymentResponse, paymentChannel: PaymentChannel?)
    }
}
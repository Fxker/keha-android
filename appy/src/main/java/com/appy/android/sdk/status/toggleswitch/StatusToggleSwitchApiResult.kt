package com.appy.android.sdk.status.toggleswitch

import com.appy.android.code.base.data.rest.result.AppyApiResult

class StatusToggleSwitchApiResult : AppyApiResult<APStatusToggleSwitch>()
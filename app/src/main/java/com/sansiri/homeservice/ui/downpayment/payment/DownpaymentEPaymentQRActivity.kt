package com.sansiri.homeservice.ui.downpayment.payment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.ui.epayment.barcode.QRContract
import com.sansiri.homeservice.ui.epayment.barcode.QRPresenter
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_barcode.*
import com.sansiri.homeservice.ui.epayment.qr.QRActivity


class DownpaymentEPaymentQRActivity : QRActivity("DOWNPAYMENT_EPAYMENT_QR") {

    override var mPresenter: QRContract.Presenter = QRPresenter()
    var mDownPaymentItem: DownpaymentItem? = null
    var mHome: Hut? = null

    companion object {
        const val PAYMENT = "PAYMENT"
        const val HOME = "HOME"

        fun start(activity: Activity, home: Hut, downPaymentItem: DownpaymentItem) {
            val intent = Intent(activity, DownpaymentEPaymentQRActivity::class.java)
            intent.putExtra(PAYMENT, downPaymentItem)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buttonDownload.hide()

        mDownPaymentItem = intent.getParcelableExtra(DownpaymentEPaymentActivity.PAYMENT)
        mHome = intent.getParcelableExtra(DownpaymentEPaymentActivity.HOME)

        renderQRCode(mDownPaymentItem?.paymentBarcode)
    }

    override fun getDetailBitmap(): Bitmap = createDetailBitmap(this, mHome, mDownPaymentItem)

    override fun onResume() {
        super.onResume()
        sendScreenView("DOWNPAYMENT_EPAYMENT_QR")
    }

    override fun showError(message: String) {
    }
}

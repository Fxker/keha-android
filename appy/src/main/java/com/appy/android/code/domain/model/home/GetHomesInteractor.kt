package com.appy.android.code.domain.model.home

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.home.HomesApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal class GetHomesInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<HomesApiResult, HomesForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: HomesForm): Observable<HomesApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getHomes(params.headers, params.params)
    }
}
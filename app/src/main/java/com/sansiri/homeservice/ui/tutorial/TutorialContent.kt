package com.sansiri.homeservice.ui.tutorial

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by sansiri on 4/5/18.
 */
data class TutorialContent(val icon: Int, val description: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(icon)
        writeString(description)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TutorialContent> = object : Parcelable.Creator<TutorialContent> {
            override fun createFromParcel(source: Parcel): TutorialContent = TutorialContent(source)
            override fun newArray(size: Int): Array<TutorialContent?> = arrayOfNulls(size)
        }
    }
}
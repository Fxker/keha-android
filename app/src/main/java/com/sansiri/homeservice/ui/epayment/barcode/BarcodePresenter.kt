package com.sansiri.homeservice.ui.epayment.barcode

import com.sansiri.homeservice.ui.base.BasePresenterImpl

class BarcodePresenter : BasePresenterImpl<BarcodeContract.View>(), BarcodeContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }
}
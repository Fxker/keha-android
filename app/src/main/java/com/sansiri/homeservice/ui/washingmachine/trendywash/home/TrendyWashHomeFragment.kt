package com.sansiri.homeservice.ui.washingmachine.trendywash.home


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.scanner.QRScannerActivity
import com.sansiri.homeservice.ui.washingmachine.trendywash.TrendyWashMainActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_trendy_wash_home.*
import org.koin.androidx.scope.currentScope

class TrendyWashHomeFragment : BaseV2Fragment<TrendyWashHomeContract.View, TrendyWashHomeContract.Presenter>(), TrendyWashHomeContract.View {
    override val mPresenter: TrendyWashHomeContract.Presenter by currentScope.inject()

    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mTitle: String? = null
    val mAdapter = WashingMachineAdapter() { machine ->
        launchMachine(machine)
    }

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"
        const val TITLE = "TITLE"

        fun newInstance(unitId: String, userId: String, title: String?): TrendyWashHomeFragment {
            return TrendyWashHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(USER_ID, userId)
                    putString(TITLE, title)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mUserId = arguments?.getString(USER_ID)
        mTitle = arguments?.getString(TITLE)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRENDY_WASH_MAIN")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trendy_wash_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        val textToolbar = view.findViewById<TextView>(R.id.textToolbar)

        (activity as AppCompatActivity).apply {
            setSupportActionBar(toolbar)
            supportActionBar?.title = ""
            textToolbar.text = mTitle ?: "Trendy Wash"
            textToolbar.isAllCaps = false
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

        with(list) {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mAdapter
        }

        buttonAddMoney.setOnClickListener {
            (activity as TrendyWashMainActivity).launchAddMoney()
        }

        setHasOptionsMenu(true)

        mPresenter.init(mUnitId ?: "", mUserId ?: "")
        mPresenter.start()

        mPresenter.machines?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data != null && resource.data!!.isNotEmpty()) {
                        resource.data?.let { bindData(it) }
                        showLoading()
                    } else {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let { bindData(it) }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })

        mPresenter.creditInfo?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                }
                Resource.SUCCESS -> {
                    resource.data?.let { showCredit(it.remainCredits ?: "0") }
                }
                Resource.ERROR -> {
                    showError(resource.message ?: "Error")
                }
            }
        })

        if (!BuildConfig.BUILD_TYPE.contains("release")) {
            textToolbar.setOnClickListener {
                (activity as TrendyWashMainActivity).logout()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_qr, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_qr) {
            launchQRScanner()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == QRScannerActivity.CODE) {
            mPresenter.findMachineById(data?.getStringExtra(QRScannerActivity.DATA))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(machines: List<TrendyWashMachine>) {
        mAdapter.setData(machines)
    }

    override fun showCredit(remainCredits: String) {
        textBalance.text = remainCredits
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun launchQRScanner() {
        sendEvent("TRENDY_WASH_MAIN", "SCAN", "MACHINE_QR")
        QRScannerActivity.start(this)
    }

    override fun launchMachine(machine: TrendyWashMachine) {
        if (activity!! is TrendyWashMainActivity) {
            (activity as TrendyWashMainActivity).launchWashingMachine(machine)
        }
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    override fun showErrorNoTitle(message: String) {
        activity?.alertMessageError(message)
    }
}

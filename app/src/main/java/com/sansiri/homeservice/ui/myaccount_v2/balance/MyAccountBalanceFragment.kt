package com.sansiri.homeservice.ui.myaccount_v2.balance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.myaccount.BalanceV2
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_my_account_balance.*

class MyAccountBalanceFragment : BaseFragment<MyAccountBalanceContract.View, MyAccountBalanceContract.Presenter>(), MyAccountBalanceContract.View {
    override var mPresenter: MyAccountBalanceContract.Presenter = MyAccountBalancePresenter()
    private var mHome: Hut? = null

    companion object {
        const val HOME = "HOME"

        fun newInstance(home: Hut): MyAccountBalanceFragment {
            val fragment = MyAccountBalanceFragment()
            val bundle = Bundle().apply {
                putParcelable(HOME, home)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHome = arguments?.getParcelable(HOME)

    }

    val mAdapter = MyAccountBalanceAdapter(R.color.colorCancel) {

    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_account_balance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        with(listHistory) {
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
            adapter = mAdapter
            layoutManager = linearLayoutManager
        }

        scrollView.setOnScrollChangeListener(object : EndlessNestedScrollViewListener(linearLayoutManager) {
            override fun onLoadMore(currentPage: Int) {
                mPresenter.fetch(currentPage)
            }
        })

        mPresenter.init(mHome?.unitId ?: "")
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT_BALANCE")
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindBalance(balance: BalanceV2) {
        textBalance.text = balance.totalBalance.toCurrencyFormat()
    }

    override fun bindHistory(paymentHistory: List<PaymentHistory>) {
        titleHistory.show()
        listHistory.show()
        mAdapter.addData(paymentHistory)
    }

    override fun showBalanceError() {
        textBalance.text = "Error"
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

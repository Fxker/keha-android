package com.sansiri.homeservice.model.api

data class OnionShackDeviceStatus(
	val name: String? = null,
	val id: String? = null,
	val type: String? = null,
	val status: Int? = null
)

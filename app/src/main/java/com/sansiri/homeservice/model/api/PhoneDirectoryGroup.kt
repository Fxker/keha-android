package com.sansiri.homeservice.model.api

import android.provider.ContactsContract

data class PhoneDirectoryGroup(
		val nameTh: String? = null,
		val createdAt: String? = null,
		val sansiriProjectId: String? = null,
		val name: String? = null,
		val objectId: String? = null,
		val updatedAt: String? = null,
		val directories: List<PhoneDirectory> = listOf()
)

package com.sansiri.homeservice.ui.homecare.create.detail


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.adapter.MultiSelectPhotoAdapter
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.base.PhotoFragment
import com.sansiri.homeservice.ui.homecare.create.HomeCareCreateActivity
import com.sansiri.homeservice.util.*
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_home_care_detail.*
import kotlinx.android.synthetic.main.view_progress_status.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class HomeCareDetailFragment : PhotoFragment<HomeCareDetailContract.View, HomeCareDetailContract.Presenter>(), HomeCareDetailContract.View, IPickResult {
    override var mPresenter: HomeCareDetailContract.Presenter = HomeCareDetailPresenter()
    private var typeId: Int = 0


    companion object {
        val ID = "ID"
        fun newInstance(id: Int): Fragment {
            val fragment = HomeCareDetailFragment()
            val bundle = Bundle()
            bundle.putInt(ID, id)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        typeId = arguments?.getInt(ID, 0) ?: 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_home_care_detail)
    }

    private var mUnitObjectId: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter.maximumSelection = 18

        listPhoto.apply {
            mAdapter.setData(bitmaps)
            adapter = mAdapter
        }

        mUnitObjectId = (activity as HomeCareCreateActivity).unitObjectId ?: ""

        buttonCreate.setOnClickListener {
            sendEvent("HOMECARE", "CLICK", "HOMECARE_CREATE_REPAIR_REQUEST")
            mPresenter.submit()
        }

        buttonCreate.text = getString(R.string.send_home_care_request)

        val id = System.currentTimeMillis().toString()

        mPresenter.init(mUnitObjectId!!, typeId, id)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("HOMECARE_DETAIL")
    }

    override fun bindUser(profile: Profile?) {
        profile?.let {
            editTextName.setText(it.displayname ?: "${profile.firstname} ${profile.lastname}")
            editTextContact.setText(it.phoneNumbers?.get(0)?.tel)
            editTextEmail.setText(it.email)
        }
    }

    override fun prepareBitmapToUploadIfExist() {
        if (bitmaps.isNotEmpty()) {
            sendEvent("HOMECARE", "UPLOAD", "PHOTO")

            compressBitmapObservable.observe().subscribe({ imageToUpload ->
                mPresenter.uploadImage(imageToUpload)
            }) {
                showError(getString(R.string.error_fail_to_upload))
            }
        } else {
            mPresenter.createRequest(listOf())
        }
    }

    override fun showUploadProgress(progress: Int, max: Int) {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = "${getString(R.string.uploading)} ($progress/$max)"
    }

    override fun getDetail(): String = editTextDescription.text.toString()

    override fun getName(): String {
        return editTextName.text.toString()
    }

    override fun getContact(): String {
        return editTextContact.text.toString()
    }

    override fun getEmail(): String {
        return editTextEmail.text.toString()
    }

    override fun hideUploadProgress() {
        panelProgress.hide()
        buttonCreate.show()
        editTextDescription.isEnabled = true
    }

    override fun showSubmitProgress() {
        panelProgress.show()
        buttonCreate.hide()
        editTextDescription.isEnabled = false
        textProgress.text = getString(R.string.submitting)
    }

    override fun tryAgain() {
        editTextDescription.isEnabled = true
        buttonCreate.text = getString(R.string.try_again)
        panelProgress.hide()
        buttonCreate.show()
    }

    override fun showSuccess() {
        progressBar.hide()
        activity?.alert(getString(R.string.home_are_thank_you_message), getString(R.string.done)) {
            yesButton {
                val returnIntent = Intent()
                activity?.setResult(Activity.RESULT_OK, returnIntent)
                activity?.finish()
            }
        }?.apply {
            isCancelable = false
            show()
        }
        textProgress.text = getString(R.string.done)
    }

    override fun showNotice(notice: String) {
        textNotice.show()
        textNotice.text = notice
    }

    override fun showAlertToEnterDetail() {
        activity?.snack("Please fill in details")
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }


    override fun onDestroy() {
        (activity as HomeCareCreateActivity).setToolbarTitle(getString(R.string.repair_request))
        super.onDestroy()
    }
}
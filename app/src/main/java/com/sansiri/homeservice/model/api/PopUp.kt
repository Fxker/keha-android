package com.sansiri.homeservice.model.api

import android.os.Parcel
import android.os.Parcelable

data class PopUp(
        val actionType: String? = null,
        val createdAt: String? = null,
        val imageUrl: String? = null,
        val navigateToPage: String? = null,
        val title: String? = null,
        val objectId: String? = null,
        val url: String? = null,
        val updatedAt: String? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(actionType)
        writeString(createdAt)
        writeString(imageUrl)
        writeString(navigateToPage)
        writeString(title)
        writeString(objectId)
        writeString(url)
        writeString(updatedAt)
    }

    companion object {
        val EXTERNAL_BROWSER = "EXTERNAL_BROWSER"
        val DEEP_LINK = "DEEP_LINK"

        @JvmField
        val CREATOR: Parcelable.Creator<PopUp> = object : Parcelable.Creator<PopUp> {
            override fun createFromParcel(source: Parcel): PopUp = PopUp(source)
            override fun newArray(size: Int): Array<PopUp?> = arrayOfNulls(size)
        }
    }
}

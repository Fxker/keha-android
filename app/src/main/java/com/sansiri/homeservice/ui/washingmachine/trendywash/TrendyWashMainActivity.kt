package com.sansiri.homeservice.ui.washingmachine.trendywash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.TrendyWashAddMoneyFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.TrendyWashHomeFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine.TrendyWashMachineFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.login.TrendyWashLoginFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.registration.TrendyWashRegisterFragment
import com.sansiri.homeservice.util.addFragment
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.replaceFragment

class TrendyWashMainActivity : LocalizationActivity() {


    companion object {
        val HOME = "HOME"
        val MENU = "MENU"
        val CUSTOMER_ID = "CUSTOMER_ID"

        fun start(activity: Activity, home: Hut, menu: Menu) {
            val intent = Intent(activity, TrendyWashMainActivity::class.java)
            intent.putExtra(HOME, home)
            if (menu is DynamicMenu)
                intent.putExtra(MENU, menu)
            activity.startActivity(intent)
        }

        fun loggedIn(activity: Activity, customerId: String?) {
            val intent = Intent(activity, TrendyWashMainActivity::class.java)
            intent.putExtra(CUSTOMER_ID, customerId)
            activity.startActivity(intent)
        }
    }

    private var mHome: Hut? = null
    private var mMenu: DynamicMenu? = null
    private var mUserId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trendy_wash_main)
        mHome = intent.getParcelableExtra(HOME)
        mMenu = intent.getParcelableExtra(MENU)


        mUserId = PreferenceHelper.defaultPrefs(this)[PreferenceHelper.TREND_WASH_USER_ID]
        if (mUserId != null) {
            launchHome(mUserId!!)
        } else {
            clearCookie() // clear cookie in webview
            launchLogin()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        mUserId = intent?.getStringExtra(CUSTOMER_ID)
        if (mUserId != null) {
            PreferenceHelper.defaultPrefs(this)[PreferenceHelper.TREND_WASH_USER_ID] = mUserId
            launchHome(mUserId!!)
        } else if (mHome == null) {
            showError("Authentication Failed")
        }
    }

    fun launchLogin() {
        if (mHome != null) {
            addFragment(TrendyWashLoginFragment.newInstance(mHome?.unitId
                    ?: "", mMenu?.icon), false)
        } else {
            showError()
        }
    }

    fun launchRegister() {
        if (mHome != null) {
            addFragment(TrendyWashRegisterFragment.newInstance(mHome?.unitId
                    ?: "", mMenu?.icon), true)
        } else {
            showError()
        }
    }

    fun launchHome(userId: String) {
        if (mHome != null) {
            mUserId = userId
            clearAllFragmentInStack()
            replaceFragment(TrendyWashHomeFragment.newInstance(mHome?.unitId
                    ?: "", userId, mMenu?.title), false)
        } else {
            showError()
        }
    }

    fun launchAddMoney() {
        if (mHome != null && mUserId != null) {
            addFragment(TrendyWashAddMoneyFragment.newInstance(mHome?.unitId
                    ?: "", mUserId!!), true)
        } else {
            showError()
        }
    }

    fun launchWashingMachine(machine: TrendyWashMachine) {
        if (mHome != null && mUserId != null) {
            launchWashingMachine(mHome?.unitId ?: "", mUserId!!, machine)
        } else {
            showError()
        }
    }

    fun launchWashingMachine(unitId: String, userId: String, machine: TrendyWashMachine) {
        addFragment(TrendyWashMachineFragment.newInstance(unitId, userId, machine), true)
    }

    fun logout() {
        PreferenceHelper.defaultPrefs(this)[PreferenceHelper.TREND_WASH_USER_ID] = null
        launchLogin()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    fun showError(message: String? = null) {
        alertError(message ?: getString(R.string.error_default_message))
    }


    private fun clearAllFragmentInStack() {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }

    private fun clearCookie() {
        CookieSyncManager.createInstance(this)
        CookieManager.getInstance().removeAllCookie()
    }

}

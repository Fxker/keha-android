package com.appy.android.code.domain.model.command.scenario

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.scenario.ScenarioCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandScenarioForm(
        val homeId: String,
        val scenarioId: String,
        val command: ScenarioCommand
) : AppyForm() {
    val mSendCommandScenarioJsonBody: SendCommandScenarioJsonBody = SendCommandScenarioJsonBody()

    init {
        this.mSendCommandScenarioJsonBody.command = command.action()
    }

}
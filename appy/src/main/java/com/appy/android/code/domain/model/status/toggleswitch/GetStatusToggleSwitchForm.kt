package com.appy.android.code.domain.model.status.toggleswitch

import com.appy.android.code.data.rest.form.AppyForm

internal class GetStatusToggleSwitchForm(
        internal val homeId: String,
        internal val switchId: String
) : AppyForm()
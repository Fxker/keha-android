package com.sansiri.homeservice.ui.facilitybooking.calendar

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View


/**
 * Created by sansiri on 10/16/17.
 */
class ItemDecorator(private val mSpace: Int) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (position != 0)
            outRect.top = mSpace
    }
}
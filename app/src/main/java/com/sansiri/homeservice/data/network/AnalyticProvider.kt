package com.sansiri.homeservice.data.network

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
//import com.google.android.gms.analytics.HitBuilders
//import com.google.android.gms.analytics.Tracker
import com.google.firebase.analytics.FirebaseAnalytics
import com.microsoft.appcenter.analytics.Analytics
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.util.observe

open class AnalyticProvider(val context: Context) {
    //    private var mTracker: Tracker? = null
    private var mPrefs: SharedPreferences? = null
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    init {
//        mTracker = application.getDefaultTracker()
        mPrefs = PreferenceHelper.defaultPrefs(context)

//        if (RuntimeCache.me != null) {
//            mTracker?.set("&uid", RuntimeCache.me?.objectId)
//        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)
    }

    fun sendScreenView(screenName: String) {
//        mTracker?.setScreenName(screenName)
//        mTracker?.send(HitBuilders.ScreenViewBuilder().build())

        val properties = hashMapOf<String, String>()
        properties["ScreenName"] = screenName
        properties["AppVersion"] = BuildConfig.VERSION_NAME
        mPrefs?.get(PreferenceHelper.USER_ID, "")?.let { properties["UID"] = it }
        RuntimeCache.selectedHome?.let { properties["UnitID"] = it.unitId }

        Analytics.trackEvent("View", properties)

        val bundle = Bundle().apply {
            putString("ScreenName", screenName)
            putString("AppVersion", BuildConfig.VERSION_NAME)
        }
        mPrefs?.get(PreferenceHelper.USER_ID, "")?.let {
            mFirebaseAnalytics?.setUserProperty("UID", it)
        }
        RuntimeCache.selectedHome?.let { mFirebaseAnalytics?.setUserProperty("UnitID", it.unitId) }

        mFirebaseAnalytics?.logEvent("View", bundle)
    }

    fun sendEvent(category: String, action: String, label: String?, logVersion: String = "2", optionalProp: List<Pair<String, String>>? = null) {
        /*
       *  Google Analytics
       */
//        val builder = HitBuilders.EventBuilder()
//                .setCategory(category)
//                .setAction(action)
//
//        if (label != null) {
//            builder.setLabel(label)
//        }
//        mTracker?.send(builder.build())

        /*
        *  MS Analytics
        */
        val properties = hashMapOf<String, String>()
        properties["Category"] = category
        properties["Action"] = action
        properties["Label"] = label ?: ""
        properties["AppVersion"] = BuildConfig.VERSION_NAME
        properties["LogVersion"] = logVersion
        mPrefs?.get(PreferenceHelper.USER_ID, "")?.let { properties["UID"] = it }
        RuntimeCache.selectedHome?.let { properties["UnitID"] = it.unitId }

        optionalProp?.forEach { prop ->
            properties[prop.first] = prop.second
        }

        Analytics.trackEvent(category, properties)

        /*
        * Firebase Analytics
         */
        val bundle = Bundle().apply {
            putString("Category", category)
            putString("Action", action)
            putString("Label", label)
            putString("AppVersion", BuildConfig.VERSION_NAME)
            putString("LogVersion", logVersion)
            optionalProp?.forEach { prop ->
                putString(prop.first, prop.second)
            }
        }
        mPrefs?.get(PreferenceHelper.USER_ID, "")?.let {
            mFirebaseAnalytics?.setUserProperty("UID", it)
        }
        RuntimeCache.selectedHome?.let { mFirebaseAnalytics?.setUserProperty("UnitID", it.unitId) }

        mFirebaseAnalytics?.logEvent(action, bundle)
    }

    companion object {
        fun sendError(url: String, code: Int, body: String?) {
            val properties = hashMapOf<String, String>()
            properties["Url"] = url
            properties["Code"] = code.toString()
            if (body != null && body.isNotEmpty()) {
                properties["Body"] = body
            }
            RuntimeCache.me?.objectId?.let { properties["UID"] = it }
            RuntimeCache.selectedHome?.let { properties["UnitID"] = it.unitId }

            Analytics.trackEvent("Error", properties)
        }

        fun sendError(channel: String, email: String?, providerId: String?, body: String?) {
            val properties = hashMapOf<String, String>()
            properties["Channel"] = channel
            if (!email.isNullOrEmpty()) properties["Email"] = email
            if (!providerId.isNullOrEmpty()) properties["ProviderId"] = providerId
            if (!body.isNullOrEmpty()) properties["Body"] = body
            RuntimeCache.me?.objectId?.let { properties["UID"] = it }
            RuntimeCache.selectedHome?.let { properties["UnitID"] = it.unitId }

            Analytics.trackEvent("Error", properties)
        }
    }
}

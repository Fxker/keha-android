package com.sansiri.homeservice.model.api

import com.sansiri.homeservice.model.api.partner.aqi.Air

data class ProjectInfo(
        var objectId: String = "",
        var name: String = "",
        var coverImageUrl: String = "",
        var iconImageUrl: String = "",
        var lat: Double = 0.0,
        var long: Double = 0.0,
        var createdAt: String = "",
        var updatedAt: String = ""
) {
    var air: Air? = null
}
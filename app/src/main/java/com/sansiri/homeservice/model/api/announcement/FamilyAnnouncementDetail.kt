package com.sansiri.homeservice.model.api.announcement

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class FamilyAnnouncementDetail(
        var announcementId: String? = null,
        override var createdAt: String? = null,
        override var startedAt: String? = null,
        override var subTitle: String? = null,
        override var countryCode: String? = null,
        override var coverImageUrl: String? = null,
        override var langCode: String? = null,
        override var detailsEncode: String? = null,
        override var detailsHtml: String? = null,
        override var title: String? = null,
        override var updatedAt: String? = null
) : AnnouncementDetail, Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(announcementId)
        writeString(createdAt)
        writeString(startedAt)
        writeString(subTitle)
        writeString(countryCode)
        writeString(coverImageUrl)
        writeString(langCode)
        writeString(detailsEncode)
        writeString(detailsHtml)
        writeString(title)
        writeString(updatedAt)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<FamilyAnnouncementDetail> = object : Parcelable.Creator<FamilyAnnouncementDetail> {
            override fun createFromParcel(source: Parcel): FamilyAnnouncementDetail = FamilyAnnouncementDetail(source)
            override fun newArray(size: Int): Array<FamilyAnnouncementDetail?> = arrayOfNulls(size)
        }
    }
}

package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 11/27/17.
 */
data class Code (val code: String? = null)
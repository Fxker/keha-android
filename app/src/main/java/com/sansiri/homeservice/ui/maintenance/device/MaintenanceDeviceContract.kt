package com.sansiri.homeservice.ui.maintenance.device

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MaintenanceDeviceContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut, device: MaintenanceDevice?)
        fun fetch()
        fun requestForDetail(plan: MaintenancePlan)
    }

    interface View : BaseView {
        fun bindDeviceDetail(device: MaintenanceDevice?)
        fun bindPlanDetail(plans: List<MaintenancePlan>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
        fun launchDetail(home: Hut, plan: MaintenancePlan)
    }
}
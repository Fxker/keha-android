package com.appy.android.sdk.status.blind

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.blind.BlindCommand
import com.appy.android.sdk.status.APStatus

/**
 * Created by ssa-dev-04 on 19/1/2561.
 */
class APStatusBlind : APStatus<BlindCommand> {
    constructor() : super()
    constructor(control: APControl, value: String) : super(control, value)
}
package com.sansiri.homeservice.ui.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.sansiri.homeservice.R
import com.sansiri.homeservice.service.AppFirebaseInstanceIdService
import com.sansiri.homeservice.ui.main.MainActivity
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.register.idcard.CitizenIDVerifyFragment
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.replaceFragment

class RegisterActivity : BaseActivity<RegisterContract.View, RegisterContract.Presenter>(), RegisterContract.View {
    override var mPresenter: RegisterContract.Presenter = RegisterPresenter()

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, RegisterActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        replaceFragment(CitizenIDVerifyFragment.newInstance(), false)
    }

    fun storeCustomerId(customerId: String) {
        mPresenter.storeCustomerId(customerId)
    }

    fun bindUser(onSuccess: () -> Unit, onFail: (Throwable) -> Unit) {
        mPresenter.bindUser(onSuccess, onFail)
    }

    fun launchMainPage() {
        val deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        AppFirebaseInstanceIdService.sendRegistrationToServer(deviceId)
        MainActivity.start(this)
        finish()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

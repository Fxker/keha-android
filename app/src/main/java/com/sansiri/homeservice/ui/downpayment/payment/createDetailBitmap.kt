package com.sansiri.homeservice.ui.downpayment.payment

import android.content.Context
import android.graphics.*
import androidx.core.content.res.ResourcesCompat
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.ui.epayment.CodeEngine
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.toCurrencyFormat

fun createDetailBitmap(context: Context, home: Hut?, downPaymentItem: DownpaymentItem?): Bitmap {
    
    val finalBitmap = Bitmap.createBitmap(
            CodeEngine.imageWidth,
            680,
            Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(finalBitmap)
    val width = canvas.width - CodeEngine.padding * 2

    canvas.drawRect(0f, 0f, finalBitmap.width.toFloat(), finalBitmap.height.toFloat(), Paint().apply {
        style = Paint.Style.FILL
        color = Color.WHITE
    })

    val textBodyPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 18f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textTitlePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 24f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textHeadlinePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 28f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textDisplayPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 36f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    canvas.translate(CodeEngine.padding.toFloat(), CodeEngine.padding.toFloat())

    val addressLayout = textLayout(home?.address, textHeadlinePaint, width)
    addressLayout.draw(canvas)

    canvas.translate(0f, addressLayout.height.toFloat() + CodeEngine.newlineMargin)

    val projectTitleLayout = textLayout(home?.title, textBodyPaint, width)
    projectTitleLayout.draw(canvas)

    canvas.translate(0f, projectTitleLayout.height.toFloat() + CodeEngine.newlineMargin + 20)

    val downpaymentTitleLayout = textLayout(downPaymentItem?.title, textTitlePaint, width)
    downpaymentTitleLayout.draw(canvas)

    canvas.translate(0f, downpaymentTitleLayout.height.toFloat() + CodeEngine.newlineMargin)

    val amountTitleLayout = textLayout(context.getString(R.string.amount), textBodyPaint, width)
    amountTitleLayout.draw(canvas)

    canvas.translate(0f, amountTitleLayout.height.toFloat() + CodeEngine.newlineMargin)

    val amountLayout = textLayout(downPaymentItem?.cost?.toCurrencyFormat(), textDisplayPaint, width)
    amountLayout.draw(canvas)

    canvas.translate(0f, amountLayout.height.toFloat() + CodeEngine.newlineMargin + 20)

    val dateTitleLayout = textLayout(context.getString(R.string.due), textBodyPaint, width)
    dateTitleLayout.draw(canvas)

    canvas.translate(0f, dateTitleLayout.height.toFloat() + CodeEngine.newlineMargin)

    val dateLayout = textLayout(downPaymentItem?.date?.report(), textTitlePaint, width)
    dateLayout.draw(canvas)


    return finalBitmap
}

fun textLayout(text: String?, paint: TextPaint, width: Int): StaticLayout {
    return StaticLayout(text, paint, width, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false)
}
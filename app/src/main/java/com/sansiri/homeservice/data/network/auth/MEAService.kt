package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.squareup.okhttp.ResponseBody
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface MEAService {
    @GET("me/consent/mea")
    fun getMEAConsent(): Observable<Response<Consent>>

    @POST("me/consent/mea")
    fun acceptMEAConsent(): Completable

    @GET("me/units/{unitId}/partners/mea")
    fun getMEAInfo(@Path("unitId") unitId: String): Observable<MEAInfo>

    @POST("me/units/{unitId}/partners/mea")
    fun saveCA(@Path("unitId") unitId: String, @Body meaInfo: MEAInfo): Observable<MEAInfo>
}
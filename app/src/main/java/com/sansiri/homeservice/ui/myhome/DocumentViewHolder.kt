package com.sansiri.homeservice.ui.myhome

import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.MyHomeDocument
import com.sansiri.homeservice.model.api.announcement.Announcement

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_document_my_home.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class DocumentViewHolder(val view: View, val itemClick: (MyHomeDocument) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(document: MyHomeDocument) {
        with(view) {
            Glide.with(context).loadCache(document.imageUrl ?: "", imageThumbnail)
            textTitle.text = document.detail?.title
            panelClickable.setOnClickListener {
                itemClick(document)
            }
        }
    }
}
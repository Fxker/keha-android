package com.sansiri.homeservice.ui.sell.create.detail

import com.sansiri.homeservice.model.SellOption
import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 11/7/17.
 */
class SellCreateDetailPresenter : BasePresenterImpl<SellCreateDetailContract.View>(), SellCreateDetailContract.Presenter {

    override fun start() {

    }

    override fun destroy() {
    }

    override fun validate(vararg sellOption: SellOption) {
        if (sellOption.all { !it.isEnabled }) {
            mView?.showError("Please select your option")
            return
        }

        sellOption.iterator().forEach {
            if (it.isEnabled && it.cost.isNullOrEmpty()) {
                mView?.showError("Please fill the cost in selected field")
                return
            }
        }


        mView?.launchNext()
    }

}
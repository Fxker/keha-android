package vc.siriventures.hsa.admin.model.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FileRequest(
        val filename: String? = null
): Parcelable
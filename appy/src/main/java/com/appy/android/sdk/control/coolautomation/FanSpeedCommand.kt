package com.appy.android.sdk.control.coolautomation

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
enum class FanSpeedCommand  : APCoolAutomationParameter {
    VERY_LOW {
        override fun action(): String {
            return "V"
        }
    },
    LOW {
        override fun action(): String {
            return "L"
        }
    },
    MEDIUM {
        override fun action(): String {
            return "M"
        }
    },
    HIGH {
        override fun action(): String {
            return "H"
        }
    },
    VERY_HIGH {
        override fun action(): String {
            return "A"
        }
    },
    AUTO {
        override fun action(): String {
            return "A"
        }
    };
}
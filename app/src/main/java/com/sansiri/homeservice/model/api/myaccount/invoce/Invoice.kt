package com.sansiri.homeservice.model.api.myaccount.invoce

import android.os.Parcel
import android.os.Parcelable
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class Invoice(
        val objectId: String? = null,
        val paymentBarcode: String? = null,
        val amount: Double,
        val grandTotal: Double,
        val createdAt: String? = null,
        val paidAt: String? = null,
        val dueAt: String? = null,
        val paymentChannels: List<PaymentChannel>? = null,
        val items: List<InvoiceItem>? = null,
        val taxId: String? = null,
        val reference1: String? = null,
        val reference2: String? = null
) : Parcelable {
    val _createdAt: Date?
        get() {
            return try {
                if (this.createdAt == null) return null

                DateTime(this.createdAt).toDate()
            } catch (e: ParseException) {
                null
            }
        }

    val _paidAt: Date?
        get() {
            return try {
                if (this.paidAt == null) return null

                DateTime(this.paidAt).toDate()
            } catch (e: ParseException) {
                null
            }
        }

    val _dueAt: Date?
        get() {
            return try {
                if (this.dueAt == null) return null

                DateTime(this.dueAt).toDate()
            } catch (e: ParseException) {
                null
            }
        }

}

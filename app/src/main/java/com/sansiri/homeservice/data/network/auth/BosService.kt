package com.sansiri.homeservice.data.network.auth

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.sansiri.homeservice.model.api.ListResponse
import com.sansiri.homeservice.model.api.bos.BosDetail
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.model.menu.BosMenu
import com.squareup.okhttp.Response
import com.squareup.okhttp.ResponseBody
import io.reactivex.Completable
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.http.*

interface BosService {
    @GET("me/units/{unitId}/partners/plus/bos/request/type")
    fun getBosType(@Path("unitId") unitId: String): Observable<ListResponse<BosMenu>>

    @POST("me/units/{unitId}/partners/plus/bos/issue/common-area")
    fun submitCommonAreaIssue(@Path("unitId") unitId: String, @Body bosDetail: BosDetail): Completable

    @POST("me/units/{unitId}/partners/plus/bos/issue/my-home")
    fun submitMyHomeIssue(@Path("unitId") unitId: String,  @Body bosDetail: BosDetail): Completable
}
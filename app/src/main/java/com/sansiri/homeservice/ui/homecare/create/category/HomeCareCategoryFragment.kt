package com.sansiri.homeservice.ui.homecare.create.category


import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator

import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.homecare.create.HomeCareCreateActivity
import com.sansiri.homeservice.ui.homecare.create.detail.HomeCareDetailFragment
import com.sansiri.homeservice.util.*
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton


class HomeCareCategoryFragment : BaseFragment<HomeCareCategoryContract.View, HomeCareCategoryContract.Presenter>(), HomeCareCategoryContract.View {
    override var mPresenter: HomeCareCategoryContract.Presenter = HomeCareCategoryPresenter()

    companion object {
        @JvmStatic
        fun newInstance(): HomeCareCategoryFragment {
            return HomeCareCategoryFragment()
        }
    }

    val mAdapter = GridMenuAdapter(null, null) {
        mPresenter.selectCategory(it)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as HomeCareCreateActivity).setToolbarTitle(getString(R.string.repair_request))

        list.apply {
            val gridLayoutManager = GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
            adapter = mAdapter
        }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("HOMECARE_CATEGORY")
    }

    override fun bindCategories(categories: List<Menu>) {
        mAdapter.setDataDelay(categories)
    }

    override fun nextScreen(menu: Menu) {
        val id  = menu.id?.toInt()
        sendEvent("HOMECARE", "CLICK", "HOMECARE_CATEGORY_$id")
        if (id != null) {
            val parent = activity as HomeCareCreateActivity
            parent.addFragment(HomeCareDetailFragment.newInstance(id), true)
            parent.setToolbarTitle(menu.title ?: "")
        } else {
            showError(getString(R.string.error_default_message))
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

}

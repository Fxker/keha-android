package com.sansiri.homeservice.ui.visitor.history

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.mailbox.detail.MailBoxDetailActivity
import com.sansiri.homeservice.ui.mailbox.history.MailBoxHistoryActivity
import com.sansiri.homeservice.ui.mailbox.history.MailBoxHistorySectionAdapter
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*

class VisitorAccessHistoryActivity : BaseActivity<VisitorAccessHistoryContract.View, VisitorAccessHistoryContract.Presenter>(), VisitorAccessHistoryContract.View {
    override var mPresenter: VisitorAccessHistoryContract.Presenter = VisitorAccessHistoryPresenter()

    val mAdapter = SectionedRecyclerViewAdapter()
    var mUnitId: String? = null

    companion object {
        val UNIT_ID = "UNIT_ID"
        val TITLE = "TITLE"
        fun start(activity: Activity, unitId: String, title: String?) {
            val intent = Intent(activity, VisitorAccessHistoryActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.history))

        mUnitId = intent?.getStringExtra(UNIT_ID)

        with(list) {
            setHasFixedSize(true)
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int) {
                    mPresenter.fetchData(mUnitId ?: "", page)
                }
            })

            adapter = mAdapter
        }

        mPresenter.fetchData(mUnitId ?: "", 0)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("VISITOR_ACCESS_HISTORY")
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun bindData(list: List<VisitorAccessHistory>) {
        val mapSection = hashMapOf<Date, MutableList<VisitorAccessHistory>>()
        mAdapter.removeAllSections()

        list.forEach {
            val reportDate = it._createdAt?.resetTime()
            if(reportDate != null) {
                if (mapSection[reportDate] == null) {
                    mapSection[reportDate] = mutableListOf(it)
                } else {
                    mapSection[reportDate]?.add(it)
                }
            }
        }

        mapSection.toSortedMap(compareByDescending<Date> { it }).forEach {
            val adapter = VisitorAccessHistorySectionAdapter(it.key.report(), it.value.sortedByDescending { it._createdAt }) { mail ->
                // MailBoxDetailActivity.start(this, mHome!!, mail)
            }
            mAdapter.addSection(adapter)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        if (mAdapter.itemCount == 0) {
            progressBar.show()
        }
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        if (mAdapter.itemCount == 0) {
            textNoData.show()
        }
    }


    override fun showError(message: String) {
        alertError(message)
    }
}

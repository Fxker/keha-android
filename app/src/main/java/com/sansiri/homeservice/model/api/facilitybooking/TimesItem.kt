package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.util.toDate
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class TimesItem(val duration: Int = 0,
                     @SerializedName("start_time")
                     val startTime: String?,
                     val booked: Boolean = false,
                     @SerializedName("booked_by")
                     val bookedBy: BookedBy?,
                     @SerializedName("end_time")
                     val endTime: String?,
                     @SerializedName("maintenance_end")
                     val maintenanceEnd: String?,
                     @SerializedName("preparation_start")
                     val preparationStart: String?,
                     @SerializedName("book_before")
                     val bookBefore: String?,
                     val enabled: Boolean = false) : Parcelable {
    val _endTime: Date?
        get() {
            return try {
                if (this.endTime == null) return null

                endTime.toDate("yyyy-MM-dd HH:mm:ss Z")
            } catch (e: Exception) {
                null
            }
        }

    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            1 == source.readInt(),
            source.readParcelable<BookedBy>(BookedBy::class.java.classLoader),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(duration)
        writeString(startTime)
        writeInt((if (booked) 1 else 0))
        writeParcelable(bookedBy, 0)
        writeString(endTime)
        writeString(maintenanceEnd)
        writeString(preparationStart)
        writeString(bookBefore)
        writeInt((if (enabled) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TimesItem> = object : Parcelable.Creator<TimesItem> {
            override fun createFromParcel(source: Parcel): TimesItem = TimesItem(source)
            override fun newArray(size: Int): Array<TimesItem?> = arrayOfNulls(size)
        }
    }
}
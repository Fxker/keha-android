package com.sansiri.homeservice.model.api.myaccount

import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class PaymentHistory(
	val invoiceNo: String? = null,
	val receiptNo: String? = null,
	val amount: Double? = null,
	val depAction: String? = null,
	val description: String? = null,
	val date: String? = null
) {
	val _date: Date?
		get() {
			return try {
				if (this.date == null) return null

				DateTime(this.date).toDate()
			} catch (e: ParseException) {
				null
			}
		}
}

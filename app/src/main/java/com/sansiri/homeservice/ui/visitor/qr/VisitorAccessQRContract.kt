package com.sansiri.homeservice.ui.visitor.qr

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface VisitorAccessQRContract {
    interface Presenter : BasePresenter<View> {
        fun init(code: String)
    }

    interface View : BaseView {
        fun generateQRBitmap(code: String)
    }
}
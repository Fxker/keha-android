package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result

import androidx.lifecycle.MutableLiveData
import com.ccpp.pgw.sdk.android.model.option.TransactionDetail
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.gateway2c2p.Gateway2c2pRepository
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.base.CoroutineScopePresenter
import kotlinx.coroutines.*

class PaymentGateway2C2PTransactionResultPresenter(val repository: Gateway2c2pRepository) : CoroutineScopePresenter<PaymentGateway2C2PTransactionResultContract.View>(), PaymentGateway2C2PTransactionResultContract.Presenter {
    var mTransactionId: String? = null
    var mBillingEmail: String? = null
    override var transactionDetail: MutableLiveData<Resource<Transaction2C2PDetail>>? = null

    override fun init(transactionId: String?, billingEmail: String?) {
        mTransactionId = transactionId
        mBillingEmail = billingEmail
    }

    override fun start() {
        fetch()
    }

    fun fetch() {
        if (mTransactionId != null) {
            transactionDetail = repository.inquiryTransaction(mTransactionId!!)
        } else {
            mView?.showError(R.string.error_default_message)
        }
    }

    override fun saveCard(pan: String?, cardToken: String, method: String?) {
        launch {
            repository.saveCard(SavingCreditCard(securedCardNumber = pan, cardToken = cardToken, provider = method, billingEmail = mBillingEmail))
        }
    }

    override fun destroy() {
    }
}
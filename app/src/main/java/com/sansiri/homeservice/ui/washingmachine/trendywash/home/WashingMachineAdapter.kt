package com.sansiri.homeservice.ui.washingmachine.trendywash.home

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class WashingMachineAdapter(val itemClick: (TrendyWashMachine) -> Unit) : RecyclerView.Adapter<WashingMachineViewHolder>() {
    private val mData = mutableListOf<TrendyWashMachine>()


    override fun getItemCount() = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WashingMachineViewHolder {
        return WashingMachineViewHolder(parent.inflate(R.layout.view_summary_card), itemClick)
    }

    override fun onBindViewHolder(holder: WashingMachineViewHolder, position: Int) {
        holder.countDownTimer?.cancel()
        holder.bind(mData[position])
    }

    fun setData(data: List<TrendyWashMachine>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }
}
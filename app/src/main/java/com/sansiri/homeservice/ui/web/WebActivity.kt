package com.sansiri.homeservice.ui.web

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.activity_web.*
import android.net.Uri
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import com.google.android.gms.common.Feature
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.util.setBackToolbar
import im.delight.android.webview.AdvancedWebView
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.util.launchExternalBrowser
import io.opencensus.trace.MessageEvent
import com.sansiri.homeservice.model.event.CloseEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


open class WebActivity : AppCompatActivity(), IWeb, AdvancedWebView.Listener {
    protected lateinit var analyticProvider: AnalyticProvider

    companion object {
        val URL = "URL"
        val TITLE = "TITLE"
        val QUERY = "QUERY"
        val ADD_TOKEN_TO_REQUEST = "ADD_TOKEN_TO_REQUEST"
        val CODE = 4563

        fun start(activity: Activity, url: String, title: String, addTokenWithRequest: Boolean = false) {
            val intent = Intent(activity, WebActivity::class.java)
            intent.putExtra(URL, url)
            intent.putExtra(TITLE, title)
            intent.putExtra(ADD_TOKEN_TO_REQUEST, addTokenWithRequest)
            activity.startActivityForResult(intent, CODE)
        }

        fun start(fragment: Fragment, url: String, title: String, addTokenWithRequest: Boolean = false) {
            val intent = Intent(fragment.activity, WebActivity::class.java)
            intent.putExtra(URL, url)
            intent.putExtra(TITLE, title)
            intent.putExtra(ADD_TOKEN_TO_REQUEST, addTokenWithRequest)
            fragment.startActivityForResult(intent, CODE)
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)


        analyticProvider = AnalyticProvider(application as com.sansiri.homeservice.AppController)

        val url = intent.getStringExtra(URL)
        val title = intent.getStringExtra(TITLE)
        val addTokenWithRequest = intent.getBooleanExtra(ADD_TOKEN_TO_REQUEST, false)

        setBackToolbar(title)

        if (savedInstanceState != null) {
            webview.restoreState(savedInstanceState)
        }

        initWebView(url, addTokenWithRequest)
    }

    override fun initWebView(url: String, addTokenWithRequest: Boolean) {

        webview.setListener(this, this)
        webview.makeItSmart(this, progressBar) { query, url ->
            if (query == "external_browser") {
                launchExternalBrowser(url ?: "")
            } else {
                launchExtraRedirect(query)
            }
        }

        if (addTokenWithRequest) {
            ApiRepositoryProvider.getToken { token ->
                val locale = ApiRepositoryProvider.getLanguage()
                if (token != null) {
                    val map = mapOf(
                            Pair("Authorization", token),
                            Pair("Accept-Language", locale),
                            Pair("AppVersion", BuildConfig.VERSION_NAME)
                    )
                    webview.loadUrl(url, map)
                } else {
                    webview.loadUrl(url)
                }
            }
        } else {
            webview.loadUrl(url)
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        webview.onResume()
        analyticProvider.sendScreenView("WEB")
    }

    override fun onPause() {
        webview.onPause()
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        webview.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        webview.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_close) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_close, menu)
        return true
    }

    override fun onBackPressed() {
        if (!webview.onBackPressed()) return
        super.onBackPressed()
    }

    override fun onPageFinished(url: String?) {
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        Log.d("WebActivity", description)
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
        Log.d("WebActivity", url)
        url?.let { launchExternalBrowser(url) }
    }

    override fun onExternalPageRequest(url: String?) {
        Log.d("WebActivity", url)
        url?.let { launchExternalBrowser(url) }
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onClose(event: CloseEvent) {
        if (event.forceClose) {
            finish()
        }
    }

    private fun launchExtraRedirect(query: String) {
        val returnIntent = Intent()
        returnIntent.putExtra(QUERY, query)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
}

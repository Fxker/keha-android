package com.appy.android.automation.network

import android.util.Log

import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket
import java.util.Timer
import java.util.TimerTask

/**
 * Created by Tar on 9/15/16 AD.
 */
class THMStatusListener {

    protected var onReceivedResponseListener: OnReceivedResponseListener? = null

    fun start() {
        try {
            activite = true
            startThreadRead()
        } catch (e: Exception) {

        }
    }

    private fun startThreadRead() {
        tr = Thread(Runnable {
            while (activite) {

                try {
                    if (ds == null) {
                        ds = MulticastSocket(8760)
                        ds!!.joinGroup(InetAddress.getByName("239.255.255.255"))
                    }

                    ds!!.broadcast = true
                    ds!!.reuseAddress = true
                    ds!!.soTimeout = 0

                    t = Timer()
                    tt = object : TimerTask() {

                        override fun run() {
                            val packet: DatagramPacket
                            try {
                                if (activite) {
                                    val buffer = "STATUS OUTPUT".toByteArray()

                                    packet = DatagramPacket(buffer, buffer.size, InetAddress.getByName("255.255.255.255"), 8760)

                                    ds!!.send(packet)
                                    ds!!.reuseAddress = true
                                }
                            } catch (e: Exception) {
                            }

                        }
                    }
                    t!!.schedule(tt, 500, 5000)


                    while (activite) {
                        Log.i("Dev", "Listening")
                        val buffer = ByteArray(1024)
                        val packet = DatagramPacket(buffer, buffer.size)
                        ds!!.receive(packet)
                        ds!!.reuseAddress = true

                        val str = String(buffer, 0, packet.length)
                        Log.i("Dev", "Response : " + str)

                        if (onReceivedResponseListener != null) {
                            onReceivedResponseListener!!.onReceivedResponse(this@THMStatusListener, str)
                        }
                    }

                    ds!!.close()
                    tt!!.cancel()
                    t!!.purge()
                    t!!.cancel()
                } catch (e: Exception) {
                    stop()
                }

            }
        })

        tr!!.start()
    }

    fun stop() {
        android.util.Log.d("Dev", "feedback was stoped")

        activite = false
        android.util.Log.d("Dev", "activite was false")

        if (ds != null) {
            val aux = ds
            ds = null
            aux!!.close()
        }
        android.util.Log.d("Dev", "datagram socket was closed")

        if (tr != null) {
            val aux = tr
            tr = null
            aux!!.interrupt()
            android.util.Log.i("threadread", "...thread stop!")
        }
        android.util.Log.d("Dev", "thread read stoped")

        if (tt != null) {
            val aux = tt
            tt = null
            aux!!.cancel()
        }
        android.util.Log.d("Dev", "timertask was canceled")

        if (t != null) {
            val aux = t
            t = null
            aux!!.purge()
            aux.cancel()
        }
        android.util.Log.d("Dev", "timer was canceled and stoped")
    }

    interface OnReceivedResponseListener {
        fun onReceivedResponse(socketHandler: THMStatusListener, response: String)
    }

    companion object {
        private var activite: Boolean = false
        private var tr: Thread? = null
        private var ds: MulticastSocket? = null
        private var tt: TimerTask? = null
        private var t: Timer? = null
    }
}

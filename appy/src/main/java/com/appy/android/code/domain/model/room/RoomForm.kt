package com.appy.android.code.domain.model.room

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
internal open class RoomForm(
        internal val homeId: String,
        internal val roomId: String
) : AppyForm()
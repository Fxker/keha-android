package com.appy.android.code.base.domain.subscriber

import io.reactivex.observers.DisposableObserver

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal open class DefaultSubscriber<T> : DisposableObserver<T>() {

    var resultObj: T? = null
    var error: Throwable? = null

    override fun onComplete() {

    }

    override fun onError(e: Throwable) {

    }

    override fun onNext(t: T) {
        resultObj = t
    }
}
package com.sansiri.homeservice.ui.homecare.history.detail

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_homecare_report.view.*
import org.jetbrains.anko.textColor

class HomeCareReportViewHolder (val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    fun bind(color: String?, data: HomeCareHistory.HomeCareSubItem) {
        with(view) {
            detail.text = data.detail
            detailType.text = data.detailType
            detailItem.text = data.detailItem
            statusName.text = data.statusName
            openedAt.text = data.openedDate?.report("dd.M.yyyy") ?: "-"
            completedAt.text = data.completedDate?.report("dd.M.yyyy") ?: "-"

            color?.let { statusName.textColor = Color.parseColor(color) }

            setAttachment(data.attachmentsBefore ?: listOf(), beforeTitle, beforeList)
            setAttachment(data.attachmentsDuring ?: listOf(), duringTitle, duringList)
            setAttachment(data.attachmentsAfter ?: listOf(), afterTitle, afterList)
        }
    }

    private fun setAttachment(attachments: List<HomeCareHistory.Attachment>, title: TextView, thumbnails: RecyclerView) {
        if (attachments.isEmpty()) {
            title.visibility = View.GONE
            thumbnails.visibility = View.GONE
            return
        }
        title.visibility = View.VISIBLE
        thumbnails.visibility = View.VISIBLE
        with(thumbnails.adapter as HomeCareHistorySubItemAttachmentAdapter) {
            this.mData = attachments.map { it.url ?: "" }
        }
    }
}
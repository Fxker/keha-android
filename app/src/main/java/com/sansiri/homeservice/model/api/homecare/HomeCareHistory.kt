package com.sansiri.homeservice.model.api.homecare

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class HomeCareHistory(
		val acceptedAt: String? = null,
		val source: String? = null,
		val requestId: String? = null,
		val statusName: String? = null,
		val typeId: Int,
		val typeIconUrl: String? = null,
		val detail: String? = null,
		val requestJobId: String? = null,
		val statusId: Int,
		val projectId: String?,
		val unitId: String?,
		val typeName: String?,
		val note: String?,
		val statusBgColor: String?,
		val remark: String?,
		val referenceId: String?,
		val createdAt: String?,
		val updatedAt: String?,
		val attachments: List<Attachment>?,
		val subItems: List<HomeCareSubItem>?
): Parcelable {

	@Parcelize
	data class Attachment(val itemNo: Int, val url: String?) : Parcelable

	@Parcelize
	data class HomeCareSubItem(
			val objectId: String?,
			val statusId: Int,
			val statusName: String?,
			val detail: String?,
			val detailType: String?,
			val detailItem: String?,
			val openedAt: String?,
			val completedAt: String?,
			var attachmentsBefore: List<Attachment>?,
			var attachmentsDuring: List<Attachment>?,
			var attachmentsAfter: List<Attachment>?
	) : Parcelable {
		val openedDate: Date?
			get() {
				if (this.openedAt.isNullOrBlank()) {
					return null
				}
				return try {
					DateTime(this.openedAt).toDate()
				} catch (e: ParseException) {
					null
				}
			}

		val completedDate: Date?
			get() {
				if (this.completedAt.isNullOrBlank()) {
					return null
				}
				return try {
					DateTime(this.completedAt).toDate()
				} catch (e: ParseException) {
					null
				}
			}
	}

	val createdDate: Date?
		get() {
			if (this.createdAt.isNullOrBlank()) {
				return null
			}
			return try {
				DateTime(this.createdAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	val acceptedDate: Date?
		get() {
			if (this.acceptedAt.isNullOrBlank()) {
				return null
			}
			return try {
				DateTime(acceptedAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	val updatedDate: Date?
		get() {
			if (this.updatedAt.isNullOrBlank()) {
				return null
			}
			return try {
				DateTime(this.updatedAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	fun sortSubItemAttachments() {
		this.subItems?.forEach { subItem ->
				subItem.attachmentsBefore = subItem.attachmentsBefore?.sortedBy { it.itemNo }
				subItem.attachmentsAfter = subItem.attachmentsAfter?.sortedBy { it.itemNo }
				subItem.attachmentsDuring = subItem.attachmentsDuring?.sortedBy { it.itemNo }
			}
	}

}


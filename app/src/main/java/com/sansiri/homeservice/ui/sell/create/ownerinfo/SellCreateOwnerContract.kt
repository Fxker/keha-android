package com.sansiri.homeservice.ui.sell.create.ownerinfo

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/7/17.
 */
interface SellCreateOwnerContract {
    interface Presenter : BasePresenter<View> {
    }

    interface View : BaseView {
    }
}
package com.sansiri.homeservice.ui.chat

import androidx.recyclerview.widget.RecyclerView
import android.text.util.Linkify
import android.view.View
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.util.TimeShow
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_chat_box_me.view.*
import me.saket.bettermovementmethod.BetterLinkMovementMethod
import org.joda.time.DateTime
import java.util.*

/**
 * Created by oakraw on 28/10/2017 AD.
 */
class MeMessageViewHolder(val view: View, val onLinkClicked: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(message: ChatMessage) {
        with(view) {
            textMessage.text = message.text
            BetterLinkMovementMethod
                    .linkify(Linkify.WEB_URLS, textMessage).setOnLinkClickListener { textView, url ->
                        onLinkClicked(url)
                        true
                    }
            textTime.text = TimeShow.show(message.createdAt)
        }
    }
}

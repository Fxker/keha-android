package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 11/23/17.
 */
interface Doc {
    var title: String?
    var url: String?
}
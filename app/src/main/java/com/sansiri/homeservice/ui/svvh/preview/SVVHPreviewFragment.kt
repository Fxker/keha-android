package com.sansiri.homeservice.ui.svvh.preview

import android.graphics.Color
import android.os.Bundle
import androidx.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.transition.Slide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHPromotion
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.svvh.SVVHActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_svvh_preview.*

class SVVHPreviewFragment : BaseFragment<SVVHPreviewContract.View, SVVHPreviewContract.Presenter>(), SVVHPreviewContract.View {
    override var mPresenter: SVVHPreviewContract.Presenter = SVVHPreviewPresenter()
    private var mHome: Hut? = null
    var mCustomer: SVVHRegisterCustomer? = null

    companion object {
        const val HOME = "HOME"
        const val CUSTOMER = "CUSTOMER"

        fun newInstance(home: Hut, customer: SVVHRegisterCustomer): SVVHPreviewFragment {
            val fragment = SVVHPreviewFragment()
            val bundle = Bundle().apply {
                putParcelable(HOME, home)
                putParcelable(CUSTOMER, customer)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_svvh_preview, container, false)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHome = arguments?.getParcelable(HOME)
        mCustomer = arguments?.getParcelable(CUSTOMER)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SVVH_INFORMATION")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mHome != null && mCustomer != null) {
            mPresenter.init(mHome!!, mCustomer!!)
            mPresenter.start()
        }
    }

    override fun bindData(customer: SVVHRegisterCustomer) {
        textName.text = "${customer.firstname} ${customer.lastname}"
        textIDCard.text = customer.citizenOrPassportId
        textPhoneNumber.text = customer.contactNumber
        textBirthDate.text = customer._birthDate?.reportDMYFull()
        textGender.text = when (customer.gender) {
            SVVHRegisterCustomer.MALE -> getString(R.string.male)
            SVVHRegisterCustomer.FEMALE -> getString(R.string.female)
            else -> ""
        }

        buttonContinue.setOnClickListener {
            sendEvent("SVVH", "CLICK", "START")
            (activity as SVVHActivity).launchWebView(customer.redirectUrl)
        }
    }

    override fun showPromotion(promotion: SVVHPromotion) {
        val color = try {
            Color.parseColor(promotion.titleRGB)
        } catch (e: Exception) {
            null
        }
        val slide = Slide()
        slide.slideEdge = Gravity.END
        TransitionManager.beginDelayedTransition(container, slide)
        layoutPromotion.show()
        textTitlePromotion.text = promotion.title
        textDetailPromotion.text = promotion.details

        color?.let { textTitlePromotion.setTextColor(it) }
    }


    override fun showError(message: String) {
        activity?.alertError(message)
    }
}
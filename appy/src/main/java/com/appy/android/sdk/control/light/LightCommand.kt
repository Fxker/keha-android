package com.appy.android.sdk.control.light

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class LightCommand : APCommand {
    OFF {
        override fun action(): String {
            return "CONTROL_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "CONTROL_ON"
        }
    };

    companion object {
        @JvmStatic
        fun getLastCommand(value: String): LightCommand {
            return LightCommand.values()[value.toInt()]
        }

        @JvmStatic
        fun getAvailableCommand(value: String): List<LightCommand> {
            var lightCommandArray = LightCommand.values().filter { it != getLastCommand(value) }
            return lightCommandArray
        }
    }
}
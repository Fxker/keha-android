package com.sansiri.homeservice.model.api.payment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class Transaction2C2PDetail(
	val createdAt: String? = null,
	val total: Double? = null,
	val cost: Double? = null,
	val method: String? = null,
	val fee: Double? = null,
	val channel: String? = null,
	val transactionId: String? = null,
	val paymentAt: String? = null,
	val status: String? = null,
	val updatedAt: String? = null,
	val externalData: ExternalData? = null
): Parcelable {
	companion object {
		val GATEWAY_2C2P = "2C2P_SCB"
	}
	val _paymentAt: Date?
		get() {
			try {
				return DateTime(this.paymentAt).toDate()
			} catch (e: ParseException) {
				return null
			}
		}

	@Parcelize
	data class ExternalData(
			val pan: String? = null,
			val cardToken: String? = null
	) : Parcelable
}

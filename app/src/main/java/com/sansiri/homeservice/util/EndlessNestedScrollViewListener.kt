package com.sansiri.homeservice.util

import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log


abstract class EndlessNestedScrollViewListener(private val mLinearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager) : NestedScrollView.OnScrollChangeListener {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of html to load.
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0

    private var currentPage = 0

    override fun onScrollChange(view: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if(view.getChildAt(view.childCount - 1) != null) {
            if ((scrollY >= (view.getChildAt(view.childCount - 1).measuredHeight - view.measuredHeight)) &&
                    scrollY > oldScrollY) {

                visibleItemCount = mLinearLayoutManager.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false
                        previousTotal = totalItemCount
                    }
                }

                if (!loading) {
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount) {
                        currentPage++
                        onLoadMore(currentPage)
                        loading = true
                    }
                }
            }
        }
    }

    abstract fun onLoadMore(currentPage: Int)

    companion object {
        var TAG = EndlessNestedScrollViewListener::class.java.simpleName
    }
}
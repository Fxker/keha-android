package com.sansiri.homeservice.model.api.payment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentResponse(
        val transactionId: String? = null,
        val totalAmountText: String? = null,
        val feeText: String? = null,
        val feeDetails: String? = null,
        val poweredByText: String? = null,
        val actionType: String? = null,
        val externalUri: String? = null,
        val popupTitle: String? = null,
        val popupMessage: String? = null,
        val externalData: ExternalData? = null
) : Parcelable {
    companion object ActionType {
        const val EXTERNAL_BROWSER = "EXTERNAL_BROWSER"
        const val SDK = "SDK"
    }

    @Parcelize
    data class ExternalData(
            val paymentToken: String? = null,
            val merchantId: String? = null
    ) : Parcelable
}

package com.sansiri.homeservice.ui.svvh.preview

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHPromotion
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SVVHPreviewContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut, customer: SVVHRegisterCustomer)
        fun fetchPromotion()
    }

    interface View : BaseView {
        fun bindData(customer: SVVHRegisterCustomer)
        fun showPromotion(promotion: SVVHPromotion)
    }
}
package com.sansiri.homeservice.ui.announcement.content

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_content.*
import android.app.ActivityOptions
import android.app.SearchManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.view.animation.OvershootInterpolator
import android.webkit.WebView
import android.webkit.WebViewClient
import com.bumptech.glide.Glide
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.photo.PreviewImageActivity
import com.sansiri.homeservice.util.JSInterface
import im.delight.android.webview.AdvancedWebView


class ContentActivity : BaseActivity<ContentContract.View, ContentContract.Presenter>(), ContentContract.View, AdvancedWebView.Listener {
    override var mPresenter: ContentContract.Presenter = ContentPresenter()

    val jsInterface = JSInterface {
        showFullImage(it)
    }

    companion object {
        val ID = "ID"
        val CONTENT = "CONTENT"
        val TYPE = "TYPE"

        fun start(activity: Activity, announcementId: String) {
            val intent = Intent(activity, ContentActivity::class.java)
            intent.putExtra(ID, announcementId)
            activity.startActivity(intent)
        }

        fun start(activity: Activity, titleType: String, announcement: Announcement<*>?) {
            val intent = Intent(activity, ContentActivity::class.java)
            intent.putExtra(CONTENT, announcement)
            intent.putExtra(TYPE, titleType)
            activity.startActivity(intent)
        }

        fun start(activity: Activity, coverImage: View?, titleType: String, announcement: Announcement<*>?) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && coverImage != null) {
                val intent = Intent(activity, ContentActivity::class.java)
                intent.putExtra(CONTENT, announcement)
                intent.putExtra(TYPE, titleType)
                val transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(activity, coverImage, activity.getString(R.string.transition_cover))
                activity.startActivity(intent, transitionActivityOptions.toBundle())
            } else {
                start(activity, titleType, announcement)
            }
        }
    }

    @SuppressLint("JavascriptInterface", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        setBackToolbar(toolbar, "")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val slideUp = Slide(Gravity.TOP)
            slideUp.duration = 500
            slideUp.interpolator = OvershootInterpolator(1f)
            slideUp.excludeTarget(android.R.id.statusBarBackground, true)
            slideUp.excludeTarget(android.R.id.navigationBarBackground, true)
            window.enterTransition = slideUp
            window.returnTransition = slideUp
        }


        val announcement = intent.getParcelableExtra<Announcement<*>>(CONTENT)
        val announcementId = intent.getStringExtra(ID)

        textType.text = intent.getStringExtra(TYPE) ?: ""

        if (announcement != null) {
            renderAnnouncement(announcement)
            sendEvent("ANNOUNCEMENT", "VIEW", announcement.objectId)

        } else if (announcementId != null) {
            mPresenter.fetchData(announcementId)
            sendEvent("ANNOUNCEMENT", "VIEW", announcementId)
        }

    }

    override fun renderAnnouncement(announcement: Announcement<*>) {
        val detail = announcement.details?.getOrNull(0)
        if (detail != null) {
            if (detail.detailsHtml != null) {
                webView.setListener(this, this)
                webView.webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                        var removeNotNeedPathUrl = url.removePrefix("file:///android_asset/css/")
                        try {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(removeNotNeedPathUrl))
                            startActivity(intent)
                            return true
                        } catch (e: Exception) {
                            val intent = Intent(Intent.ACTION_WEB_SEARCH)
                            intent.putExtra(SearchManager.QUERY, removeNotNeedPathUrl);
                            startActivity(intent)
                            return true
                        }
                    }
                }

                val contentTextSize = PreferenceHelper.defaultPrefs(this)[PreferenceHelper.CONTENT_TEXT_SIZE, 22]
                        ?: 22

                webView.settings.javaScriptEnabled = true
//                webView.settings.defaultFontSize = contentTextSize
                webView.addJavascriptInterface(jsInterface, "JSInterface")

                val replacedHtml = detail.detailsHtml?.replace("<img", "<img onclick='showImage(this.src)'")

                val html = "<link rel=\"stylesheet\" type=\"text/css\" href=\"classic.css\"/>" +
                        "<script>\n" +
                        "  function showImage(img){\n" +
                        "    window.JSInterface.openFullScreenImage(img);\n" +
                        "  }\n" +
                        "</script>" +
                        "${replacedHtml}"
                webView.loadHtml(html, "file:///android_asset/css/")
            }

            Glide.with(this).loadCache(detail.coverImageUrl ?: "", imageCover)
            imageCover.setOnClickListener {
                showFullImage(detail.coverImageUrl ?: "")
            }
            textTitle.text = detail.title
            textDate.text = announcement.displayDate?.report()
        }
    }

    private fun showFullImage(src: String) {
        PreviewImageActivity.start(this, src)
    }

    private fun increaseTextSize() {
        if (webView.settings.defaultFontSize < 40) {
            webView.settings.defaultFontSize += 2
            PreferenceHelper.defaultPrefs(this)[PreferenceHelper.CONTENT_TEXT_SIZE] = webView.settings.defaultFontSize
        }

    }

    private fun decreaseTextSize() {
        if (webView.settings.defaultFontSize > 12) {
            webView.settings.defaultFontSize -= 2
            PreferenceHelper.defaultPrefs(this)[PreferenceHelper.CONTENT_TEXT_SIZE] = webView.settings.defaultFontSize
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_content, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_zoom_in -> increaseTextSize()
            R.id.action_zoom_out -> decreaseTextSize()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("ANNOUNCEMENT_DETAIL")
    }

    override fun showError(message: String) {
    }

    fun showLoading() {
    }

    fun hideLoading() {
    }

    override fun onPageFinished(url: String?) {
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
    }

    override fun onExternalPageRequest(url: String?) {
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }
}

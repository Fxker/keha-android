package com.sansiri.homeservice.ui.epayment.gateway.phone


import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.view.*
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_payment_gateway_phone_verify.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.textColor
import org.koin.androidx.scope.currentScope

class PaymentGatewayPhoneVerifyFragment : BaseV2Fragment<PaymentGatewayPhoneVerifyContract.View, PaymentGatewayPhoneVerifyContract.Presenter>(), PaymentGatewayPhoneVerifyContract.View {
    override val mPresenter: PaymentGatewayPhoneVerifyContract.Presenter by currentScope.inject()
    private var mPaymentGatewayData: PaymentGatewayData? = null
    private var mPhoneNumber: String? = null
        set(value) {
            buttonContinue.isEnabled = !value.isNullOrEmpty()
            buttonContinue.alpha = if (value.isNullOrEmpty()) 0.5f else 1f
            field = value
        }

    companion object {
        const val DATA = "DATA"

        fun newInstance(paymentGatewayData: PaymentGatewayData): PaymentGatewayPhoneVerifyFragment {
            return PaymentGatewayPhoneVerifyFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(DATA, paymentGatewayData)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPaymentGatewayData = arguments?.getParcelable<PaymentGatewayData>(DATA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment_gateway_phone_verify, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mPaymentGatewayData != null) {
            mPresenter.init(mPaymentGatewayData!!)
            mPresenter.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(paymentGatewayData: PaymentGatewayData) {
        Glide.with(this).load(paymentGatewayData.icon).into(imageLogo)
        paymentGatewayData.mainThemeColor?.let { header.setBackgroundColor(it) }
        paymentGatewayData.merchantName?.let { textMerchantName.text = it }
        paymentGatewayData.paymentTitle?.let { textTitle.text = it }
        paymentGatewayData.refCode1?.let { textRefCode1.text = it }
        paymentGatewayData.refCode2?.let { textRefCode2.text = it }
        textAmount.text = paymentGatewayData.amount.toCurrencyFormat()
        textMessage.text = "${String.format(resources.getString(R.string.payment_phone_verify_message))} ${paymentGatewayData.gatewayName}"

        editPhone.addTextChangedListener(TextWatcherExtend {
            mPhoneNumber = it
        })

        buttonContinue.setOnClickListener {
            mPresenter.verifyPhone(mPhoneNumber ?: "")
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            paymentGatewayData.mainThemeColor?.let { buttonContinue.backgroundTintList = ColorStateList.valueOf(it) }
            paymentGatewayData.accentThemeColor?.let { buttonContinue.textColor = it }
        }

        mPhoneNumber = ""
    }

    override fun launchWeb(url: String) {
        WebActivity.start(this, url, "")
    }

    override fun showLoading() {
        progressBar.show()
        buttonContinue.hide()
        editPhone.isEnabled = false
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonContinue.show()
        editPhone.isEnabled = true
    }

    override fun processResponse(response: PaymentResponse) {
        activity?.apply {
            val returnIntent = Intent()
            returnIntent.putExtra(PaymentGatewayActivity.RESPONSE, response)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    override fun showPopup(message: String) {
        activity?.alert(message, "")
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

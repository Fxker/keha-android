package com.sansiri.homeservice.ui.register.federatiallogin

import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import android.os.Bundle
import android.util.Log
import retrofit2.HttpException


/**
 * Created by sansiri on 10/17/17.
 */
class LoginPresenter : BasePresenterImpl<LoginContract.View>(), LoginContract.Presenter {

    private var mAccessToken: AccessToken? = null
    private var mGoogleId: String? = null
    private var mGoogleEmail: String? = null

    override fun start() {
        mAccessToken = null
        // ClientConfiguration()
    }

    override fun destroy() {
    }


    override fun facebookLogin(accessToken: AccessToken) {
        reset()

        mAccessToken = accessToken

        mView?.showLoading()
        FirebaseAuthManager.facebookSignIn(accessToken.token, {
            mView?.hideLoading()
            mView?.launchNext()
        }) {
            //            AnalyticProvider.sendError("FACEBOOK", error?., null, message)
            mView?.hideLoading()
            mView?.showError(it.localizedMessage)
        }
    }

    override fun googleLogin(id: String, email: String, token: String) {
        reset()

        mGoogleId = id
        mGoogleEmail = email

        mView?.showLoading()
        FirebaseAuthManager.googleSignIn(token, {
            mView?.hideLoading()
            mView?.launchNext()
        }) {
            mView?.hideLoading()
            mView?.showError(it.localizedMessage)
        }
    }

    override fun validateAccount() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                it.getMe().observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            mView?.hideLoading()
                            if (it.objectId != null) {
                                mView?.launchMainPage()
                            } else {
                                sendReport("This account doesn't exist")
                                mView?.showError("This account doesn't exist")
                                mView?.signOut()
                            }
                        }, {
                            mView?.hideLoading()
                            mView?.signOut()

                            if (it is HttpException) {
                                sendReport(it.response()?.raw().toString())
                                if (it.code() == 404) {
                                    mView?.showMissingUser()
                                }
                            } else {
                                mView?.showError(it.localizedMessage)
                            }
                        })
            } else {
                mView?.hideLoading()
                mView?.showError("Something went wrong")
                mView?.signOut()
            }
        }
    }

    private fun reset() {
        mAccessToken = null
        mGoogleId = null
        mGoogleEmail = null
    }

    private fun sendReport(errorBody: String) {
        if (mAccessToken != null) {
            sendFacebookReport(mAccessToken!!, errorBody)
        } else if (mGoogleId != null && mGoogleEmail != null) {
            sendGoogleReport(mGoogleId!!, mGoogleEmail!!, errorBody)
        }
    }

    private fun sendFacebookReport(accessToken: AccessToken, errorBody: String) {
        val request = GraphRequest.newMeRequest(accessToken) { obj, response ->
            val id = obj.getString("id")
            val email = obj.getString("email")
            AnalyticProvider.sendError("FACEBOOK", email, id, errorBody)
        }

        val parameters = Bundle()
        parameters.putString("fields", "id,email")
        request.parameters = parameters
        request.executeAsync()
    }

    private fun sendGoogleReport(id: String, email: String, errorBody: String) {
        AnalyticProvider.sendError("GOOGLE", email, id, errorBody)
    }


}
package com.appy.android.automation.helper

import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom

import android.content.Context
import kotlin.experimental.and

class StringHelper {

    companion object {
        private var sr: SecureRandom? = null
        private var context: Context? = null

        @JvmStatic
        fun init(_context: Context) {
            context = _context
        }

        @JvmStatic
        fun get(id: Int): String {
            return get(context, id)
        }

        @JvmStatic
        fun get(context: Context?, id: Int): String {
            return context!!.getString(id)
        }

        @JvmStatic
        fun get(id: Int, vararg args: Any): String {
            return get(context, id, *args)
        }

        @JvmStatic
        fun get(context: Context?, id: Int, vararg args: Any): String {
            return context!!.getString(id, *args)
        }

        @JvmStatic
        fun random(): String? {
            if (sr == null) {
                sr = SecureRandom()
            }
            return encrypt(BigInteger(130, sr).toString(32))
        }

        @JvmStatic
        fun encrypt(string: String): String? {
            val digest: MessageDigest
            try {
                digest = MessageDigest.getInstance("MD5")
                digest.reset()
                digest.update(string.toByteArray())
                val a = digest.digest()
                val len = a.size
                val sb = StringBuilder(len shl 1)
                for (i in 0 until len) {
                    sb.append(Character.forDigit((a[i] and 0xf0.toByte()).toInt().shr(4), 16))
                    sb.append(Character.forDigit(((a[i] and 0x0f.toByte()).toInt()), 16))
                }
                return sb.toString()
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            return null
        }
    }
}
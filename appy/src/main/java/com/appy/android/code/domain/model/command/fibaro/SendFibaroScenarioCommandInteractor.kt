package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.error.APErrorFactory
import com.appy.android.code.data.rest.service.FibaroService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
internal class SendFibaroScenarioCommandInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendFibaroScenarioCommandForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendFibaroScenarioCommandForm): Observable<CommandApiResult> {
        return restClient
            .createAPIService(FibaroService::class.java, params.getUsername()!!, params.getPassword()!!)
            .startScene(params.getURL(), params.body)
            .map {
                if (!it.isSuccessful) {
                    throw APErrorFactory.create(it)
                }

                CommandApiResult()
            }
    }
}
package com.sansiri.homeservice.ui.inspection

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.model.api.Inspection
import kotlinx.android.synthetic.main.view_inspection.view.*

/**
 * Created by sansiri on 11/1/17.
 */

class InspectionViewHolder(val view: View, val itemClick: (String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(inspection: Inspection) {
        with(view) {
            textTitle.text = inspection.title
            textSubtitle.text = inspection.typeName
            textStatus.text = inspection.statusText
        }

    }

}
package com.sansiri.homeservice.model.api

data class Token(val token: String)
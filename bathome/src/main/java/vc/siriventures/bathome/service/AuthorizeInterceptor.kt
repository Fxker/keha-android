package vc.siriventures.bathome.service

import android.app.Activity
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import okhttp3.*

import java.io.IOException

import vc.siriventures.bathome.model.GatewayInfoRequest
import vc.siriventures.bathome.model.GatewayInfoResponse
import android.net.Uri
import android.util.TimeUtils
import com.digithai.bathome.BatHomeLib
import io.reactivex.Observable
import java.lang.Exception
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit


class AuthorizeInterceptor(private val activity: Activity, val projectId: String, val username: String, val password: String) : Interceptor {
    val batHomeAuth = BAtHomeAuth(activity)
    companion object {
        var cacheProjectId: String = ""
        var cacheUsername: String = ""
        var cachePassword: String = ""
        var actualUri: Uri? = null
        var authorizeTimeStamp: Long? = null
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (cacheProjectId != projectId || cacheUsername != username || cachePassword != password) {
            cacheProjectId = projectId
            cacheUsername = username
            cachePassword = password

            try {
                authorize(chain)
                actualUri?.let { request = buildRequestWithActualUri(request, it) }
            } catch (e: JsonSyntaxException) {

            } catch (e: Exception) {
                Log.e("", "")
            }
        } else {
            if (actualUri == null || isAuthorizationExpire()) {
                try {
                    authorize(chain)
                    actualUri?.let { request = buildRequestWithActualUri(request, it) }
                } catch (e: JsonSyntaxException) {

                } catch (e: Exception) {
                    Log.e("", "")
                }
            } else {
                request = buildRequestWithActualUri(request, actualUri!!)
            }
        }
        return chain.proceed(request)
    }

    private fun authorize(chain: Interceptor.Chain) {
        val requestBody = Gson().toJson(GatewayInfoRequest(cacheProjectId, cacheUsername, cachePassword))

        val authorizeRequest = Request.Builder()
                .method("POST", RequestBody.create(MediaType.parse("application/json"), requestBody))
                .url("http://iot.digithaigroup.com:5679/BatHomeWebService.svc/authenticate")
                .build()

//        BatHomeLib().authenticate()

        val response = chain.proceed(authorizeRequest)

        if (response.body() != null) {
            val gatewayInfoResponse = Gson().fromJson(response.body()!!.string(), GatewayInfoResponse::class.java)

            val host = selectHost(gatewayInfoResponse.privateUrl, gatewayInfoResponse.publicUrl)
            actualUri = Uri.parse(host)
            authorizeTimeStamp = System.currentTimeMillis()
        }
    }

    private fun isAuthorizationExpire(): Boolean {
        if (authorizeTimeStamp != null) {
            val diff = System.currentTimeMillis() - authorizeTimeStamp!!
            if (diff < TimeUnit.MINUTES.toMillis(4)) {
                return false
            }
        }

        return true
    }

    private fun selectHost(privateUrl: String?, publicUrl: String?): String {
        if (privateUrl != null) {
            val uri = Uri.parse(privateUrl)
            if (uri != null && uri.host != null) {
                val process = Runtime.getRuntime().exec("/system/bin/ping -c 1 ${uri.host}")
                val returnVal = process.waitFor()
                Log.d("oakraw ping", "${uri.host} $returnVal")
                if (returnVal == 0) {
                    return privateUrl
                }
            }
        }

        if (publicUrl != null) {
            return publicUrl
        }

        return ""
    }

    private fun buildRequestWithActualUri(request: Request, actualUri: Uri): Request {
        val newUrl = request.url().newBuilder()
                .scheme(actualUri.scheme)
                .host(actualUri.host)
                .port(actualUri.port)
                .build()

        return request.newBuilder()
                .url(newUrl)
                .build()
    }
}
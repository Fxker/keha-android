package com.appy.android.code.domain.model.home

import com.appy.android.code.data.rest.form.AppyForm

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal open class HomeForm(
        internal val homeId: String
) : AppyForm()
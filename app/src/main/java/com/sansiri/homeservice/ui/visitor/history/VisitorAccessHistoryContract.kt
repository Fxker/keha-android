package com.sansiri.homeservice.ui.visitor.history

import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface VisitorAccessHistoryContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String, page: Int)
    }

    interface View : BaseView {
        fun bindData(list: List<VisitorAccessHistory>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
    }
}
package com.sansiri.homeservice.ui.parking.smartparking

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SmartParkingContract {
    interface Presenter : BasePresenter<View> {
        var smartParkingStatus: MutableLiveData<Resource<SmartParkingStatus>>?
        fun init(unitId: String)
        fun fetch()
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun bindData(smartParkingStatus: SmartParkingStatus)
        fun setFetchingObserver(data: MutableLiveData<Resource<SmartParkingStatus>>)
    }
}
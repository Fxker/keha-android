package com.sansiri.homeservice.ui.parking.smartparking

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.random
import kotlinx.android.synthetic.main.view_parking_reservation.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ReservationAdapter() : RecyclerView.Adapter<ReservationAdapter.ViewHolder>() {
    private val mData = mutableListOf<SmartParkingStatus.Reservation>()
    override fun getItemCount(): Int = mData.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_parking_reservation))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    fun setData(data: List<SmartParkingStatus.Reservation>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(reservation: SmartParkingStatus.Reservation) = with(view) {
            textNo.text = "${reservation.no}."
            textTitle.text = reservation.cardId
            clockAnim.speed = 0.03f
//            clockAnim.frame = (reservation.timeRemain % 12) * 30
            textTime.text = reservation.timeRemainDisplay
        }
    }
}
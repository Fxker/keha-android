package com.sansiri.homeservice.model.api

import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class IBoxItem(
        val createdAt: String? = null,
        val firstname: String? = null,
        val partnerIboxId: String? = null,
        val middlename: String? = null,
        val iboxName: String? = null,
        val fullname: String? = null,
        val objectId: String? = null,
        val referenceId: String? = null,
        val lastname: String? = null,
        val updatedAt: String? = null
) {
    val _createdAt: Date?
        get() {
            try {
                return DateTime(this.createdAt).toDate()
            } catch (e: ParseException) {
                return null
            }
        }
}

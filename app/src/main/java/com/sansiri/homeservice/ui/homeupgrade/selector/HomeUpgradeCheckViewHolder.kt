package com.sansiri.homeservice.ui.homeupgrade.selector

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_home_upgrade_check.view.*
import kotlinx.android.synthetic.main.view_menu_card_no_title.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class HomeUpgradeCheckViewHolder(val view: View, val itemSelected: (HomeUpgradeHardware) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: HomeUpgradeHardware) {
        with(view) {
            textTitle.text = data.name
            textDescription.text = data.description
            Glide.with(this).loadCache(data.imageUrl ?: "", imageLogo as ImageView)

            toggleLayout.addRelatedChild(textTitle, textDescription, imageLogo)

            logo.setOnClickListener {
                checkbox.isChecked = !checkbox.isChecked
            }

            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                toggleLayout.isChecked = isChecked
                data.amount =  if (isChecked) 1 else 0
                itemSelected(data)
            }

            toggleLayout.isChecked = data.amount > 0
            checkbox.isChecked = data.amount > 0
        }
    }
}

/*
* class HomeUpgradeCheckViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private lateinit var textTitle: TextView
    private lateinit var textDescription: TextView
    private lateinit var imageLogo: ImageView
    private lateinit var checkbox: CheckBox
    private lateinit var toggleLayout: ToggleLayout
    private lateinit var itemSelected: (HomeUpgradeHardware) -> Unit

    constructor(view: View, itemSelected: (HomeUpgradeHardware) -> Unit) : this(view) {
        with(view) {
            textTitle = findViewById(R.id.textTitle)
            textDescription = findViewById(R.id.textDescription)
            imageLogo = findViewById(R.id.imageLogo)
            checkbox = findViewById(R.id.checkbox)
            toggleLayout = findViewById(R.id.toggleLayout)
        }
        this.itemSelected = itemSelected
    }

    fun bind(html: HomeUpgradeHardware) {
        with(view) {

            textTitle.text = html.name
            textDescription.text = html.description
            Glide.with(this).loadCache(html.imageUrl ?: "", imageLogo as ImageView)

            toggleLayout.addRelatedChild(textTitle, textDescription, imageLogo)
            toggleLayout.isChecked = false

            findViewById<View>(R.id.logo).setOnClickListener {
                checkbox.isChecked = !checkbox.isChecked
            }

            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                toggleLayout.isChecked = isChecked
                html.amount = if (isChecked) 1 else 0
                itemSelected(html)
            }

        }
    }
}*/
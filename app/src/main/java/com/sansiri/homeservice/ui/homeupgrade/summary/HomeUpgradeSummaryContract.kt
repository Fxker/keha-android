package com.sansiri.homeservice.ui.homeupgrade.summary

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeUpgradeSummaryContract {
    interface Presenter : BasePresenter<View> {
        fun init(homeUpgradeRooms: List<HomeUpgradeRoom>, unitId: String)
        fun requestNextStep()
    }

    interface View : BaseView {
        fun bindData(summary: HashMap<String, List<HomeUpgradeHardware>>)
        fun showLoading()
        fun hideLoading()
        fun showTotalPrice(totalPrice: Double)
        fun launchNextStep(orderRequest: ArrayList<HomeUpgradeOrderRequest>, unitId: String)
    }
}
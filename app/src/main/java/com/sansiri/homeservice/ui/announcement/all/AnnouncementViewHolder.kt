package com.sansiri.homeservice.ui.announcement.all

import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.announcement.Announcement

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import kotlinx.android.synthetic.main.view_announcement_large.view.*

/**
 * Created by sansiri on 1/19/18.
 */
class AnnouncementViewHolder(val view: View, val itemClick: (View, Announcement<*>) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(announcement: Announcement<*>) {
        with(view) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                card.preventCornerOverlap = false
            }
            val detail = announcement.details?.getOrNull(0)
            textTitle.text = detail?.title
            textSubtitle.text = detail?.subTitle
            textDate.text = announcement.displayDate?.report()
            Glide.with(context).loadCache(detail?.coverImageUrl ?: "", imageThumbnail)
            panelClickable.setOnClickListener {
                itemClick(imageThumbnail, announcement)
            }
        }
    }
}
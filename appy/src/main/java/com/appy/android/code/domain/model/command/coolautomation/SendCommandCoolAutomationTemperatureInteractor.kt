package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
internal class SendCommandCoolAutomationTemperatureInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendCommandCoolAutomationTemperatureForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendCommandCoolAutomationTemperatureForm): Observable<CommandApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .sendCommandCoolAutomationTemperature(params.headers, params.homeId, params.coolId, params.mSendCommandCoolAutomationTemperatureJsonBody)
    }
}
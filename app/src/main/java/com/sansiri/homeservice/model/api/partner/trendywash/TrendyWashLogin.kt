package com.sansiri.homeservice.model.api.partner.trendywash

import com.google.gson.annotations.SerializedName

data class TrendyWashLogin (
        @SerializedName("mobileno")
        val mobileNo: String,
        val password: String
)
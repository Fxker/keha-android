package com.sansiri.homeservice.ui.facilitybooking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.Reservation
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class FacilityBookingActivity : BaseActivity<FacilityBookingContract.View, FacilityBookingContract.Presenter>(), FacilityBookingContract.View {

    override var mPresenter: FacilityBookingContract.Presenter = FacilityBookingPresenter()

    companion object {
        val PROJECT_ID = "PROJECT_ID"
        val UNIT_ID = "UNIT_ID"
        val ADDRESS = "ADDRESS"
        val TITLE = "TITLE"

        fun start(activity: Activity, projectId: String, unitId: String, address: String, title: String?) {
            val intent = Intent(activity, FacilityBookingActivity::class.java)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(TITLE, title)
            intent.putExtra(ADDRESS, address)
            activity.startActivity(intent)
        }
    }

    val mAdapter = GridMenuAdapter(null, null) {
        mPresenter.requestToLaunchCalendar(it.title ?: "", it.id ?: "")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent?.getStringExtra(TITLE) ?: getString(R.string.facility_booking))

        list.apply {
            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
            adapter = mAdapter
        }

        val projectId = intent?.getStringExtra(PROJECT_ID)
        val unitId = intent?.getStringExtra(UNIT_ID)
        val address = intent?.getStringExtra(ADDRESS)

        mPresenter.init(projectId ?: "", unitId ?: "", address ?: "")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("Facility Booking Screen")
    }

    override fun bindFacility(menu: List<Menu>) {
        mAdapter.setDataDelay(menu)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(text: String) {
        alert(text, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }


    override fun showNoData() {
        textNoData.show()
    }

    override fun launchCalendar(projectId: String, unitId: String, address: String, facility: FacilityBooking) {
        FacilityBookingDetailActivity.start(this, projectId, unitId, address, facility)
    }
}

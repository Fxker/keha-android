package com.appy.android.sdk.control.coolautomation

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
enum class CoolAutomationCommand : APCommand {
    OFF {
        override fun action(): String {
            return "CONTROL_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "CONTROL_ON"
        }
    },
    MODE_COOL {
        override fun action(): String {
            return "AC_MODE_COOL"
        }
    },
    MODE_FAN {
        override fun action(): String {
            return "AC_MODE_FAN"
        }
    },
    TEMPERATURE {
        override fun action(): String {
            return "AC_SET_TEMP"
        }
    },
    FAN_SPEED {
        override fun action(): String {
            return "AC_SET_FAN_SPEED"
        }
    },
    FAN_SWING {
        override fun action(): String {
            return "AC_SET_FAN_SWING"
        }
    };
}
package com.sansiri.homeservice.ui.automation.orvibo.home

//import com.homemate.sdk.model.HMFamily
//import com.sansiri.homeservice.ui.base.BasePresenter
//import com.sansiri.homeservice.ui.base.BaseView
//
//interface OrviboHomeContract {
//    interface Presenter : BasePresenter<View> {
//        fun fetch()
//        fun switchFamily(familyId: String?)
//        fun logout()
//        fun init(unitId: String)
//    }
//
//    interface View : BaseView {
//        fun launchLoginPage()
//        fun bindFamilies(families: List<HMFamily>)
//        fun bindCurrentFamily(currentFamily: HMFamily)
//        fun hideFamilySelector()
//        fun logoutSuccess(): Any
//    }
//}
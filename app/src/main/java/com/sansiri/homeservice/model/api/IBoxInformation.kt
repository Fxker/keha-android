package com.sansiri.homeservice.model.api

data class IBoxInformation(
	val iboxMobileNo: String? = null
)

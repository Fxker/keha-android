package com.appy.android.code.base.domain.executor

import java.util.concurrent.Executor

interface ThreadExecutor : Executor

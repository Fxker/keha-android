package com.sansiri.homeservice.ui.homecare.history.detail


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_homecare_history_detail.*

class HomeCareHistoryDetailActivity : BaseV2Activity<HomeCareHistoryDetailContract.View, HomeCareHistoryDetailContract.Presenter>(), HomeCareHistoryDetailContract.View {
    override var mPresenter: HomeCareHistoryDetailContract.Presenter = HomeCareHistoryDetailPresenter()

    val mAdapter = HomeCareHistoryDetailAdapter{}

    companion object {
        val HOME_CARE_HISTORY = "HOME_CARE_HISTORY"
        val TITLE = "TITLE"
        fun start(activity: Activity, homeCareHistory: HomeCareHistory, title: String?) {
            val intent = Intent(activity, HomeCareHistoryDetailActivity::class.java)
            intent.putExtra(HOME_CARE_HISTORY, homeCareHistory)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homecare_history_detail)

        intent?.getParcelableExtra<HomeCareHistory>(HOME_CARE_HISTORY)?.let { mAdapter.setData(it) }

        setBackToolbar(intent.getStringExtra(TITLE) ?: "")

        detail_list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@HomeCareHistoryDetailActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("HOMECARE_HISTORY_DETAIL")
    }

    override fun showError(message: String) {
        alertError(message)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
package com.sansiri.homeservice.model.api.announcement

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by sansiri on 12/1/17.
 */
class ProjectAnnouncement(
        @SerializedName("objectId")
        override var objectId: String? = null,
        @SerializedName("isAutoActivate")
        override var isAutoActivate: Boolean? = null,
        @SerializedName("isSupportNonUser")
        override var isSupportNonUser: Boolean? = null,
        @SerializedName("isActive")
        override var isActive: Boolean? = null,
        @SerializedName("updatedAt")
        override var updatedAt: String? = null,
        @SerializedName("createdAt")
        override var createdAt: String? = null,
        @SerializedName("publishedAt")
        override var publishedAt: String? = null,
        @SerializedName("createdBy")
        override var createdBy: String? = null,
        @SerializedName("details")
        override var details: List<ProjectAnnouncementDetail>? = null) : Announcement<ProjectAnnouncementDetail>, Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.createTypedArrayList(ProjectAnnouncementDetail.CREATOR)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(objectId)
                writeValue(isAutoActivate)
                writeValue(isSupportNonUser)
                writeValue(isActive)
                writeString(updatedAt)
                writeString(createdAt)
                writeString(publishedAt)
                writeString(createdBy)
                writeTypedList(details)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<ProjectAnnouncement> = object : Parcelable.Creator<ProjectAnnouncement> {
                        override fun createFromParcel(source: Parcel): ProjectAnnouncement = ProjectAnnouncement(source)
                        override fun newArray(size: Int): Array<ProjectAnnouncement?> = arrayOfNulls(size)
                }
        }
}
package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_home_automation_selector.view.*
import kotlinx.android.synthetic.main.view_icon_and_title.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class SelectorViewHolder(val view: View, val overlayColor: Int, val itemClick: (HomeAutomation, Int) -> Unit) : LifecycleViewHolder(view) {

    fun bind(data: HomeAutomation) {
        with(view) {
            if (data is AppyHomeAutomation) {
                data.apControl?.icon?.let {
                    Glide.with(this).loadCache(it, imageLogo)
                }
            } else {
                imageLogo.setImageResource(data.icon)
            }

            imageLogo.setColorFilter(context.resources.getColor(overlayColor))
            textTitle.text = data.title

            buttonPrevious.setOnClickListener {

            }

            buttonNext.setOnClickListener {

            }
        }
    }
}
package com.sansiri.homeservice.model.api.downpayment

import android.os.Parcelable
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class DownpaymentItem(
        var dueDate: String? = null,
        val isPaid: Boolean,
        val cost: Double,
        val isOverdue: Boolean,
        val paymentId: Int? = null,
        val paidDate: String? = null,
        val title: String? = null,
        var paymentBarcode: String? = null,
        val isSupportEpayment: Boolean = false,
        val paymentChannels: List<PaymentChannel>? = null,
        val reference1: String? = null,
        val reference2: String? = null,
        val taxId: String? = null
) : Parcelable {
    val date: Date?
        get() {
            return try {
                if (this.dueDate == null) return null

                val date = DateTime(this.dueDate).toDate()
                date
            } catch (e: ParseException) {
                null
            }
        }

}
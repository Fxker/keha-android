package com.sansiri.homeservice.model.api.partner.mea

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Consent(
	val createdAt: String? = null,
	val description: String? = null,
	val version: Int? = null,
	val objectId: String? = null,
	val updatedAt: String? = null
): Parcelable

package com.appy.android.code.base.models

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal abstract class BaseControlJsonModel {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("class")
    var klass: String? = null
}
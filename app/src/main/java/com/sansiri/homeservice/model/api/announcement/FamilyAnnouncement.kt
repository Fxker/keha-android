package com.sansiri.homeservice.model.api.announcement

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by sansiri on 12/1/17.
 */
class FamilyAnnouncement(
        @SerializedName("objectId")
        override var objectId: String? = null,
        @SerializedName("isAutoActivate")
        override var isAutoActivate: Boolean? = null,
        @SerializedName("isSupportNonUser")
        override var isSupportNonUser: Boolean? = null,
        @SerializedName("isActive")
        override var isActive: Boolean? = null,
        @SerializedName("updatedAt")
        override var updatedAt: String? = null,
        @SerializedName("publishedAt")
        override var publishedAt: String? = null,
        @SerializedName("createdAt")
        override var createdAt: String? = null,
        @SerializedName("createdBy")
        override var createdBy: String? = null,
        @SerializedName("details")
        override var details: List<FamilyAnnouncementDetail>? = null) : Announcement<FamilyAnnouncementDetail>, Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readValue(Boolean::class.java.classLoader) as Boolean?,
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.createTypedArrayList(FamilyAnnouncementDetail.CREATOR)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(objectId)
                writeValue(isAutoActivate)
                writeValue(isSupportNonUser)
                writeValue(isActive)
                writeString(updatedAt)
                writeString(publishedAt)
                writeString(createdAt)
                writeString(createdBy)
                writeTypedList(details)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<FamilyAnnouncement> = object : Parcelable.Creator<FamilyAnnouncement> {
                        override fun createFromParcel(source: Parcel): FamilyAnnouncement = FamilyAnnouncement(source)
                        override fun newArray(size: Int): Array<FamilyAnnouncement?> = arrayOfNulls(size)
                }
        }
}
package com.sansiri.homeservice.data.network.auth

import android.util.Base64
import com.appy.android.sdk.home.APHome
import com.appy.android.sdk.room.APRoom
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.model.api.*
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.SuggestionRequest
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBookingRequest
import com.sansiri.homeservice.model.api.homecare.HomeCareRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRequest
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDetailSubmit
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashLogin
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachineStart
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGatewayRequest
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashRegisterCustomer
import com.sansiri.homeservice.util.observe
import io.reactivex.Observable
import okhttp3.ResponseBody
import vc.siriventures.hsa.admin.model.api.FileRequest
import vc.siriventures.hsa.admin.model.api.FileResponse


/**
 * Created by sansiri on 10/24/17.
 */
class ApiAuthRepository(val idToken: String, val apiService: ApiAuthService, val language: String) {
    val TAG = "ApiAuthRepository"

    fun bindUser(customerId: String): Observable<ResponseBody> = apiService.bindUser(idToken, customerId)

    fun getHome(): Observable<MutableList<Home>> {
        RuntimeCache.clear()

        return Observable.create<MutableList<Home>> { emitter ->
            val homeList = mutableListOf<Home>()


            getAllUnits().observe().flatMapIterable { list -> list }
                    .map { return@map createHomeFromUnit(it) }
                    .toList()
                    .subscribe({ home ->
                        var taskCount = 0
                        val taskNumber = 4

                        homeList.addAll(home)
                        emitter.onNext(homeList)
                        taskCount += 1

                        getAllProjects().observe().subscribe({ projects ->
                            RuntimeCache.projects.addAll(projects)
                            assignProjectToHome(homeList, projects)
                            // Store.saveHome(homeList)
                            emitter.onNext(homeList)

                            taskCount += 1
                            if (taskCount == taskNumber) emitter.onComplete()
                        }) {
                            emitter.onError(it)
                        }

                        getMenu().observe().subscribe({ menu ->
                            homeList.forEach { home ->
                                val unitMenu = menu[home.unitId]

                                home.section = unitMenu
                            }
                            emitter.onNext(homeList)

                            taskCount += 1
                            if (taskCount == taskNumber) emitter.onComplete()

                        }) {
                            emitter.onError(it)
                        }


                        getAllProjectAnnouncement().observe().subscribe({ announcements ->
                            homeList.forEach { home ->
                                val announcement = announcements[home.projectId]
                                announcement?.let { home.announcement = announcement }
                            }
                            emitter.onNext(homeList)

                            taskCount += 1
                            if (taskCount == taskNumber) emitter.onComplete()

                        }) {
                            emitter.onError(it)
                        }

                    }) {
                        emitter.onError(it)
                    }
        }
    }

    fun getHomeless(): Observable<MutableList<Home>> {
        val homeList = mutableListOf<Home>()
        return Observable.create<MutableList<Home>> { emitter ->
            getAllUnits().observe().flatMapIterable { list -> list }
                    .map { return@map createHomeFromUnit(it) }
                    .toList()
                    .subscribe({ home ->
                        homeList.addAll(home)
                        getAllProjects().observe().subscribe({ projects ->
                            RuntimeCache.projects.addAll(projects)
                            assignProjectToHome(homeList, projects)
                            // Store.saveHome(homeList)
                            emitter.onNext(homeList)
                        }) {
                            emitter.onError(it)
                        }
                    }) {
                        emitter.onError(it)
                    }
        }
    }


    private fun createHomeFromUnit(unit: UnitInfo) = Home(
            unit.objectId,
            "",
            "",
            "",
            unit.address,
            unit.projectId,
            0.0,
            0.0,
            unit.homeAutomationGateway,
            unit.isSupportHomeUpgrade,
            unit.homeUpgradeRequestId,
            unit.isTransfer
    )

    private fun assignProjectToHome(homeList: List<Home>, projects: List<ProjectInfo>) {
        homeList.forEach { home ->
            val foundProject = projects.find { it.objectId == home.projectId }
            foundProject?.let {
                home.image = it.coverImageUrl
                home.icon = it.iconImageUrl
                home.title = it.name
                home.lat = it.lat
                home.lng = it.long
            }
        }
    }

    fun getAllUnits() = apiService.getAllUnits(idToken).map { return@map it.items }

    fun getAllProjects(): Observable<List<ProjectInfo>> {
        if (RuntimeCache.projects.isNotEmpty()) {
            return Observable.just(RuntimeCache.projects)
        }
        return apiService.getAllProjects(idToken, language).map { return@map it.items }
    }

    fun getTodaySummary() = apiService.getTodaySummary(idToken, language)

    fun getMenu() = apiService.getDynamicButton(idToken, language).map {
        RuntimeCache.dynamicButton = it
        return@map it
    }

    fun getHomeAutomationRoom(unitId: String): Observable<List<APRoom>> {
        val home = APHome()
        home.id = unitId
        return AppyRepositoryImpl().getRooms(home)
    }

    fun getFamilyAnnouncement(offSet: Int, limit: Int) = apiService.getFamilyAnnouncement(idToken, language, offSet, limit).map { return@map it.items }

    fun getAllProjectAnnouncement() = apiService.getAllProjectAnnouncement(idToken, language)

    fun getCentralProjectAnnouncement(offSet: Int, limit: Int) = apiService.getCentralProjectAnnouncement(idToken, language, offSet, limit).map { return@map it.items }

    fun getProjectAnnouncement(projectId: String, offSet: Int, limit: Int) = apiService.getProjectAnnouncement(idToken, language, projectId, offSet, limit).map { return@map it.items }

    fun getContactCR(unitId: String) = apiService.getContact(idToken, language, unitId)

    fun getBalance(unitId: String) = apiService.getBalance(idToken, language, unitId)

    fun getInvoicePayable(unitId: String) = apiService.getInvoicePayable(idToken, language, unitId)

    fun getBalanceV2(unitId: String) = apiService.getBalanceV2(idToken, language, unitId)

    fun getBalanceHistory(unitId: String, offSet: Int, limit: Int) = apiService.getBalanceHistory(idToken, language, unitId, offSet, limit)

    fun getInvoicePayableV2(unitId: String, offSet: Int, limit: Int) = apiService.getInvoicePayableV2(idToken, language, unitId, offSet, limit)

    fun getMyHome(unitId: String) = apiService.getMyHome(idToken, language, unitId)

    fun getMyHomeDocument(unitId: String) = apiService.getMyHomeDocument(idToken, language, unitId).map { return@map it.items }

    fun getTransferDoc(unitId: String) = apiService.getTransferDoc(idToken, language, unitId)

    fun getTransferCost(unitId: String) = apiService.getCostDoc(idToken, language, unitId)

    fun getDownpayment(unitId: String) = apiService.getDownpayment(idToken, language, unitId)

    fun getMailBox(unitId: String) = apiService.getMailBox(idToken, language, unitId).map { return@map it.items }

    fun getMailBoxHistory(unitId: String, offSet: Int, limit: Int) = apiService.getMailBoxHistory(idToken, language, unitId, offSet, limit).map { it.items }

    fun getInspection(unitId: String) = apiService.getInspection(idToken, language, unitId)
    fun registerIBox(unitObjectId: String, request: IBoxInformation) = apiService.registerIBox(idToken, language, unitObjectId, request)

    fun getIBox(unitObjectId: String, offSet: Int, limit: Int) = apiService.getIBox(idToken, language, unitObjectId, offSet, limit).map { return@map it.items }

    fun getMe(): Observable<Profile> {
        if (RuntimeCache.me != null) {
            return Observable.just(RuntimeCache.me)
        }
        return apiService.me(idToken, language).map {
            RuntimeCache.me = it
            return@map it
        }
    }

    fun getQR(): Observable<Data> = apiService.getQR(idToken)

    fun uploadFile(filename: String, byteArray: ByteArray): Observable<List<String>> {
        val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
        val file = UploadFile(filename, encoded)
        return apiService.uploadFile(idToken, file)
    }

    fun createFileToUpload(filename: String): Observable<FileResponse> {
        val file = FileRequest(filename)
        return apiService.createFileToUpload(idToken, file)
    }

    fun submitSuggestion(unitId: String, suggestionRequest: SuggestionRequest) = apiService.submitSuggestion(idToken, unitId, suggestionRequest)

    fun getHomeCareHistory(unitId: String, limit: Int, offset: Int, orderBy: String? = null, statusId: String? = null) = apiService.getHomeCareHistory(idToken, language, unitId, limit.toString(), offset.toString(), orderBy, statusId)

    fun submitHomeCare(unitId: String, homeCareRequest: HomeCareRequest) = apiService.submitHomeCare(idToken, language, unitId, homeCareRequest)

    fun registerNotification(deviceId: String, registerNotificationRequest: RegisterNotificationRequest) = apiService.registerNotification(idToken, deviceId, registerNotificationRequest)

    fun unregisterNotification(deviceId: String) = apiService.unregisterNotification(idToken, deviceId)

    fun notifyJuristic(projectId: String, unitId: String, chat: ChatMessage) = apiService.notifyJuristic(idToken, projectId, unitId, chat)

    fun getFacilityBooking(projectId: String, includeCategoryData: Boolean = false) = apiService.getFacilityBooking(idToken, language, projectId, includeCategoryData.toString()).map { it.data }

    fun getFacilityBookingById(projectId: String, facilityId: String) = apiService.getFacilityBookingById(idToken, language, projectId, facilityId).map { it.data }

    fun submitFacilityBooking(projectId: String, unitId: String, request: FacilityBookingRequest) = apiService.submitFacility(idToken, language, projectId, unitId, request)

    fun cancelFacilityBooking(unitId: String, bookingId: String) = apiService.cancelFacility(idToken, language, unitId, bookingId)

    fun getHomeUpgradeList(unitId: String) = apiService.getHomeUpgradeList(idToken, language, unitId).map { return@map it.items }

    fun submitHomeUpgrade(unitId: String, request: HomeUpgradeRequest) = apiService.submitHomeUpgrade(idToken, language, unitId, request)

    fun getHomeUpgradeHistory(unitId: String) = apiService.getHomeUpgradeHistory(idToken, language, unitId)

    fun getPopUp() = apiService.getPopUp(idToken, language).map { return@map it.items }

    fun getVisitorAccessOptions(unitId: String) = apiService.getVisitorAccessOptions(idToken, language, unitId)

    fun submitVisitorAccess(unitId: String, visitorAccessRequest: VisitorAccessRequest) = apiService.submitVisitorAccess(idToken, language, unitId, visitorAccessRequest)

    fun getVisitorAccessHistory(unitId: String, offSet: Int, limit: Int) = apiService.getVisitorAccessHistory(idToken, language, unitId, offSet, limit).map { it.items }

    fun getNearestMaintenancePlan(unitId: String) = apiService.getNearestMaintenancePlan(idToken, language, unitId).map { it.items }

    fun getMaintenanceDevice(unitId: String) = apiService.getMaintenanceDevice(idToken, language, unitId).map { it.items }

    fun getMaintenancePlan(unitId: String, planId: String) = apiService.getMaintenancePlan(idToken, language, unitId, planId).map { it.items }

    fun getMaintenanceAccessory(unitId: String, accessoryId: String) = apiService.getMaintenanceAccessory(idToken, language, unitId, accessoryId)

    fun submitMaintenanceAccessory(unitId: String, accessoryId: String, body: MaintenanceDetailSubmit) = apiService.submitMaintenanceAccessory(idToken, language, unitId, accessoryId, body)

    fun getGoogleAssistant() = apiService.getGoogleAssistant(idToken, language).map { return@map it.items }

    fun bindGoogleAssistant(unitId: String, googleJwt: GoogleJwt) = apiService.bindGoogleAssistant(idToken, language, unitId, googleJwt)

    fun updateGoogleAssistant(unitId: String, googleId: String, googleAssistant: GoogleAssistant) = apiService.updateGoogleAssistant(idToken, language, unitId, googleId, googleAssistant)

    fun deleteGoogleAssistant(googleId: String) = apiService.deleteGoogleAssistant(idToken, language, googleId)

    fun getSVVHCustomer(unitId: String) = apiService.getSVVHCustomer(idToken, language, unitId)

    fun getSVVHPromotion(unitId: String) = apiService.getSVVHPromotion(idToken, language, unitId)

    fun submitSVVH(unitId: String, request: SVVHRegisterCustomer) = apiService.submitSVVH(idToken, language, unitId, request)

    fun registerTrendyWashAccount(unitId: String, request: TrendyWashRegisterCustomer) = apiService.registerTrendyWashAccount(idToken, language, unitId, request)

    fun loginTrendyWash(unitId: String, request: TrendyWashLogin) = apiService.loginTrendyWash(idToken, language, unitId, request)

    fun getTrendyWashWashingMachines(unitId: String) = apiService.getTrendyWashWashingMachines(idToken, language, unitId)

    fun getTrendyWashCredit(unitId: String, trendyWashUserId: String) = apiService.getTrendyWashCredit(idToken, language, unitId, trendyWashUserId)

    fun checkoutTrendyWashPaymentQR(unitId: String, trendyWashUserId: String, request: TrendyWashPaymentGatewayRequest) = apiService.checkoutTrendyWashPaymentQR(idToken, language, unitId, trendyWashUserId, request)

    fun checkoutTrendyWashCreditCard(unitId: String, trendyWashUserId: String, request: TrendyWashPaymentGatewayRequest) = apiService.checkoutTrendyWashCreditCard(idToken, language, unitId, trendyWashUserId, request)

    fun startTrendyWashMachine(unitId: String, trendyWashUserId: String, machineId: String, request: TrendyWashMachineStart) = apiService.startTrendyWashMachine(idToken, language, unitId, trendyWashUserId, machineId, request)

    fun getTutorial(menu: String) = apiService.getTutorial(idToken, language, menu)
}
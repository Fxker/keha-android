package com.sansiri.homeservice.ui.epayment.gateway.phone

import com.sansiri.homeservice.data.repository.payment.PaymentRepository
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class PaymentGatewayPhoneVerifyPresenter(val repository: PaymentRepository) : BasePresenterImpl<PaymentGatewayPhoneVerifyContract.View>(), PaymentGatewayPhoneVerifyContract.Presenter {
    private var mPaymentGatewayData: PaymentGatewayData? = null

    override fun init(paymentGatewayData: PaymentGatewayData) {
        mPaymentGatewayData = paymentGatewayData
    }

    override fun start() {
        if (mPaymentGatewayData != null) {
            mView?.bindData(mPaymentGatewayData!!)
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun verifyPhone(phoneNumber: String) {
        if (mPaymentGatewayData?.command != null) {
            mView?.showLoading()
            mCompositeDisposable.add(
                    repository.requestPayment(PaymentRequest(
                            mPaymentGatewayData?.command,
                            phoneNumber
                    )).observe().subscribe({ response ->
                        mView?.hideLoading()
//                        response.message?.let { mView?.showPopup(it) }
                        mView?.processResponse(response)
                    }) { error ->
                        mView?.hideLoading()
                        mView?.showError(error.showHttpError())
                    }
            )
        }
    }
}
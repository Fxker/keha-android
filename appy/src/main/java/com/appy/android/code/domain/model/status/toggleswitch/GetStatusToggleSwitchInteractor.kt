package com.appy.android.code.domain.model.status.toggleswitch

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.status.light.StatusLightApiResult
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
internal class GetStatusToggleSwitchInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<StatusToggleSwitchApiResult, GetStatusToggleSwitchForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetStatusToggleSwitchForm): Observable<StatusToggleSwitchApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getStatusToggleSwitch(params.headers, params.homeId, params.switchId, params.params)
    }
}
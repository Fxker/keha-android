package com.sansiri.homeservice.component.bottomsheet

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.Resources
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import android.widget.DatePicker
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.reportDMYFull
import kotlinx.android.synthetic.main.view_date_picker.*
import java.util.*


/**
 * Created by sansiri on 10/12/17.
 */
class DatePickerBottomSheet() : BottomSheetDialogFragment() {

    var onDatePickerChanged: ((Calendar) -> Unit)? = null
    private var mDatePicker: DatePicker? = null

    private var mCalendar: Calendar? = null
    private var minDate: Calendar? = null
    private var maxDate: Calendar? = null
    private var year: Int = -1
    private var monthOfYear: Int = -1
    private var dayOfMonth: Int = -1
    private var hideDate = false

    @SuppressLint("ValidFragment")
    constructor(initDate: Calendar, hideDate: Boolean, onDateChanged: (Calendar) -> Unit) : this() {
        this.mCalendar = initDate
        this.year = initDate[Calendar.YEAR]
        this.monthOfYear = initDate[Calendar.MONTH]
        this.dayOfMonth = initDate[Calendar.DAY_OF_MONTH]
        this.onDatePickerChanged = onDateChanged
        this.hideDate = hideDate
    }


    @SuppressLint("ValidFragment")
    constructor(initDate: Calendar, hideDate: Boolean, minDate: Calendar?, maxDate: Calendar?, onDateChanged: (Calendar) -> Unit) : this() {
        this.mCalendar = initDate
        this.minDate = minDate
        this.maxDate = maxDate
        this.year = initDate[Calendar.YEAR]
        this.monthOfYear = initDate[Calendar.MONTH]
        this.dayOfMonth = initDate[Calendar.DAY_OF_MONTH]
        this.onDatePickerChanged = onDateChanged
        this.hideDate = hideDate
    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.view_date_picker, null)
        mDatePicker = view.findViewById<DatePicker>(R.id.datePicker)
        view.findViewById<View>(R.id.buttonDone).setOnClickListener {
            mCalendar?.let { this.onDatePickerChanged?.invoke(it) }
            dismiss()
        }

        if (hideDate) {
            mDatePicker?.findViewById<View>(Resources.getSystem().getIdentifier("day", "id", "android"))?.visibility = View.GONE
        }

        mDatePicker?.init(year, monthOfYear, dayOfMonth) { _, year, monthOfYear, dayOfMonth ->
            with(this@DatePickerBottomSheet) {
                this.year = year
                this.monthOfYear = monthOfYear
                this.dayOfMonth = dayOfMonth

                mCalendar?.set(Calendar.YEAR, year)
                mCalendar?.set(Calendar.MONTH, monthOfYear)
                mCalendar?.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            }
        }

        minDate?.let { mDatePicker?.minDate = it.time.time }
        maxDate?.let { mDatePicker?.maxDate = it.time.time }

        dialog.setContentView(view)
    }

}
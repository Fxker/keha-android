package com.sansiri.homeservice.ui.register

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by sansiri on 11/8/17.
 */
class RegisterPresenter : BasePresenterImpl<RegisterContract.View>(), RegisterContract.Presenter {
    private var mCustomerId: String? = null

    override fun start() {
    }

    override fun destroy() {
    }

    override fun bindUser(onSuccess: () -> Unit, onFail: (Throwable) -> Unit) {
        ApiRepositoryProvider.provideApiAuthRepository() {
            if (it != null && !mCustomerId.isNullOrEmpty()) {
                it.bindUser(mCustomerId!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            onSuccess()
                        }, {
                            onFail(it)
                        })
            } else {
                onFail(Throwable("Something went wrong", null))
            }
        }

    }

    override fun storeCustomerId(customerId: String) {
        this.mCustomerId = customerId;
    }
}
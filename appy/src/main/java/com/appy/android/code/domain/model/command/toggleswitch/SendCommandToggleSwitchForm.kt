package com.appy.android.code.domain.model.command.toggleswitch

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand

internal class SendCommandToggleSwitchForm(
        val homeId: String,
        val switchId: String,
        val command: ToggleSwitchCommand
) : AppyForm() {
    val mSendCommandToggleSwitchJsonBody: SendCommandToggleSwitchJsonBody = SendCommandToggleSwitchJsonBody()

    init {
        this.mSendCommandToggleSwitchJsonBody.command = command.action()
    }

}
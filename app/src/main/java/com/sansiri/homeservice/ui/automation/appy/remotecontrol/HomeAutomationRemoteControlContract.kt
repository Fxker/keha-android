package com.sansiri.homeservice.ui.automation.appy.remotecontrol

import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeAutomationRemoteControlContract {
    interface Presenter : BasePresenter<View> {
        fun init(control: APRemoteControl, homeId: String, projectId: String)
        fun action(actionRemoteControl: APActionRemoteControl)
    }

    interface View : BaseView {
        fun bindData(control: List<APActionRemoteControl>)
        fun showMessage(text: String)
    }
}
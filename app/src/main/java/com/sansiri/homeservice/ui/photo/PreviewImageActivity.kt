package com.sansiri.homeservice.ui.photo

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R

import kotlinx.android.synthetic.main.activity_photo.*

class PreviewImageActivity : AppCompatActivity() {

    companion object {
        val URL = "URL"

        fun start(activity: Activity, url: String) {
            val intent = Intent(activity, PreviewImageActivity::class.java)
            intent.putExtra(URL, url)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        photoView.attacher.maximumScale = 10.0f

        intent.getStringExtra(URL)?.let { url ->
            Glide.with(this).load(url)
                    .into(photoView)
        }
    }
}

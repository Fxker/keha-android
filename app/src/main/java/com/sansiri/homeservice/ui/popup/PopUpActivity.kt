package com.sansiri.homeservice.ui.popup

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PopUp
import androidx.core.app.ActivityOptionsCompat
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_pop_up.*
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.web.WebActivity
import android.net.Uri


class PopUpActivity : BaseActivity<PopUpContract.View, PopUpContract.Presenter>(), PopUpContract.View {
    override var mPresenter: PopUpContract.Presenter = PopUpPresenter()

    companion object {
        private const val POP_UPS = "POP_UPS"

        fun start(activity: Activity, popups: ArrayList<PopUp>) {
            val intent = Intent(activity, PopUpActivity::class.java)
            intent.putParcelableArrayListExtra(POP_UPS, popups)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity)
            activity.startActivity(intent, options.toBundle())
        }
    }

    private var mPopUps: ArrayList<PopUp>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.sansiri.homeservice.R.layout.activity_pop_up)

        mPopUps = intent.getParcelableArrayListExtra<PopUp>(POP_UPS)

        if (mPopUps != null) {
            viewPager.apply {
                val defaultGap = resources.getDimension(R.dimen.activity_vertical_margin).toInt()
                val halfGap = resources.getDimension(R.dimen.space_half).toInt()

                setPadding(defaultGap, 0, defaultGap, 0)
                clipToPadding = false
                pageMargin = halfGap

                adapter = PopUpPagerAdapter(mPopUps!!) { popup ->
                    if (popup.actionType == PopUp.EXTERNAL_BROWSER) {
                        sendEvent("POPUP", "EXTERNAL_BROWSER", popup.objectId)
                        launchWeb(popup)
                    } else if (popup.actionType == PopUp.DEEP_LINK) {
                        sendEvent("POPUP", "DEEP_LINK", popup.objectId)
                        popup.url?.let {
                            launchDeepLink(popup.url)
                        }
                    }
                }
                pageIndicatorView.setViewPager(this)
                addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(p0: Int) {
                    }

                    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                    }

                    override fun onPageSelected(position: Int) {
                        if (mPopUps?.get(position) != null) {
                            sendEvent("POPUP", "VIEW", mPopUps?.get(position)?.objectId)
                        }
                    }
                })

                if (mPopUps?.get(0) != null) {
                    sendEvent("POPUP", "VIEW", mPopUps?.get(0)?.objectId)
                }
            }
        } else {
            finish()
        }

        buttonClose.setOnClickListener {
            onBackPressed()
        }

        //        if (popup.objectId != null && !PreferenceHelper.isPopupIsRead(this, popup.objectId)) {
//            sendEvent("POPUP", "VIEW", "${popup.objectId}")
//

//
//            if (popup.actionType == PopUp.EXTERNAL_BROWSER) {
//                dialog.findViewById<View>(R.id.layoutClickable).apply {
//                    setOnClickListener {
//                        popup.url?.let {
//                            sendEvent("POPUP", PopUp.EXTERNAL_BROWSER, "${popup.objectId}")
//                            WebActivity.start(this@MainActivity, it, popup.title ?: "")
//                            if (checkboxDontShowAgain.isChecked) {
//                                PreferenceHelper.setReadPopup(this@MainActivity, popup.objectId)
//                            }
//                            alertDialog.dismiss()
//                        }
//                    }
//                }
//            }
//
//            Glide.with(this)
//                    .load(popup.imageUrl)
//                    .into(dialog.findViewById(R.id.image) as ImageView)
//        }
    }

    private fun launchWeb(popup: PopUp) {
        WebActivity.start(this, popup.url ?: "", popup.title ?: "")
    }

    private fun launchDeepLink(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    private fun setReadPopUp(popups: List<PopUp>) {
        if (checkboxDontShowAgain.isChecked) {
            popups.forEach { popup ->
                if (popup.objectId != null) PreferenceHelper.setReadPopup(this, popup.objectId)
            }
        }
    }

    override fun onBackPressed() {
        setReadPopUp(mPopUps!!)
        super.onBackPressed()
    }

    override fun showError(message: String) {
    }
}

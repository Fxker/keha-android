package com.sansiri.homeservice.model.api.announcement

import android.os.Parcelable
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

interface AnnouncementDetail: Parcelable {
    var createdAt: String?
    var startedAt: String?
    var subTitle: String?
    var countryCode: String?
    var coverImageUrl: String?
    var langCode: String?
    var detailsEncode: String?
    var detailsHtml: String?
    var title: String?
    var updatedAt: String?

    val displayDate: Date?
        get() {
            return try {
                if (this.createdAt == null) return null

                DateTime(this.createdAt).toDate()
            } catch (e: ParseException) {
                null
            }
        }
}

package com.sansiri.homeservice.data.network

import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GetTokenResult

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class FirebaseUserIdTokenInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        try {
            val user = FirebaseAuth.getInstance().currentUser
            if (user == null) {
                throw Exception("User is not logged in.")
            } else {
                val task = user.getIdToken(true)
                val tokenResult = Tasks.await(task)
                val idToken = tokenResult.token

                if (idToken == null) {
                    throw Exception("idToken is null")
                } else {
                    val modifiedRequest = request.newBuilder()
                            .addHeader("Authorization", idToken)
                            .build()
                    return chain.proceed(modifiedRequest)
                }
            }
        } catch (e: Exception) {
            throw IOException(e.message)
        }

    }
}
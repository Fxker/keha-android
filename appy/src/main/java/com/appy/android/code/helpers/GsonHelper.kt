package com.minor.bemynt.helpers

import com.appy.android.code.data.rest.adapter.DateWithNullAdapter
import com.appy.android.code.data.rest.adapter.DateWithTimeZoneAdapter
import com.appy.android.code.domain.model.shared.common.DateWithTimeZone
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.appy.android.code.base.data.rest.adapter.GsonDateAdapter
import java.util.*

/**
 * Created by tar on 7/20/2017 AD.
 */
internal class GsonHelper {

    companion object {
        fun builder(): GsonBuilder {
            return GsonBuilder()
                    .registerTypeAdapter(Date::class.java, GsonDateAdapter("yyyy-MM-dd HH:mm:ss"))
                    .registerTypeAdapter(DateWithTimeZone::class.java, DateWithTimeZoneAdapter())
                    .registerTypeAdapter(Date::class.java, DateWithNullAdapter())
        }

        fun build(): Gson {
            return builder().create()
        }
    }

}
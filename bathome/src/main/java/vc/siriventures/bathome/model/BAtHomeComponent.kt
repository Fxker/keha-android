package vc.siriventures.bathome.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BAtHomeComponent(
	val id: String? = null,
	val type: String? = null,
	val description: String? = null,
	var status: String? = null,
	val group: String? = null,
	val dimmer: Boolean,
	val ac: Boolean
): Parcelable {
	companion object {
		const val TYPE_LIGHT = "light"
		const val TYPE_SCENE = "scenario"
	}
}

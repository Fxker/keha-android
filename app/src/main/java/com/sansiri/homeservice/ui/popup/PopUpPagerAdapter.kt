package com.sansiri.homeservice.ui.popup

import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PopUp
import com.sansiri.homeservice.ui.tutorial.TutorialContent
import com.sansiri.homeservice.ui.tutorial.TutorialFragment

import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_popup_image.view.*
import android.content.Context.LAYOUT_INFLATER_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.content.Context
import com.bumptech.glide.Glide


class PopUpPagerAdapter(val items: List<PopUp>, val onClicked: (PopUp) -> Unit) : androidx.viewpager.widget.PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val item = items[position]

        val inflater = container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.view_popup_image, null)
        if (item.actionType == PopUp.EXTERNAL_BROWSER || item.actionType == PopUp.DEEP_LINK) {
            view.layoutClickable.isClickable = true
            view.layoutClickable.setOnClickListener {
                if (item.url != null) {
                    onClicked(item)
                }
            }
        } else {
            view.layoutClickable.isClickable = false
        }
        Glide.with(container).load(item.imageUrl).into(view.image)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int = items.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }
}
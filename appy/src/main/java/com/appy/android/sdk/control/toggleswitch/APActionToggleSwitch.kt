package com.appy.android.sdk.control.toggleswitch

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

class APActionToggleSwitch : APAction<ToggleSwitchCommand>, Parcelable {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionToggleSwitch> {
        override fun createFromParcel(parcel: Parcel): APActionToggleSwitch {
            return APActionToggleSwitch(parcel)
        }

        override fun newArray(size: Int): Array<APActionToggleSwitch?> {
            return arrayOfNulls(size)
        }
    }
}
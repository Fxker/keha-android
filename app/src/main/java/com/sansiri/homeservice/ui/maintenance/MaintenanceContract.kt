package com.sansiri.homeservice.ui.maintenance

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MaintenanceContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut)
        fun fetch()
        fun requestToLaunchDevice(planId: String)
        fun requestToLaunchDetail(plan: MaintenancePlan)
    }

    interface View : BaseView {
        fun bindPlan(plans: List<MaintenancePlan>)
        fun bindDevice(devices: List<MaintenanceDevice>)
        fun launchDevice(home: Hut, device: MaintenanceDevice)
        fun showEmptyPlan()
        fun showEmptyDevice()
        fun showLoadingPlan()
        fun hideLoadingPlan()
        fun showLoadingDevice()
        fun hideLoadingDevice()
        fun launchDetail(mHome: Hut, accessoryId: String, planId: String?, plan: MaintenancePlan?)
    }
}
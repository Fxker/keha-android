package com.sansiri.homeservice.model.api.payment

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentChannel(
        val channel: String? = null,
        val command: String? = null,
        val iconUrl: String? = null,
        val textColorRGB: String? = null,
        val bgColorRGB: String? = null,
        val title: String? = null,
        val subTitle: String? = null,
        val merchantName: String? = null,
        val gatewayName: String? = null,
        val requiredData: Int
) : Parcelable {
    enum class Action {
        PHONE_NUMBER_REQUIRED,
        NOTHING
    }

    fun getAction(): Action {
        return when {
            requiredData.and(1) > 0 -> Action.PHONE_NUMBER_REQUIRED
            else -> Action.NOTHING
        }
    }
}

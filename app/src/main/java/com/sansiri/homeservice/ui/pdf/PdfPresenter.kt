package com.sansiri.homeservice.ui.pdf

import android.net.Uri
import android.os.Environment
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.FileDownloader
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.IOException

/**
 * Created by sansiri on 2/9/18.
 */
class PdfPresenter : BasePresenterImpl<PdfContract.View>(), PdfContract.Presenter {
    var pdfByte: ByteArray? = null
    var pdfTitle: String = "document.pdf"
    var pdfUrl: String = ""

    override fun start() {
    }

    override fun destroy() {
    }

    override fun downloadPdfFromUrl(url: String) {
        pdfUrl = url
        try {
            pdfTitle = Uri.parse(url).lastPathSegment
        } catch (e: RuntimeException) {
        }

        mView?.showLoading()

        ApiRepositoryProvider.provideApiUnAuthRepository().downloadFileWithDynamicUrl(url).observe().subscribe({ response ->
            mView?.hideLoading()

            pdfByte = response.bytes()
            if (pdfByte != null) {
                mView?.renderPdf(pdfByte!!)
            }
        }) {
            mView?.hideLoading()
            mView?.showError(it.showHttpError())
        }
    }

    override fun download() {
        if (pdfByte != null) {
            mView?.createFile(pdfByte!!, pdfTitle)
        }
    }

    override fun share() {
        mView?.shareFile(pdfUrl)
    }
}
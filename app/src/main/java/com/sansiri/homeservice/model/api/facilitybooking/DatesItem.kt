package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class DatesItem(
        val date: String? = null,
        val times: List<TimesItem>? = null
) : Parcelable {
    val _date: Date?
        get() {
            try {
                return DateTime(this.date).toDate()
            } catch (e: ParseException) {
                return null
            }
        }

    constructor(source: Parcel) : this(
            source.readString(),
            source.createTypedArrayList(TimesItem.CREATOR) as List<TimesItem>?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(date)
        writeTypedList(times)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<DatesItem> = object : Parcelable.Creator<DatesItem> {
            override fun createFromParcel(source: Parcel): DatesItem = DatesItem(source)
            override fun newArray(size: Int): Array<DatesItem?> = arrayOfNulls(size)
        }
    }
}

fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Runs all the tests
### android dev
```
fastlane android dev
```

### android staging
```
fastlane android staging
```

### android prod
```
fastlane android prod
```

### android devPlus
```
fastlane android devPlus
```

### android prodPlus
```
fastlane android prodPlus
```

### android testslack
```
fastlane android testslack
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).

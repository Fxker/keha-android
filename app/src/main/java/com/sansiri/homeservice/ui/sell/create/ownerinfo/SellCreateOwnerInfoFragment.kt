package com.sansiri.homeservice.ui.sell.create.ownerinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.bottomsheet.TimePickerBottomSheet
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.sell.create.SellCreateActivity
import com.sansiri.homeservice.util.reportTime
import com.sansiri.homeservice.util.snackError
import kotlinx.android.synthetic.main.fragment_sell_owner_info.*
import java.util.*

class SellCreateOwnerInfoFragment : BaseFragment<SellCreateOwnerContract.View, SellCreateOwnerContract.Presenter>(), SellCreateOwnerContract.View {
    override var mPresenter: SellCreateOwnerContract.Presenter = SellCreateOwnerPresenter()
    private lateinit var  mTimeStartPicker: TimePickerBottomSheet
    private lateinit var  mTimeEndPicker: TimePickerBottomSheet

    companion object {
        fun newInstance(): SellCreateOwnerInfoFragment {
            return SellCreateOwnerInfoFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sell_owner_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val calendarStart = Calendar.getInstance()
        calendarStart.set(Calendar.HOUR_OF_DAY, 8)
        calendarStart.set(Calendar.MINUTE, 0)

        mTimeStartPicker = TimePickerBottomSheet(calendarStart) { time ->
            textTimeStart.text = time.reportTime()
        }

        val calendarEnd = Calendar.getInstance()
        calendarEnd.set(Calendar.HOUR_OF_DAY, 22)
        calendarEnd.set(Calendar.MINUTE, 0)

        mTimeEndPicker = TimePickerBottomSheet(calendarEnd) { time ->
            textTimeEnd.text = time.reportTime()
        }

        textTimeStart.text = calendarStart.reportTime()
        textTimeEnd.text = calendarEnd.reportTime()

        textTimeStart.setOnClickListener {
            mTimeStartPicker.show(fragmentManager!!, "BookingDialogFragment")
        }
        textTimeEnd.setOnClickListener {
            mTimeEndPicker.show(fragmentManager!!, "BookingDialogFragment")
        }

        buttonNext.setOnClickListener { activity?.finish() }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        (activity as SellCreateActivity).setToolbarTitle("รายละเอียดการติดต่อ")
    }

    override fun showError(message: String) {
        activity?.snackError(message)
    }
}
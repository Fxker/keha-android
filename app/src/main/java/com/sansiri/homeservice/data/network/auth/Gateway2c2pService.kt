package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.Token
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.squareup.okhttp.ResponseBody
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface Gateway2c2pService {
    @GET("me/payments/{transactionId}")
    fun inquiryTransaction(@Path("transactionId") transactionId: String): Deferred<Response<Transaction2C2PDetail>>
}
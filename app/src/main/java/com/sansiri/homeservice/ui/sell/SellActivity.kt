package com.sansiri.homeservice.ui.sell

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Sell
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.sell.create.SellCreateActivity
import com.sansiri.homeservice.util.setBackToolbar

class SellActivity : SellAndRentActivity() {

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, SellActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_create) {
            SellCreateActivity.start(this, false)
        }

        return true
    }
}
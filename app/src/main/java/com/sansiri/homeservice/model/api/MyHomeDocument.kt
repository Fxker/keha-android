package com.sansiri.homeservice.model.api

data class MyHomeDocument(
        val createdAt: String? = null,
        val imageUrl: String? = null,
        val permission: Int? = null,
        val detail: Detail? = null,
        val objectId: String? = null,
        val updatedAt: String? = null
) {
    data class Detail(
            val updatedAt: String? = null,
            val createdAt: String? = null,
            val langCode: String? = null,
            val title: String? = null,
            val objectId: String? = null,
            val url: String? = null
    )

}

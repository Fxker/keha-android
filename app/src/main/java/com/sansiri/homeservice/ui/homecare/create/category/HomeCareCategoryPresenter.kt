package com.sansiri.homeservice.ui.homecare.create.category

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/11/17.
 */
class HomeCareCategoryPresenter : BasePresenterImpl<HomeCareCategoryContract.View>(), HomeCareCategoryContract.Presenter {
    override fun start() {
        loadCategories()
    }

    override fun destroy() {
    }

    override fun loadCategories() {
        mView?.showLoading()
        val api = ApiRepositoryProvider.provideApiUnAuthRepository()
        val disposable = api.getHomeCareType({
            mView?.hideLoading()
            mView?.bindCategories(it)
        }) {
            mView?.hideLoading()
            mView?.showError(it.showHttpError())
        }

        disposable?.let {
            mCompositeDisposable.add(it)
        }

    }

    override fun selectCategory(menu: Menu) {
        //TODO store selected option
        mView?.nextScreen(menu)
    }
}

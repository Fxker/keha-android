package com.sansiri.homeservice.ui.notification

import android.app.Notification
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.main.MainActivity
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import com.sansiri.homeservice.util.toJson
import kotlinx.android.synthetic.main.fragment_notification.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.currentScope

class NotificationFragment : BaseV2Fragment<NotificationContract.View, NotificationContract.Presenter>(), NotificationContract.View {
    override val mPresenter: NotificationContract.Presenter by currentScope.inject()

    private val adapter = NotificationAdapter() { notification ->
       mPresenter.onNotificationClicked(notification)
    }

    companion object {
        fun newInstance(): NotificationFragment {
            return NotificationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.adapter = adapter
        buttonDelete.setOnClickListener {
            activity?.alert(
                    "Are you sure you want to delete these notifications?"
            ) {
                noButton {
                }
                yesButton {
                    mPresenter.deleteAll()
                }
            }?.show()
        }
        attachObserver()
    }

    override fun attachObserver() {
        mPresenter.notifications.observe(this, Observer {
            mPresenter.handleNotification(it)
        })

    }

    override fun bindData(notifications: List<PushNotification>) {
        list.show()
        textNoData.hide()
        adapter.setData(notifications.sortedByDescending { it.createdAt })
    }

    override fun showNoData() {
        list.hide()
        textNoData.show()
    }

    override fun launchNotificationAction(notification: PushNotification) {
        val intent = Intent(context, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        notification.type?.let { intent.putExtra(NotificationHelper.TYPE, it) }
        notification.data?.let { intent.putExtra(NotificationHelper.DATA, it.toJson()) }
        startActivity(intent)
    }

    override fun showError(message: String) {
    }
}
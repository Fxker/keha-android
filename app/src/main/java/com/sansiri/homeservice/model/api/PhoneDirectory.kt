package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

data class PhoneDirectory(

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("createdAt")
	val createdAt: String? = null,

	@SerializedName("phones")
	val phones: List<String> = listOf(),

	@SerializedName("objectId")
	val objectId: String? = null,

	@SerializedName("updatedAt")
	val updatedAt: String? = null
)
package com.appy.android.sdk.control.fibaro

import com.appy.android.sdk.control.APCommand

/**
 * Created by Francisco Barrios 15/11/2018.
 */

enum class FibaroTemperatureCommand : APCommand {

    TEMP_18 {
        override fun action(): String {
            return "{\"args\":[10, 18]}"
        }
    },
    TEMP_19 {
        override fun action(): String {
            return "{\"args\":[10, 19]}"
        }
    },
    TEMP_20 {
        override fun action(): String {
            return "{\"args\":[10, 20]}"
        }
    },
    TEMP_21 {
        override fun action(): String {
            return "{\"args\":[10, 21]}"
        }
    },
    TEMP_22 {
        override fun action(): String {
            return "{\"args\":[10, 22]}"
        }
    },
    TEMP_23 {
        override fun action(): String {
            return "{\"args\":[10, 23]}"
        }
    },
    TEMP_24 {
        override fun action(): String {
            return "{\"args\":[10, 24]}"
        }
    },
    TEMP_25 {
        override fun action(): String {
            return "{\"args\":[10, 25]}"
        }
    },
    TEMP_26 {
        override fun action(): String {
            return "{\"args\":[10, 26]}"
        }
    },
    TEMP_27 {
        override fun action(): String {
            return "{\"args\":[10, 27]}"
        }
    },
    TEMP_28 {
        override fun action(): String {
            return "{\"args\":[10, 28]}"
        }
    },
    TEMP_29 {
        override fun action(): String {
            return "{\"args\":[10, 29]}"
        }
    },
    TEMP_30 {
        override fun action(): String {
            return "{\"args\":[10, 30]}"
        }
    };
}
package com.sansiri.homeservice.ui.header

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.model.Theme
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.tintImage
import kotlinx.android.synthetic.main.fragment_header_logo.*


class HeaderLogoFragment : Fragment() {

    companion object {
        fun newInstance(): HeaderLogoFragment {
            return HeaderLogoFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_header_logo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        FirebaseRemoteConfigManager().fetch { remote ->
            remote.let { remoteConfig ->
                if (imageLogo != null && isAdded) {
                    val logo = remoteConfig.getString(FirebaseRemoteConfigManager.LOGO_IMAGE)
                    if (!logo.isNullOrEmpty()) {
                        Glide.with(this).load(logo).into(imageLogo)
                    }

                    val themeJson = remoteConfig.getString(FirebaseRemoteConfigManager.THEME)
                    val theme = Gson().fromJson<Theme>(themeJson, Theme::class.java)

                    theme?.titleColor?.let {
                        val color = Color.parseColor(it)
                        imageLogo.tintImage(color)
                    }
                }
            }
        }

    }
}
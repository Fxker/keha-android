package com.appy.android.sdk.control.remotecontrol

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ActionJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.fibaro.FibaroFanSpeedCommand
import com.appy.android.sdk.control.fibaro.FibaroOnOffCommand
import com.appy.android.sdk.control.fibaro.FibaroOperationModeCommand
import com.appy.android.sdk.control.fibaro.FibaroTemperatureCommand

class APActionRemoteControl : APAction<String>, Parcelable {

    internal var parameters: HashMap<String, String>? = null
    internal var localCommand: String? = null
    internal var deviceType: String? = null
    internal var deviceId: String? = null

    constructor() : super()

    fun init(actionJsonModel: ActionJsonModel) {
        this.name = actionJsonModel.name
        this.description = actionJsonModel.description
        this.action = actionJsonModel.action

        actionJsonModel.localActions?.let {
            this.localCommand = it.command
            this.deviceType = it.deviceType
            this.deviceId = it.deviceId
            //this.parameters = it.parameters

            val map = HashMap<String, String>()
            if (it.command.equals("setMode") && it.deviceType.equals("com.fibaro.operatingMode") && actionJsonModel.action.equals("ON")) {
                map["heat"] = FibaroOperationModeCommand.HEAT.action()
                map["cool"] = FibaroOperationModeCommand.COOL.action()
                map["resume"] = FibaroOperationModeCommand.RESUME.action()
                map["fan_only"] = FibaroOperationModeCommand.FAN_ONLY.action()
                map["dry_air"] = FibaroOperationModeCommand.DRY_AIR.action()
                map["auto_change_over"] = FibaroOperationModeCommand.AUTO_CHANGE_OVER.action()
                this.parameters = map
            }

            if (it.command.equals("setMode") && it.deviceType.equals("com.fibaro.operatingMode") && actionJsonModel.action.equals("OFF")) {
                map["off"] = FibaroOnOffCommand.OFF.action()
                this.parameters = map
            }

            if (it.command.equals("setThermostatSetpoint") && it.deviceType.equals("com.fibaro.setPoint")) {
                map["18 celcius"] = FibaroTemperatureCommand.TEMP_18.action()
                map["19 celcius"] = FibaroTemperatureCommand.TEMP_19.action()
                map["20 celcius"] = FibaroTemperatureCommand.TEMP_20.action()
                map["21 celcius"] = FibaroTemperatureCommand.TEMP_21.action()
                map["22 celcius"] = FibaroTemperatureCommand.TEMP_22.action()
                map["23 celcius"] = FibaroTemperatureCommand.TEMP_23.action()
                map["24 celcius"] = FibaroTemperatureCommand.TEMP_24.action()
                map["25 celcius"] = FibaroTemperatureCommand.TEMP_25.action()
                map["26 celcius"] = FibaroTemperatureCommand.TEMP_26.action()
                map["27 celcius"] = FibaroTemperatureCommand.TEMP_27.action()
                map["28 celcius"] = FibaroTemperatureCommand.TEMP_28.action()
                map["29 celcius"] = FibaroTemperatureCommand.TEMP_29.action()
                map["30 celcius"] = FibaroTemperatureCommand.TEMP_30.action()
                this.parameters = map
            }

            if (it.command.equals("setFanMode") && it.deviceType.equals("com.fibaro.fanMode")) {
                map["off"] = FibaroFanSpeedCommand.OFF.action()
                map["auto_low"] = FibaroFanSpeedCommand.AUTO_LOW.action()
                map["auto_medium_speed"] = FibaroFanSpeedCommand.AUTO_MEDIUM_SPEED.action()
                map["auto_high_speed"] = FibaroFanSpeedCommand.AUTO_HIGH_SPEED.action()
                map["low_speed"] = FibaroFanSpeedCommand.LOW_SPEED.action()
                map["medium_speed"] = FibaroFanSpeedCommand.MEDIUM_SPEED.action()
                map["high_speed"] = FibaroFanSpeedCommand.HIGH_SPEED.action()
                this.parameters = map
            }
        }
    }

    fun getParameters(): List<String>? {
        return this.parameters?.keys?.map { it }
    }

    internal fun convertParameter(parameter: String): String {
        return parameters?.get(parameter).orEmpty()
    }

    constructor(source: Parcel) : super(source) {
        source.readSerializable()?.let {
            this.parameters = it as HashMap<String, String>
        }
        this.deviceId = source.readString()
        this.deviceType = source.readString()
        this.localCommand = source.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeSerializable(parameters)
        dest.writeString(deviceId)
        dest.writeString(deviceType)
        dest.writeString(localCommand)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionRemoteControl> {
        override fun createFromParcel(parcel: Parcel): APActionRemoteControl {
            return APActionRemoteControl(parcel)
        }

        override fun newArray(size: Int): Array<APActionRemoteControl?> {
            return arrayOfNulls(size)
        }
    }
}
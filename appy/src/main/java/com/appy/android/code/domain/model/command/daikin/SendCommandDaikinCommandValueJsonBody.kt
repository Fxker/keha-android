package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody
import com.google.gson.annotations.SerializedName

internal class SendCommandDaikinCommandValueJsonBody : BaseCommandJsonBody<String>() {
    @SerializedName("command_value")
    var commandValue: String? = null
}
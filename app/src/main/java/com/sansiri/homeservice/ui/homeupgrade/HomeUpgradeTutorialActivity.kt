package com.sansiri.homeservice.ui.homeupgrade

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.homeupgrade.selector.HomeUpgradeSelectorActivity
import com.sansiri.homeservice.ui.tutorial.TutorialActivity
import com.sansiri.homeservice.ui.tutorial.TutorialContent
import kotlinx.android.synthetic.main.activity_tutorial.*

/**
 * Created by sansiri on 4/5/18.
 */
class HomeUpgradeTutorialActivity : TutorialActivity() {
    companion object {
        val UNIT_ID = "UNIT_ID"
        fun start(activity: Activity, unitId: String) {
            val intent = Intent(activity, HomeUpgradeTutorialActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val unitId = intent.getStringExtra(UNIT_ID)

        pagerAdapter?.setItem(listOf(
                TutorialContent(R.drawable.home_upgrade_tutorial1, getString(R.string.home_upgrade_tutorial1)),
                TutorialContent(R.drawable.home_upgrade_tutorial2, getString(R.string.home_upgrade_tutorial2)),
                TutorialContent(R.drawable.home_upgrade_tutorial3, getString(R.string.home_upgrade_tutorial3))
        ))

        textTitle.text = getString(R.string.upgrade_your_home)

        viewPager.addOnPageChangeListener( object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (position == pagerAdapter!!.count - 1) {
                    buttonNext.text = getString(com.sansiri.homeservice.R.string.start_shopping)
                } else {
                    buttonNext.text = getString(R.string.next)
                }
            }

        })

        buttonNext.setOnClickListener {
            if (viewPager.currentItem < pagerAdapter!!.count - 1) {
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
            } else {
                HomeUpgradeSelectorActivity.start(this, unitId)
            }
        }
    }
}
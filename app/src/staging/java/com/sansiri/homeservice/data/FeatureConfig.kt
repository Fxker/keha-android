package com.sansiri.homeservice.data

object FeatureConfig: IFeatureConfig {
    override val ANNOUNCEMENT_ALL = true
    override val ANNOUNCEMENT_PROJECT = true
    override val SANSIRI_FAMILY = true
    override val VOICE_COMMAND = true
    override val GOOGLE_ASSISTANT = true
    override val LOUNGE: Boolean = true
    override val IS_SUPPORT_CALLCENTER = true
}
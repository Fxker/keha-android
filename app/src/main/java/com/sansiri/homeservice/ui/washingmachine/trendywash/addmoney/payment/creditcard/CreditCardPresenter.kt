package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashPaymentGateWayQR
import com.sansiri.homeservice.util.removeSpace

class CreditCardPresenter(val repository: TrendyWashRepository) : BasePresenterImpl<CreditCardContract.View>(), CreditCardContract.Presenter {
    private var mCardName: String? = null
    private var mCardNumber: String? = null
    private var mCCV: String? = null
    private var mExpiryMonth: Int? = null
    private var mExpiryYear: Int? = null

    var mUnitId: String? = null
    var mUserId: String? = null
    var mAmount: Int = 0

    override var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>? = null

    override fun init(unitId: String?, userId: String?, amount: Int) {
        mUnitId = unitId
        mUserId = userId
        mAmount = amount
    }

    override fun start() {
        validate()
        fetch()
    }

    override fun fetch() {
        if (mUnitId != null && mUserId != null) {
            creditInfo = repository.getTrendyWashCredit(mUnitId!!, mUserId!!)
        }
    }


    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun setCardName(name: String) {
        mCardName = name
        validate()
    }

    override fun setCardNumber(number: String) {
        mCardNumber = number
        validate()
    }

    override fun setCCV(ccv: String) {
        mCCV = ccv
        validate()
    }

    override fun setExpiryMonth(month: Int) {
        mExpiryMonth = month
        validate()
    }

    override fun setExpiryYear(year: Int) {
        mExpiryYear = year
        validate()
    }

    override fun validate() {
        if (
                mCardName != null &&
                mCardNumber != null &&
                mCCV != null &&
                mExpiryMonth != null &&
                mExpiryYear != null
        ) {
            mView?.enableContinueButton()
        } else {
            mView?.disableContinueButton()
        }
    }

    override fun purchase(): MutableLiveData<Resource<TrendyWashPaymentGateWayQR>>? {
        val cardNumber = mCardNumber?.removeSpace()
        if (cardNumber == null || cardNumber.length < 16) {
            mView?.showCreditCardError()
            return null
        }

        return if (mUnitId != null && mUserId != null) {
            repository.checkoutTrendyWashCreditCard(
                    amount = mAmount,
                    unitId = mUnitId!!,
                    trendyWashUserId = mUserId!!,
                    cardNumber = cardNumber,
                    cardName = mCardName ?: "",
                    expirationMonth = mExpiryMonth ?: 0,
                    expirationYear = mExpiryYear ?: 0,
                    securityCode = mCCV ?: "")
        } else null
    }
}
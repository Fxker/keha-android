package com.sansiri.homeservice.ui.announcement.all

import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 12/14/17.
 */
interface AllAnnouncementContract {
    interface Presenter : BasePresenter<View> {
        fun init(id: String?)
        fun fetchData(page: Int)
    }

    interface View : BaseView {
        fun addData(list: List<Announcement<*>>)
        fun showLoading()
        fun hideLoading()
    }
}
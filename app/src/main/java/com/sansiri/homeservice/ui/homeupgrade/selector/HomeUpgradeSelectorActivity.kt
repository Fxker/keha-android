package com.sansiri.homeservice.ui.homeupgrade.selector

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.homeupgrade.summary.HomeUpgradeSummaryActivity
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.synthetic.main.activity_home_upgrade_selector.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class HomeUpgradeSelectorActivity : BaseActivity<HomeUpgradeSelectorContract.View, HomeUpgradeSelectorContract.Presenter>(), HomeUpgradeSelectorContract.View {

    override var mPresenter:HomeUpgradeSelectorContract.Presenter = HomeUpgradeSelectorPresenter()
    private var totalPrice: Double = 0.0
        set(value) {
            buttonNext.isEnabled = value > 0
            buttonNext.alpha = if (value > 0) 1f else 0.5f
            field = value
        }

    companion object {
        val UNIT_ID = "UNIT_ID"
        fun start(activity: Activity, unitId: String) {
            val intent = Intent(activity, HomeUpgradeSelectorActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            activity.startActivity(intent)
        }
    }

    val mAdapter = HomeUpgradeAdapter() { hardware ->
        mPresenter.addOrderToList(hardware)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_upgrade_selector)

        setBackToolbar(getString(R.string.choose_the_devices_you_want_to_set_up))

        val unitId = intent.getStringExtra(UNIT_ID)

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@HomeUpgradeSelectorActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }

        buttonNext.setOnClickListener {
            mPresenter.requestSummary()
        }

        showTotalPrice(0.0)

        mPresenter.init(unitId)
        mPresenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun bindData(rooms: List<HomeUpgradeRoom>) {
        mAdapter.setData(rooms)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        alert(message, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }

    override fun showTotalPrice(totalPrice: Double) {
        this.totalPrice = totalPrice
        textTotalPrice.text = "${totalPrice.toCurrencyFormat()} ${getString(R.string.baht)}"
    }

    override fun launchSummary(homeUpgradeRooms: ArrayList<HomeUpgradeRoom>, unitId: String) {
        HomeUpgradeSummaryActivity.start(this, homeUpgradeRooms, unitId)
    }
}

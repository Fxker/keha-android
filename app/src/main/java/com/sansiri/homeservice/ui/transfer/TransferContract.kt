package com.sansiri.homeservice.ui.transfer

import com.sansiri.homeservice.model.api.Doc
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/3/17.
 */
interface TransferContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String)
    }

    interface View : BaseView {
        fun bindData(docs: List<Doc>)
        fun showLoading()
        fun hideLoading()
        fun addData(doc: Doc)
    }
}
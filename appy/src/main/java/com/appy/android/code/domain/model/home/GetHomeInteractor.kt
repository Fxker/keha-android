package com.appy.android.code.domain.model.home

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.home.HomeApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal class GetHomeInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<HomeApiResult, HomeForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: HomeForm): Observable<HomeApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getHome(params.headers, params.homeId, params.params)
    }
}
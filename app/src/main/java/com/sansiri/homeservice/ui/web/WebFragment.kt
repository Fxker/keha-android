package com.sansiri.homeservice.ui.web

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.activity_web.*
import android.net.Uri
import android.net.http.SslError
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import android.widget.TextView
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.setBackToolbar
import com.sansiri.homeservice.util.show
import im.delight.android.webview.AdvancedWebView
import androidx.core.content.ContextCompat.startActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.util.launchExternalBrowser
import kotlinx.android.synthetic.main.toolbar.*


open class WebFragment : androidx.fragment.app.Fragment(), IWeb, AdvancedWebView.Listener {
    private var mAddTokenWithRequest: Boolean = false

    protected lateinit var analyticProvider: AnalyticProvider
    protected var SCREEN_NAME_LOG = "WEB"

    companion object {
        val URL = "URL"
        val ADD_TOKEN_TO_REQUEST = "ADD_TOKEN_TO_REQUEST"

        fun newInstance(url: String, addTokenWithRequest: Boolean = false): WebFragment {
            val fragment = WebFragment()
            val bundle = Bundle()
            bundle.putString(URL, url)
            bundle.putBoolean(ADD_TOKEN_TO_REQUEST, addTokenWithRequest)
            fragment.arguments = bundle
            return fragment
        }
    }

    private var mUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticProvider = AnalyticProvider(activity!!.application as AppController)
        mUrl = arguments?.getString(URL)
        mAddTokenWithRequest = arguments?.getBoolean(ADD_TOKEN_TO_REQUEST) ?: false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.hide()

        if (savedInstanceState != null) {
            // Restore the previous URL and history stack
            webview.restoreState(savedInstanceState)
        }

        initWebView(mUrl ?: "", mAddTokenWithRequest)
    }

    override fun initWebView(url: String, addTokenWithRequest: Boolean) {
        webview.setListener(activity, this)
        webview.makeItSmart(this, progressBar) { query, url ->
            if (query == "external_browser") {
                activity?.launchExternalBrowser(url ?: "")
            } else {
                // launchExtraRedirect(query)
            }
        }

        if (addTokenWithRequest) {
            ApiRepositoryProvider.getToken { token ->
                val locale = ApiRepositoryProvider.getLanguage()
                if (token != null) {
                    val map = mapOf(
                            Pair("Authorization", token),
                            Pair("Accept-Language", locale),
                            Pair("AppVersion", BuildConfig.VERSION_NAME)
                    )
                    webview.loadUrl(url, map)
                } else {
                    webview.loadUrl(url)
                }
            }
        } else {
            webview.loadUrl(url)
        }
    }


    override fun onResume() {
        super.onResume()
        webview?.onResume()
        analyticProvider.sendScreenView(SCREEN_NAME_LOG)
        analyticProvider.sendEvent(SCREEN_NAME_LOG, "OPEN", mUrl)
    }

    override fun onPause() {
        webview?.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        webview?.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        webview.onActivityResult(requestCode, resultCode, data)
    }


    override fun onPageFinished(url: String?) {
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        Log.d("WebActivity", description)
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
        Log.d("WebActivity", url)
        url?.let { activity?.launchExternalBrowser(url) }
    }

    override fun onExternalPageRequest(url: String?) {
        Log.d("WebActivity", url)
        url?.let { activity?.launchExternalBrowser(url) }
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }
}

package com.sansiri.homeservice.ui.announcement.all

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.menu.Menu

import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.withDelay
import kotlinx.android.synthetic.main.view_announcement_large.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
open class AllAnnouncementAdapter(open val itemClick: (View, Announcement<*>) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    protected val mData = mutableListOf<Announcement<*>>()

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if(holder is AnnouncementViewHolder) {
            holder.bind(mData[position])
        }
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return AnnouncementViewHolder(parent.inflate(R.layout.view_announcement_large), itemClick)
    }


    fun setData(list: List<Announcement<*>>) {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }

    fun addData(list: List<Announcement<*>>) {
        mData.addAll(list)
        notifyDataSetChanged()
    }

    fun addDataDelay(data: List<Announcement<*>>) {
        data.forEach {
            {
                mData.add(it)
                notifyItemInserted(mData.size - 1)
            }.withDelay(1)
        }
    }

}
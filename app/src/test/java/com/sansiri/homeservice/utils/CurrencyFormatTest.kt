package com.sansiri.homeservice.util

import org.junit.Test

import org.junit.Assert.*

class CurrencyFormatTest {

    @Test
    fun `Should cut off decimal 00`() {
        assertEquals("if decimal is zero should cut off .00 ", "0", 0.0.toCurrencyFormat())
    }

    @Test
    fun `Always display decimal 2 digits`() {
        assertEquals("55.55", 55.55.toCurrencyFormat())
        assertEquals("10.10", 10.1.toCurrencyFormat())
    }
}
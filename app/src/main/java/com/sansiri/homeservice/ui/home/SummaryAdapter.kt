package com.sansiri.homeservice.ui.home

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.api.TodaySummary
import com.sansiri.homeservice.util.*

import kotlinx.android.synthetic.main.view_summary_menu.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class SummaryAdapter(val itemClick: (TodaySummary) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<SummaryAdapter.ViewHolder>() {
    private var mData =listOf<TodaySummary>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_summary_menu), itemClick)
    }

    fun setData(data: List<TodaySummary>) {
        if (mData != data) {
            mData = data
            notifyDataSetChanged()
        }
    }

    class ViewHolder(val view: View, val itemClick: (TodaySummary) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(data: TodaySummary) {
            with(view) {
                when(data.type) {
                    GenericMenu.DOWNPAYMENT -> {
                        textExtra.show()
                        textTitle.invisible()
                        textSubtitle.invisible()
                        textExtra.setTextColor(resources.getColor(R.color.colorCancel))
                        textExtra.text = data.title
                    }
                    GenericMenu.PROJECT_PROGRESS -> {
                        textExtra.show()
                        textTitle.invisible()
                        textSubtitle.invisible()
                        textExtra.setTextColor(resources.getColor(R.color.textDark))
                        textExtra.text = data.title
                    }
                    else -> {
                        textExtra.hide()
                        textTitle.show()
                        textSubtitle.show()
                        textTitle.text = data.title
                        textSubtitle.text = data.subTitle
                    }
                }

                textType.text = data.typeName
                textDate.text = data._createdAt?.report()
                Glide.with(context).load(data.iconUrl).into(imageIcon)
                // imageIcon.setImageResource(html.icon)
                root.setOnClickListener { itemClick(data) }
            }

        }
    }
}
package com.sansiri.homeservice.model.api.partner.svvh

data class SVVHRegisterResponse(
        val redirectUrl: String? = null
)
package com.sansiri.homeservice.ui.washingmachine.trendywash.home

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TrendyWashHomeContract {
    interface Presenter : BasePresenter<View> {
        var machines: MutableLiveData<Resource<List<TrendyWashMachine>>>?
        var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>?

        fun init(unitId: String, userId: String)
        fun findMachineById(machineNo: String?)
        fun fetchMachines(isUpdate: Boolean = false)
        fun fetchCredit(isUpdate: Boolean = false)
    }

    interface View : BaseView {
        fun bindData(machines: List<TrendyWashMachine>)
        fun showLoading()
        fun hideLoading()
        fun showCredit(remainCredits: String)
        fun launchQRScanner()
        fun launchMachine(machine: TrendyWashMachine)
        fun showErrorNoTitle(message: String)
    }
}
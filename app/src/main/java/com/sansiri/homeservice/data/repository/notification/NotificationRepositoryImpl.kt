package com.sansiri.homeservice.data.repository.notification

import androidx.annotation.WorkerThread
import com.sansiri.homeservice.data.database.notification.NotificationDao
import com.sansiri.homeservice.model.api.notification.PushNotification

class NotificationRepositoryImpl(val notificationDao: NotificationDao) : NotificationRepository {
    override fun getNotifications() = notificationDao.getNotifications()

    @WorkerThread
    override suspend fun insert(notification: PushNotification) {
        notificationDao.insertNotification(notification)
    }

    @WorkerThread
    override suspend fun deleteFeatureNotification(featureId: String, unitId: String) {
        notificationDao.deleteNotification(featureId, unitId)
    }

    override suspend fun deleteAll() {
        notificationDao.deleteAll()
    }

    @WorkerThread
    override suspend fun markAsRead(notificationId: Int) {
        notificationDao.markAsRead(notificationId)
    }

    @WorkerThread
    override suspend fun markAsRead(featureId: String, unitId: String) {
        notificationDao.markAsRead(featureId, unitId)
    }
}
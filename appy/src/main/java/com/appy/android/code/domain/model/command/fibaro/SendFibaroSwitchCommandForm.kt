package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand

class SendFibaroSwitchCommandForm(light: APToggleSwitch, command: ToggleSwitchCommand) : SendFibaroCommandForm(light) {

    init {
        action = getCommand(command)
    }

    fun getCommand(command: ToggleSwitchCommand): String {
        return if (command == ToggleSwitchCommand.ON) {
            "turnOn"
        } else {
            "turnOff"
        }
    }
}
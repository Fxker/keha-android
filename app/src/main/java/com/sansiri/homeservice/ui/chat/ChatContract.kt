package com.sansiri.homeservice.ui.chat

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.chat.ChatTemplate
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/18/17.
 */
interface ChatContract {
    interface Presenter : BasePresenter<View> {
        fun init(home: Hut)
        fun sendMessage(message: String)
        fun sendPhoto(imageUrl: String)
        fun loadMore()
        fun upload(fileName: String, imageFile: ByteArray)
        fun sendMessage(message: ChatMessage)
    }

    interface View : BaseView {
        fun addMessage(message: List<ChatMessage>)
        fun addMessageAt(message: List<ChatMessage>, position: Int)
        fun showUploadError()
        fun showUploadProgress()
        fun hideUploadProgress()
        fun updateUploadProgress(progress: Double)
        fun showSendMessageError()
        fun showNotice()
        fun hideNotice()
        fun bindChatTemplate(chatTemplates: List<ChatTemplate>)
        fun showChatTemplateButton()
        fun hideChatTemplateButton()
        fun showPanelChatTemplate()
        fun hidePanelChatTemplate()
        fun disableChatTemplateButton()
        fun enableChatTemplateButton()
    }
}
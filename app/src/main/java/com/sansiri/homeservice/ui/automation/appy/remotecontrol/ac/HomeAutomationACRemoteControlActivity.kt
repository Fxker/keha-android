package com.sansiri.homeservice.ui.automation.appy.remotecontrol.ac

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.ToggleLayout
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_container.*
import kotlinx.android.synthetic.main.view_home_automation_air_condition_v2.view.*

class HomeAutomationACRemoteControlActivity : BaseActivity<HomeAutomationACRemoteControlContract.View, HomeAutomationACRemoteControlContract.Presenter>(), HomeAutomationACRemoteControlContract.View {
    override var mPresenter: HomeAutomationACRemoteControlContract.Presenter = HomeAutomationACRemoteControlPresenter()
    private var mACView: View? = null

    companion object {
        const val CONTROL = "CONTROL"
        const val PROJECT_ID = "PROJECT_ID"
        const val HOME_ID = "HOME_ID"
        fun start(activity: Activity, control: APRemoteControl, projectId: String, homeId: String) {
            val intent = Intent(activity, HomeAutomationACRemoteControlActivity::class.java)
            intent.putExtra(CONTROL, control)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(HOME_ID, homeId)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        val control = intent.getParcelableExtra<APRemoteControl>(CONTROL)
        val homeId = intent.getStringExtra(HOME_ID)
        val projectId = intent.getStringExtra(PROJECT_ID)

        if (control != null) {
            setBackToolbar(control.title ?: "")

            mPresenter.init(control, homeId, projectId)
            mPresenter.start()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(text: String) {
        snack(text)
    }

    override fun showError(message: String) {
        snackError(message)
    }

    override fun restoreSave(homeId: String, deviceId: String) {
        val save = PreferenceHelper.getHomeAutomationSelection(this, homeId, deviceId)
        save?.let { mPresenter.mapSave(save) }
    }

    override fun bindAirConditioner(control: APRemoteControl) {
        container.show()
        mACView = container.inflate(R.layout.view_home_automation_air_condition_v2)
        container.addView(mACView)

        mACView?.apply {
            panelSwing.hide()

            buttonDecrease.setOnClickListener {
                mPresenter.decreaseTemp()
            }
            buttonIncrease.setOnClickListener {
                mPresenter.increaseTemp()
            }
            toggleGroupFan.setOnItemSelectedListener { index ->
                mPresenter.commandFan(index)
            }
            toggleGroupMode.setOnItemSelectedListener { index ->
                mPresenter.commandMode(index)
            }
            buttonOnOff.setOnCheckedChangeListener(object : ToggleLayout.OnCheckedChangeListener {
                override fun onCheckedChanged(isChecked: Boolean) {
                    mPresenter.commandTurnOn(isChecked)
                }
            })
            buttonOnOff.addRelatedChild(imageButtonOnOff, textButtonOnOff)
        }

    }


    override fun setTemperature(degree: Int) {
        mACView?.textDegree?.text = "$degree°"
    }

    override fun setTurnOn(isTurnOn: Boolean) {
        mACView?.apply {
            buttonOnOff.isChecked = isTurnOn
            textButtonOnOff.text = if (isTurnOn) getString(R.string.on) else getString(R.string.off)
        }
    }

    override fun setCommandFanIndex(index: Int) {
        mACView?.toggleGroupFan?.selectItem(index)
    }

    override fun setCommandModeIndex(index: Int) {
        mACView?.toggleGroupMode?.selectItem(index)
    }

    override fun save(homeId: String, deviceId: String, map: HashMap<String, String>) {
        PreferenceHelper.saveHomeAutomationSelection(this, homeId, deviceId, map)
    }
}

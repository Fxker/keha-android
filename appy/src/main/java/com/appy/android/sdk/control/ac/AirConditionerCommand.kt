package com.appy.android.sdk.control.ac

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class AirConditionerCommand : APCommand {
    OFF {
        override fun action(): String {
            return "CONTROL_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "CONTROL_ON"
        }
    };
}
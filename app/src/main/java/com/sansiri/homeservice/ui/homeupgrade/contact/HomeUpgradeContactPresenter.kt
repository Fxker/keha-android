package com.sansiri.homeservice.ui.homeupgrade.contact

import android.util.Patterns
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.homeupgrade.*
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import retrofit2.HttpException

/**
 * Created by sansiri on 12/19/17.
 */
class HomeUpgradeContactPresenter : BasePresenterImpl<HomeUpgradeContactContract.View>(), HomeUpgradeContactContract.Presenter {

    private lateinit var orderRequest: List<HomeUpgradeOrderRequest>
    private lateinit var unitId: String

    override fun init(orderRequest: List<HomeUpgradeOrderRequest>, unitId: String) {
        this.orderRequest = orderRequest
        this.unitId = unitId
    }

    override fun start() {
        ApiRepositoryProvider?.provideApiAuthRepository { api ->
            api?.let {
                mCompositeDisposable.add(it.getMe().observe().subscribe({ profile ->
                    mView?.bindData(profile.displayname
                            ?: "${profile.firstname} ${profile.lastname}", profile.email
                            ?: "", profile.phoneNumber ?: "")
                }) {

                })
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun submit(name: String, email: String, phone: String) {
        if (validate(name, email, phone)) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    mView?.showSubmitting()
                    mCompositeDisposable.add(
                            api.submitHomeUpgrade(unitId, HomeUpgradeRequest(
                                    HomeUpgradeCustomer(name, email, phone),
                                    orderRequest
                            )).observe().subscribe({
                                mView?.showSubmitted()
                            }) {
                                if (it is HttpException) {
                                    val jsonError = it.response()?.errorBody()?.string()
                                    try {
                                        val error = Gson().fromJson(jsonError, Message::class.java)
                                        mView?.showError(error.message ?: it.showHttpError())
                                    } catch (e: JsonSyntaxException) {
                                        e.printStackTrace()
                                        mView?.showError(it.showHttpError())
                                    }
                                }
                            }
                    )
                } else {

                }
            }
        }
    }

    private fun validate(name: String, email: String, phone: String): Boolean {
        var isPass = true
        if (name.isBlank()) {
            mView?.nameError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        }

        if (email.isBlank()) {
            mView?.nameError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        }

        if (phone.isBlank()) {
            mView?.phoneError(mView?.getStringFromRes(R.string.please_fill_out_this_field) ?: "")
            isPass = false
        }

        if (!isEmailValid(email)) {
            mView?.emailError(mView?.getStringFromRes(R.string.please_enter_a_valid_email) ?: "")
            isPass = false
        }

        return isPass
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}
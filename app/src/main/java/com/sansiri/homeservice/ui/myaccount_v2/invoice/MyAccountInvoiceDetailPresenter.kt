package com.sansiri.homeservice.ui.myaccount_v2.invoice

import com.sansiri.homeservice.data.repository.payment.PaymentRepository
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentRequest
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import retrofit2.HttpException

class MyAccountInvoiceDetailPresenter : BasePresenterImpl<MyAccountInvoiceDetailContract.View>(), MyAccountInvoiceDetailContract.Presenter {
    private var mInvoice: Invoice? = null

    override fun init(invoice: Invoice) {
        this.mInvoice = invoice
    }

    override fun start() {
        if (mInvoice != null) {
            mView?.bindData(mInvoice!!)
        } else {

        }
    }

    override fun destroy() {
    }
}
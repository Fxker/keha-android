package com.sansiri.homeservice.data.network.firebase

import android.net.Uri
import android.util.Log
import com.google.firebase.firestore.*
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.util.toJson
import java.lang.IllegalStateException
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.UploadTask
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.chat.ChatTemplate
import com.sansiri.homeservice.util.observe


/**
 * Created by sansiri on 12/21/17.
 */
class FireStoreManager(val projectId: String, val unitId: String) {
    companion object {
        val CHAT_COUNT_IN_PAGE = 10
        val UNREAD_COUNT = "staffUnreadCount"
    }

    private var mDb = FirebaseFirestore.getInstance()
    private var mStorage = FirebaseStorage.getInstance()
    private var mChatRef = mDb.collection("chats").document(projectId)
            .collection("units").document(unitId)
    private var mChatTemplateRef = mDb.collection("messages").document("template")
    private var mRegistration: ListenerRegistration? = null

    private var lastVisibleItem: DocumentSnapshot? = null
    private var loadFirstConversation: Boolean = true

    fun attachChatRoom(onSuccess: (List<ChatMessage>) -> Unit) {
        mRegistration = mChatRef.collection("messages")
                .orderBy("createdAt", Query.Direction.DESCENDING)
                .limit(CHAT_COUNT_IN_PAGE.toLong())
                .addSnapshotListener(EventListener<QuerySnapshot> { snapshots, e ->
                    if (e != null) {
                        Log.d("FireStoreManager", "Listen failed.", e)
                        return@EventListener
                    }

                    val chatList = convertSnapshotToChatMessage(loadFirstConversation, snapshots)
                    loadFirstConversation = false
                    onSuccess(chatList)
                })
    }

    fun loadMoreConversation(onSuccess: (List<ChatMessage>) -> Unit, onFail: (Exception) -> Unit) {
        lastVisibleItem?.let {
            mChatRef.collection("messages")
                    .orderBy("createdAt", Query.Direction.DESCENDING)
                    .startAfter(it)
                    .limit(CHAT_COUNT_IN_PAGE.toLong())
                    .get()
                    .addOnSuccessListener { snapshots ->
                        val chatList = convertSnapshotToChatMessage(true, snapshots)
                        onSuccess(chatList)
                    }
                    .addOnFailureListener {
                        onFail(it)
                    }
        }
    }

    fun sendMessage(message: ChatMessage, onSuccess: (ChatMessage) -> Unit, onFail: (Exception) -> Unit) {
        mChatRef.collection("messages")
                .add(message)
                .addOnSuccessListener {
                    onSuccess(message)
                    setUnReadCount()
                }
                .addOnFailureListener {
                    onFail(it)
                }
    }

    fun notifyJuristic(home: Hut, chat: ChatMessage) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            api?.notifyJuristic(home.projectId, home.unitId, chat)?.observe()?.subscribe({}){}
        }
    }

    fun setUnReadCount() {
        getUnReadCount { count ->
            mChatRef.set(mapOf(Pair(UNREAD_COUNT, count + 1)), SetOptions.merge())
                    .addOnSuccessListener {

                    }.addOnFailureListener {

                    }
        }

    }

    fun getUnReadCount(onSuccess: (Long) -> Unit) {
        mChatRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                try {
                    val unreadCount = document?.getLong(UNREAD_COUNT)
                    if (unreadCount != null) {
                        onSuccess(unreadCount)
                    } else {
                        onSuccess(0)
                    }
                } catch (e: IllegalStateException) {
                    onSuccess(0)
                } catch (e: RuntimeException) {
                    try {
                        val unreadCountText = document?.getString(UNREAD_COUNT)
                        val unreadCount = unreadCountText?.toLong()
                        onSuccess(unreadCount ?: 0)
                    } catch (e: Exception) {
                        onSuccess(0)
                    }
                }
            } else {

            }
        }
    }

    fun getChatTemplateDefault(onSuccess: (List<ChatTemplate>) -> Unit, onFail: (Exception) -> Unit) {
        mChatTemplateRef.collection("default")
                .orderBy("weight", Query.Direction.ASCENDING)
                .get()
                .addOnSuccessListener { snapshots ->
                    val chatTemplateList = convertSnapshotToChatTemplate(snapshots)
                    onSuccess(chatTemplateList)
                }
                .addOnFailureListener {
                    onFail(it)
                }
    }

    fun detach() {
        mRegistration?.remove()
    }

    private fun convertSnapshotToChatMessage(saveLastVisibleItem: Boolean, snapshots: QuerySnapshot?): List<ChatMessage> {
        if (snapshots == null || snapshots.isEmpty) return listOf()

        if (saveLastVisibleItem) {
            lastVisibleItem = snapshots.documents
                    .get(snapshots.size() - 1)
        }

        return snapshots.documentChanges
                .filter { it.type == DocumentChange.Type.ADDED }
                .map { dc ->
                    return@map Gson().fromJson(dc.document.data.toJson(), ChatMessage::class.java)
                }
    }

    private fun convertSnapshotToChatTemplate(snapshots: QuerySnapshot?): List<ChatTemplate> {
        if (snapshots == null || snapshots.isEmpty) return listOf()

        return snapshots.documents
                .map { dc ->
                    return@map Gson().fromJson(dc.data?.toJson(), ChatTemplate::class.java)
                }
    }


    fun uploadImage(fileName: String, imageFile: ByteArray, onProgress:(Double) -> Unit, onSuccess: (Uri) -> Unit, onFail: (Exception) -> Unit) {
        val ref = mStorage.getReference("chat").child("images/${System.currentTimeMillis()}_${fileName}")
        ref.putBytes(imageFile).addOnProgressListener { taskSnapshot ->
            val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.getTotalByteCount()
            onProgress(progress)
        }.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful && task.result != null) {
                onSuccess(task.result!!)
            } else {
                onFail(IllegalArgumentException("Upload Fail"))
            }
        }
    }
}
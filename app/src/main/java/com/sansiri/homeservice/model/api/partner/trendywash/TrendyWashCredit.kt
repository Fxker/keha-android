package com.sansiri.homeservice.model.api.partner.trendywash

data class TrendyWashCredit(
        val id: String? = null,
        val name: String? = null,
        val email: String? = null,
        val joinDate: String? = null,
        val birthDate: String? = null,
        val tokenId: String? = null,
        val os: String? = null,
        val facebookId: String? = null,
        val appVersion: String? = null,
        val creditCardText: String? = null,
        val mobileNo: String? = null,
        val customerStatus: String? = null,
        val remainCredits: String? = null,
        val lastActive: String? = null,
        val creditList: List<Int>? = null
)

package com.sansiri.homeservice.ui.electric.mea.home

import com.mea.energysdk.widget.MEAEnergyWidget
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MeaElectricHomeContract {
    interface Presenter : BasePresenter<View> {
        fun loadWidget(meaWidget: MEAEnergyWidget?, ca: String)
    }

    interface View : BaseView {
    }
}
package com.sansiri.homeservice.model

/**
 * Created by sansiri on 11/1/17.
 */
data class Inspection(val title: String, val subTitle: String, val status: String)
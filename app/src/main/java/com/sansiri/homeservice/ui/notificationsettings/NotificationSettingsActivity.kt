package com.sansiri.homeservice.ui.notificationsettings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.NOTIFICATION_ALL
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.ToggleMenu
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.defaultSharedPreferences


class NotificationSettingsActivity : BaseActivity<NotificationSettingsContract.View, NotificationSettingsContract.Presenter>(), NotificationSettingsContract.View {
    override var mPresenter: NotificationSettingsContract.Presenter = NotificationSettingsPresenter()

    val mAdapter = GridMenuAdapter(null, null) { menu ->
        if (menu is ToggleMenu) {
            menu.isChecked = !menu.isChecked
            updatePref(menu.id ?: "", menu.isChecked)

            if (menu.id == Feature.NOTIFICATION_SETTINGS_ALL) {
                sendEvent("NOTIFICATION_SETTINGS", if (menu.isChecked) "ON" else "OFF", "ALL_NOTIFICATION" )
            }
        }
        refresh()
    }

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, NotificationSettingsActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setBackToolbar(resources.getString(R.string.notification_settings))

        list.apply {
            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            adapter = mAdapter
        }

        val prefs = PreferenceHelper.defaultPrefs(this)
        val notificationPrefs = prefs[NOTIFICATION_ALL, true] ?: true

        bindData(listOf<Menu>(
                ToggleMenu(Feature.NOTIFICATION_SETTINGS_ALL, resources.getString(R.string.notification), null, notificationPrefs)
        ))
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("NOTIFICATION_SETTINGS")
    }

    override fun bindData(menu: List<Menu>) {
        mAdapter.setData(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun refresh() {
        mAdapter.notifyDataSetChanged()
    }

    private fun updatePref(feature: String, checked: Boolean) {
        val prefs = PreferenceHelper.defaultPrefs(this)
        if (feature == Feature.NOTIFICATION_SETTINGS_ALL) {
            prefs[NOTIFICATION_ALL] = checked
        }
    }

    override fun showError(message: String) {
        error(message)
    }
}

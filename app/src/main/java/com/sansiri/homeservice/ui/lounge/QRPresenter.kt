package com.sansiri.homeservice.ui.lounge

import com.google.zxing.MultiFormatWriter
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.generateQR
import com.sansiri.homeservice.util.observe
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by sansiri on 11/30/17.
 */
class QRPresenter : BasePresenterImpl<QRContract.View>(), QRContract.Presenter {
    override fun start() {
        fetchData()
    }

    override fun destroy() {
    }

    override fun fetchData() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if(it != null) {
                it.getQR().observe().subscribe({
                    renderQR(it.data)
                }){
                    mView?.hideLoading()
                }
            }
        }
    }

    private fun renderQR(data: String) {
        doAsync {
            val bitmap = MultiFormatWriter().generateQR(data)
            uiThread {
                mView?.hideLoading()
                mView?.bindQR(bitmap)
            }
        }
    }
}
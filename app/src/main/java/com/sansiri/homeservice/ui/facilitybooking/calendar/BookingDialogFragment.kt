package com.sansiri.homeservice.ui.facilitybooking.calendar

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.TimeDialogFragment
import com.sansiri.homeservice.component.bottomsheet.TimePickerBottomSheet
import kotlinx.android.synthetic.main.dialog_booking.*
import kotlinx.android.synthetic.main.dialog_general.*
import java.util.*

/**
 * Created by sansiri on 10/16/17.
 */
class BookingDialogFragment(): TimeDialogFragment() {
    @SuppressLint("ValidFragment")
    constructor(onSubmit: ((Calendar, Calendar) -> Unit)): this() {
        this.onSubmit = onSubmit
    }

    private var mStartTime: Calendar? = null
    private var mEndTime: Calendar? = null
    private var onSubmit: ((Calendar, Calendar) -> Unit)? = null


    private lateinit var  mTimeStartPicker: TimePickerBottomSheet
    private lateinit var  mTimeEndPicker: TimePickerBottomSheet


    override val onClickListener: () -> Unit = {
        if(mStartTime != null && mEndTime != null) {
            onSubmit?.invoke(mStartTime!!, mEndTime!!  )
        }
        dismiss()
    }

    companion object {
        val TIME = "TIME"
        fun newInstance(startTime: Calendar, onSubmit: (startTime: Calendar, endTime: Calendar) -> Unit): BookingDialogFragment {
            val fragment = BookingDialogFragment(onSubmit)
            val bundle = Bundle()
            bundle.putSerializable(TIME, startTime)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sentTime = arguments?.getSerializable(TIME) as Calendar
        mStartTime = sentTime.clone() as Calendar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_booking, null)
        panelContent.addView(view)

        mEndTime = mStartTime?.clone() as Calendar
        mEndTime?.add(Calendar.HOUR, 1)

        textTimeStart.text = getTextTime(mStartTime)
        textTimeEnd.text = getTextTime(mEndTime)

        mTimeStartPicker = TimePickerBottomSheet(mStartTime!!) { time ->
            textTimeStart.text = getTextTime(time)
        }

        mTimeEndPicker = TimePickerBottomSheet(mEndTime!!) { time ->
            textTimeEnd.text = getTextTime(time)
        }

        textTimeStart.setOnClickListener {
            mTimeStartPicker.show(fragmentManager!!, "BookingDialogFragment")
        }
        textTimeEnd.setOnClickListener {
            mTimeEndPicker.show(fragmentManager!!, "BookingDialogFragment")
        }


    }
}

package com.sansiri.homeservice.model.api

import com.google.gson.annotations.SerializedName

data class VisitorAccessRequest(

        @SerializedName("visitorFirstname")
        val visitorFirstName: String? = null,

        @SerializedName("visitorLastname")
        val visitorLastName: String? = null,

        @SerializedName("visitorCitizenId")
        val visitorCitizenId: String? = null,

        @SerializedName("visitorMobile")
        val visitorMobile: String? = null,

        @SerializedName("visitorAccessTime")
        val visitorAccessTime: String? = null,

        @SerializedName("accessOption")
        val accessOption: String? = null,

        @SerializedName("effectiveTime")
        val effectiveTime: String? = null

) {
        companion object {
            val UNLIMITED = "0"
            val ONE_TIME = "1"
        }
}
package com.sansiri.homeservice.ui.maintenance.detail

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDetailSubmit
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

class MaintenanceDetailPresenter : BasePresenterImpl<MaintenanceDetailContract.View>(), MaintenanceDetailContract.Presenter {
    var mHome: Hut? = null
    var mPlan: MaintenancePlan? = null
    var mPlanId: String? = null
    var mAccessoryId: String? = null


    override fun init(home: Hut, accessoryId: String, planId: String?, plan: MaintenancePlan?) {
        this.mHome = home
        this.mPlan = plan
        this.mPlanId = planId
        this.mAccessoryId = accessoryId
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                if (mPlan != null) {
                    mView?.bindData(mPlan)
                } else if (mPlanId != null) {
                    mCompositeDisposable.add(
                            api.getMaintenancePlan(mHome?.unitId ?: "", mPlanId
                                    ?: "").observe().subscribe({ plans ->
                                val data = plans.filter { it.accessoryId == mAccessoryId }.getOrNull(0)
                                mView?.bindData(data)
                            }) {

                            }
                    )
                }

                mCompositeDisposable.add(
                        api.getMaintenanceAccessory(mHome!!.unitId, mPlan?.accessoryId ?: mAccessoryId
                                ?: "").observe().subscribe({ detail ->
                            mView?.showWeb(detail.html ?: "")
                        }) {
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }

    override fun submit() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                api.submitMaintenanceAccessory(
                        mHome!!.unitId,
                        mPlan?.accessoryId ?: "",
                        MaintenanceDetailSubmit(DateTime.now(DateTimeZone.UTC).toString())
                ).observe().subscribe({
                    mView?.showSubmitSuccess(mAccessoryId ?: "")
                }) {
                    mView?.showError(it.showHttpError())
                }
            }
        }
    }

    override fun destroy() {
    }
}
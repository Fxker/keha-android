package com.sansiri.homeservice.ui.inspection

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 12/12/17.
 */
class InspectionPresenter() : BasePresenterImpl<InspectionContract.View>(), InspectionContract.Presenter {
    private var unitObjectId: String? = null

    override fun init(unitObjectId: String?) {
        this.unitObjectId = unitObjectId
    }

    override fun start() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null && unitObjectId != null) {
                mView?.showLoading()
                mCompositeDisposable.add(
                        api.getInspection(unitObjectId!!).observe().subscribe({
                            mView?.hideLoading()
                            if (it.isEmpty()) {
                                mView?.showNoData()
                            } else {
                                mView?.bindData(it)
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }

//    override fun classifyDate(inspections: List<Inspection>) {
//        val mapSection = hashMapOf<String, MutableList<Inspection>>()
//
//        inspections.forEach {
//            val date = it.displayDate?.report() ?: ""
//
//            if (mapSection[date] == null) {
//                mapSection.put(date, mutableListOf(it))
//            } else {
//                mapSection[date]?.add(it)
//            }
//        }
//
//        mapSection.toSortedMap()
//    }

    override fun destroy() {
    }
}
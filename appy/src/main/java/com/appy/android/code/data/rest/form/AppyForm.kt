package com.appy.android.code.data.rest.form

import com.appy.android.code.data.rest.params.ParamsMap
import com.appy.android.sdk.AppySDK

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
internal open class AppyForm  {

    var headers: HashMap<String, String> = createHeaders()
    var params: ParamsMap? = createParams()

    open fun addParam(key: String, value: String) {
        params?.put(key, value)
    }

    open fun removeParam(key: String) {
        params?.remove(key)
    }

    open fun createParams(): ParamsMap {
        val params = ParamsMap()

        AppySDK.instance().pin?.let {
            params.put("pin_code", it)
        }

        AppySDK.instance().unit?.let {
            params.put("unit", it)
        }

        return params
    }

    open fun createHeaders(): HashMap<String, String> {
        val headers = HashMap<String, String>()

        AppySDK.instance().apiKey?.let {
            headers.put("appy_api_key", it)
        }

        AppySDK.instance().appKey?.let {
            headers.put("appy_app_key", it)
        }

        return headers
    }

    open fun isValid(): Boolean {
        return true
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.registration

import android.util.Patterns
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class TrendyWashRegisterPresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TrendyWashRegisterContract.View>(), TrendyWashRegisterContract.Presenter {
    private var mUnitId: String? = null
    private var mFirstName: String? = null
    private var mLastName: String? = null
    private var mEmail: String? = null
    private var mTel: String? = null
    private var mPassword: String? = null
    private var mPasswordConfirm: String? = null

    override fun init(unitId: String) {
        mUnitId = unitId
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                api.getMe().observe().subscribe({ profile ->
                    mView?.bindData(profile)
                }) {}
            }
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun setFirstName(firstName: String) {
        mFirstName = firstName
    }

    override fun setLastName(lastName: String) {
        mLastName = lastName
    }

    override fun setEmail(email: String) {
        mEmail = email
    }

    override fun setTel(tel: String) {
        mTel = tel
    }

    override fun setPassword(password: String) {
        mPassword = password
    }

    override fun setPasswordConfirm(passwordConfirm: String) {
        mPasswordConfirm = passwordConfirm
    }

    override fun validate(): Boolean {
        var isFail = false
        if (mEmail == null) {
            mView?.showEmailError(R.string.please_fill_out_this_field)
            isFail = true
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            mView?.showEmailError(R.string.please_enter_a_valid_email)
            isFail = true
        }

        return !isFail
    }

    override fun submit() =
            repository.registerTrendyWashAccount(mUnitId!!, TrendyWashRegisterCustomer(
                    name = "$mFirstName $mLastName",
                    email = mEmail,
                    mobileno = mTel,
                    password = mPassword
            ))


}
package com.sansiri.homeservice.ui.bos.type

import com.sansiri.homeservice.data.repository.bos.BosRepository
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import io.reactivex.rxkotlin.plusAssign

class BosTypePresenter(val repository: BosRepository) : BasePresenterImpl<BosTypeContract.View>(), BosTypeContract.Presenter {
    var mUnitId: String? = null
    var mGroupType: Int = 0

    override fun init(unitId: String) {
        this.mUnitId = unitId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
    }

    override fun fetch() {
        if (mUnitId != null) {
            mView?.showLoading()
            mCompositeDisposable += repository.getBosType(mUnitId!!).observe().subscribe({
                mView?.bindJobTypes(it.items)
                mView?.hideLoading()
            }, {
                mView?.showError(it.showHttpError())
                mView?.hideLoading()
            })
        }
    }

    override fun selectAreaType(groupType: Int) {
        mGroupType = groupType
    }

    override fun selectJobType(jobType: BosMenu) {
        mView?.launchBosDetail(mGroupType, jobType)
    }
}
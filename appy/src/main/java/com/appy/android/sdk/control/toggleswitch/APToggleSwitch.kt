package com.appy.android.sdk.control.toggleswitch

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

class APToggleSwitch : APControl, Parcelable {
    var mActionToggles: List<APActionToggleSwitch>? = null

    override val actions: List<APAction<*>>
        get() = mActionToggles.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActionToggles = source.createTypedArrayList(APActionToggleSwitch)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActionToggles = controlJsonModel.actions?.map {
            val action = APActionToggleSwitch()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                ToggleSwitchCommand.OFF.action() -> {
                    action.action = ToggleSwitchCommand.OFF
                }
                ToggleSwitchCommand.ON.action() -> {
                    action.action = ToggleSwitchCommand.ON
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActionToggles)
    }

    companion object CREATOR : Parcelable.Creator<APToggleSwitch> {
        const val TYPE = "APToggleSwitch"

        override fun createFromParcel(parcel: Parcel): APToggleSwitch {
            val type = parcel.readString()
            return APToggleSwitch(parcel)
        }

        override fun newArray(size: Int): Array<APToggleSwitch?> {
            return arrayOfNulls(size)
        }
    }
}
package com.appy.android.code.data.rest.JsonModel

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
internal class RoomJsonModel {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("alternate_title")
    var titleAlternate: String? = null
    @SerializedName("icon")
    var icon: ImageJsonModel? = null
    @SerializedName("background")
    var background: ImageJsonModel? = null
    @SerializedName("controls")
    var controls: List<ControlJsonModel>? = ArrayList()
}
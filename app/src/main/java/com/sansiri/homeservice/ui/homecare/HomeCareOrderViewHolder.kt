package com.sansiri.homeservice.ui.homecare

import android.graphics.Color
import android.view.View
import com.bumptech.glide.Glide
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.tintImage
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class HomeCareOrderViewHolder(val view: View, val itemClick: (homeCareHistory: HomeCareHistory) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: HomeCareHistory) {
        with(view) {
            root.isClickable = true
            root.setOnClickListener {
                itemClick(data)
            }
            data.typeIconUrl?.let {
                Glide.with(context).loadCache(it, imageIcon)
            }

            imageIcon.tintImage(Color.parseColor(data.statusBgColor))
            textType.setTextColor(Color.parseColor(data.statusBgColor))
            textType.text = data.statusName
            textDate.text = data.createdDate?.report() ?: "-"
            textTitle.text = data.typeName
            textSubtitle.text = data.detail
        }
    }
}
package com.sansiri.homeservice.data

interface IFeatureConfig {
    val ANNOUNCEMENT_ALL: Boolean
    val ANNOUNCEMENT_PROJECT: Boolean
    val SANSIRI_FAMILY: Boolean
    val VOICE_COMMAND: Boolean
    val GOOGLE_ASSISTANT: Boolean
    val LOUNGE: Boolean
    val IS_SUPPORT_CALLCENTER: Boolean
}
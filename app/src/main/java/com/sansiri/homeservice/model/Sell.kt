package com.sansiri.homeservice.model

import java.util.*

/**
 * Created by sansiri on 11/7/17.
 */
data class Sell(val address: String, val title: String, val status: String, val date: Calendar)
package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.coolautomation.CoolAutomationCommand

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
internal class SendCommandCoolAutomationForm(
        val homeId: String,
        val coolId: String,
        val command: CoolAutomationCommand
) : AppyForm() {
    val mSendCommandCoolAutomationJsonBody: SendCommandCoolAutomationJsonBody = SendCommandCoolAutomationJsonBody()

    init {
        this.mSendCommandCoolAutomationJsonBody.command = command.action()
    }

}
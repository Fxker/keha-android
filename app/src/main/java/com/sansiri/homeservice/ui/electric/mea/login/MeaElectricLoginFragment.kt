package com.sansiri.homeservice.ui.electric.mea.login


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import com.sansiri.homeservice.util.TextWatcherExtend
import kotlinx.android.synthetic.main.fragment_electric_mea_login.*
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainActivity
import com.sansiri.homeservice.ui.scanner.QRScannerActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import org.koin.androidx.scope.currentScope

class MeaElectricLoginFragment : BaseV2Fragment<MeaElectricLoginContract.View, MeaElectricLoginContract.Presenter>(), MeaElectricLoginContract.View {
    override val mPresenter: MeaElectricLoginContract.Presenter by currentScope.inject()
    private var mHome: Hut? = null
    private var mIconUrl: String? = null
    private var mMEAInfo: MEAInfo? = null

    companion object {
        const val HOME = "HOME"
        const val ICON_URL = "ICON_URL"
        const val MEA_INFO = "MEA_INFO"

        fun newInstance(home: Hut, meaInfo: MEAInfo, iconUrl: String?): MeaElectricLoginFragment {
            return MeaElectricLoginFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(HOME, home)
                    putParcelable(MEA_INFO, meaInfo)
                    putString(ICON_URL, iconUrl)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHome = arguments?.getParcelable<Hut>(HOME)
        mIconUrl = arguments?.getString(ICON_URL)
        mMEAInfo = arguments?.getParcelable<MEAInfo>(MEA_INFO)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MEA_REGISTER")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_electric_mea_login, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editCA.addTextChangedListener(TextWatcherExtend {
            checkLoginButtonAvailability()
        })

        checkLoginButtonAvailability()

        mIconUrl?.let { Glide.with(this).load(it).into(imageIcon) }

        if (BuildConfig.DEBUG) {
            editCA.setText("014163413")
        }

        buttonLogin.setOnClickListener {
            sendEvent("MEA_REGISTER", "MEA_REGISTER", "REGISTER")
            mPresenter.login(editCA.text.toString())
        }
        buttonQR.setOnClickListener {
            sendEvent("MEA_REGISTER", "MEA_REGISTER", "SCAN_BARCODE")
            QRScannerActivity.start(this)
        }

        mHome?.let { mPresenter.init(mHome!!, mMEAInfo) }
        mPresenter.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == QRScannerActivity.CODE) {
            val code = data?.getStringExtra(QRScannerActivity.DATA)
            mPresenter.process(code)
//            editCA.setText()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun launchHome(ca: String) {
        (activity as MEAElectricMainActivity).launchHome(ca)
    }

    override fun fillCA(ca: String?) {
        editCA.setText(ca)
    }

    override fun showLoading() {
        progressBar.show()
        buttonLogin.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonLogin.show()
    }

    private fun checkLoginButtonAvailability() {
        val isEnable = editCA.text.toString().isNotEmpty()
        buttonLogin.isEnabled = isEnable
        buttonLogin.alpha = if (isEnable) 1f else 0.5f
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

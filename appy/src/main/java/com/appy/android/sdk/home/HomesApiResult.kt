package com.appy.android.sdk.home

import com.appy.android.code.base.data.rest.result.AppyApiResult

/**
 * Created by ssa-dev-4 on 26/9/2560.
 */
class HomesApiResult : AppyApiResult<List<APHome>>()
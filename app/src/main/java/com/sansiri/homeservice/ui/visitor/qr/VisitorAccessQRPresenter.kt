package com.sansiri.homeservice.ui.visitor.qr

import com.sansiri.homeservice.ui.base.BasePresenterImpl

class VisitorAccessQRPresenter : BasePresenterImpl<VisitorAccessQRContract.View>(), VisitorAccessQRContract.Presenter {

    private var mCode: String? = null

    override fun init(code: String) {
       this.mCode = code
    }

    override fun start() {
        if (mCode != null) {
            mView?.generateQRBitmap(mCode!!)
        }
    }

    override fun destroy() {
    }
}
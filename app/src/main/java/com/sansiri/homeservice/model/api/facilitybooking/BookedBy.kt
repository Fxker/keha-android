package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class BookedBy(val phone: String = "",
                    @SerializedName("last_name")
                    val lastName: String = "",
                    val id: String = "",
                    @SerializedName("booking_id")
                    val bookingId: String = "",
                    val avatar: String = "",
                    @SerializedName("first_name")
                    val firstName: String = "",
                    val email: String = "",
                    val note: String = "") : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(phone)
        writeString(lastName)
        writeString(id)
        writeString(bookingId)
        writeString(avatar)
        writeString(firstName)
        writeString(email)
        writeString(note)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<BookedBy> = object : Parcelable.Creator<BookedBy> {
            override fun createFromParcel(source: Parcel): BookedBy = BookedBy(source)
            override fun newArray(size: Int): Array<BookedBy?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.data.network.firebase

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.model.api.MultiLanguageMessage
import com.sansiri.homeservice.model.menu.DynamicMenu
import java.util.*


/**
 * Created by oakraw on 25/3/2018 AD.
 */
class FirebaseRemoteConfigManager() {
    companion object {
        val LOGIN_PRIORITY = "login_priority_android"
        val FORGOT_PASSWORD = "forgot_password_android"
        val VOICE_CONTROL = "voice_control_android"
        val SANSIRI_FAMILY_URL = "sansiri_family_privilege_url"
        val HOME_WALLPAPER = "home_wallpaper"
        val PRIVILEGE_DYNAMIC_BUTTON = "privilege_dynamic_button"
        val LOGO_IMAGE = "logo_image"
        val HOME_CARE_NOTICE = "home_care_notice"
        val THEME = "theme"

        private var fetchSuccess = false
    }

    var cacheExpiration: Long = 43200
    val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

    init {
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        mFirebaseRemoteConfig.setConfigSettings(configSettings)
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults)
    }

    fun fetch(onSuccess: (FirebaseRemoteConfig) -> Unit) {
        if (fetchSuccess) {
            onSuccess(mFirebaseRemoteConfig)
            return
        }

        if (mFirebaseRemoteConfig.info.configSettings.isDeveloperModeEnabled) {
            cacheExpiration = 0
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                fetchSuccess = true
                mFirebaseRemoteConfig.activateFetched()
            }
            onSuccess(mFirebaseRemoteConfig)
        }
    }

    fun getPrivilegeDynamicButton(onSuccess: (DynamicMenu) -> Unit) {
        fetch {
            val json = it.getString(PRIVILEGE_DYNAMIC_BUTTON)
            try {
                val dynamicMenu = Gson().fromJson<DynamicMenu>(json, DynamicMenu::class.java)
                onSuccess(dynamicMenu)
            } catch (e: Exception) {
            }
        }
    }

    fun getHomeCareNotice(onSuccess: (String?) -> Unit) {
        fetch {
            val json = it.getString(HOME_CARE_NOTICE)
            try {
                val message = Gson().fromJson<MultiLanguageMessage>(json, MultiLanguageMessage::class.java)
                onSuccess(
                        if (Store.getLocale() == Locale("th")) {
                            message.th
                        } else {
                            message.en
                        }
                )
            } catch (e: Exception) {
                onSuccess(null)
            }
        }
    }
}
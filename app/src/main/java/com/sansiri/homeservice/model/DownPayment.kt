package com.sansiri.homeservice.model

import java.util.*

/**
 * Created by sansiri on 10/18/17.
 */
data class DownPayment(val title: String = "", val description: String = "", val date: Calendar, val type: String)
package vc.siriventures.bathome.constants

import java.lang.Exception

class BAtHomeException(val status: Int): Exception() {
    companion object {
        const val API_ERROR = 0
        const val HOST_NOT_FOUND = 1
    }
}
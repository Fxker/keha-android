package com.sansiri.homeservice.ui.visitor

import com.sansiri.homeservice.model.api.TextValue
import com.sansiri.homeservice.model.api.VisitorAccessRequest
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

interface VisitorAccessContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun setFirstName(firstName: String)
        fun setLastName(lastName: String)
        fun setIDCard(idCard: String)
        fun setTel(tel: String)
        fun setDate(calendar: Calendar)
        fun validate(selectedOption: Int)
        fun submit(request: VisitorAccessRequest)
        fun fetch()
    }

    interface View : BaseView {
        fun showFirstNameError()
        fun showLastNameError()
        fun showIDCardError()
        fun showTelError()
        fun showLoading()
        fun hideLoading()
        fun launchQR(code: String)
        fun showAccessOptions(options: List<TextValue>)
    }
}
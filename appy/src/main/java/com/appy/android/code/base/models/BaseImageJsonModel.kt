package com.appy.android.code.base.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
abstract class BaseImageJsonModel : Parcelable {
    @SerializedName("original")
    var original: String? = null

    @SerializedName("ah_xxlarge")
    var xxLarge: String? = null

    @SerializedName("ah_xlarge")
    var xLarge: String? = null

    @SerializedName("ah_large")
    var large: String? = null

    @SerializedName("ah_medium")
    var medium: String? = null

    @SerializedName("ah_small")
    var small: String? = null

    @SerializedName("ah_thumb")
    var thumb: String? = null

    constructor()

    constructor(source: Parcel) : this() {
        original = source.readString()
        xxLarge = source.readString()
        xLarge = source.readString()
        large = source.readString()
        medium = source.readString()
        small = source.readString()
        thumb = source.readString()
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(original)
        dest.writeString(xxLarge)
        dest.writeString(xLarge)
        dest.writeString(large)
        dest.writeString(medium)
        dest.writeString(small)
        dest.writeString(thumb)
    }
}
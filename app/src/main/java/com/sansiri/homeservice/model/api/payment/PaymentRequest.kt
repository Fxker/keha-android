package com.sansiri.homeservice.model.api.payment

data class PaymentRequest(
	val paymentCommand: String? = null,
	val mobileNo: String? = null
)

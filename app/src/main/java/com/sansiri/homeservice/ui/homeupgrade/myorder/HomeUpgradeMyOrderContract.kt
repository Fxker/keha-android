package com.sansiri.homeservice.ui.homeupgrade.myorder

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeUpgradeMyOrderContract {
    interface Presenter : BasePresenter<View> {
        fun init(history: HomeUpgradeHistory)
    }

    interface View : BaseView {
        fun bindData(history: HomeUpgradeHistory)
        fun bindOrderList(summary: HashMap<String, MutableList<HomeUpgradeHardware>>)
    }
}
package com.sansiri.homeservice.util

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by oakraw on 28/10/2017 AD.
 */
class TextWatcherExtend(private val textChanged:(String) -> Unit): TextWatcher {
    override fun afterTextChanged(p0: Editable) {
        textChanged(p0.toString())
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
package com.sansiri.homeservice.ui.projectprogress

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.View
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_image_project_progress.view.*


/**
 * Created by sansiri on 11/6/17.
 */
class ProjectImageAdapter(val context: Context): androidx.viewpager.widget.PagerAdapter() {

    private val mImages: MutableList<String> = mutableListOf()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = container.inflate(R.layout.view_image_project_progress)
        Glide.with(view).loadCache(mImages[position], view.image)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container?.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = mImages.size

    fun setData(images: List<String>) {
        mImages.clear()
        mImages.addAll(images)
        notifyDataSetChanged()
    }
}
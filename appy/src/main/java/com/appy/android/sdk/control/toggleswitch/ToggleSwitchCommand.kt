package com.appy.android.sdk.control.toggleswitch

import com.appy.android.sdk.control.APCommand

enum class ToggleSwitchCommand : APCommand {
    OFF {
        override fun action(): String {
            return "CONTROL_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "CONTROL_ON"
        }
    };

    companion object {
        @JvmStatic
        fun getLastCommand(value: String): String {
            return ToggleSwitchCommand.values()[value.toInt()].action()
        }

        @JvmStatic
        fun getAvailableCommand(value: String): List<String> {
            val toggleSwitchCommandArray = arrayListOf<String>()
            ToggleSwitchCommand.values().forEach {
                if (it.action() != value) {
                    toggleSwitchCommandArray.add(it.action())
                }
            }
            return toggleSwitchCommandArray
        }
    }
}
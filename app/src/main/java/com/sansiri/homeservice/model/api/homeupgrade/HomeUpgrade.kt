package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcelable
import com.sansiri.homeservice.data.database.Store

interface HomeUpgrade: Parcelable {
	val id: String
	val nameEn: String?
	val nameTh: String?

	val name: String
	get() =
		if(Store.getLocale()?.language == "th" && nameTh != null) {
			nameTh!!
		} else {
			nameEn ?: ""
		}

	companion object {
		val AWAITING_APPOINTMENT = "awaiting-appointment"
		val AWAITING_INSTALLATION = "awaiting-installation"
	}
}
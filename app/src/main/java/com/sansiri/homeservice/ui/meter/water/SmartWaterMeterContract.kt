package com.sansiri.homeservice.ui.meter.water

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SmartWaterMeterContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        var smartWaterMeter: MutableLiveData<Resource<SmartWaterMeterOverAll>>?
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
    }
}
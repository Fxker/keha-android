package com.sansiri.homeservice.ui.projectprogress

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.ProjectProgress
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_project_progress.*
import kotlinx.android.synthetic.main.view_project_progress_report.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import java.util.*

class ProjectProgressActivity : BaseActivity<ProjectProgressContract.View, ProjectProgressContract.Presenter>(), ProjectProgressContract.View {
    override var mPresenter: ProjectProgressContract.Presenter = ProjectProgressPresenter()

    val mAdapter = ProjectImageAdapter(this)
    private var mProjectTitle: String = ""

    companion object {
        val PROJECT_ID = "PROJECT_ID"
        val PROJECT_TITLE = "PROJECT_TITLE"
        val TITLE = "TITLE"

        fun start(activity: Activity, projectId: String, projectTitle: String, title: String?) {
            val intent = Intent(activity, ProjectProgressActivity::class.java)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(PROJECT_TITLE, projectTitle)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_progress)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.project_progress))

        viewPager.adapter = mAdapter
        pageIndicatorView.setViewPager(viewPager)

//        mPlanAdapter.setData(mutableListOf(
//                R.drawable.the_line_101_1.toString(),
//                R.drawable.the_line_101_2.toString(),
//                R.drawable.the_line_101_3.toString(),
//                R.drawable.the_line_101_4.toString(),
//                R.drawable.the_line_101_5.toString()
//        ))

        val projectId = intent?.getStringExtra(PROJECT_ID)
        mProjectTitle = intent?.getStringExtra(PROJECT_TITLE) ?: ""
        if (projectId != null) {
            mPresenter.fetchData(projectId)
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("PROJECT_PROGRESS")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(projectProgress: ProjectProgress) {
        textName.text = mProjectTitle

        textDate.text = Calendar.getInstance().time.reportMMY()

        if (projectProgress.imagesUrl != null) {
            val imagesUrl = mutableListOf<String>()
            projectProgress.imagesUrl.forEach {
                imagesUrl.add(it.imageUrl)
            }
            mAdapter.setData(imagesUrl)
            pageIndicatorView.setViewPager(viewPager)
        } else {
            mAdapter.setData(listOf(
                    projectProgress.imageurl ?: ""
            ))
        }

        textSummaryProgress.text = "${Math.round(projectProgress.progressoverall)}%"
        progressBarSummary.progress = Math.round(projectProgress.progressoverall)
        textSummarySubtitle.text = projectProgress.shorttitle

        textSummaryDescription.text = "${projectProgress.eia}\n${projectProgress.consts}"

        val viewStructure = panelDetail.inflate(R.layout.view_project_progress_report)
        viewStructure.textTitle.text = resources.getString(R.string.structure)
        viewStructure.textProgress.text = "${Math.round(projectProgress.prostructure)}%"
        viewStructure.progressBar.progress = Math.round(projectProgress.prostructure)
        viewStructure.textDescription.text = "${projectProgress.prostructuretxt}"
        panelDetail.addView(viewStructure)

        val viewSystem = panelDetail.inflate(R.layout.view_project_progress_report)
        viewSystem.textTitle.text = resources.getString(R.string.system)
        viewSystem.textProgress.text = "${Math.round(projectProgress.prosystem)}%"
        viewSystem.progressBar.progress = Math.round(projectProgress.prosystem)
        viewSystem.textDescription.text = "${projectProgress.prosystemtxt}"
        panelDetail.addView(viewSystem)

        val viewArchitect = panelDetail.inflate(R.layout.view_project_progress_report)
        viewArchitect.textTitle.text = resources.getString(R.string.architecture)
        viewArchitect.textProgress.text = "${Math.round(projectProgress.proarchitecture)}%"
        viewArchitect.progressBar.progress = Math.round(projectProgress.proarchitecture)
        viewArchitect.textDescription.text = "${projectProgress.proarchitecturetxt}"
        panelDetail.addView(viewArchitect)

    }

    override fun hideData() {
        panelDetail.hide()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(localizedMessage: String) {
        alert(localizedMessage, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }.show()
    }

    override fun showComingSoon() {
        textMessage.show()
    }
}

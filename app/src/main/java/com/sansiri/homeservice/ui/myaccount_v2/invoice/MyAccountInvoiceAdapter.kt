package com.sansiri.homeservice.ui.myaccount_v2.invoice

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class MyAccountInvoiceAdapter(val color: Int, val itemClick: (Invoice) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<Invoice>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return MyAccountInvoiceViewHolder(parent.inflate(R.layout.view_summary_payable_card), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is MyAccountInvoiceViewHolder)
            holder.bind(mData[position], color)
    }

    fun setData(data: List<Invoice>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }
     fun addData(data: List<Invoice>) {
        mData.addAll(data)
        notifyDataSetChanged()
    }


}
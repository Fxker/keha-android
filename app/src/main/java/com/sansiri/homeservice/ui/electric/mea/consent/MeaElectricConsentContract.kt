package com.sansiri.homeservice.ui.electric.mea.consent

import com.sansiri.homeservice.model.api.partner.mea.Consent
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MeaElectricConsentContract {
    interface Presenter : BasePresenter<View> {
        fun init(consent: Consent?)
        fun accept()
    }

    interface View : BaseView {
        fun bindData(consent: Consent)
        fun showLoading()
        fun hideLoading()
        fun launchNext()

    }
}
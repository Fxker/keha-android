package com.appy.android.sdk.control.scenario

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionScenario : APAction<ScenarioCommand>, Parcelable {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionScenario> {
        override fun createFromParcel(parcel: Parcel): APActionScenario {
            return APActionScenario(parcel)
        }

        override fun newArray(size: Int): Array<APActionScenario?> {
            return arrayOfNulls(size)
        }
    }
}
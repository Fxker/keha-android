package com.sansiri.homeservice.model.menu

/**
 * Created by sansiri on 12/12/17.
 */
object ActionType {
    val EXTERNAL_BROWSER = "EXTERNAL_BROWSER"
    val EXTERNAL_APP = "EXTERNAL_APP"
    val MORE_MENU = "MORE_MENU"
    val EXTERNAL_BROWSER_WITH_HEADER_TOKEN = "EXTERNAL_BROWSER_WITH_HEADER_TOKEN"
}
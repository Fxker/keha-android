package com.sansiri.homeservice.model.api

data class TextValue(
	val text: String? = null,
	val value: String? = null
)

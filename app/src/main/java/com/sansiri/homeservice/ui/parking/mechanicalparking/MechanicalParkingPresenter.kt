package com.sansiri.homeservice.ui.parking.mechanicalparking

import com.sansiri.homeservice.data.network.firebase.FirebaseDatabaseManager
import com.sansiri.homeservice.model.api.ParkingQueue
import com.sansiri.homeservice.ui.base.BasePresenterImpl

/**
 * Created by sansiri on 2/27/18.
 */
class MechanicalParkingPresenter : BasePresenterImpl<MechanicalParkingContract.View>(), MechanicalParkingContract.Presenter {
    private val mDb = FirebaseDatabaseManager()
    private var mQueue: List<ParkingQueue>? = null
    private var isReady = false
    private var isAlert = false
    override var mQueueId: String = ""

    override fun start() {
        mView?.hideCardStatus()
    }

    override fun destroy() {
        mDb.detachParkingQueue()
    }

    override fun attachParkingQueue(projectId: String) {
        mDb.attachParkingQueue(projectId) { queue ->
            mQueue = queue.filterNot { it.processStatus == "Done" || it.processStatus == "Cancel" }
            mView?.enableQRButton()
            if (mQueueId.isNotEmpty()) {
                processQRScanning(mQueueId)
            } else {
                invokeSummary()
            }
        }
    }

    override fun invokeSummary() {
        mQueue?.let { mView?.showSummary(it) }
    }


    override fun processQRScanning(queueId: String?) {
        if (queueId != null) {
            mQueueId = queueId
            mView?.save(mQueueId)

            val myQueue = mQueue?.find { it.queueId.toString() == queueId }
            if (myQueue != null) {
                if (!isReady && myQueue.processStatus == "Ready") {
                    isAlert = true
                    mView?.alert(0)
                }
                isReady = myQueue.processStatus == "Ready"

                mView?.showCardStatus(myQueue)

            } else {
                mView?.clearSave()
                mView?.hideCardStatus()
                return
            }

            mView?.showMyQueueETA(myQueue.queueNo ?: "", myQueue.estimateTime)
            if (!isAlert) {
                mView?.alert(myQueue.estimateTime)
            }
        } else {
        }
    }

    override fun reset() {
        isReady = false
        isAlert = false
        invokeSummary()
        mView?.hideCardStatus()
        mView?.clearSave()
    }
}
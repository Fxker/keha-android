package com.sansiri.homeservice.di

import com.google.firebase.auth.FirebaseAuth
//import com.homemate.sdk.HomeMateSDK
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.sansiri.homeservice.data.database.AppDatabase
import com.sansiri.homeservice.data.network.OkHttpBuilder
import com.sansiri.homeservice.data.network.RetrofitBuilder
import com.sansiri.homeservice.data.network.auth.*
import com.sansiri.homeservice.data.network.firebase.NewFirebaseAuthManager
import com.sansiri.homeservice.data.repository.appy.AppyRepository
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.data.repository.bos.BosRepository
import com.sansiri.homeservice.data.repository.bos.BosRepositoryImpl
import com.sansiri.homeservice.data.repository.gateway2c2p.Gateway2c2pRepository
import com.sansiri.homeservice.data.repository.gateway2c2p.Gateway2c2pRepositoryImpl
import com.sansiri.homeservice.data.repository.mea.MeaRepository
import com.sansiri.homeservice.data.repository.mea.MeaRepositoryImpl
import com.sansiri.homeservice.data.repository.omise.OmiseRepository
import com.sansiri.homeservice.data.repository.omise.OmiseRepositoryImpl
import com.sansiri.homeservice.data.repository.meter.SmartMeterRepository
import com.sansiri.homeservice.data.repository.meter.SmartMeterRepositoryImpl
import com.sansiri.homeservice.data.repository.notification.NotificationRepository
import com.sansiri.homeservice.data.repository.notification.NotificationRepositoryImpl
import com.sansiri.homeservice.data.repository.orvibo.OrviboRepository
//import com.sansiri.homeservice.data.repository.orvibo.OrviboRepositoryImpl
import com.sansiri.homeservice.data.repository.parking.SmartParkingRepository
import com.sansiri.homeservice.data.repository.parking.SmartParkingRepositoryImpl
import com.sansiri.homeservice.data.repository.payment.PaymentRepository
import com.sansiri.homeservice.data.repository.payment.PaymentRepositoryImpl
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepositoryImpl
import com.sansiri.homeservice.di.DatasourceProperties.MEA_API_KEY
import com.sansiri.homeservice.di.DatasourceProperties.MEA_CLIENT_CODE
import com.sansiri.homeservice.di.DatasourceProperties.OMISE_PKEY
import com.sansiri.homeservice.di.DatasourceProperties.SERVER_URL
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import vc.siriventures.bathome.BAtHomeSDK

object DatasourceProperties {
    const val SERVER_URL = "SERVER_URL"
    const val OMISE_PKEY = "OMISE_PKEY"
    const val MEA_CLIENT_CODE = "MEA_CLIENT_CODE"
    const val MEA_API_KEY = "MEA_API_KEY"
}

val baseModule = module {
    single(named("okHttpAuth")) { OkHttpBuilder(get()).buildAuth() }
    single(named("okHttpUnAuth")) { OkHttpBuilder(get()).buildUnAuth() }
    single<CallAdapter.Factory>(named("CoroutineFactory")) { CoroutineCallAdapterFactory() }
    single<CallAdapter.Factory>(named("RxJava2Factory")) { RxJava2CallAdapterFactory.create() }
    single<Converter.Factory> { GsonConverterFactory.create() }
    single(named("retrofitAuthCoroutine")) { RetrofitBuilder(get(named("okHttpAuth")), get(), get(named("CoroutineFactory"))) }
    single(named("retrofitAuthRxJava2")) { RetrofitBuilder(get(named("okHttpAuth")), get(), get(named("RxJava2Factory"))) }
    single(named("retrofitUnAuth")) { RetrofitBuilder(get(named("okHttpUnAuth")), get(), get()) }
    single { FirebaseAuth.getInstance() }
    single { NewFirebaseAuthManager(get()) }
}

val networkModule = module {
    single<ApiAuthService> { get<RetrofitBuilder>(named("retrofitAuthCoroutine")).build(getProperty(SERVER_URL)) }
    single<PaymentService> { get<RetrofitBuilder>(named("retrofitAuthRxJava2")).build(getProperty(SERVER_URL)) }
    single<TrendyWashService> { get<RetrofitBuilder>(named("retrofitAuthCoroutine")).build(getProperty(SERVER_URL)) }
    single<Gateway2c2pService> { get<RetrofitBuilder>(named("retrofitAuthCoroutine")).build(getProperty(SERVER_URL)) }
    single<ParkingService> { get<RetrofitBuilder>(named("retrofitAuthCoroutine")).build(getProperty(SERVER_URL)) }
    single<SmartMeterService> { get<RetrofitBuilder>(named("retrofitAuthCoroutine")).build(getProperty(SERVER_URL)) }
    single<MEAService> { get<RetrofitBuilder>(named("retrofitAuthRxJava2")).build(getProperty(SERVER_URL)) }
    single<BosService> { get<RetrofitBuilder>(named("retrofitAuthRxJava2")).build(getProperty(SERVER_URL)) }
}

val repositoryModule = module {
    single<TrendyWashRepository> { TrendyWashRepositoryImpl(get(), get(), getProperty(SERVER_URL)) }
    single<AppyRepository> { AppyRepositoryImpl() }
    single<NotificationRepository> { NotificationRepositoryImpl(get()) }
//    single { HomeMateSDK(context = get()) }
    single { AppDatabase.getDatabase(context = get()).orviboDao() }
    single { AppDatabase.getDatabase(context = get()).savingCreditCardDao() }
    single { AppDatabase.getDatabase(context = get()).notificationDao() }
    single<PaymentRepository> { PaymentRepositoryImpl(get()) }
//    single<OrviboRepository> { OrviboRepositoryImpl(orviboDao = get(), homeMateSDK = get()) }
    single<OmiseRepository> { OmiseRepositoryImpl(getProperty(OMISE_PKEY)) }
    single<Gateway2c2pRepository> { Gateway2c2pRepositoryImpl(get(), get(), get()) }
    single<SmartParkingRepository> { SmartParkingRepositoryImpl(get()) }
    single { BAtHomeSDK(get()) }
    single<SmartMeterRepository> { SmartMeterRepositoryImpl(get()) }
    single<MeaRepository> { MeaRepositoryImpl(getProperty(MEA_API_KEY), getProperty(MEA_CLIENT_CODE), get()) }
    single<BosRepository> { BosRepositoryImpl(get()) }
}





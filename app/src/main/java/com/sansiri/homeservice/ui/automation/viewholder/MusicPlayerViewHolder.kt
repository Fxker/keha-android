package com.sansiri.homeservice.ui.automation.viewholder

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import kotlinx.android.synthetic.main.view_home_automation_music_player.view.*
import kotlinx.android.synthetic.main.view_icon_and_title.view.*

/**
 * Created by sansiri on 10/9/17.
 */
class MusicPlayerViewHolder(val view: View, val overlayColor: Int, val itemClick: (HomeAutomation, Int) -> Unit) : LifecycleViewHolder(view) {
    companion object {
        val REQUEST_PLAY_MUSIC = 0
        val REQUEST_PAUSE_MUSIC = 1
        val REQUEST_NEXT_MUSIC = 2
        val REQUEST_PREV_MUSIC = 3
    }

    val rootLayout: View = view.layoutTitle
    var isPlaying = false

    fun bind(data: HomeAutomation) {
        with(view) {
            imageLogo.setImageResource(data.icon)
            imageLogo.setColorFilter(context.resources.getColor(overlayColor))
            textTitle.text = data.title
            buttonPlay.setOnClickListener {
                itemClick(data, if (isPlaying) REQUEST_PAUSE_MUSIC else REQUEST_PLAY_MUSIC)
                isPlaying = !isPlaying
                setPlayButton(isPlaying)
            }
            buttonNext.setOnClickListener { itemClick(data, REQUEST_NEXT_MUSIC) }
            buttonPrev.setOnClickListener { itemClick(data, REQUEST_PREV_MUSIC) }
            setPlayButton(isPlaying)
        }
    }

    fun setPlayButton(isPlaying: Boolean) {
        if (isPlaying) {
            view.buttonPlay.setImageResource(R.drawable.ic_pause_track)
        } else {
            view.buttonPlay.setImageResource(R.drawable.ic_play_track)
        }
    }
}
package com.sansiri.homeservice.ui.base

import android.content.Context
import com.sansiri.homeservice.model.menu.BosMenu

/**
 * Created by oakraw on 6/12/2017 AD.
 */
interface BaseView {
    fun sendScreenView(screenName: String)
    fun sendEvent(category: String, action: String, label: String?, logVersion: String = "2", vararg optionalProp: Pair<String, String>)
    fun showError(message: String)
}
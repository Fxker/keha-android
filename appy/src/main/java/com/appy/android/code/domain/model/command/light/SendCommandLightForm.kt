package com.appy.android.code.domain.model.command.light

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.light.LightCommand

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandLightForm(
        val homeId: String,
        val lightId: String,
        val command: LightCommand
) : AppyForm() {
    val mSendCommandLightJsonBody: SendCommandLightJsonBody = SendCommandLightJsonBody()

    init {
        this.mSendCommandLightJsonBody.command = command.action()
    }

}
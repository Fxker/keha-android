package com.appy.android.sdk.status.toggleswitch

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand
import com.appy.android.sdk.status.APStatus

class APStatusToggleSwitch : APStatus<String> {
    constructor() : super()
    constructor(control: APControl, value: String) : super(control, value)
}
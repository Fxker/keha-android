package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcel
import android.os.Parcelable

data class HomeUpgradeRoom(
        override val id: String,
        override val nameEn: String?,
        override val nameTh: String?,
        val hardwareList: List<HomeUpgradeHardware>? = null,
        val type: String? = null
) : HomeUpgrade, Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.createTypedArrayList(HomeUpgradeHardware.CREATOR),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(nameEn)
        writeString(nameTh)
        writeTypedList(hardwareList)
        writeString(type)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeUpgradeRoom> = object : Parcelable.Creator<HomeUpgradeRoom> {
            override fun createFromParcel(source: Parcel): HomeUpgradeRoom = HomeUpgradeRoom(source)
            override fun newArray(size: Int): Array<HomeUpgradeRoom?> = arrayOfNulls(size)
        }
    }
}
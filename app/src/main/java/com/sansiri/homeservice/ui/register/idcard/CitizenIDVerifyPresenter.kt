package com.sansiri.homeservice.ui.register.idcard

import android.util.Log
import com.google.gson.Gson
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.CitizenIDVerifyResult
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.ui.register.federatiallogin.LoginFragment
import com.sansiri.homeservice.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * Created by sansiri on 10/24/17.
 */
class CitizenIDVerifyPresenter : BasePresenterImpl<CitizenIDVerifyContract.View>(), CitizenIDVerifyContract.Presenter {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun start() {
    }

    override fun destroy() {
        compositeDisposable.clear()
    }

    override fun verifyCitizenID(citizenID: String) {
        if (citizenID.isEmpty()) {
            mView?.showPleaseEnterCitizenId()
            return
        }

        val repository = ApiRepositoryProvider.provideApiUnAuthRepository()

        mView?.showLoading()

        val observable = repository.verifyCitizenId(citizenID.removeSpace().removeDash())
                .observe()
                .subscribe({
                    if (!it.customerId.isNullOrBlank()) {
                        mView?.launchRegisterScreen(it.customerId)
                    } else {
                        mView?.showError("Cannot verified")
                    }
                    mView?.hideLoading()
                }, {
                    mView?.hideLoading()
                    when (it.getHttpCode()) {
                        409 -> {
                            if (it is HttpException) {
                                val body = it.response()?.errorBody()
                                if (body != null) {
                                    val result = Gson().fromJson<CitizenIDVerifyResult>(body.string(), CitizenIDVerifyResult::class.java)

                                    if (result.loginWithEmail) {
                                        mView?.launchLoginScreen(LoginFragment.EMAIL, result.registeredWithEmailOrName)
                                        return@subscribe
                                    }

                                    if (result.loginWithFacebook) {
                                        mView?.launchLoginScreen(LoginFragment.FACEBOOK, result.registeredWithEmailOrName)
                                        return@subscribe
                                    }

                                    if (result.loginWithGoogle) {
                                        mView?.launchLoginScreen(LoginFragment.GOOGLE, result.registeredWithEmailOrName)
                                        return@subscribe
                                    }
                                }
                                mView?.launchLoginScreen(null, null)
                            }
                        }
                        404 -> mView?.showHelpDialog()
                        else -> mView?.showError(it.showHttpError())
                    }
                })
        compositeDisposable.add(observable)
    }
}
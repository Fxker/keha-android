package com.sansiri.homeservice.ui.downpayment

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.myaccount.invoce.InvoiceItem
import com.sansiri.homeservice.ui.myaccount_v2.invoice.InvoiceItemViewHolder
import com.sansiri.homeservice.util.inflate

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class InvoiceItemAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<InvoiceItemViewHolder>() {
    private val mData = mutableListOf<InvoiceItem>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvoiceItemViewHolder {
        return InvoiceItemViewHolder(parent.inflate(R.layout.view_invoice_item))
    }

    override fun onBindViewHolder(holder: InvoiceItemViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    fun setData(data: List<InvoiceItem>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }
}
package com.sansiri.homeservice.ui.myaccount_v2

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 10/31/17.
 */
class MyAccountPresenter : BasePresenterImpl<MyAccountContract.View>(), MyAccountContract.Presenter {

    override fun start() {
    }

    override fun destroy() {

    }
}
package com.appy.android.automation.network

import com.appy.android.automation.helper.Validator

import java.io.IOException
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket

class UDPSocketHandler(address: String, port: Int) : SocketHandler() {

    internal var socket: MulticastSocket? = null

    var onConnectedListener: OnConnectedListener? = null
    var onSendDataListener: OnSendDataListener? = null
    var onTerminatedListener: OnTerminatedListener? = null

    init {
        this.address = address
        this.port = port
    }

    override fun run() {
        try {
            socket = MulticastSocket(port)
            socket!!.reuseAddress = true
            socket!!.connect(InetAddress.getByName(address), port)

            if (onConnectedListener != null) {
                onConnectedListener!!.onConnected(this)
            }
        } catch (e: Exception) {
            e.printStackTrace()

            terminate(SocketHandler.Companion.CONNECTION_ERROR)

            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }
        }

    }

    fun send(message: String) {
        if (!Validator.isValid(message)) {
            return
        }

        try {
            socket!!.broadcast = false

            val bytes = message.toByteArray()

            val packet = DatagramPacket(bytes, bytes.size)
            socket!!.send(packet)

            scheduleTimeOut()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun broadcast(message: String) {
        if (!Validator.isValid(message)) {
            return
        }

        try {
            socket!!.broadcast = true

            val bytes = message.toByteArray()

            val packet = DatagramPacket(bytes, bytes.size)
            socket!!.send(packet)

            scheduleTimeOut()
        } catch (e: IOException) {
            setErrorCode(e, SocketHandler.Companion.FAILED_TO_WRITE)
        }

    }

    override fun terminate() {
        super.terminate()

        try {
            if (socket != null) {
                socket!!.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (onTerminatedListener != null) {
                onTerminatedListener!!.onTerminated(this)
            }

            onConnectedListener = null
            onTerminatedListener = null
        }
    }

    interface OnConnectedListener {
        fun onConnected(socketHandler: UDPSocketHandler)
    }

    interface OnSendDataListener {
        fun onReceivedResponse(socketHandler: UDPSocketHandler)
    }

    interface OnTerminatedListener {
        fun onTerminated(socketHandler: UDPSocketHandler)
    }
}
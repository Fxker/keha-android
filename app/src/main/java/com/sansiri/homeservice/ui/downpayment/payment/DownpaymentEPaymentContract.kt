package com.sansiri.homeservice.ui.downpayment.payment

import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface DownpaymentEPaymentContract {
    interface Presenter : BasePresenter<View> {
        fun init(downPaymentItem: DownpaymentItem)
    }

    interface View : BaseView {
        fun showPaymentOptions(downPaymentItem: DownpaymentItem)
    }
}
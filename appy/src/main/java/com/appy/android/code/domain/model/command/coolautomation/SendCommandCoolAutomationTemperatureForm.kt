package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.coolautomation.CoolAutomationCommand
import com.appy.android.sdk.control.coolautomation.TemperatureCommand

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
internal class SendCommandCoolAutomationTemperatureForm(
        val homeId: String,
        val coolId: String,
        val commandParam: TemperatureCommand
) : AppyForm() {
    val mSendCommandCoolAutomationTemperatureJsonBody = SendCommandCoolAutomationTemperatureJsonBody()

    init {
        this.mSendCommandCoolAutomationTemperatureJsonBody.command = CoolAutomationCommand.TEMPERATURE.action()
        this.mSendCommandCoolAutomationTemperatureJsonBody.commandParam = commandParam.action()
    }
}
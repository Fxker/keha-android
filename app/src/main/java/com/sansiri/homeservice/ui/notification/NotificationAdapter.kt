package com.sansiri.homeservice.ui.notification

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.util.*
import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_notification.view.*

class NotificationAdapter(val itemClick: (PushNotification) -> Unit) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    private var mData = mutableListOf<PushNotification>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.view_notification)
        return ViewHolder(view, itemClick)
    }


    fun setData(notifications: List<PushNotification>) {
        val diffCallback = NotificationDiffCallback(mData, notifications)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mData.clear()
        mData.addAll(notifications)
        diffResult.dispatchUpdatesTo(this)
    }

    class ViewHolder(val view: View, val itemClick: (PushNotification) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(notification: PushNotification) {
            with(view) {

                if (notification.data?.profileImage != null) {
                    imageCircular.show()
                    image.hide()
                    Glide.with(this).loadCache(notification.data.profileImage, imageCircular)
                }
//                else if (notification.type != null) {
//                    imageCircular.hide()
//                    image.show()
//                    image.setImageResource(getLocalIcon(notification.type))
//                    image.tintImage(R.color.textDark)
//                }
                else {
                    imageCircular.hide()
                    image.hide()
                }

                textTitle.text = notification.title
                textContent.text = notification.message


                notification.createdAt?.let {
                    textDate.show()
                    textDate.text = TimeShow.show(notification.createdAt)
                }

                layoutClickable.setOnClickListener { itemClick(notification) }

                if (notification.isRead) {
                    textTitle.setTextColor(resources.getColor(R.color.textSubTitle2))
                    textContent.setTextColor(resources.getColor(R.color.textSubTitle2))
                    textDate.setTextColor(resources.getColor(R.color.textSubTitle2))
//                    layoutClickable.setBackgroundColor(resources.getColor(R.color.background2))
                } else {
                    textTitle.setTextColor(resources.getColor(R.color.textAccent))
                    textContent.setTextColor(resources.getColor(R.color.textSubTitle))
                    textDate.setTextColor(resources.getColor(R.color.textSubTitle2))
//                    layoutClickable.setBackgroundColor(resources.getColor(R.color.background))
                }
            }
        }

        fun getLocalIcon(type: String) = when (type) {
            Feature.ANNOUNCEMENT -> R.drawable.ic_news_paper
            Feature.MAIL_BOX -> R.drawable.ic_mail_box
            Feature.CHAT -> R.drawable.ic_contact_us
            else -> R.drawable.ic_mail
        }
    }

}
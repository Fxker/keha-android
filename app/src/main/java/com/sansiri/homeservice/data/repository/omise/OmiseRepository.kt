package com.sansiri.homeservice.data.repository.omise

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource

interface OmiseRepository {
    fun verifyCard(cardNumber: String, cardName: String, expirationMonth: Int, expirationYear: Int, securityCode: String, onSuccess: (String) -> Unit, onError: (Throwable) -> Unit)
}
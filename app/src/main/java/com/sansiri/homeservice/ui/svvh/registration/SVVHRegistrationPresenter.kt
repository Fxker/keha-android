package com.sansiri.homeservice.ui.svvh.registration

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.partner.svvh.SVVHRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError
import org.joda.time.DateTime
import java.util.*

class SVVHRegistrationPresenter : BasePresenterImpl<SVVHRegistrationContract.View>(), SVVHRegistrationContract.Presenter {
    private var mUnitId: String? = null
    private var mCustomer: SVVHRegisterCustomer? = null
    private var mFirstName: String? = null
    private var mLastName: String? = null
    private var mIDCard: String? = null
    private var mTel: String? = null
    private var mBirthDate: String? = null
    private var mGender: String? = null

    override fun init(home: Hut, customer: SVVHRegisterCustomer?) {
        mUnitId = home.unitId
        mCustomer = customer
    }

    override fun start() {
        if (mCustomer != null) {
            mView?.bindData(mCustomer!!)
        }
    }


    override fun setFirstName(firstName: String) {
        this.mFirstName = firstName
    }

    override fun setLastName(lastName: String) {
        this.mLastName = lastName
    }

    override fun setIDCard(idCard: String) {
        this.mIDCard = idCard
    }

    override fun setTel(tel: String) {
        this.mTel = tel
    }

    override fun setBirthDate(calendar: Calendar) {
        this.mBirthDate = DateTime(calendar.time).toString()
    }

    override fun setGender(gender: String) {
        this.mGender = gender
    }

    override fun submit() {
        if (!validate()) return

        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                api.submitSVVH(
                        mUnitId ?: "",
                        SVVHRegisterCustomer(
                                mFirstName,
                                mLastName,
                                mCustomer?.citizenOrPassportId,
                                mBirthDate,
                                mTel,
                                mGender
                        )).observe().subscribe({ response ->
                    mView?.hideLoading()
                    mView?.redirectUrl(response.redirectUrl ?: "")
                }) {
                    mView?.showError(it.showHttpError())
                    mView?.hideLoading()
                }
            }
        }
    }

    private fun validate(): Boolean {
        var isValidate = true

        if (mFirstName.isNullOrEmpty()) {
            mView?.showFirstNameError()
            isValidate = false
        }

        if (mLastName.isNullOrEmpty()) {
            mView?.showLastNameError()
            isValidate = false
        }

        if (mTel.isNullOrEmpty()) {
            mView?.showTelError()
            isValidate = false
        }

        if (mBirthDate.isNullOrEmpty()) {
            mView?.showBirthDateError()
            isValidate = false
        }

        if (mGender.isNullOrEmpty()) {
            mView?.showGenderError()
            isValidate = false
        }

        return isValidate
    }

    override fun destroy() {
    }
}
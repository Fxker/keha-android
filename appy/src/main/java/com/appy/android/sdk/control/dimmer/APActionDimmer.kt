package com.appy.android.sdk.control.dimmer

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.control.APAction

/**
 * Created by ssa-dev-04 on 12/3/2561.
 */
class APActionDimmer : APAction<DimmerCommand>, Parcelable {
    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APActionDimmer> {
        override fun createFromParcel(parcel: Parcel): APActionDimmer {
            return APActionDimmer(parcel)
        }

        override fun newArray(size: Int): Array<APActionDimmer?> {
            return arrayOfNulls(size)
        }
    }
}
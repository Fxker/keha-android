package com.appy.android.code.domain.model.room

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.room.RoomsApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
internal class GetRoomsInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<RoomsApiResult, RoomsForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: RoomsForm): Observable<RoomsApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getRooms(params.headers, params.homeId, params.params)
    }
}
package com.appy.android.sdk.control.outlet

import com.appy.android.sdk.control.APCommand

/**
 * Created by ssa-dev-4 on 5/10/2560.
 */
enum class OutletCommand : APCommand {
    OFF {
        override fun action(): String {
            return "CONTROL_OFF"
        }
    },
    ON {
        override fun action(): String {
            return "CONTROL_ON"
        }
    };
}
package com.appy.android.code.base.models

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
abstract class BaseParameterJsonModel {
    @SerializedName("value")
    var value: String? = null
    @SerializedName("description")
    var description: String? = null
}
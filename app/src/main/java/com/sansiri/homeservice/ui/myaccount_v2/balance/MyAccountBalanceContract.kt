package com.sansiri.homeservice.ui.myaccount_v2.balance

import com.sansiri.homeservice.model.api.myaccount.BalanceV2
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MyAccountBalanceContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun fetch(page: Int)
    }

    interface View : BaseView {
        fun bindBalance(balance: BalanceV2)
        fun bindHistory(paymentHistory: List<PaymentHistory>)
        fun showBalanceError()
    }
}
package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class HomeUpgradeHardware(
		override val id: String,
		override val nameTh: String?,
		override val nameEn: String?,
		val imageUrl: String? = null,
		val price: Double? = null,
		val description: String? = null,
		val attributes: Attribute? = null,
		var roomId: String? = null,
		var amount: Int = 0
) : HomeUpgrade, Parcelable {
	data class Attribute(
			val multiple: Boolean = false,
			val fundamental: Boolean = false
	) : Parcelable {
		constructor(source: Parcel) : this(
				source.readValue(Boolean::class.java.classLoader) as Boolean,
				source.readValue(Boolean::class.java.classLoader) as Boolean
		)

		override fun describeContents() = 0

		override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
			writeValue(multiple)
			writeValue(fundamental)
		}

		companion object {
			@JvmField
			val CREATOR: Parcelable.Creator<Attribute> = object : Parcelable.Creator<Attribute> {
				override fun createFromParcel(source: Parcel): Attribute = Attribute(source)
				override fun newArray(size: Int): Array<Attribute?> = arrayOfNulls(size)
			}
		}
	}

	constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readValue(Double::class.java.classLoader) as Double?,
			source.readString(),
			source.readParcelable<Attribute>(Attribute::class.java.classLoader),
			source.readString(),
			source.readInt()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(id)
		writeString(nameTh)
		writeString(nameEn)
		writeString(imageUrl)
		writeValue(price)
		writeString(description)
		writeParcelable(attributes, 0)
		writeString(roomId)
		writeInt(amount)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<HomeUpgradeHardware> = object : Parcelable.Creator<HomeUpgradeHardware> {
			override fun createFromParcel(source: Parcel): HomeUpgradeHardware = HomeUpgradeHardware(source)
			override fun newArray(size: Int): Array<HomeUpgradeHardware?> = arrayOfNulls(size)
		}
	}
}
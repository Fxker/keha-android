package com.sansiri.homeservice.data.network

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.room.APRoom
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.homecare.HomeCareRequest
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.model.api.ProjectInfo
import com.sansiri.homeservice.model.menu.HomeCareMenu
import com.sansiri.homeservice.model.menu.Section

/**
 * Created by sansiri on 12/14/17.
 */
object RuntimeCache {
    var projects = mutableListOf<ProjectInfo>()
    var homeCareRequests = mutableListOf<HomeCareRequest>()
    var homeCareType = mutableListOf<HomeCareMenu>()
    var dynamicButton: Map<String, List<Section>>? = null
    var isMeaConsentAccepted = false

    var selectedHome: Home? = null

    val cacheRoom = hashMapOf<String, List<APRoom>>()
    val cacheDevice = hashMapOf<String, List<APControl>>()

    fun clear() {
        projects.clear()
        homeCareRequests.clear()
        homeCareType.clear()
        me = null
    }

    var me: Profile? = null
}
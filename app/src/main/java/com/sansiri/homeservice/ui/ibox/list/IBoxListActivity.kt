package com.sansiri.homeservice.ui.ibox.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.IBoxItem
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.ibox.register.IBoxRegisterActivity
import com.sansiri.homeservice.ui.mailbox.detail.MailBoxDetailActivity
import com.sansiri.homeservice.ui.mailbox.history.MailBoxHistoryActivity
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*

class IBoxListActivity : BaseActivity<IBoxListContract.View, IBoxListContract.Presenter>(), IBoxListContract.View {
    override var mPresenter: IBoxListContract.Presenter = IBoxListPresenter()

    val mAdapter = SectionedRecyclerViewAdapter()
    var mHome: Hut? = null
    var mTitle: String? = null
    var mPhoneNo: String? = null
    private var menuSettings: MenuItem? = null

    companion object {
        val HOME = "HOME"
        val TITLE = "TITLE"
        val PHONE_NO = "PHONE_NO"
        fun start(activity: Activity, home: Hut, title: String?, phoneNumber: String? = null) {
            val intent = Intent(activity, IBoxListActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            intent.putExtra(PHONE_NO, phoneNumber)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        mHome = intent?.getParcelableExtra<Hut>(HOME)
        mTitle = intent?.getStringExtra(TITLE)
        mPhoneNo = intent?.getStringExtra(PHONE_NO)

        if (mPhoneNo == null) menuSettings?.isVisible = false

        setBackToolbar(mTitle ?: "iBOX", false)

        textNoData.text = getString(R.string.no_mail)

        with(list) {
            adapter = mAdapter
        }

        mPresenter.fetchData(mHome?.unitId ?: "")
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("IBOX")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuSettings = menu?.findItem(R.id.action_settings)
        if (mPhoneNo == null) menuSettings?.isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_settings -> IBoxRegisterActivity.start(this, mHome!!, mTitle, mPhoneNo)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun bindData(list: List<IBoxItem>) {
        val mapSection = hashMapOf<Date, MutableList<IBoxItem>>()

        list.forEach {
            val reportDate = it._createdAt?.resetTime()
            if (reportDate != null) {
                if (mapSection[reportDate] == null) {
                    mapSection.put(reportDate, mutableListOf(it))
                } else {
                    mapSection[reportDate]?.add(it)
                }
            }
        }

        mapSection.toSortedMap(compareByDescending<Date> { it }).forEach {
            val adapter = IBoxSectionAdapter(it.key.report(), it.value.sortedByDescending { it._createdAt })
            mAdapter.addSection(adapter)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        textNoData.show()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

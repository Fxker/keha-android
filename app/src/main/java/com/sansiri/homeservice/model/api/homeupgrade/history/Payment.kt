package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable

data class Payment(
		val status: String? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(status)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Payment> = object : Parcelable.Creator<Payment> {
			override fun createFromParcel(source: Parcel): Payment = Payment(source)
			override fun newArray(size: Int): Array<Payment?> = arrayOfNulls(size)
		}
	}
}

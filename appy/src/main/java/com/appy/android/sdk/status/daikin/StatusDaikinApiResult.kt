package com.appy.android.sdk.status.daikin

import com.appy.android.code.base.data.rest.result.AppyApiResult

class StatusDaikinApiResult : AppyApiResult<APStatusDaikin>()
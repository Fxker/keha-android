package com.sansiri.homeservice.ui.base

import io.reactivex.disposables.CompositeDisposable
import android.icu.lang.UCharacter.GraphemeClusterBreak.V



/**
 * Created by oakraw on 6/12/2017 AD.
 */
abstract class BasePresenterImpl<V : BaseView>() : BasePresenter<V> {
    protected val mCompositeDisposable = CompositeDisposable()

    protected var mView: V? = null

    override fun attachView(view: V) {
        mView = view
    }

    override fun detachView() {
        mCompositeDisposable.dispose()
        mView = null
    }
}
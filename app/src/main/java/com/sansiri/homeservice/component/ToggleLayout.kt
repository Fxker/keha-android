package com.sansiri.homeservice.component

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.Checkable
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.sansiri.homeservice.R

/**
 * Created by sansiri on 10/10/17.
 */
class ToggleLayout : FrameLayout, Checkable {
    private var mChecked: Boolean = false
    private var mOnCheckedChangeListener: OnCheckedChangeListener? = null
    var mColorOn = Color.BLUE
    var mColorOff = Color.GRAY

    val mChildren = mutableListOf<View>()
    val mGroup = mutableListOf<View>()

    constructor(context: Context) : super(context) {
    }


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)


    private fun initialize(context: Context, attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.ToggleLayout, 0, 0)
        mColorOn = a.getColor(R.styleable.ToggleLayout_onColor, Color.BLUE)
        mColorOff = a.getColor(R.styleable.ToggleLayout_offColor, Color.GRAY)
        mChecked = a.getBoolean(R.styleable.ToggleLayout_isChecked, false)

        setOnTouchListener { v, event ->
             when(event.action) {
                MotionEvent.ACTION_UP ->
                    toggle()
            }

            false
        }

        a.recycle()
    }

    override fun setChecked(checked: Boolean) {
        if (mChecked != checked) {
            mChecked = checked
            refreshView(mChecked)
            mOnCheckedChangeListener?.onCheckedChanged(checked)
        }
    }

    fun addRelatedChild(vararg views: View) {
        mChildren.addAll(views)
        refreshView(mChecked)
    }

    fun groupToggleButton(vararg views: View) {
        mGroup.addAll(views)
        mGroup.forEach {  }
    }

    private fun refreshView(checked: Boolean) {
        if (checked) {
            mChildren.forEach {
                renderOn(it)
            }
        } else {
            mChildren.forEach {
                renderOff(it)
            }
        }
    }

    private fun renderOn(view: View) {
        if (view is ImageView) {
            view.setColorFilter(mColorOn)
        } else if (view is TextView) {
            view.setTextColor(mColorOn)
        }
    }

    private fun renderOff(view: View) {
        if (view is ImageView) {
            view.setColorFilter(mColorOff)
        } else if (view is TextView) {
            view.setTextColor(mColorOff)
        }
    }

    override fun isChecked(): Boolean = mChecked

    override fun toggle() {
        isChecked = !mChecked
    }


    fun setOnCheckedChangeListener(listener: OnCheckedChangeListener) {
        mOnCheckedChangeListener = listener
    }

    interface OnCheckedChangeListener {
        fun onCheckedChanged(isChecked: Boolean)
    }

}
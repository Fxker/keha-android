package com.sansiri.homeservice.model.api.meter.electric

import android.os.Parcelable
import com.sansiri.homeservice.util.toCurrencyFormat
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class SmartElectricMeterDetail(
	val kiloWattHour: Double,
	val earthLeakUsage: Double,
	val voltUsage: Double,
	val kiloVoltAmpsReactiveHours: Double,
	val kiloWattHourUsage: Double,
	val activePowerUsage: Double,
	val currentUsage: Double,
	val earthLeak: Double,
	val costEstimateUsage: Double,
	val current: Double,
	val meterId: String? = null,
	val volt: Double,
	val kiloVoltAmpsReactiveHoursUsage: Double,
	val powerFactorUsage: Double,
	val activePower: Double,
	val activePowerDisplay: String,
	val powerFactor: Double,
	val timestamp: String? = null
): Parcelable{
	val _timestamp: Date?
		get() {
			return try {
				if (this.timestamp == null) return null

				DateTime(this.timestamp).toDate()
			} catch (e: ParseException) {
				null
			}
		}

	val displayActivePower: String
		get() {
			return "${activePower.toCurrencyFormat()} W"
		}
}

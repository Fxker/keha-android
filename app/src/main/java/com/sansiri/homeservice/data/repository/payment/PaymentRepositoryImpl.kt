package com.sansiri.homeservice.data.repository.payment

import com.sansiri.homeservice.data.network.auth.PaymentService
import com.sansiri.homeservice.model.api.payment.PaymentRequest

class PaymentRepositoryImpl(val service: PaymentService) : PaymentRepository {
    override fun requestPayment(request: PaymentRequest) = service.requestPayment(request)
}
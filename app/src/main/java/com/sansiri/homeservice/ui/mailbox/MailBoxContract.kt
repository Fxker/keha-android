package com.sansiri.homeservice.ui.mailbox

import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/12/17.
 */
interface MailBoxContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String)
    }

    interface View : BaseView {
        fun bindData(mutableListOf: List<MailBox>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
    }
}
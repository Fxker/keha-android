package com.sansiri.homeservice.ui.ibox.list

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.IBoxItem
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

class IBoxSectionAdapter(val title: String, val mData: List<IBoxItem>, val itemClick: ((IBoxItem) -> Unit)? = null) : StatelessSection(SectionParameters.Builder(R.layout.view_summary_card)
        .headerResourceId(R.layout.view_mailbox_date)
        .build()) {

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return IBoxViewHolder(view, itemClick)
    }

    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is IBoxViewHolder) {
            holder.bind(mData[position])
        }
    }
}
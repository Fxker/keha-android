package com.sansiri.homeservice.ui.home

import android.location.Location
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.room.APRoom
//import com.homemate.sdk.model.HMRoom
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.data.repository.appy.AppyRepository
import com.sansiri.homeservice.data.repository.notification.NotificationRepository
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.api.UnitInfo
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgrade
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import com.sansiri.homeservice.model.menu.*
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.AsyncSubject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by sansiri on 11/27/17.
 */
class HomePresenter(val appyRepository: AppyRepository, val notificationRepository: NotificationRepository/*, val orviboRepository: OrviboRepository*/) : BasePresenterImpl<HomeContract.View>(), HomeContract.Presenter {
    private var homeList: MutableList<Home>? = null
    private val selectedHomeObservable: AsyncSubject<List<Home>> = AsyncSubject.create()
    private var cacheSectionPosition: Int = -1
    private var cacheDynamicMenu: DynamicMenu? = null
    private var disableFindNearestHome = false

    private val mHomeUpgradeCompositeDisposable = CompositeDisposable()
    private var mAqiCompositeDisposable = CompositeDisposable()

    override var currentPosition: Int = -1
    override var isAnnouncementShow = false

    override var notifications = notificationRepository.getNotifications()

    companion object {
        private var userLocation: Location? = null
    }


    override fun start() {
        mView?.hideHome()
        mView?.hideSummary()
        mView?.hideAnnouncement()
        mView?.hideHomeAutomation()
        mView?.hideSection()
        userLocation?.let {
            sortHome(it)
        }

        fetchHome()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetchHome() {
        ApiRepositoryProvider.provideApiAuthRepository() { api ->
            if (api != null) {
                mView?.showLoading()

                if (mView != null) {
                    mCompositeDisposable.add(api.getHome().observe().subscribe({ homes ->
                        homeList = homes
                        if (homes.isEmpty()) {
                            mView?.showHomeMissing()
                        } else {
                            mView?.bindHome(homes)
                        }
                        mView?.hideLoading()

                        val projectTitle = homes.getOrNull(0)?.title
                        if (!projectTitle.isNullOrEmpty()) {
                            selectedHomeObservable.onNext(homes)
                            selectedHomeObservable.onComplete()
                        }
                    }, {
                        mView?.showError(it.showHttpError())
                        mView?.hideLoading()
                    }) {
                       mView?.attachObserver()
                    })

                    mCompositeDisposable.add(api.getMe().observe().subscribe({
                        mView?.saveProfileId(it.objectId)
                    }, { e ->
                        when (e.getHttpCode()) {
                            404 -> {
                                mView?.showSessionTimeoutDialog()
                            }
                        }
                    }))
                }
            } else {
                mView?.showError("Network alertError")
            }
        }
    }


    override fun selectHome(position: Int) {
        val home = homeList?.getOrNull(position)
        if (home != null) {
            RuntimeCache.selectedHome = home
            with(home) {


                if (summary != null) {
                    mView?.bindSummary(summary!!)
                } else {
                    mView?.hideSummary()
                }

                if (section != null && cacheSectionPosition != position) {
                    mView?.bindSection(section!!)
                    cacheSectionPosition = position
                }

                if (currentPosition != position) {
                    mView?.hideAnnouncement()
                    mView?.hideHomeUpgrade()
                    mView?.hideBAtHome()
                    mHomeUpgradeCompositeDisposable.clear()
                    mView?.hideHomeAutomation()
                    isAnnouncementShow = false

                    if (home.homeAutomationGateway != null) {
                        when (home.homeAutomationGateway!!.provider) {
                            UnitInfo.HomeAutomationGateway.Provider.APPY -> {
                                fetchHomeControlRooms(home.projectId, home.homeAutomationGateway?.appyId
                                        ?: "", home.homeAutomationGateway?.pin, { rooms ->
                                    val roomMenu = mutableListOf<Menu>()
                                    rooms.forEach { room ->
                                        fetchHomeControlDevices(home.homeAutomationGateway?.appyId!!, room.id
                                                ?: "", {}) {}
                                        roomMenu.add(room.convert())
                                    }
                                    if (currentPosition == position) {
                                        mView?.bindHomeAutomation(roomMenu)
                                    }
                                }) {
                                    mView?.hideHomeAutomation()
                                }
                            }
                            UnitInfo.HomeAutomationGateway.Provider.BATHOME -> {
                                if (home.homeAutomationGateway!!.username != null && home.homeAutomationGateway!!.password != null && home.homeAutomationGateway!!.username != null)
                                    mView?.showBAtHome(home.homeAutomationGateway!!.username!!, home.homeAutomationGateway!!.password!!, home.homeAutomationGateway!!.code
                                            ?: "")
                            }
//                            UnitInfo.HomeAutomationGateway.Provider.ORVIBO -> {
//                                mView?.subscribeOrviboHome(orviboRepository.getOrviboHome(home.unitId))
//                                if (section != null) {
//                                    val menu = section!!.find { s -> s.sectionName == "MENU" }?.items
//                                    if (menu?.find { it.title == "Orvibo" } == null) {
//                                        menu?.add(
//                                                DynamicMenu(Feature.PARTNER_ORBIVO, "Orvibo", null)
//                                        )
//                                    }
//                                    mView?.bindSection(section!!)
//                                }
//                            }
                        }


                    } else {
//                        mView?.subscribeOrviboHome(orviboRepository.getOrviboHome(home.unitId))
                    }

                    if (!BuildConfig.BUILD_TYPE.contains("release")) {
                        if (home.isSupportHomeUpgrade) {
                            fetchHomeUpgrade(home)
                        }
                    }

                    fetchAqi(home)
                }

                if (announcement != null && announcement?.isNotEmpty()!! && home.isTransfer) {
                    isAnnouncementShow = true
                    mView?.bindAnnouncement(announcement!!)
                }
            }
        } else {

        }

        currentPosition = position
    }

    override fun sortHome(location: Location) {
        userLocation = location

        if (homeList != null) {
            findNearestHome(location)
        } else {
            mView?.let {
                selectedHomeObservable.observe().subscribe(it, {
                    findNearestHome(location)
                })
            }
        }
    }

    private fun findNearestHome(location: Location) {
        if (!disableFindNearestHome) {
            doAsync {
                var minDistanceHomeIndex = 0
                var minDistance = Integer.MAX_VALUE
                homeList?.forEachIndexed { index, home ->
                    val homeLocation = Location(location.provider).apply {
                        latitude = home.lat
                        longitude = home.lng
                    }

                    val distance = homeLocation.distanceTo(location)
                    if (distance < minDistance) {
                        minDistance = distance.toInt()
                        minDistanceHomeIndex = index
                    }
                }
                uiThread {
                    if (minDistanceHomeIndex != currentPosition) {
                        mView?.scrollToHome(minDistanceHomeIndex)
                    }
                }
            }
        }
    }

    private fun fetchHomeControlRooms(projectId: String, unitId: String, pin: String?, onSuccess: (List<APRoom>) -> Unit, onFail: (Throwable) -> Unit) {
        appyRepository.setProjectId(projectId, pin)
        appyRepository.getRooms(unitId).observe().subscribe(mView!!, {
            onSuccess(it)
        }) {
            onFail(it)
        }
    }

    private fun fetchHomeControlDevices(unitId: String, roomId: String, onSuccess: (List<APControl>) -> Unit, onFail: (Throwable) -> Unit) {
        appyRepository?.getControl(unitId, roomId)?.observe()?.subscribe(mView!!, {
            onSuccess(it)
        }) {
            onFail(it)
        }
    }

    private fun fetchHomeUpgrade(home: Home) {
        mView?.showLoadingHomeUpgrade()
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mHomeUpgradeCompositeDisposable.add(api.getHomeUpgradeHistory(home.unitId).observe().subscribe(mView!!, { history ->
                    when (history.installation?.status) {
                        HomeUpgrade.AWAITING_APPOINTMENT -> {
                            mView?.showHomeUpgradeWaiting(history)
                        }
                        HomeUpgrade.AWAITING_INSTALLATION -> {
                            mView?.showHomeUpgradeAppointment(history)
                        }
                        else -> {
                            mView?.showHomeUpgrade()
                        }
                    }
                }) {
                    mView?.hideHomeUpgrade()
                })
            }
        }
    }

    override fun requestHomeAutomationScene(sceneMenu: Menu) {
        if (RuntimeCache.selectedHome != null) {
            when (sceneMenu) {
//                is OrviboHomeAutomationRoomMenu -> {
//                    orviboRepository.getHubInfo({ hubs ->
//                        mView?.hideLoading()
//
//                        if (!hubs.isNullOrEmpty()) {
//                            val hub = hubs.first()
//                            hub.id?.let { orviboRepository.activateScene(it, sceneMenu.id) {} }
//                        }
//                    }) {
//
//                    }
//                }
            }

        }
    }

    override fun requestHomeAutomation(roomMenu: Menu) {
        if (RuntimeCache.selectedHome != null) {
            when (roomMenu) {
//                is OrviboHomeAutomationRoomMenu -> {
//                    mView?.launchOrviboHomeAutomation(roomMenu.title, roomMenu.id)
//
//                }
                is AppyHomeAutomationRoomMenu -> {
                    mView?.launchAppyHomeAutomation(roomMenu.title
                            ?: "", RuntimeCache.selectedHome?.projectId
                            ?: "", RuntimeCache.selectedHome?.homeAutomationGateway?.appyId
                            ?: "", roomMenu.id
                            ?: "")
                }
            }

        }
    }


    override fun requestHomeUpgrade() {
        RuntimeCache.selectedHome.let { mView?.launchHomeUpgrade(it!!.unitId) }
    }

    override fun requestAqi() {
        if (RuntimeCache.selectedHome != null) {
            mView?.launchAqi(RuntimeCache.selectedHome!!)
        }
    }

    override fun selectMenu(menu: Menu) {
        RuntimeCache.selectedHome.let {
            when (menu.id) {
                Feature.MAIL_BOX -> mView?.launchMailbox(it!!, menu.title)
                Feature.HOME_CARE -> mView?.launchHomeCare(it!!.unitId, menu.title)
                Feature.FACILITY_BOOKING -> mView?.launchFacilityBooking(it!!, menu.title)
                Feature.DOWNPAYMENT -> mView?.launchDownPayment(it!!, menu.title)
                Feature.CHAT -> mView?.launchChat(it!!, menu)
                Feature.PHONE_DIRECTORY -> mView?.launchPhoneDirectory(it!!.projectId, menu.title)
                Feature.HOME_WALLET -> mView?.launchMyAccount(it!!, menu.title)
                Feature.INSPECTION -> mView?.launchInspection(it!!.unitId, menu.title)
                Feature.APPOINTMENT -> mView?.launchAppointment()
                Feature.TRANSFER -> mView?.launchTransfer(it!!.unitId, menu.title)
                Feature.CONTACT_US -> mView?.launchContactUs(it!!.unitId, menu.title)
                Feature.PROJECT_PROGRESS -> mView?.launchProjectProgress(it!!.projectId, it.title, menu.title)
                Feature.MECHANICAL_PARKING -> mView?.launchMechanicalParking(it!!.projectId, menu.title)
                Feature.SALE_RENTOUT -> mView?.launchSellAndRentOut()
                Feature.SALE -> mView?.launchSell()
                Feature.SUGGESTION -> mView?.launchSuggestion(it!!.unitId, menu.title)
                Feature.VISITOR_QR_TEOHONG -> mView?.launchVisitorAccess(it!!, menu.title)
                Feature.IBOX -> mView?.launchiBOX(it!!, menu)
                Feature.MY_HOME -> mView?.launchMyHome(it!!, menu.title)
                Feature.MAINTENANCE_GUIDE -> mView?.launchMaintenanceGuide(it!!, menu.title, null, null)
                Feature.PARTNER_SAMITIVEJ_HOSPITAL -> mView?.launchSVVH(it!!, menu)
                Feature.PARTNER_ORBIVO -> mView?.launchOrvibo(it!!)
                Feature.PARTNER_TRENDY_WASH -> mView?.launchTrendyWash(it!!, menu)
                Feature.PARTNER_TEOHONG_MECHANICAL_PARKING -> mView?.launchSmartParking(it!!, menu)
                Feature.PARTNER_SANSIRI_SMART_WATER_METER -> mView?.launchSmartWaterMeter(it!!, menu)
                Feature.PARTNER_SANSIRI_SMART_ELECTRIC_METER -> mView?.launchSmartElectricMeter(it!!, menu)
                Feature.PARTNER_SANSIRI_MEA -> mView?.launchMea(it!!, menu)
                Feature.PARTNER_PLUS_BOS -> mView?.launchBos(it!!, menu)
                else -> {
                    if (menu is DynamicMenu) {
                        actionExtraMenu(menu)
                    } else {

                    }
                }
            }
        }
    }


    private fun actionExtraMenu(menu: DynamicMenu) {
        cacheDynamicMenu = menu.copy()

        if (menu.url != null) {
            if (menu.type == ActionType.EXTERNAL_BROWSER_WITH_HEADER_TOKEN) {
                mView?.launchWebWithToken(menu.url ?: "", menu.title ?: "")
            } else {
                mView?.launchWeb(menu.url ?: "", menu.title ?: "")
            }
            return
        } else {
            when (menu.type) {
                ActionType.EXTERNAL_APP -> {
                    if (menu.androidUri != null) {
                        mView?.launchDeepLink(menu.id ?: "", menu.title
                                ?: "", menu.androidUri, menu.androidPackageName)
                    } else if (menu.androidPackageName != null) {
                        mView?.launchAnotherApp(menu.id ?: "", menu.title
                                ?: "", menu.androidPackageName)
                    } else {
                        mView?.showError("Cannot open the app.")
                    }
                }
                ActionType.EXTERNAL_BROWSER_WITH_HEADER_TOKEN -> {
                }
                ActionType.EXTERNAL_BROWSER -> {
                    menu.url?.let { mView?.launchWeb(it, menu.title ?: "") }
                }
                ActionType.MORE_MENU -> {
                    menu.items?.let {
                        mView?.launchMenuService(menu.id ?: "", menu.title ?: "", it)
                    }
                }
                else -> {
                    mView?.launchDialogToUpdate { }
                }
            }
        }
    }

    override fun reopenPreviousMenuWith(query: String) {
        if (cacheDynamicMenu != null) {
            val action = query.toUpperCase()
            if (action == ActionType.EXTERNAL_APP) {
                cacheDynamicMenu?.url = null
            }
            cacheDynamicMenu?.type = action
            actionExtraMenu(cacheDynamicMenu!!)
        }
    }

    override fun invokeProjectList() {
        val list = arrayListOf<Hut>()
        homeList?.forEach {
            list.add(it.toHut())
        }
        mView?.launceProjectPreview(list, RuntimeCache.selectedHome?.toHut())
    }

    override fun prepareForVoiceCommand() {
        if (RuntimeCache.selectedHome != null) {
            mView?.launchVoiceCommand(RuntimeCache.selectedHome)
        }
    }

    override fun requestMenuNavigation(feature: String, data: PushNotificationData?) {
        disableFindNearestHome = true
        if (homeList != null) {
            if (data?.unitId != null) {
                val targetHome = homeList?.find { home -> home.unitId == data.unitId }
                mView?.scrollToHome(homeList?.indexOf(targetHome) ?: 0)
                targetHome?.let {
                    actionOnNavigate(feature, it, data)
                }
            } else {
                actionOnNavigate(feature, null, data)
            }

        } else {
            mView?.let {
                selectedHomeObservable.observe().subscribe(it, {
                    if (data?.unitId != null) {
                        val targetHome = it.find { home -> home.unitId == data.unitId }
                        mView?.scrollToHome(it.indexOf(targetHome) ?: 0)
                        targetHome?.let {
                            actionOnNavigate(feature, it, data)
                        }
                    } else {
                        actionOnNavigate(feature, null, data)
                    }
                }) {}
            }
        }
    }

    override fun findHome(homeId: String) {
        val targetHome = homeList!!.find { it.unitId == homeId }
        mView?.scrollToHome(homeList!!.indexOf(targetHome))
    }

    override fun fetchAqi(home: Home) {
        val project = RuntimeCache.projects.find { it.objectId == home.projectId }
        if (project?.air != null) {
            mView?.bindAirData(project.air!!)
        } else {
            mAqiCompositeDisposable.dispose()
            mAqiCompositeDisposable = CompositeDisposable()
            mView?.loadingAirData()

            val api = ApiRepositoryProvider.provideApiUnAuthRepository()
            mAqiCompositeDisposable.add(
                    api.getAQI(home.projectId).observe().subscribe({ air ->
                        project?.let { it.air = air }
                        mView?.bindAirData(air)
                    }) {
                        mView?.errorAirData()
                        // mView?.bindAirData(null)
                    }
            )
        }
    }

    override fun handleNotification(notifications: List<PushNotification>?) {
        if (!notifications.isNullOrEmpty()) {
            notifications.filter { notification -> notification.type == Feature.CHAT }.map { notification ->
                if (!notification.isRead) {
                    // if chat notification has unitId
                    notification.data?.unitId?.let { unitId ->
                        // match notification with homeList
                        homeList?.find { home -> home.unitId == unitId }?.let { home ->
                            home.section?.find { section -> section.sectionName?.toLowerCase() == "menu" }?.let { section ->
                                section.items?.find { menu -> menu.id == Feature.CHAT }?.let { menu ->
                                    menu.badge += 1
                                }
                            }
                        }
                    }
                }
            }
            RuntimeCache.selectedHome?.section?.let { section ->
                mView?.bindSection(section, true)
            }

        }

    }

    override fun clearNotificationHistory(unitId: String, featureId: String) {
        GlobalScope.launch {
            notificationRepository.markAsRead(featureId, unitId)
        }
    }

    override fun fetchOrviboRoom(orviboHome: OrviboHome?) {
//        orviboRepository.getScenes()?.let { scenes ->
//            mView?.bindHomeAutomationScene(
//                    scenes.map { scene ->
//                        OrviboHomeAutomationRoomMenu(
//                                scene.id ?: "",
//                                scene.name ?: "",
//                                HMRoom.RoomType.SCENE
//                        )
//                    }
//            )
//        }
//
//        orviboRepository.getRooms()?.let { rooms ->
//            val convertedRooms = rooms.map { room ->
//                OrviboHomeAutomationRoomMenu(
//                        room.id ?: "",
//                        room.name ?: "",
//                        room.type
//                )
//            }.toMutableList()
//
//            mView?.bindHomeAutomation(convertedRooms)
//        }
    }

    private fun actionOnNavigate(feature: String, home: Home?, data: PushNotificationData?) {
        val menu = findTitleFeature(feature, home)
        when (feature) {
            Feature.CHAT -> home?.let { mView?.launchChat(home, menu) }
            Feature.MAIL_BOX -> home?.let { mView?.launchMailbox(home, menu?.title) }
            Feature.ANNOUNCEMENT -> mView?.launchAnnouncement(data?.objectId ?: "")
            Feature.IBOX -> home?.let { mView?.launchiBOX(home, menu) }
            Feature.MAINTENANCE_GUIDE -> home?.let { mView?.launchMaintenanceGuide(home, menu?.title, data?.planId, data?.accessoryId) }
        }
    }

    private fun findTitleFeature(feature: String, home: Home?): Menu? {
        if (home?.section != null) {
            for (section in home.section!!) {
                if (section.items != null) {
                    for (menu in section.items!!) {
                        if (menu.id == feature) {
                            return menu
                        }
                    }
                }
            }
        }

        return null
    }


}
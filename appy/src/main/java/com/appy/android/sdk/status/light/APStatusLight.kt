package com.appy.android.sdk.status.light

import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.light.LightCommand
import com.appy.android.sdk.status.APStatus

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
class APStatusLight : APStatus<LightCommand> {
    constructor() : super()
    constructor(control: APControl, value: String) : super(control, value)
}
package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.daikin.DaikinCommand
import com.appy.android.sdk.control.daikin.DaikinOnOffCommand

internal class SendCommandDaikinOnOffForm(
        val homeId: String,
        val daikinId: String,
        val commandValue: DaikinOnOffCommand
) : AppyForm() {
    val mSendCommandDaikinCommandValueJsonBody: SendCommandDaikinCommandValueJsonBody = SendCommandDaikinCommandValueJsonBody()

    init {
        this.mSendCommandDaikinCommandValueJsonBody.command = DaikinCommand.ON_OFF.action()
        this.mSendCommandDaikinCommandValueJsonBody.commandValue = commandValue.action()
    }
}
package com.sansiri.homeservice.ui.facilitybooking.calendar


import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.Timeline
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.Reservation
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.facilitybooking.FacilityBookingDetailActivity
import com.sansiri.homeservice.ui.facilitybooking.detail.FacilityBookingDetailFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_facility_booking_calendar.*
import java.util.*


class FacilityBookingCalendarFragment : BaseFragment<FacilityBookingCalendarContract.View, FacilityBookingCalendarContract.Presenter>(), FacilityBookingCalendarContract.View {
    override var mPresenter: FacilityBookingCalendarContract.Presenter = FacilityBookingCalendarPresenter()

    val bookList = mutableListOf<Booking>()
    private var mProjectId: String = ""
    private var mFacility: FacilityBooking? = null
    private var selectedDate: String? = null
    private var previousBookingId: String = ""

    companion object {
        val PROJECT_ID = "PROJECT_ID"
        val FACILITY = "FACILITY"

        fun newInstance(projectId: String, facility: FacilityBooking): FacilityBookingCalendarFragment {
            val fragment = FacilityBookingCalendarFragment()

            val bundle = Bundle()
            bundle.putString(PROJECT_ID, projectId)
            bundle.putParcelable(FACILITY, facility)
            fragment.arguments = bundle

            return fragment
        }
    }


    val mTimelineAdapter = TimelineAdapter({ startTime, endTime ->
        val booking = Booking(
                Calendar.getInstance().timeInMillis.toString(),
                startTime,
                endTime,
                ""
        )

        (activity as FacilityBookingDetailActivity).launchCreateBooking(booking, mPresenter.currentTimeline) {
            mPresenter.fetchData()
        }

    }) { timeline ->
        launchEditBooking(timeline)
    }

    private fun launchEditBooking(timeline: Timeline) {
        val book = bookList.find { it.idBooking == timeline.idBooking }
        book?.let {
            val bookedSlots = bookList.filter { item -> item.idBooking == timeline.idBooking }
            val startBooking = bookedSlots.minBy { item -> item.startTime }
            val endBooking = bookedSlots.maxBy { item -> item.endTime }

            if (startBooking != null && endBooking != null) {
                val myBooking = startBooking.copy().apply {
                    endTime = endBooking.endTime
                }

                (activity as FacilityBookingDetailActivity).launchEditBooking(myBooking) { booking ->
                    mPresenter.fetchData()
                }
            }

        }

    }

    override fun onResume() {
        super.onResume()
        sendScreenView("Facility Booking Time Slot Screen")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mProjectId = arguments?.getString(PROJECT_ID) ?: ""
        mFacility = arguments?.getParcelable<FacilityBooking>(FACILITY)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_facility_booking_calendar, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(listTimeline) {
            setHasFixedSize(true)
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            addItemDecoration(ItemDecorator((-10).px))
            adapter = mTimelineAdapter
            isFocusable = false
        }

        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            previousBookingId = ""
            selectedDate = createDateFormat(year, month, dayOfMonth)
            mPresenter.selectBookingDate(createDateFormat(year, month, dayOfMonth))
        }

        if (mFacility != null) {
            mPresenter.init(mProjectId, mFacility!!)
            mPresenter.fetchData()
        } else {
            // TODO: showError
        }

    }

    override fun setReservation(value: Reservation?) {
        value?.let { (activity as FacilityBookingDetailActivity).setReservation(value) }
    }

    override fun setCalendarDate(date: String) {
        calendarView.date = date.toDate().time
    }

    override fun getCalendarDate(): String? {
//        if (selectedDate == null) {
//            val calendar = Calendar.getInstance();
//            calendar.timeInMillis = calendarView.date
//            return createDateFormat(calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH])
//        }
        return selectedDate
    }

    private fun createDateFormat(year: Int, month: Int, dayOfMonth: Int) = "$year-${(month + 1).format(2)}-${dayOfMonth.format(2)}"

    override fun initTimeLine(startHour: Int, endHour: Int) {
        val listOfTime = mutableListOf<Timeline>()

        for (i in startHour..endHour) {
            val calendar = Calendar.getInstance()
            calendar.set(calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH], i, 0, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            listOfTime.add(Timeline(calendar))
        }

        mTimelineAdapter.setData(listOfTime)
    }

    override fun clearTimeLine() {
        mTimelineAdapter.clearData()
    }

    override fun addSlotToTimeLine(startBooking: Calendar, disable: Boolean, nextAvailableSlot: Date?) {
        mTimelineAdapter.addData(Timeline(startBooking).apply {
            this.disable = disable
            if (disable && nextAvailableSlot != null) {
                this.title = "${getString(R.string.facility_booking_next_available)} ${nextAvailableSlot?.report()} ${nextAvailableSlot?.reportTime()}"
            }
        })
    }

    override fun showDescription(text: String) {
        // textSubtitle.text = text
    }

    override fun showLoading() {
        progressBar.show()
        listTimeline.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
        listTimeline.show()
    }

    override fun showUnavailable() {
        textUnavailable.show()
        listTimeline.hide()
    }

    override fun hideUnavailable() {
        textUnavailable.hide()
        listTimeline.show()
    }

    override fun bookMine(id: String, startTime: Calendar, endTime: Calendar, note: String) {
        bookList.add(Booking(id, startTime, endTime, note))
        val title = if (previousBookingId == id) {
            ""
        } else if (note.isNotBlank()) {
            "${getString(R.string.my_booking)}: ${note}"
        } else {
            getString(R.string.my_booking)
        }

        mTimelineAdapter.myBook(id, startTime, endTime, title)

        if (id.isNotEmpty()) previousBookingId = id
    }

    override fun book(startTime: Calendar, endTime: Calendar) {
        mTimelineAdapter.book(Calendar.getInstance().timeInMillis.toString(), startTime, endTime, getString(R.string.not_available))
    }

    override fun showError(message: String) {
        activity?.snackError(message)
    }
}

package com.sansiri.homeservice.data.repository.orvibo

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
//import com.homemate.sdk.HomeMateSDK
//import com.homemate.sdk.model.HMDevice
//import com.homemate.sdk.model.HMFamily
//import com.homemate.sdk.model.HMHub
import com.sansiri.homeservice.data.database.orvibo.OrviboDao
import com.sansiri.homeservice.model.database.orvibo.OrviboHome

//class OrviboRepositoryImpl(val orviboDao: OrviboDao, val homeMateSDK: HomeMateSDK): OrviboRepository {
//    override fun getOrviboHome(unitId: String): LiveData<OrviboHome> = orviboDao.getOrviboHome(unitId)
//
//    @WorkerThread
//    override suspend fun insertOrviboHome(home: OrviboHome) = orviboDao.insertOrviboHome(home)
//
//    @WorkerThread
//    override suspend fun deleteOrviboHome(unitId: String) = orviboDao.deleteOrviboHome(unitId)
//
//    override fun isLoggedIn() = homeMateSDK.isLoggedIn()
//
//    override fun login(username: String, password: String, onSuccess: () -> Unit, onError: (String) -> Unit) = homeMateSDK.login(username, password, onSuccess, onError)
//
//    override fun logout() = homeMateSDK.logOut()
//
//    override fun getPasswordMD5(): String = homeMateSDK.getCurrentPasswordMD5()
//
//    override fun getFamilies(onSuccess: (List<HMFamily>) -> Unit) =  homeMateSDK.getFamilies(onSuccess)
//
//    override fun getCurrentFamilyId() = homeMateSDK.getCurrentFamilyId()
//
//    override fun switchFamily(familyId: String, onSuccess: () -> Unit) = homeMateSDK.switchFamily(familyId, onSuccess)
//
//    @WorkerThread
//    override suspend fun updateFamilyId(unitId: String, familyId: String)  {
//        orviboDao.updateFamilyId(unitId, familyId)
//    }
//
//    override fun getScenes() = homeMateSDK.getScenes()
//
//    override fun getRooms() = homeMateSDK.getRooms()
//
//    override fun getDevices(hubId: String?, roomId: String): List<HMDevice>? {
//        val devices = homeMateSDK.getDevices(roomId)
//        if (hubId != null) {
//            val statuses = homeMateSDK.getDevicesStatus(hubId)
//            devices?.forEach { device ->
//                statuses.forEach { status ->
//                    if (device.id == status.deviceId) {
//                        device.status = status
//                    }
//                }
//            }
//        }
//
//        return devices
//    }
//
//    override fun getHubInfo(onSuccess: (List<HMHub>?) -> Unit, onError: () -> Unit) = homeMateSDK.getHubInfo(onSuccess, onError)
//
//    override fun turnOnSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit) = homeMateSDK.turnOnSwitch(hubId, deviceId, onSuccess)
//
//    override fun turnOffSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit) = homeMateSDK.turnOffSwitch(hubId, deviceId, onSuccess)
//
//    override fun toggleSwitch(hubId: String, deviceId: String, onSuccess: () -> Unit)  = homeMateSDK.toggleSwitch(hubId, deviceId, onSuccess)
//
//    override fun dimmingLight(hubId: String, deviceId: String, progress: Int, onSuccess: () -> Unit) = homeMateSDK.dimmingLight(hubId, deviceId, progress, onSuccess)
//
//    override fun activateScene(hubId: String, sceneId: String, onSuccess: () -> Unit) = homeMateSDK.activateScene(hubId, sceneId, onSuccess)
//}
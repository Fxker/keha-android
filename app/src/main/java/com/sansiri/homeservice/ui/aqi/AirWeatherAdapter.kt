package com.sansiri.homeservice.ui.aqi

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.partner.aqi.AirWeather

import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_air_weather.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class AirWeatherAdapter() : androidx.recyclerview.widget.RecyclerView.Adapter<AirWeatherAdapter.ViewHolder>() {
    private val mData = mutableListOf<AirWeather>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirWeatherAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_air_weather))
    }

    override fun onBindViewHolder(holder: AirWeatherAdapter.ViewHolder, position: Int) {
        holder.bindData(mData[position])
    }

    fun setData(data: List<AirWeather>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bindData(weather: AirWeather) {
            with(view) {
                Glide.with(context).loadCache(weather.iconUrl ?: "", imageIcon)
                textValue.text = weather.valueDisplay
            }
        }
    }
}
package com.sansiri.homeservice.ui.phonedirectory

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.PhoneDirectoryGroup
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by sansiri on 10/12/17.
 */
class PhoneDirectorySectionAdapter(val title: String, val mData: MutableList<PhoneDirectoryGroup>, val itemClick: (String) -> Unit) : StatelessSection(SectionParameters.Builder(R.layout.view_phone_directory_card)
        .headerResourceId(R.layout.view_mailbox_date)
        .build()) {

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return PhoneDirectoryViewHolder(view, itemClick)
    }

    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is PhoneDirectoryViewHolder) {
            holder.bind(mData[position])
        }
    }


}
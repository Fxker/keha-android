package com.sansiri.homeservice

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.view.Window
import android.view.WindowManager
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate
import com.appy.android.sdk.AppySDK
import com.facebook.FacebookSdk
import com.google.firebase.FirebaseApp
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.RetrofitFactory
import com.sansiri.homeservice.util.Connectivity
import net.danlew.android.joda.JodaTimeAndroid
import com.crashlytics.android.Crashlytics
import com.facebook.appevents.AppEventsLogger
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig
//import com.homemate.sdk.HomeMateSDK
import com.mea.energysdk.service.MEAEnergy
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.language.LanguageActivity
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.ui.register.forgotpassword.ForgotPasswordActivity
import com.sansiri.homeservice.ui.welcome.WelcomeActivity
import com.sansiri.homeservice.util.withDelay
import io.fabric.sdk.android.Fabric
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.AppCenter
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.di.*
import com.sansiri.homeservice.util.TimeShow
import io.reactivex.plugins.RxJavaPlugins
import me.leolin.shortcutbadger.ShortcutBadger
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


/**
 * Created by sansiri on 10/12/17.
 */
class AppController : MultiDexApplication() {
    var localizationDelegate = LocalizationApplicationDelegate(this)
    //    private var sAnalytics: GoogleAnalytics? = null
//    private var sTracker: Tracker? = null
    private var isSplashScreenShown = false

    override fun onCreate() {
        super.onCreate()

//        sAnalytics = GoogleAnalytics.getInstance(this)
        AppCenter.start(this, getString(R.string.ms_secret_key), Analytics::class.java)

        Fabric.with(this, Crashlytics())
        FirebaseApp.initializeApp(this)
        FirebaseRemoteConfigManager().fetch { }
        AppySDK.init(this)
        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(this)
        Connectivity.initialize(this)
        Store.initialize(this)
        JodaTimeAndroid.init(this)
        TimeShow.init(this)
        initFresco()
        initEndpoint()

//        HomeMateSDK.init("Home Service", this)
        MEAEnergy.getInstance().setInitializeApp(this)

        startKoin {
            androidLogger()
            androidContext(this@AppController)
            androidFileProperties()
            modules(modules)
        }

        RxJavaPlugins.setErrorHandler {  }
    }

//    @Synchronized
//    fun getDefaultTracker(): Tracker? {
//        if (sTracker == null) {
//            sTracker = sAnalytics?.newTracker(R.xml.global_tracker)
//        }
//
//        return sTracker
//    }

    fun initEndpoint() {
        RetrofitFactory.endpoint = getString(R.string.app_endpoint)
    }

    fun initFresco() {
        val config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
                .setDownsampleEnabled(true)
                .build()
        Fresco.initialize(this, config)
    }

    fun removeBadgeCount() {
        val prefs = PreferenceHelper.defaultPrefs(this)
        prefs[PreferenceHelper.NOTIFICATION_COUNT] = 0
        ShortcutBadger.removeCount(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base))
        MultiDex.install(this);
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    fun guardRoute(activity: Activity) {
        val prefs = PreferenceHelper.defaultPrefs(this)

        if (prefs[PreferenceHelper.IS_FIRST_TIME, true]!!) {
            when (activity) {
                is LanguageActivity -> return
            }
            LanguageActivity.start(activity)
            activity.finish()
        } else if (!FirebaseAuthManager.isSignIn()) {
            when (activity) {
                is WelcomeActivity, is RegisterActivity, is ForgotPasswordActivity, is ContentActivity -> return
            }

            WelcomeActivity.start(activity)
            activity.finish()
        }

        // showSplashScreen(activity)
    }

    private fun showSplashScreen(activity: Activity) {
        if (!isSplashScreenShown) {
            isSplashScreenShown = true

            val dialog = Dialog(activity, R.style.AppTheme_FullScreen)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.activity_splash_screen)
            dialog.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
            dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            dialog.window.attributes.windowAnimations = R.style.SplashScreenDialog
            dialog.setCancelable(false)
            dialog.show()

            if (true) {
                { dialog.dismiss() }.withDelay(2000)
            }
        }
    }
}
package com.appy.android.code.dependency.component

import android.content.Context
import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.TheadHandlerImpl
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.dependency.module.AppySDKModule
import com.appy.android.sdk.AppySDK
import dagger.Component
import javax.inject.Singleton

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
@Singleton
@Component(modules = arrayOf(AppySDKModule::class))
internal interface AppySDKComponent {

    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun threadHandler(): TheadHandlerImpl

    fun restClient(): RestClient

    fun inject(appySDK: AppySDK)
}
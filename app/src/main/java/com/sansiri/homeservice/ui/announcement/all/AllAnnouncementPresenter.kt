package com.sansiri.homeservice.ui.announcement.all

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.announcement.summary.SummaryItem
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 12/14/17.
 */
class AllAnnouncementPresenter : BasePresenterImpl<AllAnnouncementContract.View>(), AllAnnouncementContract.Presenter {
    var id: String? = null
    override fun init(id: String?) {
        this.id = id
    }

    override fun fetchData(page: Int) {
        id?.let {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    if (page == 0) {
                        mView?.showLoading()
                    }
                    if (id == SummaryItem.SANSIRI_FAMILY) {
                        api.getFamilyAnnouncement(page * 5, 5).observe().subscribe({
                            mView?.hideLoading()
                            mView?.addData(it)
                        }){
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                    } else  if (id == SummaryItem.ANNOUNCEMENT) {
                        api.getCentralProjectAnnouncement(page * 5, 5).observe().subscribe({
                            mView?.hideLoading()
                            mView?.addData(it)
                        }){
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                    } else {
                        if (id != null) {
                            api.getProjectAnnouncement(id!!, page * 5, 5).observe().subscribe({
                                mView?.hideLoading()
                                mView?.addData(it)
                            }){
                                mView?.hideLoading()
                                mView?.showError(it.showHttpError())
                            }
                        } else {

                        }
                    }
                } else {

                }
            }
        }
    }

    override fun start() {
    }

    override fun destroy() {
    }
}
package com.sansiri.homeservice.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.os.SystemClock
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build.VERSION_CODES.O
import android.os.Build.VERSION.SDK_INT
import androidx.core.app.NotificationCompat
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.main.MainActivity
import android.media.AudioAttributes
import android.net.Uri
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.sansiri.homeservice.data.database.notification.NotificationDao
import com.sansiri.homeservice.data.repository.notification.NotificationRepository
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.menu.Feature
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.koin.core.KoinComponent
import org.koin.core.inject


/**
 * Created by oakraw on 17/11/2017 AD.
 */
class NotificationHelper(val context: Context, val notificationId: Int) : KoinComponent {
    companion object {
        val DATA = "DATA"
        val TYPE = "TYPE"
    }

    val CHANNEL_ID = "HSA"
    val CHANNEL_NAME = "General Notification"

    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    var notificationBuilder: NotificationCompat.Builder? = null
    val notificationRepository: NotificationRepository by inject()
    var title: String? = null
    var content: String? = null
    var color: Int = context.resources.getColor(R.color.colorSecondary)
    var type: String? = null
    var data: String? = null

    @SuppressLint("NewApi")
    fun create(title: String, content: String, color: Int, type: String? = null, data: String? = null): NotificationHelper {
        this.title = title
        this.content = content
        this.color = color
        this.type = type
        this.data = data

        val resultIntent = Intent(context, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        type?.let { resultIntent.putExtra(TYPE, it) }
        data?.let { resultIntent.putExtra(DATA, it) }
        val pendingIntent = PendingIntent.getActivity(
                context,
                notificationId,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        var channelId = CHANNEL_ID
        when (type) {
            Feature.MECHANICAL_PARKING -> {
                channelId = "HSA-MP"
                setUpChannel(channelId, "Mechanical Parking", R.raw.car_horn)
            }
            else -> {
                channelId = CHANNEL_ID
                setUpChannel(channelId, CHANNEL_NAME)
            }
        }

        val builder = NotificationCompat.Builder(context, channelId)

        notificationBuilder = builder.setContentTitle(title)
                .setContentText(content)
                .setStyle(NotificationCompat.BigTextStyle().bigText(content))
                .setVibrate(longArrayOf(100, 100, 100, 100))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(pendingIntent)
                .setColor(color)
                .setAutoCancel(true)

        val notification = try {
            val notificationData = Gson().fromJson(data, PushNotificationData::class.java)
            PushNotification(title = title, message = content, type = type, createdAt = DateTime.now().toString(), data = notificationData)
        } catch (e: JsonSyntaxException) {
            PushNotification(title = title, message = content,  createdAt = DateTime.now().toString(), type = type)
        }
        saveToDatabase(notification)

        return this
    }

    private fun saveToDatabase(notification: PushNotification) {
        GlobalScope.launch {
            if (notification.type == Feature.CHAT && notification.data?.unitId != null) {
                // clear previous notification
                notificationRepository.deleteFeatureNotification(notification.type, notification.data.unitId)
            }
            notificationRepository.insert(notification)
        }
    }

    @SuppressLint("NewApi")
    fun setUpChannel(channelId: String, channelName: String, soundRes: Int? = null) {
        if (SDK_INT >= O) {
            val channel = NotificationChannel(channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_HIGH)

            soundRes?.let { setSound(channel, soundRes) }
            notificationManager.createNotificationChannel(channel)
        } else {
            soundRes?.let { setSound(soundRes) }
        }
    }

    @SuppressLint("NewApi")
    fun setSound(channel: NotificationChannel, soundRes: Int) {
        val soundUri = Uri.parse("android.resource://" + context.packageName + "/" + soundRes)
        val audioAttr = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build()
        soundUri?.let { channel.setSound(soundUri, audioAttr) }
        notificationManager.createNotificationChannel(channel)
    }

    fun setSound(soundRes: Int) {
        val soundUri = Uri.parse("android.resource://" + context.packageName + "/" + soundRes)
        soundUri?.let { notificationBuilder?.setSound(soundUri) }
    }

    fun schedule(delayInMillis: Long) {
        val notificationIntent = Intent(context, NotificationPublisher::class.java)
        notificationIntent.putExtra(NotificationPublisher.ID, notificationId)
        notificationIntent.putExtra(NotificationPublisher.TITLE, title)
        notificationIntent.putExtra(NotificationPublisher.CONTENT, content)
        notificationIntent.putExtra(NotificationPublisher.COLOR, color)
        notificationIntent.putExtra(NotificationPublisher.TYPE, type)

        val pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val futureInMillis = SystemClock.elapsedRealtime() + delayInMillis
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)
    }


    fun submit() {
        notificationBuilder?.let {
            notificationManager.notify(notificationId, it.build())
        }
    }
}
package com.sansiri.homeservice.ui.ibox.list

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.IBoxItem
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.reportTime
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/12/17.
 */
class IBoxViewHolder(val view: View, val itemClick: ((IBoxItem) -> Unit)? = null) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: IBoxItem) {
        with(view) {
            textType.text = "${context.getString(R.string.ibox_number)}: ${data.referenceId}"
            textTitle.text = data.fullname
            textSubtitle.text = "${data._createdAt?.reportTime()}"
            imageIcon.setImageResource(R.drawable.ic_locker)
            textDate.hide()
            root.background = null
//            root.setOnClickListener {
//                itemClick(html)
//            }
        }
    }
}
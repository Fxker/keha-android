package com.sansiri.homeservice.ui.automation.appy.remotecontrol

import com.appy.android.code.base.data.rest.error.APError
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.home.APHome
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.showHttpError

class HomeAutomationRemoteControlPresenter : BasePresenterImpl<HomeAutomationRemoteControlContract.View>(), HomeAutomationRemoteControlContract.Presenter {
    lateinit var mAppyManager: AppyRepositoryImpl
    lateinit var control: APRemoteControl
    lateinit var homeId: String
    lateinit var projectId: String

    override fun init(control: APRemoteControl, homeId: String, projectId: String) {
        this.control = control
        this.homeId = homeId
        this.projectId = projectId
        mAppyManager = AppyRepositoryImpl(projectId, "12345")
    }

    override fun start() {
        if (control.mActions != null) {
            mView?.bindData(control.mActions!!)
        }
    }

    override fun destroy() {
    }


    override fun action(actionRemoteControl: APActionRemoteControl) {
        val home = APHome()
        home.id = mAppyManager.getDedicatedUnitId(homeId ?: "")

        mAppyManager.commandRemoteControl(home, control, actionRemoteControl, {
            mView?.showMessage("Success")
        }) {
            processErrorMessage(it)
        }

    }

    private fun processErrorMessage(error: Throwable) {
        if (error is APError) {
            mView?.showError(error.description ?: error.error ?: error.code?.toString() ?: "Error")
        } else {
            mView?.showError(error.showHttpError())
        }
    }
}
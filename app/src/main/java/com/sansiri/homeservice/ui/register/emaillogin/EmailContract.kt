package com.sansiri.homeservice.ui.register.emaillogin

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/17/17.
 */
interface EmailContract {
    interface Presenter : BasePresenter<View> {
        fun validateAndSignIn(username: String, password: String)
        fun signIn(email: String, password: String)
    }

    interface View : BaseView {
        fun usernameError(message: String)
        fun passwordError(message: String)
        fun showLoading()
        fun hideLoading()
        fun nextScreen()
        fun getStringFromRes(res: Int): String
        fun signOut()
        fun showError(email: String, message: String)
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.airbnb.lottie.LottieAnimationView
import com.sansiri.homeservice.BuildConfig

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.component.creditcard.ExpiryMonthSpinnerAdapter
import com.sansiri.homeservice.component.creditcard.ExpiryYearSpinnerAdapter
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_trendy_wash_payment_gateway_credit_card.*
import org.koin.androidx.scope.currentScope


class CreditCardFragment : BaseV2Fragment<CreditCardContract.View, CreditCardContract.Presenter>(), CreditCardContract.View {
    override val mPresenter: CreditCardContract.Presenter by currentScope.inject()

    private var mSubmitDialog: AlertDialog? = null
    private var mSuccessAnimation: LottieAnimationView? = null
    private var mProgressBar: ProgressBar? = null
    private var mTextUpload: TextView? = null

    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mAmount: Int = 0

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"
        const val AMOUNT = "AMOUNT"

        fun newInstance(unitId: String, userId: String, amount: Int): CreditCardFragment {
            return CreditCardFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(USER_ID, userId)
                    putInt(AMOUNT, amount)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mUserId = arguments?.getString(USER_ID)
        mAmount = arguments?.getInt(AMOUNT) ?: 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trendy_wash_payment_gateway_credit_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val builder = AlertDialog.Builder(context!!, R.style.Base_Theme_AppCompat_Dialog)
        layoutInflater.inflate(R.layout.dialog_upload, null).apply {
            mProgressBar = this.findViewById(R.id.progressBar)
            mSuccessAnimation = this.findViewById(R.id.success)
            mTextUpload = this.findViewById(R.id.textName)
            builder.setView(this)
            mSubmitDialog = builder.create()
        }

        spinnerExpiryMonth.adapter = ExpiryMonthSpinnerAdapter()
        spinnerExpiryYear.adapter = ExpiryYearSpinnerAdapter()

        editName.addTextChangedListener(TextWatcherExtend {
            mPresenter.setCardName(it)
        })

        editCreditCardNumber.addTextChangedListener(TextWatcherExtend {
            mPresenter.setCardNumber(it)
        })

        editCVV.addTextChangedListener(TextWatcherExtend {
            mPresenter.setCCV(it)
        })

        spinnerExpiryMonth.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val month = spinnerExpiryMonth.selectedItem as Int
                mPresenter.setExpiryMonth(month)
            }
        }

        spinnerExpiryYear.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val year = spinnerExpiryYear.selectedItem as Int
                mPresenter.setExpiryYear(year)
            }
        }

        buttonConfirm.setOnClickListener {
            mPresenter.purchase()?.observe(this, Observer { resource ->
                when (resource?.status) {
                    Resource.LOADING -> {
                        showSubmitting()
                    }
                    Resource.SUCCESS -> {
                        showSubmitted()
                    }
                    Resource.ERROR -> {
                        hideSubmitting()
                        showError(resource.message ?: "Error")
                    }
                }
            })
        }

        if (BuildConfig.BUILD_TYPE == "debug") {
            editName.setText("JOHN DOW")
            editCreditCardNumber.setText("4242 4242 4242 4242")
            editCVV.setText("123")
            spinnerExpiryMonth.setSelection(3)
            spinnerExpiryYear.setSelection(2)
        }

        mPresenter.init(mUnitId, mUserId, mAmount)
        mPresenter.start()

        mPresenter.creditInfo?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.SUCCESS -> {
                    textNotice.show()
                    textNotice.text = resource.data?.creditCardText
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showSubmitting() {
        mTextUpload?.text = "Validating credit card..."
        mSuccessAnimation?.hide()
        mProgressBar?.show()
        mSubmitDialog?.show()
    }

    override fun showSubmitted() {
        if (mSubmitDialog != null && !mSubmitDialog!!.isShowing) {
            mSubmitDialog!!.show()
        }
        mProgressBar?.hide()
        mTextUpload?.text = "Order completed"
        mSuccessAnimation?.show()
        mSuccessAnimation?.playAnimation()
        ({ goBack() }).withDelay(2500)

        sendEvent("TRENDY_WASH_TOP_UP_CHANNEL", "PURCHASE", "CREDIT_CARD")
    }

    override fun hideSubmitting() {
        mSubmitDialog?.dismiss()
    }

    override fun enableContinueButton() {
        buttonConfirm.isEnabled = true
        buttonConfirm.alpha = 1f
    }

    override fun disableContinueButton() {
        buttonConfirm.isEnabled = false
        buttonConfirm.alpha = 0.5f
    }

    override fun showCreditCardError() {
        editCreditCardNumber.error = getString(R.string.please_fill_out_this_field)
    }

    private fun goBack() {
        activity?.setResult(Activity.RESULT_OK, Intent())
        activity?.finish()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

}

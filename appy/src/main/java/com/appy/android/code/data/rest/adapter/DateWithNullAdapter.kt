package com.appy.android.code.data.rest.adapter

import com.appy.android.code.base.data.rest.adapter.BaseGsonAdapter
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
internal class DateWithNullAdapter : BaseGsonAdapter(), JsonDeserializer<Date?> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Date? {
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US)
        return try {
            df.parse(json!!.asString)
        } catch (e: ParseException) {
            null
        }
    }

}
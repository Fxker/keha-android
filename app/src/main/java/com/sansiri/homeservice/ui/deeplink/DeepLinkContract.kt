package com.sansiri.homeservice.ui.deeplink

import android.net.Uri
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import io.reactivex.Single

interface DeepLinkContract {
    interface Presenter : BasePresenter<View> {
        fun fetchMe(): Single<Profile>
        fun requestLaunchWeb(uri: Uri, useToken: Boolean = false)
    }

    interface View : BaseView {
        fun launchWeb(url: String)
        fun launchWebWithToken(url: String)
        fun launchLatestActivityIfExist()
        fun closeWeb()
        fun launchTrendyWash(customerId: String?)
    }
}
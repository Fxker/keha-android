package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sansiri.homeservice.R

/**
 * Created by oakraw on 10/3/2017 AD.
 */
class LocalIconMenu(
        @SerializedName("type")
        override val id: String?,
        @SerializedName("name")
        override val title: String? = null,
        val iconRes: Int? = null,
        @SerializedName("iconUrl")
        override val icon: String? = null
) : Menu, Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readValue(Int::class.java.classLoader) as Int?,
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(id)
                writeString(title)
                writeValue(iconRes)
                writeString(icon)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<LocalIconMenu> = object : Parcelable.Creator<LocalIconMenu> {
                        override fun createFromParcel(source: Parcel): LocalIconMenu = LocalIconMenu(source)
                        override fun newArray(size: Int): Array<LocalIconMenu?> = arrayOfNulls(size)
                }
        }
}

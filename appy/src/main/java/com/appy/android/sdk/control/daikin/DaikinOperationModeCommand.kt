package com.appy.android.sdk.control.daikin

enum class DaikinOperationModeCommand: APDaikinParameter {
    FAN {
        override fun action(): String {
            return "1"
        }
    },
    HEAT {
        override fun action(): String {
            return "2"
        }
    },
    COOL {
        override fun action(): String {
            return "4"
        }
    },
    DRY {
        override fun action(): String {
            return "64"
        }
    };
}
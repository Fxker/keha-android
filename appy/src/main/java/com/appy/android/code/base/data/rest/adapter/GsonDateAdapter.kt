package com.appy.android.code.base.data.rest.adapter

import com.appy.android.code.base.data.rest.adapter.BaseGsonAdapter
import com.google.gson.*
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by gregoire barret on 5/14/15.
 * For MasuProj project.
 *
 *
 * Parse the date to string and the string to Date of json parsing.
 */
open class GsonDateAdapter : BaseGsonAdapter, JsonSerializer<Date>, JsonDeserializer<Date> {
  private val dateFormat: DateFormat

  /**
   * Constructor of the Type adapter, the date is on the yyyy-MM-dd'T'HH:mm:ssZ" format.
   */
  constructor() {
    dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US)
    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
  }

  /**
   * Constructor of the Type adapter.
   */
  constructor(format: String) {
    dateFormat = SimpleDateFormat(format, Locale.US)
    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
  }

  /**
   * Constructor of the Type adapter.
   */
  constructor(format: String, timeZone: TimeZone) {
    dateFormat = SimpleDateFormat(format, Locale.US)
    dateFormat.timeZone = timeZone
  }

  /**
   * Serialise the [Date] object into [JsonElement]

   * @param date                     date to serialize
   * *
   * @param type                     Not used
   * *
   * @param jsonSerializationContext Not used
   * *
   * @return String JsonElement for set the date into json
   */
  @Synchronized override fun serialize(date: Date, type: Type,
                                       jsonSerializationContext: JsonSerializationContext): JsonElement {
    return JsonPrimitive(dateFormat.format(date))
  }

  /**
   * Deserialize the [JsonElement] date string into [Date]

   * @param jsonElement                string date to deserialize
   * *
   * @param type                       not used
   * *
   * @param jsonDeserializationContext not used
   * *
   * @return [Date] object
   */
  @Synchronized override fun deserialize(jsonElement: JsonElement, type: Type,
                                         jsonDeserializationContext: JsonDeserializationContext): Date {
    try {
      return dateFormat.parse(jsonElement.asString)
    } catch (e: ParseException) {
      throw JsonParseException(e)
    }

  }

  companion object {

    val TAG = "GsonDateAdapter"
  }
}

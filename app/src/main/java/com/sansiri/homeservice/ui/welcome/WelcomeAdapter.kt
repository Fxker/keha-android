package com.sansiri.homeservice.ui.welcome

import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.ui.announcement.all.AllAnnouncementAdapter
import com.sansiri.homeservice.ui.announcement.all.AnnouncementViewHolder
import com.sansiri.homeservice.util.inflate

/**
 * Created by sansiri on 1/19/18.
 */
class WelcomeAdapter(override val itemClick: (View, Announcement<*>) -> Unit): AllAnnouncementAdapter(itemClick) {
    val HEADER = 0
    val BODY = 1

    override fun getItemCount(): Int = mData.size + 1

    override fun getItemViewType(position: Int): Int {
        return when(position) {
            0 -> HEADER
            else -> BODY
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER -> LogoViewHolder(parent.inflate(R.layout.fragment_header_logo))
            else -> AnnouncementViewHolder(parent.inflate(R.layout.view_announcement_large), itemClick)
        }

    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if(holder is AnnouncementViewHolder) {
            holder.bind(mData[position - 1])
        }else if(holder is LogoViewHolder) {
            holder.bind()
        }
    }




}
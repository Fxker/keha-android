package com.sansiri.homeservice.model.api

import android.os.Parcel
import android.os.Parcelable

data class ParkingQueue(
        val queueId: Int,
        val processStatus: String? = null,
        val lotNo: String? = null,
        val processType: String? = null,
        val estimateTime: Int,
        val queueNo: String? = null
) : Parcelable {
    constructor() : this(0, "", "", "", 0, "")

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(queueId)
        parcel.writeString(processStatus)
        parcel.writeString(lotNo)
        parcel.writeString(processType)
        parcel.writeInt(estimateTime)
        parcel.writeString(queueNo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ParkingQueue> {
        override fun createFromParcel(parcel: Parcel): ParkingQueue {
            return ParkingQueue(parcel)
        }

        override fun newArray(size: Int): Array<ParkingQueue?> {
            return arrayOfNulls(size)
        }
    }

}

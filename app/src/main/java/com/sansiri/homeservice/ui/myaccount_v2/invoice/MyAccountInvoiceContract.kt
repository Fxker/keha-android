package com.sansiri.homeservice.ui.myaccount_v2.invoice

import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface MyAccountInvoiceContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun fetch(page: Int)
    }

    interface View : BaseView {
        fun bindData(invoices: List<Invoice>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
    }
}
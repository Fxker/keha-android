package com.sansiri.homeservice.ui.facilitybooking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.Menu
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.api.facilitybooking.CategoryData
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.Reservation
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import com.sansiri.homeservice.ui.facilitybooking.calendar.FacilityBookingCalendarFragment
import com.sansiri.homeservice.ui.facilitybooking.detail.FacilityBookingDetailFragment
import com.sansiri.homeservice.util.*

class FacilityBookingDetailActivity : LocalizationActivity() {

    companion object {
        val BOOKING_DETAIL = "BOOKING_DETAIL"
        val PROJECT_ID = "PROJECT_ID"
        val UNIT_ID = "UNIT_ID"
        val ADDRESS = "ADDRESS"
        val FACILITY = "FACILITY"
        val CALENDAR = "CALENDAR"

        fun start(activity: Activity, projectId: String, unitId: String, address: String, facility: FacilityBooking) {
            val intent = Intent(activity, FacilityBookingDetailActivity::class.java)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(ADDRESS, address)
            intent.putExtra(FACILITY, facility)
            activity.startActivity(intent)
        }
    }

    private var mProjectId: String = ""
    private var mUnitId: String = ""
    private var mFacility: FacilityBooking? = null
    private var mAddress: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        mProjectId = intent.getStringExtra(PROJECT_ID)
        mUnitId = intent.getStringExtra(UNIT_ID)
        mFacility = intent.getParcelableExtra<FacilityBooking>(FACILITY)
        mAddress = intent.getStringExtra(ADDRESS)

        setBackToolbar(mFacility?.getContent()?.title ?: "")

        if (mFacility != null) {
            selectScreen(FacilityBookingCalendarFragment.newInstance(mProjectId, mFacility!!), false)
        } else {
            snackError(getString(R.string.error_default_message))
            finish()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_facility_booking_info, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        when(item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.navigation_info -> mFacility?.let {
                val facility = mFacility?.copy()
                facility?.categoryData = null
                FacilityBookingInfoActivity.start(this, facility!!)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun selectScreen(fragment: androidx.fragment.app.Fragment, isAddToBackStack: Boolean = true) {
        addFragment(fragment, isAddToBackStack)
    }

    fun launchCreateBooking(booking: Booking, timeline: List<TimesItem>?, onSubmit: (Booking) -> Unit) {
        val fragment = FacilityBookingDetailFragment.newInstance(mProjectId, mUnitId, mAddress ?: "", mFacility!!, booking, timeline,false, onSubmit)
        addNonExistFragment(fragment, "CreateBooking"
                , true)
    }

    fun launchEditBooking(booking: Booking, onSubmit: (Booking) -> Unit) {
        val fragment = FacilityBookingDetailFragment.newInstance(mProjectId, mUnitId, mAddress ?: "", mFacility!!, booking, null,true, onSubmit)
        addNonExistFragment(fragment, "EditBooking", true)
    }


    fun setReservation(reservation: Reservation) {
        mFacility?.categoryData = CategoryData(reservation)
    }
}

package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 10/24/17.
 */
data class CitizenIDVerifyResult(
        val customerId: String,
        val loginWithEmail: Boolean,
        val loginWithFacebook: Boolean,
        val loginWithGoogle: Boolean,
        val loginWithTwitter: Boolean,
        val registeredWithEmailOrName: String
)
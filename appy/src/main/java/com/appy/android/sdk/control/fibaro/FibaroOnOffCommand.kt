package com.appy.android.sdk.control.fibaro

import com.appy.android.sdk.control.APCommand

/**
 * Created by Francisco Barrios 15/11/2018.
 */

enum class FibaroOnOffCommand : APCommand {
    ON {
        override fun action(): String {
            return "{\"args\":[5]}"
        }
    },
    OFF {
        override fun action(): String {
            return "{\"args\":[0]}"
        }
    };
}
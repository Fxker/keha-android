package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.error.APErrorFactory
import com.appy.android.code.data.rest.service.GenericNetworkService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 12/10/2560.
 */
internal class SendFibaroCommandInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendFibaroCommandForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendFibaroCommandForm): Observable<CommandApiResult> {
        return restClient
            .createAPIService(GenericNetworkService::class.java, params.getUsername()!!, params.getPassword()!!)
            .post(params.getURL(), params.body)
            .map {
                if (!it.isSuccessful) {
                    throw APErrorFactory.create(it)
                }

                CommandApiResult()
            }
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.ui.epayment.CodeEngine
import com.sansiri.homeservice.ui.epayment.EPaymentFragment
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_trendy_wash_payment_gateway_transfer.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.koin.androidx.scope.currentScope


class TransferFragment : EPaymentFragment<TransferContract.View, TransferContract.Presenter>(), TransferContract.View {
    override val mPresenter: TransferContract.Presenter by currentScope.inject()

    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mAmount: Int = 0
    private var mQRCode: String? = null
    private var mCreditInfo: TrendyWashCredit? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"
        const val AMOUNT = "AMOUNT"

        fun newInstance(unitId: String, userId: String, amount: Int): TransferFragment {
            return TransferFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(USER_ID, userId)
                    putInt(AMOUNT, amount)
                }
            }
        }
    }

    override fun getDetailBitmap(): Bitmap = createDetailBitmap(this.context!!, mAmount, mCreditInfo?.email)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mUserId = arguments?.getString(USER_ID)
        mAmount = arguments?.getInt(AMOUNT) ?: 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trendy_wash_payment_gateway_transfer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.init(mUnitId, mUserId, mAmount)
        mPresenter.start()

        mPresenter.paymentGateWayQR?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data != null) {
                        resource.data?.paymentQrCode?.let { bindData(it) }
                    } else {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.paymentQrCode?.let { bindData(it) }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })

        mPresenter.creditInfoLiveData?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.SUCCESS -> {
                    mCreditInfo = resource.data
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(qrCode: String) {
        mQRCode = qrCode
        doAsync {
            val bitmapQR = CodeEngine(context!!).createQR(qrCode ?: "")
            uiThread {
                layoutQR.show()

                imageQR.setImageBitmap(bitmapQR)

                buttonSave.setOnClickListener {
                    save(bitmapQR)
                    sendEvent("TRENDY_WASH_TOP_UP_CHANNEL", "SAVE", "QR")
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.show()
        layoutQR.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

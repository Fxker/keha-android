package com.sansiri.homeservice.ui.meter.water.detail

import com.sansiri.homeservice.data.repository.meter.SmartMeterRepository
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.toISO8601
import java.util.*

class SmartWaterMeterDetailPresenter(val repository: SmartMeterRepository) : BasePresenterImpl<SmartWaterMeterDetailContract.View>(), SmartWaterMeterDetailContract.Presenter {
    private var mType: String? = null
    private var mUnitId: String? = null
    private var mSelectedDate: Calendar = Calendar.getInstance()

    override fun init(type: String?, unitId: String?, initDate: Calendar?) {
        this.mType = type
        this.mUnitId = unitId
        initDate?.let {  this.mSelectedDate = it }
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        mView?.attachHistoryObserver(
                repository.getSmartWaterMeterHistory(mUnitId ?: "", "default", mType ?: "", mSelectedDate.time.toISO8601())
        )
    }

    override fun destroy() {
    }

    override fun requestDatePicker() {
        if (mType == SmartWaterMeterOverAll.Summarize.MONTH) {
            mView?.showMonthPicker(mSelectedDate)
        } else {
            mView?.showDatePicker(mSelectedDate)
        }
    }

    override fun fetchFromSelectedDate(calendar: Calendar) {
        mSelectedDate = calendar
        mView?.showSelectedDate(calendar)
        fetch()
    }
}
package com.appy.android.code.domain.model.command.toggleswitch

import com.appy.android.code.data.rest.form.AppyForm

internal class GetToggleSwitchsForm(
        internal val homeId: String
) : AppyForm()
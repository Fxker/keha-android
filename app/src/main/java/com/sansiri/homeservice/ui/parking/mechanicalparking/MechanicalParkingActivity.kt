package com.sansiri.homeservice.ui.parking.mechanicalparking

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.api.ParkingQueue
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.scanner.QRScannerActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_mechanical_parking.*
import java.util.concurrent.TimeUnit

class MechanicalParkingActivity : BaseActivity<MechanicalParkingContract.View, MechanicalParkingContract.Presenter>(), MechanicalParkingContract.View {
    override var mPresenter: MechanicalParkingContract.Presenter = MechanicalParkingPresenter()
    private lateinit var prefs: SharedPreferences

    companion object {
        val TITLE = "TITLE"
        val PROJECT_ID = "PROJECT_ID"

        fun start(activity: Activity, projectId: String, title: String?) {
            val intent = Intent(activity, MechanicalParkingActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(PROJECT_ID, projectId)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mechanical_parking)

        setBackToolbar(intent.getStringExtra(TITLE) ?: "")

        prefs = PreferenceHelper.defaultPrefs(this)
        mPresenter.mQueueId = prefs[PreferenceHelper.MECHANICAL_PARKING, ""] ?: ""

        mPresenter.start()

        val projectId = intent?.getStringExtra(PROJECT_ID)
        if (projectId != null) {
            mPresenter.attachParkingQueue(projectId)
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MECHANICAL_PARKING")
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == QRScannerActivity.CODE) {
            mPresenter.processQRScanning(data?.getStringExtra(QRScannerActivity.DATA))
        }
    }

    override fun showSummary(queue: List<ParkingQueue>) {
        textQueue.text = queue.size.toString()

        var totalTime = 0
        queue.forEach {
            totalTime += it.estimateTime
        }

        val lastQueue = queue.maxBy {
            it.estimateTime
        }
        if (lastQueue != null) {
            showTime(lastQueue.estimateTime)
        } else {
            showTime(0)
        }

    }

    private fun showTime(time: Int) {
        textTime.text = "${time} ${getString(R.string.min)}"
    }

    override fun enableQRButton() {
        buttonQR.isClickable = true
        buttonQR.setCardBackgroundColor(resources.getColor(R.color.colorAccent))
        buttonQR.setOnClickListener {
            sendEvent("MECHANICAL_PARKING", "CLICK", "SCAN_QR")
            QRScannerActivity.start(this)
        }
    }

    override fun showCardStatus(myQueue: ParkingQueue) {
        textQueueDescription.text = getString(R.string.queue)
        textTimeDescription.text = getString(R.string.your_eta)
        cardStatus.show()
        textNo.text = "Slot no. ${myQueue.lotNo}"
        when (myQueue.processStatus) {
            "Queued" -> {
                textStatus.text = getString(R.string.waiting_in_queue)
                imageLogo.tintImage(resources.getColor(R.color.colorCancel))
            }
            "Processing" -> {
                textStatus.text = getString(R.string.processing)
                imageLogo.tintImage(resources.getColor(R.color.colorCancel))
            }
            "Ready" -> {
                textStatus.text = getString(R.string.your_car_has_arrived)
                imageLogo.tintImage(resources.getColor(R.color.colorDone))
            }
            "Done", "Cancel" -> mPresenter.reset()
        }
    }

    override fun hideCardStatus() {
        mPresenter.invokeSummary()
        textQueueDescription.text = getString(R.string.total_queue)
        textTimeDescription.text = getString(R.string.total_eta)
        cardStatus.invisible()
    }

    override fun showMyQueueETA(queueNo: String, eta: Int) {
        textQueue.text = queueNo
        showTime(eta)
    }

    override fun alert(timeToAlert: Int) {
        NotificationHelper(this, 23).create(
                "Mechanical Parking",
                getString(R.string.your_car_has_arrived),
                resources.getColor(R.color.colorDone),
                Feature.MECHANICAL_PARKING).schedule(TimeUnit.MINUTES.toMillis(timeToAlert.toLong()))
    }

    override fun save(queueId: String) {
        prefs[PreferenceHelper.MECHANICAL_PARKING] = queueId
    }

    override fun clearSave() {
        prefs[PreferenceHelper.MECHANICAL_PARKING] = null
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

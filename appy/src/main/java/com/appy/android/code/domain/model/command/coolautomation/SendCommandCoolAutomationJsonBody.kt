package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
internal class SendCommandCoolAutomationJsonBody : BaseCommandJsonBody<String>() {
}
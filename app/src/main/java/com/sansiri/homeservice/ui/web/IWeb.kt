package com.sansiri.homeservice.ui.web

interface IWeb {
    fun initWebView(url: String, addTokenWithRequest: Boolean)
}
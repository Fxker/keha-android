package com.sansiri.homeservice.ui.projectlist

import android.content.res.Resources
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.UnitInfo
import com.sansiri.homeservice.model.menu.Menu

import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.withDelay
import kotlinx.android.synthetic.main.view_project_list.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ProjectListAdapter(val itemClick: (home: Hut) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<ProjectListAdapter.ViewHolder>() {
    private var mData = mutableListOf<Hut>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }
    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.view_project_list)
        return ViewHolder(view, itemClick)
    }


    fun setData(units: List<Hut>) {
        mData.clear()
        mData.addAll(units)
        notifyDataSetChanged()
    }

    fun setDataDelay(data: List<Hut>) {
        mData.clear()
        data.forEach {
            {
                mData.add(it)
                notifyItemInserted(mData.size - 1)
            }.withDelay(100)
        }
    }

    class ViewHolder(val view: View, val itemClick: (home: Hut) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(home: Hut) {
            with(view) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    card.preventCornerOverlap = false
                }
                layout.setOnClickListener {
                    itemClick(home)
                }
                textAddress.text = home.address
                textProject.text = home.title
                if (home.image.isNotEmpty()) {
                    Glide.with(context).load(home.image).placeholder(R.drawable.placeholder_project).transition(DrawableTransitionOptions.withCrossFade()).into(imageProject)
                }
            }
        }
    }

}
package com.sansiri.homeservice.data.repository.omise

import androidx.lifecycle.MutableLiveData
import co.omise.android.Client
import co.omise.android.TokenRequest
import co.omise.android.TokenRequestListener
import co.omise.android.models.Token
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.util.showHttpError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class OmiseRepositoryImpl(val omiseKey: String) : OmiseRepository {
    /**
     * @return id token
     */
    override fun verifyCard(cardNumber: String, cardName: String, expirationMonth: Int, expirationYear: Int, securityCode: String, onSuccess: (String) -> Unit, onError: (Throwable) -> Unit) {
        val data = MutableLiveData<Resource<String>>()

        val client = Client(omiseKey)
        val request = TokenRequest().apply {
            this.number = cardNumber
            this.name = cardName
            this.expirationMonth = expirationMonth ?: 0
            this.expirationYear = expirationYear ?: 0
            this.securityCode = securityCode
        }

        client.send(request, object : TokenRequestListener {
            override fun onTokenRequestSucceed(request: TokenRequest, token: Token) {
                val tokenId = token.id
                onSuccess(tokenId)
            }

            override fun onTokenRequestFailed(request: TokenRequest, e: Throwable) {
                onError(e)
                e.printStackTrace()
            }
        })
    }
}
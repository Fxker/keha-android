package com.sansiri.homeservice.ui.myaccount_v2.invoice

import android.view.View
import com.sansiri.homeservice.model.api.myaccount.invoce.InvoiceItem
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_invoice_item.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class InvoiceItemViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(invoiceItem: InvoiceItem) {
        with(view) {
            textTitle.text = invoiceItem.loanTypeDescription
            textAmount.text = invoiceItem.amount.toCurrencyFormat()
        }
    }
}
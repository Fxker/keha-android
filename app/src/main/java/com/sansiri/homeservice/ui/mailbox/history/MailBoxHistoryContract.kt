package com.sansiri.homeservice.ui.mailbox.history

import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/12/17.
 */
interface MailBoxHistoryContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(unitObjectId: String, page: Int)
    }

    interface View : BaseView {
        fun bindData(list: List<MailBox>)
        fun showLoading()
        fun hideLoading()
        fun showNoData()
    }
}
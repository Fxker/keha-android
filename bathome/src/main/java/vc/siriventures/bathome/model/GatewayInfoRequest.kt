package vc.siriventures.bathome.model

import com.google.gson.annotations.SerializedName

data class GatewayInfoRequest(
        val projectCode: String? = null,
        val userName: String? = null,
        val password: String? = null
)
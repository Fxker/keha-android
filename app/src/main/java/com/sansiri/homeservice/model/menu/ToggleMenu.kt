package com.sansiri.homeservice.model.menu

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by oakraw on 10/3/2017 AD.
 */
class ToggleMenu(
        override val id: String?,
        override val title: String? = null,
        override val icon: String? = null,
        var isChecked: Boolean
) : Menu, Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(title)
        writeString(icon)
        writeInt((if (isChecked) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ToggleMenu> = object : Parcelable.Creator<ToggleMenu> {
            override fun createFromParcel(source: Parcel): ToggleMenu = ToggleMenu(source)
            override fun newArray(size: Int): Array<ToggleMenu?> = arrayOfNulls(size)
        }
    }
}

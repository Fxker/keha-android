package com.sansiri.homeservice.data.network.firebase

import android.app.Activity
import android.provider.Settings
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.util.observe


/**
 * Created by sansiri on 11/8/17.
 */
object FirebaseAuthManager {
    private var mAuth: FirebaseAuth? = null

    init {
        mAuth = FirebaseAuth.getInstance()
    }

    fun getToken(onSuccess: (String) -> Unit, onFail: (Exception) -> Unit) {
        if (mAuth?.currentUser == null) {
            onFail(Exception())
        }

        mAuth?.currentUser?.getIdToken(true)?.addOnCompleteListener {
            if (it.isSuccessful) {
                it.result?.token?.let {
                    onSuccess(it)
                }
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun signUp(email: String, password: String, onSuccess: () -> Unit, onFail: (Exception) -> Unit) {
        mAuth?.createUserWithEmailAndPassword(email, password)?.addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun isSignIn(): Boolean {
        return mAuth?.currentUser != null
    }

    fun signIn(email: String, password: String, onSuccess: () -> Unit, onFail: (Exception) -> Unit) {
        mAuth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun facebookSignIn(token: String, onSuccess: () -> Unit, onFail: (Exception) -> Unit) {
        val credential = FacebookAuthProvider.getCredential(token)
        mAuth?.signInWithCredential(credential)?.addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun googleSignIn(token: String, onSuccess: () -> Unit, onFail: (Exception) -> Unit) {
        val credential = GoogleAuthProvider.getCredential(token, null)
        mAuth?.signInWithCredential(credential)?.addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun resetPassword(email: String, onSuccess: () -> Unit, onFail: (Exception) -> Unit) {
        mAuth?.sendPasswordResetEmail(email)?.addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }?.addOnFailureListener {
            onFail(it)
        }
    }

    fun signOut(activity: Activity, callback: () -> Unit) {
        val deviceId = Settings.Secure.getString(activity?.contentResolver, Settings.Secure.ANDROID_ID)
        ApiRepositoryProvider.provideApiAuthRepository {
            it?.unregisterNotification(deviceId ?: "")?.observe()?.subscribe({
                doSignOut(activity)
                callback()
            }) {
                doSignOut(activity)
                callback()
            }
        }
    }

    private fun doSignOut(activity: Activity) {
        mAuth?.signOut()
        PreferenceHelper.clear(activity)
        RuntimeCache.clear()
        LoginManager.getInstance().logOut()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.google_server_client_id))
                .requestEmail()
                .build()

        GoogleSignIn.getClient(activity, gso).signOut()
    }
}
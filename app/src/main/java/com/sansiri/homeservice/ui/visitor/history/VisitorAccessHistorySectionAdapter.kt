package com.sansiri.homeservice.ui.visitor.history

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.VisitorAccessHistory
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by sansiri on 10/12/17.
 */
class VisitorAccessHistorySectionAdapter(val title: String, val mData: List<VisitorAccessHistory>, val itemClick: (VisitorAccessHistory) -> Unit) : StatelessSection(SectionParameters.Builder(R.layout.view_summary_card)
        .headerResourceId(R.layout.view_mailbox_date)
        .build()) {

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return VisitorAccessHistoryViewHolder(view, itemClick)
    }

    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is VisitorAccessHistoryViewHolder) {
            holder.bind(mData[position])
        }
    }


}
package com.sansiri.homeservice.ui.more

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

/**
 * Created by sansiri on 10/24/17.
 */
class MorePresenter : BasePresenterImpl<MoreContract.View>(), MoreContract.Presenter {
    override fun start() {

    }

    override fun destroy() {
    }

    override fun selectMenu(menu: Menu) {
        when (menu.id) {
            Feature.PROFILE -> mView?.launchProfile()
            Feature.LOUNGE -> mView?.launchSansiriLounge()
            Feature.LANGUAGE -> mView?.launchLanguage()
            Feature.NOTIFICATION_SETTINGS -> mView?.launchNotificationSettings()
            Feature.CONNECT_GOOGLE_ASSISTANT -> mView?.launchGoogleAssistant()
            Feature.SIGN_OUT -> {
                mView?.signOut()
            }
        }
    }

    override fun unregisterNotification(deviceId: String?, callback: () -> Unit) {
        ApiRepositoryProvider.provideApiAuthRepository {
            it?.unregisterNotification(deviceId ?: "")?.observe()?.subscribe({
                callback()
            }) {
                callback()
            }
        }
    }
}

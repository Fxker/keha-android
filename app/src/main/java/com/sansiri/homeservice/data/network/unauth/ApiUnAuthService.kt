package com.sansiri.homeservice.data.network.unauth

import com.sansiri.homeservice.data.network.RetrofitFactory
import com.sansiri.homeservice.model.api.*
import com.sansiri.homeservice.model.api.announcement.*
import com.sansiri.homeservice.model.api.partner.aqi.Air
import com.sansiri.homeservice.model.menu.HomeCareMenu
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface ApiUnAuthService {
    @GET("customers/verify/citizen/{citizenId_or_passportNo}")
    fun verifyCitizenID(@Path("citizenId_or_passportNo") citizenID: String): Observable<CitizenIDVerifyResult>

    @GET("projects/{projectId}/directories/phones")
    fun getPhoneDirectory(@Header("Accept-Language") language: String, @Path("projectId") projectId: String): Observable<ListResponse<PhoneDirectoryGroup>>

    @GET("projects/{projectId}/partners/sansiri/progresses/building")
    fun getProjectProgress(@Header("Accept-Language") language: String, @Path("projectId") projectId: String): Observable<ProjectProgress>

    @GET("announcements/family")
    fun getFamilyAnnouncement(@Header("Accept-Language") language: String, @Query("offset") offSet: Int, @Query("limit") limit: Int): Observable<ListResponse<FamilyAnnouncement>>

    @GET("partners/sansiri/homecare/type")
    fun getHomeCareType(@Header("Accept-Language") language: String): Observable<List<HomeCareMenu>>

    @GET("announcements/{objectId}")
    fun getAnnouncementById(@Header("Accept-Language") language: String, @Path("objectId") objectId: String): Observable<ProjectAnnouncement>

    @PUT("devices/{deviceId}/notification/register")
    fun registerNotification(@Path("deviceId") deviceId: String, @Body registerNotificationRequest: RegisterNotificationRequest): Observable<ResponseBody>

    @GET
    fun downloadFileWithDynamicUrl(@Url fileUrl: String): Observable<ResponseBody>

    @GET("projects/{projectId}/partners/weather/aqi")
    fun getAQI(@Header("Accept-Language") language: String, @Path("projectId") projectId: String): Observable<Air>

    @PUT
    fun uploadFile(@Url url: String, @Body body: RequestBody): Observable<ResponseBody>

    companion object Factory {
        fun create(): ApiUnAuthService {
            return RetrofitFactory.instance.create(ApiUnAuthService::class.java)
        }
    }
}
package com.sansiri.homeservice.ui.epayment.barcode

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface BarcodeContract {
    interface Presenter : BasePresenter<View> {
    }

    interface View : BaseView {
    }
}
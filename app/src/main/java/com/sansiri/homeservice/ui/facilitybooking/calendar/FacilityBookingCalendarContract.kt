package com.sansiri.homeservice.ui.facilitybooking.calendar

import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.Reservation
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import org.joda.time.DateTime
import java.util.*

/**
 * Created by sansiri on 10/16/17.
 */
interface FacilityBookingCalendarContract {
    interface Presenter : BasePresenter<View> {
        var currentTimeline: List<TimesItem>?
        fun init(projectId: String, facility: FacilityBooking)
        fun fetchData()
        fun selectBookingDate(date: String)
    }

    interface View : BaseView {
        fun initTimeLine(startHour: Int, endHour:Int)
        fun showDescription(text: String)
        fun showLoading()
        fun hideLoading()
        fun showUnavailable()
        fun hideUnavailable()
        fun book(startTime: Calendar, endTime: Calendar)
        fun clearTimeLine()
        fun addSlotToTimeLine(startBooking: Calendar, disable: Boolean = false, nextAvailableSlot: Date? = null)
        fun getCalendarDate(): String?
        fun bookMine(id: String, startTime: Calendar, endTime: Calendar, note: String)
        fun setCalendarDate(date: String)
        fun setReservation(value: Reservation?)
    }
}
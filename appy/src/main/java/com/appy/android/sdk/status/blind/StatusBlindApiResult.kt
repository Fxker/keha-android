package com.appy.android.sdk.status.blind

import com.appy.android.code.base.data.rest.result.AppyApiResult

/**
 * Created by ssa-dev-04 on 19/1/2561.
 */
class StatusBlindApiResult : AppyApiResult<List<APStatusBlind>>()
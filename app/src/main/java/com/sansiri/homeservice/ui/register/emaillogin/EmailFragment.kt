package com.sansiri.homeservice.ui.register.emaillogin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.microsoft.appcenter.analytics.Analytics
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.ui.register.forgotpassword.ForgotPasswordActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.removeSpace
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_email.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class EmailFragment : BaseFragment<EmailContract.View, EmailContract.Presenter>(), EmailContract.View {

    override var mPresenter: EmailContract.Presenter = EmailPresenter()
    private var isSignUp: Boolean = false

    companion object {
        fun newInstance(): EmailFragment {
            val fragment = EmailFragment()

            val bundle = Bundle()
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonForgotPassword.hide()

        val remoteConfig = FirebaseRemoteConfigManager()
        remoteConfig.fetch {
            if (it.getBoolean(FirebaseRemoteConfigManager.FORGOT_PASSWORD)) {
                buttonForgotPassword.show()
            } else {
                buttonForgotPassword.hide()
            }
        }

        if (BuildConfig.DEBUG) {
            editUsername.setText("tas2@mail.com")
            editPassword.setText("123456789")
        }

        buttonSignIn.setOnClickListener {
            mPresenter.validateAndSignIn(editUsername.text.toString().removeSpace(), editPassword.text.toString())
        }

        buttonForgotPassword.setOnClickListener {
            ForgotPasswordActivity.start(activity!!, editUsername.text.toString())
        }
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("EMAIL_SIGN_IN")
    }

    override fun usernameError(message: String) {
        editUsername.error = message
    }

    override fun passwordError(message: String) {
        editPassword.error = message
    }

    override fun showLoading() {
        editUsername.isEnabled = false
        editPassword.isEnabled = false
        progressBar.show()
        buttonSignIn.hide()
    }

    override fun getStringFromRes(res: Int): String {
        return getString(res)
    }

    override fun hideLoading() {
        editUsername.isEnabled = true
        editPassword.isEnabled = true
        progressBar.hide()
        buttonSignIn.show()
    }

    override fun showError(email: String, message: String) {
        AnalyticProvider.sendError("EMAIL", email, null, message)
        showError(message)
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }

    override fun nextScreen() {
        val registerActivity = activity as RegisterActivity
        registerActivity.launchMainPage()
    }

    override fun signOut() {
        FirebaseAuthManager.signOut(activity!!){}
    }
}
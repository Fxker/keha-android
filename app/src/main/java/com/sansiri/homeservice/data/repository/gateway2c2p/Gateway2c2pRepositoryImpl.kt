package com.sansiri.homeservice.data.repository.gateway2c2p

import android.content.Context
import android.util.JsonReader
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.Observer
import com.ccpp.pgw.sdk.android.builder.CardTokenPaymentBuilder
import com.ccpp.pgw.sdk.android.core.PGWSDK
import com.ccpp.pgw.sdk.android.enums.APIEnvironment
import com.sansiri.homeservice.data.network.auth.Gateway2c2pService
import com.ccpp.pgw.sdk.android.builder.CreditCardPaymentBuilder
import com.ccpp.pgw.sdk.android.model.payment.CreditCardPayment
import com.ccpp.pgw.sdk.android.builder.TransactionRequestBuilder
import com.ccpp.pgw.sdk.android.model.api.request.TransactionRequest
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.network.networkCall
import kotlinx.coroutines.runBlocking
import com.ccpp.pgw.sdk.android.enums.APIResponseCode
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.ccpp.pgw.sdk.android.callback.TransactionResultCallback
import com.sansiri.homeservice.data.database.creditcard.SavingCreditCardDao
import com.sansiri.homeservice.data.network.awaitResult
import com.sansiri.homeservice.data.network.getOrThrow
import com.sansiri.homeservice.model.api.payment.Transaction2C2PDetail
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class Gateway2c2pRepositoryImpl(val context: Context, val service: Gateway2c2pService, val savingCreditCardDao: SavingCreditCardDao) : Gateway2c2pRepository {

    var paymentToken = "roZG9I1hk/GYjNt+BYPYbzXcA++LZnSaJJXrgH1bseJG+t2yu9d1TfBulwTkLMaqB96uFjpKXR2nztDIM/QRSOMU7P/mFIl9sUCDsROeOVw/3Lr0y+H38X8m1ZfW//Dq"
    private var mCreditCardPayment: CreditCardPayment? = null
    var listener: Gateway2c2pRepository.Listener? = null

    override fun init(merchantId: String, listener: Gateway2c2pRepository.Listener) {
        this.listener = listener
        PGWSDK.builder(context)
                .setMerchantID(merchantId)
                .setAPIEnvironment(APIEnvironment.SANDBOX)
                .init()
    }

    override fun createCreditCard(cardName: String, cardNumber: String, ccv: String, expiryMonth: Int, expiryYear: Int, isRemember: Boolean, email: String?) {
        mCreditCardPayment = CreditCardPaymentBuilder(cardNumber)
                .setName(cardName)
                .setExpiryMonth(expiryMonth)
                .setExpiryYear(expiryYear)
                .setSecurityCode(ccv)
                .setEmail(email)
                .setStoreCard(isRemember)
                .build()
    }

    override fun createCreditCard(cardToken: String, ccv: String, email: String?) {
        mCreditCardPayment = CardTokenPaymentBuilder(cardToken)
                .setSecurityCode(ccv)
//                .setEmail(email)
                .build()
    }

    override fun getSavingCreditCards() = savingCreditCardDao.getSavingCreditCards()

    @WorkerThread
    override suspend fun saveCard(creditCard: SavingCreditCard) = savingCreditCardDao.insertCreditCard(creditCard)

    @WorkerThread
    override suspend fun deleteCard(cardToken: String) = savingCreditCardDao.deleteCreditCard(cardToken)

    override fun inquiryTransaction(transactionId: String) = networkCall<Transaction2C2PDetail>() {
        client = service.inquiryTransaction(transactionId)
    }

    override fun execute(token: String) {
        if (mCreditCardPayment == null) {
            listener?.onFailure(Exception("Please be sure your credit card information was correct"))
            return
        }

        val transactionRequest = TransactionRequestBuilder(token)
                .withCreditCardPayment(mCreditCardPayment)
                .build()

        PGWSDK.getInstance().proceedTransaction(transactionRequest, object : TransactionResultCallback {

            override fun onResponse(response: TransactionResultResponse) {
                when (response.responseCode) {
                    APIResponseCode.TRANSACTION_AUTHENTICATE -> {
                        listener?.onRequestWeb(response.redirectUrl)
                    }
                    APIResponseCode.TRANSACTION_COMPLETED -> {
                        val transactionID = response.transactionID
                        listener?.onTransactionSuccess(transactionID)
                    }
                    else -> {
                        listener?.onFailure(Exception(response.responseDescription))
                    }
                }
            }

            override fun onFailure(error: Throwable) {
                Log.d("", "")
                listener?.onFailure(error)
                //Get error response and display error
            }
        })
    }

}
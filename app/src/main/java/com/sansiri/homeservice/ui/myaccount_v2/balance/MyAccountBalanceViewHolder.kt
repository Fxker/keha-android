package com.sansiri.homeservice.ui.myaccount_v2.balance

import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.myaccount.PaymentHistory
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_card.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class MyAccountBalanceViewHolder(val view: View, val itemClick: (PaymentHistory) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(invoice: PaymentHistory, color: Int) {
        with(view) {
            imageIcon.setImageResource(R.drawable.ic_my_account)
            if (invoice.amount != null && invoice.amount < 0) {
                textTitle.setTextColor(context.resources.getColor(color))
                textTitle.text = invoice.amount.toCurrencyFormat()
            } else {
                textTitle.setTextColor(context.resources.getColor(R.color.colorDone))
                textTitle.text = "+${invoice.amount?.toCurrencyFormat()}"
            }
            textTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_headline2))
            textSubtitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_body2))
            textType.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_body2))


            textType.text = invoice._date?.report()
            textSubtitle.text = invoice.description

            root.isClickable = false
        }
    }
}
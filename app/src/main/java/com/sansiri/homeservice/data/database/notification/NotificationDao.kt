package com.sansiri.homeservice.data.database.notification

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sansiri.homeservice.model.api.notification.PushNotification
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import io.reactivex.Flowable

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification")
    fun getNotifications(): LiveData<List<PushNotification>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNotification(notification: PushNotification)

    @Query("DELETE FROM notification WHERE type = :featureId AND data_unitId = :unitId")
    suspend fun deleteNotification(featureId: String, unitId: String)

    @Query("UPDATE notification SET isRead = 1 WHERE _id = :notificationId")
    suspend fun markAsRead(notificationId: Int)

    @Query("UPDATE notification SET isRead = 1 WHERE type = :featureId AND data_unitId = :unitId")
    suspend fun markAsRead(featureId: String, unitId: String)

    @Query("DELETE FROM notification")
    fun deleteAll()
}
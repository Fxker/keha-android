package com.sansiri.homeservice.ui.automation.appy.remotecontrol

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.animation.OvershootInterpolator
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.*
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_list.*

class HomeAutomationRemoteControlActivity : BaseActivity<HomeAutomationRemoteControlContract.View, HomeAutomationRemoteControlContract.Presenter>(), HomeAutomationRemoteControlContract.View {
    override var mPresenter: HomeAutomationRemoteControlContract.Presenter = HomeAutomationRemoteControlPresenter()

    companion object {
        const val CONTROL = "CONTROL"
        const val PROJECT_ID = "PROJECT_ID"
        const val HOME_ID = "HOME_ID"
        fun start(activity: Activity, control: APRemoteControl, projectId: String, homeId: String) {
            val intent = Intent(activity, HomeAutomationRemoteControlActivity::class.java)
            intent.putExtra(CONTROL, control)
            intent.putExtra(PROJECT_ID, projectId)
            intent.putExtra(HOME_ID, homeId)
            activity.startActivity(intent)
        }
    }

    val mAdapter = RemoteControlAdapter() { actionRemoteControl ->
        mPresenter.action(actionRemoteControl)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val control = intent.getParcelableExtra<APRemoteControl>(CONTROL)
        val homeId = intent.getStringExtra(HOME_ID)
        val projectId = intent.getStringExtra(PROJECT_ID)

        if (control != null) {
            setBackToolbar(control.title ?: "")

            mPresenter.init(control, homeId, projectId)
            mPresenter.start()

            with(list) {
                adapter = mAdapter
                itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
                itemAnimator?.addDuration = 400
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(text: String) {
        snack(text)
    }

    override fun showError(message: String) {
        snackError(message)
    }

    override fun bindData(control: List<APActionRemoteControl>) {
        mAdapter.setData(control)
    }
}

package com.sansiri.homeservice.ui.meter.electric

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.github.mikephil.charting.data.Entry
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterDetail
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterOverAll
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.meter.LineChartHelper
import com.sansiri.homeservice.ui.meter.electric.detail.SmartElectricMeterDetailActivity
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_smart_electric.*
import kotlinx.android.synthetic.main.view_smart_electric_meter_history_item.view.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.koin.androidx.scope.currentScope


class SmartElectricMeterActivity : BaseV2Activity<SmartElectricMeterContract.View, SmartElectricMeterContract.Presenter>(), SmartElectricMeterContract.View {
    private var progressDailog: ProgressDialog? = null
    override val mPresenter: SmartElectricMeterContract.Presenter by currentScope.inject()
    private var mUnitId: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val TITLE = "TITLE"

        fun start(activity: Activity, unitId: String, title: String?) {
            val intent = Intent(activity, SmartElectricMeterActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smart_electric)

        mUnitId = intent.getStringExtra(SmartParkingActivity.UNIT_ID)
        val title = intent.getStringExtra(SmartParkingActivity.TITLE)

        mPresenter.init(mUnitId ?: "")
        mPresenter.start()

        swipeRefresh.setOnRefreshListener {
            mPresenter.fetch()
        }

        buttonBack.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SMART_ELECTRIC_METER")
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }


    override fun attachFetchingObserver(meterLatestDetail: MutableLiveData<Resource<SmartElectricMeterDetail>>, meterOverAll: MutableLiveData<Resource<SmartElectricMeterOverAll>>) {
        meterLatestDetail.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    progressBar.show()
                }
                Resource.SUCCESS -> {
                    progressBar.hide()
                    swipeRefresh.isRefreshing = false
                    resource.data?.let {
                        textNowAmount.text = it.activePowerDisplay
                    }
                }
                Resource.ERROR -> {
                    progressBar.hide()
                    showError(resource.message ?: "Error")
                }
            }
        })

        meterOverAll.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {

                    if (resource.data != null) {
                        progressBar.show()
                        resource.data?.let { bindData(it) }
                    } else {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    swipeRefresh.isRefreshing = false
                    resource.data?.let {
                        bindData(it)
                    }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })

    }

    fun bindData(overall: SmartElectricMeterOverAll) {

        textTodayCost.text = overall.today?.costEstimateDisplay
        textTodayAmount.text = overall.today?.killWattDisplay

        containerHistory.removeAllViews()
        overall.histories?.forEach { history ->
            val view = LayoutInflater.from(this@SmartElectricMeterActivity).inflate(R.layout.view_smart_electric_meter_history_item, containerHistory, false)
            view.textTitle.text = history.title
            view.textSubTitle.text = history.displayKiloWattHour
            view.textCost.text = history.displayCostEstimate
            view.setOnClickListener {
                SmartElectricMeterDetailActivity.start(this, history, mUnitId ?: "")
            }
            containerHistory.addView(view)

        }

        renderChart(overall.graph)
    }

    private fun renderChart(graphData: List<SmartElectricMeterDetail>?) {
        if (graphData.isNullOrEmpty()) {
            chart.hide()
            return
        } else {
            chart.show()
        }

        val entries = graphData.map { data ->
            Entry(data._timestamp?.hours?.toFloat() ?: 0f, data.kiloWattHourUsage.toFloat())
        }

        LineChartHelper.LineChartBuilder(this, chart, R.layout.view_smart_electric_graph_value_preview)
                .createDataSet(
                        entries,
                        "",
                        resources.getColor(R.color.colorYellowLineChart),
                        resources.getDrawable(R.drawable.graph_yellow_fade),
                        resources.getColor(R.color.colorElectric)
                ).build()
    }

    override fun showLoading() {
        progressDailog = indeterminateProgressDialog(getString(R.string.loading))
    }

    override fun hideLoading() {
        progressDailog?.dismiss()
    }

    override fun showError(message: String) {
        snackError(message)
    }
}

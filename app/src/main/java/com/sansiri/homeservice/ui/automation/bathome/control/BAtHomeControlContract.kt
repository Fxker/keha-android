package com.sansiri.homeservice.ui.automation.bathome.control

import android.app.Activity
import com.sansiri.homeservice.model.automation.BAtHomeAutomation
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import vc.siriventures.bathome.model.BAtHomeComponent

interface BAtHomeControlContract {
    interface Presenter : BasePresenter<View> {
        fun init(activity: Activity, username: String?, password: String?, projectId: String?, type: String?)
        fun fetch(type: String?)
        fun action(homeAutomation: BAtHomeAutomation, command: Any)
    }

    interface View : BaseView {
        fun bindData(controls: List<BAtHomeAutomation>)
        fun showMessage(text: String)
        fun showLoading()
        fun hideLoading()
        fun showNoDevice()
        fun notifyList()
    }
}
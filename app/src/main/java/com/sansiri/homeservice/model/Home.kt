package com.sansiri.homeservice.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.appy.android.sdk.room.APRoom
import com.sansiri.homeservice.model.api.AppyConfig
import com.sansiri.homeservice.model.api.TodaySummary
import com.sansiri.homeservice.model.api.UnitInfo
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.announcement.ProjectAnnouncement
import com.sansiri.homeservice.model.api.announcement.ProjectAnnouncementDetail
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Section
import kotlinx.android.parcel.Parcelize

/**
 * Created by oakraw on 10/3/2017 AD.
 */
@Parcelize
data class Home(
        var unitId: String = "",
        var title: String = "",
        var icon: String = "",
        var image: String = "",
        var address: String = "",
        var projectId: String = "",
        var lat: Double = 0.0,
        var lng: Double = 0.0,
        var homeAutomationGateway: UnitInfo.HomeAutomationGateway? = null,
        var isSupportHomeUpgrade: Boolean = false,
        var homeUpgradeRequestId: String = "",
        var isTransfer: Boolean = false,
        var section: List<Section>? = null,
        var summary: List<TodaySummary>? = null,
        var announcement: List<ProjectAnnouncement>? = null
) : Parcelable {
    fun toHut() = Hut(
            unitId,
            title,
            icon,
            image,
            address,
            projectId,
            lat,
            lng

    )
}
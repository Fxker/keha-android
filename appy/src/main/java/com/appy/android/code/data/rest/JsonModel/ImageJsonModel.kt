package com.appy.android.code.data.rest.JsonModel

import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 7/11/2560.
 */
class ImageJsonModel {
    @SerializedName("original")
    var original: String? = null
    @SerializedName("ah_xxlarge")
    var xxLarge: String? = null
    @SerializedName("ah_xlarge")
    var xLarge: String? = null
    @SerializedName("ah_large")
    var large: String? = null
    @SerializedName("ah_medium")
    var medium: String? = null
    @SerializedName("ah_small")
    var small: String? = null
    @SerializedName("ah_thumb")
    var thumb: String? = null
}
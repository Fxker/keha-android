package com.sansiri.homeservice.model.api.notification

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "notification_data")
data class PushNotificationData(
        @PrimaryKey
        val objectId: String = "asa",
        val unitId: String? = null,
        val projectId: String? = null,
        val accessoryId: String? = null,
        val profileImage: String? = null,
        val planId: String? = null
)
package vc.siriventures.bathome.model

import com.google.gson.annotations.SerializedName

data class GatewayInfoResponse(

	@SerializedName("ErrorDescription")
	val errorDescription: String? = null,

	@SerializedName("public_url")
	val publicUrl: String? = null,

	@SerializedName("private_url")
	val privateUrl: String? = null,

	@SerializedName("ErrorCode")
	val errorCode: String? = null
)
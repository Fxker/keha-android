package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.coolautomation.APCoolAutomationParameter
import com.appy.android.sdk.control.coolautomation.CoolAutomationCommand

/**
 * Created by ssa-dev-4 on 6/10/2560.
 */
internal class SendCommandCoolAutomationParamForm(
        val homeId: String,
        val coolId: String,
        val command: CoolAutomationCommand,
        val paramCommand: APCoolAutomationParameter
) : AppyForm() {
    val mSendCommandCoolAutomationParamJsonBody: SendCommandCoolAutomationParamJsonBody = SendCommandCoolAutomationParamJsonBody()

    init {
        this.mSendCommandCoolAutomationParamJsonBody.command = command.action()
        this.mSendCommandCoolAutomationParamJsonBody.commandParam = paramCommand.action()
    }

}
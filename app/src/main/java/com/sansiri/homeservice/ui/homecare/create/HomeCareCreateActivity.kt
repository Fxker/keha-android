package com.sansiri.homeservice.ui.homecare.create

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.ui.homecare.create.category.HomeCareCategoryFragment
import com.sansiri.homeservice.ui.mailbox.MailBoxActivity
import com.sansiri.homeservice.util.replaceFragment
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.toolbar.*

class HomeCareCreateActivity : LocalizationActivity() {

    companion object {
        val CODE = 2250
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"

        fun start(activity: Activity, unitObjectId: String) {
            val intent = Intent(activity, HomeCareCreateActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            activity.startActivityForResult(intent, CODE)
        }
    }

    var unitObjectId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)
        setBackToolbar("")

        replaceFragment(HomeCareCategoryFragment.newInstance(), false)
        unitObjectId = intent?.getStringExtra(UNIT_OBJECT_ID)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        removeAllFragments()
        replaceFragment(HomeCareCategoryFragment.newInstance(), false)
        unitObjectId = intent?.getStringExtra(UNIT_OBJECT_ID)
    }

    fun setToolbarTitle(title: String) {
        textToolbar.text = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun removeAllFragments() {
        while (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

//    override fun onBackPressed() {
//        if (supportFragmentManager.backStackEntryCount > 0) {
//            super.onBackPressed()
//        /*} else {
//            AnalyticProvider(application as AppController).sendEvent("HOMECARE","CLICK", "HOMECARE_CREATE_CANCEL")
//            finishAffinity()
//        }*/
//    }

}

package com.sansiri.homeservice.model.api.bos

data class BosDetail (
    var zone: String? = null,
    var contactName: String? = null,
    var imageUrl: String? = null,
    var contactPhoneNo: String? = null,
    var typeId: String? = null,
    var detail: String? = null,
    var floor: String? = null,
    var building: String? = null
)

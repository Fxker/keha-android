package com.sansiri.homeservice.ui.homecare.history.detail

import android.content.res.Resources
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.px
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.view_photo_card.view.*

class HomeCareHistorySubItemAttachmentAdapter(val columnNo: Int): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mData: List<String>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.view_photo_card)
        val displayWidth = Resources.getSystem().displayMetrics.widthPixels
        val width = displayWidth / columnNo
        view.layoutParams.width = width
        return ThumbnailViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mData?.size ?: 0
    }

    override fun onBindViewHolder(thumbnailViewHolder: RecyclerView.ViewHolder, position: Int) {
        if (thumbnailViewHolder !is ThumbnailViewHolder) { return }
        mData?.get(position)?.let {
            thumbnailViewHolder.bind(it)
            thumbnailViewHolder.view.layoutClickable.setOnClickListener { view ->
                ImageViewer.Builder(view.context, mData)
                        .setStartPosition(position)
                        .show()
            }
        }
    }

}

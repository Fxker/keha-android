package com.sansiri.homeservice.ui.base

import android.content.Context
import android.os.Bundle
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.AppController
import com.sansiri.homeservice.data.network.AnalyticProvider


/**
 * Created by oakraw on 6/12/2017 AD.
 */
abstract class BaseV2Activity<in V : BaseView, T : BasePresenter<V>>: LocalizationActivity(), BaseView {
    protected abstract val mPresenter: T
    private var analyticProvider: AnalyticProvider? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attachView(this as V)

        val app = application as AppController
        app.guardRoute(this)
        analyticProvider = AnalyticProvider(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }


    override fun sendScreenView(screenName: String) {
        analyticProvider?.sendScreenView(screenName)
    }

    override fun sendEvent(category: String, action: String, label: String?, logVersion: String, vararg optionalProp: Pair<String, String>) {
        analyticProvider?.sendEvent(category, action, label, logVersion, optionalProp.toList())
    }
}
package com.sansiri.homeservice.ui.mailbox.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.google.zxing.MultiFormatWriter
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.model.api.chat.ChatMessage
import com.sansiri.homeservice.model.api.chat.Sender
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.chat.ChatActivity
import com.sansiri.homeservice.ui.mailbox.sandee.SandeeFragmentDialog
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_mail_box_detail.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

class MailBoxDetailActivity : BaseActivity<MailBoxDetailContract.View, MailBoxDetailContract.Presenter>(), MailBoxDetailContract.View, SandeeFragmentDialog.Listener {
    override var mPresenter: MailBoxDetailContract.Presenter = MailBoxDetailPresenter()
    private var mHome: Hut? = null
    private var mMailbox: MailBox? = null

    companion object {
        val MAILBOX = "MAILBOX"
        val HOME = "HOME"
        fun start(activity: Activity, home: Hut, mail: MailBox) {
            val intent = Intent(activity, MailBoxDetailActivity::class.java)
            intent.putExtra(MAILBOX, mail)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    override fun onAppointmentSelected(date: String, time: String) {
        val me = ChatMessage(ChatMessage.TYPE_TEXT, DateTime.now(DateTimeZone.UTC).toString(), Sender(Sender.TYPE_USER),
                String.format(getString(R.string.sandee_message), mMailbox?.objectId, time, date),
                null)

        val sender = ChatMessage(ChatMessage.TYPE_TEXT, DateTime.now(DateTimeZone.UTC).apply { plusSeconds(1) }.toString(), Sender(Sender.TYPE_STAFF), getString(R.string.sandee_response), null)
        mHome?.let { ChatActivity.start(this, it, getString(R.string.chat), arrayListOf(me, sender)) }

        try {
            val dateIso8601 = "$date $time".toDate("d MMM yyyy HH:mm")
            sendEvent("SANDEE_REQUEST", "SUBMIT", mMailbox?.objectId, "2", Pair("RequestDateTime", dateIso8601.toString()))
        } catch (e: Exception) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mail_box_detail)

        setBackToolbar(getString(R.string.mailbox))
        buttonRequest.setOnClickListener { showSandeeDialog() }

        mMailbox = intent.getParcelableExtra<MailBox>(MAILBOX)
        mHome = intent.getParcelableExtra(HOME)

        if (mMailbox != null) {
            bindData(mMailbox!!)
        } else {
            showError(getString(R.string.error_default_message))
        }
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MAIL_BOX_DETAIL")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun bindData(mail: MailBox) {
        textTitle.text = mail.to

        textID.text = mail.objectId
        textDate.text = mail._createdAt?.report()
        textType.text = mail.typeDisplayText
        textCategory.text = mail.categoryDisplayText
        textColor.text = mail.colorDisplayText
        textSize.text = mail.sizeDisplayText
        textDetail.text = mail.details

        if (mail.isSupportSandee) {
            buttonRequest.show()
        } else {
            buttonRequest.hide()
        }
    }

    override fun generateBarcode(data: String) {
        val bitmap = MultiFormatWriter().generateBarcode(data)
        barcode.setImageBitmap(bitmap)
    }

    override fun showSandeeDialog() {
        val frag = supportFragmentManager.findFragmentByTag("fragment_edit_name")
        if (frag != null) {
            supportFragmentManager.beginTransaction().remove(frag).commit()
        }
        SandeeFragmentDialog().show(supportFragmentManager, "fragment_edit_name")
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

package com.appy.android.code.domain.model.command.daikin

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.daikin.DaikinCommand
import com.appy.android.sdk.control.daikin.DaikinFanSpeedCommand

internal class SendCommandDaikinFanSpeedForm(
        val homeId: String,
        val daikinId: String,
        val commandValue: DaikinFanSpeedCommand
) : AppyForm() {
    val mSendCommandDaikinCommandValueJsonBody: SendCommandDaikinCommandValueJsonBody = SendCommandDaikinCommandValueJsonBody()

    init {
        this.mSendCommandDaikinCommandValueJsonBody.command = DaikinCommand.FAN_SPEED.action()
        this.mSendCommandDaikinCommandValueJsonBody.commandValue = commandValue.action()
    }
}
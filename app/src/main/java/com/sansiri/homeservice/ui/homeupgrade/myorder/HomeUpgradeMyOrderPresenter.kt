package com.sansiri.homeservice.ui.homeupgrade.myorder

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.ui.base.BasePresenterImpl

class HomeUpgradeMyOrderPresenter : BasePresenterImpl<HomeUpgradeMyOrderContract.View>(), HomeUpgradeMyOrderContract.Presenter {
    private lateinit var mHistory: HomeUpgradeHistory

    override fun init(history: HomeUpgradeHistory) {
        this.mHistory = history
    }

    override fun start() {
        mView?.bindData(mHistory)

        val section = hashMapOf<String, MutableList<HomeUpgradeHardware>>()
        mHistory.items?.forEach { room ->
            val name = room?.room?.name ?: ""
            if (room?.hardware != null) {
                if (section[name] == null) {
                    section[name] = mutableListOf()
                }
                room.hardware.amount = room.amount ?: 0
                section[name]?.add(room.hardware)
            }
        }

        mView?.bindOrderList(section)
    }

    override fun destroy() {
    }


}
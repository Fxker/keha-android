package com.sansiri.homeservice.ui.maintenance.device

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenanceDevice
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.getHttpCode
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

class MaintenanceDevicePresenter : BasePresenterImpl<MaintenanceDeviceContract.View>(), MaintenanceDeviceContract.Presenter {
    private var mHome: Hut? = null
    private var mDevice: MaintenanceDevice? = null

    override fun init(home: Hut, device: MaintenanceDevice?) {
        this.mHome = home
        this.mDevice = device
    }

    override fun start() {
        fetch()
    }

    override fun fetch() {
        mView?.bindDeviceDetail(mDevice)

        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                mView?.showLoading()
                mCompositeDisposable.add(
                        api.getMaintenancePlan(mHome?.unitId ?: "", mDevice?.planId
                                ?: "").observe().subscribe({ plans ->
                            mView?.hideLoading()
                            if (plans.isNotEmpty()) {
                                mView?.bindPlanDetail(plans)
                            } else {
                                mView?.showNoData()
                            }
                        }) {
                            mView?.hideLoading()
                            mView?.showNoData()
                            if (it.getHttpCode() != 404) {
                                mView?.showError(it.showHttpError())
                            }
                        }
                )
            }
        }
    }

    override fun requestForDetail(plan: MaintenancePlan) {
        if (mHome != null) {
            mView?.launchDetail(mHome!!, plan)
        }
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }
}
package com.sansiri.homeservice.ui.announcement.summary

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.announcement.all.AllAnnouncementActivity
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

class SummaryAnnouncementFragment : BaseFragment<SummaryAnnouncementContract.View, SummaryAnnouncementContract.Presenter>(), SummaryAnnouncementContract.View {
    override var mPresenter: SummaryAnnouncementContract.Presenter = SummaryAnnouncementPresenter()

    val mAdapter = SummaryAnnouncementAdapter({ view, titleType, announcement ->
        ContentActivity.start(activity!!, view, titleType, announcement)
    }) { title, id ->
        AllAnnouncementActivity.start(activity!!, title, id)
    }

    companion object {
        fun newInstance(): SummaryAnnouncementFragment {
            return SummaryAnnouncementFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            adapter = mAdapter
        }
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("ANNOUNCEMENT")
    }

    override fun addDataAt(position: Int, summary: SummaryItem) {
        val firstAnnouncement = summary.announcement.getOrNull(0)
        val secondAnnouncement = summary.announcement.getOrNull(1)
        val thirdAnnouncement = summary.announcement.getOrNull(2)

        if (firstAnnouncement != null || secondAnnouncement!= null || thirdAnnouncement != null) {
            mAdapter.addDataAt(position, summary)
        }
    }

    override fun addData(summary: SummaryItem) {
        mAdapter.addData(summary)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(text: String) {
        activity?.alert(text, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }?.show()
    }


}

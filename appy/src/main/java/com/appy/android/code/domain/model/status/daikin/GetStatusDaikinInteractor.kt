package com.appy.android.code.domain.model.status.daikin

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.status.daikin.StatusDaikinApiResult
import io.reactivex.Observable
import javax.inject.Inject

internal class GetStatusDaikinInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<StatusDaikinApiResult, GetStatusDaikinForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetStatusDaikinForm): Observable<StatusDaikinApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getStatusDaikin(params.headers, params.homeId, params.daikinId, params.params)
    }
}
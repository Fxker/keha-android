package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable

data class HMFamily(
        var id: String? = null,
        var pic: String? = null,
        var creator: String? = null,
        var name: String? = null,
        var nicknameInFamily: String? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(pic)
        writeString(creator)
        writeString(name)
        writeString(nicknameInFamily)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMFamily> = object : Parcelable.Creator<HMFamily> {
            override fun createFromParcel(source: Parcel): HMFamily = HMFamily(source)
            override fun newArray(size: Int): Array<HMFamily?> = arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.register.idcard

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/24/17.
 */
interface CitizenIDVerifyContract {
    interface Presenter : BasePresenter<View> {
        fun verifyCitizenID(citizenID: String)
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun launchRegisterScreen(customerId: String)
        fun launchLoginScreen(provider: String?, registeredWithEmailOrName: String?)
        fun showPleaseEnterCitizenId()
        fun showHelpDialog()
    }
}
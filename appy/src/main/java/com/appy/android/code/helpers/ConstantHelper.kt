package com.minor.bemynt.helpers

import android.content.Context
import com.appy.android.sdk.AppySDK

/**
 * Created by tar on 8/31/2017 AD.
 */
internal class ConstantHelper {

    companion object {
        lateinit var context: Context

        @JvmStatic
        fun init(_context: Context) {
            context = _context
        }

        fun getBaseApiDomain(): String {
            return if (AppySDK.instance().isProduction) {
                "https://napi.appysphere.com"
            } else {
                "https://napi-staging.appysphere.com"
            }
        }

        fun getControlApiDomain(): String {
            return if (AppySDK.instance().isProduction) {
                "https://ha.appysphere.com/v3/"
            } else {
                "https://ha-staging.appysphere.com/v3/"
            }
        }
    }
}
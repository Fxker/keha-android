package com.appy.android.sdk.control.light

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APLight : APControl, Parcelable {
    var mActions: List<APActionLight>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionLight)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionLight()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                LightCommand.OFF.action() -> {
                    action.action = LightCommand.OFF
                }
                LightCommand.ON.action() -> {
                    action.action = LightCommand.ON
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APLight> {
        const val TYPE = "APLight"

        override fun createFromParcel(parcel: Parcel): APLight {
            val type = parcel.readString()
            return APLight(parcel)
        }

        override fun newArray(size: Int): Array<APLight?> {
            return arrayOfNulls(size)
        }
    }
}
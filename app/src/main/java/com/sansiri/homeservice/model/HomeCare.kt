package com.sansiri.homeservice.model

import java.util.*

/**
 * Created by sansiri on 10/10/17.
 */
data class HomeCare(val title: String?, val description: String?, val icon: String?) {
    enum class Type {
        TITLE,
        RECEIVED,
        DONE,
        PENDING,
        REJECT
    }
}
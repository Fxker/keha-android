package com.sansiri.homeservice.ui.maintenance

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.ui.maintenance.device.MaintenancePlanDetailViewHolder
import com.sansiri.homeservice.util.inflate

/**
 * Created by sansiri on 10/12/17.
 */
class MaintenancePlanDetailAdapter(val itemClick: (MaintenancePlan) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<MaintenancePlan>()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return MaintenancePlanDetailViewHolder(parent.inflate(R.layout.view_summary_menu_min), itemClick)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is MaintenancePlanDetailViewHolder)
            holder.bind(mData[position])
    }

    fun setData(data: List<MaintenancePlan>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }
}
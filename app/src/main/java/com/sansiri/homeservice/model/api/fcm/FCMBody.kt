package com.sansiri.homeservice.model.api.fcm

data class FCMBody(
	val to: String? = null,
	val notification: Data? = null
)

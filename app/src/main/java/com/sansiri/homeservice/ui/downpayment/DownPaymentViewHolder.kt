package com.sansiri.homeservice.ui.downpayment

import android.util.TypedValue
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.view_summary_payable_card.view.*

/**
 * Created by sansiri on 10/11/17.
 */
class DownPaymentViewHolder(val view: View, val itemClick: ((DownpaymentItem) -> Unit)?) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(data: DownpaymentItem, color: Int) {
        with(view) {
            imageIcon.setImageResource(R.drawable.ic_bill)
            textTitle.setTextColor(context.resources.getColor(color))
            textTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.text_display1))

            textSubtitle.text = data.date?.report()
            if (data.isOverdue) {
                textDescription.setTextColor(context.resources.getColor(R.color.colorCancel))
            }
            textTitle.text = data.cost.toCurrencyFormat()
            textDescription.text = data.title

            if(itemClick != null && data.isSupportEpayment) {
                layoutPayable.show()
                root.setOnClickListener {
                    itemClick.invoke(data)
                }
            } else {
                layoutPayable.hide()
                root.isClickable = false
                root.background = null
            }
        }
    }
}
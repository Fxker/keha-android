package com.sansiri.homeservice.ui.notificationsettings

import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 1/9/18.
 */
interface NotificationSettingsContract {
    interface Presenter : BasePresenter<View> {
    }

    interface View : BaseView {
        fun bindData(menu: List<Menu>)
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.repository.trendywash.TrendyWashRepository
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.ui.base.BasePresenterImpl

class TrendyWashAddMoneyPresenter(val repository: TrendyWashRepository) : BasePresenterImpl<TrendyWashAddMoneyContract.View>(), TrendyWashAddMoneyContract.Presenter {
    private var mUnitId: String? = null
    private var mUserId: String? = null
    override var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>? = null

    override fun init(unitId: String, userId: String) {
        mUnitId = unitId
        mUserId = userId
    }

    override fun start() {
        fetch()
    }

    override fun destroy() {
        mCompositeDisposable.clear()
    }

    override fun fetch() {
        if (mUnitId != null && mUserId != null) {
            creditInfo = repository.getTrendyWashCredit(mUnitId!!, mUserId!!)
        }
    }

}
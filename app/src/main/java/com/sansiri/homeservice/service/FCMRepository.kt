package com.sansiri.homeservice.service

import com.sansiri.homeservice.model.api.fcm.FCMBody
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody

/**
 * Created by sansiri on 10/24/17.
 */
class FCMRepository(val idToken: String, val apiService: FCMService) {

    fun sendMessage(body: FCMBody): Observable<ResponseBody> = apiService.sendMessage("key=$idToken", body).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())

}
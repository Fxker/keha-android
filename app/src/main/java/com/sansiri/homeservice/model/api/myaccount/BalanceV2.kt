package com.sansiri.homeservice.model.api.myaccount

import com.google.gson.annotations.SerializedName

/**
 * Created by sansiri on 10/31/17.
 */
data class BalanceV2 (
        val income: Double,
        val lastBalance: Double,
        val totalBalance: Double
)
package com.appy.android.code.data.rest.JsonModel.fibaro

import com.google.gson.annotations.SerializedName

class DeviceJsonModel {

    @SerializedName("id")
    val id: Int = 0

    @SerializedName("name")
    val name: String = ""

    @SerializedName("type")
    val type: String = ""

    @SerializedName("baseType")
    val baseType: String = ""

    @SerializedName("enabled")
    val enabled: Boolean = true

    @SerializedName("visible")
    val visible: Boolean = true

    @SerializedName("isPlugin")
    val isPlugin: Boolean = true

    @SerializedName("properties")
    val properties: Properties = Properties()

    class Properties {

        @SerializedName("parameters")
        val parameters: List<Parameter> = listOf()

        @SerializedName("value")
        val value: String = ""
    }

    class Parameter {

        @SerializedName("id")
        val id: Int = 0

        @SerializedName("lastReportedValue")
        val lastReportedValue: Int = 0

        @SerializedName("lastSetValue")
        val lastSetValue: Int = 0

        @SerializedName("size")
        val size: Int = 0

        @SerializedName("value")
        val value: Int = 0
    }
}
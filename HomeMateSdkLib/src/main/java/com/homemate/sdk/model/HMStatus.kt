package com.homemate.sdk.model

import android.os.Parcel
import android.os.Parcelable

data class HMStatus(
        val deviceId: String? = null,
        val statusId: String? = null,
        val isOnline: Boolean = false,
        val value1: Int = 0,
        val value2: Int = 0,
        val value3: Int = 0,
        val value4: Int = 0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(deviceId)
        writeString(statusId)
        writeInt((if (isOnline) 1 else 0))
        writeInt(value1)
        writeInt(value2)
        writeInt(value3)
        writeInt(value4)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HMStatus> = object : Parcelable.Creator<HMStatus> {
            override fun createFromParcel(source: Parcel): HMStatus = HMStatus(source)
            override fun newArray(size: Int): Array<HMStatus?> = arrayOfNulls(size)
        }
    }
}
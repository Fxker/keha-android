package com.sansiri.homeservice.ui.tutorial.dynamic

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.sansiri.homeservice.model.api.tutorial.TutorialPage

/**
 * Created by sansiri on 4/5/18.
 */
class TutorialPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    val items = mutableListOf<TutorialPage>()

    override fun getItem(position: Int): androidx.fragment.app.Fragment = TutorialDynamicFragment.newInstance(items[position])

    override fun getCount(): Int = items.size

    fun setItem(content: List<TutorialPage>) {
        items.clear()
        items.addAll(content)
        notifyDataSetChanged()
    }

}
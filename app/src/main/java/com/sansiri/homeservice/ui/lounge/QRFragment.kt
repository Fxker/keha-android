package com.sansiri.homeservice.ui.lounge

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.GeneralDialogFragment
import com.sansiri.homeservice.model.api.Cr
import com.sansiri.homeservice.ui.base.BaseView
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.dialog_general.*
import kotlinx.android.synthetic.main.dialog_qr.*

/**
 * Created by sansiri on 10/16/17.
 */
class QRFragment(): GeneralDialogFragment(), BaseView, QRContract.View {
    val mPresenter = QRPresenter()

    override val onClickListener: () -> Unit = {
    }

    companion object {
        fun newInstance(): QRFragment {
            val fragment = QRFragment()
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_qr, null)
        mPresenter.attachView(this)
        panelContent.addView(view)
        buttonSubmit.hide()

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("SANSIRI_LOUNGE")
    }

    override fun bindQR(bitmap: Bitmap) {
        image.setImageBitmap(bitmap)
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

}
package com.sansiri.homeservice.ui.bos.type

import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface BosTypeContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun init(unitId: String)
        fun selectAreaType(groupType: Int)
        fun selectJobType(jobType: BosMenu)
    }

    interface View : BaseView {
        fun bindJobTypes(types: List<BosMenu>)
        fun showLoading()
        fun hideLoading()
        fun launchBosDetail(groupType: Int, jobType: BosMenu)
    }
}
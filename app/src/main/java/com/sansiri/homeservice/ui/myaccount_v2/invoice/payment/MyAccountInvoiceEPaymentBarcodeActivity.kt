package com.sansiri.homeservice.ui.myaccount_v2.invoice.payment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.epayment.barcode.BarcodeActivity
import com.sansiri.homeservice.ui.epayment.barcode.BarcodeContract
import com.sansiri.homeservice.ui.epayment.barcode.BarcodePresenter

class MyAccountInvoiceEPaymentBarcodeActivity : BarcodeActivity("MY_ACCOUNT_INVOICE_EPAYMENT_BARCODE") {
    override var mPresenter: BarcodeContract.Presenter = BarcodePresenter()
    var mInvoice: Invoice? = null
    var mHome: Hut? = null

    companion object {
        val INVOICE = "INVOICE"
        val HOME = "HOME"
        fun start(activity: Activity, home: Hut, invoice: Invoice) {
            val intent = Intent(activity, MyAccountInvoiceEPaymentBarcodeActivity::class.java)
            intent.putExtra(INVOICE, invoice)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mInvoice = intent.getParcelableExtra(INVOICE)
        mHome = intent.getParcelableExtra(HOME)

        renderBarcode(mInvoice?.paymentBarcode)
    }

    override fun getDetailBitmap(): Bitmap = createDetailBitmap(this, mHome, mInvoice)

    override fun onResume() {
        super.onResume()
        sendScreenView("MY_ACCOUNT_INVOICE_EPAYMENT_BARCODE")
    }


    override fun showError(message: String) {
    }
}

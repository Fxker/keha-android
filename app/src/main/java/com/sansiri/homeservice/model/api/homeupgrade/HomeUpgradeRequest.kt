package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcel
import android.os.Parcelable

data class HomeUpgradeRequest(
        val customer: HomeUpgradeCustomer,
        val items: List<HomeUpgradeOrderRequest>
)
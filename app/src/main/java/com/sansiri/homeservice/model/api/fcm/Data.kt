package com.sansiri.homeservice.model.api.fcm

data class Data(
		val title: String? = null,
		val text: String? = null
)

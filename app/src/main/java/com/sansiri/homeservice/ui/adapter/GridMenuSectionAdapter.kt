package com.sansiri.homeservice.ui.adapter

import android.graphics.Color
import androidx.core.content.ContextCompat
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class GridMenuSectionAdapter(
        val overlayColorRes: Int = R.color.colorSecondary,
        val title: String,
        var mData: List<DynamicMenu> = mutableListOf<DynamicMenu>(),
        val itemClick: (Menu) -> Unit
) : StatelessSection(SectionParameters.Builder(R.layout.view_menu_card)
        .headerResourceId(R.layout.view_title_extend)
        .build()) {
    companion object {
        val NO_OVERLAY = 0
    }

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return IconMenuViewHolder(
                ContextCompat.getColor(view.context, overlayColorRes),
                Color.WHITE,
                ContextCompat.getColor(view.context, R.color.colorAccent),
                view,
                itemClick)
    }

    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is IconMenuViewHolder) {
            holder.bind(mData[position])
        }
    }
}
package com.sansiri.homeservice.ui.welcome

import com.sansiri.homeservice.model.api.announcement.FamilyAnnouncement
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by oakraw on 10/2/2017 AD.
 */
interface WelcomeContract {
    interface Presenter: BasePresenter<View> {
        fun fetchData(page: Int)
    }

    interface View: BaseView {
        fun bindData(announcement: List<FamilyAnnouncement>)
        fun showLoading()
        fun hideLoading()
    }
}
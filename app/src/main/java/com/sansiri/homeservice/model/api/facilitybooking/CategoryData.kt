package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable

data class CategoryData(
		val reservation: Reservation? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readParcelable<Reservation>(Reservation::class.java.classLoader)
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeParcelable(reservation, 0)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<CategoryData> = object : Parcelable.Creator<CategoryData> {
			override fun createFromParcel(source: Parcel): CategoryData = CategoryData(source)
			override fun newArray(size: Int): Array<CategoryData?> = arrayOfNulls(size)
		}
	}
}

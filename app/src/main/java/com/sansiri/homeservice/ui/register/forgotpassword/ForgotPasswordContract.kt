package com.sansiri.homeservice.ui.register.forgotpassword

import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 2/26/18.
 */
interface ForgotPasswordContract {
    interface Presenter : BasePresenter<View> {
        fun resetPassword(email: String)
        fun validateAndSend(email: String)
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun emailError(message: String)
        fun getStringFromRes(res: Int): String
        fun showSuccess()
    }
}
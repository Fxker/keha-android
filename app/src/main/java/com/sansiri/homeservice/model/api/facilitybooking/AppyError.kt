package com.sansiri.homeservice.model.api.facilitybooking

/**
 * Created by sansiri on 2/23/18.
 */
data class AppyError (val errors: List<String>)
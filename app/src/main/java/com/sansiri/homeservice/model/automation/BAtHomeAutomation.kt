package com.sansiri.homeservice.model.automation

import com.appy.android.sdk.control.APControl
import vc.siriventures.bathome.model.BAtHomeComponent

/**
 * Created by sansiri on 10/10/17.
 */
data class BAtHomeAutomation(
        override var title: String = "",
        override var icon: Int,
        override var type: HomeAutomation.Type,
        val component: BAtHomeComponent? = null,
        override var action: Int = 0) : HomeAutomation() {
    companion object {
        const val ACTION_OFF = "off"
        const val ACTION_ON = "on"
        const val TYPE_LIGHT = "TYPE_LIGHT"
        const val TYPE_AC = "TYPE_AC"
        const val TYPE_SCENE = "TYPE_SCENE"
    }
}
package com.appy.android.sdk.control.daikin

import com.appy.android.sdk.control.APCommand

enum class DaikinCommand : APCommand {
    ON_OFF {
        override fun action(): String {
            return "ON_OFF"
        }
    },
    OPERATION_MODE {
        override fun action(): String {
            return "OPERATION_MODE"
        }
    },
    TEMP {
        override fun action(): String {
            return "TEMP"
        }
    },
    FAN_SPEED {
        override fun action(): String {
            return "FAN_SPEED"
        }
    },
    FAN_DIRECTION {
        override fun action(): String {
            return "FAN_DIRECTION"
        }
    };
}
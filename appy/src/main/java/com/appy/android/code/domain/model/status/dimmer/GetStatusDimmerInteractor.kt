package com.appy.android.code.domain.model.status.dimmer

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
internal class GetStatusDimmerInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<StatusDimmerApiResult, GetStatusDimmerForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: GetStatusDimmerForm): Observable<StatusDimmerApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getStatusDimmer(params.headers, params.homeId, params.dimmerId, params.params)
    }
}
package com.sansiri.homeservice.ui.automation.appy.remotecontrol.ac

import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeAutomationACRemoteControlContract {
    interface Presenter : BasePresenter<View> {
        fun init(control: APRemoteControl, homeId: String, projectId: String)
        fun decreaseTemp()
        fun increaseTemp()
        fun commandTemp(temp: Int)
        fun action(actionRemoteControl: APActionRemoteControl, param: String)
        fun commandFan(index: Int)
        fun commandMode(index: Int)
        fun commandTurnOn(isTurnOn: Boolean)
        fun mapSave(save: Map<String, String>)
    }

    interface View : BaseView {
        fun restoreSave(homeId: String, deviceId: String)
        fun showMessage(text: String)
        fun bindAirConditioner(control: APRemoteControl)
        fun setTemperature(degree: Int)
        fun setTurnOn(isTurnOn: Boolean)
        fun setCommandFanIndex(index: Int)
        fun setCommandModeIndex(index: Int)
        fun save(homeId: String, deviceId: String, map: HashMap<String, String>)
    }
}
package com.sansiri.homeservice.ui.suggestion


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.adapter.MultiSelectPhotoAdapter
import com.sansiri.homeservice.ui.base.BaseActivity
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.fragment_home_care_detail.*
import android.os.SystemClock
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.ui.language.LanguageActivity
import com.sansiri.homeservice.ui.mailbox.MailBoxActivity
import com.sansiri.homeservice.util.*
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_progress_status.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton


class SuggestionActivity : LocalizationActivity() {

    companion object {
        val UNIT_OBJECT_ID = "UNIT_OBJECT_ID"
        val TITLE = "TITLE"

        fun start(activity: Activity, unitObjectId: String, title: String?) {
            val intent = Intent(activity, SuggestionActivity::class.java)
            intent.putExtra(UNIT_OBJECT_ID, unitObjectId)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.suggestion))
        val unitId = intent?.getStringExtra(UNIT_OBJECT_ID)

        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, SuggestFragment.newInstance(unitId ?: "")).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

}

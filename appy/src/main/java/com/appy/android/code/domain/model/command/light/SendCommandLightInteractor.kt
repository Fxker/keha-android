package com.appy.android.code.domain.model.command.light

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.CommandApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 4/10/2560.
 */
internal class SendCommandLightInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<CommandApiResult, SendCommandLightForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: SendCommandLightForm): Observable<CommandApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .sendCommandLight( params.headers, params.homeId, params.lightId, params.mSendCommandLightJsonBody)
    }
}
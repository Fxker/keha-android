package com.sansiri.homeservice.ui.service

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.GenericMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_list.*

class MenuServiceActivity : AppCompatActivity() {

    companion object {
        val MENU = "SECTION"
        val TITLE = "TITLE"
        var onAction: ((Activity, Menu) -> Unit)? = null

        fun start(activity: Activity, title: String, menu: List<Menu>, onAction: (Activity, Menu) -> Unit) {
            this.onAction = onAction
            val intent = Intent(activity, MenuServiceActivity::class.java)
            intent.putExtra(MENU, menu.toTypedArray())
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }

    val mAdapter = GridMenuAdapter(null, null) {
        onAction?.invoke(this, it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val menuArray = intent?.getParcelableArrayExtra(MENU)
        val title = intent?.getStringExtra(TITLE) ?: ""


        setBackToolbar(title)

        list.apply {
            val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            adapter = mAdapter
        }

        menuArray?.let {
            bindData(it.toList() as List<GenericMenu>)
        }
    }

    private fun bindData(menu: List<GenericMenu>) {
        mAdapter.setData(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}

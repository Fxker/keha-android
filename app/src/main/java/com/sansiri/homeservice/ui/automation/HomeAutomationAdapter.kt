package com.sansiri.homeservice.ui.automation

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.automation.HomeAutomation
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder
import com.sansiri.homeservice.ui.automation.viewholder.*
import com.sansiri.homeservice.util.inflate
import com.sansiri.homeservice.util.px

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class HomeAutomationAdapter(val overlayColor: Int = R.color.colorSecondary, val itemClick: (HomeAutomation, Any) -> Unit) : RecyclerView.Adapter<LifecycleViewHolder>() {
    private var mData: List<HomeAutomation> = listOf()

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LifecycleViewHolder {
        when (viewType) {
            HomeAutomation.Type.REMOTE_CONTROL.ordinal -> return RemoteControlViewHolder(parent.inflate(R.layout.view_home_automation_remote_control_action), itemClick)
            HomeAutomation.Type.SWITCH.ordinal -> return SwitchViewHolder(parent.inflate(R.layout.view_home_automation_switch), itemClick)
            HomeAutomation.Type.TOGGLE.ordinal -> return ToggleSwitchViewHolder(parent.inflate(R.layout.view_home_automation_toggle_switch), itemClick)
            HomeAutomation.Type.SELECTOR.ordinal -> return SelectorViewHolder(parent.inflate(R.layout.view_home_automation_selector), overlayColor, itemClick)
            HomeAutomation.Type.CURTAIN.ordinal -> return CurtainViewHolder(parent.inflate(R.layout.view_home_automation_curtain), overlayColor, itemClick)
            HomeAutomation.Type.AC.ordinal -> return AirConditionViewHolder(parent.inflate(R.layout.view_home_automation_air_condition_v2), overlayColor, itemClick)
            HomeAutomation.Type.SEEK_BAR.ordinal -> return SeekbarViewHolder(parent.inflate(R.layout.view_home_automation_seekbar), overlayColor, itemClick)
            HomeAutomation.Type.REMOTE_CONTROL.ordinal -> return SwitchViewHolder(parent.inflate(R.layout.view_home_automation_switch), itemClick)
            HomeAutomation.Type.MUSIC_PLAYER.ordinal -> return MusicPlayerViewHolder(parent.inflate(R.layout.view_home_automation_music_player), overlayColor, itemClick)
            HomeAutomation.Type.GROUP_TOGGLE.ordinal -> return GroupToggleViewHolder(parent.inflate(R.layout.view_menu_home_automation_group_toggle), itemClick)
            else -> {
                return TitleViewHolder(parent.inflate(R.layout.view_title_extend))
            }
        }
    }

    fun resizeView(view: View, widthRatio: Float = 1f, heightRatio: Float = 1f) {
        with(view) {
            val gridColumn = context.resources.getInteger(R.integer.grid_column)
            // set card to relating with screen programmatically
            val params = layoutParams
            val displayWidth = Resources.getSystem().displayMetrics.widthPixels - (13.px * 2).toInt()

            if (widthRatio > 0) {
                params.width = (displayWidth * widthRatio / gridColumn).toInt()
            } else {
                params.width = widthRatio.toInt() //fix shadow take out some space}
            }

            if (heightRatio > 0) {
                params.height = ((displayWidth * heightRatio / gridColumn)).toInt()
            } else {
                params.height = heightRatio.toInt() //fix shadow take out some space}
            }

            layoutParams = params
        }
    }

    override fun getItemViewType(position: Int): Int = mData[position].type.ordinal

    override fun onBindViewHolder(holder: LifecycleViewHolder, position: Int) {
        if (holder is ToggleSwitchViewHolder) {
            resizeView(holder.view)
            holder.bind(mData[position])
        } else if (holder is SwitchViewHolder) {
            resizeView(holder.view)
            holder.bind(mData[position])
        } else if (holder is SelectorViewHolder) {
            resizeView(holder.view, 2f)
            holder.bind(mData[position])
        }else if (holder is CurtainViewHolder) {
            resizeView(holder.view, 3f)
            holder.bind(mData[position])
        } else if (holder is SeekbarViewHolder) {
            resizeView(holder.view, 3f, 1f)
            holder.bind(mData[position])
        } else if (holder is TitleViewHolder) {
            resizeView(holder.view, FrameLayout.LayoutParams.MATCH_PARENT.toFloat(), FrameLayout.LayoutParams.WRAP_CONTENT.toFloat())
            holder.bind(mData[position].title)
        } else if (holder is AirConditionViewHolder) {
            resizeView(holder.view, 3f, FrameLayout.LayoutParams.WRAP_CONTENT.toFloat())
            holder.bind(mData[position])
        } else if (holder is MusicPlayerViewHolder) {
            resizeView(holder.view, 3f, 1f)
            holder.bind(mData[position])
        } else if (holder is GroupToggleViewHolder) {
            resizeView(holder.view, FrameLayout.LayoutParams.MATCH_PARENT.toFloat(), FrameLayout.LayoutParams.WRAP_CONTENT.toFloat())
            holder.bind(mData[position])
        } else if (holder is RemoteControlViewHolder) {
            resizeView(holder.view, 3f, 1f)
            holder.bind(mData[position])
        }
    }
    fun setData(data: List<HomeAutomation>) {
        mData = data
        notifyDataSetChanged()
    }

    fun notifyItemChange(homeAutomation: HomeAutomation) {
        val position = mData.indexOf(homeAutomation)
        notifyItemChanged(position)
    }

    override fun onViewAttachedToWindow(holder: LifecycleViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAppear()
    }

    override fun onViewDetachedFromWindow(holder: LifecycleViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDisappear()
    }
}
package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Language(
        @SerializedName("language_code")
        val languageCode: String? = null,
        val language: String? = null,
        @SerializedName("default_language")
        val defaultLanguage: Boolean? = null
) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readValue(Boolean::class.java.classLoader) as Boolean?
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(languageCode)
                writeString(language)
                writeValue(defaultLanguage)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Language> = object : Parcelable.Creator<Language> {
                        override fun createFromParcel(source: Parcel): Language = Language(source)
                        override fun newArray(size: Int): Array<Language?> = arrayOfNulls(size)
                }
        }
}

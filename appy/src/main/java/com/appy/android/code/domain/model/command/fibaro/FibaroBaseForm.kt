package com.appy.android.code.domain.model.command.fibaro

import com.appy.android.sdk.control.APControl

open class FibaroBaseForm(
    internal val control: APControl
) {

    open fun getControlId(): String? {
        return control.localIds.values.first()
    }

    open fun getHost(): String? {
        return control.localhost + ":" + control.localport
    }

    open fun getUsername(): String? {
        return control.username
    }

    open fun getPassword(): String? {
        return control.password
    }
}
package com.sansiri.homeservice.data.repository.appy

import android.util.Log
import com.appy.android.sdk.AppySDK
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.CommandApiResult
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.blind.BlindCommand
import com.appy.android.sdk.control.daikin.APDaikin
import com.appy.android.sdk.control.daikin.APDaikinParameter
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.dimmer.DimmerCommand
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.light.LightCommand
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.scenario.ScenarioCommand
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand
import com.appy.android.sdk.home.APHome
import com.appy.android.sdk.home.HomesApiResult
import com.appy.android.sdk.room.APRoom
import com.appy.android.sdk.room.RoomApiResult
import com.appy.android.sdk.room.RoomsApiResult
import com.appy.android.sdk.status.blind.StatusBlindApiResult
import com.appy.android.sdk.status.daikin.StatusDaikinApiResult
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import com.appy.android.sdk.status.light.StatusLightApiResult
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import com.sansiri.homeservice.data.network.RuntimeCache.cacheDevice
import com.sansiri.homeservice.data.network.RuntimeCache.cacheRoom
import com.sansiri.homeservice.util.withDelay
import io.reactivex.Observable


/**
 * Created by sansiri on 11/7/17.
 */
class AppyRepositoryImpl(): AppyRepository {
    companion object {
        var isProduction = true

        fun clearCache() {
            cacheRoom.clear()
            cacheRoom.clear()
        }
    }

    private var testRoom = false

    enum class Type(val key: String) {
        DIMMER("Dimmer"),
        LIGHT("Light"),
        SCENE("Scene"),
        AC("AC"),
        TOGGLE("Switch"),
        REMOTE_CONTROL("Remote Control"),
        COOL_MASTER_NET("CoolMasterNet"),
        BLIND("Blind"),
        OUTLET("Outlet"),
    }

    init {
        AppySDK.initialize("2e31b7f60d49c7b41b1891180a96324a"/*, "r9QphvUc9kw4"*/)
    }

    constructor(projectId: String, pin: String?) : this() {
        AppySDK.setProduction(isProduction)
        AppySDK.setAppKey(getDedicatedProjectId(projectId))
        AppySDK.Automation.setPin(pin ?: "12345")
    }

    override fun setPin(pin: String) {
        AppySDK.Automation.setPin(pin)
    }

    override fun setProjectId(projectId: String,  pin: String?) {
        AppySDK.setProduction(isProduction)
        AppySDK.setAppKey(getDedicatedProjectId(projectId))
        AppySDK.Automation.setPin(pin ?: "12345")
    }


    override fun getDedicatedProjectId(projectId: String): String = when {
        testRoom -> "r9QphvUc9kw4"
        else -> projectId
    }

    override fun getDedicatedUnitId(unitId: String): String =
            when {
                //testRoom -> "xzoRvKoX" //sv room
                testRoom -> "1RBkrDjn"
                else -> unitId
            }


    override fun setLocalMode(isLocalMode: Boolean) {
        AppySDK.Automation.setLocalMode(isLocalMode)
    }

    override fun getHomes(onSuccess: (HomesApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getHomes(object : AppySDK.HomesCallback {
            override fun onSuccess(homesApiResult: HomesApiResult) {
                onSuccess(homesApiResult)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }


    override fun getRooms(unitId: String): Observable<List<APRoom>> {
        val home = APHome()
        home.id = getDedicatedUnitId(unitId)

        return getRooms(home)
    }

    override fun getRooms(home: APHome): Observable<List<APRoom>> {
        if (cacheRoom.contains(home.id)) {
            return Observable.just(cacheRoom[home.id])
        }
        return Observable.create<List<APRoom>> { emitter ->
            AppySDK.Automation.getRooms(home, object : AppySDK.RoomsCallback {
                override fun onSuccess(t: RoomsApiResult) {
                    if (home.id != null && t.data != null) {
                        cacheRoom.put(home.id!!, t.data!!)
                    }
                    emitter.onNext(t.data ?: listOf())
                    emitter.onComplete()
                }

                override fun onFailed(e: Throwable) {
                    emitter.onError(e)
                }
            })
        }
    }

    override fun getControl(unitId: String, roomId: String): Observable<List<APControl>> {
        val home = APHome()
        home.id = getDedicatedUnitId(unitId)

        val room = APRoom()
        room.id = roomId

        if (cacheDevice.contains(roomId)) {
            return Observable.just(cacheDevice[roomId])
        }

        return Observable.create<List<APControl>> { emitter ->
            AppySDK.Automation.getRoom(home, room, object : AppySDK.RoomCallback {
                override fun onSuccess(t: RoomApiResult) {
                    if (t.datas?.controls?.isNotEmpty() == true) {
                        cacheDevice.put(roomId, t.datas?.controls!!)
                    }
                    emitter.onNext(t.datas?.controls!!)
                    emitter.onComplete()
                }

                override fun onFailed(e: Throwable) {
                    emitter.onError(e)
                }
            })
        }

    }


//    fun getAllControl(unitId: String): Observable<List<APControl>> {
//        val home = APHome()
//        home.id = unitId
//
//        if (cacheDevice.contains(home.id)) {
//            return Observable.just(cacheDevice[home.id!!])
//        }
//        return Observable.create<List<APControl>> { emitter ->
//            AppySDK.Automation.getControls(home, object : AppySDK.ControlsCallback {
//                override fun onSuccess(t: ControlsApiResult) {
//                    if (home.id != null && t.datas.isNotEmpty()) {
//                        cacheDevice.put(home.id!!, t.datas)
//                    }
//                    emitter.onNext(t.datas)
//                    emitter.onComplete()
//                }
//
//                override fun onFailed(e: Throwable) {
//                    emitter.onError(e)
//                }
//            })
//        }
//    }


    override fun getControl(home: APHome, room: APRoom, onSuccess: (RoomApiResult) -> Unit, onFailed: (Throwable) -> Unit) {

        AppySDK.Automation.getRoom(home, room, object : AppySDK.RoomCallback {
            override fun onSuccess(roomApiResult: RoomApiResult) {
                onSuccess(roomApiResult)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })

    }

    override fun getControl(unitId: String, roomId: String, onSuccess: (RoomApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        val home = APHome()
        home.id = getDedicatedUnitId(unitId)

        val room = APRoom()
        room.id = roomId

        AppySDK.Automation.getRoom(home, room, object : AppySDK.RoomCallback {
            override fun onSuccess(roomApiResult: RoomApiResult) {
                onSuccess(roomApiResult)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })

    }


    //    fun getControl(onSuccess: (RoomApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
//        getHomes({ homes ->
//            // homes.html?
//            val home = homes.html?.last()
//            if (home != null) {
//                getRooms(home, { rooms ->
//                    val room = rooms.html?.last()
//                    if (room != null) {
//                        getControl(home, room, { controls ->
//                            onSuccess(controls)
//                        }) {
//                            Log.e("", it.message, it)
//                            onFailed(it)
//                        }
//                    }
//                }) { onFailed(it) }
//            }
//        }) { onFailed(it) }
//    }
    override fun checkStatusLight(homeId: String, light: APLight, onSuccess: (StatusLightApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getStatusLight(APHome().apply { id = getDedicatedUnitId(homeId) }, light, object : AppySDK.StatusLightCallback {
            override fun onFailed(e: Throwable) {
                onFailed(e)
            }

            override fun onSuccess(t: StatusLightApiResult) {
                onSuccess(t)
            }
        })
    }

    override fun checkStatusToggleSwitch(homeId: String, light: APToggleSwitch, onSuccess: (StatusToggleSwitchApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getStatusToggleSwitch(APHome().apply { id = getDedicatedUnitId(homeId) }, light, object : AppySDK.StatusToggleSwitchCallback {
            override fun onFailed(e: Throwable) {
                onFailed(e)
            }

            override fun onSuccess(t: StatusToggleSwitchApiResult) {
                onSuccess(t)
            }
        })
    }

    override fun checkStatusDimmer(homeId: String, dimmer: APDimmer, onSuccess: (StatusDimmerApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getStatusDimmer(APHome().apply { id = getDedicatedUnitId(homeId) }, dimmer, object : AppySDK.StatusDimmerCallback {
            override fun onSuccess(t: StatusDimmerApiResult) {
                onSuccess(t)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }

    override fun checkStatusDaikin(home: APHome, device: APDaikin, onSuccess: (StatusDaikinApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getStatusDaikin(home, device, object: AppySDK.StatusDaikinCallback {
            override fun onSuccess(t: StatusDaikinApiResult) {
                onSuccess(t)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }

    override fun checkStatusBlind(homeId: String, blind: APBlind, onSuccess: (StatusBlindApiResult) -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.getStatusBlind(APHome().apply { id = getDedicatedUnitId(homeId) }, blind, object : AppySDK.StatusBlindCallback {
            override fun onSuccess(t: StatusBlindApiResult) {
                onSuccess(t)
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }


    override fun commandLight(unitId: String, lightId: String, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        val home = APHome()
        home.id = getDedicatedUnitId(unitId)

        val light = APLight()
        light.id = lightId

        commandLight(home, light, commandNumber, onSuccess, onFailed)
    }

    override fun commandLight(home: APHome, light: APLight, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        val command = if (commandNumber == LightCommand.OFF.ordinal) LightCommand.OFF else LightCommand.ON
        {
            AppySDK.Automation.sendCommand(home, light, command, object : AppySDK.CommandCallback {
                override fun onFailed(e: Throwable) {
                    onFailed(e)
                }

                override fun onSuccess(t: CommandApiResult) {
                    onSuccess()
                }
            })
        }.withDelay(100)
    }

    override fun commandToggleSwitch(home: APHome, toggle: APToggleSwitch, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        val command = if (commandNumber == 0) ToggleSwitchCommand.OFF else ToggleSwitchCommand.ON
        AppySDK.Automation.sendCommand(home, toggle, command, object : AppySDK.CommandCallback {
            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }

    override fun commandBlind(home: APHome, blind: APBlind, commandNumber: Int, delay: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {

        val command = BlindCommand.values()[commandNumber]

        {
            AppySDK.Automation.sendCommand(home, blind, command, object : AppySDK.CommandCallback {
                override fun onSuccess(t: CommandApiResult) {
                    Log.d("Home Automation", "delay at $delay")
                    onSuccess()
                }

                override fun onFailed(e: Throwable) {
                    onFailed(e)
                }
            })
        }.withDelay(delay.toLong())
    }

    override fun commandBlind(home: APHome, blind: APBlind, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        commandBlind(home, blind, commandNumber, 100, onSuccess, onFailed)
    }

    override fun commandDimmer(unitId: String, dimmerId: String, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        val home = APHome()
        home.id = getDedicatedUnitId(unitId)

        val dimmer = APDimmer()
        dimmer.id = dimmerId

        commandDimmer(home, dimmer, commandNumber, onSuccess, onFailed)
    }

    override fun commandDimmer(home: APHome, dimmer: APDimmer, commandNumber: Int, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        val command = when (commandNumber) {
            0 -> DimmerCommand.OFF
            1 -> DimmerCommand.DIMMER20
            2 -> DimmerCommand.DIMMER20
            3 -> DimmerCommand.DIMMER30
            4 -> DimmerCommand.DIMMER40
            5 -> DimmerCommand.DIMMER50
            6 -> DimmerCommand.DIMMER60
            7 -> DimmerCommand.DIMMER70
            8 -> DimmerCommand.DIMMER80
            9 -> DimmerCommand.DIMMER90
            else -> DimmerCommand.DIMMER100
        }
        AppySDK.Automation.sendCommand(home, dimmer, command, object : AppySDK.CommandCallback {
            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }

    override fun commandScene(home: APHome, scene: APScenario, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.sendCommand(home, scene, ScenarioCommand.ACTIVATE
                , object : AppySDK.CommandCallback {
            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }
        })
    }

    override fun commandRemoteControl(home: APHome, remoteControl: APRemoteControl, actionRemoteControl: APActionRemoteControl, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.sendCommand(home, remoteControl, actionRemoteControl, object : AppySDK.CommandCallback {
            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }

        })
    }

    override fun commandRemoteControl(home: APHome, remoteControl: APRemoteControl, actionRemoteControl: APActionRemoteControl, param: String, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.sendCommand(home, remoteControl, actionRemoteControl, param, object : AppySDK.CommandCallback {
            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }

            override fun onFailed(e: Throwable) {
                onFailed(e)
            }

        })

    }

    override fun commandDaikin(home: APHome, device: APDaikin, param: APDaikinParameter, onSuccess: () -> Unit, onFailed: (Throwable) -> Unit) {
        AppySDK.Automation.sendCommand(home, device, param, object: AppySDK.CommandCallback {
            override fun onFailed(e: Throwable) {
                onFailed(e)
            }

            override fun onSuccess(t: CommandApiResult) {
                onSuccess()
            }
        })
    }


}
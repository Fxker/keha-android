package com.sansiri.homeservice.util

import android.content.res.Resources
import android.view.View
import com.sansiri.homeservice.R

fun View.gridSize(widthRatio: Float = 1f, heightRatio: Float = 1f) {
    val gridColumn = context.resources.getInteger(R.integer.grid_column)
    // set card to relating with screen programmatically
    val params = layoutParams
    val displayWidth = Resources.getSystem().displayMetrics.widthPixels - (13.px * 2).toInt()

    if (widthRatio > 0) {
        params.width = (displayWidth * widthRatio / gridColumn).toInt()
    } else {
        params.width = widthRatio.toInt() //fix shadow take out some space}
    }

    if (heightRatio > 0) {
        params.height = (displayWidth * heightRatio / gridColumn).toInt()
    } else {
        params.height = heightRatio.toInt() //fix shadow take out some space}
    }

    layoutParams = params
}

fun View.gridSizeWidth(widthRatio: Float = 1f) {
    val gridColumn = context.resources.getInteger(R.integer.grid_column)
    // set card to relating with screen programmatically
    val params = layoutParams
    val displayWidth = Resources.getSystem().displayMetrics.widthPixels - (13.px * 2).toInt()

    if (widthRatio > 0) {
        params.width = (displayWidth * widthRatio / gridColumn).toInt()
    } else {
        params.width = widthRatio.toInt() //fix shadow take out some space}
    }

    layoutParams = params
}
package com.sansiri.homeservice.ui.automation.orvibo.control

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
//import com.homemate.sdk.model.HMControl
//import com.homemate.sdk.model.HMDevice
import com.sansiri.homeservice.model.automation.HomeAutomation
//import com.sansiri.homeservice.model.automation.OrviboHomeAutomation
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/7/17.
 */
//interface OrviboHomeAutomationContract {
//    interface Presenter : BasePresenter<View> {
//        var devicesLiveData: MutableLiveData<List<HMControl>>
//
//        fun fetchData(roomId: String?)
//        fun controlDevice(homeAutomation: OrviboHomeAutomation, command: Any)
//        fun onDeviceUpdated(devices: List<HMControl>)
//        fun controlScene(homeAutomation: OrviboHomeAutomation)
//    }
//
//    interface View : BaseView {
//        fun bindData(controls: List<HomeAutomation>)
//        fun showMessage(text: String)
//        fun updateDevices()
//        fun showLoading()
//        fun hideLoading()
//        fun showNoData()
//        fun notifyItemChange(homeAutomation: OrviboHomeAutomation)
//    }
//}
package com.sansiri.homeservice.model.api

/**
 * Created by sansiri on 11/27/17.
 */
data class ListResponse<T> (
    val count: Int = 0,
    val items: List<T>
)
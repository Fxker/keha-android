package com.sansiri.homeservice.ui.homecare.history.detail

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.sansiri.homeservice.R
import kotlinx.android.synthetic.main.view_photo_card.view.*

class ThumbnailViewHolder (val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    fun bind(url: String) {
        with(view) {
            Glide.with(context)
                    .load(url)
                    .thumbnail(0.1f)
                    .placeholder(R.drawable.placeholder_project)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .centerCrop()
                    .into(imageView)
        }
    }
}
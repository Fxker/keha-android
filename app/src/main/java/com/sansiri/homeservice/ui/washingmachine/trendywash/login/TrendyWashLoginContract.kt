package com.sansiri.homeservice.ui.washingmachine.trendywash.login

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashRegisterCustomer
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TrendyWashLoginContract {
    interface Presenter : BasePresenter<View> {
        fun login(mobileNo: String, password: String): MutableLiveData<Resource<TrendyWashRegisterCustomer>>
        fun init(unitId: String)
        fun getFacebookLoginLink(): String
    }

    interface View : BaseView {
        fun launchHome(userId: String)
        fun showLoading()
        fun hideLoading()
        fun launchWeb(url: String)
    }
}
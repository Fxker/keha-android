package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TrendyWashAddMoneyContract {
    interface Presenter : BasePresenter<View> {
        var creditInfo: MutableLiveData<Resource<TrendyWashCredit>>?
        fun init(unitId: String, userId: String)
        fun fetch()
    }

    interface View : BaseView {
        fun bindData(creditInfo: TrendyWashCredit)
        fun showLoading()
        fun hideLoading()
    }
}
package com.sansiri.homeservice.model.api.partner.aqi

import android.os.Parcel
import android.os.Parcelable

data class AirAqi(
        val objectId: String? = null,
        val title: String? = null,
        val description: String? = null,
        val unit: String? = null,
        val value: Double? = null,
        val color: String? = null,
        val iconUrl: String? = null,
        var valueDisplay: String? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(objectId)
        writeString(title)
        writeString(description)
        writeString(unit)
        writeValue(value)
        writeString(color)
        writeString(iconUrl)
        writeString(valueDisplay)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AirAqi> = object : Parcelable.Creator<AirAqi> {
            override fun createFromParcel(source: Parcel): AirAqi = AirAqi(source)
            override fun newArray(size: Int): Array<AirAqi?> = arrayOfNulls(size)
        }
    }
}
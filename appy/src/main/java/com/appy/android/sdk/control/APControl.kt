package com.appy.android.sdk.control

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.ac.APAirConditioner
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.outlet.APOutlet
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.google.gson.annotations.SerializedName

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
abstract class APControl : Parcelable {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("alternate_title")
    var titleAlternate: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("class")
    var klass: String? = null

    @SerializedName("icon")
    var icon: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("password")
    var password: String? = null

    // Bticno
    @SerializedName("circuit_address")
    var circuitAddress: String? = null

    @SerializedName("host_address")
    var hostAddress: String? = null

    @SerializedName("scene_command")
    var sceneAddress: String? = null

    // Fibaro
    @SerializedName("local_host")
    var localhost: String? = null

    @SerializedName("local_port")
    var localport: String? = null

    @SerializedName("hc_name")
    var hcName: String? = null

    @SerializedName("local_id")
    var localIds = HashMap<String, String>()


    constructor()

    constructor(source: Parcel) : this() {
        id = source.readString()
        title = source.readString()
        titleAlternate = source.readString()
        type = source.readString()
        klass = source.readString()
        icon = source.readString()
        username = source.readString()
        password = source.readString()

        // Bticino
        circuitAddress = source.readString()
        hostAddress = source.readString()
        sceneAddress = source.readString()

        // Fibaro
        localhost = source.readString()
        localport = source.readString()
        hcName = source.readString()

        source.readSerializable()?.let {
            localIds = it as HashMap<String, String>
        }
    }

    open fun init(controlJsonModel: ControlJsonModel) {
        this.id = controlJsonModel.id
        this.title = controlJsonModel.title
        this.titleAlternate = controlJsonModel.titleAlternate
        this.type = controlJsonModel.type
        this.klass = controlJsonModel.klass
        this.icon = controlJsonModel.icon
        this.username = controlJsonModel.username
        this.password = controlJsonModel.password

        // Bticino
        this.circuitAddress = controlJsonModel.circuitAddress
        this.hostAddress = controlJsonModel.hostAddress
        this.sceneAddress = controlJsonModel.sceneAddress

        // Fibaro
        this.localhost = controlJsonModel.localhost
        this.localport = controlJsonModel.localport
        this.hcName = controlJsonModel.hcName
        this.localIds = controlJsonModel.localId.ids
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        when {
            this is APAirConditioner -> dest.writeString(APAirConditioner.TYPE)
            this is APBlind -> dest.writeString(APBlind.TYPE)
            this is APDimmer -> dest.writeString(APDimmer.TYPE)
            this is APLight -> dest.writeString(APLight.TYPE)
            this is APOutlet -> dest.writeString(APOutlet.TYPE)
            this is APRemoteControl -> dest.writeString(APRemoteControl.TYPE)
            this is APScenario -> dest.writeString(APScenario.TYPE)
            this is APToggleSwitch -> dest.writeString(APToggleSwitch.TYPE)
        }
        dest.writeString(id)
        dest.writeString(title)
        dest.writeString(titleAlternate)
        dest.writeString(type)
        dest.writeString(klass)
        dest.writeString(icon)
        dest.writeString(username)
        dest.writeString(password)

        // Bticino
        dest.writeString(circuitAddress)
        dest.writeString(hostAddress)
        dest.writeString(sceneAddress)

        // Fibaro
        dest.writeString(localhost)
        dest.writeString(localport)
        dest.writeString(hcName)
        dest.writeSerializable(localIds)
    }

    companion object CREATOR : Parcelable.Creator<APControl> {
        override fun createFromParcel(parcel: Parcel): APControl {
            val type = parcel.readString()
            return when (type) {
                APAirConditioner.TYPE -> APAirConditioner(parcel)
                APBlind.TYPE -> APBlind(parcel)
                APDimmer.TYPE -> APDimmer(parcel)
                APLight.TYPE -> APLight(parcel)
                APOutlet.TYPE -> APOutlet(parcel)
                APRemoteControl.TYPE -> APRemoteControl(parcel)
                APScenario.TYPE -> APScenario(parcel)
                APToggleSwitch.TYPE -> APToggleSwitch(parcel)
                else -> throw Exception("Type not found")
            }
        }

        override fun newArray(size: Int): Array<APControl?> {
            return arrayOfNulls(size)
        }
    }

    fun isOverkizModel(): Boolean {
        return when (klass) {
            null -> false
            else -> klass!!.contains("Overkiz")
        }
    }

    fun isFibaroModel(): Boolean {
        return when (klass) {
            null -> false
            else -> klass!!.contains("Fibaro")
        }
    }

    abstract val actions: List<APAction<*>>
}
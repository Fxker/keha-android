package com.sansiri.homeservice.model.api

import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class VisitorAccessHistory(
	val createdAt: String? = null,
	val message: String? = null,
	val objectId: String? = null,
	val updatedAt: String? = null
) {
	val _createdAt: Date?
		get() {
			return try {
				if (this.createdAt == null) return null

				DateTime(this.createdAt).toDate()
			} catch (e: ParseException) {
				null
			}
		}
}

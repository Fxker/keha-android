package com.sansiri.homeservice.model.api.partner.aqi

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

@Parcelize
data class Air(
        val lat: Double? = null,
        val lng: Double? = null,
        val placeName: String? = null,
        val weathers: List<AirWeather>? = null,
        val aqi: AirAqi? = null,
        val pollutants: List<AirPollutant>? = null,
        val updatedAt: String? = null) : Parcelable {

    val _updatedAt: Date?
        get() {
            try {
                return DateTime(this.updatedAt).toDate()
            } catch (e: ParseException) {
                return null
            }
        }
}
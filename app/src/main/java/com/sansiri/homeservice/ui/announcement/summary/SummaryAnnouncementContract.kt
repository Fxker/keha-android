package com.sansiri.homeservice.ui.announcement.summary

import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.announcement.FamilyAnnouncement
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/18/17.
 */
interface SummaryAnnouncementContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData()
    }

    interface View : BaseView {
        fun addData(summary: SummaryItem)
        fun addDataAt(position: Int, summary: SummaryItem)
        fun showLoading()
        fun hideLoading()
        fun getString(stringRes: Int): String
    }
}
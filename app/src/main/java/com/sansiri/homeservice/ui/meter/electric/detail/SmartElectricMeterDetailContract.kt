package com.sansiri.homeservice.ui.meter.electric.detail

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterHistoryList
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView
import java.util.*

interface SmartElectricMeterDetailContract {
    interface Presenter : BasePresenter<View> {
        fun init(type: String?, unitId: String?, initDate: Calendar?)
        fun fetch()
        fun requestDatePicker()
        fun fetchFromSelectedDate(calendar: Calendar)
    }

    interface View : BaseView {
        fun attachHistoryObserver(smartElectricMeterHistory: MutableLiveData<Resource<SmartElectricMeterHistoryList>>)
        fun bindData(history: SmartElectricMeterHistoryList)
        fun showLoading()
        fun hideLoading()
        fun showDatePicker(calendar: Calendar)
        fun showMonthPicker(calendar: Calendar)
        fun showSelectedDate(calendar: Calendar)
    }
}
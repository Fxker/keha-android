package com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.model.database.creditcard.SavingCreditCard
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface PaymentGateway2C2PContract {
    interface Presenter : BasePresenter<View> {
        fun init(paymentResponse: PaymentResponse?)
        fun setCardName(name: String)
        fun setCardNumber(number: String)
        fun setCCV(ccv: String)
        fun setExpiryMonth(month: Int)
        fun setExpiryYear(year: Int)
        fun validate()
        fun execute()
        fun handleResponse(response: TransactionResultResponse)
        fun setRememberCreditCard(checked: Boolean)
        fun setSavingCard(creditCards: List<SavingCreditCard>)
        fun setNewCard()
        fun setEmail(email: String)
        fun deleteCard()

        var liveDataSavingCreditCards: LiveData<List<SavingCreditCard>>?
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun launchWeb(url: String)
        fun enableContinueButton()
        fun disableContinueButton()
        fun showError(messageRes: Int)
        fun launchTransactionResult(transactionID: String, email: String? = null)
        fun showSavingCard(securedCardNumber: String?, cardProvider: String?, paymentResponse: PaymentResponse?)
        fun deleteSavingCard()
        fun showNewCard(paymentResponse: PaymentResponse?)
        fun showSavingEmail(billingEmail: String)
    }
}
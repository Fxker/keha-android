package com.sansiri.homeservice.ui.aqi

import com.sansiri.homeservice.model.api.partner.aqi.Air
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/18/17.
 */
interface AqiContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun init(projectId: String)
    }

    interface View : BaseView {
        fun bindData(air: Air)
        fun showLoading()
        fun hideLoading()
    }
}
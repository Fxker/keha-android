package com.sansiri.homeservice.component.bottomsheet

import android.annotation.SuppressLint
import android.app.Dialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import com.sansiri.homeservice.R
import android.widget.TimePicker
import java.util.*


/**
 * Created by sansiri on 10/12/17.
 */
class TimePickerBottomSheet() : BottomSheetDialogFragment() {

    var onTimePickerChange: ((Calendar) -> Unit)? = null
    var mCalendar: Calendar? = null
    var hour = -1
        set(value) {
            field = value
            mTimePicker?.currentHour = value
        }
    var minute = -1
        set(value) {
            field = value
            mTimePicker?.currentMinute = minute
        }

    private var mTimePicker: TimePicker? = null

    @SuppressLint("ValidFragment")
    constructor(onTimeChange: (Calendar) -> Unit) : this() {
        this.onTimePickerChange = onTimeChange
    }

    @SuppressLint("ValidFragment")
    constructor(time: Calendar, onTimeChange: (Calendar) -> Unit) : this() {
        this.mCalendar = time
        this.hour = time[Calendar.HOUR_OF_DAY]
        this.minute = time[Calendar.MINUTE]
        this.onTimePickerChange = onTimeChange
    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.view_time_picker, null)
        view.findViewById<View>(R.id.buttonDone).setOnClickListener { dismiss() }
        mTimePicker = view.findViewById<TimePicker>(R.id.timePicker)
        if(hour != -1 && minute != -1) {
            mTimePicker?.currentHour = hour
            mTimePicker?.currentMinute = minute
        }
        mTimePicker?.setIs24HourView(true)
        mTimePicker?.setOnTimeChangedListener { _, hour, minute ->
            with(this@TimePickerBottomSheet) {
                this.hour = hour
                this.minute = minute
                this.mCalendar?.set(Calendar.HOUR_OF_DAY, hour)
                this.mCalendar?.set(Calendar.MINUTE, minute)
                if (this.mCalendar != null && this.onTimePickerChange != null) {
                    this.onTimePickerChange?.invoke(mCalendar!!)
                }
            }
        }
        dialog.setContentView(view)
    }
}

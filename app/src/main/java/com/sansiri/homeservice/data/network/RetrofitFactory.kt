package com.sansiri.homeservice.data.network

import com.facebook.FacebookSdk.getCacheDir
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


/**
 * Created by sansiri on 10/24/17.
 */
object RetrofitFactory {
    val cache = Cache(getCacheDir(), 10 * 1024 * 1024)
    //
//    val cacheIntercepter = object: Interceptor {
//        override fun intercept(chain: Interceptor.Chain): Response {
//            val originalResponse = chain.proceed(chain.request())
//            if (Connectivity.isConnected()) {
//                val maxAge = 60 // read from cache for 1 minute
//                Log.d("RetrofitFactory", originalResponse.message());
//                return originalResponse.newBuilder()
//                        .header("Cache-Control", "public, max-age=" + maxAge)
//                        .build()
//            } else {
//                Log.d("RetrofitFactory", originalResponse.message());
//                val maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
//                return originalResponse.newBuilder()
//                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
//                        .build()
//            }
//        }
//
//    }
    var endpoint = ""

    val httpClient = OkHttpClient.Builder().apply {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        addInterceptor(logging)
        addInterceptor(AppInterceptor())
        cache(cache)
        connectTimeout(5, TimeUnit.MINUTES)
        writeTimeout(5, TimeUnit.MINUTES)
        readTimeout(5, TimeUnit.MINUTES)
    }


    val instance: Retrofit by lazy {
        Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(endpoint)
                .build()
    }

    var instanceOnionShack: Retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .baseUrl("https://graph-na02-useast1.api.smartthings.com/api/smartapps/installations/")
            .build()

    var instanceFCM: Retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .baseUrl("https://fcm.googleapis.com/fcm/")
            .build()
}
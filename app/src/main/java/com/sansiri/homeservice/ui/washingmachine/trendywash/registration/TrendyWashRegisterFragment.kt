package com.sansiri.homeservice.ui.washingmachine.trendywash.registration


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.Profile
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_trendy_wash_registration.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.scope.currentScope
import android.text.method.PasswordTransformationMethod
import com.bumptech.glide.Glide
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.ui.washingmachine.trendywash.TrendyWashMainActivity


class TrendyWashRegisterFragment : BaseV2Fragment<TrendyWashRegisterContract.View, TrendyWashRegisterContract.Presenter>(), TrendyWashRegisterContract.View {
    override val mPresenter: TrendyWashRegisterContract.Presenter by currentScope.inject()

    private var mUnitID: String? = null
    private var mIconUrl: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val ICON_URL = "ICON_URL"

        fun newInstance(unitId: String, iconUrl: String?): TrendyWashRegisterFragment {
            return TrendyWashRegisterFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(ICON_URL, iconUrl)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trendy_wash_registration, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitID = arguments?.getString(UNIT_ID)
        mIconUrl = arguments?.getString(ICON_URL)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRENDY_WASH_REGISTER")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setBackToolbar(toolbar, getString(R.string.register))

        buttonRegister.setOnClickListener {
            if (mPresenter.validate()) {
                mPresenter.submit().observe(this, Observer { resource ->
                    when (resource?.status) {
                        Resource.LOADING -> {
                            showLoading()
                        }
                        Resource.SUCCESS -> {
                            hideLoading()
                            if (resource.data?.id != null) {
                                launchHome(resource.data?.id!!)
                            }
                        }
                        Resource.ERROR -> {
                            hideLoading()
                            showError(resource.message ?: "Error")
                        }
                    }
                })
            }
        }

        editFirstname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setFirstName(it)
        })

        editLastname.addTextChangedListener(TextWatcherExtend {
            mPresenter.setLastName(it)
        })

        editEmail.addTextChangedListener(TextWatcherExtend {
            mPresenter.setEmail(it)
        })

        editTel.addTextChangedListener(TextWatcherExtend {
            mPresenter.setTel(it)
        })

        editPassword.addTextChangedListener(TextWatcherExtend {
            mPresenter.setPassword(it)
        })

        editPasswordConfirm.addTextChangedListener(TextWatcherExtend {
            mPresenter.setPasswordConfirm(it)
        })

        editPassword.transformationMethod = PasswordTransformationMethod()
        editPasswordConfirm.transformationMethod = PasswordTransformationMethod()

        mIconUrl?.let { Glide.with(this).load(it).into(imageIcon) }

        mPresenter.init(mUnitID ?: "")
        mPresenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(profile: Profile) {
        editFirstname.setText(profile.firstname)
        editLastname.setText(profile.lastname)
        editTel.setText(profile.phoneNumber)
        editEmail.setText(profile.email)
    }

    override fun showLoading() {
        progressBar.show()
        buttonRegister.hide()
    }

    override fun hideLoading() {
        progressBar.hide()
        buttonRegister.show()
    }

    override fun launchHome(userId: String) {
        sendEvent("TRENDY_WASH_REGISTER", "ACTION", "REGISTER")
        PreferenceHelper.defaultPrefs(context!!)[PreferenceHelper.TREND_WASH_USER_ID] = userId
        if (activity is TrendyWashMainActivity) {
            (activity as TrendyWashMainActivity).launchHome(userId)
        }
    }

    override fun showEmailError(messageRes: Int) {
        editEmail.error = getString(messageRes)
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}

package com.appy.android.sdk.control.blind

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APBlind : APControl, Parcelable {
    var mActions: List<APActionBlind>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionBlind)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionBlind()
            action.name = it.name
            action.description = it.description

            when (it.action) {
                BlindCommand.ON.action() -> {
                    action.action = BlindCommand.ON
                }
                BlindCommand.OFF.action() -> {
                    action.action = BlindCommand.OFF
                }
                BlindCommand.STOP.action() -> {
                    action.action = BlindCommand.STOP
                }
            }

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APBlind> {
        const val TYPE = "APBlind"

        override fun createFromParcel(parcel: Parcel): APBlind {
            val type = parcel.readString()
            return APBlind(parcel)
        }

        override fun newArray(size: Int): Array<APBlind?> {
            return arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.ui.homeupgrade.selector

import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeOrderRequest
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeRoom
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface HomeUpgradeSelectorContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun addOrderToList(hardware: HomeUpgradeHardware)
        fun requestSummary()

    }

    interface View : BaseView {
        fun bindData(rooms: List<HomeUpgradeRoom>)
        fun showLoading()
        fun hideLoading()
        fun showTotalPrice(totalPrice: Double)
        fun launchSummary(homeUpgradeRooms: ArrayList<HomeUpgradeRoom>, unitId: String)
    }
}
package com.sansiri.homeservice.model.api.tutorial

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TutorialPage(
        val button: Button? = null,
        val title: String? = null,
        val details: String? = null,
        val imageUrl: String? = null,
        val contentColorRGB: String? = null,
        val language: String? = null
) : Parcelable {
    @Parcelize
    data class Button(
            val text: String? = null,
            val actionType: String? = null
    ) : Parcelable {
        companion object {
            const val NEXT_PAGE = "NEXT_PAGE"
            const val CLOSE_PAGE = "CLOSE_PAGE"
        }
    }
}

package com.sansiri.homeservice.ui.mailbox

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.MailBox
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.mailbox.detail.MailBoxDetailActivity
import com.sansiri.homeservice.ui.mailbox.history.MailBoxHistoryActivity
import com.sansiri.homeservice.util.*
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import java.util.*

class MailBoxActivity : BaseActivity<MailBoxContract.View, MailBoxContract.Presenter>(), MailBoxContract.View {
    override var mPresenter: MailBoxContract.Presenter = MailBoxPresenter()

    val mAdapter = SectionedRecyclerViewAdapter()
    var mHome: Hut? = null

    companion object {
        val HOME = "HOME"
        val TITLE = "TITLE"
        fun start(activity: Activity, home: Hut, title: String?) {
            val intent = Intent(activity, MailBoxActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(TITLE, title)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setBackToolbar(intent.getStringExtra(TITLE) ?: getString(R.string.mailbox))

        textNoData.text = getString(R.string.no_mail)

        with(list) {
            adapter = mAdapter
        }

        mHome = intent?.getParcelableExtra(HOME)
        mPresenter.fetchData(mHome?.unitId ?: "")
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("MAIL_BOX")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_history, menu)
        menu.findItem(R.id.action_history).tint(ContextCompat.getColor(this, R.color.textAccent))
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_history -> MailBoxHistoryActivity.start(this, mHome!!, null)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun bindData(list: List<MailBox>) {
        val mapSection = hashMapOf<Date, MutableList<MailBox>>()

        list.forEach {
            val reportDate = it._createdAt?.resetTime()
            if(reportDate != null) {
                if (mapSection[reportDate] == null) {
                    mapSection.put(reportDate, mutableListOf(it))
                } else {
                    mapSection[reportDate]?.add(it)
                }
            }
        }

        mapSection.toSortedMap(compareByDescending<Date> { it }).forEach {
            val adapter = MailBoxSectionAdapter(it.key.report(), it.value.sortedByDescending { it._createdAt }) { mail ->
                MailBoxDetailActivity.start(this, mHome!!, mail)
            }
            mAdapter.addSection(adapter)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showNoData() {
        textNoData.show()
    }

    override fun showError(message: String) {
        alertError(message)
    }
}
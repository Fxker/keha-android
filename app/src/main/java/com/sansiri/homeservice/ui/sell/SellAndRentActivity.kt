package com.sansiri.homeservice.ui.sell

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Sell
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.sell.create.SellCreateActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_list.*

open class SellAndRentActivity : BaseActivity<SellContract.View, SellContract.Presenter>(), SellContract.View {
    override var mPresenter: SellContract.Presenter = SellPresenter()
    private var isRent: Boolean = true
    val mAdapter = SellHistoryAdapter()

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, SellAndRentActivity::class.java)
            activity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setBackToolbar(getString(R.string.sell_and_rent_out))

        list.apply {
            adapter = mAdapter
        }

        mPresenter.fetchData()
    }


    override fun onResume() {
        super.onResume()
        sendScreenView("Sell & Rent Out Screen")
    }
    override fun bindData(list: List<Sell>) {
        mAdapter.setData(list)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_create_new, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item.itemId == R.id.action_create) {
            SellCreateActivity.start(this, isRent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

package com.sansiri.homeservice.ui.homecare.history.detail

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homecare.HomeCareHistory
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_homecare_report.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class HomeCareHistoryDetailAdapter(val itemClick: () -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mData: HomeCareHistory? = null

    override fun getItemViewType(position: Int): Int {
        return when(position) {
            0 -> 0
            else -> 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            0 -> HomeCareSummaryReportViewHolder(parent.inflate(R.layout.view_homecare_summary_report_card))
            else -> {
                val view = parent.inflate(R.layout.view_homecare_report)
                initList(view.beforeList)
                initList(view.duringList)
                initList(view.afterList)
                HomeCareReportViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int = (mData?.subItems?.size ?: 0) + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is HomeCareSummaryReportViewHolder -> mData?.let { holder.bind(it) }
            is HomeCareReportViewHolder -> mData?.let {
                val subItem = it.subItems?.get(position - 1)
                if (subItem != null) {
                    holder.bind(it.statusBgColor, subItem)
                }
            }
        }
    }

    fun setData(data: HomeCareHistory) {
        mData = data
        notifyDataSetChanged()
    }

    private fun initList(list: RecyclerView) = list.apply {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        adapter = HomeCareHistorySubItemAttachmentAdapter(context.resources.getInteger(R.integer.grid_column))
    }

}
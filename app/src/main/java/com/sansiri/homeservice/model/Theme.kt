package com.sansiri.homeservice.model

import android.graphics.Color


data class Theme(
        val backgroundColor: String? = null,
        val titleColor: String? = null,
        val menuColor: String? = null,
        val menuTitleColor: String? = null,
        val accentColor: String? = null) {

    fun getMenuColor(): Int? =
            if (menuColor != null) {
                Color.parseColor(menuColor)
            } else {
                null
            }

    fun getMenuTitleColor(): Int? =
            if (menuTitleColor != null) {
                Color.parseColor(menuTitleColor)
            } else {
                null
            }

    fun getAccentColor(): Int? =
            if (accentColor != null) {
                Color.parseColor(accentColor)
            } else {
                null
            }

}
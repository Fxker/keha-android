package com.sansiri.homeservice.ui.sell.create

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.MenuItem
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.sell.create.detail.SellCreateDetailFragment
import com.sansiri.homeservice.util.addFragment
import com.sansiri.homeservice.util.replaceFragment
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.toolbar.*

class SellCreateActivity : LocalizationActivity() {

    companion object {
        val RENT = "RENT"
        fun start(activity: Activity, isRent: Boolean = true) {
            val intent = Intent(activity, SellCreateActivity::class.java)
            intent.putExtra(RENT, isRent)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        setBackToolbar("")

        selectScreen(SellCreateDetailFragment.newInstance(intent.getBooleanExtra(RENT, true)))
    }

    fun selectScreen(fragment: androidx.fragment.app.Fragment, isAddToBackStack: Boolean = false) {
        addFragment(fragment, isAddToBackStack)
    }

    fun setToolbarTitle(title: String) {
        textToolbar.text = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}

package com.sansiri.homeservice.model

data class HomeGoogleAssistant(
        var home: Home? = null,
        var email: String? = null
)
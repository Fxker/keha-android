package com.sansiri.homeservice.service

import android.content.SharedPreferences
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.database.PreferenceHelper

import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.menu.Feature
import me.leolin.shortcutbadger.ShortcutBadger
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by sansiri on 1/15/18.
 */
class AppFirebaseMessagingService : FirebaseMessagingService() {
    lateinit var prefs: SharedPreferences

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        prefs = PreferenceHelper.defaultPrefs(this)

        showBadgeCount()

        if (remoteMessage.notification != null) {
            checkNotification(remoteMessage.notification?.title ?: "Home", remoteMessage.notification?.body ?: "Click to open app", null, null)
        } else if (remoteMessage.data != null) {
            val data = remoteMessage.data
            checkNotification(
                    data["title"] ?: "Home",
                    data["message"] ?: "Click to open app",
                    data["type"],
                    data["data"]
            )
        }
    }

    fun checkNotification(title: String, body: String, type: String?, data: String?) {
        interceptNotificationAction(type)

        val isNotifyAll = prefs[PreferenceHelper.NOTIFICATION_ALL, true] ?: true

        if (isNotifyAll) {
            val color = resources.getColor(R.color.colorAccent)

            val helper = NotificationHelper(this, NotificationID.getId(type ?: "")).create(title, body, color, type, data)
            helper.submit()
        }
    }

    private fun showBadgeCount() {
        var notificationCount = prefs[PreferenceHelper.NOTIFICATION_COUNT, 0] ?: 0
        notificationCount += 1
        prefs[PreferenceHelper.NOTIFICATION_COUNT] = notificationCount
        ShortcutBadger.applyCount(this, notificationCount)
    }

    private fun interceptNotificationAction(type: String?) {
        if (type != null) {
            when (type) {
                Feature.ANNOUNCEMENT -> prefs[PreferenceHelper.NOTIFICATION_ANNOUNCEMENT] = true
            }
        }
    }

    object NotificationID {
        private val c = AtomicInteger(20)

        fun getId(type: String): Int {
            if (type == Feature.CHAT) {
                return 0
            }
            return c.incrementAndGet()
        }
    }

}
package com.appy.android.sdk.control.daikin

enum class DaikinFanSpeedCommand: APDaikinParameter {
    LOW {
        override fun action(): String {
            return "0"
        }
    },
    MEDIUM {
        override fun action(): String {
            return "1"
        }
    },
    HIGH {
        override fun action(): String {
            return "2"
        }
    };
}
package com.sansiri.homeservice.ui.homeupgrade.myorder

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.homeupgrade.HomeUpgradeHardware
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.service.NotificationHelper
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.homeupgrade.summary.HomeUpgradeSummarySectionAdapter
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.report
import com.sansiri.homeservice.util.setBackToolbar
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_home_upgrade_my_order.*

class HomeUpgradeMyOrderActivity : BaseActivity<HomeUpgradeMyOrderContract.View, HomeUpgradeMyOrderContract.Presenter>(), HomeUpgradeMyOrderContract.View {
    override var mPresenter: HomeUpgradeMyOrderContract.Presenter = HomeUpgradeMyOrderPresenter()
    val mAdapter =  SectionedRecyclerViewAdapter()

    companion object {
        val HISTORY = "HISTORY"
        fun start(activity: Activity, history: HomeUpgradeHistory) {
            val intent = Intent(activity, HomeUpgradeMyOrderActivity::class.java)
            intent.putExtra(HISTORY, history)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_upgrade_my_order)

        setBackToolbar(getString(R.string.order_summary))

        val history = intent.getParcelableExtra<HomeUpgradeHistory>(HISTORY)

        list.apply {
            val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@HomeUpgradeMyOrderActivity)
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }

        mPresenter.init(history)
        mPresenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun bindData(history: HomeUpgradeHistory) {
        textStatus.text = getString(
                when (history.status) {
                    "in-process" -> R.string.in_progress
                    "awaiting-appointment" -> R.string.pending_appointment
                    "awaiting-installation" -> R.string.pending_installation
                    else -> R.string.processing
                })
        if (history.installation?.date != null) {
            textDate.text = history.installation._date?.report("d MMM yyyy HH:mm")
        } else {
            textDateTitle.hide()
            textDate.hide()
        }
        textName.text = history.customer?.name
        textPhoneNumber.text = history.customer?.phone
        textEmail.text = history.customer?.email
        if (history.vendor?.contact != null) {
            buttonContact.setOnClickListener {
                val builder = AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Dialog)
                val view = layoutInflater.inflate(R.layout.dialog_home_upgrade_contact, null)
                view.findViewById<TextView>(R.id.textName).text = history.vendor.contact.name
                view.findViewById<TextView>(R.id.textPhone).text = history.vendor.contact.phone
                view.findViewById<TextView>(R.id.textEmail).text = history.vendor.contact.email

                view.findViewById<View>(R.id.buttonPhone).setOnClickListener {
                    launchPhoneCall(history.vendor.contact.phone ?: "")
                }

                view.findViewById<View>(R.id.buttonEmail).setOnClickListener {
                    launchEmail(history.vendor.contact.email ?: "")
                }

                builder.setView(view).create().show()
            }
        } else {
            buttonContact.hide()
        }
    }

    override fun bindOrderList(summary: HashMap<String, MutableList<HomeUpgradeHardware>>) {
        for((key, value) in summary) {
            val adapter = HomeUpgradeSummarySectionAdapter(key, value)
            mAdapter.addSection(adapter)
        }

        mAdapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
    }

    private fun launchPhoneCall(tel: String) {
        try {
            val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
            startActivity(phoneIntent)
        } catch (e: ActivityNotFoundException) {
            // showError("Your device doesn't support this feature.")
        }
    }

    private fun launchEmail(email: String) {
        try {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$email"))
            startActivity(emailIntent)
        } catch (e: ActivityNotFoundException) {
            // showError("Your device doesn't support this feature.")
        }
    }
}

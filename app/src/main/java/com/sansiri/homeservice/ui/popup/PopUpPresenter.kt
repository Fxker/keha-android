package com.sansiri.homeservice.ui.popup

import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe

class PopUpPresenter : BasePresenterImpl<PopUpContract.View>(), PopUpContract.Presenter {
    override fun start() {
    }

    override fun destroy() {
    }

}
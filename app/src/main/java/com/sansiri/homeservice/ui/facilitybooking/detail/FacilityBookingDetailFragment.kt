package com.sansiri.homeservice.ui.facilitybooking.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.bottomsheet.TimePickerBottomSheet
import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.TimesItem
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.fragment_booking_detail.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.collections.forEachWithIndex
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("ValidFragment")
class FacilityBookingDetailFragment() : BaseFragment<FacilityBookingDetailContract.View, FacilityBookingDetailContract.Presenter>(), FacilityBookingDetailContract.View {
    override var mPresenter: FacilityBookingDetailContract.Presenter = FacilityBookingDetailPresenter()

    constructor(onSubmit: ((Booking) -> Unit)) : this() {
        this.onSubmit = onSubmit
    }

    //    private var mStartTime: Calendar? = null
//    private var mEndTime: Calendar? = null
    private var onSubmit: ((Booking) -> Unit)? = null

    //    private lateinit var mTimeStartPicker: TimePickerBottomSheet
//    private lateinit var mTimeEndPicker: TimePickerBottomSheet
    private var mBooking: Booking? = null
    private var isEditMode = false
    private var mProjectId: String = ""
    private var mUnitId: String = ""
    private var mAddress: String? = null
    private var mFacility: FacilityBooking? = null
    private var mTimeline: List<TimesItem>? = null
    private var isAgree = false
        set(value) {
            if (value) {
                buttonCreate.isEnabled = true
                buttonCreate.alpha = 1.0f
            } else {
                buttonCreate.isEnabled = false
                buttonCreate.alpha = 0.8f
            }
        }

    companion object {
        val PROJECT_ID = "PROJECT_ID"
        val UNIT_ID = "UNIT_ID"
        val FACILITY = "FACILITY"
        val BOOKING = "BOOKING"
        val TIMELINE = "TIMELINE"
        val ADDRESS = "ADDRESS"
        val EDIT_MODE = "EDIT_MODE"

        fun newInstance(projectId: String, unitId: String, address: String, facility: FacilityBooking, booking: Booking, timeline: List<TimesItem>? = null, isEditMode: Boolean = false, onSubmit: ((Booking) -> Unit)): FacilityBookingDetailFragment {
            val fragment = FacilityBookingDetailFragment(onSubmit)
            val bundle = Bundle()
            bundle.putString(PROJECT_ID, projectId)
            bundle.putString(UNIT_ID, unitId)
            bundle.putParcelable(FACILITY, facility)
            bundle.putParcelable(BOOKING, booking)
            bundle.putString(ADDRESS, address)
            timeline?.let {
                bundle.putParcelableArrayList(TIMELINE, ArrayList(timeline))
            }
            bundle.putBoolean(EDIT_MODE, isEditMode)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mProjectId = arguments?.getString(PROJECT_ID) ?: ""
        mUnitId = arguments?.getString(UNIT_ID) ?: ""
        mFacility = arguments?.getParcelable(FACILITY)
        mAddress = arguments?.getString(ADDRESS)
        mTimeline = arguments?.getParcelableArrayList<TimesItem>(TIMELINE)
        mBooking = arguments?.getParcelable(BOOKING)
        isEditMode = arguments?.getBoolean(EDIT_MODE) ?: false
    }

    override fun onResume() {
        super.onResume()
        if (isEditMode) {
            sendScreenView("Facility Booking Edit Screen")
        } else {
            sendScreenView("Facility Booking Create Screen")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (mFacility != null && mBooking != null) {
            mPresenter.init(mProjectId, mUnitId, mAddress ?: "", mFacility!!, mBooking!!)
        } else {
            // TODO: showError
        }

        textTimeStart.text = getTextTime(mBooking?.startTime)
        textTimeEnd.text = getTextTime(mBooking?.endTime)
        textDate.text = mBooking?.startTime?.reportDate()

//        mTimeStartPicker = TimePickerBottomSheet(mBooking?.startTime!!) { time ->
//            textTimeStart.text = getTextTime(time)
//        }

//        mTimeEndPicker = TimePickerBottomSheet(mBooking?.endTime!!) { time ->
//            textTimeEnd.text = getTextTime(time)
//        }


        if (isEditMode) {
            showEditMode()
        } else {
            textTimeStart.background = null
            textTimeStart.setTextColor(resources.getColor(R.color.textPrimary))
            if (mTimeline != null) {
                textTimeEnd.hide()
                spinnerTimeEnd.show()

                var allowIndex = 0
                run loop@{
                    mTimeline?.forEachWithIndex { index, timesItem ->
                        if (timesItem._endTime?.reportTime() == mBooking?.endTime?.reportTime()) {
                            allowIndex = index
                            return@loop
                        }
                    }
                }

                var listEndTime = mTimeline!!.subList(allowIndex, mTimeline!!.size)
                val maximumSlotPerBooking = mFacility?.categoryData?.reservation?.rules?.maximumSlotPerBooking
                if (maximumSlotPerBooking != null && maximumSlotPerBooking < listEndTime.size) {
                    listEndTime = listEndTime.subList(0, maximumSlotPerBooking)
                }


                val adapter = TimeSpinnerAdapter(context!!, R.layout.view_text_facility_time, listEndTime)
                spinnerTimeEnd.adapter = adapter


            } else {
                textTimeEnd.show()
                textTimeEnd.background = null
                textTimeEnd.text = getTextTime(mBooking?.endTime)
            }

            this.isAgree = false
            checkbox.setOnCheckedChangeListener { _, isChecked ->
                this.isAgree = isChecked
            }

            mPresenter.requestTermsAndConditions()

            buttonCreate.setOnClickListener {
                buttonCreate.isEnabled = false
                val selectedEndTime = spinnerTimeEnd.selectedItem as TimesItem
                selectedEndTime.endTime?.let { mPresenter.submit(editNote.text.toString(), selectedEndTime.endTime) }
            }
        }

        mPresenter.start()
    }

    private fun showEditMode() {
        editNote.hide()
        textTitleTerm.hide()
        panelTerm.hide()
        buttonCreate.hide()
        textTimeStart.background = null
        textTimeStart.setTextColor(resources.getColor(R.color.textPrimary))
        textTimeEnd.background = null
        textTimeEnd.setTextColor(resources.getColor(R.color.textPrimary))

        textNote.text = mBooking?.note
        textNote.show()
        buttonCancel.show()
        buttonCancel.setOnClickListener {
            buttonCancel.isEnabled = false
            launchConfirmDialog()
        }
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun showTermsAndConditions(text: String) {
        textTerm.text = text
//        alert(text, getString(R.string.terms_and_conditions)) {
//            yesButton { }
//        }.show()
    }

    private fun getTextTime(time: Calendar?): String {
        return "${time?.get(Calendar.HOUR_OF_DAY)?.format(2)}:${time?.get(Calendar.MINUTE)?.format(2)}"
    }

    private fun launchConfirmDialog() {
        CancelBookingConfirmDialogFragment.newInstance({
            mPresenter.cancelBooking()
        }).show(fragmentManager!!, "BookingDialogFragment")
    }

    override fun success() {
        onSubmit?.invoke(mBooking!!)
        fragmentManager?.popBackStack()
    }

    override fun showLoading() {
        checkbox.isEnabled = false
        editNote.isEnabled = false
        if (isEditMode) buttonCancel.hide() else buttonCreate.hide()
        progressBar.show()
    }

    override fun hideLoading() {
        checkbox.isEnabled = true
        editNote.isEnabled = true
        buttonCreate.isEnabled = true
        buttonCancel.isEnabled = true
        if (isEditMode) buttonCancel.show() else buttonCreate.show()
        progressBar.hide()
    }


    override fun showError(text: String) {
        activity?.alert(text, getString(R.string.error_something_went_wrong)) {
            yesButton { }
        }?.show()
    }

    override fun showError(res: Int) {
        showError(getString(res))
    }

}

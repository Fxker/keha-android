package com.sansiri.homeservice.data.network.auth

import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterDetail
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterHistoryList
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterOverAll
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterHistoryList
import com.sansiri.homeservice.model.api.meter.water.SmartWaterMeterOverAll
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by sansiri on 10/24/17.
 */
interface SmartMeterService {
    @GET("me/units/{unitId}/partners/sansiri/smart/meter/water/{meterId}/statistics/overall")
    fun getSmartWaterMeterSummary(@Path("unitId") unitId: String, @Path("meterId") meterId: String, @Query("includes") includes: String = "graph", @Query("dateTime") dateTime: String): Deferred<Response<SmartWaterMeterOverAll>>

    @GET("me/units/{unitId}/partners/sansiri/smart/meter/water/{meterId}/statistics/history")
    fun getSmartWaterMeterHistory(
            @Path("unitId") unitId: String,
            @Path("meterId") meterId: String,
            @Query("dateType") dateType: String,
            @Query("dateTime") dateTime: String): Deferred<Response<SmartWaterMeterHistoryList>>

    @GET("me/units/{unitId}/partners/sansiri/smart/meter/electric/{meterId}/statistics/overall")
    fun getSmartElectricMeterSummary(@Path("unitId") unitId: String, @Path("meterId") meterId: String, @Query("includes") includes: String = "graph", @Query("dateTime") dateTime: String): Deferred<Response<SmartElectricMeterOverAll>>

    @GET("me/units/{unitId}/partners/sansiri/smart/meter/electric/{meterId}/statistics/last")
    fun getSmartElectricMeterLast(@Path("unitId") unitId: String, @Path("meterId") meterId: String, @Query("includes") includes: String = "graph", @Query("dateTime") dateTime: String): Deferred<Response<SmartElectricMeterDetail>>

    @GET("me/units/{unitId}/partners/sansiri/smart/meter/electric/{meterId}/statistics/history")
    fun getSmartElectricMeterHistory(
            @Path("unitId") unitId: String,
            @Path("meterId") meterId: String,
            @Query("dateType") dateType: String,
            @Query("dateTime") dateTime: String): Deferred<Response<SmartElectricMeterHistoryList>>
}
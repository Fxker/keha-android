package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.coolautomation.CoolAutomationCommand
import com.appy.android.sdk.control.coolautomation.FanSwingCommand

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
internal class SendCommandCoolAutomationFanSwingForm(
        val homeId: String,
        val coolId: String,
        val commandParam: FanSwingCommand
) : AppyForm() {
    val mSendCommandCoolAutomationFanSwingJsonBody = SendCommandCoolAutomationFanSwingJsonBody()

    init {
        this.mSendCommandCoolAutomationFanSwingJsonBody.command = CoolAutomationCommand.FAN_SWING.action()
        this.mSendCommandCoolAutomationFanSwingJsonBody.commandParam = commandParam.action()
    }
}
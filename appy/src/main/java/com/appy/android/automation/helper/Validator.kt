package com.appy.android.automation.helper

import android.text.Editable

class Validator {

    companion object {

        @JvmStatic
        fun isValid(string: String?): Boolean {
            return string != null && string.trim { it <= ' ' } != "" && string.trim { it <= ' ' } != "null"
        }

        @JvmStatic
        fun isValid(editable: Editable?): Boolean {
            return if (editable == null) false else isValid(editable.toString())
        }

        @JvmStatic
        fun isValidEmail(email: String, allowBlank: Boolean): Boolean {
            return if (!isValid(email)) allowBlank else android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        @JvmStatic
        fun isValidEmail(editable: Editable, allowBlank: Boolean): Boolean {
            return isValidEmail(editable.toString(), allowBlank)
        }

        @JvmStatic
        fun isValid(list: List<*>?): Boolean {
            return list != null && list.isNotEmpty()
        }
    }
}
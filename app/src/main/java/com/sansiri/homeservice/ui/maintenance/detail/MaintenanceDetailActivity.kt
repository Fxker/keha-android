package com.sansiri.homeservice.ui.maintenance.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.webkit.GeolocationPermissions
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.sansiri.homeservice.R
import com.sansiri.homeservice.component.UploadAlertDialog
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.maintenance.MaintenancePlan
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.maintenance.MaintenanceActivity
import com.sansiri.homeservice.util.*
import kotlinx.android.synthetic.main.activity_maintenance_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class MaintenanceDetailActivity : BaseActivity<MaintenanceDetailContract.View, MaintenanceDetailContract.Presenter>(), MaintenanceDetailContract.View {
    override var mPresenter: MaintenanceDetailContract.Presenter = MaintenanceDetailPresenter()
    private lateinit var mUploadDialog: UploadAlertDialog

    companion object {
        const val HOME = "HOME"
        const val QUERY = "QUERY"
        const val CODE = 11
        const val PLAN_ID = "PLAN_ID"
        const val PLAN = "PLAN"
        const val ACCESSORY_ID = "ACCESSORY_ID"

        fun start(activity: Activity, home: Hut, accessoryId: String, planId: String?, plan: MaintenancePlan?) {
            val intent = Intent(activity, MaintenanceDetailActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(PLAN_ID, planId)
            intent.putExtra(PLAN, plan)
            intent.putExtra(ACCESSORY_ID, accessoryId)
            activity.startActivityForResult(intent, CODE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maintenance_detail)

        val home = intent.getParcelableExtra<Hut>(HOME)
        val plan = intent.getParcelableExtra<MaintenancePlan>(PLAN)
        val planId = intent.getStringExtra(MaintenanceActivity.PLAN_ID)
        val accessoryId = intent.getStringExtra(MaintenanceActivity.ACCESSORY_ID)

        setBackToolbar(plan?.accessoryName ?: "")

        buttonSubmit.setOnClickListener {
            submit()
        }

        mUploadDialog = UploadAlertDialog(this)

        mPresenter.init(home, accessoryId, planId, plan)
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("${Feature.MAINTENANCE_GUIDE}_DETAIL")
    }


    override fun bindData(plan: MaintenancePlan?) {
        if (plan != null) {
            textToolbar.text = plan.accessoryName
            textDate.text = plan._planDatetime?.reportDMYFull()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showWeb(html: String) {
        webView.loadData(html, "text/html", "UTF-8")
        webView.settings.javaScriptEnabled = true

        val finalHtml = "<link rel=\"stylesheet\" type=\"text/css\" href=\"classic.css\"/>$html"
        webView.loadHtml(finalHtml, "file:///android_asset/css/")
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                progressBar?.progress = newProgress
                if (newProgress == 100) {
                    progressBar?.hide()
                } else {
                    progressBar?.show()
                }
            }

            override fun onGeolocationPermissionsShowPrompt(origin: String?, callback: GeolocationPermissions.Callback?) {
                callback?.invoke(origin, true, false)
            }
        }
    }

    override fun showLoading() {
        mUploadDialog.showUploading(getString(R.string.processing))
    }

    override fun showSubmitSuccess(accessoryId: String) {
        sendEvent("${Feature.MAINTENANCE_GUIDE}_DETAIL", "CLICK", "MAINTENANCE_SUCCESS", "2", Pair("AccessoryId", accessoryId))

        mUploadDialog.showDone(getString(R.string.maintenance_completed))
        ({
            mUploadDialog.hide()
            val returnIntent = Intent()
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }).withDelay(2000)
    }

    override fun showError(message: String) {
        alertError(message)
    }

    private fun submit() {
        alert(getString(R.string.maintenance_done_confirm_message), getString(R.string.maintenance_done)) {
            yesButton {
                mPresenter.submit()
            }
            noButton {  }

        }.show()
    }
}

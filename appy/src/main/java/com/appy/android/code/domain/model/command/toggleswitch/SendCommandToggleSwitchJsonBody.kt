package com.appy.android.code.domain.model.command.toggleswitch

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody

internal class SendCommandToggleSwitchJsonBody : BaseCommandJsonBody<String>() {

}
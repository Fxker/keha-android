package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.sansiri.homeservice.R
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashCredit
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.TrendyWashPaymentGatewayActivity
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.fragment_trendy_wash_add_money.*
import org.koin.androidx.scope.currentScope


class TrendyWashAddMoneyFragment : BaseV2Fragment<TrendyWashAddMoneyContract.View, TrendyWashAddMoneyContract.Presenter>(), TrendyWashAddMoneyContract.View {
    override val mPresenter: TrendyWashAddMoneyContract.Presenter by currentScope.inject()
    private var mUnitId: String? = null
    private var mUserId: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"

        fun newInstance(unitId: String, userId: String): TrendyWashAddMoneyFragment {
            return TrendyWashAddMoneyFragment().apply {
                arguments = Bundle().apply {
                    putString(UNIT_ID, unitId)
                    putString(USER_ID, userId)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
        mUserId = arguments?.getString(USER_ID)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRENDY_WASH_TOP_UP")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trendy_wash_add_money, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textToolbar.text = "เติมเงิน"
        buttonBack.setOnClickListener {
            activity?.onBackPressed()
        }

        mPresenter.init(mUnitId ?: "", mUserId ?: "")
        mPresenter.start()

        mPresenter.creditInfo?.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.LOADING -> {
                    if (resource.data != null) {
                        bindData(resource.data!!)
                    } else {
                        showLoading()
                    }
                }
                Resource.SUCCESS -> {
                    hideLoading()
                    resource.data?.let { bindData(it) }
                }
                Resource.ERROR -> {
                    hideLoading()
                    showError(resource.message ?: "Error")
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == TrendyWashPaymentGatewayActivity.CODE) {
            mPresenter.fetch()
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.action_qr)
        if (item != null)
            item.isVisible = false
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun bindData(creditInfo: TrendyWashCredit) {
        textBalance.text = creditInfo.remainCredits ?: "0"

        list.apply {
            layoutManager = GridLayoutManager(this.context, resources.getInteger(R.integer.grid_column))
            adapter = GridMenuAdapter() { amount ->
                if (mUnitId != null && mUserId != null) {
                    TrendyWashPaymentGatewayActivity.start(this@TrendyWashAddMoneyFragment, mUnitId!!, mUserId!!, amount)
                }
            }.apply {
                setData(creditInfo.creditList ?: listOf())
            }
        }
    }

    override fun showLoading() {
        progressBar?.show()
    }

    override fun hideLoading() {
        progressBar?.hide()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}
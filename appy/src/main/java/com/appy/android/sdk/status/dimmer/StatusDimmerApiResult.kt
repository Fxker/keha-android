package com.appy.android.sdk.status.dimmer

import com.appy.android.code.base.data.rest.result.AppyApiResult

/**
 * Created by ssa-dev-4 on 9/11/2560.
 */
class StatusDimmerApiResult : AppyApiResult<APStatusDimmer>()
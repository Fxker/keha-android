package com.appy.android.sdk.control.dimmer

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

/**
 * Created by ssa-dev-4 on 13/10/2560.
 */
class APDimmer : APControl, Parcelable {
    var mActions: List<APActionDimmer>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionDimmer)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = if (isFibaroModel()) {
            DimmerCommand.values().map {
                val action = APActionDimmer()
                action.action = it

                action
            }
        } else {
            controlJsonModel.actions?.map {
                val action = APActionDimmer()
                action.name = it.name
                action.description = it.description

                when (it.action) {
                    DimmerCommand.OFF.action() -> {
                        action.action = DimmerCommand.OFF
                    }
                    DimmerCommand.ON.action() -> {
                        action.action = DimmerCommand.ON
                    }
                    DimmerCommand.DIMMER10.action() -> {
                        action.action = DimmerCommand.DIMMER10
                    }
                    DimmerCommand.DIMMER20.action() -> {
                        action.action = DimmerCommand.DIMMER20
                    }
                    DimmerCommand.DIMMER30.action() -> {
                        action.action = DimmerCommand.DIMMER30
                    }
                    DimmerCommand.DIMMER40.action() -> {
                        action.action = DimmerCommand.DIMMER40
                    }
                    DimmerCommand.DIMMER50.action() -> {
                        action.action = DimmerCommand.DIMMER50
                    }
                    DimmerCommand.DIMMER60.action() -> {
                        action.action = DimmerCommand.DIMMER60
                    }
                    DimmerCommand.DIMMER70.action() -> {
                        action.action = DimmerCommand.DIMMER70
                    }
                    DimmerCommand.DIMMER80.action() -> {
                        action.action = DimmerCommand.DIMMER80
                    }
                    DimmerCommand.DIMMER90.action() -> {
                        action.action = DimmerCommand.DIMMER90
                    }
                    DimmerCommand.DIMMER100.action() -> {
                        action.action = DimmerCommand.DIMMER100
                    }
                }

                action
            }
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APDimmer> {
        const val TYPE = "APDimmer"

        override fun createFromParcel(parcel: Parcel): APDimmer {
            val type = parcel.readString()
            return APDimmer(parcel)
        }

        override fun newArray(size: Int): Array<APDimmer?> {
            return arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.model.api.facilitybooking

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Rules(
        @SerializedName("maximum_slot_per_booking")
        val maximumSlotPerBooking: Int? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(maximumSlotPerBooking)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Rules> = object : Parcelable.Creator<Rules> {
            override fun createFromParcel(source: Parcel): Rules = Rules(source)
            override fun newArray(size: Int): Array<Rules?> = arrayOfNulls(size)
        }
    }
}
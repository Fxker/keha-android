package com.sansiri.homeservice.ui.announcement.summary

import android.os.Build
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.util.*

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.view_announcement_summary.view.*

/**
 * Created by sansiri on 10/18/17.
 */
class SummaryAnnouncementAdapter(val itemClick: (View, String, Announcement<*>) -> Unit, val moreClick: (String, String) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<SummaryAnnouncementAdapter.ViewHolder>() {
    private var mData = mutableListOf<SummaryItem>()
    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.view_announcement_summary), itemClick, moreClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    class ViewHolder(val view: View, val itemClick: (View, String, Announcement<*>) -> Unit, val moreClick: (String, String) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(summaryItem: SummaryItem) {
            with(view) {
                textTitle.text = summaryItem.title
                textTitle.setOnClickListener {
                    moreClick(summaryItem.title, summaryItem.id)
                }
                imageNext.setOnClickListener { moreClick(summaryItem.title, summaryItem.id) }

                val viewPrimary = findViewById<View>(R.id.primaryAnnouncement)
                val viewSub = findViewById<View>(R.id.subAnnouncement)
                val viewSub2 = findViewById<View>(R.id.subAnnouncement2)

                val announcement = arrayListOf<Announcement<*>>()
                announcement.addAll(summaryItem.announcement)
                announcement.sortByDescending { it.displayDate }

                val firstAnnouncement = announcement.getOrNull(0)
                val secondAnnouncement = announcement.getOrNull(1)
                val thirdAnnouncement = announcement.getOrNull(2)

                checkAnnouncementIsExist(summaryItem.title, firstAnnouncement, viewPrimary)
                if (secondAnnouncement == null && thirdAnnouncement == null) {
                    checkAnnouncementIsExist(summaryItem.title, secondAnnouncement, viewSub, true)
                    checkAnnouncementIsExist(summaryItem.title, thirdAnnouncement, viewSub2, true)
                } else {
                    checkAnnouncementIsExist(summaryItem.title, secondAnnouncement, viewSub)
                    checkAnnouncementIsExist(summaryItem.title, thirdAnnouncement, viewSub2)
                }
            }
        }

        private fun checkAnnouncementIsExist(title: String, data: Announcement<*>?, view: View, isGone: Boolean = false) {
            if (data != null) {
                bindToSubView(title, view, data)
                view.show()
            } else {
                if (isGone) {
                    view.hide()
                } else {
                    view.invisible()
                }
            }
        }

        private fun bindToSubView(title: String, view: View, data: Announcement<*>) {
            val announcement = data.details?.getOrNull(0)

            val cover = view.findViewById<ImageView>(R.id.imageThumbnail)
            Glide.with(view.context).loadCache(announcement?.coverImageUrl ?: "", cover)
            view.findViewById<TextView>(R.id.textTitle).text = announcement?.title
            view.findViewById<TextView>(R.id.textSubtitle).text = announcement?.subTitle
            view.findViewById<TextView>(R.id.textDate).text = announcement?.displayDate?.report()
            view.setOnClickListener {
                itemClick(cover, title, data)
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                view.findViewById<androidx.cardview.widget.CardView>(R.id.card).preventCornerOverlap = false
            }
        }
    }

    fun setData(list: List<SummaryItem>) {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }

    fun addData(summary: SummaryItem) {
        mData.add(summary)
        notifyDataSetChanged()
    }


    fun addDataAt(position: Int, summary: SummaryItem) {
        if (mData.isEmpty()) {
            mData.add(summary)
        } else {
            mData.add(position, summary)
        }
        notifyDataSetChanged()
    }

}
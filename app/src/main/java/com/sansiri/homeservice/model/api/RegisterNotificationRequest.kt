package com.sansiri.homeservice.model.api

data class RegisterNotificationRequest(
	val fcmToken: String? = null,
	val langCode: String? = null,
	val countryCode: String? = null,
	val platform: String = "android"
)

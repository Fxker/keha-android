package com.sansiri.homeservice.ui.epayment.options

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentBarcodeActivity
import com.sansiri.homeservice.ui.downpayment.payment.DownpaymentEPaymentQRActivity
import com.sansiri.homeservice.ui.epayment.gateway.PaymentGatewayActivity
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.capture
import com.sansiri.homeservice.util.glide.loadCache
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.toByteArray
import kotlinx.android.synthetic.main.activity_downpayment_epayment.textTitle
import kotlinx.android.synthetic.main.fragment_epayment_options.*
import kotlinx.android.synthetic.main.view_payment_button.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.textColor
import org.koin.androidx.scope.currentScope
import java.lang.Exception

class EPaymentOptionsFragment : BaseV2Fragment<EPaymentOptionsContract.View, EPaymentOptionsContract.Presenter>(), EPaymentOptionsContract.View {
    override val mPresenter: EPaymentOptionsContract.Presenter by currentScope.inject()
    private var mDialogLoading: ProgressDialog? = null
    private var mPaymentGatewayData: PaymentGatewayData? = null
    private var mDetailImage: ByteArray? = null
    private var isShowBarcodePayment: Boolean = false
    private var analyticTitle: String? = null
    private var mPaymentChannels: List<PaymentChannel>? = null
    private var mListener: OnFragmentInteractionListener? = null

    companion object {
        const val PAYMENT_CHANNEL = "PAYMENT_CHANNEL"
        const val PAYMENT_GATEWAY_DATA = "PAYMENT_GATEWAY_DATA"
        const val DETAIL_IMAGE = "DETAIL_IMAGE"
        const val IS_SHOW_BARCODE_PAYMENT = "IS_SHOW_BARCODE_PAYMENT"
        const val ANALYTIC_TITLE = "ANALYTIC_TITLE"

        fun newInstance(paymentChannels: List<PaymentChannel>, paymentGatewayData: PaymentGatewayData, detailImage: ByteArray?, isShowBarcodePayment: Boolean, analyticTitle: String): EPaymentOptionsFragment {
            return EPaymentOptionsFragment().apply {
                arguments = Bundle().apply {
                    val array = arrayListOf<PaymentChannel>()
                    array.addAll(paymentChannels)

                    putParcelableArrayList(PAYMENT_CHANNEL, array)
                    putParcelable(PAYMENT_GATEWAY_DATA, paymentGatewayData)
                    putByteArray(DETAIL_IMAGE, detailImage)
                    putBoolean(IS_SHOW_BARCODE_PAYMENT, isShowBarcodePayment)
                    putString(ANALYTIC_TITLE, analyticTitle)
                }
            }
        }
    }

    interface OnFragmentInteractionListener {
        fun onBarcodeClicked()
        fun onQRClicked()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPaymentChannels = arguments?.getParcelableArrayList<PaymentChannel>(PAYMENT_CHANNEL)
        mPaymentGatewayData = arguments?.getParcelable<PaymentGatewayData>(PAYMENT_GATEWAY_DATA)
        mDetailImage = arguments?.getByteArray(DETAIL_IMAGE)
        isShowBarcodePayment = arguments?.getBoolean(IS_SHOW_BARCODE_PAYMENT, false) ?: false
        analyticTitle = arguments?.getString(ANALYTIC_TITLE)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_epayment_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isShowBarcodePayment) {
            buttonBarcode.setOnClickListener {
                mListener?.onBarcodeClicked()
            }
            buttonQR.setOnClickListener {
                mListener?.onQRClicked()
            }
        } else {
            buttonBarcode.hide()
            buttonQR.hide()
        }

        mPresenter.init(mPaymentChannels, mPaymentGatewayData)
        mPresenter.start()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }


    override fun showOtherPaymentOptions(paymentChannels: List<PaymentChannel>) {
        paymentChannels.forEach { paymentChannel ->
            val mainColor = try {
                Color.parseColor(paymentChannel.bgColorRGB)
            } catch (e: Exception) {
                null
            }

            val accentColor = try {
                Color.parseColor(paymentChannel.textColorRGB)
            } catch (e: Exception) {
                null
            }

            val view = (activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_payment_button, layoutPaymentOptions, false).apply {
                if (paymentChannel.title != null) textTitle.text = paymentChannel.title else textTitle.hide()
                if (paymentChannel.subTitle != null) textSubTitle.text = paymentChannel.subTitle else textSubTitle.hide()
                if (paymentChannel.iconUrl != null) Glide.with(context).loadCache(paymentChannel.iconUrl, imageIcon) else imageIcon.hide()
                mainColor?.let { card.setCardBackgroundColor(mainColor) }
                accentColor?.let {
                    textTitle.textColor = accentColor
                    textSubTitle.textColor = accentColor
                }
                layoutClickable.setOnClickListener {
                    analyticTitle?.let { sendEvent("${analyticTitle}_EPAYMENT", "CLICK", paymentChannel.channel) }
                    mPresenter.takeAction(paymentChannel)
                }
            }

            layoutPaymentOptions.addView(view)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PaymentGatewayActivity.PHONE_CODE -> {
                    val response = data?.getParcelableExtra<PaymentResponse>(PaymentGatewayActivity.RESPONSE)
                    if (response != null) {
                        mPresenter.handleResponse(response)
                    }
                }
                PaymentGatewayActivity.GATEWAY_2C2P_CODE -> {
                    activity?.finish()
                }
            }
        }

    }

    override fun launchWeb(url: String) {
        WebActivity.start(this, url, "")
    }


    override fun launchPhoneVerify(paymentGateWayData: PaymentGatewayData, paymentChannel: PaymentChannel) {
        val mainColor = try {
            Color.parseColor(paymentChannel.bgColorRGB)
        } catch (e: Exception) {
            null
        }

        val accentColor = try {
            Color.parseColor(paymentChannel.textColorRGB)
        } catch (e: Exception) {
            null
        }

        if (paymentChannel.command != null) {
            paymentGateWayData.apply {
                command = paymentChannel.command
                merchantName = paymentChannel.merchantName
                gatewayName = paymentChannel.gatewayName
                mainThemeColor = mainColor
                accentThemeColor = accentColor
                icon = paymentChannel.iconUrl
            }
            PaymentGatewayActivity.startPhoneVerify(this,
                    paymentGateWayData.apply {
                        command = paymentChannel.command
                        merchantName = paymentChannel.merchantName
                        gatewayName = paymentChannel.gatewayName
                        mainThemeColor = mainColor
                        accentThemeColor = accentColor
                        icon = paymentChannel.iconUrl
                    })
        } else {
            showError(getString(R.string.error_default_message))
        }
    }

    override fun showLoading() {
        mDialogLoading = activity?.indeterminateProgressDialog(getString(R.string.loading), null)
    }

    override fun hideLoading() {
        mDialogLoading?.dismiss()
    }

    override fun showPopup(title: String, message: String, onSuccess: () -> Unit) {
        activity?.alert(message, title) {
            okButton {
                onSuccess.invoke()
            }
        }?.show()
    }

    override fun launch2C2P(response: PaymentResponse, paymentChannel: PaymentChannel?) {
        PaymentGatewayActivity.start2c2p(this, paymentChannel, response, mDetailImage, analyticTitle)
    }

    override fun showError(message: String) {
    }
}
package com.sansiri.homeservice.model.api.meter.water

import com.sansiri.homeservice.util.toCurrencyFormat

data class SmartWaterMeterHistoryList(
        val count: Int = 0,
        val items: List<SmartWaterMeterDetail>,
        val title: String? = null,
        val dateType: String? = null,
        val timestamp: String? = null,
        val isFillEmptyData: Boolean = false,
        val liter: Double,
        val literDisplay: String,
        val costEstimate: Double,
        val costEstimateDisplay: String) {
    val displayLiter: String
        get() {
            return liter.toCurrencyFormat()
        }
}
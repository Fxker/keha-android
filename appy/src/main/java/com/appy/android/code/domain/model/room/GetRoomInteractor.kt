package com.appy.android.code.domain.model.room

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.room.RoomApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
internal class GetRoomInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<RoomApiResult, RoomForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: RoomForm): Observable<RoomApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getRoom(params.headers, params.homeId, params.roomId, params.params)
    }
}
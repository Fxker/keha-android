package com.sansiri.homeservice.ui.home

import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.transition.Fade
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.TodaySummary
import com.sansiri.homeservice.model.api.announcement.Announcement
import com.sansiri.homeservice.model.api.announcement.ProjectAnnouncementDetail
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.model.menu.Section
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.announcement.content.ContentActivity
import com.sansiri.homeservice.ui.appointment.AppointmentActivity
import com.sansiri.homeservice.ui.chat.ChatActivity
import com.sansiri.homeservice.ui.contact.ContactDialogFragment
import com.sansiri.homeservice.ui.downpayment.DownPaymentActivity
import com.sansiri.homeservice.ui.facilitybooking.FacilityBookingActivity
import com.sansiri.homeservice.ui.homecare.create.HomeCareCreateActivity
import com.sansiri.homeservice.ui.homecare.history.HomeCareHistoryActivity
import com.sansiri.homeservice.ui.inspection.InspectionActivity
import com.sansiri.homeservice.ui.mailbox.MailBoxActivity
import com.sansiri.homeservice.ui.myaccount_v2.MyAccountActivity
import com.sansiri.homeservice.ui.phonedirectory.PhoneDirectoryActivity
import com.sansiri.homeservice.ui.projectprogress.ProjectProgressActivity
import com.sansiri.homeservice.ui.sell.SellActivity
import com.sansiri.homeservice.ui.sell.SellAndRentActivity
import com.sansiri.homeservice.ui.service.MenuServiceSectionActivity
import com.sansiri.homeservice.ui.suggestion.SuggestionActivity
import com.sansiri.homeservice.ui.transfer.TransferActivity
import com.sansiri.homeservice.ui.voicecommand.VoiceCommandActivity
import com.sansiri.homeservice.ui.web.WebActivity
import com.sansiri.homeservice.util.*
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.Orientation
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.view_home_section.view.*
import android.view.Gravity
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import com.bumptech.glide.Glide
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.gson.Gson
import com.rd.animation.type.AnimationType
import com.sansiri.homeservice.data.FeatureConfig
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.data.network.RuntimeCache
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.Theme
import com.sansiri.homeservice.model.api.partner.aqi.Air
import com.sansiri.homeservice.model.api.homeupgrade.history.HomeUpgradeHistory
import com.sansiri.homeservice.model.api.notification.PushNotificationData
import com.sansiri.homeservice.model.database.orvibo.OrviboHome
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Feature
import com.sansiri.homeservice.ui.adapter.GridMenuDynamicAdapter
import com.sansiri.homeservice.ui.adapter.GridMenuHorizontalAdapter
import com.sansiri.homeservice.ui.aqi.AqiActivity
import com.sansiri.homeservice.ui.automation.appy.HomeAutomationActivity
import com.sansiri.homeservice.ui.automation.bathome.widget.BAtHomeWidgetFragment
//import com.sansiri.homeservice.ui.automation.orvibo.control.OrviboHomeAutomationActivity
//import com.sansiri.homeservice.ui.automation.orvibo.home.OrviboHomeActivity
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.bos.BosActivity
import com.sansiri.homeservice.ui.electric.mea.MEAElectricMainActivity
import com.sansiri.homeservice.ui.homeupgrade.HomeUpgradeTutorialActivity
import com.sansiri.homeservice.ui.homeupgrade.myorder.HomeUpgradeMyOrderActivity
import com.sansiri.homeservice.ui.ibox.list.IBoxListActivity
import com.sansiri.homeservice.ui.ibox.register.IBoxRegisterActivity
import com.sansiri.homeservice.ui.maintenance.MaintenanceActivity
import com.sansiri.homeservice.ui.meter.electric.SmartElectricMeterActivity
import com.sansiri.homeservice.ui.meter.water.SmartWaterMeterActivity
import com.sansiri.homeservice.ui.parking.mechanicalparking.MechanicalParkingActivity
import com.sansiri.homeservice.ui.myhome.MyHomeActivity
import com.sansiri.homeservice.ui.parking.smartparking.SmartParkingActivity
import com.sansiri.homeservice.ui.projectlist.ProjectListActivity
import com.sansiri.homeservice.ui.svvh.SVVHActivity
import com.sansiri.homeservice.ui.visitor.VisitorAccessActivity
import com.sansiri.homeservice.ui.washingmachine.trendywash.TrendyWashMainActivity
import com.sansiri.homeservice.ui.welcome.WelcomeActivity

import com.sansiri.homeservice.util.glide.loadCache
import kotlinx.android.synthetic.main.fragment_home.buttonUpgradeHome
import org.jetbrains.anko.*
import org.koin.android.ext.android.inject

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class HomeFragment : BaseV2Fragment<HomeContract.View, HomeContract.Presenter>(), HomeContract.View, DiscreteScrollView.OnItemChangedListener<ProjectAdapter.ViewHolder> {
    override val mPresenter: HomeContract.Presenter by inject()
    private var mTheme: Theme? = null

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    var smoothScroller: androidx.recyclerview.widget.RecyclerView.SmoothScroller? = null
    private var logoutProgressDialog: ProgressDialog? = null

    val mHomeAdapter = ProjectAdapter() { home ->
    }

    val mSummaryAdapter = SummaryAdapter() {
        //mPresenter.selectSummary(it)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_home)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // close voice command button
        buttonVoiceCommand.hide()


        smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }
        }

        with(projectPicker) {
            setOrientation(Orientation.HORIZONTAL)
            setOffscreenItems(3)
            adapter = mHomeAdapter
            setItemTransitionTimeMillis(150)
            addOnItemChangedListener(this@HomeFragment)
        }


        with(listSummary) {
            layoutManager = GridLayoutManager(this.context, resources.getInteger(R.integer.list_column))
            adapter = mSummaryAdapter
        }


        listAnnouncement.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            (layoutManager as LinearLayoutManager).setAutoMeasureEnabled(true)
            setHasFixedSize(true)
        }

        buttonProjectPreview.setOnClickListener {
            mPresenter.invokeProjectList()
        }

        buttonReport.setOnClickListener {
            callPhone(getString(R.string.call_center))
        }

        pageIndicatorView.setAnimationType(AnimationType.COLOR)
        hideHomeAutomation()
        hideHomeUpgrade()

        if (!BuildConfig.BUILD_TYPE.contains("release")) {
            headerLogoWrapper.setOnClickListener {
                activity?.snack("version ${BuildConfig.VERSION_NAME}")
            }
            headerLogoWrapper.setOnLongClickListener {
                AppyRepositoryImpl.isProduction = !AppyRepositoryImpl.isProduction
                AppyRepositoryImpl.clearCache()
                activity?.snack("Switch to Appy ${if (AppyRepositoryImpl.isProduction) "Prod" else "Stg"}")
                refreshHome(true)
                true
            }
        }

        panelAqi.setOnClickListener {
            mPresenter.requestAqi()
        }

        fetchRemoteStyle()
        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        // refresh aqi value
        RuntimeCache.selectedHome?.let { mPresenter.fetchAqi(it) }
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun onCurrentItemChanged(viewHolder: ProjectAdapter.ViewHolder?, adapterPosition: Int) {
        pageIndicatorView?.selection = adapterPosition
        mPresenter.selectHome(adapterPosition)
        sendScreenView("HOME")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ProjectListActivity.CODE -> {
                    val homeId = data?.getStringExtra(ProjectListActivity.HOME)
                    homeId?.let { mPresenter.findHome(homeId) }
                }
                WebActivity.CODE -> {
                    val query = data?.getStringExtra(WebActivity.QUERY)
                    if (query != null) {
                        mPresenter.reopenPreviousMenuWith(query)
                    }
                }
            }
        }
    }

    override fun attachObserver() {
        mPresenter.notifications.observe(this, Observer {
            mPresenter.handleNotification(it)
        })

    }

//    private fun updateSection() {
//        sectionPanel.forEachChild { view ->
//            val list = view.findViewById<RecyclerView>(R.id.list)
//            if (list is RecyclerView && list.adapter is GridMenuDynamicAdapter) {
//                (list.adapter as GridMenuDynamicAdapter).notifyDataChanged()
//            }
//        }
//    }

    override fun sortHomeByLocation(location: Location?) {
        if (location != null) {
            mPresenter.sortHome(location)
        }
    }

    override fun bindHome(home: List<Home>) {
        mHomeAdapter.setData(home)
        pageIndicatorView?.count = mHomeAdapter.itemCount

        titleWelcome.show()
        if (home.size > 3) {
            buttonProjectPreview.show()
        }
        projectPicker.show()
        pageIndicatorView.show()

//        mPresenter.selectHome(0)

        val animation = Fade()
        TransitionManager.beginDelayedTransition(container, animation)

    }

    override fun scrollToHome(position: Int) {
        projectPicker.scrollToPosition(position)
        pageIndicatorView?.selection = position
        mPresenter.selectHome(position)
    }

    override fun refreshHome(forceUpdate: Boolean) {
        if (forceUpdate) {
            mPresenter.fetchHome()
        }
        mHomeAdapter.notifyDataSetChanged()
    }

    override fun showHomeMissing() {
        if (!FeatureConfig.IS_SUPPORT_CALLCENTER) {
            buttonReport.hide()
        }
        layoutHouseMissing.show()
    }

    override fun bindSection(sections: List<Section>, update: Boolean) {
        sectionPanel.show()
        sectionPanel.removeAllViews()
        sections.forEachIndexed { index, section ->
            val view = sectionPanel.inflate(R.layout.view_home_section)
            view.textTitle.text = section.displayName
            mTheme?.titleColor?.let { view.textTitle.setTextColor(Color.parseColor(it)) }

            val flexBoxLayoutManager = FlexboxLayoutManager(context).apply {
                flexWrap = FlexWrap.WRAP
                flexDirection = FlexDirection.ROW
                alignItems = AlignItems.STRETCH
            }

            view.list.apply {
                layoutManager = flexBoxLayoutManager
                val menuAdapter = GridMenuDynamicAdapter(mTheme?.getMenuTitleColor(), mTheme?.getMenuColor(), mTheme?.getAccentColor()) {
                    sendEvent(section.sectionName ?: "", "CLICK", it.id)
                    mPresenter.selectMenu(it)
                }

                val animator = ScaleInAnimator(OvershootInterpolator(1f))
                itemAnimator = animator
                if (BuildConfig.BUILD_TYPE == "debug" && index == 0) {
                    val list = arrayListOf<DynamicMenu>()
                    list.addAll(section.items!!)
                    list.add(DynamicMenu(id = Feature.PARTNER_SANSIRI_MEA, title = "ค่าไฟ", icon = "https://upload.wikimedia.org/wikipedia/th/thumb/3/38/MEA_Logo.png/220px-MEA_Logo.png"))
                    if (update) menuAdapter.setData(list) else menuAdapter.setDataDelay(list)
                } else {
                    if (update) menuAdapter.setData(section.items
                            ?: listOf()) else menuAdapter.setDataDelay(section.items ?: listOf())
                }
                adapter = menuAdapter
            }
            sectionPanel.addView(view)
        }
    }

    override fun bindSummary(summary: List<TodaySummary>) {
        titleSummary.show()
        listSummary.show()
        mSummaryAdapter.setData(summary)
    }

    override fun bindHomeAutomation(homeAutomation: List<Menu>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            titleHomeAutomation.show()
            listHomeAutomation.show()
            buttonUpgradeHome.hide()
            textHomeAutomationUncompatible.hide()

            with(listHomeAutomation) {
                if (homeAutomation.size <= 6) {
                    layoutManager = GridLayoutManager(this.context, resources.getInteger(R.integer.grid_column))
                    adapter = GridMenuAdapter(mTheme?.getAccentColor()
                            ?: resources.getColor(R.color.colorAccent), mTheme?.getMenuColor()) {
                        mPresenter.requestHomeAutomation(it)
                    }.apply {
                        val animator = ScaleInAnimator(OvershootInterpolator(1f))
                        itemAnimator = animator
                        setDataDelay(homeAutomation)
                    }
                } else {
                    layoutManager = GridLayoutManager(this.context, 2, GridLayoutManager.HORIZONTAL, false)
                    adapter = GridMenuHorizontalAdapter(mTheme?.getAccentColor()
                            ?: resources.getColor(R.color.colorAccent), mTheme?.getMenuColor(), resources.getInteger(R.integer.grid_column) + 0.2) {
                        mPresenter.requestHomeAutomation(it)
                    }.apply {
                        val animator = ScaleInAnimator(OvershootInterpolator(1f))
                        itemAnimator = animator
                        setDataDelay(homeAutomation)
                    }

                }
            }
        } else {
            titleHomeAutomation.show()
            textHomeAutomationUncompatible.show()
            listHomeAutomation.hide()
            buttonUpgradeHome.hide()
        }
    }

    override fun bindHomeAutomationScene(homeAutomation: List<Menu>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            titleHomeAutomationScene.show()
            listHomeAutomationScene.show()

            with(listHomeAutomationScene) {
                if (homeAutomation.size <= 6) {
                    layoutManager = GridLayoutManager(this.context, resources.getInteger(R.integer.grid_column))
                    adapter = GridMenuAdapter(mTheme?.getAccentColor()
                            ?: resources.getColor(R.color.colorAccent), mTheme?.getMenuColor()) {
                        mPresenter.requestHomeAutomationScene(it)
                    }.apply {
                        val animator = ScaleInAnimator(OvershootInterpolator(1f))
                        itemAnimator = animator
                        setDataDelay(homeAutomation)
                    }
                } else {
                    layoutManager = GridLayoutManager(this.context, 2, GridLayoutManager.HORIZONTAL, false)
                    adapter = GridMenuHorizontalAdapter(mTheme?.getAccentColor()
                            ?: resources.getColor(R.color.colorAccent), mTheme?.getMenuColor(), resources.getInteger(R.integer.grid_column) + 0.2) {
                        mPresenter.requestHomeAutomationScene(it)
                    }.apply {
                        val animator = ScaleInAnimator(OvershootInterpolator(1f))
                        itemAnimator = animator
                        setDataDelay(homeAutomation)
                    }

                }
            }
        }
    }

    override fun showLoadingHomeUpgrade() {
        titleHomeAutomation.show()
        listHomeAutomation.hide()
        buttonUpgradeHome.show()

        buttonUpgradeHome.setOnClickListener(null)
        buttonUpgradeHome.removeAllViews()

        layoutInflater.inflate(R.layout.view_menu_home_upgrade_loading, buttonUpgradeHome)
    }

    override fun showHomeUpgrade() {
        titleHomeAutomation.show()
        listHomeAutomation.hide()
        buttonUpgradeHome.show()

        buttonUpgradeHome.setOnClickListener {
            mPresenter.requestHomeUpgrade()
        }

        buttonUpgradeHome.removeAllViews()

        layoutInflater.inflate(R.layout.view_menu_home_upgrade_start, buttonUpgradeHome)
    }

    override fun showHomeUpgradeWaiting(history: HomeUpgradeHistory) {
        titleHomeAutomation.show()
        listHomeAutomation.hide()
        buttonUpgradeHome.show()

        buttonUpgradeHome.setOnClickListener {
            launchHomeUpgradeMyOrder(history)
        }

        buttonUpgradeHome.removeAllViews()


        layoutInflater.inflate(R.layout.view_menu_home_upgrade_waiting, buttonUpgradeHome)
    }

    override fun showHomeUpgradeAppointment(history: HomeUpgradeHistory) {
        titleHomeAutomation.show()
        listHomeAutomation.hide()
        buttonUpgradeHome.show()

        buttonUpgradeHome.setOnClickListener {
            launchHomeUpgradeMyOrder(history)
        }

        buttonUpgradeHome.removeAllViews()

        val view = layoutInflater.inflate(R.layout.view_menu_home_upgrade_appointment, buttonUpgradeHome)
        view.findViewById<TextView>(R.id.textTime).text = history.installation?._date?.reportTime()
        view.findViewById<TextView>(R.id.textDate).text = history.installation?._date?.report()

    }

    override fun hideHomeUpgrade() {
        titleHomeAutomation.hide()
        buttonUpgradeHome.hide()
    }

    override fun showBAtHome(username: String, password: String, projectId: String) {
        if (BuildConfig.BUILD_TYPE == "debug") {
            dynamicContainer.show()
            fragmentManager?.beginTransaction()?.replace(R.id.dynamicContainer, BAtHomeWidgetFragment.newInstance(username, password, projectId))?.commit()
        }
    }

    override fun hideBAtHome() {
        dynamicContainer.hide()
    }

    override fun bindAnnouncement(announcement: List<Announcement<ProjectAnnouncementDetail>>) {
        val mAnnouncementAdapter = HomeAnnouncementAdapter(mTheme?.getMenuTitleColor(), mTheme?.getMenuColor()) { announcement ->
            val title = RuntimeCache.selectedHome?.title
            ContentActivity.start(activity!!, null, title ?: "", announcement)
        }

        mAnnouncementAdapter.setData(announcement)
        listAnnouncement.adapter = mAnnouncementAdapter

        titleAnnouncement?.show()
        ({
            if (mPresenter.isAnnouncementShow) {
                val slide = Slide()
                slide.slideEdge = Gravity.RIGHT
                slide.interpolator = OvershootInterpolator(1f)
                listAnnouncement?.let { TransitionManager.beginDelayedTransition(listAnnouncement, slide) }
                listAnnouncement?.show()
            }

        }).withDelay(100)
    }

    override fun hideHome() {
        titleWelcome.hide()
        buttonProjectPreview.hide()
        projectPicker.hide()
        pageIndicatorView.hide()
    }

    override fun hideSummary() {
        titleSummary.hide()
        listSummary.hide()
    }

    override fun hideAnnouncement() {
        listAnnouncement.scrollToPosition(0)
        titleAnnouncement.hide()
        listAnnouncement.hide()
    }

    override fun hideHomeAutomation() {
        titleHomeAutomation.hide()
        listHomeAutomation.hide()
        titleHomeAutomationScene.hide()
        listHomeAutomationScene.hide()
        textHomeAutomationUncompatible.hide()
    }

    override fun hideSection() {
        sectionPanel.hide()
        sectionPanel.removeAllViews()
    }

    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        activity?.snackError(message)
    }

    override fun launchOrviboHomeAutomation(title: String, roomId: String) {
//        OrviboHomeAutomationActivity.start(activity!!, title, roomId)
    }

    override fun launchAppyHomeAutomation(title: String, projectId: String, homeId: String, roomId: String) {
        HomeAutomationActivity.start(activity!!, title, projectId, homeId, roomId)
    }

    override fun launchMailbox(home: Home, title: String?) {
        MailBoxActivity.start(activity!!, home.toHut(), title)
    }

    override fun launchiBOX(home: Home, menu: Menu?) {
        if (menu is DynamicMenu) {
            if (menu.data?.iboxMobileNo != null) {
                IBoxListActivity.start(activity!!, home.toHut(), menu.title, menu.data!!.iboxMobileNo)
            } else {
                IBoxRegisterActivity.start(activity!!, home.toHut(), menu.title)
            }
        } else {
            IBoxListActivity.start(activity!!, home.toHut(), null, null)
        }
    }

    override fun launchHomeCare(unitObjectId: String, title: String?) {
        HomeCareHistoryActivity.start(activity!!, unitObjectId, title)
    }

    override fun launchHomeCareCreate(unitObjectId: String) {
        HomeCareCreateActivity.start(activity!!, unitObjectId)
    }

    override fun launchDownPayment(home: Home, title: String?) {
        DownPaymentActivity.start(activity!!, home.toHut(), title)
    }

    override fun launchFacilityBooking(home: Home, title: String?) {
        FacilityBookingActivity.start(activity!!, home.projectId, home.unitId, home.address, title)
    }

    override fun launchFacilityBookingDetail(title: String, projectId: String, facilityId: String) {
        // FacilityBookingDetailActivity.start(activity!!, title, projectId, facilityId)
    }

    override fun launchChat(home: Home, menu: Menu?) {
        menu?.id?.let { mPresenter.clearNotificationHistory(home.unitId, it) }
        ChatActivity.start(activity!!, home.toHut(), menu?.title)
    }

    override fun launchPhoneDirectory(projectId: String, title: String?) {
        PhoneDirectoryActivity.start(activity!!, projectId, title)
    }

    override fun launchWeb(url: String, title: String) {
        WebActivity.start(this, url, title)
    }

    override fun launchWebWithToken(url: String, title: String) {
        WebActivity.start(this, url, title, true)
    }

    override fun launchMyAccount(home: Home, title: String?) {
        MyAccountActivity.start(activity!!, home.toHut(), title)
    }

    override fun launchInspection(unitObjectId: String, title: String?) {
        InspectionActivity.start(activity!!, unitObjectId, title)
    }

    override fun launchAppointment() {
        launchDialogToUpdate { AppointmentActivity.start(activity!!) }
    }

    override fun launchTransfer(unitObjectId: String, title: String?) {
        TransferActivity.start(activity!!, unitObjectId, title)
    }

    override fun launchContactUs(unitObjectId: String, title: String?) {
        ContactDialogFragment.newInstance(unitObjectId).show(fragmentManager!!, "ContactDialogFragment")
    }

    override fun launchProjectProgress(projectId: String, projectTitle: String, title: String?) {
        ProjectProgressActivity.start(activity!!, projectId, projectTitle, title)
    }

    override fun launchSellAndRentOut() {
        launchDialogToUpdate { SellAndRentActivity.start(activity!!) }
    }

    override fun launchSell() {
        launchDialogToUpdate { SellActivity.start(activity!!) }
    }

    override fun launchAnotherApp(id: String, title: String, packageName: String) {
        sendEvent("Shopping & Service", "Open", id)
        activity?.startApp(packageName)
    }

    override fun launchPlayStore(packageName: String) {
        activity?.startApp(packageName, true)
    }

    override fun launchMenuService(id: String, title: String, section: List<Section>) {
        sendEvent("Shopping & Service", "Open", id)
        MenuServiceSectionActivity.start(activity!!, title, section)
    }

    override fun launchSuggestion(unitObjectId: String, title: String?) {
        SuggestionActivity.start(activity!!, unitObjectId, title)
    }

    override fun launchVoiceCommand(home: Home?) {
        if (home != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (BuildConfig.BUILD_TYPE.contains("release") || BuildConfig.BUILD_TYPE.contentEquals("staging")) {
                    VoiceCommandActivity.start(activity!!, home, false)
                } else {
                    activity?.selector(null, listOf("ข้อมูลจริง", "ข้อมูลจำลอง")) { _, i ->
                        if (i == 0) {
                            VoiceCommandActivity.start(activity!!, home, false)
                        } else {
                            VoiceCommandActivity.start(activity!!, home, true)
                        }
                    }
                }
            } else {
                launchSystemUnCompatible()
            }
        }
    }

    override fun launceProjectPreview(homeList: ArrayList<Hut>, selectedHome: Hut?) {
        sendEvent("UNIT_CARD", "CLICK", "UNIT_SWITCH")
        ProjectListActivity.start(this, homeList, selectedHome)
    }

    override fun launchMyHome(home: Home, title: String?) {
        MyHomeActivity.start(activity!!, home.toHut(), title)
    }

    override fun launchMechanicalParking(projectId: String, title: String?) {
        MechanicalParkingActivity.start(activity!!, projectId, title)
    }

    override fun launchAnnouncement(announcementId: String) {
        ContentActivity.start(activity!!, announcementId)
    }

    override fun launchHomeUpgrade(unitId: String) {
        HomeUpgradeTutorialActivity.start(activity!!, unitId)
    }

    override fun launchHomeUpgradeMyOrder(history: HomeUpgradeHistory) {
        HomeUpgradeMyOrderActivity.start(activity!!, history)
    }

    override fun launchVisitorAccess(home: Home, title: String?) {
        VisitorAccessActivity.start(activity!!, home.toHut(), title)
    }

    override fun launchMaintenanceGuide(home: Home, title: String?, planId: String?, accessoryId: String?) {
        MaintenanceActivity.start(activity!!, home.toHut(), title, planId, accessoryId)
    }

    override fun launchDeepLink(id: String, title: String, uri: String, androidPackageName: String?) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(uri)
            startActivity(intent)
            sendEvent("Shopping & Service", "Open", id)
        } catch (e: ActivityNotFoundException) {
            androidPackageName?.let {
                launchAnotherApp(id, title, it)
            }
        }
    }

    override fun launchAqi(home: Home) {
        AqiActivity.start(activity!!, home.toHut())
    }

    override fun loadingAirData() {
        textTemperature.text = getString(R.string.loading)
        textPm25Value.text = "-"

        panelAqi.hide()
        imageWeather.hide()
        imageAqi.invisible()
//        textPm25.invisible()
//        textAqi.invisible()
        imageTextAqi.invisible()
        textPm25Value.invisible()
    }

    override fun bindAirData(air: Air) {
        val temperature = air.weathers?.find { it.objectId == "TEMPERATURE" }

        if (temperature == null && air.aqi == null) {
            panelAqi.hide()
            return
        } else {
            val slide = Slide()
            slide.slideEdge = Gravity.TOP
            slide.interpolator = FastOutSlowInInterpolator()
            TransitionManager.beginDelayedTransition(panelAqiWrapper, slide)
            panelAqi.show()
        }

        if (temperature != null) {
            if (temperature.iconUrl != null) {
                Glide.with(this).loadCache(temperature.iconUrl, imageWeather)
                imageWeather.show()
            } else {
                imageWeather.hide()
            }
            textTemperature.text = temperature.valueDisplay
        } else {
            imageWeather.hide()
            textTemperature.text = getString(R.string.no_data)
        }

        if (air.aqi != null) {
            imageAqi.show()
//            textPm25.show()
//            textAqi.show()
            imageTextAqi.show()
            textPm25Value.show()

            textPm25Value.text = air.aqi.valueDisplay
            Glide.with(this).loadCache(air.aqi.iconUrl ?: "", imageAqi)

//            textPm25.textColor = Color.parseColor(air.aqi.color)
//            textAqi.textColor = Color.parseColor(air.aqi.color)
            imageTextAqi.tintImage(Color.parseColor(air.aqi.color))
            textPm25Value.textColor = Color.parseColor(air.aqi.color)
        } else {
            imageAqi.invisible()
//            textPm25.invisible()
//            textAqi.invisible()
            imageTextAqi.invisible()
            textPm25Value.invisible()
        }
    }

    override fun errorAirData() {
        panelAqi.hide()
    }

    override fun launchSVVH(home: Home, menu: Menu) {
        SVVHActivity.start(activity!!, home.toHut(), menu)
    }

    override fun launchTrendyWash(home: Home, menu: Menu) {
        TrendyWashMainActivity.start(activity!!, home.toHut(), menu)
    }

    override fun launchSmartParking(home: Home, menu: Menu) {
        SmartParkingActivity.start(activity!!, home.unitId, menu.title)
    }

    override fun launchSmartWaterMeter(home: Home, menu: Menu) {
        SmartWaterMeterActivity.start(activity!!, home.unitId, menu.title)
    }

    override fun launchSmartElectricMeter(home: Home, menu: Menu) {
        SmartElectricMeterActivity.start(activity!!, home.unitId, menu.title)
    }

    override fun launchOrvibo(home: Home) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            OrviboHomeActivity.start(activity!!, home.toHut())
//        } else {
//            launchSystemUnCompatible()
//        }
    }

    override fun subscribeOrviboHome(orviboHomeLiveData: LiveData<OrviboHome>) {
//        orviboHomeLiveData.observe(this, Observer { orviboHome ->
//            if (orviboHome != null) {
//                mPresenter.fetchOrviboRoom(orviboHome)
//            } else {
//                hideHomeAutomation()
//            }
//        })
    }

    override fun launchMea(home: Home, menu: Menu) {
        MEAElectricMainActivity.start(activity!!, home.toHut(), menu)
    }

    override fun launchBos(home: Home, menu: Menu) {
        BosActivity.start(activity!!, home.toHut(), menu)
    }

    override fun launchDialogToUpdate(function: () -> Unit) {
        if (BuildConfig.BUILD_TYPE.contains("release")) {
            activity?.alert(
                    getString(R.string.app_update_detail),
                    getString(R.string.app_update)
            ) {
                yesButton {
                    launchPlayStore(BuildConfig.APPLICATION_ID)
                }
            }?.show()
        } else {
            function.invoke()
        }
    }

    private fun launchSystemUnCompatible() {
        activity?.alert(getString(R.string.system_uncompatible)) {
            noButton { }
        }?.show()
    }

    fun navigationToMenu(feature: String, data: PushNotificationData?) {
        mPresenter.requestMenuNavigation(feature, data)
    }

    override fun showSessionTimeoutDialog() {
        activity?.alert(getString(R.string.error_session_time_out), null) {
            yesButton { signOut() }
        }?.show()
    }

    override fun saveProfileId(uid: String?) {
        if (uid != null) {
            PreferenceHelper.defaultPrefs(activity!!)[PreferenceHelper.USER_ID] = uid
        }
    }

    override fun showSignOut() {
        logoutProgressDialog = activity?.indeterminateProgressDialog(getString(R.string.signing_out), null)
    }

    override fun hideSignOut() {
        logoutProgressDialog?.dismiss()
    }

    override fun signOut() {
        showSignOut()
        FirebaseAuthManager.signOut(activity!!) {
            hideSignOut()
            WelcomeActivity.start(activity!!)
            activity?.finish()
        }
    }

    private fun fetchRemoteStyle() {
        FirebaseRemoteConfigManager().fetch { remote ->
            remote.let { remoteConfig ->
                val url = remoteConfig.getString(FirebaseRemoteConfigManager.HOME_WALLPAPER)
                if (!url.isNullOrEmpty()) {
                    imageWallpaper.show()
                    Glide.with(this).load(url).into(imageWallpaper)
                }

                val themeJson = remoteConfig.getString(FirebaseRemoteConfigManager.THEME)
                // {"backgroundColor":"#AB0000","titleColor":"#ffffff","menuColor":"#FF5500","menuTitleColor":"#ffffff","accentColor":"#FFDD00"}
                mTheme = Gson().fromJson<Theme>(themeJson, Theme::class.java)
                mTheme?.backgroundColor?.let {
                    val color = Color.parseColor(it)
                    layoutRoot.setBackgroundColor(color)
                    buttonVoiceCommand.apply {
                        backgroundTintList = ColorStateList.valueOf(color)
                    }
                }
                mTheme?.titleColor?.let {
                    val color = Color.parseColor(it)
                    buttonProjectPreview.tintImage(color)
                    titleWelcome.setTextColor(color)
                    titleSummary.setTextColor(color)
                    titleHomeAutomation.setTextColor(color)
                    titleAnnouncement.setTextColor(color)
                    pageIndicatorView.selectedColor = color
                }
                mTheme?.accentColor?.let {
                    val color = Color.parseColor(it)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        buttonVoiceCommand?.imageTintList = ColorStateList.valueOf(color)
                    }
                }

                // logo
                remoteConfig.getString(FirebaseRemoteConfigManager.LOGO_IMAGE)?.let { logo ->

                }
            }
        }
    }

    private fun callPhone(tel: String) {
        val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
        sendEvent("PHONE_DIRECTORY", "CLICK", "TELEPHONE")
        startActivity(phoneIntent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        try {
            fragmentManager?.findFragmentById(R.id.headerLogo)?.let {
                fragmentManager?.beginTransaction()?.remove(it)?.commit()
            }
        } catch (e: Exception) {

        }

    }
}
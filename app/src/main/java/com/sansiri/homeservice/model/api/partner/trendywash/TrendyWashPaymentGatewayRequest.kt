package com.sansiri.homeservice.model.api.partner.trendywash

data class TrendyWashPaymentGatewayRequest(
	val amount: Int = 0,
	val omiseCreditCardToken: String? = null
)

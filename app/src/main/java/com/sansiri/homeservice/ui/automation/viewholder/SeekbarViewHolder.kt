package com.sansiri.homeservice.ui.automation.viewholder

import android.graphics.Color
import android.os.Build
import android.view.View
import com.sansiri.homeservice.model.automation.AppyHomeAutomation
import com.sansiri.homeservice.model.automation.HomeAutomation
import kotlinx.android.synthetic.main.view_icon_and_title.view.*
import kotlinx.android.synthetic.main.view_home_automation_seekbar.view.*
import android.widget.SeekBar
import com.bumptech.glide.Glide
//import com.homemate.sdk.model.HMDevice
//import com.sansiri.homeservice.model.automation.OrviboHomeAutomation
import com.sansiri.homeservice.ui.adapter.LifecycleViewHolder

import com.sansiri.homeservice.util.glide.loadCache


/**
 * Created by sansiri on 10/9/17.
 */
class SeekbarViewHolder(val view: View, val overlayColor: Int, val itemClick: (HomeAutomation, Int) -> Unit) : LifecycleViewHolder(view) {
    val rootLayout: View = view.layoutTitle
    var progress = 0

    fun bind(data: HomeAutomation) {
        with(view) {
            imageLogo.setImageResource(data.icon)
            imageLogo.setColorFilter(context.resources.getColor(overlayColor))
            textTitle.text = data.title

            if (data is AppyHomeAutomation) {
                data.apControl?.icon?.let {
                    Glide.with(this).loadCache(it, imageLogo)
                }
                seekBar.max = 10
                progress = data.action
                setStatus(progress)


                seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar?) {
                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar?) {
                        progress = seekBar?.progress!!
                        itemClick(data, progress)
                        setStatus(progress)
                    }
                })

                infoWrapper.setOnClickListener {
                    progress = if (progress == 0) 10 else 0
                    itemClick(data, progress)

                    setStatus(progress)
                }
            }
//            else if (data is OrviboHomeAutomation && data.isDimmer() && data.orviboDevice is HMDevice) {
//                seekBar.max = 255
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    seekBar.min = 5
//                }
//                val isOn = data.orviboDevice.status?.value1 == 0
//                var actualProgress = data.orviboDevice.status?.value2 ?: 0
//                progress = if (isOn) actualProgress else 0
//                setStatus(progress)
//
//                seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
//                    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
//                    }
//
//                    override fun onStartTrackingTouch(seekBar: SeekBar?) {
//                    }
//
//                    override fun onStopTrackingTouch(seekBar: SeekBar?) {
//                        progress = seekBar?.progress!!
//                        actualProgress = seekBar?.progress!!
//                        itemClick(data, progress)
//                        setStatus(progress)
//                    }
//                })
//
//
//                infoWrapper.setOnClickListener {
//                    progress = if (progress == 0) actualProgress else 0
//                    itemClick(data, progress)
//                    setStatus(progress)
//                }
//            }



        }

    }

    fun setStatus(status: Int) {
        with(view) {
            seekBar.progress = status
            imageLogo.setColorFilter(if (status == 0) Color.GRAY else context.resources.getColor(overlayColor))
        }
    }
//
//    private fun createStepValueSeekbar(seekBar: SeekBar, MIN: Int, MAX: Int, STEP: Int, currentValue: Int) {
//        seekBar.max = 100
//        seekBar.progress = calculateProgress(currentValue, MIN, MAX, STEP)
//
//        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
//            override fun onStopTrackingTouch(seekBar: SeekBar) {}
//
//            override fun onStartTrackingTouch(seekBar: SeekBar) {}
//
//            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
//                val value = Math.round((progress * (MAX - MIN) / 100).toFloat()).toDouble()
//                val displayValue = (value.toInt() + MIN) / STEP * STEP
//            }
//        })
//    }
//
//    private fun calculateProgress(value: Int, MIN: Int, MAX: Int, STEP: Int): Int {
//        return 100 * (value - MIN) / (MAX - MIN)
//    }
}
package com.sansiri.homeservice.ui.bos

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.model.menu.DynamicMenu
import com.sansiri.homeservice.model.menu.Menu
import com.sansiri.homeservice.ui.bos.detail.BosDetailFragment
import com.sansiri.homeservice.ui.bos.type.BosTypeFragment
import com.sansiri.homeservice.ui.language.LanguageActivity
import com.sansiri.homeservice.util.addFragment
import com.sansiri.homeservice.util.replaceFragment
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.toolbar.*

class BosActivity: LocalizationActivity() {

    private var mHome: Hut? = null
    private var mMenu: DynamicMenu? = null

    companion object {
        val HOME = "HOME"
        val MENU = "MENU"

        val TYPE_UNIT = 0
        val TYPE_FACILITY = 1

        fun start(activity: Activity, home: Hut, menu: Menu) {
            val intent = Intent(activity, BosActivity::class.java)
            intent.putExtra(HOME, home)
            if (menu is DynamicMenu)
                intent.putExtra(MENU, menu)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        setBackToolbar("")

        mHome = intent.getParcelableExtra<Hut>(HOME)
        mMenu = intent.getParcelableExtra<DynamicMenu>(MENU) ?: null

        setToolbarTitle()

        launchTypeSelect()
    }

    fun setToolbarTitle(title: String? = null) {
        textToolbar.text = title ?: mMenu?.title ?: ""
    }


    fun launchTypeSelect() {
        if (mHome != null) {
            replaceFragment(BosTypeFragment.newInstance(mHome!!.unitId), false)
        }
    }

    fun launchDetail(groupType: Int, jobType: BosMenu) {
        if (mHome != null) {
            addFragment(BosDetailFragment.newInstance(mHome!!.unitId, groupType, jobType), true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}
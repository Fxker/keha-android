package com.appy.android.code.domain.model.command.coolautomation

import com.appy.android.code.data.rest.form.AppyForm
import com.appy.android.sdk.control.coolautomation.CoolAutomationCommand
import com.appy.android.sdk.control.coolautomation.FanSpeedCommand

/**
 * Created by ssa-dev-04 on 1/12/2560.
 */
internal class SendCommandCoolAutomationFanSpeedForm(
        val homeId: String,
        val coolId: String,
        val commandParam: FanSpeedCommand
) : AppyForm() {
    val mSendCommandCoolAutomationFanSpeedJsonBody = SendCommandCoolAutomationFanSpeedJsonBody()

    init {
        this.mSendCommandCoolAutomationFanSpeedJsonBody.command = CoolAutomationCommand.FAN_SPEED.action()
        this.mSendCommandCoolAutomationFanSpeedJsonBody.commandParam = commandParam.action()
    }
}
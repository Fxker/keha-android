package com.sansiri.homeservice.ui.contact

import android.util.Log
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.observe
import com.sansiri.homeservice.util.showHttpError

/**
 * Created by sansiri on 11/30/17.
 */
class ContactDialogPresenter : BasePresenterImpl<ContactDialogContract.View>(), ContactDialogContract.Presenter {
    private lateinit var mUnitId: String

    fun start(unitId: String) {
        this.mUnitId = unitId
        fetchData()
    }

    override fun start() {

    }

    override fun destroy() {
    }

    override fun fetchData() {
        mView?.showLoading()
        ApiRepositoryProvider.provideApiAuthRepository {
            if (it != null) {
                mCompositeDisposable.add(
                        it.getContactCR(mUnitId).observe().subscribe({
                            mView?.hideLoading()
                            mView?.bindData(it.crTransfer)
                        }) {
                            mView?.hideLoading()
                            mView?.showError(it.showHttpError())
                        }
                )
            }
        }
    }

}
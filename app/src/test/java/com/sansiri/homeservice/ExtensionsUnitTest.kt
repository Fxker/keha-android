package com.sansiri.homeservice

import com.sansiri.homeservice.util.toCurrencyFormat
import org.junit.Assert
import org.junit.Test

/**
 * Created by sansiri on 11/14/17.
 */
class ExtensionsUnitTest {
    @Test
    fun toCommaFormat() {
        val expected = "2,000,000,000"
        val actual = 2000000000.toCurrencyFormat()
        Assert.assertEquals(expected, actual)
    }

}
package com.appy.android.sdk

import android.app.Application
import com.appy.android.automation.manager.AutomationManager
import com.appy.android.automation.network.BTCStatusListener
import com.appy.android.automation.network.TCPSocketHandler
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult
import com.appy.android.code.base.domain.callback.BasicApiCallbackWithResult
import com.appy.android.code.base.domain.subscriber.BaseApiSubscriber
import com.appy.android.code.data.rest.error.APErrorFactory
import com.appy.android.code.data.rest.error.APResourceNotFoundException
import com.appy.android.code.dependency.component.AppySDKComponent
import com.appy.android.code.dependency.component.DaggerAppySDKComponent
import com.appy.android.code.dependency.module.AppySDKModule
import com.appy.android.code.domain.model.command.ac.GetAirConditionersInteractor
import com.appy.android.code.domain.model.command.ac.SendCommandAirConditionerForm
import com.appy.android.code.domain.model.command.ac.SendCommandAirConditionerInteractor
import com.appy.android.code.domain.model.command.blind.GetBlindsForm
import com.appy.android.code.domain.model.command.blind.GetBlindsInteractor
import com.appy.android.code.domain.model.command.blind.SendCommandBlindForm
import com.appy.android.code.domain.model.command.blind.SendCommandBlindInteractor
import com.appy.android.code.domain.model.command.coolautomation.*
import com.appy.android.code.domain.model.command.daikin.*
import com.appy.android.code.domain.model.command.dimmer.GetDimmersForm
import com.appy.android.code.domain.model.command.dimmer.GetDimmersInteractor
import com.appy.android.code.domain.model.command.dimmer.SendCommandDimmerForm
import com.appy.android.code.domain.model.command.dimmer.SendCommandDimmerInteractor
import com.appy.android.code.domain.model.command.fibaro.*
import com.appy.android.code.domain.model.command.light.GetLightsForm
import com.appy.android.code.domain.model.command.light.GetLightsInteractor
import com.appy.android.code.domain.model.command.light.SendCommandLightForm
import com.appy.android.code.domain.model.command.light.SendCommandLightInteractor
import com.appy.android.code.domain.model.command.outlet.GetOutletsInteractor
import com.appy.android.code.domain.model.command.outlet.SendCommandOutletForm
import com.appy.android.code.domain.model.command.outlet.SendCommandOutletInteractor
import com.appy.android.code.domain.model.command.remotecontrol.GetRemoteControlsForm
import com.appy.android.code.domain.model.command.remotecontrol.GetRemoteControlsInteractor
import com.appy.android.code.domain.model.command.remotecontrol.SendCommandRemoteControlForm
import com.appy.android.code.domain.model.command.remotecontrol.SendCommandRemoteControlInteractor
import com.appy.android.code.domain.model.command.scenario.GetScenariosForm
import com.appy.android.code.domain.model.command.scenario.GetScenariosInteractor
import com.appy.android.code.domain.model.command.scenario.SendCommandScenarioForm
import com.appy.android.code.domain.model.command.scenario.SendCommandScenarioInteractor
import com.appy.android.code.domain.model.command.toggleswitch.GetToggleSwitchsForm
import com.appy.android.code.domain.model.command.toggleswitch.GetToggleSwitchsInteractor
import com.appy.android.code.domain.model.command.toggleswitch.SendCommandToggleSwitchForm
import com.appy.android.code.domain.model.command.toggleswitch.SendCommandToggleSwitchInteractor
import com.appy.android.code.domain.model.control.ControlForm
import com.appy.android.code.domain.model.control.GetControlsInteractor
import com.appy.android.code.domain.model.home.GetHomeInteractor
import com.appy.android.code.domain.model.home.GetHomesInteractor
import com.appy.android.code.domain.model.home.HomeForm
import com.appy.android.code.domain.model.home.HomesForm
import com.appy.android.code.domain.model.room.GetRoomInteractor
import com.appy.android.code.domain.model.room.GetRoomsInteractor
import com.appy.android.code.domain.model.room.RoomForm
import com.appy.android.code.domain.model.room.RoomsForm
import com.appy.android.code.domain.model.status.blind.GetStatusBlindForm
import com.appy.android.code.domain.model.status.blind.GetStatusBlindInteractor
import com.appy.android.code.domain.model.status.daikin.GetStatusDaikinForm
import com.appy.android.code.domain.model.status.daikin.GetStatusDaikinInteractor
import com.appy.android.code.domain.model.status.dimmer.GetStatusDimmerForm
import com.appy.android.code.domain.model.status.dimmer.GetStatusDimmerInteractor
import com.appy.android.code.domain.model.status.light.GetStatusLightForm
import com.appy.android.code.domain.model.status.light.GetStatusLightInteractor
import com.appy.android.code.domain.model.status.toggleswitch.GetStatusToggleSwitchForm
import com.appy.android.code.domain.model.status.toggleswitch.GetStatusToggleSwitchInteractor
import com.appy.android.code.helpers.CommandHelper
import com.appy.android.code.helpers.HashIDHelper
import com.appy.android.sdk.control.APControl
import com.appy.android.sdk.control.CommandApiResult
import com.appy.android.sdk.control.ControlsApiResult
import com.appy.android.sdk.control.ac.APAirConditioner
import com.appy.android.sdk.control.ac.AirConditionerCommand
import com.appy.android.sdk.control.blind.APBlind
import com.appy.android.sdk.control.blind.BlindCommand
import com.appy.android.sdk.control.daikin.*
import com.appy.android.sdk.control.dimmer.APDimmer
import com.appy.android.sdk.control.dimmer.DimmerCommand
import com.appy.android.sdk.control.light.APLight
import com.appy.android.sdk.control.light.LightCommand
import com.appy.android.sdk.control.outlet.APOutlet
import com.appy.android.sdk.control.outlet.OutletCommand
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.appy.android.sdk.control.remotecontrol.APRemoteControl
import com.appy.android.sdk.control.scenario.APScenario
import com.appy.android.sdk.control.scenario.ScenarioCommand
import com.appy.android.sdk.control.toggleswitch.APToggleSwitch
import com.appy.android.sdk.control.toggleswitch.ToggleSwitchCommand
import com.appy.android.sdk.home.APHome
import com.appy.android.sdk.home.HomeApiResult
import com.appy.android.sdk.home.HomesApiResult
import com.appy.android.sdk.room.APRoom
import com.appy.android.sdk.room.RoomApiResult
import com.appy.android.sdk.room.RoomsApiResult
import com.appy.android.sdk.status.blind.APStatusBlind
import com.appy.android.sdk.status.blind.StatusBlindApiResult
import com.appy.android.sdk.status.daikin.StatusDaikinApiResult
import com.appy.android.sdk.status.dimmer.APStatusDimmer
import com.appy.android.sdk.status.dimmer.StatusDimmerApiResult
import com.appy.android.sdk.status.light.APStatusLight
import com.appy.android.sdk.status.light.StatusLightApiResult
import com.appy.android.sdk.status.toggleswitch.APStatusToggleSwitch
import com.appy.android.sdk.status.toggleswitch.StatusToggleSwitchApiResult
import com.google.gson.JsonObject
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
class AppySDK {

    @Inject
    internal lateinit var mGetHomeInteractor: GetHomeInteractor
    @Inject
    internal lateinit var mGetHomesInteractor: GetHomesInteractor
    @Inject
    internal lateinit var mGetControlsInteractor: GetControlsInteractor
    @Inject
    internal lateinit var mGetRoomInteractor: GetRoomInteractor
    @Inject
    internal lateinit var mGetRoomsInteractor: GetRoomsInteractor
    @Inject
    internal lateinit var mGetAirConditionersInteractor: GetAirConditionersInteractor
    @Inject
    internal lateinit var mGetBlindsInteractor: GetBlindsInteractor
    @Inject
    internal lateinit var mGetCoolAutomationsInteractor: GetCoolAutomationsInteractor
    @Inject
    internal lateinit var mGetDaikinsInteractor: GetDaikinsInteractor
    @Inject
    internal lateinit var mGetDimmersInteractor: GetDimmersInteractor
    @Inject
    internal lateinit var mGetLightsInteractor: GetLightsInteractor
    @Inject
    internal lateinit var mGetOutletsInteractor: GetOutletsInteractor
    @Inject
    internal lateinit var mGetRemoteControlsInteractor: GetRemoteControlsInteractor
    @Inject
    internal lateinit var mGetScenariosInteractor: GetScenariosInteractor
    @Inject
    internal lateinit var mGetToggleSwitchsInteractor: GetToggleSwitchsInteractor
    @Inject
    internal lateinit var mSendCommandAirConditionerInteractor: SendCommandAirConditionerInteractor
    @Inject
    internal lateinit var mSendCommandBlindInteractor: SendCommandBlindInteractor
    @Inject
    internal lateinit var mSendCommandCoolAutomationInteractor: SendCommandCoolAutomationInteractor
    @Inject
    internal lateinit var mSendCommandCoolAutomationParamInteractor: SendCommandCoolAutomationParamInteractor
    @Inject
    internal lateinit var mSendCommandDaikinFanDirectionInteractor: SendCommandDaikinFanDirectionInteractor
    @Inject
    internal lateinit var mSendCommandDaikinFanSpeedInteractor: SendCommandDaikinFanSpeedInteractor
    @Inject
    internal lateinit var mSendCommandDaikinOnOffInteractor: SendCommandDaikinOnOffInteractor
    @Inject
    internal lateinit var mSendCommandDaikinOperationModeInteractor: SendCommandDaikinOperationModeInteractor
    @Inject
    internal lateinit var mSendCommandDaikinTemperatureInteractor: SendCommandDaikinTemperatureInteractor
    @Inject
    internal lateinit var mSendCommandDimmerInteractor: SendCommandDimmerInteractor
    @Inject
    internal lateinit var mSendCommandLightInteractor: SendCommandLightInteractor
    @Inject
    internal lateinit var mSendCommandOutletInteractor: SendCommandOutletInteractor
    @Inject
    internal lateinit var mSendCommandRemoteControlsInteractor: SendCommandRemoteControlInteractor
    @Inject
    internal lateinit var mSendCommandScenarioInteractor: SendCommandScenarioInteractor
    @Inject
    internal lateinit var mSendCommandToggleSwitchsInteractor: SendCommandToggleSwitchInteractor
    @Inject
    internal lateinit var mGetStatusBlindInteractor: GetStatusBlindInteractor
    @Inject
    internal lateinit var mGetStatusDaikinInteractor: GetStatusDaikinInteractor
    @Inject
    internal lateinit var mGetStatusDimmerInteractor: GetStatusDimmerInteractor
    @Inject
    internal lateinit var mGetStatusLightInteractor: GetStatusLightInteractor
    @Inject
    internal lateinit var mGetStatusToggleSwitchInteractor: GetStatusToggleSwitchInteractor
    @Inject
    internal lateinit var mSendCommandCoolAutomationTemperatureInteractor: SendCommandCoolAutomationTemperatureInteractor
    @Inject
    internal lateinit var mSendCommandCoolAutomationFanSpeedInteractor: SendCommandCoolAutomationFanSpeedInteractor
    @Inject
    internal lateinit var mSendFibaroCommandInteractor: SendFibaroCommandInteractor
    @Inject
    internal lateinit var mSendFibaroScenarioCommandInteractor: SendFibaroScenarioCommandInteractor
    @Inject
    internal lateinit var mGetFibaroDeviceStatusInteractor: GetFibaroDeviceStatusInteractor

    var apiKey: String? = null

    @JvmField
    var appKey: String? = null
    @JvmField
    var isProduction = false

    var pin: String? = null
    var unit: String? = null
    var localMode = false

    companion object {
        var appySDK: AppySDK? = null
        private var appySDKComponent: AppySDKComponent? = null

        internal fun instance(): AppySDK {
            if (appySDK == null) {
                appySDK = AppySDK()

                appySDKComponent?.inject(appySDK!!)
            }

            return appySDK!!
        }

        @JvmStatic
        fun init(application: Application) {
            appySDKComponent = DaggerAppySDKComponent.builder()
                .appySDKModule(AppySDKModule(application))
                .build()
        }

        @JvmStatic
        fun initialize(apiKey: String) {
            instance().apiKey = apiKey
        }

        @JvmStatic
        fun setProduction(isProduction: Boolean) {
            instance().isProduction = isProduction
        }

        @JvmStatic
        fun setAppKey(appKey: String) {
            instance().appKey = appKey
        }
    }

    object Automation {

        @JvmStatic
        fun setPin(pin: String) {
            instance().pin = pin
        }

        @JvmStatic
        fun setUnit(unit: String) {
            instance().unit = unit
        }

        @JvmStatic
        fun setLocalMode(localMode: Boolean) {
            instance().localMode = localMode
        }

        @JvmStatic
        fun getHome(apHome: APHome, homesCallback: HomeCallback) {
            instance().mGetHomeInteractor.execute(BaseApiSubscriber(homesCallback), HomeForm(apHome.id!!))
        }

        @JvmStatic
        fun getHomes(homesCallback: HomesCallback) {
            instance().mGetHomesInteractor.execute(BaseApiSubscriber(homesCallback), HomesForm())
        }

        @JvmStatic
        fun getControls(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetControlsInteractor.execute(BaseApiSubscriber(controlsCallback), ControlForm(apHome.id!!))
        }

        @JvmStatic
        fun getRoom(apHome: APHome, apRoom: APRoom, roomCallback: RoomCallback) {
            instance().mGetRoomInteractor.execute(BaseApiSubscriber(roomCallback), RoomForm(apHome.id!!, apRoom.id!!))
        }

        @JvmStatic
        fun getRooms(apHome: APHome, roomsCallback: RoomsCallback) {
            instance().mGetRoomsInteractor.execute(BaseApiSubscriber(roomsCallback), RoomsForm(apHome.id!!))
        }

        // Merge to ToggleSwitch
        // @JvmStatic
        // fun getAirConditioners(apHome: APHome, controlsCallback: ControlsCallback) {
        //     instance().mGetAirConditionersInteractor.execute(BaseApiSubscriber(controlsCallback), GetAirConditionersForm(apHome.id!!))
        // }

        @JvmStatic
        fun getBlinds(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetBlindsInteractor.execute(BaseApiSubscriber(controlsCallback), GetBlindsForm(apHome.id!!))
        }

        // @JvmStatic
        // fun getCoolAutomations(apHome: APHome, controlsCallback: ControlsCallback) {
        //     instance().mGetCoolAutomationsInteractor.execute(BaseApiSubscriber(controlsCallback), GetCoolAutomationsForm(apHome.id!!))
        // }

        @JvmStatic
        fun getDaikins(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetDaikinsInteractor.execute(BaseApiSubscriber(controlsCallback), GetDaikinsForm(apHome.id!!))
        }

        @JvmStatic
        fun getDimmers(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetDimmersInteractor.execute(BaseApiSubscriber(controlsCallback), GetDimmersForm(apHome.id!!))
        }

        @JvmStatic
        fun getLights(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetLightsInteractor.execute(BaseApiSubscriber(controlsCallback), GetLightsForm(apHome.id!!))
        }

        // Merge to ToggleSwitch
        // @JvmStatic
        // fun getOutlets(apHome: APHome, controlsCallback: ControlsCallback) {
        //     instance().mGetOutletsInteractor.execute(BaseApiSubscriber(controlsCallback), GetOutletsForm(apHome.id!!))
        // }

        @JvmStatic
        fun getRemoteControls(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetRemoteControlsInteractor.execute(BaseApiSubscriber(controlsCallback), GetRemoteControlsForm(apHome.id!!))
        }

        @JvmStatic
        fun getScenarios(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetScenariosInteractor.execute(BaseApiSubscriber(controlsCallback), GetScenariosForm(apHome.id!!))
        }

        @JvmStatic
        fun getToggleSwitches(apHome: APHome, controlsCallback: ControlsCallback) {
            instance().mGetToggleSwitchsInteractor.execute(BaseApiSubscriber(controlsCallback), GetToggleSwitchsForm(apHome.id!!))
        }

        // Merge to ToggleSwitch
        @JvmStatic
        fun sendCommand(apHome: APHome, apAirConditioner: APAirConditioner, command: AirConditionerCommand, commandCallback: CommandCallback) {
            when (apAirConditioner.isOverkizModel()) {
                true -> {
                    instance().mSendCommandAirConditionerInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandAirConditionerForm(apHome.id!!, apAirConditioner.id!!, command))
                }
                false -> {
                    when (instance().localMode) {
                        true -> {
                            try {
                                val address = instance().getAddress(apAirConditioner)
                                val command = CommandHelper.getCommandMessage(apAirConditioner, command)
                                AutomationManager.instance?.sendBTCCommand(
                                    address,
                                    command,
                                    object : AutomationManager.ReceiveBTCCommandResponseListener {
                                        override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                        }
                                    },
                                    object : AutomationManager.SendBTCCommandListener {
                                        override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                            if (success) {
                                                commandCallback.onSuccess(CommandApiResult())
                                            } else {
                                                commandCallback.onFailed(RuntimeException())
                                            }
                                        }

                                        override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                            commandCallback.onFailed(APErrorFactory.create(errorCode))
                                        }
                                    }
                                )
                            } catch (e: Exception) {
                                commandCallback.onFailed(e)
                            }
                        }
                        false -> {
                            instance().mSendCommandAirConditionerInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandAirConditionerForm(apHome.id!!, apAirConditioner.id!!, command))
                        }
                    }
                }
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apBlind: APBlind, command: BlindCommand, commandCallback: CommandCallback) {
            when (apBlind.isOverkizModel()) {
                true -> {
                    instance().mSendCommandBlindInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandBlindForm(apHome.id!!, apBlind.id!!, command))
                }
                false -> {
                    when (instance().localMode) {
                        true -> {
                            try {
                                val address = instance().getAddress(apBlind)
                                val command = CommandHelper.getCommandMessage(apBlind, command)
                                AutomationManager.instance?.sendBTCCommand(
                                    address,
                                    command,
                                    object : AutomationManager.ReceiveBTCCommandResponseListener {
                                        override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                        }
                                    },
                                    object : AutomationManager.SendBTCCommandListener {
                                        override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                            if (success) {
                                                commandCallback.onSuccess(CommandApiResult())
                                            } else {
                                                commandCallback.onFailed(RuntimeException())
                                            }
                                        }

                                        override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                            commandCallback.onFailed(APErrorFactory.create(errorCode))
                                        }
                                    }
                                )
                            } catch (e: Exception) {
                                commandCallback.onFailed(e)
                            }
                        }
                        false -> {
                            instance().mSendCommandBlindInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandBlindForm(apHome.id!!, apBlind.id!!, command))
                        }
                    }
                }
            }
        }

        // @JvmStatic
        // fun sendCommand(apHome: APHome, apCoolAutomation: APCoolAutomation, command: CoolAutomationCommand, commandCallback: CommandCallback) {
        // instance().mSendCommandCoolAutomationInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandCoolAutomationForm(apHome.id!!, apCoolAutomation.id!!, instance().pin!!, instance().apiKey!!, instance().appKey!!, command))
        // }

        // @JvmStatic
        // fun sendCommand(apHome: APHome, apCoolAutomation: APCoolAutomation, command: CoolAutomationCommand, paramCommand: CoolAutomationParamCommand, commandCallback: CommandCallback) {
        // instance().mSendCommandCoolAutomationParamInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandCoolAutomationParamForm(apHome.id!!, apCoolAutomation.id!!, instance().pin!!, instance().apiKey!!, instance().appKey!!, command, paramCommand))
        // }

        @JvmStatic
        fun sendCommand(apHome: APHome, apDaikin: APDaikin, parameter: APDaikinParameter, commandCallback: CommandCallback) {
            when(parameter) {
                is DaikinFanDirectionCommand -> {
                    instance().mSendCommandDaikinFanDirectionInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDaikinFanDirectionForm(apHome.id!!, apDaikin.id!!, parameter))
                }
                is DaikinFanSpeedCommand -> {
                    instance().mSendCommandDaikinFanSpeedInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDaikinFanSpeedForm(apHome.id!!, apDaikin.id!!, parameter))
                }
                is DaikinOnOffCommand -> {
                    instance().mSendCommandDaikinOnOffInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDaikinOnOffForm(apHome.id!!, apDaikin.id!!, parameter))
                }
                is DaikinOperationModeCommand -> {
                    instance().mSendCommandDaikinOperationModeInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDaikinOperationModeForm(apHome.id!!, apDaikin.id!!, parameter))
                }
                is DaikinTemperatureCommand -> {
                    instance().mSendCommandDaikinTemperatureInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDaikinTemperatureForm(apHome.id!!, apDaikin.id!!, parameter))
                }
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apDimmer: APDimmer, command: DimmerCommand, commandCallback: CommandCallback) {
            if (apDimmer.isOverkizModel()) {
                instance().mSendCommandDimmerInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDimmerForm(apHome.id!!, apDimmer.id!!, command))
            } else if (apDimmer.isFibaroModel()) {
                instance().mSendFibaroCommandInteractor.execute(BaseApiSubscriber(commandCallback), SendFibaroDimmerCommandForm(apDimmer, command))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apDimmer)
                            val command = CommandHelper.getCommandMessage(apDimmer, command)
                            AutomationManager.instance?.sendBTCCommand(
                                address,
                                command,
                                object : AutomationManager.ReceiveBTCCommandResponseListener {
                                    override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                    }
                                },
                                object : AutomationManager.SendBTCCommandListener {
                                    override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                        if (success) {
                                            commandCallback.onSuccess(CommandApiResult())
                                        } else {
                                            commandCallback.onFailed(RuntimeException())
                                        }
                                    }

                                    override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                        commandCallback.onFailed(APErrorFactory.create(errorCode))
                                    }
                                }
                            )
                        } catch (e: Exception) {
                            commandCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mSendCommandDimmerInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandDimmerForm(apHome.id!!, apDimmer.id!!, command))
                    }
                }
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apLight: APLight, command: LightCommand, commandCallback: CommandCallback) {
            if (apLight.isOverkizModel()) {
                instance().mSendCommandLightInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandLightForm(apHome.id!!, apLight.id!!, command))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apLight)
                            val command = CommandHelper.getCommandMessage(apLight, command)
                            AutomationManager.instance?.sendBTCCommand(
                                address,
                                command,
                                object : AutomationManager.ReceiveBTCCommandResponseListener {
                                    override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                    }
                                },
                                object : AutomationManager.SendBTCCommandListener {
                                    override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                        if (success) {
                                            commandCallback.onSuccess(CommandApiResult())
                                        } else {
                                            commandCallback.onFailed(RuntimeException())
                                        }
                                    }

                                    override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                        commandCallback.onFailed(APErrorFactory.create(errorCode))
                                    }
                                }
                            )
                        } catch (e: Exception) {
                            commandCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mSendCommandLightInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandLightForm(apHome.id!!, apLight.id!!, command))
                    }
                }
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apOutlet: APOutlet, command: OutletCommand, commandCallback: CommandCallback) {
            when (apOutlet.isOverkizModel()) {
                true -> {
                    instance().mSendCommandOutletInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandOutletForm(apHome.id!!, apOutlet.id!!, command))
                }
                false -> {
                    when (instance().localMode) {
                        true -> {
                            try {
                                val address = instance().getAddress(apOutlet)
                                val command = CommandHelper.getCommandMessage(apOutlet, command)
                                AutomationManager.instance?.sendBTCCommand(
                                    address,
                                    command,
                                    object : AutomationManager.ReceiveBTCCommandResponseListener {
                                        override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                        }
                                    },
                                    object : AutomationManager.SendBTCCommandListener {
                                        override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                            if (success) {
                                                commandCallback.onSuccess(CommandApiResult())
                                            } else {
                                                commandCallback.onFailed(RuntimeException())
                                            }
                                        }

                                        override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                            commandCallback.onFailed(APErrorFactory.create(errorCode))
                                        }
                                    }
                                )
                            } catch (e: Exception) {
                                commandCallback.onFailed(e)
                            }
                        }
                        false -> {
                            instance().mSendCommandOutletInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandOutletForm(apHome.id!!, apOutlet.id!!, command))
                        }
                    }
                }
            }
        }

        @JvmStatic
        @Deprecated("This method will be deprecate soon, please switch to sendCommand(APHome, APRemoteControl, APActionRemoteControl, CommandCallback)")
        fun sendCommand(apHome: APHome, apRemoteControl: APRemoteControl, command: String, commandCallback: CommandCallback) {
            instance().mSendCommandRemoteControlsInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandRemoteControlForm(apHome.id!!, apRemoteControl.id!!, command))
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apRemoteControl: APRemoteControl, action: APActionRemoteControl, commandCallback: CommandCallback) {
            sendCommand(apHome, apRemoteControl, action, null, commandCallback)
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apRemoteControl: APRemoteControl, action: APActionRemoteControl, parameter: String?, commandCallback: CommandCallback) {
            if (apRemoteControl.isOverkizModel()) {
                instance().mSendCommandRemoteControlsInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandRemoteControlForm(apHome.id!!, apRemoteControl.id!!, action.action!!))
            } else if (apRemoteControl.isFibaroModel()) {
                instance().mSendFibaroCommandInteractor.execute(BaseApiSubscriber(commandCallback), SendFibaroRemoteControlCommandForm(apRemoteControl, action, parameter))
            } else {
                commandCallback.onFailed(APResourceNotFoundException())
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apScenario: APScenario, command: ScenarioCommand, commandCallback: CommandCallback) {
            if (apScenario.isOverkizModel()) {
                instance().mSendCommandScenarioInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandScenarioForm(apHome.id!!, apScenario.id!!, command))
            } else if (apScenario.isFibaroModel()) {
                instance().mSendFibaroScenarioCommandInteractor.execute(BaseApiSubscriber(commandCallback), SendFibaroScenarioCommandForm(apScenario, command))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apScenario)
                            val command = CommandHelper.getCommandMessage(apScenario)
                            AutomationManager.instance?.sendBTCCommand(
                                address,
                                command,
                                object : AutomationManager.ReceiveBTCCommandResponseListener {
                                    override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                    }
                                },
                                object : AutomationManager.SendBTCCommandListener {
                                    override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                        if (success) {
                                            commandCallback.onSuccess(CommandApiResult())
                                        } else {
                                            commandCallback.onFailed(RuntimeException())
                                        }
                                    }

                                    override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                        commandCallback.onFailed(APErrorFactory.create(errorCode))
                                    }
                                }
                            )
                        } catch (e: Exception) {
                            commandCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mSendCommandScenarioInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandScenarioForm(apHome.id!!, apScenario.id!!, command))
                    }
                }
            }
        }

        @JvmStatic
        fun sendCommand(apHome: APHome, apToggleSwitch: APToggleSwitch, command: ToggleSwitchCommand, commandCallback: CommandCallback) {
            if (apToggleSwitch.isOverkizModel()) {
                instance().mSendCommandToggleSwitchsInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandToggleSwitchForm(apHome.id!!, apToggleSwitch.id!!, command))
            } else if (apToggleSwitch.isFibaroModel()) {
                instance().mSendFibaroCommandInteractor.execute(BaseApiSubscriber(commandCallback), SendFibaroSwitchCommandForm(apToggleSwitch, command))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apToggleSwitch)
                            val command = CommandHelper.getCommandMessage(apToggleSwitch)
                            AutomationManager.instance?.sendBTCCommand(
                                address,
                                command,
                                object : AutomationManager.ReceiveBTCCommandResponseListener {
                                    override fun onReceived(TCPSocketHandler: TCPSocketHandler, command: String, response: String) {

                                    }
                                },
                                object : AutomationManager.SendBTCCommandListener {
                                    override fun onSubmitted(TCPSocketHandler: TCPSocketHandler, success: Boolean) {
                                        if (success) {
                                            commandCallback.onSuccess(CommandApiResult())
                                        } else {
                                            commandCallback.onFailed(RuntimeException())
                                        }
                                    }

                                    override fun onFailed(TCPSocketHandler: TCPSocketHandler, errorCode: Int) {
                                        commandCallback.onFailed(APErrorFactory.create(errorCode))
                                    }
                                }
                            )
                        } catch (e: Exception) {
                            commandCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mSendCommandToggleSwitchsInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandToggleSwitchForm(apHome.id!!, apToggleSwitch.id!!, command))
                    }
                }
            }
        }

        @JvmStatic
        fun getStatusBlind(apHome: APHome, apBlind: APBlind, statusBlindCallback: StatusBlindCallback) {
            when (apBlind.isOverkizModel()) {
                true -> {
                    instance().mGetStatusBlindInteractor.execute(BaseApiSubscriber(statusBlindCallback), GetStatusBlindForm(apHome.id!!, apBlind.id!!))
                }
                false -> {
                    when (instance().localMode) {
                        true -> {
                            try {
                                val address = instance().getAddress(apBlind)
                                val command = CommandHelper.getStatusMessage(apBlind)
                                AutomationManager.instance?.let {
                                    it.getBTCStatus(address, command, object : BTCStatusListener {
                                        override fun onReceiveMessage(status: String, fetchMessage: String, value: String) {
                                            if (command == fetchMessage) {
                                                val currentCommand = BlindCommand.getLastCommand(value)
                                                val availableCommand = BlindCommand.getAvailableCommand(value)

                                                val apStatusBlind = APStatusBlind(apBlind, value)
                                                apStatusBlind.lastAction = currentCommand
                                                apStatusBlind.availableAction = availableCommand

                                                val statusBlindApiResult = StatusBlindApiResult()
                                                val apStatusBlindList = ArrayList<APStatusBlind>()
                                                apStatusBlindList.add(apStatusBlind)

                                                val appyApiStatusResult = AppyApiStatusResult()
                                                appyApiStatusResult.code = 200
                                                appyApiStatusResult.description = "success"

                                                statusBlindApiResult.status = appyApiStatusResult
                                                statusBlindApiResult.data = apStatusBlindList

                                                statusBlindCallback.onSuccess(statusBlindApiResult)
                                            }
                                        }
                                    })
                                }
                            } catch (e: Exception) {
                                statusBlindCallback.onFailed(e)
                            }
                        }
                        false -> {
                            instance().mGetStatusBlindInteractor.execute(BaseApiSubscriber(statusBlindCallback), GetStatusBlindForm(apHome.id!!, apBlind.id!!))
                        }
                    }
                }
            }
        }

        @JvmStatic
        fun getStatusDaikin(apHome: APHome, apDaikin: APDaikin, statusDaikinCallback: StatusDaikinCallback) {
            instance().mGetStatusDaikinInteractor.execute(BaseApiSubscriber(statusDaikinCallback), GetStatusDaikinForm(apHome.id!!, apDaikin.id!!))
        }

        @JvmStatic
        fun getStatusDimmer(apHome: APHome, apDimmer: APDimmer, statusDimmerCallback: StatusDimmerCallback) {
            if (apDimmer.isOverkizModel()) {
                instance().mGetStatusDimmerInteractor.execute(BaseApiSubscriber(statusDimmerCallback), GetStatusDimmerForm(apHome.id!!, apDimmer.id!!))
            } else if (apDimmer.isFibaroModel()) {
                instance().mGetFibaroDeviceStatusInteractor.execute(BaseApiSubscriber(statusDimmerCallback), GetFibaroDeviceStatusForm(apDimmer))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apDimmer)
                            val command = CommandHelper.getStatusMessage(apDimmer)
                            AutomationManager.instance?.let {
                                it.getBTCStatus(address, command, object : BTCStatusListener {
                                    override fun onReceiveMessage(status: String, fetchMessage: String, value: String) {
                                        if (command == fetchMessage) {
                                            val currentCommand = DimmerCommand.getLastCommand(value)
                                            val availableCommand = DimmerCommand.getAvailableCommand(value)

                                            val apStatusDimmer = APStatusDimmer(apDimmer, value)
                                            apStatusDimmer.lastAction = currentCommand
                                            apStatusDimmer.availableAction = availableCommand

                                            val statusDimmerApiResult = StatusDimmerApiResult()
                                            // val apStatusDimmerList = ArrayList<APStatusDimmer>()
                                            // apStatusDimmerList.add(apStatusDimmer)

                                            val appyApiStatusResult = AppyApiStatusResult()
                                            appyApiStatusResult.code = 200
                                            appyApiStatusResult.description = "success"

                                            statusDimmerApiResult.status = appyApiStatusResult
                                            statusDimmerApiResult.data = apStatusDimmer

                                            statusDimmerCallback.onSuccess(statusDimmerApiResult)
                                        }
                                    }
                                })
                            }
                        } catch (e: Exception) {
                            statusDimmerCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mGetStatusDimmerInteractor.execute(BaseApiSubscriber(statusDimmerCallback), GetStatusDimmerForm(apHome.id!!, apDimmer.id!!))
                    }
                }
            }
        }

        @JvmStatic
        fun getStatusLight(apHome: APHome, apLight: APLight, statusLightCallback: StatusLightCallback) {
            when (apLight.isOverkizModel()) {
                true -> {
                    instance().mGetStatusLightInteractor.execute(BaseApiSubscriber(statusLightCallback), GetStatusLightForm(apHome.id!!, apLight.id!!))
                }
                false -> {
                    when (apLight.isOverkizModel()) {
                        true -> {
                            instance().mGetStatusLightInteractor.execute(BaseApiSubscriber(statusLightCallback), GetStatusLightForm(apHome.id!!, apLight.id!!))
                        }
                        false -> {
                            when (instance().localMode) {
                                true -> {
                                    try {
                                        val address = instance().getAddress(apLight)
                                        val command = CommandHelper.getStatusMessage(apLight)
                                        AutomationManager.instance?.let {
                                            it.getBTCStatus(address, command, object : BTCStatusListener {
                                                override fun onReceiveMessage(status: String, fetchMessage: String, value: String) {
                                                    if (command == fetchMessage) {
                                                        val currentCommand = LightCommand.getLastCommand(value)
                                                        val availableCommand = LightCommand.getAvailableCommand(value)

                                                        val apStatusLight = APStatusLight(apLight, value)
                                                        apStatusLight.lastAction = currentCommand
                                                        apStatusLight.availableAction = availableCommand

                                                        val statusLightApiResult = StatusLightApiResult()
                                                        val apStatusLightList = ArrayList<APStatusLight>()
                                                        apStatusLightList.add(apStatusLight)

                                                        val appyApiStatusResult = AppyApiStatusResult()
                                                        appyApiStatusResult.code = 200
                                                        appyApiStatusResult.description = "success"

                                                        statusLightApiResult.status = appyApiStatusResult
                                                        statusLightApiResult.data = apStatusLightList

                                                        statusLightCallback.onSuccess(statusLightApiResult)
                                                    }
                                                }
                                            })
                                        }
                                    } catch (e: Exception) {
                                        statusLightCallback.onFailed(e)
                                    }
                                }
                                false -> {
                                    instance().mGetStatusLightInteractor.execute(BaseApiSubscriber(statusLightCallback), GetStatusLightForm(apHome.id!!, apLight.id!!))
                                }
                            }
                        }
                    }
                }
            }
        }

        @JvmStatic
        fun getStatusToggleSwitch(apHome: APHome, apToggleSwitch: APToggleSwitch, statusToggleSwitchCallback: StatusToggleSwitchCallback) {
            if (apToggleSwitch.isOverkizModel()) {
                instance().mGetStatusToggleSwitchInteractor.execute(BaseApiSubscriber(statusToggleSwitchCallback), GetStatusToggleSwitchForm(apHome.id!!, apToggleSwitch.id!!))
            } else if (apToggleSwitch.isFibaroModel()) {
                instance().mGetFibaroDeviceStatusInteractor.execute(BaseApiSubscriber(statusToggleSwitchCallback), GetFibaroDeviceStatusForm(apToggleSwitch))
            } else {
                when (instance().localMode) {
                    true -> {
                        try {
                            val address = instance().getAddress(apToggleSwitch)
                            val command = CommandHelper.getStatusMessage(apToggleSwitch)
                            AutomationManager.instance?.let {
                                it.getBTCStatus(address, command, object : BTCStatusListener {
                                    override fun onReceiveMessage(status: String, fetchMessage: String, value: String) {
                                        if (command == fetchMessage) {
                                            val currentCommand = ToggleSwitchCommand.getLastCommand(value)
                                            val availableCommand = ToggleSwitchCommand.getAvailableCommand(value)

                                            val apStatusToggleSwitch = APStatusToggleSwitch(apToggleSwitch, value)
                                            apStatusToggleSwitch.lastAction = currentCommand
                                            apStatusToggleSwitch.availableAction = availableCommand

                                            val statusToggleSwitchApiResult = StatusToggleSwitchApiResult()
                                            // val apStatusToggleSwitchList = ArrayList<APStatusToggleSwitch>()
                                            // apStatusToggleSwitchList.add(apStatusToggleSwitch)

                                            val appyApiStatusResult = AppyApiStatusResult()
                                            appyApiStatusResult.code = 200
                                            appyApiStatusResult.description = "success"

                                            statusToggleSwitchApiResult.status = appyApiStatusResult
                                            statusToggleSwitchApiResult.data = apStatusToggleSwitch

                                            statusToggleSwitchCallback.onSuccess(statusToggleSwitchApiResult)
                                        }
                                    }
                                })
                            }
                        } catch (e: Exception) {
                            statusToggleSwitchCallback.onFailed(e)
                        }
                    }
                    false -> {
                        instance().mGetStatusToggleSwitchInteractor.execute(BaseApiSubscriber(statusToggleSwitchCallback), GetStatusToggleSwitchForm(apHome.id!!, apToggleSwitch.id!!))
                    }
                }
            }
        }

        // @JvmStatic
        // fun sendCommandTemperature(apHome: APHome, apCoolAutomation: APCoolAutomation, temperatureCommand: DaikinTemperatureCommand, commandCallback: CommandCallback) {
        // instance().mSendCommandCoolAutomationTemperatureInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandCoolAutomationTemperatureForm(apHome.id!!, apCoolAutomation.id!!, instance().pin!!, instance().apiKey!!, instance().appKey!!, temperatureCommand))
        // }

        // @JvmStatic
        // fun sendCommandFanSpeed(apHome: APHome, apCoolAutomation: APCoolAutomation, fanSpeedCommand: DaikinFanSpeedCommand, commandCallback: CommandCallback) {
        // instance().mSendCommandCoolAutomationFanSpeedInteractor.execute(BaseApiSubscriber(commandCallback), SendCommandCoolAutomationFanSpeedForm(apHome.id!!, apCoolAutomation.id!!, instance().pin!!, instance().apiKey!!, instance().appKey!!, fanSpeedCommand))
        // }

    }

    interface GenericCallback : BasicApiCallbackWithResult<JsonObject>
    interface HomeCallback : BasicApiCallbackWithResult<HomeApiResult>
    interface HomesCallback : BasicApiCallbackWithResult<HomesApiResult>
    interface ControlsCallback : BasicApiCallbackWithResult<ControlsApiResult>
    interface RoomCallback : BasicApiCallbackWithResult<RoomApiResult>
    interface RoomsCallback : BasicApiCallbackWithResult<RoomsApiResult>
    interface CommandCallback : BasicApiCallbackWithResult<CommandApiResult>
    interface StatusBlindCallback : BasicApiCallbackWithResult<StatusBlindApiResult>
    interface StatusDaikinCallback : BasicApiCallbackWithResult<StatusDaikinApiResult>
    interface StatusDimmerCallback : BasicApiCallbackWithResult<StatusDimmerApiResult>
    interface StatusLightCallback : BasicApiCallbackWithResult<StatusLightApiResult>
    interface StatusToggleSwitchCallback : BasicApiCallbackWithResult<StatusToggleSwitchApiResult>

    @Throws(Exception::class)
    private fun getAddress(apControl: APControl): String {
        if (apControl.hostAddress == null) throw Exception("Host address is not found")
        if (apControl.circuitAddress == null) throw Exception("Circuit address is not found")
        val ipLongArray = HashIDHelper.decode(apControl.hostAddress!!)
        return ipLongArray[0].toString() + "." + ipLongArray[1].toString() + "." + ipLongArray[2].toString() + "." + ipLongArray[3].toString()
    }
}
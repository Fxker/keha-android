package com.sansiri.homeservice.ui.myaccount_v2.invoice.payment

import android.content.Context
import android.graphics.*
import androidx.core.content.res.ResourcesCompat
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.myaccount.invoce.Invoice
import com.sansiri.homeservice.ui.epayment.CodeEngine
import com.sansiri.homeservice.util.reportDMYFull
import com.sansiri.homeservice.util.reportDMYNumber
import com.sansiri.homeservice.util.toCurrencyFormat

fun createDetailBitmap(context: Context, home: Hut?, invoice: Invoice?): Bitmap {

    val finalBitmap = Bitmap.createBitmap(
            CodeEngine.imageWidth,
            920,
            Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(finalBitmap)
    val width = canvas.width - CodeEngine.padding * 2

    canvas.drawRect(0f, 0f, finalBitmap.width.toFloat(), finalBitmap.height.toFloat(), Paint().apply {
        style = Paint.Style.FILL
        color = Color.WHITE
    })

    val textBodyPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 20f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textBodyBoldPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 20f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textTitlePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 24f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textTitleBoldPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 24f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textHeadlinePaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 28f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textDisplayPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 36f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textDisplayBoldPaint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 36f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    val textDisplay2Paint = TextPaint().apply {
        color = context.resources.getColor(R.color.colorSecondary)
        typeface = Typeface.create(ResourcesCompat.getFont(context, R.font.graphik_th_regular), Typeface.NORMAL)
        textSize = 48f * CodeEngine.fontMultiplier
        isAntiAlias = true
    }

    canvas.translate(CodeEngine.padding.toFloat(), CodeEngine.padding.toFloat())

    val billingLayout = textLayout("${context.getString(R.string.invoice_number)} ${invoice?.objectId}", textTitleBoldPaint, width)
    billingLayout.draw(canvas)

    canvas.translate(0f, billingLayout.height.toFloat() + CodeEngine.newlineMargin)

    val billingDateWidth = textTitleBoldPaint.measureText(context.getString(R.string.billing_date)) + 20f

    val billingDateLayout = textLayout(context.getString(R.string.billing_date), textTitleBoldPaint, billingDateWidth.toInt())
    billingDateLayout.draw(canvas)

    canvas.translate(billingDateWidth, 0f)

    val createdDateLayout = textLayout(invoice?._createdAt?.reportDMYNumber(), textTitlePaint, width)
    createdDateLayout.draw(canvas)

    canvas.translate(billingDateWidth * -1, createdDateLayout.height.toFloat() + CodeEngine.newlineMargin + 40)

    val addressLayout = textLayout(home?.address, textDisplayBoldPaint, width)
    addressLayout.draw(canvas)

    canvas.translate(0f, addressLayout.height.toFloat() + CodeEngine.newlineMargin)

    val projectTitleLayout = textLayout(home?.title, textBodyBoldPaint, width)
    projectTitleLayout.draw(canvas)

    canvas.translate(0f, projectTitleLayout.height.toFloat() + CodeEngine.newlineMargin + 20)

    val amountTitleLayout = textLayout(context.getString(R.string.total_unpaid_balance), textBodyPaint, width)
    amountTitleLayout.draw(canvas)

    canvas.translate(0f, amountTitleLayout.height.toFloat() + CodeEngine.newlineMargin)

    val amountLayout = textLayout(invoice?.amount?.toCurrencyFormat(), textDisplay2Paint, width)
    amountLayout.draw(canvas)

    canvas.translate(0f, amountLayout.height.toFloat() + CodeEngine.newlineMargin + 30)

    val dateTitleLayout = textLayout(context.getString(R.string.due_date), textBodyPaint, width)
    dateTitleLayout.draw(canvas)

    canvas.translate(0f, dateTitleLayout.height.toFloat() + CodeEngine.newlineMargin)

    val dateLayout = textLayout(invoice?._dueAt?.reportDMYFull(), textTitleBoldPaint, width)
    dateLayout.draw(canvas)


    return finalBitmap
}

fun textLayout(text: String?, paint: TextPaint, width: Int): StaticLayout {
    return StaticLayout(text, paint, width, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false)
}

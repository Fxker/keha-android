package com.sansiri.homeservice.data.repository.parking

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.data.network.auth.ParkingService
import com.sansiri.homeservice.data.network.networkCall
import com.sansiri.homeservice.model.api.partner.parking.SmartParkingStatus
import kotlinx.coroutines.*


/**
 * Created by sansiri on 11/7/17.
 */
class SmartParkingRepositoryImpl(val service: ParkingService): SmartParkingRepository {
    private val FETCH_TIME_INTERVAL = 10000L

    private val smartParkingStatus = MutableLiveData<Resource<SmartParkingStatus>>()
    private var fetchSmartParkingStatusJob: Job? = null

    override fun getSmartParkingStatus(unitId: String): MutableLiveData<Resource<SmartParkingStatus>> {
        fetchSmartParkingStatusJob = fetchJob(unitId)
        return smartParkingStatus
    }

    private fun fetchJob(unitId: String): Job = GlobalScope.launch {
        while(true) {
            withContext(Dispatchers.Main) {
                networkCall<SmartParkingStatus>(smartParkingStatus) {
                    client = service.getSmartParkingStatus(unitId)
                }
            }
            delay(FETCH_TIME_INTERVAL)
        }
    }

    override fun destroy() {
        fetchSmartParkingStatusJob?.cancel()
    }
}
package com.sansiri.homeservice.ui.voicecommand

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.onionshack.onionspeech.*
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.data.database.PreferenceHelper
import com.sansiri.homeservice.data.database.PreferenceHelper.get
import com.sansiri.homeservice.data.database.PreferenceHelper.set
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.util.ImageCompress
import com.sansiri.homeservice.util.alertError
import java.util.*

class VoiceCommandActivity : BaseActivity<VoiceCommandContract.View, VoiceCommandContract.Presenter>(), VoiceCommandContract.View {
    override var mPresenter: VoiceCommandContract.Presenter = VoiceCommandPresenter()
    private var isLaunchVoiceCommandScreen = false
    private var mHome: Home? = null
    var demoBookingNoVacancy = false

    companion object {
        val HOME = "HOME"
        val IS_MOCK = "IS_MOCK"

        fun start(activity: Activity, home: Home, isMock: Boolean) {
            val intent = Intent(activity, VoiceCommandActivity::class.java)
            intent.putExtra(HOME, home)
            intent.putExtra(IS_MOCK, isMock)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ProgressBar(this))

        var isMock = false
        if (Intent.ACTION_VOICE_COMMAND.equals(intent?.action)) {
            val prefs = PreferenceHelper.defaultPrefs(this)
            val myHome = prefs[PreferenceHelper.MY_HOME, ""] ?: ""
            if (myHome.isNotEmpty()) {
                try {
                    mHome = Gson().fromJson(myHome, Home::class.java)
                } catch (e: JsonSyntaxException) {
                    showShortCutError()
                }
            } else {
                showShortCutError()
            }
            isMock = true
        } else {
            mHome = intent?.getParcelableExtra<Home>(HOME)
            isMock = intent?.getBooleanExtra(IS_MOCK, false) ?: false

            val prefs = PreferenceHelper.defaultPrefs(this)
            prefs[PreferenceHelper.MY_HOME] = Gson().toJson(mHome)
        }

        if (BuildConfig.BUILD_TYPE.contains("release")) {
            OnionSpeech.setServer(OnionSpeech.Server.Production)
        } else if (BuildConfig.BUILD_TYPE.contentEquals("staging")) {
            OnionSpeech.setServer(OnionSpeech.Server.Staging)
        } else {
            OnionSpeech.setServer(OnionSpeech.Server.Staging)
        }

        if (mHome != null) {
            mPresenter.init(mHome!!, isMock)
        } else {

        }
    }


    override fun launchVoiceCommandScreen(userId: String, rooms: List<OnionRoom>, facilities: List<OnionFacility>) {
        isLaunchVoiceCommandScreen = true

        val onionService = object : OnionHomeService {

            override fun uploadImage(image: Bitmap, callback: OnionHomeService.UploadImageCallback?) {
                 val scaledBitmap = ImageCompress().resizeImage(image)
                 mPresenter.uploadImage(scaledBitmap, { url ->
                     callback?.completed(url)
                 }){
                     callback?.failed(it.message)
                 }

            }

            override fun getUtilityBalances(callback: OnionHomeService.GetUtilityBalanceCallback?) {
                mPresenter.actionCheckInvoice { map, date ->
                    callback?.completed(map, date)
                }
            }

            override fun openFacilityBookingPage(id: String) {
                mPresenter.requestToOpenFacility(id)
               // FacilityBookingDetailActivity.start(this@VoiceCommandActivity, mHome?.projectId ?: "", mHome?.unitId ?: "")
            }

            override fun controlDevice(controlDevice: String?, param: OnionHomeControlParameter) {
                rooms.forEach { room ->
                    val device = room.devices.find { it.id == controlDevice }
                    if (device != null) {
                        mPresenter.actionHomeAutomation(device, param)
                    }
                }
            }

            override fun bookFacility(id: String, startTime: Date, endTime: Date, callback: OnionHomeService.BookFacilityCallback?) {
                mPresenter.bookFacility(id, startTime, endTime, {
                    callback?.completed(OnionHomeService.BookFacilityCallback.BookingResult.Success, null)
                }) { result, data ->
                    callback?.completed(result, data)
                }
            }

            override fun checkFacilityVacancy(id: String?, callback: OnionHomeService.CheckFacilityVacancyCallback?) {
                callback?.completed(
                        Calendar.getInstance().apply {
                            set(Calendar.MINUTE, 0)
                            add(Calendar.HOUR, 1)
                        }.time,
                        Calendar.getInstance().apply {
                            set(Calendar.MINUTE, 0)
                            add(Calendar.HOUR, 2)
                        }.time)
            }
        }

        OnionSpeech(userId, onionService, rooms, facilities).startSpeech(this)
    }


    override fun launchDemoVoiceCommandScreen(userId: String) {
        isLaunchVoiceCommandScreen = true

        val devices = arrayListOf<OnionDevice>(
                OnionDevice("1", "TV Light", OnionDevice.Type.Light),
                OnionDevice("2", "Chandelier", OnionDevice.Type.Light),
                OnionDevice("3", "Bed Head Light", OnionDevice.Type.Dimmer),
                OnionDevice("4", "M.Bedroom AC", OnionDevice.Type.AirConditioner),
                OnionDevice("5", "M.Bedroom Blind", OnionDevice.Type.Blind)

        )

        val rooms = arrayListOf<OnionRoom>(
                OnionRoom("929292", "M.Bedroom", "Bedroom", devices)
        )

        val start = Calendar.getInstance()
        start.set(Calendar.HOUR_OF_DAY, 8)
        start.set(Calendar.MINUTE, 0)
        start.set(Calendar.SECOND, 0)
        start.set(Calendar.MILLISECOND, 0)

        val end = Calendar.getInstance()
        end.set(Calendar.HOUR_OF_DAY, 18)
        end.set(Calendar.MINUTE, 0)
        end.set(Calendar.SECOND, 0)
        end.set(Calendar.MILLISECOND, 0)


        val facilities = arrayListOf<OnionFacility>(
                OnionFacility("1", "Library HomeUpgradeRoom", start.time, end.time),
                OnionFacility("2", "Tea HomeUpgradeRoom", start.time, end.time),
                OnionFacility("3", "Social Lounge", start.time, end.time),
                OnionFacility("4", "Screening HomeUpgradeRoom", start.time, end.time),
                OnionFacility("5", "Spa HomeUpgradeRoom", start.time, end.time),
                OnionFacility("6", "Multipurpose HomeUpgradeRoom", start.time, end.time)
        )

        OnionSpeech(userId, object : OnionHomeService {
            override fun uploadImage(p0: Bitmap?, p1: OnionHomeService.UploadImageCallback?) {
                Toast.makeText(this@VoiceCommandActivity, "สมมติว่าอัพรูปเสร็จแล้ว", Toast.LENGTH_SHORT).show()
                p1?.completed("")
            }

            override fun getUtilityBalances(callback: OnionHomeService.GetUtilityBalanceCallback?) {
                val calendar = Calendar.getInstance()
                calendar.add(Calendar.DAY_OF_YEAR, 1)
                val tomorrow = calendar.time

                val map = if (mHome?.isTransfer != false) {
                    hashMapOf(
                            Pair(OnionHomeService.UtilityType.Insurance, rand(500, 1000).toDouble()),
                            Pair(OnionHomeService.UtilityType.Water, rand(100, 1000).toDouble()),
                            Pair(OnionHomeService.UtilityType.CommonFee, rand(100, 200).toDouble()),
                            Pair(OnionHomeService.UtilityType.Deposit, rand(100, 200).toDouble() * 10)
                    )
                } else {
                    hashMapOf(
                            Pair(OnionHomeService.UtilityType.UpcomingPayment, rand(10000, 50000).toDouble()),
                            Pair(OnionHomeService.UtilityType.OverduePayment, rand(10000, 50000).toDouble())
                    )
                }

                callback?.completed(
                        map,
                        tomorrow
                )
            }

            override fun openFacilityBookingPage(id: String) {
                Toast.makeText(this@VoiceCommandActivity, "สมมติว่าพาไปเปิดหน้า Facility Booking", Toast.LENGTH_SHORT).show()
                // FacilityBookingDetailActivity.start(this@VoiceCommandActivity, mHome?.projectId ?: "", mHome?.unitId ?: "", FacilityBooking())
            }

            override fun controlDevice(controlDevice: String?, param: OnionHomeControlParameter) {
//                if (controlDevice == devices[0].id) {
//                    ApiRepositoryProvider.provideOnionShackRepository().dimmerOn(if (param.controlType.name == "On") "100" else "0")
//                } else {
//                    ApiRepositoryProvider.provideOnionShackRepository().dimmerOn(param.percent.toInt().toString())
//                }
                // ApiRepositoryProvider.provideOnionShackRepository().dimmerOn(param?.con)
            }

            override fun bookFacility(id: String?, p1: Date?, p2: Date?, callback: OnionHomeService.BookFacilityCallback?) {
                val rand = if (demoBookingNoVacancy) 0 else rand(0, 2)

                when (rand) {
                    0 -> {
                        callback?.completed(OnionHomeService.BookFacilityCallback.BookingResult.Success, null)
                        demoBookingNoVacancy = false
                    }
                    1 -> {
                        demoBookingNoVacancy = true
                        val calendar = Calendar.getInstance()
                        calendar.time = p1
                        calendar.add(Calendar.HOUR, 2)
                        val start = calendar.time
                        calendar.add(Calendar.HOUR, 3)
                        val end = calendar.time

                        callback?.completed(OnionHomeService.BookFacilityCallback.BookingResult.Busy,
                                mapOf(
                                        Pair(OnionHomeService.BookFacilityCallback.BookingResultAdditionalInfo.SuggestionFromDate, start),
                                        Pair(OnionHomeService.BookFacilityCallback.BookingResultAdditionalInfo.SuggestionToDate, end)
                                )
                        )
                    }
                    2 -> {
//                        callback?.completed(OnionHomeService.BookFacilityCallback.BookingResult.ExceedMaximum,
//                                mapOf(
//                                        Pair(OnionHomeService.BookFacilityCallback.BookingResultAdditionalInfo.MaximumBookingAllowance, 3)
//                                )
//                        )
                    }
                    else -> {
                        callback?.completed(OnionHomeService.BookFacilityCallback.BookingResult.Success, null)
                    }
                }
            }

            override fun checkFacilityVacancy(id: String?, callback: OnionHomeService.CheckFacilityVacancyCallback?) {
                callback?.completed(
                        Calendar.getInstance().apply {
                            set(Calendar.MINUTE, 0)
                            add(Calendar.HOUR, 1)
                        }.time,
                        Calendar.getInstance().apply {
                            set(Calendar.MINUTE, 0)
                            add(Calendar.HOUR, 2)
                        }.time)
                Log.d("OnionShack", id)
            }
        }, rooms, facilities).startSpeech(this)
    }

    private fun rand(from: Int, to: Int): Int {
        val random = Random()
        return random.nextInt(to - from) + from
    }

    private fun showShortCutError() {
        Toast.makeText(this, "Please open Home and launch Voice Command manually.", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onDestroy() {
        mPresenter.destroy()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        if (isLaunchVoiceCommandScreen) {
            finish()
        } else {
            sendScreenView("VOICE_COMMAND")
        }
    }

    override fun showError(message: String) {
        alertError(message)
    }
}

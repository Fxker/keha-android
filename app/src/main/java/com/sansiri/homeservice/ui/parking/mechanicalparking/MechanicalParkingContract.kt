package com.sansiri.homeservice.ui.parking.mechanicalparking

import com.sansiri.homeservice.model.api.ParkingQueue
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 2/27/18.
 */
interface MechanicalParkingContract {
    interface Presenter : BasePresenter<View> {
        var mQueueId: String

        fun attachParkingQueue(projectId: String)
        fun processQRScanning(queueId: String?)
        fun invokeSummary()
        fun reset()
    }

    interface View : BaseView {
        fun showSummary(queue: List<ParkingQueue>)
        fun showCardStatus(myQueue: ParkingQueue)
        fun hideCardStatus()
        fun showMyQueueETA(queueNo: String, eta: Int)
        fun enableQRButton()
        fun alert(timeToAlert: Int)
        fun save(queueId: String)
        fun clearSave()
    }
}
package com.sansiri.homeservice.ui.epayment.gateway

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.ccpp.pgw.sdk.android.model.api.response.TransactionResultResponse
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.payment.PaymentChannel
import com.sansiri.homeservice.model.api.payment.PaymentGatewayData
import com.sansiri.homeservice.model.api.payment.PaymentResponse
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.PaymentGateway2C2PFragment
import com.sansiri.homeservice.ui.epayment.gateway.gateway2c2p.result.PaymentGateway2C2PTransactionResultFragment
import com.sansiri.homeservice.ui.epayment.gateway.phone.PaymentGatewayPhoneVerifyFragment
import com.sansiri.homeservice.util.addFragment
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_fragment_container2.*
import kotlinx.android.synthetic.main.appbar_center_title.*

class PaymentGatewayActivity : LocalizationActivity() {


    companion object {
        const val DATA = "DATA"
        const val TYPE = "TYPE"
        private const val PHONE_VERIFY = "PHONE_VERIFY"
        private const val GATEWAY_2C2P = "2C2P"
        const val RESPONSE = "RESPONSE"
        const val DETAIL_IMAGE = "DETAIL_IMAGE"
        const val PAYMENT_CHANNEL = "PAYMENT_CHANNEL"
        const val PAYMENT_RESPONSE = "PAYMENT_RESPONSE"
        const val ANALYTIC_TITLE = "ANALYTIC_TITLE"
        const val PHONE_CODE = 1
        const val GATEWAY_2C2P_CODE = 2

        fun startPhoneVerify(activity: Activity, paymentGatewayData: PaymentGatewayData) {
            val intent = Intent(activity, PaymentGatewayActivity::class.java)
            intent.putExtra(DATA, paymentGatewayData)
            intent.putExtra(TYPE, PHONE_VERIFY)
            activity.startActivityForResult(intent, PHONE_CODE)
        }

        fun startPhoneVerify(fragment: Fragment, paymentGatewayData: PaymentGatewayData) {
            val intent = Intent(fragment.context, PaymentGatewayActivity::class.java)
            intent.putExtra(DATA, paymentGatewayData)
            intent.putExtra(TYPE, PHONE_VERIFY)
            fragment.startActivityForResult(intent, PHONE_CODE)
        }

        fun start2c2p(activity: Activity, paymentChannel: PaymentChannel?, response: PaymentResponse?, detailImage: ByteArray?, analyticTitle: String?) {
            val intent = Intent(activity, PaymentGatewayActivity::class.java)
            intent.putExtra(TYPE, GATEWAY_2C2P)
            intent.putExtra(PAYMENT_RESPONSE, response)
            intent.putExtra(PAYMENT_CHANNEL, paymentChannel)
            intent.putExtra(DETAIL_IMAGE, detailImage)
            intent.putExtra(ANALYTIC_TITLE, analyticTitle)
            activity.startActivityForResult(intent, GATEWAY_2C2P_CODE)
        }

        fun start2c2p(fragment: Fragment, paymentChannel: PaymentChannel?, response: PaymentResponse?, detailImage: ByteArray?, analyticTitle: String?) {
            val intent = Intent(fragment.context, PaymentGatewayActivity::class.java)
            intent.putExtra(TYPE, GATEWAY_2C2P)
            intent.putExtra(PAYMENT_RESPONSE, response)
            intent.putExtra(PAYMENT_CHANNEL, paymentChannel)
            intent.putExtra(DETAIL_IMAGE, detailImage)
            intent.putExtra(ANALYTIC_TITLE, analyticTitle)
            fragment.startActivityForResult(intent, GATEWAY_2C2P_CODE)
        }
    }

    private var mPayment2C2PResponse: PaymentResponse? = null
    private var mPayment2C2PFragment: PaymentGateway2C2PFragment? = null
    private var analyticTitle: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container2)


        val paymentGatewayData = intent.getParcelableExtra<PaymentGatewayData>(DATA)
        val paymentChannel = intent.getParcelableExtra<PaymentChannel>(PAYMENT_CHANNEL)
        val detailImage = intent.getByteArrayExtra(DETAIL_IMAGE)
        analyticTitle = intent.getStringExtra(ANALYTIC_TITLE)
        mPayment2C2PResponse = intent.getParcelableExtra<PaymentResponse>(PAYMENT_RESPONSE)

        setBackToolbar(findViewById(R.id.toolbar), "")
        textToolbar.text = paymentChannel?.title ?: ""

        when (intent.getStringExtra(TYPE)) {
            PHONE_VERIFY -> {
                paymentGatewayData?.let { launchPhoneVerify(it) }
            }
            GATEWAY_2C2P -> {
                launch2c2p(mPayment2C2PResponse, detailImage)
            }
        }
    }

    private fun launchPhoneVerify(paymentGatewayData: PaymentGatewayData) {
        addFragment(PaymentGatewayPhoneVerifyFragment.newInstance(paymentGatewayData), false)
        scrollView.scrollTo(0, 0)
    }

    private fun launch2c2p(paymentResponse: PaymentResponse?, detailImage: ByteArray?) {
        mPayment2C2PFragment = PaymentGateway2C2PFragment.newInstance(paymentResponse, detailImage, analyticTitle)
        mPayment2C2PFragment?.let { addFragment(it, false) }
        scrollView.scrollTo(0, 0)
    }


    fun launch2C2PTransactionResult(billingEmail: String?) {
        val transactionId = mPayment2C2PResponse?.transactionId
        addFragment(PaymentGateway2C2PTransactionResultFragment.newInstance(transactionId, billingEmail, analyticTitle), false)
        scrollView.scrollTo(0, 0)
    }

    fun update2c2p(response: TransactionResultResponse) {
        mPayment2C2PFragment?.onResponse(response)
    }

    fun addFragment(fragment: Fragment) {
        addFragment(fragment, true)
        scrollView.scrollTo(0, 0)
    }

    fun finish2c2p() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


}

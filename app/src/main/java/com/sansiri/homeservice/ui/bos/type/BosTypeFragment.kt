package com.sansiri.homeservice.ui.bos.type

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.menu.BosMenu
import com.sansiri.homeservice.ui.adapter.GridMenuAdapter
import com.sansiri.homeservice.ui.base.BaseV2Fragment
import com.sansiri.homeservice.ui.bos.BosActivity
import com.sansiri.homeservice.ui.bos.BosActivity.Companion.TYPE_FACILITY
import com.sansiri.homeservice.ui.bos.BosActivity.Companion.TYPE_UNIT
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.fragment_bos_type.*
import org.koin.androidx.scope.currentScope

class BosTypeFragment: BaseV2Fragment<BosTypeContract.View, BosTypeContract.Presenter>(), BosTypeContract.View {
    override val mPresenter: BosTypeContract.Presenter by currentScope.inject()

    private var mUnitId: String? = null

    companion object {
        const val UNIT_ID = "UNIT_ID"

        fun newInstance(unitId: String): BosTypeFragment {
            return BosTypeFragment().apply {
                val bundle = Bundle()
                bundle.putString(UNIT_ID, unitId)
                arguments = bundle
            }
        }
    }

    val mAdapter = GridMenuAdapter(null, null) {
        if (it is BosMenu) {
            mPresenter.selectJobType(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUnitId = arguments?.getString(UNIT_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bos_type, container, false)
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("BOS_TYPE")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        listType.apply {
            val gridLayoutManager = GridLayoutManager(context, resources.getInteger(R.integer.grid_column))
            layoutManager = gridLayoutManager
            itemAnimator = ScaleInAnimator(OvershootInterpolator(1f) as Interpolator?)
            adapter = mAdapter
        }

        tabUnit.findViewById<ImageView>(R.id.imageLogo).setImageResource(R.drawable.ic_door)
        tabUnit.findViewById<TextView>(R.id.textTitle).text = getString(R.string.unit)

        tabFacility.findViewById<ImageView>(R.id.imageLogo).setImageResource(R.drawable.ic_building)
        tabFacility.findViewById<TextView>(R.id.textTitle).text = getString(R.string.common_area)

        tabGroupOptions.apply {
            addTab(
                    tabUnit,
                    tabFacility
            )
            addViewIdAndColorStatus(R.id.imageLogo, resources.getColor(R.color.textLight), resources.getColor(R.color.textAccent))
            addViewIdAndColorStatus(R.id.toggleArea, resources.getColor(R.color.colorAccent), resources.getColor(R.color.background))
            addViewIdAndColorStatus(R.id.textTitle, resources.getColor(R.color.textLight), resources.getColor(R.color.textPrimary))
        }

        tabGroupOptions.setOnItemSelectedListener { position ->
            when (position) {
                0 -> {
                    mPresenter.selectAreaType(TYPE_UNIT)
                }
                1 -> {
                    mPresenter.selectAreaType(TYPE_FACILITY)
                }
            }
        }

        tabGroupOptions.selectItem(0)

        if (mUnitId != null) {
            mPresenter.init(mUnitId!!)
            mPresenter.start()
        }
    }

    override fun bindJobTypes(types: List<BosMenu>) {
        listType.show()
        mAdapter.setData(types)
    }

    override fun launchBosDetail(groupType: Int, jobType: BosMenu) {
        if (activity is BosActivity) {
            (activity as BosActivity).launchDetail(groupType, jobType)

            val label = if (groupType == TYPE_UNIT) "TYPE_UNIT" else "TYPE_COMMON_AREA"
            sendEvent("BOS_TYPE", "SELECT", label, "1", Pair("TypeId", jobType.id ?: ""))
        }
    }


    override fun showLoading() {
        progressBar.show()
    }

    override fun hideLoading() {
        progressBar.hide()
    }

    override fun showError(message: String) {
        activity?.alertError(message)
    }
}
package com.sansiri.homeservice.ui.base

import android.graphics.Bitmap
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.adapter.MultiSelectPhotoAdapter
import com.sansiri.homeservice.util.ImageCompress
import com.sansiri.homeservice.util.compress
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import io.reactivex.Observable

abstract class PhotoV2Fragment<in V : BaseView, T : BasePresenter<V>> : BaseV2Fragment<V, T>(), IPickResult {
    var mPickUpDialog: PickImageDialog? = null
    val bitmaps = mutableListOf<Bitmap>()
    val mAdapter = MultiSelectPhotoAdapter() {
        mPickUpDialog?.show(activity)
    }

    val compressBitmapObservable = Observable.create<List<Pair<String, ByteArray>>> { emitter ->
        val imageToUpload = mutableListOf<Pair<String, ByteArray>>()
        try {
            bitmaps.forEach { bitmap ->
                val scaledBitmap = ImageCompress().resizeImage(bitmap)
                val byteArray = scaledBitmap.compress()
                imageToUpload.add(Pair("${SystemClock.uptimeMillis()}.jpg", byteArray))
            }
            emitter.onNext(imageToUpload)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val setup = PickSetup().setSystemDialog(true)
        mPickUpDialog = PickImageDialog.build(setup)
        mPickUpDialog?.setOnPickResult(this)
    }

    override fun onPickResult(result: PickResult) {
        if (result.bitmap != null) {
            bitmaps.add(result.bitmap)
            mAdapter.notifyDataSetChanged()
        } else {
            showError(getString(R.string.error_photo_decode))
        }
    }
}
package com.sansiri.homeservice.ui.home

import android.content.res.Resources
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.UnitInfo

import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_project.view.*

/**
 * Created by oakraw on 9/29/2017 AD.
 */
class ProjectAdapter(val itemClick: (Home) -> Unit) : RecyclerView.Adapter<ProjectAdapter.ViewHolder>() {
    private var mData = listOf<Home>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.view) {
            // set card to relating with screen programmatically
            val params = layoutParams
            val displayWidth = Resources.getSystem().getDisplayMetrics().widthPixels
            params.width = displayWidth - (context.resources.getDimension(R.dimen.project_horizontal_margin).toInt() * 2)
            layoutParams = params
        }
        holder.bind(mData[position])
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.view_project)
        return ViewHolder(view, itemClick)
    }


    fun setData(units: List<Home>) {
        if (mData.size != units.size) {
            mData = units
        }
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View, val itemClick: (Home) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(home: Home) {
            with(view) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    card.preventCornerOverlap = false
                }

                card.isClickable = true
                card.setOnClickListener {
                    itemClick(home)
                }

                textAddress.text = home.address
                textProject.text = home.title
                if (home.image.isNotEmpty()) {
                    Glide.with(context).load(home.image).placeholder(R.drawable.placeholder_project).transition(DrawableTransitionOptions.withCrossFade()).into(imageProject)
                }
            }
        }
    }

}
package com.sansiri.homeservice.ui.facilitybooking.detail

import com.sansiri.homeservice.model.Booking
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 10/16/17.
 */
interface FacilityBookingDetailContract {
    interface Presenter : BasePresenter<View> {
        fun init(projectId: String, unitId: String, address: String, facilityId: FacilityBooking, booking: Booking)
        fun requestTermsAndConditions()
        fun submit(note: String, endTime: String)
        fun cancelBooking()
    }

    interface View : BaseView {
        fun showTermsAndConditions(text: String)
        fun success()
        fun showError(res: Int)
        fun showLoading()
        fun hideLoading()
    }
}
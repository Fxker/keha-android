package com.appy.android.sdk.control.daikin

enum class DaikinOnOffCommand: APDaikinParameter {
    ON {
        override fun action(): String {
            return "1"
        }
    },
    Off {
        override fun action(): String {
            return "0"
        }
    };
}
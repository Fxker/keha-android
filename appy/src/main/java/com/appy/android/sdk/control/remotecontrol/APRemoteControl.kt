package com.appy.android.sdk.control.remotecontrol

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.data.rest.JsonModel.ControlJsonModel
import com.appy.android.sdk.control.APAction
import com.appy.android.sdk.control.APControl

class APRemoteControl : APControl, Parcelable {
    var mActions: List<APActionRemoteControl>? = null

    override val actions: List<APAction<*>>
        get() = mActions.orEmpty()

    constructor() : super()

    constructor(source: Parcel) : super(source) {
        mActions = source.createTypedArrayList(APActionRemoteControl)
    }

    override fun init(controlJsonModel: ControlJsonModel) {
        super.init(controlJsonModel)

        this.mActions = controlJsonModel.actions?.map {
            val action = APActionRemoteControl()
            action.init(it)

            action
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeTypedList(mActions)
    }

    companion object CREATOR : Parcelable.Creator<APRemoteControl> {
        const val TYPE = "APRemoteControl"

        override fun createFromParcel(parcel: Parcel): APRemoteControl {
            val type = parcel.readString()
            return APRemoteControl(parcel)
        }

        override fun newArray(size: Int): Array<APRemoteControl?> {
            return arrayOfNulls(size)
        }
    }
}
package com.appy.android.code.domain.model.command.remotecontrol

import com.appy.android.code.data.rest.JsonBody.BaseCommandJsonBody
import com.google.gson.annotations.SerializedName

internal class SendCommandRemoteControlJsonBody : BaseCommandJsonBody<String>() {
  @SerializedName("command_params")
  var commandParams: String? = null
}
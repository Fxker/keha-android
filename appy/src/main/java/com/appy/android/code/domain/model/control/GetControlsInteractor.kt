package com.appy.android.code.domain.model.control

import com.appy.android.code.base.domain.executor.PostExecutionThread
import com.appy.android.code.base.domain.executor.ThreadExecutor
import com.appy.android.code.base.domain.interactor.AppyRestInteractor
import com.appy.android.code.data.rest.RestClient
import com.appy.android.code.data.rest.service.HomeService
import com.appy.android.sdk.control.ControlsApiResult
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ssa-dev-4 on 27/9/2560.
 */
internal class GetControlsInteractor @Inject
constructor(threadExecutor: ThreadExecutor,
            postExecutionThread: PostExecutionThread,
            restClient: RestClient) : AppyRestInteractor<ControlsApiResult, ControlForm>(threadExecutor, postExecutionThread, restClient) {
    override fun buildApiUseCaseObservable(params: ControlForm): Observable<ControlsApiResult> {
        return restClient
                .createAPIService(HomeService::class.java)
                .getControls(params.headers, params.homeId, params.params)
    }
}
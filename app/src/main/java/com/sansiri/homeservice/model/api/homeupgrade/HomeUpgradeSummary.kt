package com.sansiri.homeservice.model.api.homeupgrade

import android.os.Parcel
import android.os.Parcelable

data class HomeUpgradeSummary(
        override val id: String,
        override val nameTh: String?,
        override val nameEn: String?,
        val imageUrl: String? = null,
        val amount: Int = 0
) : HomeUpgrade, Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(nameTh)
        writeString(nameEn)
        writeString(imageUrl)
        writeInt(amount)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeUpgradeSummary> = object : Parcelable.Creator<HomeUpgradeSummary> {
            override fun createFromParcel(source: Parcel): HomeUpgradeSummary = HomeUpgradeSummary(source)
            override fun newArray(size: Int): Array<HomeUpgradeSummary?> = arrayOfNulls(size)
        }
    }
}
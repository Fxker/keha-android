package com.appy.android.sdk.room

import com.appy.android.code.base.data.rest.result.AppyApiApiStatusResult
import com.appy.android.code.base.data.rest.result.AppyApiStatusResult

/**
 * Created by ssa-dev-4 on 2/10/2560.
 */
class RoomApiResult {
    var status: AppyApiStatusResult? = null
    var apiStatus: AppyApiApiStatusResult? = null
    var datas: APRoom? = APRoom()
}
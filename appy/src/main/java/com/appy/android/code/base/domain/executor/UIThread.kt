package com.appy.android.code.base.domain.executor

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class UIThread @Inject
constructor() : BasePostExecutionThread(), PostExecutionThread

package com.sansiri.homeservice.data.database

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase

/**
 * Helper class for working with the SQLiteDatabase.
 */
object SqliteDatabaseTestHelper {

    fun insertOrviboHome(helper: SqliteTestDbOpenHelper) {
        val db = helper.writableDatabase
        val values = ContentValues()
        values.put("unitId", "test")
        values.put("username", "test")
        values.put("passwordMD5", "test")
        values.put("familyId", "test")

        db.insertWithOnConflict("orvibo_home", null, values,
                SQLiteDatabase.CONFLICT_REPLACE)

        db.close()
    }

    fun createTable(helper: SqliteTestDbOpenHelper) {
        val db = helper.writableDatabase

        db.execSQL("CREATE TABLE IF NOT EXISTS orvibo_home (unitId TEXT NOT NULL PRIMARY KEY, username TEXT NOT NULL, passwordMD5 TEXT NOT NULL, familyId TEXT)")

        db.close()
    }

    fun clearDatabase(helper: SqliteTestDbOpenHelper) {
        val db = helper.writableDatabase

        db.execSQL("DROP TABLE IF EXISTS users")

        db.close()
    }
}
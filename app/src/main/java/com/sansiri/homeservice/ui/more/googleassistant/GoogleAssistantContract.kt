package com.sansiri.homeservice.ui.more.googleassistant

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.HomeGoogleAssistant
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface GoogleAssistantContract {
    interface Presenter : BasePresenter<View> {
        fun fetch()
        fun onGoogleLoggedIn(idToken: String?, email: String?)
        fun requestForSelectUnit()
        fun selectUnit(unitId: String)
        fun validateAndSubmit()
        fun logout()
    }

    interface View : BaseView {
        fun bindData(home: Home)
        fun launchProjectSelector(homes: List<Home>)
        fun showGoogleLoginButton()
        fun showGoogleLoggedIn(email: String?)
        fun showLoading()
        fun hideLoading()
        fun showUnregister()
        fun showSyncing()
        fun hideSyncing()
        fun showSuccess()
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.home.machine

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachine
import com.sansiri.homeservice.model.api.partner.trendywash.TrendyWashMachineStart
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface TrendyWashMachineContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String, userId: String, machine: TrendyWashMachine)
        fun startMachine(): MutableLiveData<Resource<TrendyWashMachineStart>>?
        fun startCountDown(seconds: Int)
        fun calculatePriceFromTime(second: Int)
    }

    interface View : BaseView {
        fun bindData(machine: TrendyWashMachine)
        fun showMachineStarting()
        fun showMachineAvailable()
        fun showMachineBusy()
        fun updateCountDownTime(totalSecs: Int)
        fun showCalculatedPrice(price: Int)
        fun setNotificationAlarm(seconds: Int, machine: TrendyWashMachine?)
    }
}
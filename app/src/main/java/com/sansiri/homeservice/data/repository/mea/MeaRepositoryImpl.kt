package com.sansiri.homeservice.data.repository.mea

import com.mea.energysdk.common.Env
import com.mea.energysdk.common.MEAEnergyWidgetCallback
import com.mea.energysdk.service.MEAEnergy
import com.mea.energysdk.service.MEAEnergyCallback
import com.mea.energysdk.widget.MEAEnergyWidget
import com.sansiri.homeservice.BuildConfig
import com.sansiri.homeservice.data.network.auth.MEAService
import com.sansiri.homeservice.model.api.partner.mea.MEAInfo
import org.json.JSONArray
import org.json.JSONObject

class MeaRepositoryImpl(apiKey: String, clientCode: String, val meaService: MEAService) : MeaRepository {
    init {
        MEAEnergy.getInstance().apply {
            setAPIKey(apiKey, clientCode)
            setEnv(
                    if (BuildConfig.BUILD_TYPE == "debug") {
                        Env.STAGING
                    } else {
                        Env.PRODUCTION
                    }
            )
        }
    }

    override fun getConsent() = meaService.getMEAConsent()

    override fun acceptConsent() = meaService.acceptMEAConsent()

    override fun registerCA(ca: String, projectId: String, meaInfo: MEAInfo?, onSuccess: () -> Unit, onError: (JSONArray?) -> Unit) {
        MEAEnergy.getInstance().registerCA(
                ca, // เลขที่แสดงสัญญา (CA)
                projectId, // รหัสโครงการ
                meaInfo?.projectNameTh ?: "", // ชื่อโครงการภาษาไทย
                meaInfo?.projectNameEn ?: "", // ชื่อโครงการภาษาอังกฤษ
                meaInfo?.projectAddressTh ?: "test", // ที่อยู่โครงการภาษาไทย
                meaInfo?.projectAddressEn ?: "test", // ที่อยู่โครงการภาษาอังกฤษ
                object : MEAEnergyCallback {
                    override fun onFail(error: JSONArray?) {
                        onError(error)
                    }

                    override fun onSuccess(data: JSONObject?) {
                        onSuccess()
                    }
                })

    }

    override fun showReport(widget: MEAEnergyWidget, ca: String) {
        widget.initCA(ca, object : MEAEnergyWidgetCallback {
            override fun onFail(p0: JSONArray?) {
            }

            override fun onDone() {
            }
        })
    }

    override fun getMEAInfo(unitId: String) = meaService.getMEAInfo(unitId)

    override fun saveCA(unitId: String, ca: String) = meaService.saveCA(unitId, MEAInfo(contractAccountId = ca))

    override fun getCAFromQRCode(code: String) = processQR(code)

    private fun processQR(qrCodeString: String): String {
        val split = qrCodeString.split("\r".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var ca = ""
        if (split.size == 4) {
            ca = split[1].substring(0, 9)
        }

        return ca
    }
}
package com.appy.android.sdk.control

import android.os.Parcel
import android.os.Parcelable
import com.appy.android.code.base.models.BaseImageJsonModel

/**
 * Created by ssa-dev-4 on 7/11/2560.
 */
class APImage : BaseImageJsonModel {

    constructor() : super()

    constructor(source: Parcel) : super(source)

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<APImage> {
        override fun createFromParcel(parcel: Parcel): APImage {
            return APImage(parcel)
        }

        override fun newArray(size: Int): Array<APImage?> {
            return arrayOfNulls(size)
        }
    }
}
package com.sansiri.homeservice.model.api.homeupgrade.history

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import java.text.ParseException
import java.util.*

data class Installation(
		@SerializedName("dateTimeAt")
		val date: String? = null,
		val status: String? = null
) : Parcelable {
	constructor(source: Parcel) : this(
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(date)
		writeString(status)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<Installation> = object : Parcelable.Creator<Installation> {
			override fun createFromParcel(source: Parcel): Installation = Installation(source)
			override fun newArray(size: Int): Array<Installation?> = arrayOfNulls(size)
		}
	}

	val _date: Date?
		get() {
			return try {
				if (this.date == null) return null

				DateTime(this.date).toDate()
			} catch (e: ParseException) {
				null
			}
		}
}

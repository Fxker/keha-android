package com.sansiri.homeservice.ui.privilege

import android.os.Bundle
import android.view.Menu
import android.view.View
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.model.menu.ActionType
import com.sansiri.homeservice.ui.web.WebFragment
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.observe
import kotlinx.android.synthetic.main.activity_web.*
import kotlinx.android.synthetic.main.toolbar.*

class PrivilegeFragment : WebFragment() {
    companion object {
        fun newInstance(): PrivilegeFragment {
            return PrivilegeFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SCREEN_NAME_LOG = "SANSIRI_FAMILY"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        analyticProvider.sendScreenView(SCREEN_NAME_LOG)
        toolbar.hide()

        FirebaseRemoteConfigManager().getPrivilegeDynamicButton { dynamicMenu ->
            var decodedUrl = dynamicMenu.url ?: ""

            if (decodedUrl.contains("{langcode}")) {
                decodedUrl = decodedUrl.replace("{langcode}", getLanguageCode())
            }

            initWebView(decodedUrl, dynamicMenu.type == ActionType.EXTERNAL_BROWSER_WITH_HEADER_TOKEN)
        }
    }

    private fun getLanguageCode() = if (Store.getLocale()?.language == "th") {
        "th"
    } else {
        "en"
    }
}


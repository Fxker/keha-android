package com.sansiri.homeservice.ui.register.federatiallogin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient

import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.BaseFragment
import com.sansiri.homeservice.ui.register.RegisterActivity
import com.sansiri.homeservice.ui.register.emaillogin.EmailFragment
import com.sansiri.homeservice.ui.register.signup.SignUpFragment
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.sansiri.homeservice.data.network.AnalyticProvider
import com.sansiri.homeservice.data.network.firebase.FirebaseAuthManager
import com.sansiri.homeservice.data.network.firebase.FirebaseRemoteConfigManager
import com.sansiri.homeservice.util.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton


class LoginFragment : BaseFragment<LoginContract.View, LoginContract.Presenter>(), LoginContract.View {
    override var mPresenter: LoginContract.Presenter = LoginPresenter()
    private var isSignUp: Boolean = false
    private var provider: String? = null
    private var registeredWithEmailOrName: String? = null

    val mCallbackManager = CallbackManager.Factory.create()
    private var mGoogleSignInClient: GoogleSignInClient? = null

    companion object {
        val SIGN_UP = "SIGN_UP"
        val PROVIDER = "PROVIDER"
        val REGISTERED_EMAIL = "REGISTERED_EMAIL"
        val GOOGLE_SIGN_IN = 123

        val GOOGLE = "GOOGLE"
        val FACEBOOK = "FACEBOOK"
        val EMAIL = "EMAIL"

        fun newInstance(signUp: Boolean = false, provider: String? = null, registeredWithEmailOrName: String? = null): LoginFragment {
            val fragment = LoginFragment()

            val bundle = Bundle()
            bundle.putBoolean(SIGN_UP, signUp)
            bundle.putString(PROVIDER, provider)
            bundle.putString(REGISTERED_EMAIL, registeredWithEmailOrName)
            fragment.arguments = bundle

            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            isSignUp = arguments?.getBoolean(SIGN_UP) ?: false
            provider = arguments?.getString(PROVIDER)
            registeredWithEmailOrName = arguments?.getString(REGISTERED_EMAIL)
        }

        LoginManager.getInstance().registerCallback(mCallbackManager, object : FacebookCallback<LoginResult?> {
            override fun onError(error: FacebookException?) {
                Log.d("facebookLogin", error?.message)
                showError(error?.message ?: getString(R.string.error_default_message))
            }

            override fun onSuccess(result: LoginResult?) {
                Log.d("facebookLogin", result.toString())
                if (result?.accessToken?.token != null)
                    mPresenter.facebookLogin(result.accessToken)
            }

            override fun onCancel() {
                Log.d("facebookLogin", "")
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textTitle.text = if (isSignUp) getString(R.string.register) else getString(R.string.login)
        buttonEmail.text = if (isSignUp) getString(R.string.register_with_email) else getString(R.string.log_in_with_email)

        buttonFacebook.setOnClickListener {
            if (isSignUp) {
                sendEvent("SIGN_IN", "CLICK", "FACEBOOK")
            }
            LoginManager.getInstance().apply {
                logOut()
                logInWithReadPermissions(this@LoginFragment, Arrays.asList("public_profile", "email"))
            }
        }

        buttonGoogle.setOnClickListener {
            if (isSignUp) {
                sendEvent("SIGN_IN", "CLICK", "GOOGLE")
            }
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.google_server_client_id))
                    .requestEmail()
                    .build()

            mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso)

            val signInIntent = mGoogleSignInClient?.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
        }

        buttonEmail.setOnClickListener {
            if (isSignUp) {
                sendEvent("SIGN_IN", "CLICK", "EMAIL")
                (activity as RegisterActivity).replaceFragment(SignUpFragment.newInstance(), true)
            } else {
                sendEvent("SIGN_UP", "CLICK", "EMAIL")
                (activity as RegisterActivity).replaceFragment(EmailFragment.newInstance(), true)
            }
        }

        provider?.let {
            FirebaseRemoteConfigManager().fetch {
                if (it.getBoolean(FirebaseRemoteConfigManager.LOGIN_PRIORITY)) {
                    showCorrectLoginProvider(provider!!)
                }
            }
        }

        mPresenter.start()
    }

    override fun onResume() {
        super.onResume()
        if (isSignUp) {
            sendScreenView("SIGN_UP")
        } else {
            sendScreenView("SIGN_IN")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    mPresenter.googleLogin(account.id ?: "", account.email ?: "", account.idToken ?: "")
                }
            } catch (e: ApiException) {
                showError(e.localizedMessage ?: getString(R.string.error_something_went_wrong))
            }
        } else if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun launchNext() {
        val registerActivity = activity as RegisterActivity
        if (isSignUp) {
            showLoading()
            registerActivity.bindUser({
                hideLoading()
                mPresenter.validateAccount()
            }) {
                hideLoading()
                showError(it.showHttpError())
            }
        } else {
            mPresenter.validateAccount()
        }
    }

    override fun launchMainPage() {
        val registerActivity = activity as RegisterActivity
        registerActivity.launchMainPage()
    }

    override fun showLoading() {
        progressBar?.show()
    }

    override fun hideLoading() {
        progressBar?.hide()
    }

    override fun showMissingUser() {
        val message = if (registeredWithEmailOrName != null)
            String.format(getString(R.string.hint_registered_with_email), registeredWithEmailOrName)
        else
            getString(R.string.no_user_error)
        showError(message)
    }

    override fun showError(message: String) {
        //Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT).show()
        activity?.alertError(message)
    }

    override fun signOut() {
        FirebaseAuthManager.signOut(activity!!) {}
    }

    private fun showCorrectLoginProvider(provider: String) {
        buttonFacebook.hide()
        buttonGoogle.hide()
        textOr.hide()
        buttonEmail.hide()

        if (provider == FACEBOOK) {
            buttonFacebook.show()
        }

        if (provider == GOOGLE) {
            buttonGoogle.show()
        }

        if (provider == EMAIL) {
            buttonEmail.show()
        }
    }
}

package com.sansiri.homeservice.ui.downpayment

import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

/**
 * Created by sansiri on 11/14/17.
 */
interface DownpaymentContract {
    interface Presenter : BasePresenter<View> {
        fun fetchData(home: Hut)
        fun requestEPayment(downPaymentItem: DownpaymentItem)
    }

    interface View : BaseView {
        fun showNetPrice(netPrice: Double?)
        fun showPaidAmount(paidAmount: Double?)
        fun showRemainingAmount(remainAmount: Double?)
        fun addPaidList(downpaymentItem: DownpaymentItem)
        fun addFutureList(downpaymentItem: DownpaymentItem)
        fun addUpComing(downpaymentItem: DownpaymentItem)
        fun addOverdue(downpaymentItem: DownpaymentItem)
        fun launchDownpayment(home: Hut, downPaymentItem: DownpaymentItem)
    }
}
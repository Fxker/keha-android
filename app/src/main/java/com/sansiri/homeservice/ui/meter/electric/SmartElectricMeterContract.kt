package com.sansiri.homeservice.ui.meter.electric

import androidx.lifecycle.MutableLiveData
import com.sansiri.homeservice.data.network.Resource
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterDetail
import com.sansiri.homeservice.model.api.meter.electric.SmartElectricMeterOverAll
import com.sansiri.homeservice.ui.base.BasePresenter
import com.sansiri.homeservice.ui.base.BaseView

interface SmartElectricMeterContract {
    interface Presenter : BasePresenter<View> {
        fun init(unitId: String)
        fun fetch()
    }

    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun attachFetchingObserver(meterLatestDetail: MutableLiveData<Resource<SmartElectricMeterDetail>>, meterOverAll: MutableLiveData<Resource<SmartElectricMeterOverAll>>)
    }
}
package com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.sansiri.homeservice.R
import com.sansiri.homeservice.ui.base.AnalyticsActivity
import com.sansiri.homeservice.util.setBackToolbar
import kotlinx.android.synthetic.main.activity_trendy_wash_payment_gateway.*
import com.sansiri.homeservice.ui.base.BaseActivity
import com.sansiri.homeservice.ui.base.BaseV2Activity
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.creditcard.CreditCardFragment
import com.sansiri.homeservice.ui.washingmachine.trendywash.addmoney.payment.transfer.TransferFragment
import com.sansiri.homeservice.util.alertError
import com.sansiri.homeservice.util.replaceFragment
import com.sansiri.homeservice.util.tintImage
import org.koin.androidx.scope.currentScope


class TrendyWashPaymentGatewayActivity : AnalyticsActivity() {
    companion object {
        const val UNIT_ID = "UNIT_ID"
        const val USER_ID = "USER_ID"
        const val AMOUNT = "AMOUNT"
        const val CODE = 23

        fun start(fragment: Fragment, unitId: String, userId: String, amount: Int) {
            val intent = Intent(fragment.context, TrendyWashPaymentGatewayActivity::class.java)
            intent.putExtra(UNIT_ID, unitId)
            intent.putExtra(USER_ID, userId)
            intent.putExtra(AMOUNT, amount)
            fragment.startActivityForResult(intent, CODE)
        }
    }

    private var mUnitId: String? = null
    private var mUserId: String? = null
    private var mAmount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trendy_wash_payment_gateway)

        mUnitId = intent?.getStringExtra(UNIT_ID)
        mUserId = intent?.getStringExtra(USER_ID)
        mAmount = intent?.getIntExtra(AMOUNT, 0) ?: 0

        setBackToolbar("เติมเงิน")

        tabCreditCard.findViewById<ImageView>(R.id.imageLogo).apply {
            setImageResource(R.drawable.ic_credit_card2)
        }
        tabCreditCard.findViewById<TextView>(R.id.textTitle).text = getString(R.string.credit_card)

        tabPromptPay.findViewById<ImageView>(R.id.imageLogo).apply {
            setImageResource(R.drawable.ic_transfer_money)
        }
        tabPromptPay.findViewById<TextView>(R.id.textTitle).text = getString(R.string.qr_payment)

        tabPaymentOptions.apply {
            addTab(
                    tabCreditCard,
                    tabPromptPay
            )
            addViewIdAndColorStatus(R.id.imageLogo, resources.getColor(R.color.textLight), resources.getColor(R.color.textAccent))
            addViewIdAndColorStatus(R.id.toggleArea, resources.getColor(R.color.colorAccent), resources.getColor(R.color.background))
            addViewIdAndColorStatus(R.id.textTitle, resources.getColor(R.color.textLight), resources.getColor(R.color.textPrimary))
        }

        tabPaymentOptions.setOnItemSelectedListener { position ->
            when (position) {
                0 -> {
                    if (mUnitId != null && mUserId != null) {
                        replaceFragment(CreditCardFragment.newInstance(mUnitId!!, mUserId!!, mAmount), false)
                    } else {
                        showError(getString(R.string.error_default_message))
                    }
                }
                1 -> {
                    if (mUnitId != null && mUserId != null) {
                        replaceFragment(TransferFragment.newInstance(mUnitId!!, mUserId!!, mAmount), false)
                    } else {
                        showError(getString(R.string.error_default_message))
                    }
                }
            }
        }
        tabPaymentOptions.selectItem(0)
        textBalance.text = "+$mAmount"
    }

    override fun onResume() {
        super.onResume()
        sendScreenView("TRENDY_WASH_TOP_UP_CHANNEL")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


    fun showError(message: String) {
        alertError(message)
    }
}

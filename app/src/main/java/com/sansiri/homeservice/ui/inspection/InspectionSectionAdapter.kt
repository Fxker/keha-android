package com.sansiri.homeservice.ui.inspection

import android.view.View
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Inspection
import com.sansiri.homeservice.ui.automation.viewholder.TitleViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by sansiri on 10/12/17.
 */
class InspectionSectionAdapter(val title: String, val mData: List<Inspection>, val itemClick: (String) -> Unit) : StatelessSection(SectionParameters.Builder(R.layout.view_inspection)
        .headerResourceId(R.layout.view_title)
        .build()) {

    override fun getContentItemsTotal(): Int = mData.size

    override fun getHeaderViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return TitleViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?) {
        if (holder is TitleViewHolder) {
            holder.bind(title)
        }
    }

    override fun getItemViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return InspectionViewHolder(view, itemClick)
    }


    override fun onBindItemViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, position: Int) {
        if (holder is InspectionViewHolder) {
            holder.bind(mData[position])
        }
    }

}
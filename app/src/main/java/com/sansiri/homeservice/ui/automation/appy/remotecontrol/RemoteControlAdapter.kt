package com.sansiri.homeservice.ui.automation.appy.remotecontrol

import android.view.ViewGroup
import com.appy.android.sdk.control.remotecontrol.APActionRemoteControl
import com.sansiri.homeservice.R
import com.sansiri.homeservice.util.inflate

class RemoteControlAdapter(val itemClick: (APActionRemoteControl) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<APRemoteControlViewHolder>() {
    private var mData = mutableListOf<APActionRemoteControl>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): APRemoteControlViewHolder {
        return APRemoteControlViewHolder(parent.inflate(R.layout.view_home_automation_remote_control_action), itemClick)
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: APRemoteControlViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    fun setData(data: List<APActionRemoteControl>) {
        mData.clear()
        mData.addAll(data)
    }

}
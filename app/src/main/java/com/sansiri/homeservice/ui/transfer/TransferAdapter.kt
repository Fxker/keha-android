package com.sansiri.homeservice.ui.transfer

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sansiri.homeservice.R
import com.sansiri.homeservice.model.api.Doc
import com.sansiri.homeservice.util.inflate
import kotlinx.android.synthetic.main.view_document.view.*

/**
 * Created by sansiri on 11/3/17.
 */
class TransferAdapter(val itemClick: (Doc) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<TransferAdapter.ViewHolder>() {
    val mData = mutableListOf<Doc>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransferAdapter.ViewHolder {
        val view = parent.inflate(R.layout.view_document)
        return ViewHolder(view, itemClick)
    }

    override fun getItemCount(): Int = mData.size

    fun setData(data: List<Doc>) {
        mData.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: Doc) {
        mData.add(data)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TransferAdapter.ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    class ViewHolder(val view: View, val itemClick: (Doc) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(doc: Doc) {
            with(view) {
                textTitle.text = doc.title
                root.setOnClickListener {
                    itemClick(doc)
                }
            }
        }
    }

}
package com.sansiri.homeservice.service

import android.annotation.SuppressLint
import android.provider.Settings
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.sansiri.homeservice.data.database.Store
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.model.api.RegisterNotificationRequest
import com.sansiri.homeservice.ui.language.getLanguageExtension
import com.sansiri.homeservice.util.observe
import java.util.*

/**
 * Created by sansiri on 12/26/17.
 */
class AppFirebaseInstanceIdService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        sendRegistrationToServer(deviceId)
    }

    @SuppressLint("HardwareIds")
    companion object {
        fun sendRegistrationToServer(deviceId: String) {
            this.sendRegistrationToServer(deviceId, Store.getLocale() ?: Locale.ENGLISH)
        }

        fun sendRegistrationToServer(deviceId: String, locale: Locale) {
            val refreshedToken = FirebaseInstanceId.getInstance().token
            refreshedToken?.let {

                val language = locale.getLanguageExtension()
                val requestBody = RegisterNotificationRequest(
                        it,
                        language
                )

                ApiRepositoryProvider.provideApiAuthRepository { api ->
                    if (api != null) {
                        api.registerNotification(deviceId, requestBody).observe().subscribe({}) {}
                    } else {
                        ApiRepositoryProvider.provideApiUnAuthRepository().registerNotification(
                                deviceId, requestBody
                        ).observe().subscribe({}) {}
                    }

                }

            }
        }
    }

}
package com.sansiri.homeservice.data.database

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.sansiri.homeservice.model.preference.HomeAutomationSelectionSave

/**
 * Created by oakraw on 30/12/2017 AD.
 */
object PreferenceHelper {
    val IS_FIRST_TIME = "IS_FIRST_TIME"
    val MY_HOME = "MY_HOME"
    val USER_ID = "USER_ID"
    val CONTENT_TEXT_SIZE = "CONTENT_TEXT_SIZE"
    val NOTIFICATION_ALL = "NOTIFICATION_ALL"
    val NOTIFICATION_COUNT = "NOTIFICATION_COUNT"
    val NOTIFICATION_ANNOUNCEMENT = "NOTIFICATION_ANNOUNCEMENT"
    val HOME_AUTOMATION_LOCAL_MODE = "HOME_AUTOMATION_LOCAL_MODE"
    val MECHANICAL_PARKING = "MECHANICAL_PARKING"
    val POPUP_READ = "POPUP_READ"
    val HOME_AUTOMATION_SELECTION = "HOME_AUTOMATION_SELECTION"
    val IS_FIRST_TIME_SVVH = "IS_FIRST_TIME_SVVH"
    val TREND_WASH_USER_ID = "TREND_WASH_USER_ID"

    fun defaultPrefs(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun customPrefs(context: Context, name: String): SharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    /**
     * puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key]
     */
    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit { it.putString(key, value) }
            is Int -> edit { it.putInt(key, value) }
            is Boolean -> edit { it.putBoolean(key, value) }
            is Float -> edit { it.putFloat(key, value) }
            is Long -> edit { it.putLong(key, value) }
            else -> throw UnsupportedOperationException("Not yet implemented")
        }

    }

    /**
     * finds value on given key.
     * [T] is the mailType of value
     * @param defaultValue optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
     */
    operator inline fun <reified T : Any> SharedPreferences.get(key: String, defaultValue: T? = null): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as? String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    fun clear(context: Context) {
        val prefs = defaultPrefs(context)
        prefs[MY_HOME] = null
        prefs[NOTIFICATION_ALL] = null
        prefs[MECHANICAL_PARKING] = null
        prefs[NOTIFICATION_COUNT] = 0
        prefs[NOTIFICATION_ANNOUNCEMENT] = null
        prefs[USER_ID] = null
        prefs[POPUP_READ] = null
        prefs[IS_FIRST_TIME_SVVH] = null
        prefs[TREND_WASH_USER_ID] = null
    }

    fun setReadPopup(context: Context, popupId: String) {
        val prefs = defaultPrefs(context)
        val popups = prefs.getStringSet(POPUP_READ, hashSetOf<String>())
        val set = hashSetOf<String>()
        set.addAll(popups)
        set.add(popupId)
        prefs.edit { it.putStringSet(POPUP_READ, set) }
    }

    fun isPopupIsRead(context: Context, popupId: String): Boolean {
        val prefs = defaultPrefs(context)
        val popups = prefs.getStringSet(POPUP_READ, hashSetOf<String>())
        return popups.contains(popupId)
    }

    fun saveHomeAutomationSelection(context: Context, unitId: String, deviceId: String, value: Map<String, String>? = null) {
        val json = Gson().toJson(HomeAutomationSelectionSave(unitId, deviceId, value))
        defaultPrefs(context).edit { it.putString(HOME_AUTOMATION_SELECTION, json) }
    }

    fun getHomeAutomationSelection(context: Context, unitId: String, deviceId: String): Map<String, String>? {
        val json = defaultPrefs(context).getString(HOME_AUTOMATION_SELECTION,  null)
        val save = Gson().fromJson(json, HomeAutomationSelectionSave::class.java)
        return if(save != null && save.unitId == unitId && save.deviceId == deviceId) save.value else null
    }

}
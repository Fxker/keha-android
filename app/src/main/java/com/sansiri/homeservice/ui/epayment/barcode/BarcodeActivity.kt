package com.sansiri.homeservice.ui.epayment.barcode

import android.os.Bundle
import com.sansiri.homeservice.ui.epayment.CodeEngine
import com.sansiri.homeservice.ui.epayment.EPaymentActivity
import com.sansiri.homeservice.util.hide
import com.sansiri.homeservice.util.show
import kotlinx.android.synthetic.main.activity_barcode.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

abstract class BarcodeActivity(val analyticCode: String) : EPaymentActivity<BarcodeContract.View, BarcodeContract.Presenter>(), BarcodeContract.View {
    override var mPresenter: BarcodeContract.Presenter = BarcodePresenter()

    lateinit var mCodeEngine: CodeEngine

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buttonDownload.hide()
        panelRotation.angle = 270
    }

    fun renderBarcode(code: String?) {
        val processCode = code?.replace("\n", " ")
        textBarcode.text = processCode

        mCodeEngine = CodeEngine(this)

        doAsync {
            val bitmap = mCodeEngine.createBarcode(code ?: "")
            val finalBitmap = mCodeEngine.createBarcodeWithCode(bitmap, processCode)
            uiThread {
                buttonDownload.show()
                buttonDownload.setOnClickListener {
                    sendEvent(analyticCode, "SAVE", "BARCODE")
                    save(finalBitmap)
                }
                imageBarcode.setImageBitmap(bitmap)
            }
        }

        buttonBack.setOnClickListener {
            onBackPressed()
        }
    }


    override fun showError(message: String) {
    }
}

package com.sansiri.homeservice.ui.voicecommand

import android.graphics.Bitmap
import android.os.SystemClock
import com.appy.android.sdk.control.APControl
import com.google.gson.Gson
import com.onionshack.onionspeech.*
import com.sansiri.homeservice.data.network.ApiRepositoryProvider
import com.sansiri.homeservice.data.repository.appy.AppyRepositoryImpl
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.api.facilitybooking.AppyError
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBooking
import com.sansiri.homeservice.model.api.facilitybooking.FacilityBookingRequest
import com.sansiri.homeservice.ui.base.BasePresenterImpl
import com.sansiri.homeservice.util.*
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.*
import com.sansiri.homeservice.model.api.Profile
import kotlin.collections.HashMap

/**
 * Created by sansiri on 11/3/17.
 */
class VoiceCommandPresenter : BasePresenterImpl<VoiceCommandContract.View>(), VoiceCommandContract.Presenter {
    private var mHome: Home? = null
    private var mAppyManager: AppyRepositoryImpl? = null

    private var cacheInvoice: HashMap<OnionHomeService.UtilityType, Double> = hashMapOf()
    private var cacheDate = Date()
    private var devices = mutableListOf<APControl>()
    private var mFacility: List<FacilityBooking>? = null
    private var mProfile: Profile? = null

    override fun init(home: Home, isMock: Boolean) {
//        this.mHome = home
//
//        ApiRepositoryProvider.provideApiAuthRepository { api ->
//            if (api != null) {
//                mCompositeDisposable.add(
//                        api.getMe().observe()
//                                .subscribe({
//                                    this.mProfile = it
//                                    if (!isMock) {
//                                        fetchData(it.objectId ?: "")
//                                    } else {
//                                        mView?.launchDemoVoiceCommandScreen(it.objectId ?: "")
//                                    }
//                                }) {}
//                )
//            }
//        }
    }

    override fun start() {
    }

    override fun destroy() {
        cacheInvoice = hashMapOf()
    }

    override fun fetchData(userId: String) {
//        if (mHome?.appyConfig?.appyId != null) {
////            mAppyManager = AppyRepositoryImpl("r9QphvUc9kw4", "12345")
////            mAppyManager?.getRooms("oAj08Ej7")?.observe()?.subscribe({ rooms ->
//            mAppyManager = AppyRepositoryImpl(mHome?.projectId
//                    ?: "", mHome?.appyConfig?.pin)
//            mAppyManager?.getRooms(mHome?.appyConfig?.appyId ?: "")?.observe()?.subscribe({ rooms ->
//                val onionRooms = mutableListOf<OnionRoom>()
//
//                val roomCount = rooms.size
//                var count = 0
//
//                rooms.forEach { room ->
//                    mAppyManager?.getControl(mHome?.appyConfig?.appyId ?: "", room.id
//                            ?: "")?.observe()?.subscribe({ devices ->
//                        this.devices.addAll(devices)
//                        val onionDevice = devices.map { device -> device.convert() }.filter { it.type != OnionDevice.Type.UNDEFINED }
//                        onionRooms.add(OnionRoom(room.id, room.title, "", onionDevice, room.titleAlternate))
//                        count += 1
//                        if (count >= roomCount) {
//                            fetchFacility(userId, onionRooms)
//                        }
//                    }) {}
//                }
//            }) {}
//        } else {
//            fetchFacility(userId, listOf())
//        }
    }


    private fun fetchFacility(userId: String, onionRooms: List<OnionRoom>) {
//        ApiRepositoryProvider.provideApiAuthRepository { api ->
//            if (api != null) {
//                mCompositeDisposable.add(
//                        api.getFacilityBooking(mHome?.projectId
//                                ?: "", true).observe().subscribe({ facilities ->
//                            mFacility = facilities
//                            val facilityOnion = mutableListOf<OnionFacility>()
//                            facilities.forEach { facility ->
//                                val timeSlot = facility.categoryData?.reservation?.dates?.first()?.times
//                                facilityOnion.add(OnionFacility(facility.id,
//                                        facility.findContentFromLocale("en")?.title,
//                                        parseAppyDate(timeSlot?.first()?.startTime!!),
//                                        parseAppyDate(timeSlot.last().startTime ?: "")))
//                            }
//
//                            mView?.launchVoiceCommandScreen(userId, onionRooms, facilityOnion)
//                        }) {
//                            mView?.launchVoiceCommandScreen(userId, onionRooms, listOf())
//                        })
//            }
//        }
    }

    var blindCount = 0

    override fun actionHomeAutomation(device: OnionDevice, param: OnionHomeControlParameter) {
//        val home = APHome()
//        home.id = mAppyManager?.getDedicatedUnitId(mHome?.appyConfig?.appyId ?: "")
//
//        val appyDevice = devices.find { it.id == device.id }
//
//        when (appyDevice?.type) {
//            AppyRepositoryImpl.Type.LIGHT.key -> {
//                mAppyManager?.commandLight(home, appyDevice as APLight, if (param.controlType.ordinal == OnionHomeControlParameter.ControlType.On.ordinal) 1 else 0, {}) {}
//            }
//            AppyRepositoryImpl.Type.TOGGLE.key -> {
//                mAppyManager?.commandToggleSwitch(home, appyDevice as APToggleSwitch, if (param.controlType.ordinal == OnionHomeControlParameter.ControlType.On.ordinal) 1 else 0, {}) {}
//            }
//            AppyRepositoryImpl.Type.DIMMER.key -> {
////                if (param.controlType.ordinal == OnionHomeControlParameter.ControlType.Percent.ordinal) {
////                    mAppyManager?.commandDimmer(mHome?.appyUnitCode ?: "", device.id, Math.round(param.percent / 10).toInt(), {}) {}
////                } else {
////                    mAppyManager?.commandDimmer(mHome?.appyUnitCode ?: "", device.id, if (param.controlType.ordinal == OnionHomeControlParameter.ControlType.On.ordinal) 10 else 0, {}) {}
////                }
//            }
//            AppyRepositoryImpl.Type.BLIND.key -> {
//                blindCount += 1
//                mAppyManager?.commandBlind(home,
//                        appyDevice as APBlind,
//                        if (param.controlType.ordinal == OnionHomeControlParameter.ControlType.On.ordinal) BlindCommand.ON.ordinal else BlindCommand.OFF.ordinal,
//                        blindCount * 1000,
//                        {
//                            blindCount = 0
//                            Log.d("", "")
//                        }) {
//                    Log.d("", "")
//                }
//            }
//        }
    }

    override fun actionCheckInvoice(callback: (Map<OnionHomeService.UtilityType, Double>, Date) -> Unit) {
//        if (cacheInvoice.size > 0) {
//            callback(cacheInvoice, cacheDate)
//            return
//        }
//
//        ApiRepositoryProvider.provideApiAuthRepository { api ->
//            if (api != null && mHome != null) {
//                if (mHome!!.isTransfer) {
//                    // Reset Cache
////                    cacheInvoice = hashMapOf()
////                    val unitId = mHome?.unitId ?: ""
////                    Observable.zip(
////                            api.getInvoicePayableV2(unitId).map {
////                                val map = hashMapOf<OnionHomeService.UtilityType, Double>()
////                              TODO: make to compat with new api
//// it.forEach { invoice ->
////                                    val type = when (invoice.loanType) {
////                                        Invoice.COMMON, Invoice.COMMON_FACTOR -> OnionHomeService.UtilityType.CommonFee
////                                        Invoice.WATER -> OnionHomeService.UtilityType.Water
////                                        Invoice.INSURANCE -> OnionHomeService.UtilityType.Insurance
////
////                                        else -> null
////                                    }
////
////                                    if (type != null) {
////                                        map[type] = invoice.invoiceAmount
////                                    }
////                                }
//
////                                cacheInvoice.putAll(map)
////                                it
////                            },
////                            api.getBalanceV2(unitId).map {
////                                cacheInvoice[OnionHomeService.UtilityType.Deposit] = it.balance
////                                it
////                            },
////                            BiFunction { t1: Invoice, t2: BalanceV2 ->
////
////                            })?.observe()?.subscribe({
////                        callback(cacheInvoice, Date())
////                    }) {
////                        callback(cacheInvoice, Date())
////                    }
//                } else {
//                    mCompositeDisposable.add(
//                            api.getDownpayment(mHome?.unitId ?: "").observe()
//                                    .subscribe({ downpayment ->
//                                        val map = hashMapOf<OnionHomeService.UtilityType, Double>()
//
//                                        var overDue = 0.0
//                                        downpayment.items?.forEach { downpaymentItem ->
//                                            when {
//                                                downpaymentItem.isOverdue -> {
//                                                    overDue += downpaymentItem.cost
//                                                }
//                                            }
//                                        }
//
//                                        if (overDue > 0) {
//                                            map.put(OnionHomeService.UtilityType.OverduePayment, overDue)
//                                        }
//
//                                        val today = Calendar.getInstance().time
//                                        var upcomingIndex = 0
//                                        run breaker@{
//                                            downpayment.items?.forEachIndexed { index, downpaymentItem ->
//                                                if (downpaymentItem.date != null) {
//                                                    if (downpaymentItem.date!!.after(today)) {
//                                                        upcomingIndex = index
//                                                        return@breaker
//                                                    }
//                                                }
//                                            }
//                                        }
//
//                                        val upcomingDate = downpayment.items?.getOrNull(upcomingIndex)
//                                        if (upcomingDate != null) {
//                                            map.put(OnionHomeService.UtilityType.UpcomingPayment, upcomingDate.cost)
//                                        }
//
//                                        cacheInvoice = map
//
//                                        if (upcomingDate != null) {
//                                            cacheDate = upcomingDate.date!!
//                                        }
//                                        callback(map, cacheDate)
//                                    }) {
//                                        callback(mapOf(), Date())
//                                    }
//                    )
//                }
//            } else {
//                callback(mapOf(), Date())
//            }
//        }

    }

    override fun uploadImage(image: Bitmap, onSuccess: (String) -> Unit, onError: (Throwable) -> Unit) {
        ApiRepositoryProvider.provideApiAuthRepository { api ->
            if (api != null) {
                val byteArray = image.compress()
                mCompositeDisposable.add(
                        api.uploadFile("${SystemClock.uptimeMillis()}.jpg", byteArray).observe().subscribe({
                            onSuccess(it[0])
                        }) {
                            onError(it)
                        }
                )
            }
        }
    }

    override fun requestToOpenFacility(id: String) {
    }

    override fun bookFacility(id: String, startTime: Date, endTime: Date, onSuccess: () -> Unit, onError: (OnionHomeService.BookFacilityCallback.BookingResult, Map<OnionHomeService.BookFacilityCallback.BookingResultAdditionalInfo, Object>) -> Unit) {
        val facility = mFacility?.find { it.id == id }
        if (facility != null) {
            ApiRepositoryProvider.provideApiAuthRepository { api ->
                if (api != null) {
                    api.submitFacilityBooking(mHome?.projectId ?: "", mHome?.unitId
                            ?: "", FacilityBookingRequest(facility.id!!, startTime.reportAppyFormat(), endTime.reportAppyFormat(),"", mProfile?.firstname, mProfile?.lastname, mHome?.address)).observe().subscribe({
                        onSuccess()
                    }) {
                        if (it is HttpException) {
                            val error = it.response()?.errorBody()?.string()
                            if (error != null) {
                                try {
                                    val appyError = Gson().fromJson(error, AppyError::class.java)
                                    val errorCode = appyError?.errors?.getOrNull(0)
                                    if (errorCode != null) {
                                        when (errorCode) {
                                            "reservation_book_limit_reach" -> onError(OnionHomeService.BookFacilityCallback.BookingResult.ExceedMaximum, mapOf())
                                            "date_has_passed",
                                            "slot_invalid",
                                            "reservation_not_found",
                                            "booking_deadline_passed",
                                            "reservation_has_been_booked" -> onError(OnionHomeService.BookFacilityCallback.BookingResult.Busy, mapOf())
                                            else -> {
                                                onError(OnionHomeService.BookFacilityCallback.BookingResult.Busy, mapOf())
                                            }
                                        }
                                    } else {
                                        onError(OnionHomeService.BookFacilityCallback.BookingResult.Busy, mapOf())
                                    }
                                } catch (e: Exception) {
                                    onError(OnionHomeService.BookFacilityCallback.BookingResult.Busy, mapOf())
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun findVacancyFacility(facility: FacilityBooking, startTime: Date) {
//        facility.categoryData?.reservation?.dates?.forEach { item ->
//            item._date?.
//        }
    }

    private fun parseAppyDate(date: String): Date? {
        val format: SimpleDateFormat
        try {
            format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss X")
        } catch (e: Exception) {
            return null
        }
        return format.parse(date)
    }
}
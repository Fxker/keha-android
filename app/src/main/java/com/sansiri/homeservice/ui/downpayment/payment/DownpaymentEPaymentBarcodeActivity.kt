package com.sansiri.homeservice.ui.downpayment.payment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import com.sansiri.homeservice.model.Home
import com.sansiri.homeservice.model.Hut
import com.sansiri.homeservice.model.api.downpayment.DownpaymentItem
import com.sansiri.homeservice.ui.epayment.barcode.BarcodeActivity
import com.sansiri.homeservice.ui.epayment.barcode.BarcodeContract
import com.sansiri.homeservice.ui.epayment.barcode.BarcodePresenter

class DownpaymentEPaymentBarcodeActivity : BarcodeActivity("DOWNPAYMENT_EPAYMENT_BARCODE") {
    override var mPresenter: BarcodeContract.Presenter = BarcodePresenter()
    var mDownPaymentItem: DownpaymentItem? = null
    var mHome: Hut? = null

    companion object {
        val PAYMENT = "PAYMENT"
        val HOME = "HOME"
        fun start(activity: Activity, home: Hut, downPaymentItem: DownpaymentItem) {
            val intent = Intent(activity, DownpaymentEPaymentBarcodeActivity::class.java)
            intent.putExtra(PAYMENT, downPaymentItem)
            intent.putExtra(HOME, home)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDownPaymentItem = intent.getParcelableExtra<DownpaymentItem>(DownpaymentEPaymentActivity.PAYMENT)
        mHome = intent.getParcelableExtra(DownpaymentEPaymentActivity.HOME)

        renderBarcode(mDownPaymentItem?.paymentBarcode)
    }

    override fun getDetailBitmap(): Bitmap = createDetailBitmap(this, mHome, mDownPaymentItem)

    override fun onResume() {
        super.onResume()
        sendScreenView("DOWNPAYMENT_EPAYMENT_BARCODE")
    }


    override fun showError(message: String) {
    }
}
